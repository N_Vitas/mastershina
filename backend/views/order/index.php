<?php 
use yii\helpers\Html;
use common\models\Order;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

$this->title = 'Список заявок';
$this->params['breadcrumbs'][] = ['label' => $this->title];
$list = $searchModel->list == 'true' ? 'true': 'false';
$closed = $searchModel->closed == 'true' ? 'true': 'false';
$this->registerJs("
  $('.grid-view').height($(document).height()-290);
");
//   var g = $("#w0");
//   g.append(\'<input type="hidden" id="searchorder-list" value="false" name="SearchOrder[list]"/>\');
// ');
  // jQuery('#w0').yiiGridView({"filterUrl":"/","filterSelector":"#w0-filters input, #w0-filters select"});
?>
<div class="card" ng-controller="orderList">
  <nav class="navbar navbar-primary card-header" data-background-color="purple" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?= $this->title?></a>
    </div>
    <ul class="nav navbar-nav">
        <li>
          <div class="checkbox">
            <label>
              <input type="checkbox" ng-init="items.check=<?= $list?>" ng-model="items.check" ng-click="items.checkbox()" id='list' <?= $searchModel->list ? 'checked' : '';?>  ng-value="<?= $list?>" class="checkbox" name="list">
              <span class="label label-lg label-primary">Все заявки</span>
            </label>
          </div>
        </li>
        <li>
          <div class="checkbox">
            <label>
              <input type="checkbox" ng-value="items.closed=<?= $closed?>" ng-model="items.closed" ng-click="items.boxclosed()" class="checkbox">
              <span class="label label-lg label-primary">Закрытые заявки</span>
            </label>
          </div>
        </li>
    </ul>
    <a class="btn btn-simple btn-just-icon pull-right" rel="tooltip" title="Сброс фильтра" href="/order/index"><i class="material-icons">sync</i></a>
  </div>    
  </nav>
  <div class="card-content table-responsive">
    <?= yii\grid\GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
      /*Очередь столбцов id id_crm дата создания  имя телефон статусы (заявка доставка оплата) товар сумма комментарий тема email адрес автор ответственный*/
      'columns' => [
          [
            'attribute' => 'id',
            'format' => 'raw',
            'value' => function($model){
              return "<a class='' href='/order/view?id=".$model->id."'><strong>".$model->id."</strong></a>";
            },
          ],
          [
            'attribute' => 'id_crm',
            'format' => 'raw',
            'value' => function($model){
              return "<a class='' href='/order/view?id=".$model->id."'><strong>".$model->id_crm."</strong></a>";
            },
          ],
          [
            'attribute' => 'created_at',
            'format' => 'raw',
            'value' => function($model){
              return "<a class='' href='/order/view?id=".$model->id."'><strong>".date ( "d-m-Y H:i" ,$model->created_at )."</a>";
            },
            'filter' => DatePicker::widget([
              'model' => $searchModel,
              'language' => 'ru',
              'type' => DatePicker::TYPE_INPUT,//TYPE_COMPONENT_PREPEND TYPE_INPUT TYPE_COMPONENT_APPEND TYPE_RANGE TYPE_INLINE TYPE_BUTTON
              'value' => function($model){
                return $model->created_at;
              },
              'attribute' => 'created_at',
              'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy'
              ]
            ])
          ],
          'name',
          'phone',
          [
            'attribute'=>'status',
            'format' => 'raw',
            'value' => function($model){
              return "<span class='label label-".$model::$status_style[$model->status]."'>".$model::$statuses['order'][$model->status]."</span>";
            },
            'filter' => Html::activeDropDownList($searchModel, 'status',Order::$statuses['order'],['class'=>'form-control'])
            // 'filter' => Select2::widget([
            //   'model' => $searchModel,
            //   'attribute' => 'status',
            //   'theme' => Select2::THEME_BOOTSTRAP,
            //   'data' => \common\models\Order::$statuses['order'],
            //   'options' => ['placeholder' => 'Select states ...']
            // ])
          ],
          [
            'attribute'=>'status_payment',
            'format' => 'raw',
            'value' => function($model){
              return "<span class='label label-".$model::$status_style[$model->status_payment]."'>".$model::$statuses['payment'][$model->status_payment]."</span>";
            },
            'filter' => Html::activeDropDownList($searchModel, 'status_payment',Order::$statuses['payment'],['class'=>'form-control'])
          ],
          [
            'attribute'=>'status_delivery',
            'format' => 'raw',
            'value' => function($model){
              return "<span class='label label-".$model::$status_style[$model->status_delivery]."'>".$model::$statuses['delivery'][$model->status_delivery]."</span>";
            },
            'filter' => Html::activeDropDownList($searchModel, 'status_delivery',Order::$statuses['delivery'],['class'=>'form-control'])
          ],
          [
            'attribute' => 'data',
            'format' => 'raw',
            'value' => function($model){
              $pos = json_decode($model->data,true);
              $credit = strripos($model->comment,'Покупка в кредит') ? 'true':'false';
              $link = "<a href='#' ng-click='items.execute(".$model->data.",$credit)'>".$pos[0]['title']."</a>";
              return $link;
              // return $pos[0]['title'];
            }
          ],
          [
            'attribute' => 'price',
            'value' => function($model) {
                return number_format($model->price,0,',',' ')." тг.";
            },
          ],
          [
            'attribute'=>'comment',
            'value'=>function($model){
              $array = json_decode($model->comment,true);              
              if(is_array($array) && count($array)){
                return $array[count($array)-1]['comment'];
              }
              return $model->comment;
            }
          ],
          [
            'attribute' => 'theme',
            'value' => function($model) {
                return Order::$themes[$model->theme];
            },
          ],
          // 'theme',
          'email',
          'address',
          [
            'attribute' => 'author',
            'value' => function($model) {
                return $model->user->username;
            },
          ],
          [
            'attribute' => 'manager_crm',
            'value' => function($model) {
                return $model->manager ? $model->manager->username : 'Не назначен';
            },
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}'
          ],
          // [
          //   'attribute' => 'created_at',
          //   'format' => 'raw',
          //   'value' => 'created_at',
          //   'filter' => '<input type="text" value="2012-05-15 21:05" id="datetimepicker" data-date-format="yyyy-mm-dd hh:ii">',
          // ],
      ],
      'layout' => "<div class='clearfix'></div>{items}<div class='clearfix'></div>{pager}<span class='pull-right'>{summary}</span>",
      'summary' => "Показано {count} из {totalCount}",
      'emptyText' => 'Ничего не найдено',
    ]); ?>
  </div>
</div>