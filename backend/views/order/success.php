<?php 
use frontend\widgets\MenuLeft;
?>
<div class="row">
  <div class="col-md-3">
    <?= MenuLeft::widget()?>
  </div>
  <div class="col-md-9">
  	<!-- Block basket -->  	
		<div class="alert alert-success">
			<strong>Заказ оформлен!</strong> В течении дня мы свяжимся с вами!
		</div>
	</div>
</div>