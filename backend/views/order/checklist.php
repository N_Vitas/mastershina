<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$now = date("d-m-Y");
$total = 0; $i = 1;
?>
<table cellspacing="0" cellpadding="0" style="border: 1px solid #c9c9c9; padding: 0px; margin: 0px; font-size:10px;">
	<tbody>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="5"><center><strong>Чек-заявка для оформления кредита №<?=$model["id"]?> от <?= $now?></strong></center></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Цены действительны до:</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top; color: red;"><?= date("d-m-Y",strtotime($now." +2 days"))?></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>ДБ АО «Банк Хоум Кредит»</b></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>ТОО "OUTDOOR PRODUCTION"</b></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Шинный центр МАСТЕРШИНА</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ФИО клиента:</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><?= $model["name"]?></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Город:</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><?= $model["address"]?></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Код тт:</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">051057</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<!-- <tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr> -->
		<tr>
			<td style="background: #c9c9c9; border: 1px dotted black; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>№</b></td>
			<td style="background: #c9c9c9; border: 1px dotted black; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2"><b>Наименование товара</b></td>
			<td style="background: #c9c9c9; border: 1px dotted black; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>Кол-во</b></td>
			<td style="background: #c9c9c9; border: 1px dotted black; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>Стоимость</b></td>
		</tr>
		<?php foreach($model["data"] as $item):?>
		<?php 
			$price = $item["count"]*$item["old_price"];
			$total = $total + $price;
		?>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><?= $i?></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2"><?= $item["title"]?></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><?= $item["count"]?></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><?= $price?></td>
		</tr>
		<?php $i++?>
		<?php endforeach;?>
		<tr>
			<td style="background: #c9c9c9; border: 1px dotted black height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="background: #c9c9c9; border: 1px dotted black height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>Итого</b></td>
			<td style="background: #c9c9c9; border: 1px dotted black height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="background: #c9c9c9; border: 1px dotted black height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="background: #c9c9c9; border: 1px dotted black height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b><?= $total?></b></td>
		</tr>
		<!-- <tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr> -->
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>Памятка для клиента:</b></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">1.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				Просим Вас обратиться для консультации по условиям оформления потребительского кредита в ближайщий для Вас административный пункт Банка Хоум Кредит с данной чек-заявкой.
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">2.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				После одобрения заказа Банком с вами свяжутся менеджера шинного центра для согласования даты и способа передачи товара.
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">3.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				Для оформления кредита необходимо иметь при себе удостоверение личности. Для получателей пенсии необходимо дополнительно пенсионное удостоверение.
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">4.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				Банк Хоум Кредит не выдает кредит, если человек прописан в другом городе. Например: если вы живете в Алматы, а прописаны в Караганде, то вам нужно обратиться в филиал банка в Караганде.
			</td>
		</tr>
		<!-- <tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr> -->
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4"><b>Список отделений Банка Хоум Кредит</b></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>Торговый центр</b></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><b>Торговая точка</b></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2"><b>Адрес</b></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">12 месяцев</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">строительный магазин</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">пр.Райымбека, 512, уг.ул.Саина</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТЦ Гранд Парк</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">6 блок</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Кабдолова, 1</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Евразия</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">СТ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул. Утеген батыра 11А</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">CARREFOUR</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Галерея сотовых телефонов</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Кабдолова, 1/4</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">АРМАДА</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Евро-Мебель, блок 2, ряд 5Д</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Кабдолова, 1/8</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">БЦ ЛОТОС</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Chocomart, 3 этаж, вход с торца</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">уг.ул.Масанчи</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">МАСТЕРГАЗ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">установка газ.об-ния, напротив СТО GT SERVICE</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Тлендиева, 258Д</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД BAUMARKT</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">строительный магазин</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Толе би, 189а, уг.ул.Розыбакиева</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД ГУМ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">сотовые телефоны (эскалатор)</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Кунаева, 21, уг.ул.Маметовой</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД ЛЮМИР</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТОО "Опт Трэйд", отопительные котлы, 1 этаж</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">пр.Саина, уг.ул.Шаляпина</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД МЕРЕЙ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">мебель (рядом с таможней)</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">пр.Суюнбая, 2, уг.пр.Райымбека</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД МОБИЛА</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">сотовые телефоны слева от входа</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Макатаева, 81, уг.ул.Абылай хана</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД САЛАМАТ 3</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">53 бутик</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Розыбакиева,72, уг.ул.Шевченко</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТД СТУДЕНТ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">2 этаж 	АТАКЕНТ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Тимирязева, 42, уг.ул.Манаса</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТРК Mega Center Alma-Ata</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">м. IPOINT</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Розыбакиева, 247а.</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">ТЦ КУНГЕЙ</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">мебельный магазин	Алматы-1</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул Молдагалиева, 2</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Шинный центр</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">МАСТЕРШИНА</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="2">ул.Садовникова, 99, уг.ул.Сатпаева (АДК)</td>
		</tr>
		<!-- <tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr> -->
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4"><b>Памятка для кредитного консультанта Банка:</b></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">1.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				Кредит оформляется в стандартном порядке на основании данных указанных в чек-заявке.
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">2.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				Сверить срок действия ценового предложения. В случае истечения срока необходимо написать запрос на актуализацию цен (указав номер заявки).
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">3.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				После одобрения заказа необходимо отправить письмо-подтверждение на info@mastershina.kz, в теме указать Одобрен заказ № 0000 Сумма 0000 и приложитьскан. варианты следующих документов:<br/>
				1) подписанный пакет документов для магазина (заявление на получение кредита, памятка, спецификация товара, копия удостоверения личности)<br/>
				2) скан. чек-заявки
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">4.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				Дождаться ответа от специалистов партнера «Заказ № 0000 подтвержден»
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">5.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				В случае отказа в выдаче кредита сообщить об этом посредством эл. почты, указав в теме письма Отказано в  заказе № 0000
			</td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">6.</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4">
				После подтверждения об одобрении менеджер шинного центра свяжется с клиентом для согласования даты и способа передачи товара.
			</td>
		</tr>
		<!-- <tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr> -->
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;" colspan="4"><b>Контакты шинного центра для актуализации цен и подтверждения заказа:</b></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">e-mail:</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"><a href="mail:info@mastershina.kz">info@mastershina.kz</a></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
		<tr>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">Телефон:</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;">+7 (727) 390 64 04, +7 777 7 157 157</td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
			<td style="border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;"></td>
		</tr>
	</tbody>
</table>