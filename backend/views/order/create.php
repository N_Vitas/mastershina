<?php

/* @var $this yii\web\View */

$this->title = 'Оформление заказа';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="row" ng-controller="BasketForm">
  <!-- end left -->
  <div class="col-md-6">
		<div class="card">
			<div class="card-header card-header-group" data-background-color="purple">
		    <h4 class="title">Данные клиента</h4>
		    <button class="btn btn-primary btn-just-icon" rel="tooltip" title="Очистить контрагента" ng-click="orderform.createClient()">
					<i class="material-icons">group_add</i>
				</button>
		  </div>
			<div class="card-content"><!-- Block forms -->
				<form class="basketForm" id="basketForm" name="basketForm">
					<input type="hidden" name="delivery" value="{{basket.delivery_label[basket.delivery]}}" />
					<input type="hidden" name="payment" value="{{basket.payment}}" />
					<input type="hidden" name="data" value="{{basket.positions}}" />
					<input type="hidden" name="theme" value="{{orderform.theme}}" />
					<input type="hidden" name="price" value="{{basket.total()}}" />
					<input type="hidden" name="author" value="<?= yii::$app->user->id?>" />
					<input type="hidden" name="role" value="<?= Yii::$app->user->identity->getRoleName()?>" />						
					<div class="form-group">
		        <label for="client">Тема</label>
		        <select class="form-control" ng-model="orderform.theme" ng-change="orderform.changeTheme()" ng-options="v.id as v.value for v in orderform.themes">
						</select> 
					</div>		
								<!-- <option value="0" selected="true">Выберите на кого оформить заказ</option>						
								<option ng-repeat="option in orderform.clients" value="{{option.id}}">{{option.name}}</option> -->
					<div ng-init="orderform.loadClient()" class="form-group dropdown {{orderform.fClients.name.length > 0 ? 'open':''}} {{orderform.errors.name ? '' : 'has-error'}}"> <!-- ng-hide="orderform.client" -->
						<label class="control-label">Имя, Фамилия? <span ng-hide="orderform.errors.name" class="block-error">Должно быть минимум 2 символов</span></label>
						<input autocomplete="off" type="text" name="name" ng-change="orderform.changename()" ng-model="orderform.name" class="form-control" autofocus="" placeholder="Как менеджеру обратиться к Вам по телефону."/>
						<ul class="dropdown-menu">
							<li ng-repeat="p in orderform.fClients.name"><a href="javascript:void(null)" ng-click="orderform.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
						</ul>
					</div>
					
					<div class="form-group dropdown {{orderform.fClients.email.length > 0 ? 'open':''}} {{orderform.errors.email?'':'has-error'}}"> <!-- ng-hide="orderform.client" -->
					  <label class="control-label">E-mail <span ng-hide="orderform.errors.email" class="block-error">не соответствует формату email</span></label>
					  <input autocomplete="off" type="text" name="email" ng-change="orderform.changemail()" ng-model="orderform.email" class="form-control" placeholder="На этот ящик будут приходить уведомления о статусе Вашего заказа."/>
					  <ul class="dropdown-menu">
							<li ng-repeat="p in orderform.fClients.email"><a href="javascript:void(null)" ng-click="orderform.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
						</ul>
					</div>
								  
					<div class="form-group dropdown {{orderform.fClients.phone.length > 0 ? 'open':''}} {{orderform.errors.phone ? '' : 'has-error'}}"> <!-- ng-hide="orderform.client" -->
					  <label class="control-label">Телефон <span ng-hide="orderform.errors.phone" class="block-error">не соответствует формату телефона</span></label>
					  <input autocomplete="off" type="text" name="phone" id="phone" ng-change="orderform.changephone()" ng-model="orderform.phone" class="form-control" placeholder="Например +77273170077"/>	
					  <ul class="dropdown-menu">
							<li ng-repeat="p in orderform.fClients.phone"><a href="javascript:void(null)" ng-click="orderform.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
						</ul>
					</div>
					
					<div class="form-group {{orderform.errors.address?'':'has-error'}}"> <!-- ng-hide="orderform.client" -->
					  <label class="control-label">Адрес доставки <span ng-hide="orderform.errors.address" class="block-error">минимум 6 символа</span></label>
					  <textarea class="form-control" ng-change="orderform.changeaddress()" name="address" ng-model="orderform.address" placeholder="Например г.Алматы, ул. Садовникова, 99 (Лазарева)"></textarea>
					</div>
					
					<div class="form-group">
					  <label class="control-label">Комментарий</label>
					  <textarea class="form-control" name="comment" ng-model="orderform.comment" ng-bind="basket.credit ? 'Покупка в кредит':''"></textarea>
					</div>
				</form>	
				<div class="form-group">    
				  <div class="well well-small">
						<div class="title"><span>Доставка</span></div>	
				    <div class="radio">	      
			        <label><input type="radio" checked="checked" name="delivery" ng-click="orderform.validateForm()" ng-model="basket.delivery" value="samovyvos" />{{basket.delivery_label['samovyvos']}}</label> 
			        <label><input type="radio" checked name="delivery" ng-click="orderform.validateForm()" ng-model="basket.delivery" value="city_square" />{{basket.delivery_label['city_square']}}</label>     
			        <label><input type="radio" checked name="delivery" ng-click="orderform.validateForm()" ng-model="basket.delivery" value="region" />{{basket.delivery_label['region']}}</label>     
				    </div>
				    <div class="form-group" ng-show="basket.delivery == 'region'">
		          <label for="city">{{basket.delivery_label['city']}}</label>
		          <select class="form-control" ng-model="basket.city" name="city">
								<option ng-repeat="option in basket.delivery_option.region" value="{{option.title}}">{{option.title}}</option>
							</select> 
		          <label for="type">{{basket.delivery_label['type']}}</label>
		          <select class="form-control" ng-model="basket.type"="type">
								<option value="Post Exspres">Компания Post Exspres - 1 300 тг. за колесо</option>
								<option value="DPD">Компания DPD - 1 500 тг. за колесо</option>
							</select>        
						</div>
				  </div>
				</div>
		    <div class="alert alert-success">
		      <p class="title-black">Сумма : {{t(basket.priceTotal)}}</p>
		      <p class="title-black" ng-show="basket.delivery == 'region'">Доставка : {{t(basket.deliveryTotal)}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<span>Срок {{basket.delivery_time[basket.city]}}</span></p>
		      <p class="title-black" ng-show="basket.delivery == 'city_square'">Доставка : {{basket.deliveryTotal > 0 ? t(basket.deliveryTotal) : "бесплатно"}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<span>Срок на следующий день</span></p>  
		      <p class="title-black">Итого : {{t(basket.priceTotal + basket.deliveryTotal)}}</p>
		    </div>
				<!-- End block basket -->			
				
				<div class="form-group">
	        <button ng-click="orderform.sendBasketForm()" ng-disabled="basket.setLoadStatus()" class="btn btn-primary" name="contact-button">Оформить</button>
	      </div>
			</div>
		</div>
  </div>
  <div class="col-md-6">
  	<div class="card"> 		
			<nav class="navbar navbar-primary card-header" data-background-color="purple" role="navigation">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <span class="navbar-brand">Список товаров</span>
			    </div>
			    <ul class="nav navbar-nav">
        		<li>
					    <div class="checkbox">
					      <label>
					        <input type="checkbox" ng-model="basket.credit" ng-click="basket.total()" class="checkbox"/>
					        <span class="label label-lg label-primary">В кредит</span>
					      </label>
					    </div>
        		</li>
        	</ul>
			    <!-- <a class="btn btn-simple btn-just-icon pull-right" rel="tooltip" title="Сброс фильтра" href="/order/index"><i class="material-icons">sync</i></a> -->
			    <button class="btn btn-primary btn-just-icon pull-right" rel="tooltip" title="Добавить товар" ng-click="createorder.addItems()">
						<i class="material-icons">note_add</i>
					</button>
			  </div>    
		  </nav>
			<div class="card-content">
				<div class="row title" ng-show="basket.positions.length > 0">			
					<div class="hidden-xs col-md-3"><p class="">Наименование</p></div>
					<div class="hidden-xs col-md-2"><p class="">Кол.</p></div>
					<div class="hidden-xs col-md-2"><p class="">Цена</p></div>
					<div class="hidden-xs col-md-2"><p class="">Сумма</p></div>	
				</div>
				<div class="striped" ng-repeat="pos in basket.positions">
					<div class="row">
						<div class="col-md-3"><div class="vertical basket-title">{{pos.title}}</div></div>
						<div class="col-md-2">			
				      <div class="input-group verticalinput">
				      	<input type="text" ng-model="pos.count" ng-change="basket.updateCart(pos)" class="form-control text-center"/>
				      	<span class="input-group-btn-vertical">
				      		<button ng-click="basket.up(pos)" class="btn btn-primary bootstrap-touchspin" type="button"><i class="glyphicon glyphicon-chevron-up"></i></button>
				      		<button ng-click="basket.down(pos)" class="btn btn-primary bootstrap-touchspin" type="button"><i class="glyphicon glyphicon-chevron-down"></i></button>
				      	</span>
				      </div>
						</div>
						<div class="col-md-2"><div class="vertical basket-price" ng-bind="basket.credit ? t(pos.old_price) : t(pos.price)"></div></div>
						<div class="col-md-2"><div class="vertical basket-summ" ng-bind="basket.credit ? t(pos.old_price * pos.count) : t(pos.price * pos.count)"></div></div>
						<div class="hidden-xs col-md-1"><div class="vertical basket-remove"><a href="#" onclick="return false" ng-click="basket.deleteCart(pos.position)"><i class="fa fa-trash"></i></a></div></div>
						<div class="visible-xs col-md-12"><a href="#" class="btn btn-theme btn-block" onclick="return false" ng-click="basket.deleteCart(pos.position)"><i class="fa fa-trash"></i></a></div>
					</div>
				</div>
		    <div class="alert alert-warning alert-with-icon" ng-hide="basket.positions.length > 0">
            <i data-notify="icon" class="material-icons">info</i>
            <h4 class="title">В списке отсутствуют товары</h4>
            <span data-notify="message">Для оформления заказа необходимо добавить в корзину минимум один товар!</span>
        </div>
			</div>
		</div>
  </div>
</div>