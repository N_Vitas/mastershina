<?php
use yii\helpers\Html;
use common\models\Order;
use common\models\User;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

$this->title = "Заявка $model->id";
$this->params['breadcrumbs'][] = ['label' => "Список заявок","url"=>"/order"];
$this->params['breadcrumbs'][] = ['label' => $this->title];
$model->comment = json_decode($model->comment,true);
$model->data = json_decode($model->data,true);
$this->registerJS("
	$('.show').click(function(){
		$('#cheklist').modal('show');		
		$('.modal-backdrop').hide();
	})
	$('.send').click(function(){
		$.ajax({
			type:'POST',
			url:'/api/send-checklist',
			data:{id:".$model->id."}
		}).done(function(data){
			if(data.result){
				$.notify({
		    	icon: 'check',
		    	message: 'Письмо <b>Успешно</b> отправлено.'

		    },{
		        type: type[2],
		        timer: 4000,
		        placement: {
		            from: 'bottom',
		            align: 'right'
		        }
		    });	
      }else{
      	$.notify({
		    	icon: 'error_outline',
		    	message: 'Не удалось отправить письмо.'

		    },{
		        type: type[4],
		        timer: 4000,
		        placement: {
		            from: 'bottom',
		            align: 'right'
		        }
		    });
      }
		});
		$('#cheklist').modal('hide');	
	})
");
?>
<div class="row">
	<div class="col-md-6">
		<div class="card">
		  <div class="card-header" data-background-color="purple">
		    <h4 class="title"><?= $this->title?></h4>
		    <p class="category">Дата создания <?= date ( "d-m-Y H:i" ,$model->created_at )?></p>
		  </div>
		  <div class="card-content table-responsive">
		    <table class="table table-hover">
		      <tbody>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('theme')?></strong></td>
		          <td><?= Order::$themes[$model->theme]?></td>
		      	</tr>
		        <?php if($model->id_crm):?>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('id_crm')?></strong></td>
		          <td><?= $model->id_crm?></td>
		      	</tr>
		      	<?php endif;?>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('status')?></strong></td>
		          <td><?= Order::$statuses['order'][$model->status]?></td>
		      	</tr>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('status_payment')?></strong></td>
		          <td><?= Order::$statuses['payment'][$model->status_payment]?></td>
		      	</tr>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('status_delivery')?></strong></td>
		          <td><?= Order::$statuses['delivery'][$model->status_delivery]?></td>
		      	</tr>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('delivery')?></strong></td>
		          <td><?= $model->delivery?></td>
		      	</tr>
		      	<tr>
		      		<td><strong><?= $model->getAttributeLabel('payment')?></strong></td>
		          <td><?= $model->payment == 'В кредит' ? $model->payment.' <button class="btn btn-xs btn-primary pull-right show">Чек заявка</button>' : $model->payment?></td>
		      	</tr>
		      	<tr>
		      		<th  class="text-primary text-center" colspan="2"><?= $model->getAttributeLabel('author')?></th>		          
		      	</tr>
		        <tr>
		          <td><strong>Ф.И.О</strong></td>
		          <td><?php printf("%s %s %s",
		          	$model->user->firstname,
		          	$model->user->lastname,
		          	$model->user->secondname
		          )?></td>
		        </tr>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('username')?></strong></td>
		          <td><?= $model->user->username?></td>
		        </tr>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('role')?></strong></td>
		          <td><?= $model->role?></td>
		        </tr>
		        <?php if($model->manager_crm):?>
		      	<tr>
		      		<th  class="text-primary text-center" colspan="2"><?= $model->getAttributeLabel('manager_crm')?></th>		          
		      	</tr>
		        <tr>
		          <td><strong>Ф.И.О</strong></td>
		          <td><?php printf("%s %s %s",
		          	$model->manager->firstname,
		          	$model->manager->lastname,
		          	$model->manager->secondname
		          )?></td>
		        </tr>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('username')?></strong></td>
		          <td><?= $model->manager->username?></td>
		        </tr>
		      <?php endif;?>
		      </tbody>
		    </table>
		  </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card">
		  <div class="card-header" data-background-color="green">
		    <h4 class="title">Данные клиента</h4>
		    <p class="category">Общая сумма заказа <?= number_format($model->price,0,',',' ')?> тг.</p>
		  </div>
		  <div class="card-content table-responsive">
		  	<table class="table table-hover">
		      <tbody>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('name')?></strong></td>
		          <td><?= $model->name?></td>
		        </tr>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('phone')?></strong></td>
		          <td><?= $model->phone?></td>
		        </tr>
		        <?php if($model->email):?>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('email')?></strong></td>
		          <td><?= $model->email?></td>
		        </tr>
		      	<?php endif;?>
		        <?php if($model->address):?>
		        <tr>
		          <td><strong><?= $model->getAttributeLabel('address')?></strong></td>
		          <td><?= $model->address?></td>
		        </tr>
		      	<?php endif;?>
		      </tbody>
		    </table>

		    <table class="table table-hover">
		      <thead class="text-success">
			      <tr>
			        <th>Товар</th>
			        <th>Наименование</th>
			        <th>Цена</th>
			        <th>Кол.</th>
			        <th>Сумма</th>
			      </tr>
		      </thead>
		      <tbody>
		      	<?php foreach ($model->data as $index):?>
		        	<tr>
		        		<td><?= Order::getCategoryName($model->data[0]["position"]);?></td>
				        <td><?= $model->data[0]["title"]?></td>
				        <?php if($model->payment == "В кредит"):?>
				        <td><?= number_format($model->data[0]["old_price"],0,',',' ')?> тг.</td>
				        <td><?= $model->data[0]["count"]?></td>
				        <td><?= number_format($model->data[0]["count"]*$model->data[0]["old_price"],0,',',' ')?> тг.</td>
				        <?php else:?>
				        <td><?= number_format($model->data[0]["price"],0,',',' ')?> тг.</td>
				        <td><?= $model->data[0]["count"]?></td>
				        <td><?= number_format($model->data[0]["count"]*$model->data[0]["price"],0,',',' ')?> тг.</td>
				        <?php endif;?>
		        	</tr>
		      	<?php endforeach;?>
		      </tbody>
		    </table>
		  </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
		  <div class="card-header" data-background-color="purple">
		    <h4 class="title">Комментарии</h4>		    
		  </div>
		  <div class="card-content table-responsive">
				<?php if(is_array($model->comment)):?>
			  <table class="table table-hover">
		      <tbody>
			  	<?php for($i=0;$i<count($model->comment);$i++):?>
			  	<?php
			  	$user = User::findOne($model->comment[$i]["author"]);
			  	?>
	        <tr>
	          <td class="col-md-2"><strong><?= $user->username?></strong></td>
	          <td class="col-md-8"><?= $model->comment[$i]["comment"]?></td>
	          <td class="col-md-2"><?= date ( "d-m-Y H:i",$model->comment[$i]["created_at"])?></td>
	        </tr>
		    <?php endfor;?>
		      </tbody>
			  </table>
				<?php endif;?>
		    <?= Html::beginForm();?>
		  	<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9">
			    	<input type="hidden" name="author" value="<?= yii::$app->user->id?>" />
			    	<div class="form-group">
						  <textarea class="form-control" name="comment" required placeholder="Напишите здесь комментарий"></textarea>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<button type="submit" class="btn btn-primary pull-right" name="comment-button">Оставить комментарий</button>
					</div>
				</div>
		    <?= Html::endForm();?>
		  </div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="cheklist" tabindex="1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Чек-заявка для оформления кредита №<?=$model->id?> от <?= date("d-m-Y",$model->created_at)?></h4>
        </div>
        <div class="modal-body">
					<style type="text/css">
						.checkTable{border: 1px solid #c9c9c9; padding: 0px; margin: 0px;}
						.checkTable td{border: 1px dotted #c9c9c9; height: 20px; padding-left: 5px; margin: 0px; vertical-align: top;}
						.color{background: #c9c9c9; border: 1px dotted black !important;}
						.red{color: red;}
					</style>
					<table cellspacing="0" cellpadding="0" class="checkTable">
						<tbody>
							<tr>
								<td colspan="5"><center><strong>Чек-заявка для оформления кредита №<?=$model->id?> от <?= date("d-m-Y",$model->created_at)?></strong></center></td>
							</tr>
							<tr>
								<td></td>
								<td>Цены действительны до:</td>
								<td class="red"><?= date("d-m-Y",strtotime("+2 day", $model->created_at))?></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td><b>ДБ АО «Банк Хоум Кредит»</b></td>
								<td></td>
								<td></td>
								<td><b>ТОО "OUTDOOR PRODUCTION"</b></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>Шинный центр МАСТЕРШИНА</td>
							</tr>
							<tr>
								<td></td>
								<td>ФИО клиента:</td>
								<td><?= $model->name?></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>Город:</td>
								<td><?= $model->address?></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>Код тт:</td>
								<td>051057</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="color"><b>№</b></td>
								<td class="color" colspan="2"><b>Наименование товара</b></td>
								<td class="color"><b>Кол</b></td>
								<td class="color"><b>Стоимость</b></td>
							</tr>
							<?php foreach($model->data as $item):?>
							<?php 
								$price = $item["count"]*$item["old_price"];
								$total = $total + $price;
							?>
							<tr>
								<td><?= $i?></td>
								<td colspan="2"><?= $item["title"]?></td>
								<td><?= $item["count"]?></td>
								<td><?= $price?></td>
							</tr>
							<?php $i++?>
							<?php endforeach;?>
							<tr>
								<td class="color"></td>
								<td class="color"><b>Итого</b></td>
								<td class="color"></td>
								<td class="color"></td>
								<td class="color"><b><?= $total?></b></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td><b>Памятка для клиента:</b></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>1.</td>
								<td colspan="4">
									Просим Вас обратиться для консультации по условиям оформления потребительского кредита в ближайщий для Вас административный пункт Банка Хоум Кредит с данной чек-заявкой.
								</td>
							</tr>
							<tr>
								<td>2.</td>
								<td colspan="4">
									После одобрения заказа Банком с вами свяжутся менеджера шинного центра для согласования даты и способа передачи товара.
								</td>
							</tr>
							<tr>
								<td>3.</td>
								<td colspan="4">
									Для оформления кредита необходимо иметь при себе удостоверение личности. Для получателей пенсии необходимо дополнительно пенсионное удостоверение.
								</td>
							</tr>
							<tr>
								<td>4.</td>
								<td colspan="4">
									Банк Хоум Кредит не выдает кредит, если человек прописан в другом городе. Например: если вы живете в Алматы, а прописаны в Караганде, то вам нужно обратиться в филиал банка в Караганде.
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="4"><b>Список отделений Банка Хоум Кредит</b></td>
							</tr>
							<tr>
								<td></td>
								<td><b>Торговый центр</b></td>
								<td><b>Торговая точка</b></td>
								<td colspan="2"><b>Адрес</b></td>
							</tr>
							<tr>
								<td></td>
								<td>12 месяцев</td>
								<td>строительный магазин</td>
								<td colspan="2">пр.Райымбека, 512, уг.ул.Саина</td>
							</tr>
							<tr>
								<td></td>
								<td>CARREFOUR</td>
								<td>Галерея сотовых телефонов</td>
								<td colspan="2">ул.Кабдолова, 1/4</td>
							</tr>
							<tr>
								<td></td>
								<td>АРМАДА</td>
								<td>Евро-Мебель, блок 2, ряд 5Д</td>
								<td colspan="2">ул.Кабдолова, 1/8</td>
							</tr>
							<tr>
								<td></td>
								<td>БЦ ЛОТОС</td>
								<td>Chocomart, 3 этаж, вход с торца</td>
								<td colspan="2">уг.ул.Масанчи</td>
							</tr>
							<tr>
								<td></td>
								<td>МАСТЕРГАЗ</td>
								<td>установка газ.об-ния, напротив СТО GT SERVICE</td>
								<td colspan="2">ул.Тлендиева, 258Д</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД BAUMARKT</td>
								<td>строительный магазин</td>
								<td colspan="2">ул.Толе би, 189а, уг.ул.Розыбакиева</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД ГУМ</td>
								<td>сотовые телефоны (эскалатор)</td>
								<td colspan="2">ул.Кунаева, 21, уг.ул.Маметовой</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД ЛЮМИР</td>
								<td>ТОО "Опт Трэйд", отопительные котлы, 1 этаж</td>
								<td colspan="2">пр.Саина, уг.ул.Шаляпина</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД МЕРЕЙ</td>
								<td>мебель (рядом с таможней)</td>
								<td colspan="2">пр.Суюнбая, 2, уг.пр.Райымбека</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД МОБИЛА</td>
								<td>сотовые телефоны слева от входа</td>
								<td colspan="2">ул.Макатаева, 81, уг.ул.Абылай хана</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД САЛАМАТ 3</td>
								<td>53 бутик</td>
								<td colspan="2">ул.Розыбакиева,72, уг.ул.Шевченко</td>
							</tr>
							<tr>
								<td></td>
								<td>ТД СТУДЕНТ</td>
								<td>2 этаж 	АТАКЕНТ</td>
								<td colspan="2">ул.Тимирязева, 42, уг.ул.Манаса</td>
							</tr>
							<tr>
								<td></td>
								<td>ТРК Mega Center Alma-Ata</td>
								<td>м. IPOINT</td>
								<td colspan="2">ул.Розыбакиева, 247а.</td>
							</tr>
							<tr>
								<td></td>
								<td>ТЦ КУНГЕЙ</td>
								<td>мебельный магазин	Алматы-1</td>
								<td colspan="2">ул Молдагалиева, 2</td>
							</tr>
							<tr>
								<td></td>
								<td>Шинный центр</td>
								<td>МАСТЕРШИНА</td>
								<td colspan="2">ул.Садовникова, 99, уг.ул.Сатпаева (АДК)</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td><b>Памятка для кредитного консультанта Банка:</b></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>1.</td>
								<td colspan="4">
									Кредит оформляется в стандартном порядке на основании данных указанных в чек-заявке.
								</td>
							</tr>
							<tr>
								<td>2.</td>
								<td colspan="4">
									Сверить срок действия ценового предложения. В случае истечения срока необходимо написать запрос на актуализацию цен (указав номер заявки).
								</td>
							</tr>
							<tr>
								<td>3.</td>
								<td colspan="4">
									После одобрения заказа необходимо отправить письмо-подтверждение на info@mastershina.kz, в теме указать Одобрен заказ № 0000 Сумма 0000 и приложитьскан. варианты следующих документов:<br/>
									1) подписанный пакет документов для магазина (заявление на получение кредита, памятка, спецификация товара, копия удостоверения личности)<br/>
									2) скан. чек-заявки
								</td>
							</tr>
							<tr>
								<td>4.</td>
								<td colspan="4">
									Дождаться ответа от специалистов партнера «Заказ № 0000 подтвержден»
								</td>
							</tr>
							<tr>
								<td>5.</td>
								<td colspan="4">
									В случае отказа в выдаче кредита сообщить об этом посредством эл. почты, указав в теме письма Отказано в  заказе № 0000
								</td>
							</tr>
							<tr>
								<td>5.</td>
								<td colspan="4">
									После подтверждения об одобрении менеджер шинного центра свяжется с клиентом для согласования даты и способа передачи товара.
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td><b>Контакты шинного центра для актуализации цен и подтверждения заказа:</b></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>e-mail:</td>
								<td><a href="mail:info@mastershina.kz">info@mastershina.kz</a></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>Телефон:</td>
								<td>+7 (727) 390 64 04, +7 777 7 157 157</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="5"><button class="btn btn-primary pull-right send">Переслать письмо</button></td>
							</tr>
						</tbody>
					</table>
        </div>
      </div>
    </div>
</div>