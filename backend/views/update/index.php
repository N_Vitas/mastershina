<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = 'Импорт товаров';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="card card-stats">
    <div class="card-header" data-background-color="blue">
        <i class="material-icons">cloud_upload</i>
    </div>
    <div class="card-content">
        <? if ($filemtime): ?>
            <p class="category">Файл обновления обнаружен</p>
            <h4 class="title">
                Вы можете закачать новый файл обновления или использовать обнаруженный файл import.xml закаченный <?= date('d-m-Y H:i',$filemtime) ?>
            </h4>
        <? else: ?>
            <p class="category">Файл импорта не обнаружен</p>
            <h4 class="title">
                Для обновления товаров на сайте необходимо закачать на сервер файл импорта.
            </h4>
        <? endif; ?>
    </div>
    <div class="card-footer">
        <div class="stats">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                <div class="btn btn-info btn-file">
                    <?= $form->field($model, 'xmlFile')->fileInput() ?>
                </div>
                <?= Html::submitButton('Отправить файл на сервер', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end() ?>
        </div>
        <div class="pull-right">
            <?= Html::beginForm(['/update'], 'post', ['enctype' => 'application/json']) ?>
            <?= Html::hiddenInput('noFile', true) ?>
            <?= Html::submitButton('Открыть файл импорта', ['class' => 'btn btn-primary', 'disabled' => $filemtime == 0]) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>