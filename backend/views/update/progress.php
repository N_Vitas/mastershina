<?php
use yii\helpers\Html;
$this->title = 'Импорт товаров';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="row" ng-controller="Update" ng-init="xml.init('<?= $model->xmlFile->name ?>')">
  <div class="col-lg-4 col-md-4 col-lg-offset-8  col-md-offset-8 col-xs-12">
		<div class="navbar-form navbar-right" role="search">
			<div class="form-group is-empty">            
				<input type="text" ng-model="xml.search" class="form-control" placeholder="Поиск">
				<span class="material-input"></span>
			<span class="material-input"></span><span class="material-input"></span></div>
			<button type="submit" ng-click="xml.clear()" class="btn btn-white btn-round btn-just-icon">
				<i class="material-icons">clear</i><div class="ripple-container"></div>
			</button>
		</div>
	</div>
  <!-- Панель управления -->
  <div class="col-md-12 col-lg-12 col-xs-12">
    <div class="card card-nav-tabs">
      <div class="card-header" data-background-color="purple">
        <div class="nav-tabs-navigation">
          <div class="nav-tabs-wrapper">
            <span class="nav-tabs-title">Предварительный просмотр:</span>
            <ul class="nav nav-tabs" data-tabs="tabs">
              <li class="active" ng-click="xml.category('shina')">
                <a href="#shina" data-toggle="tab" aria-expanded="true">
                  <i class="material-icons icons-shiny">shiny</i>
                  Шины
                <div class="ripple-container"></div></a>
              </li>
              <li class="" ng-click="xml.category('disk')">
                <a href="#disk" data-toggle="tab" aria-expanded="false">
                  <i class="material-icons icons-disc">disc</i>
                  Диски
                <div class="ripple-container"></div></a>
              </li>
              <li class="" ng-click="xml.category('maslo')">
                <a href="#maslo" data-toggle="tab" aria-expanded="false">
                  <i class="material-icons icons-oil">oil</i>
                  Масла
                <div class="ripple-container"></div></a>
              </li>
              <li class="" ng-click="xml.category('akum')">
                <a href="#akum" data-toggle="tab" aria-expanded="false">
                  <i class="material- icons-battery">battery</i>
                  Аккуьуляторы
                <div class="ripple-container"></div></a>
              </li>
              <li class="pull-right">
                <button class="btn btn-success" ng-if="xml.progress == 0" ng-click="xml.alert()">
                  <i class="material-icons">cloud_upload</i>
                  Импорт на сайт
                  <div class="ripple-container"></div>
                </button>
                <div class="progress-border" ng-if="xml.progress > 0">
                  <div class="progress-bar" ng-style="{ 'width': xml.getProgress()}"></div>
                  <div class="progress-title">{{xml.progress}}%</div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="card-content">
        <div class="tab-content overdraft">
          <div class="tab-pane active" id="shina">
            <table class="table" ng-if="xml.shina.length > 0">
              <tbody>
                <tr ng-repeat="pr in xml.shina | filter: xml.search | limitTo : xml.limit : xml.shinapage">
                  <td>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" ng-checked="pr.send" ng-click="xml.togleProduct(pr)" class="checkbox"/>
                    </label>
                  </div>
                  </td>
                  <td><img class="picture" src="{{pr.picture}}" /></td>
                  <td>{{pr.model}}</td>
                  <td>Старая цена {{pr['СтараяЦена']}}</td>
                  <td>Новая цена {{pr['Цена']}}</td>
                  <td class="td-actions text-right">
                    <button type="button" rel="tooltip" ng-click="images.init(pr)" title="" class="btn btn-success btn-simple btn-xs" data-original-title="Remove">
                      <i class="material-icons">edit</i>
                    </button>
                    <button type="button" rel="tooltip" ng-click="xml.deleteProduct(pr)" title="" class="btn btn-danger btn-simple btn-xs" data-original-title="Remove">
                      <i class="material-icons">close</i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="disk">
            <table class="table" ng-if="xml.disk.length > 0">
              <tbody>
                <tr ng-repeat="pr in xml.disk | filter: xml.search | limitTo : xml.limit : xml.diskpage">
                  <td>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" ng-checked="pr.send" ng-click="xml.togleProduct(pr)" class="checkbox"/>
                    </label>
                  </div>
                  </td>
                  <td><img class="picture" src="{{pr.picture}}" /></td>
                  <td>{{pr.model}}</td>
                  <td>Старая цена {{pr['СтараяЦена']}}</td>
                  <td>Новая цена {{pr['Цена']}}</td>
                  <td class="td-actions text-right">
                    <button type="button" rel="tooltip" ng-click="xml.deleteProduct(pr)" title="" class="btn btn-danger btn-simple btn-xs" data-original-title="Remove">
                      <i class="material-icons">close</i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="maslo">
            <table class="table" ng-if="xml.maslo.length > 0">
              <tbody>
                <tr ng-repeat="pr in xml.maslo | filter: xml.search | limitTo : xml.limit : xml.maslopage">
                  <td>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" ng-checked="pr.send" ng-click="xml.togleProduct(pr)" class="checkbox"/>
                    </label>
                  </div>
                  </td>
                  <td><img class="picture" src="{{pr.picture}}" /></td>
                  <td>{{pr.model}}</td>
                  <td>Старая цена {{pr['СтараяЦена']}}</td>
                  <td>Новая цена {{pr['Цена']}}</td>
                  <td class="td-actions text-right">
                    <button type="button" rel="tooltip" ng-click="xml.deleteProduct(pr)" title="" class="btn btn-danger btn-simple btn-xs" data-original-title="Remove">
                      <i class="material-icons">close</i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="akum">
            <table class="table" ng-if="xml.akum.length > 0">
              <tbody>
                <tr ng-repeat="pr in xml.akum | filter: xml.search | limitTo : xml.limit : xml.akumpage">
                  <td>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" ng-checked="pr.send" ng-click="xml.togleProduct(pr)" class="checkbox"/>
                    </label>
                  </div>
                  </td>
                  <td><img class="picture" src="{{pr.picture}}" /></td>
                  <td>{{pr.model}}</td>
                  <td>Старая цена {{pr['СтараяЦена']}}</td>
                  <td>Новая цена {{pr['Цена']}}</td>
                  <td class="td-actions text-right">
                    <button type="button" rel="tooltip" ng-click="xml.deleteProduct(pr)" title="" class="btn btn-danger btn-simple btn-xs" data-original-title="Remove">
                      <i class="material-icons">close</i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="stats">
          <div class="card-content">
            <p class="category" ng-bind="xml.navigation()"></p>
            <button class="btn btn-primary" ng-disabled="xml[obj + 'page'] == 0" ng-click="xml.back()"><i class="material-icons">keyboard_arrow_left</i></button>
            <button class="btn btn-primary" ng-disabled="(xml[obj + 'page'] + xml.limit) >= xml[obj].length" ng-click="xml.next()"><i class="material-icons">keyboard_arrow_right</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="updateModalLabel">Предупреждение</h4>
          </div>
          <div class="modal-body">
            <div class="well well-small">
              <div class="card card-stats">
                  <div class="card-header" data-background-color="red">
                  <i class="material-icons">info_outline</i>
                  </div>
                  <div class="card-content">
                  <p class="category">Внимание</p>
                  <hr/>
                  <h4 class="title text-center">Будет загружен большой обьем данных. Не закрывайте эту страницу до завершения импорта.</h4>
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-primary" ng-click="xml.upload()" data-dismiss="modal">Продолжить</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="editModalLabel">Загрузка изображений</h4>
          </div>
          <div class="modal-body">
            <div class="well well-small">
              <div class="card card-stats">
                  <div class="card-header" data-background-color="green">
                  <i class="material-icons">images</i>
                  </div>
                  <div class="card-content">
                    <form>
                      <input type="file" class="input-file" onchange="angular.element(this).scope().images.change(this)" ng-file-select="onFileSelect($files)" ng-model="images.files">
                      <label class="input-label" ng-if="!images.files.data">Выберите файл</label>
                    </form>                    
                    <img ng-if="images.files.data" ng-src="{{images.files.data}}" />
                  </div>

              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-primary" ng-click="images.upload()" data-dismiss="modal">Загрузить</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>