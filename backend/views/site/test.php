<?php
use yii\helpers\Html;
?>
<div class="row" ng-controller="Test">
  <!-- Панель управления -->
  <div class="col-md-12 col-lg-12 col-xs-12">
    <div class="card">
      <div class="card-content">
        <div class="btn-group">
          <button ng-repeat="box in model.types" 
            class="btn btn-info btn-xs" 
            type="button" 
            ng-click="setType(box)" 
            ng-bind="model[box].title">            
          </button>
        </div>
        <div class="btn-group pull-right">
          <button type="button" 
            ng-class="model.style.create" 
            ng-click="openFormCreate()" 
            class="btn btn-xs"
            rel="tooltip" data-original-title="Создать запись на выбраное время">
              <i class="material-icons">note_add</i>
          </button>
          <button type="button" 
            ng-class="model.style.change" 
            class="btn btn-xs"
            ng-click="changeBox()"
            rel="tooltip" data-original-title="Сменить бокс">
              <i class="material-icons">cached</i>
          </button>
          <button type="button" 
            ng-class="model.style.update" 
            ng-click="updateConfirm()"
            class="btn btn-xs"
            rel="tooltip" data-original-title="Продлить запись на выбраное время">
              <i class="material-icons">content_copy</i>
          </button>
          <button type="button" 
            ng-class="model.style.delete"
            ng-click="deleteConfirm()"
            class="btn btn-xs"
            rel="tooltip" data-original-title="Удалить запись на выбраное время">
              <i class="material-icons">delete</i>
          </button>
        </div>
        <div class="btn-group pull-right">
          <button type="button" 
            ng-class="model.style.preview" 
            ng-click="prevPage()" 
            class="btn btn-xs"
            rel="tooltip" data-original-title="Предыдущая неделя">
              <i class="material-icons">keyboard_arrow_left</i>
          </button>
          <button type="button" 
            ng-class="model.style.next" 
            ng-click="nextPage()" 
            class="btn btn-xs"
            rel="tooltip" data-original-title="Следующая неделя">
              <i class="material-icons">keyboard_arrow_right</i>
          </button>
          <!-- <button type="button" class="btn btn-info btn-xs"><i class="material-icons">today</i></button> -->
        </div>
      </div>
    </div>

    <div class="alert alert-warning hidden">
      <div class="container-fluid">
      <div class="alert-icon">
      <i class="material-icons">warning</i>
      </div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
      </button>
      <span class="warning-text"></span>
      </div>
    </div>
  </div>
  <!-- Конец панели -->
  <!-- Таблица времени -->  
  <div class="col-md-12 col-lg-12 col-xs-12">
    <div class="card">
      <div class="card-content" ng-init="getList()">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th></th>
              <th class="selectable" ng-bind="getDayTitle('mon',model[model.type]['mon'][0])"></th>
              <th class="selectable" ng-bind="getDayTitle('tue',model[model.type]['tue'][0])"></th>
              <th class="selectable" ng-bind="getDayTitle('wed',model[model.type]['wed'][0])"></th>
              <th class="selectable" ng-bind="getDayTitle('thu',model[model.type]['thu'][0])"></th>
              <th class="selectable" ng-bind="getDayTitle('fri',model[model.type]['fri'][0])"></th>
              <th class="selectable" ng-bind="getDayTitle('sat',model[model.type]['sat'][0])"></th>
              <th class="selectable" ng-bind="getDayTitle('sun',model[model.type]['sun'][0])"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable">
                    <li class="text-center ui-widget-content datetime" ng-repeat="item in model[model.type]['mon']">
                      <i class="material-icons">schedule</i> 
                      <span ng-bind="formatDate(item.time)"></span>
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['mon']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'mon',model.type)" ng-repeat="item in model[model.type]['mon']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['tue']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'tue',model.type)" ng-repeat="item in model[model.type]['tue']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['wed']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'wed',model.type)" ng-repeat="item in model[model.type]['wed']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['thu']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'thu',model.type)" ng-repeat="item in model[model.type]['thu']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['fri']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'fri',model.type)" ng-repeat="item in model[model.type]['fri']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['sat']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'sat',model.type)" ng-repeat="item in model[model.type]['sat']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
              <td ng-mousedown="clearSelected()" class="col-md-1 col-lg-1 col-xs-1">                
                <ul class="selectable"
                    selectable
                    selectable-list="model[model.type]['sun']"
                    selectable-out="selected"
                    selectable-events="{start:'selectionStart()', stop:'selectionStop($selected)'}">
                    <li class="ui-widget-content" ng-click="openView(item)" ng-class="wronckdate(item,'sun',model.type)" ng-repeat="item in model[model.type]['sun']">
                      <i class="material-icons">store</i> 
                      <!-- <span ng-bind="item.time"></span> -->
                      <!-- <span class="pull-right" ng-bind="item.name"></span> -->
                    </li>
                </ul>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- Конец таблицы времени -->
  <!-- Форма создания записи -->
  <div class="modal fade bs-example-modal-lg" id="serviceCreate">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title modal-title-primary">Запись на шиномонтаж</h4>
        </div>
        <div class="modal-body" ng-click="cr.clearClient()">
          <div class="card card-stats">
            <div class="card-header" data-background-color="green">
              <i class="material-icons">store</i>
            </div>
            <div class="card-content">
              <p class="category"><span ng-bind="model[model.type].title"></span></p>
              <p class="category">Номер авто <strong ng-bind="sendData.inumber"></strong></p>
              <p class="category">Модель авто <strong ng-bind="sendData.model"></strong></p>
              <p class="category">Размер шин <strong ng-bind="sendData.size"></strong></p>
              <div class="clearfix"></div>

              <div class="alert alert-danger" ng-show="cr.errors.validate.length > 0">
                <div class="container-fluid text-left">
                  <span ng-repeat="err in cr.errors.validate" ng-bind="err"></span>
                </div>
              </div>
              <form class="tirefitForm" id="tirefitForm" name="tirefitForm">
                <input type="hidden" name="author" value="<?= yii::$app->user->identity->id_crm?>"/>
                <input type="hidden" name="meneger_crm" value="<?= yii::$app->user->identity->id_crm?>"/>
                <input type="hidden" name="confirmed" value="Нет"/>
                <input type="hidden" name="status" value="{{cr.getStatus()}}"/>
                <input type="hidden" name="storage" value="{{cr.getStorage()}}"/>
                <input type="hidden" name="comment" value="{{sendData.comment}}"/>
                <input type="hidden" name="inumber" value="{{sendData.inumber}}"/>
                <input type="hidden" name="model" value="{{sendData.model}}"/>
                <input type="hidden" name="size" value="{{sendData.size}}"/>      
                <div ng-init="cr.loadClient()" class="form-group dropdown {{cr.fClients.name.length > 0 ? 'open':''}} {{cr.errors.name ? '' : 'has-error'}}"> <!-- ng-hide="cr.client" -->
                  <label class="control-label">Имя, Фамилия? <span ng-hide="cr.errors.name" class="block-error">Должно быть минимум 2 символов</span></label>
                  <input autocomplete="off" type="text" name="name" ng-change="cr.changename()" ng-model="sendData.name" class="form-control" autofocus="" placeholder="Имя Фамилия клиента."/>
                  <ul class="dropdown-menu">
                    <li ng-repeat="p in cr.fClients.name"><a href="javascript:void(null)" ng-click="cr.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
                  </ul>
                </div>    
                        
                <div class="form-group dropdown {{cr.fClients.phone.length > 0 ? 'open':''}} {{cr.errors.phone ? '' : 'has-error'}}"> <!-- ng-hide="cr.client" -->
                  <label class="control-label">Телефон <span ng-hide="cr.errors.phone" class="block-error">не соответствует формату телефона</span></label>
                  <input autocomplete="off" type="text" name="phone" id="phone" ng-change="cr.changephone()" ng-model="sendData.phone" class="form-control" placeholder="Например +77273170077"/> 
                  <ul class="dropdown-menu">
                    <li ng-repeat="p in cr.fClients.phone"><a href="javascript:void(null)" ng-click="cr.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
                  </ul>
                </div>
                <div class="form-group {{cr.errors.address?'':'has-error'}}"> <!-- ng-hide="cr.client" -->
                  <label class="control-label">Адрес для шиномонтажа <span ng-hide="cr.errors.address" class="block-error">минимум 6 символа</span></label>
                  <textarea class="form-control" ng-change="cr.changeaddress()" name="address" ng-model="sendData.address" placeholder="Например г.Алматы, ул. Садовникова, 99 (Лазарева)"></textarea>
                </div>
              </form>
              <div class="form-group" ng-show="cr.auto.length > 0">
                <label for="client">Выбор автомобиля</label>
                <select class="form-control" ng-change="cr.modelSelect()" ng-options="option.model for option in cr.auto" ng-model="cr.selectedAuto"></select>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group has-error">
                    <input autocomplete="off" ng-model="cr.n_inumber" type="text" class="form-control" placeholder="Гос. номер"/>
                    <label class="control-label"><span ng-show="cr.n_inumber_err" class="block-error text-xs">Должно быть минимум 5 символов</span></label>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group has-error">
                    <input autocomplete="off" ng-model="cr.n_model" type="text" class="form-control" placeholder="Модель авто"/>
                    <label class="control-label"><span ng-show="cr.n_model_err" class="block-error text-xs">Должно быть минимум 3 символов</span></label>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group has-error">
                    <input autocomplete="off" ng-model="cr.n_size" type="text" class="form-control" placeholder="Размеры шин"/>
                    <label class="control-label"><span ng-show="cr.n_size_err" class="block-error text-xs">Должно быть минимум 5 символов</span></label>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <button class="btn btn-primary btn-xs" ng-click="cr.addAuto()">Добавить новое авто</button>
                  </div>                                
                </div>
              </div>
              <div class="row text-left">
                <div class="col-md-3">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" ng-model="sendData.storage" class="checkbox"/>
                      <span class="label label-primary">Шины на хранении</span>
                    </label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" name="comment" ng-model="sendData.comment" placeholder="Комментарий"></textarea>
                  </div>
                </div>
                <div class="col-md-3">
                  <button class="btn btn-primary btn-block" ng-click="createTir()">Записать</button>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="stats pull-right">
                <i class="material-icons">person</i> <span><?= yii::$app->user->identity->firstname?> <?= yii::$app->user->identity->lastname?> <?= yii::$app->user->identity->secondname?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <!-- Конец формы создания -->
  <!-- Форма подтверждения удаления -->
  <div class="modal fade" id="serviceDelete">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Освобождение перриода времени</h4>
        </div>
        <div class="modal-body">
          <p>Вы действительно хотите освободить запись на шиномонтаж?</p>
        </div>
        <div class="modal-footer">            
            <button type="button" ng-click="deleteTir()" class="btn btn-info btn-xs" data-dismiss="modal">Освободить</button>
            <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Отмена</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Конец формы удаления записей -->
  <!-- Форма подтверждения продления времени -->
  <div class="modal fade" id="serviceUpdate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Продление перриода времени</h4>
        </div>
        <div class="modal-body">
          <p>Вам небходимо выбрать одного клиента на которого будет продленно время</p>
          <div class="form-group" ng-show="updateMore.length > 0">
            <select class="form-control" ng-options="option.name for option in updateMore" ng-model="selectUpdateClient"></select>
          </div>
        </div>
        <div class="modal-footer">            
            <button type="button" ng-click="updateTir()" class="btn btn-info btn-xs" data-dismiss="modal">Продолжить</button>
            <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Отмена</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Конец формы продления времени -->
  <!-- Детальная информация по записи -->
  <div class="modal fade bs-example-modal-lg" id="serviceViev">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title modal-title-primary">Запись на шиномонтаж</h4>
        </div>
        <div class="modal-body">
          <div class="card card-stats">
            <div class="card-header" data-background-color="green">
              <i class="material-icons">store</i>
            </div>
            <div class="card-content">
              <p class="category"><span ng-bind="cr.view.address"></span></p>
              <p class="category"><span ng-bind="cr.view.box"></span></p>
              <p class="category"><span ng-bind="cr.view.status"></span></p>

              <table class="table table-hover" style="text-align: left;">
                <tr>
                  <td class="clearfix">Клиент</td>
                  <td><span ng-bind="cr.view.name"></span></td>
                </tr>
                <tr>
                  <td class="clearfix">Контакты</td>
                  <td><span ng-bind="cr.view.phone"></span></td>
                </tr>
                <tr>
                  <td class="clearfix">Подтвержден</td>
                  <td><span ng-bind="cr.view.confirmed"></span></td>
                </tr>
                <tr>
                  <td class="clearfix">Шины на хранении</td>
                  <td><span ng-bind="cr.view.storage"></span></td>
                </tr>
                <tr>
                  <td class="clearfix">Автомобиль</td>
                  <td>Марка <strong ng-bind="cr.view.model"></strong>  Гос. номер <strong ng-bind="cr.view.inumber"></strong></td>
                </tr>
                <tr>
                  <td class="clearfix">Размер шин</td>
                  <td><span ng-bind="cr.view.size"></span></td>
                </tr>
                <tr>
                  <td class="clearfix">Комментарии</td>
                  <td><span ng-bind="cr.view.comment"></span></td>
                </tr>
              </table>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">date_range</i> <span ng-bind="cr.view.time"></span>
              </div>
              <div class="stats pull-right">
                <i class="material-icons">person</i> <span ng-bind="cr.view.author"></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Конец детальнай информации по записи -->
  <!-- Форма выбора бокса -->
  <div class="modal fade" id="serviceChange">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Изменение бокса в записи</h4>
        </div>
        <div class="modal-body">
          <p>Вам небходимо выбрать один бокс на которого будет изменена запись</p>
          <div class="form-group" ng-show="changeList.length > 0">
            <select class="form-control" ng-options="option.title for option in changeList" ng-model="selectChangeList"></select>
          </div>
        </div>
        <div class="modal-footer">            
            <button type="button" ng-click="changeTir()" class="btn btn-info btn-xs" data-dismiss="modal">Продолжить</button>
            <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Отмена</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Конец формы выбора бокса -->
</div>