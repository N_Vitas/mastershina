<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Новый пользователь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" ng-controller="orderList">
  <nav class="navbar navbar-primary card-header" data-background-color="purple" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><?= Html::encode($this->title) ?></a>
      </div>
    </div>    
  </nav>
  <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
  <div class="card-content table-responsive">
    <div class="row">
      <div class="col-lg-4">
          <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
          <?= $form->field($model, 'email') ?>
          <?= $form->field($model, 'password')->passwordInput() ?>
          <?= $form->field($model, 'id_crm') ?>
      </div>
      <div class="col-lg-4">
          <?= $form->field($model, 'firstname') ?>
          <?= $form->field($model, 'lastname') ?>
          <?= $form->field($model, 'secondname') ?>
          <div class="form-group">
            <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
          </div>        
      </div>
      <div class="col-lg-4">
      </div>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>