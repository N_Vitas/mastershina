<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<body ng-app="cabinetApp" ng-controller="AppController">
  <div class="loader"><div><i class='fa fa-spinner fa-spin'></i></div></div>
  <!-- Modal Core -->
  <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Модальное окно</h4>
        </div>
        <div class="modal-body">
          <div class="well well-small">
            <div class="btn-toolbar" role="toolbar">
              <div class="btn-group">
                <button ng-click="createorder.setCategory('shiny')" class="btn btn-xs {{createorder.isActive('shiny')}}">Шины</button>
              </div>
              <div class="btn-group">
                <button ng-click="createorder.setCategory('disc')" class="btn btn-xs {{createorder.isActive('disc')}}">Диски</button>
              </div>
              <div class="btn-group">
                <button ng-click="createorder.setCategory('oil')" class="btn btn-xs {{createorder.isActive('oil')}}">Масла</button>
              </div>
              <div class="btn-group">
                <button ng-click="createorder.setCategory('battery')" class="btn btn-xs {{createorder.isActive('battery')}}">Аккумуляторы</button>
              </div>
              <div class="btn-group pull-right">
                <button ng-click="createorder.querySearch()" class="btn btn-primary btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                </button>
              </div>
              <div class="btn-group pull-right">
                <input type="text" ng-model="createorder.search" ng-change="createorder.querySearch()" class="form-control" placeholder="Поиск">
              </div>
            </div>
            <div class="clear"></div>
            <?= backend\widgets\FilterCreateShiny::widget()?>
            <div class="clear"></div>
            <?= backend\widgets\FilterCreateDisc::widget()?>
            <div class="clear"></div>
            <?= backend\widgets\FilterCreateOil::widget()?>
            <div class="clear"></div>
            <?= backend\widgets\FilterCreateBattery::widget()?>
            <div class="clear"></div>
          </div>
          <!-- Конец фильтра -->
          <table class="table table-hover filter">
            <thead class="text-warning">
              <tr>
                <th>Наименование</th>
                <th>Кол.</th>
                <th>Цена со скидкой</th>                
                <th>Цена в кредит</th>               
                <th>Склад</th>
                <th></th>
              </tr>
            </thead>
            <tbody ng-show="createorder.positions.length>0" ng-repeat="item in createorder.positions">
              <tr>
                <td ng-bind="item.model"></td>
                <td ng-bind="item.ostatok"></td>
                <td ng-bind="item.price"></td>
                <td ng-bind="item.old_price"></td>
                <td ng-bind="item.sklad"></td>
                <?php if(\Yii::$app->controller->action->id == 'update'):?>
                <td><button class="btn btn-primary btn-sm btn-just-icon" rel="tooltip" title="Добавить товар" ng-click="basket.addCart(item.id,createorder.category)"><i class="material-icons">note_add</i></button></td>
                <?php else : ?>
                <td><button class="btn btn-primary btn-sm btn-just-icon" rel="tooltip" title="Добавить товар" ng-click="basket.putCart(item.id,createorder.category)"><i class="material-icons">note_add</i></button></td>
                <?php endif; ?>
              </tr>
            </tbody>
          </table>
          <div id="pagination" ng-show="createorder.pages > 1" class="row">
            <button class="pull-left btn btn-primary btn-sm" ng-disabled="createorder.firstPage()" ng-click="createorder.pageBack()">Назад</button>
            <span>{{createorder.page}} из {{createorder.pages}}</span>
            <button class="pull-right  btn btn-primary btn-sm" ng-disabled="createorder.lastPage()" ng-click="createorder.pageForward()">Вперед</button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info btn-simple" data-dismiss="modal">Закрыть</button>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="/img/sidebar-1.jpg">
      <!-- data-color="purple" blue | green | orange | red"-->
      <div class="logo">
        <a href="/" class="simple-text"><?= Yii::$app->name?></a>
      </div>
      <div class="sidebar-wrapper">
        <?php
          echo Nav::widget([
            'items' => [
               [
                 'label' => '<i class="material-icons">dashboard</i> <p>Доска управления</p>',
                 'url' => ['/site/index'],
               ],
               [
                 'label' => '<i class="material-icons">library_books</i> <p>Заявки</p>',
                 'url' => ['/order/index'],
               ],
               [
                 'label' => '<i class="material-icons">content_paste</i> <p>Оформление заказа</p>',
                 'url' => ['/order/create'],
               ],
               [
                 'label' => '<i class="material-icons">build</i> <p>Запись на шиномонтаж</p>',
                 'url' => ['/services/index'],
               ],
               [
                 'label' => '<i class="material-icons">cloud_upload</i> <p>Импорт товаров</p>',
                 'url' => ['/update/index'],
               ],
               [
                 'label' => '<i class="material-icons">people</i> <p>Клиенты</p>',
                 'url' => ['/client/index'],
               ],
               [
                 'label' => '<i class="material-icons icons-shiny">shiny</i> <p>Шины</p>',
                 'url' => ['/shiny/index'],
               ],
               [
                 'label' => '<i class="material-icons icons-disc">disc</i> <p>Диски</p>',
                 'url' => ['/disc/index'],
               ],
               [
                 'label' => '<i class="material-icons icons-oil">oil</i> <p>Масла</p>',
                 'url' => ['/oil/index'],
               ],
               [
                 'label' => '<i class="material-icons icons-battery">battery</i> <p>Аккумуляторы</p>',
                 'url' => ['/battery/index'],
               ],
               // [
               //   'label' => '<i class="material-icons">library_books</i> <p>Typography</p>',
               //   'url' => ['#'],
               // ],
               // [
               //   'label' => '<i class="material-icons">bubble_chart</i> <p>Icons</p>',
               //   'url' => ['#'],
               // ],
               // [
               //   'label' => '<i class="material-icons">location_on</i> <p>Maps</p>',
               //   'url' => ['#'],
               // ],
               // [
               //   'label' => '<i class="material-icons">notifications</i> <p>Notifications</p>',
               //   'url' => ['#'],
               // ],
               [
                   'label' => '<i class="material-icons">unarchive</i> <p>Выход</p>',
                   'url' => ['site/logout'],
               ],
            ],
            'options' => ['class' =>'nav'], // set this to nav-tab to get tab-styled navigation
            'encodeLabels' => false
          ]);
        ?>
    </div>
  </div>

     <div class="main-panel">
      <!-- <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Кабинет</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="/site/index" class="dropdown-toggle">
                  <i class="material-icons">dashboard</i>
                  <p class="hidden-lg hidden-md">Dashboard</p>
                </a>
              </li>
              <?= \backend\widgets\Basket::widget()?>
              <li>
                <a href="/site/logout" class="dropdown-toggle">
                   <i class="material-icons">person</i>
                   <p class="hidden-lg hidden-md">Profile</p>
                </a>
              </li>
            </ul>

            <form class="navbar-form navbar-right" role="search">
              <div class="form-group is-empty">
                <input type="text" class="form-control" placeholder="Search">
                <span class="material-input"></span>
              </div>
              <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i><div class="ripple-container"></div>
              </button>
            </form>
          </div>
        </div>
      </nav> -->

      <div class="content">
        <div class="container-fluid">
        <?php /*yii\widgets\Breadcrumbs::widget([
          'homeLink'      =>  [
            'label'     =>  Yii::t('yii', 'Главная'),
            'url'       =>  ['/'],
            // 'class'     =>  'home',
            'template'  =>  '<span class="glyphicon glyphicon-home"></span> {link}',
          ],
          'links' => $this->params['breadcrumbs'],
          'itemTemplate'  =>  ' / {link}',
          // 'tag'           =>  'ul',
        ])*/?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
      </div>

      <footer class="footer">
        <div class="container-fluid">
          <nav class="pull-left">
            <ul>
              <li>
                <a href="#">
                  Home
                </a>
              </li>
              <li>
                <a href="#">
                  Company
                </a>
              </li>
              <li>
                <a href="#">
                  Portfolio
                </a>
              </li>
              <li>
                <a href="#">
                   Blog
                </a>
              </li>
            </ul>
          </nav>
          <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
          </p>
        </div>
      </footer>
    </div>
  </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
