<?php 
use yii\helpers\Html;
use common\models\Order;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = ['label' => $this->title];
$list = $searchModel->list == 'true' ? 'true': 'false';
$closed = $searchModel->closed == 'true' ? 'true': 'false';
$this->registerJs("
  $('.grid-view').height($(document).height()-290);
");
//   var g = $("#w0");
//   g.append(\'<input type="hidden" id="searchorder-list" value="false" name="SearchOrder[list]"/>\');
// ');
  // jQuery('#w0').yiiGridView({"filterUrl":"/","filterSelector":"#w0-filters input, #w0-filters select"});
?>
<div class="card" ng-controller="orderList">
  <nav class="navbar navbar-primary card-header" data-background-color="purple" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?= $this->title?></a>
    </div>
  </div>    
  </nav>
  <div class="card-content table-responsive">
    <?= yii\grid\GridView::widget([
      'dataProvider' => $dataProvider,
      // 'filterModel' => $searchModel,
      'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
      /*Очередь столбцов id id_crm дата создания  имя телефон статусы (заявка доставка оплата) товар сумма комментарий тема email адрес автор ответственный
	
            [['name', 'phone', 'address', 'manager', 'data'], 'required'],
            [['email'], 'unique'],
            [['created_at', 'updated_at', 'status','bin','iin','export'], 'integer'],
            [['data','phone'], 'string'],
            [['name', 'email',  'comment', 'manager','type'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 25],

      */
      'columns' => [
        'id',
        'name',
        'phone',
        'address',
        // 'manager',
        [
        	'attribute' => 'data',
        	'format' => 'raw',
        	'value' => function($model){
        		$data = json_decode($model->data,true);
        		$html = '';
        		$orders = explode(",",$data['order_id']);
        		foreach ($orders as $value) {
        			if($value == 0)
        				continue;
        			$html .= " <a href='/order/view?id=".$value."'><strong>".$value."</strong></a>";
        		}
        		return $html;
        	}
        ],
          // [
          //   'class' => 'yii\grid\ActionColumn',
          //   'template' => '{view}{update}'
          // ],
          // [
          //   'attribute' => 'created_at',
          //   'format' => 'raw',
          //   'value' => 'created_at',
          //   'filter' => '<input type="text" value="2012-05-15 21:05" id="datetimepicker" data-date-format="yyyy-mm-dd hh:ii">',
          // ],
      ],
      'layout' => "<div class='clearfix'></div>{items}<div class='clearfix'></div>{pager}<span class='pull-right'>{summary}</span>",
      'summary' => "Показано {count} из {totalCount}",
      'emptyText' => 'Ничего не найдено',
    ]); ?>
  </div>
</div>