<?php

/* @var $this yii\web\View */
use backend\widgets\Cards;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = 'Каталог дисков';
$this->params['breadcrumbs'][] = ['label' => $this->title];

// $filter = \frontend\widgets\SearchFormShiny::widget(['model'=>$searchModel,'vertical'=>true]);
?>
<?php $form = ActiveForm::begin(['method' => 'GET']); ?>
<div class="row">
	<div class="col-lg-4 col-md-4 col-lg-offset-8  col-md-offset-8 col-xs-12">
				<div class="navbar-form navbar-right" role="search">
					<div class="form-group  is-empty">            
						<input type="text" name="SearchDisc[code]" value="<?= $searchModel->code?>" class="form-control" placeholder="Поиск">
						<span class="material-input"></span>
					<span class="material-input"></span></div>
					<button type="submit" class="btn btn-white btn-round btn-just-icon">
						<i class="material-icons">search</i><div class="ripple-container"></div>
					</button>
				</div>
	</div>
	<div class="col-lg-12">
		<div class="card card-nav-tabs">
		  <div class="card-header" data-background-color="purple">
		      	<h4 class="title"><?= $this->title?>
	            <a class="pull-right" href="#" onclick="return false" ng-click="toggle()">
	              <i class="material-icons">filter_list</i>
	              <span class="hidden-xs" ng-bind="show_filter ? 'Срыть фильтр' : 'Показать фильтр' "></span>
	            	<div class="ripple-container"></div>
	           	</a>
		      	</h4>
		  </div>
		  <div class="card-content"> 
		    <div class="tab-content">
		      <div class="tab-pane active" id="shiny"> 
		      	<div ng-show="show_filter">
	      			<div class="row">
	      				<div class="col-md-4">
              		<?= $form->field($searchModel, 'size')->dropDownList($searchModel->getSize(),['onchange'=>'this.form.submit()']) ?>		
	      				</div>
	      				<div class="col-md-4">
	                <?= $form->field($searchModel, 'borer')->dropDownList($searchModel->getBorer(),['onchange'=>'this.form.submit()']) ?>
	      				</div>
	      				<div class="col-md-4">
	                <?= $form->field($searchModel, 'radius')->dropDownList($searchModel->getRadius(),['onchange'=>'this.form.submit()']) ?>
	      				</div>
	      			</div>
	      			<div class="row">
	      				<div class="col-md-3">
	                <?= $form->field($searchModel, 'stupica')->dropDownList($searchModel->getStupica(),['onchange'=>'this.form.submit()']) ?>
	      				</div>
	      				<div class="col-md-3">
              		<?= $form->field($searchModel, 'brend')->dropDownList($searchModel->getBrends(),['onchange'=>'this.form.submit()']) ?>	
	      				</div>
	      				<div class="col-md-3">	      					
	                <?= $form->field($searchModel, 'color')->dropDownList($searchModel->getColor(),['onchange'=>'this.form.submit()']) ?>
	      				</div>
	      				<div class="col-md-3">
	                <?= $form->field($searchModel, 'type')->dropDownList($searchModel->getType(),['onchange'=>'this.form.submit()']) ?>
	      				</div>
	      			</div>
            </div>    	
		      	<?= yii\widgets\ListView::widget([        
			        'dataProvider' => $models,
			        'itemView' => function($model){
			          return Cards::widget([
			            'category'=>'disc',
			            'model'=>$model,
			            'link'=> '/disc/detail/'.$model->url,
			            // 'sale'=>['title'=> 'Акция','style'=>'danger'],
			            // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
			            // 'rating'=>['count'=>5,'rating'=>4],
			          ]);
			        },
			        'itemOptions' => [
			            'tag' => 'div',
			            'class' => 'col-sm-4 col-lg-4 box-product-card',
			        ],
			        'layout' => "<span class='hidden-xs'>{summary}</span><div class='clearfix'></div>{items}<div class='clearfix'></div>{pager}", // "{sorter}\n{summary}\n{items}\n{pager}
			        'summary' => "Показано {count} из {totalCount}",
			        'emptyText' => 'Ничего не найдено',

			      ]);?>
		      </div>
		    	<!-- end shiny -->
		    </div>
		  </div>
		</div>
</div>
</div>
<?php ActiveForm::end(); ?> 