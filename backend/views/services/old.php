<?php
$page = 0;
if(yii::$app->request->get('page'))
	$page = yii::$app->request->get('page');
?>
<div class="card" ng-controller="Calendar" ng-init="cr.getList()">
	<nav class="navbar navbar-primary card-header" data-background-color="purple" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Запись на шиномонтаж</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Бокс <span ng-bind="cr.type"></span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ng-repeat="box in cr.types"><a href="javascript:void(0)" ng-click="cr.setType(box)" ng-bind="box"></a></li>
          </ul>
        </li>
        <li ng-show="cr.page > 0">
        	<a href="javascript:void(0)" ng-click="cr.prevPage()">Предыдущая неделя</a>
        </li>
        <li>
        	<a href="javascript:void(0)" ng-click="cr.nextPage()">Следующая неделя</a>
        </li>
      </ul>
    </div>
    <!-- <a class="btn btn-simple btn-just-icon pull-right" rel="tooltip" title="Сброс фильтра" href="/order/index"><i class="material-icons">sync</i></a> -->
  </div>    
  </nav>
	<!-- <div class="card-header" data-background-color="orange">
	    <h4 class="title">Запись на шиномонтаж</h4>
	    <p class="category">В перриод от 01.05.2017</p>
	</div> -->
	<div class="card-content table-responsive" ng-show="cr.list.length > 0">
		<!-- <button ng-repeat="item in cr.one " class="btn btn-primary" ng-bind="item.time"></button> -->
		<table class="table table-minheight table-striped table-bordered" ng-show="cr.type == cr.types[0]">
			<tbody>
				<tr>
					<th>Время</th>
					<th><strong>Пн</strong> <strong ng-bind="cr.one.date"></strong></th>
					<th><strong>Вт</strong> <strong ng-bind="cr.two.date"></strong></th>
					<th><strong>Ср</strong> <strong ng-bind="cr.tree.date"></strong></th>
					<th><strong>Чт</strong> <strong ng-bind="cr.four.date"></strong></th>
					<th><strong>Пт</strong> <strong ng-bind="cr.five.date"></strong></th>
					<th><strong>Сб</strong> <strong ng-bind="cr.six.date"></strong></th>
					<th><strong>Вс</strong> <strong ng-bind="cr.seven.date"></strong></th>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>09:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'9:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'9:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'9:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'9:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'9:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'9:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'9:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'9:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'9:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'9:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'9:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'9:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'9:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'9:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>09:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'9:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'9:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'9:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'9:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'9:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'9:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'9:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'9:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'9:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'9:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'9:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'9:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'9:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'9:30:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>10:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'10:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'10:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'10:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'10:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'10:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'10:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'10:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'10:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'10:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'10:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'10:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'10:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'10:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'10:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>10:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'10:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'10:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'10:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'10:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'10:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'10:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'10:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'10:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'10:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'10:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'10:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'10:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'10:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'10:30:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>11:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'11:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'11:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'11:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'11:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'11:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'11:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'11:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'11:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'11:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'11:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'11:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'11:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'11:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'11:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>11:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'11:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'11:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'11:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'11:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'11:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'11:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'11:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'11:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'11:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'11:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'11:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'11:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'11:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'11:30:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>12:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'12:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'12:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'12:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'12:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'12:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'12:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'12:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'12:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'12:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'12:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'12:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'12:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'12:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'12:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>12:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'12:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'12:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'12:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'12:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'12:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'12:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'12:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'12:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'12:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'12:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'12:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'12:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'12:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'12:30:00',cr.type,cr.seven.pull)"></td>
				</tr>				
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>13:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'13:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'13:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'13:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'13:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'13:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'13:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'13:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'13:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'13:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'13:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'13:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'13:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'13:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'13:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>13:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'13:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'13:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'13:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'13:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'13:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'13:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'13:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'13:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'13:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'13:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'13:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'13:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'13:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'13:30:00',cr.type,cr.seven.pull)"></td>
				</tr>				
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>14:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'14:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'14:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'14:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'14:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'14:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'14:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'14:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'14:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'14:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'14:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'14:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'14:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'14:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'14:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>14:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'14:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'14:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'14:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'14:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'14:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'14:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'14:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'14:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'14:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'14:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'14:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'14:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'14:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'14:30:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>15:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'15:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'15:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'15:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'15:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'15:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'15:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'15:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'15:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'15:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'15:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'15:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'15:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'15:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'15:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>15:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'15:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'15:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'15:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'15:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'15:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'15:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'15:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'15:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'15:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'15:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'15:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'15:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'15:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'15:30:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>16:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'16:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'16:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'16:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'16:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'16:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'16:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'16:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'16:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'16:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'16:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'16:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'16:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'16:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'16:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>16:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'16:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'16:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'16:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'16:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'16:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'16:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'16:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'16:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'16:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'16:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'16:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'16:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'16:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'16:30:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>17:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'17:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'17:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'17:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'17:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'17:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'17:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'17:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'17:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'17:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'17:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'17:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'17:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'17:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'17:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>17:30</span></div></td>
					<td ng-click="getDetail(cr.one.date,'17:30:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'17:30:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'17:30:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'17:30:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'17:30:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'17:30:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'17:30:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'17:30:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'17:30:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'17:30:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'17:30:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'17:30:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'17:30:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'17:30:00',cr.type,cr.seven.pull)"></td>
				</tr>				
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>18:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'18:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'18:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'18:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'18:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'18:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'18:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'18:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'18:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'18:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'18:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'18:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'18:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'18:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'18:00:00',cr.type,cr.seven.pull)"></td>
				</tr>				
			</tbody>
		</table>

		<table class="table table-minheight table-striped table-bordered" ng-show="cr.type == cr.types[1]">
			<tbody>
				<tr>
					<th>Время</th>
					<th><strong>Пн</strong> <strong ng-bind="cr.one.date"></strong></th>
					<th><strong>Вт</strong> <strong ng-bind="cr.two.date"></strong></th>
					<th><strong>Ср</strong> <strong ng-bind="cr.tree.date"></strong></th>
					<th><strong>Чт</strong> <strong ng-bind="cr.four.date"></strong></th>
					<th><strong>Пт</strong> <strong ng-bind="cr.five.date"></strong></th>
					<th><strong>Сб</strong> <strong ng-bind="cr.six.date"></strong></th>
					<th><strong>Вс</strong> <strong ng-bind="cr.seven.date"></strong></th>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>09:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'9:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'9:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'9:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'9:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'9:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'9:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'9:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'9:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'9:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'9:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'9:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'9:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'9:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'9:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>10:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'10:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'10:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'10:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'10:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'10:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'10:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'10:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'10:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'10:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'10:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'10:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'10:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'10:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'10:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>11:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'11:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'11:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'11:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'11:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'11:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'11:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'11:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'11:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'11:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'11:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'11:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'11:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'11:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'11:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>12:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'12:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'12:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'12:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'12:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'12:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'12:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'12:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'12:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'12:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'12:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'12:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'12:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'12:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'12:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>13:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'13:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'13:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'13:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'13:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'13:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'13:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'13:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'13:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'13:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'13:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'13:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'13:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'13:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'13:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>14:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'14:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'14:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'14:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'14:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'14:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'14:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'14:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'14:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'14:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'14:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'14:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'14:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'14:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'14:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>15:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'15:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'15:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'15:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'15:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'15:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'15:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'15:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'15:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'15:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'15:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'15:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'15:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'15:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'15:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>16:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'16:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'16:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'16:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'16:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'16:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'16:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'16:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'16:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'16:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'16:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'16:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'16:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'16:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'16:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>17:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'17:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'17:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'17:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'17:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'17:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'17:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'17:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'17:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'17:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'17:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'17:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'17:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'17:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'17:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>18:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'18:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'18:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'18:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'18:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'18:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'18:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'18:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'18:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'18:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'18:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'18:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'18:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'18:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'18:00:00',cr.type,cr.seven.pull)"></td>
				</tr>					
			</tbody>
			</table>
			<table class="table table-minheight table-striped table-bordered" ng-show="cr.type == cr.types[2]">
			<tbody>
				<tr>
					<th>Время</th>
					<th><strong>Пн</strong> <strong ng-bind="cr.one.date"></strong></th>
					<th><strong>Вт</strong> <strong ng-bind="cr.two.date"></strong></th>
					<th><strong>Ср</strong> <strong ng-bind="cr.tree.date"></strong></th>
					<th><strong>Чт</strong> <strong ng-bind="cr.four.date"></strong></th>
					<th><strong>Пт</strong> <strong ng-bind="cr.five.date"></strong></th>
					<th><strong>Сб</strong> <strong ng-bind="cr.six.date"></strong></th>
					<th><strong>Вс</strong> <strong ng-bind="cr.seven.date"></strong></th>
				</tr>
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>00:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'0:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'0:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'0:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'0:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'0:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'0:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'0:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'0:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'0:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'0:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'0:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'0:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'0:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'0:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>01:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'1:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'1:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'1:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'1:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'1:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'1:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'1:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'1:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'1:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'1:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'1:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'1:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'1:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'1:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>02:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'2:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'2:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'2:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'2:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'2:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'2:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'2:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'2:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'2:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'2:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'2:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'2:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'2:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'2:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>03:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'3:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'3:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'3:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'3:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'3:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'3:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'3:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'3:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'3:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'3:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'3:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'3:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'3:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'3:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>04:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'4:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'4:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'4:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'4:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'4:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'4:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'4:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'4:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'4:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'4:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'4:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'4:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'4:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'4:00:00',cr.type,cr.seven.pull)"></td>
				</tr>	
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>05:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'5:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'5:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'5:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'5:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'5:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'5:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'5:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'5:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'5:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'5:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'5:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'5:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'5:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'5:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>06:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'6:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'6:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'6:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'6:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'6:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'6:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'6:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'6:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'6:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'6:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'6:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'6:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'6:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'6:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>07:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'7:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'7:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'7:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'7:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'7:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'7:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'7:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'7:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'7:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'7:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'7:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'7:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'7:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'7:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>08:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'8:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'8:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'8:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'8:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'8:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'8:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'8:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'8:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'8:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'8:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'8:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'8:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'8:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'8:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>09:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'9:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'9:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'9:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'9:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'9:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'9:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'9:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'9:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'9:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'9:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'9:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'9:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'9:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'9:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>10:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'10:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'10:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'10:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'10:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'10:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'10:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'10:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'10:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'10:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'10:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'10:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'10:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'10:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'10:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>11:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'11:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'11:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'11:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'11:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'11:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'11:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'11:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'11:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'11:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'11:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'11:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'11:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'11:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'11:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>12:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'12:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'12:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'12:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'12:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'12:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'12:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'12:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'12:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'12:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'12:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'12:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'12:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'12:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'12:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>13:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'13:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'13:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'13:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'13:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'13:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'13:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'13:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'13:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'13:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'13:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'13:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'13:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'13:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'13:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>14:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'14:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'14:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'14:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'14:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'14:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'14:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'14:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'14:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'14:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'14:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'14:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'14:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'14:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'14:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>15:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'15:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'15:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'15:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'15:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'15:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'15:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'15:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'15:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'15:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'15:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'15:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'15:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'15:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'15:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>16:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'16:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'16:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'16:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'16:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'16:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'16:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'16:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'16:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'16:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'16:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'16:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'16:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'16:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'16:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>17:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'17:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'17:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'17:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'17:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'17:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'17:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'17:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'17:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'17:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'17:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'17:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'17:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'17:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'17:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>18:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'18:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'18:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'18:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'18:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'18:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'18:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'18:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'18:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'18:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'18:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'18:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'18:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'18:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'18:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>19:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'19:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'19:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'19:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'19:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'19:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'19:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'19:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'19:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'19:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'19:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'19:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'19:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'19:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'19:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>20:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'20:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'20:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'20:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'20:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'20:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'20:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'20:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'20:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'20:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'20:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'20:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'20:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'20:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'20:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>21:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'21:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'21:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'21:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'21:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'21:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'21:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'21:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'21:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'21:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'21:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'21:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'21:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'21:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'21:00:00',cr.type,cr.seven.pull)"></td>
				</tr>		
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>22:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'22:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'22:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'22:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'22:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'22:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'22:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'22:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'22:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'22:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'22:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'22:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'22:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'22:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'22:00:00',cr.type,cr.seven.pull)"></td>
				</tr>			
				<tr>
					<td class="col-xs-1"><div class="box box-title"><span>23:00</span></div></td>
					<td ng-click="getDetail(cr.one.date,'23:00:00',cr.type,cr.one.pull)" ng-bind-html="getData(cr.one.date,'23:00:00',cr.type,cr.one.pull)"></td>
					<td ng-click="getDetail(cr.two.date,'23:00:00',cr.type,cr.two.pull)" ng-bind-html="getData(cr.two.date,'23:00:00',cr.type,cr.two.pull)"></td>
					<td ng-click="getDetail(cr.tree.date,'23:00:00',cr.type,cr.tree.pull)" ng-bind-html="getData(cr.tree.date,'23:00:00',cr.type,cr.tree.pull)"></td>
					<td ng-click="getDetail(cr.four.date,'23:00:00',cr.type,cr.four.pull)" ng-bind-html="getData(cr.four.date,'23:00:00',cr.type,cr.four.pull)"></td>
					<td ng-click="getDetail(cr.five.date,'23:00:00',cr.type,cr.five.pull)" ng-bind-html="getData(cr.five.date,'23:00:00',cr.type,cr.five.pull)"></td>
					<td ng-click="getDetail(cr.six.date,'23:00:00',cr.type,cr.six.pull)" ng-bind-html="getData(cr.six.date,'23:00:00',cr.type,cr.six.pull)"></td>
					<td ng-click="getDetail(cr.seven.date,'23:00:00',cr.type,cr.seven.pull)" ng-bind-html="getData(cr.seven.date,'23:00:00',cr.type,cr.seven.pull)"></td>
				</tr>				
			</tbody>
		</table>
		</table>
	</div>
	<div class="modal fade bs-example-modal-lg" id="serviceViev">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title modal-title-primary">Запись на шиномонтаж</h4>
        </div>
        <div class="modal-body">
        	<div class="card card-stats">
			      <div class="card-header" data-background-color="green">
			        <i class="material-icons">store</i>
			      </div>
			      <div class="card-content">
			        <p class="category"><span ng-bind="cr.view.address"></span></p>
			        <p class="category"><span ng-bind="cr.view.box"></span></p>
			        <p class="category"><span ng-bind="cr.view.status"></span></p>

			        <table class="table table-hover" style="text-align: left;">
			        	<tr>
			        		<td class="clearfix">Клиент</td>
			        		<td><span ng-bind="cr.view.name"></span></td>
			        	</tr>
			        	<tr>
			        		<td class="clearfix">Контакты</td>
			        		<td><span ng-bind="cr.view.phone"></span></td>
			        	</tr>
			        	<tr>
			        		<td class="clearfix">Подтвержден</td>
			        		<td><span ng-bind="cr.view.confirmed"></span></td>
			        	</tr>
			        	<tr>
			        		<td class="clearfix">Шины на хранении</td>
			        		<td><span ng-bind="cr.view.storage"></span></td>
			        	</tr>
			        	<tr>
			        		<td class="clearfix">Автомобиль</td>
			        		<td><span ng-bind-template="{{cr.view.model}} Гос. номер {{cr.view.inumber}}"></span></td>
			        	</tr>
			        	<tr>
			        		<td class="clearfix">Размер шин</td>
			        		<td><span ng-bind="cr.view.size"></span></td>
			        	</tr>
			        	<tr>
			        		<td class="clearfix">Комментарии</td>
			        		<td><span ng-bind="cr.view.comment"></span></td>
			        	</tr>
			        </table>
			        <div class="stats">
								<button class="btn btn-success btn-fab btn-fab-mini btn-round" rel="tooltip" data-original-title="Редактировать запись">
									<i class="material-icons">mode_edit</i>
								</button>
								<button ng-click="cr.extendTir('<?= yii::$app->user->identity->id_crm?>')" class="btn btn-success btn-fab btn-fab-mini btn-round" rel="tooltip" data-original-title="Продлить время">
									<i class="material-icons">alarm_add</i>
								</button>
								<button ng-click="cr.deleteTir()" class="btn btn-success btn-fab btn-fab-mini btn-round" rel="tooltip" data-original-title="Освободить время и удалить запись">
									<i class="material-icons">delete</i>
								</button>
							</div>
			      </div>
			      <div class="card-footer">
			        <div class="stats">
			          <i class="material-icons">date_range</i> <span ng-bind-template="{{cr.view.day}} {{cr.view.time}}"></span>
			        </div>
			        <div class="stats pull-right">
			          <i class="material-icons">person</i> <span ng-bind="cr.view.author"></span>
			        </div>
			      </div>
			    </div>
        </div>
      </div>
    </div>
  </div>

	<div class="modal fade bs-example-modal-lg" id="serviceCreate">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title modal-title-primary">Запись на шиномонтаж</h4>
        </div>
        <div class="modal-body" ng-click="cr.clearClient()">
        	<div class="card card-stats">
			      <div class="card-header" data-background-color="green">
			        <i class="material-icons">store</i>
			      </div>
			      <div class="card-content">
			        <p class="category"><span ng-bind="cr.view.box"></span></p>
			        <p class="category">Номер авто <strong ng-bind="cr.inumber"></strong></p>
			        <p class="category">Модель авто <strong ng-bind="cr.model"></strong></p>
			        <p class="category">Размер шин <strong ng-bind="cr.size"></strong></p>
			        <div class="clearfix"></div>

							<div class="alert alert-danger" ng-show="cr.errors.validate.length > 0">
						    <div class="container-fluid text-left">
						    	<span ng-repeat="err in cr.errors.validate" ng-bind="err"></span>
						    </div>
						  </div>
			        <form class="tirefitForm" id="tirefitForm" name="tirefitForm">
								<input type="hidden" name="time" value="{{cr.view.day}} {{cr.view.time}}"/>
								<input type="hidden" name="box" value="{{cr.view.box}}"/>
								<input type="hidden" name="author" value="<?= yii::$app->user->identity->id_crm?>"/>
								<input type="hidden" name="meneger_crm" value="<?= yii::$app->user->identity->id_crm?>"/>
								<input type="hidden" name="confirmed" value="Нет"/>
								<input type="hidden" name="status" value="{{cr.getStatus()}}"/>
								<input type="hidden" name="storage" value="{{cr.getStorage()}}"/>
								<input type="hidden" name="comment" value="{{cr.comment}}"/>
								<input type="hidden" name="inumber" value="{{cr.inumber}}"/>
								<input type="hidden" name="model" value="{{cr.model}}"/>
								<input type="hidden" name="size" value="{{cr.size}}"/>			
								<div ng-init="cr.loadClient()" class="form-group dropdown {{cr.fClients.name.length > 0 ? 'open':''}} {{cr.errors.name ? '' : 'has-error'}}"> <!-- ng-hide="cr.client" -->
									<label class="control-label">Имя, Фамилия? <span ng-hide="cr.errors.name" class="block-error">Должно быть минимум 2 символов</span></label>
									<input autocomplete="off" type="text" name="name" ng-change="cr.changename()" ng-model="cr.name" class="form-control" autofocus="" placeholder="Имя Фамилия клиента."/>
									<ul class="dropdown-menu">
										<li ng-repeat="p in cr.fClients.name"><a href="javascript:void(null)" ng-click="cr.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
									</ul>
								</div>		
											  
								<div class="form-group dropdown {{cr.fClients.phone.length > 0 ? 'open':''}} {{cr.errors.phone ? '' : 'has-error'}}"> <!-- ng-hide="cr.client" -->
								  <label class="control-label">Телефон <span ng-hide="cr.errors.phone" class="block-error">не соответствует формату телефона</span></label>
								  <input autocomplete="off" type="text" name="phone" id="phone" ng-change="cr.changephone()" ng-model="cr.phone" class="form-control" placeholder="Например +77273170077"/>	
								  <ul class="dropdown-menu">
										<li ng-repeat="p in cr.fClients.phone"><a href="javascript:void(null)" ng-click="cr.setClient(p)" ng-bind-template="{{p.name}} {{p.phone}}"></a></li>
									</ul>
								</div>
								<div class="form-group {{cr.errors.address?'':'has-error'}}"> <!-- ng-hide="cr.client" -->
								  <label class="control-label">Адрес для шиномонтажа <span ng-hide="cr.errors.address" class="block-error">минимум 6 символа</span></label>
								  <textarea class="form-control" ng-change="cr.changeaddress()" name="address" ng-model="cr.address" placeholder="Например г.Алматы, ул. Садовникова, 99 (Лазарева)"></textarea>
								</div>
							</form>
							<div class="form-group" ng-show="cr.auto.length > 0">
				        <label for="client">Выбор автомобиля</label>
								<select class="form-control" ng-change="cr.modelSelect()" ng-options="option.model for option in cr.auto" ng-model="cr.selectedAuto"></select>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group has-error">
									  <input autocomplete="off" ng-model="cr.n_inumber" type="text" class="form-control" placeholder="Гос. номер"/>
									  <label class="control-label"><span ng-show="cr.n_inumber_err" class="block-error text-xs">Должно быть минимум 5 символов</span></label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group has-error">
									  <input autocomplete="off" ng-model="cr.n_model" type="text" class="form-control" placeholder="Модель авто"/>
									  <label class="control-label"><span ng-show="cr.n_model_err" class="block-error text-xs">Должно быть минимум 3 символов</span></label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group has-error">
									  <input autocomplete="off" ng-model="cr.n_size" type="text" class="form-control" placeholder="Размеры шин"/>
									  <label class="control-label"><span ng-show="cr.n_size_err" class="block-error text-xs">Должно быть минимум 5 символов</span></label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<button class="btn btn-primary btn-xs" ng-click="cr.addAuto()">Добавить новый авто</button>
									</div>																
								</div>
							</div>
							<div class="row text-left">
								<div class="col-md-3">
									<div class="checkbox">
							      <label>
							        <input type="checkbox" ng-model="cr.storage" class="checkbox"/>
							        <span class="label label-primary">Шины на хранении</span>
							      </label>
							    </div>
							  </div>
								<div class="col-md-6">
									<div class="form-group">
									  <textarea class="form-control" name="comment" ng-model="cr.comment" placeholder="Комментарий"></textarea>
									</div>
								</div>
								<div class="col-md-3">
									<button class="btn btn-primary btn-block" ng-click="cr.createTir()">Записать</button>
								</div>
							</div>
			      </div>
			      <div class="card-footer">
			        <div class="stats">
			          <i class="material-icons">date_range</i> <span ng-bind-template="{{cr.view.day}} {{cr.view.time}}"></span>
			        </div>
			        <div class="stats pull-right">
			          <i class="material-icons">person</i> <span><?= yii::$app->user->identity->firstname?> <?= yii::$app->user->identity->lastname?> <?= yii::$app->user->identity->secondname?></span>
			        </div>
			      </div>
			    </div>
        </div>
      </div>
    </div>
  </div>
</div>