<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
  public $sourcePath = '@themes/creativeTim'; 
  public $baseUrl = '@web';
  public $css = [
    'css/material-dashboard.css',
    'css/simple-autocomplete.css',
    'css/demo.css?v=19',
    // 'http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
    'css/font-awesome.min.css',
    'css/font-face.css',
    // 'http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons',
  ];
  public $js = [
    'js/jquery-ui.min.js',
    'js/bootstrap.min.js',
    'js/bootstrap-datepicker.js',
    'js/material.min.js',
    'js/chartist.min.js',
    'js/bootstrap-notify.js',
    'js/material-dashboard.js',
    'js/angular.min.js',
    'js/simple-autocomplete.js',
    'js/ngSelectable.js',
    'js/cabinetApp.js',
    'js/demo.js',
    // 'js/jquery.maskedinput.min.js',
  ];
  public $depends = [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
  ];
}