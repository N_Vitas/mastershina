<?php

namespace backend\widgets;
use Yii;

class Cards extends \yii\base\Widget
{
	public $model;	
	public $link = '#';
	public $category = 'shiny';
	public $rating = [ 'rating' => 0 ];

	public function init(){
		if (empty($this->model)) {
      throw new Exception("Error Processing Request", 1);      
    }
	}

	public function run()
	{
		return $this->render("card-".$this->category,[
			'model'=>$this->model,
			'link'=>$this->link,
			'rating'=>$this->rating,
			'category'=>$this->category,
		]);
	}
}