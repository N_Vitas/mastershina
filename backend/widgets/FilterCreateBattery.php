<?php

namespace backend\widgets;
use Yii;

class FilterCreateBattery extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("create-battery");
	}
}