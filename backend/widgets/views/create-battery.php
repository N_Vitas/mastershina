<div ng-show="createorder.category == 'battery'">
  <!-- <div class="btn-toolbar" role="toolbar">
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_ostatok" ng-click="createorder.querySearch()"/> Больше 4-х {{s_shiny_ostatk}}</label>
      </div>                       
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_sklad" ng-click="createorder.querySearch()"/> Только свои склады</label>
      </div>                       
    </div>
  </div> -->
  <div class="btn-toolbar" role="toolbar">
    <div class="input-group">
        <autocomplete  
          options="d_battery_article" 
          place-holder="Артикул"
          on-select="onSelectBatteryArticle" 
          display-property="article"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_capacity" 
          place-holder="Ёмкость"
          on-select="onSelectBatteryCapacity" 
          display-property="capacity"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_current" 
          place-holder="Стартовый ток"
          on-select="onSelectBatteryCurrent" 
          display-property="current"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_polarity" 
          place-holder="Полярность"
          on-select="onSelectBatteryPolarity" 
          display-property="polarity"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_brend" 
          place-holder="Бренд"
          on-select="onSelectBatteryBrend" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_title" 
          place-holder="Название"
          on-select="onSelectBatteryTitle" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_length" 
          place-holder="Длина"
          on-select="onSelectBatteryLength" 
          display-property="length"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_width" 
          place-holder="Ширина"
          on-select="onSelectBatteryWidth" 
          display-property="width"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_height" 
          place-holder="Высота"
          on-select="onSelectBatteryHeight" 
          display-property="height"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_country" 
          place-holder="Страна"
          on-select="onSelectBatteryCountry"
          display-property="country"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_assurance" 
          place-holder="Гарантия"
          on-select="onSelectBatteryAssurance" 
          display-property="assurance"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_battery_voltage" 
          place-holder="Напряжение"
          on-select="onSelectBatteryVoltage" 
          display-property="voltage"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="clearInput()">Сброс</button>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="createorder.querySearch()">Применить</button>
    </div>
  </div>
</div>