<?php
if($model->old_price > 0 && $model->price > 0)
  $pr = round(100 - ($model->price * 100 / $model->old_price),0,PHP_ROUND_HALF_UP);
else
  $pr = 0;$icon;
$starone = $rating['rating'] > 0 ? '':'-o';
$startwo = $rating['rating'] > 1 ? '':'-o';
$startree = $rating['rating'] > 2 ? '':'-o';
$starfour = $rating['rating'] > 3 ? '':'-o';
$starfive = $rating['rating'] > 4 ? '':'-o';
switch ($model->season){
	case 'Всесезонные':
		$icon = '<i class="material-icons font-yellow">wb_sunny</i><i class="material-icons font-light-blue">ac_unit</i>'; 
		break;
		
	case 'Летние':
		$icon = '<i class="material-icons font-yellow">wb_sunny</i>'; 
		break;
		
	case 'Зимние':
		$icon = '<i class="material-icons font-light-blue">ac_unit</i>'; 
		break;
		
	default:
		$icon = '';
		break;
}
?>
<div class="card card-profile">
	<div class="card-avatar">
		<a href="#pablo">
			<img class="img" src="<?= $model->getPicture()?>">
		</a>
	</div>

	<div class="content">
		<h6 class="category text-gray">Код <?= $model->code?></h6>
		<h4 class="card-title"><?= $model->brend->title." ".$model->title?></h4>
		<p><?= number_format($model->price,0,',',' ')?> тг.</p>
		<p>В наличии <?= $model->ostatok?> шт.</p>
		<?= $icon?>
    <?= $mode->ship ? '<i class="icons-shipi">shipi</i>':'';?>
		<p class="card-content"><?= $model->width."/".$model->height."/".$model->radius?>&nbsp;<span><?= $model->speed?></span>&nbsp;<span><?= $model->loads?></span></p>

	  <?php if(count($rating)):?>
	    <div class="rating">
	      <i class="fa fa-star<?= $starone?>"></i>
	      <i class="fa fa-star<?= $startwo?>"></i>
	      <i class="fa fa-star<?= $startree?>"></i>
	      <i class="fa fa-star<?= $starfour?>"></i>
	      <i class="fa fa-star<?= $starfive?>"></i>
	      <a href="#">(<?=$rating['count']?> отзывов)</a>
	    </div>
	  <?php endif;?>
		<a href="#" onclick="return false" ng-click="basket.putCart(<?= $model->id;?>,'<?= $category?>')" class="btn btn-primary btn-round">В корзину</a>
	</div>
</div>