<?php
if($model->old_price > 0 && $model->price > 0)
  $pr = round(100 - ($model->price * 100 / $model->old_price),0,PHP_ROUND_HALF_UP);
else
  $pr = 0;$icon;
$starone = $rating['rating'] > 0 ? '':'-o';
$startwo = $rating['rating'] > 1 ? '':'-o';
$startree = $rating['rating'] > 2 ? '':'-o';
$starfour = $rating['rating'] > 3 ? '':'-o';
$starfive = $rating['rating'] > 4 ? '':'-o';
?>
<div class="card card-profile">
	<div class="card-avatar">
		<a href="#pablo">
			<img class="img" src="<?= $model->getPicture()?>">
		</a>
	</div>
	<div class="content">
		<h6 class="category text-gray">Код <?= $model->code?></h6>
		<h4 class="card-title"><?= $model->brend->title." ".$model->title?></h4>
		<p><?= number_format($model->price,0,',',' ')?> тг.</p>
		<div>В наличии <?= $model->ostatok?> шт.</div>
		<div><?= $model->getAttributeLabel('capacity')?> <?= $model->capacity?></div>
		<div><?= $model->getAttributeLabel('current')?> <?= $model->current?></div>
		<div><?= $model->getAttributeLabel('polarity')?> <?= $model->polarity?></div>
		<div><?= $model->getAttributeLabel('voltage')?> <?= $model->voltage?></div>

	  <?php if(count($rating)):?>
	    <div class="rating">
	      <i class="fa fa-star<?= $starone?>"></i>
	      <i class="fa fa-star<?= $startwo?>"></i>
	      <i class="fa fa-star<?= $startree?>"></i>
	      <i class="fa fa-star<?= $starfour?>"></i>
	      <i class="fa fa-star<?= $starfive?>"></i>
	      <a href="#">(<?=$rating['count']?> отзывов)</a>
	    </div>
	  <?php endif;?>
		<a href="#" onclick="return false" ng-click="basket.putCart(<?= $model->id;?>,'<?= $category?>')" class="btn btn-primary btn-round">В корзину</a>
	</div>
</div>