<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="material-icons">shopping_cart</i>
    <span class="notification" ng-bind="basket.positions.length">0</span>
    <p class="hidden-lg hidden-md">Товаров</p>
  </a>
  <ul class="dropdown-menu" ng-show="basket.positions.length>0">
    <li ng-repeat="pos in basket.positions"><a href="#" onclick="return false" ng-click="basket.deleteCart(pos.position)"><img class="img-thumbnail inline" src="{{pos.url}}" width="25" alt="product"/> {{pos.title}} <i class="material-icons pull-right">remove_shopping_cart</i></a></li>
    <li class="divider"></li>
    <li class="center-block"><span class="text-primary" ng-bind-template="Итого: {{t(basket.total())}}"></span></li>
    <li><a href="/order/create" class="btn btn-primary btn-round btn-sm"><i class="material-icons hidden-xs">shopping_cart</i> Перйти к оформлению</a></li>
  </ul>
</li>