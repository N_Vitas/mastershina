<div ng-show="createorder.category == 'oil'">
  <!-- <div class="btn-toolbar" role="toolbar">
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_ostatok" ng-click="createorder.querySearch()"/> Больше 4-х {{s_shiny_ostatk}}</label>
      </div>                       
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_sklad" ng-click="createorder.querySearch()"/> Только свои склады</label>
      </div>                       
    </div>
  </div> -->
  <div class="btn-toolbar" role="toolbar">
    <div class="input-group">
        <autocomplete  
          options="d_oil_viscosity_1" 
          place-holder="Вязкость 1"
          on-select="onSelectOilViscosity_1" 
          display-property="viscosity_1"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_oil_viscosity_2" 
          place-holder="Вязкость 2"
          on-select="onSelectOilViscosity_2" 
          display-property="viscosity_2"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_oil_type_fluid" 
          place-holder="Тип жидкости"
          on-select="onSelectOilTypeFluid" 
          display-property="type_fluid"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_oil_size" 
          place-holder="Объем"
          on-select="onSelectOilSize" 
          display-property="size"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_oil_type_engine" 
          place-holder="Тип Двигателя"
          on-select="onSelectOilTypeEngine" 
          display-property="type_engine"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_oil_brend" 
          place-holder="Бренд"
          on-select="onSelectOilBrend" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_oil_title" 
          place-holder="Название"
          on-select="onSelectOilTitle" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="clearInput()">Сброс</button>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="createorder.querySearch()">Применить</button>
    </div>
  </div>
</div>