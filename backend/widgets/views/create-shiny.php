<div ng-show="createorder.category == 'shiny'">
  <div class="btn-toolbar" role="toolbar">
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_shiny_season_full" ng-click="createorder.querySearch()"/> Всесезонная</label>
      </div>                       
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_shiny_season_sun" ng-click="createorder.querySearch()"/> Лето</label>
      </div>                       
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_shiny_season_win" ng-click="createorder.querySearch()"/> Зима</label>
      </div>                       
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_shiny_ship" ng-click="createorder.querySearch()"/> Шипованные</label>
      </div>    
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_ostatok" ng-click="createorder.querySearch()"/> Больше 4-х {{s_shiny_ostatk}}</label>
      </div>                       
    </div>
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_sklad" ng-click="createorder.querySearch()"/> Только свои склады</label>
      </div>                       
    </div>
  </div>
  <div class="btn-toolbar" role="toolbar">
    <div class="input-group">
        <autocomplete  
          options="d_shiny_width" 
          place-holder="Ширина"
          on-select="onSelectShinyWidth" 
          display-property="width"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_height" 
          place-holder="Высота"
          on-select="onSelectShinyHeight" 
          display-property="height"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_radius" 
          place-holder="Диаметр"
          on-select="onSelectShinyRadius" 
          display-property="radius"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_brend" 
          place-holder="Бренд"
          on-select="onSelectShinyBrend" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_title" 
          place-holder="Название"
          on-select="onSelectShinyTitle" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_loads" 
          place-holder="Индекс нагрузки"
          on-select="onSelectShinyLoads" 
          display-property="loads"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_speed" 
          place-holder="Индекс скорости"
          on-select="onSelectShinySpeed" 
          display-property="speed"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_typeTC" 
          place-holder="ТипТС"
          on-select="onSelectShinyTypeTC" 
          display-property="typeTC"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_vendor" 
          place-holder="Марка автомобиля"
          on-select="onSelectShinyVendor" 
          display-property="vendor"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_car_filter" 
          place-holder="Модель автомобиля"
          on-select="onSelectShinyCar" 
          display-property="car"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_shiny_year" 
          place-holder="Год автомобиля"
          on-select="onSelectShinyYear" 
          display-property="year"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="clearInput()">Сброс</button>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="createorder.querySearch()">Применить</button>
    </div>
  </div>
</div>