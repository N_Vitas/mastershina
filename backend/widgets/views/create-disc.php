<div ng-show="createorder.category == 'disc'">
  <div class="btn-toolbar" role="toolbar">
    <!-- <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_ostatok" ng-click="createorder.querySearch()"/> Больше 4-х {{s_disc_ostatk}}</label>
      </div>                       
    </div> -->
    <div class="btn-group">
      <div class="checkbox">
        <label><input type="checkbox" ng-model="s_sklad" ng-click="createorder.querySearch()"/> Только свои склады</label>
      </div>                       
    </div>
  </div>
  <div class="btn-toolbar" role="toolbar">
    <div class="input-group">
        <autocomplete  
          options="d_disc_size_r" 
          place-holder="Радиус"
          on-select="onSelectDiscSizeR" 
          display-property="size_r"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_size_w" 
          place-holder="Ширина диска"
          on-select="onSelectDiscSizeW" 
          display-property="size_w"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_borer_c" 
          place-holder="Кол. отверстий"
          on-select="onSelectDiscBorerC" 
          display-property="borer_c"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_borer_d" 
          place-holder="Раст. отверстий"
          on-select="onSelectDiscBorerD" 
          display-property="borer_d"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_stupica" 
          place-holder="Диаметр ступицы"
          on-select="onSelectDiscStupica" 
          display-property="stupica"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_radius" 
          place-holder="Вылет"
          on-select="onSelectDiscRadius" 
          display-property="radius"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_brend" 
          place-holder="Производитель"
          on-select="onSelectDiscBrend" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_title" 
          place-holder="Название"
          on-select="onSelectDiscTitle" 
          display-property="title"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_type" 
          place-holder="Тип"
          on-select="onSelectDiscType" 
          display-property="type"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
        <autocomplete  
          options="d_disc_color" 
          place-holder="Цвет"
          on-select="onSelectDiscColor" 
          display-property="color"
          input-class="form-control"
          clear-input="false">
        </autocomplete>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="clearInput()">Сброс</button>
    </div>
    <div class="input-group">
      <button class="btn btn-sm btn-primary" ng-click="createorder.querySearch()">Применить</button>
    </div>
  </div>
</div>
