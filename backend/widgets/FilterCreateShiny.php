<?php

namespace backend\widgets;
use Yii;

class FilterCreateShiny extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("create-shiny");
	}
}