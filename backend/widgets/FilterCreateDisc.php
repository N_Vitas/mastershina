<?php

namespace backend\widgets;
use Yii;

class FilterCreateDisc extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("create-disc");
	}
}