<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Disc;
use common\models\Vehicle;

class SearchDisc extends Disc
{
  // Добавляем автомобили в шины
  public $vendor,$car,$year,$modification,$brend;
  public function rules()
  {
    return [
      [['size', 'borer', 'brend', 'stupica', 'radius', 'color', 'type', 'manufacturer','code'],'save']
    ];
  }
  public function init(){
    return parent::init();
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'vendor'=>'Марка',
      'car'=>'Модель',
      'year'=>'Год',
      'modification'=>'Модификация',
      'brend'=>'Бренд',
    ]);
  }

  public function getSize(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['size'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->groupBy('size')->all(),'size','size');     
  }
  public function getBorer(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['borer'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->groupBy('borer')->all(),'borer','borer');  
  }
  public function getRadius(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['radius'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->groupBy('radius')->all(),'radius',
      	function($model){
    			return str_replace('R', '', $model->radius);
    		});
  }
  public function getStupica(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['stupica'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','stupica',''])->groupBy('stupica')->all(),'stupica','stupica');
  }
  public function getColor(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['color'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','color',''])->groupBy('color')->all(),'color','color');
  }
  public function getManufacturer(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['manufacturer'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','manufacturer',''])->groupBy('manufacturer')->all(),'manufacturer','manufacturer');
  }
  public function getType(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['type'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','type',''])->groupBy('type')->all(),'type','type');
  }
  public function getBrends(){
    return ['all'=>'Все']+ArrayHelper::map(Disc::find()->select(['parent_brend_id'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('parent_brend_id')->all(),'parent_brend_id',function($model){
        return $model->brend->title;
      });
  }

  // public function getVendor($where=[]){
  //   if(isset($where['vendor']))unset($where['vendor']);
  //   $this->_vendor = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['vendor'])->where($where)->groupBy('vendor')->all(),function($model){return $this->replaceKey($model,'vendor');},'vendor');
  //   return $this->_vendor;
  // }
  // public function getCar($where=[]){
  //   if(isset($where['car']))unset($where['car']);
  //   $this->_car = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['car'])->where($where)->groupBy('car')->all(),function($model){return $this->replaceKey($model,'car');},'car');
  //   return $this->_car;
  // }
  // public function getYear($where=[]){
  //   if(isset($where['year']))unset($where['year']);
  //   $this->_year = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),function($model){return $this->replaceKey($model,'year');},'year');
  //   return $this->_year;
  //   // return ['all'=>'Все',ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),'year','year')];
  // }
  // public function getModification($where=[]){
  //   if(isset($where['modification']))unset($where['modification']);
  //   $this->_modification = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['modification'])->where($where)->groupBy('modification')->all(),function($model){return $this->replaceKey($model,'modification');},'modification');
  //   return $this->_modification;
  // }
  // public function replaceKey($model,$key){
  //   $r = new Disc();
  //   return $r->generateUrl($model->$key);
  // }
  // public function scenarios()
  // {  
  //     return Model::scenarios();
  // }

  public function search($params) {

    $query = Disc::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['price' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->load($params)) {
        return $dataProvider;
    }
    // $auto=[];
    // if($this->vendor != '' && $this->vendor != 'all'){$auto['vendor']=$this->_vendor[$this->vendor];}
    // if($this->car != '' && $this->car != 'all'){$auto['car']=$this->_car[$this->car];}
    // if($this->year != '' && $this->year != 'all'){$auto['year']=$this->_year[$this->year];}
    // if($this->modification != '' && $this->modification != 'all'){$auto['modification']=$this->_modification[$this->modification];}
    // if(count($auto)>0){
    //   $vehicle = Vehicle::find()->select(['tyres_factory'])->where($auto)->groupBy('tyres_factory')->asArray()->all();
    //   if(count($vehicle) > 0)
    //   $size=[];
    //   foreach ($vehicle as $key => $value) {
    //     if($value['tyres_factory'] == ''){
    //       continue;
    //     }
    //     $size = array_merge($size,explode("|", $value['tyres_factory']));
    //   }
    //   if(count($size)>0)
    //     $query->andFilterWhere(['in','size',$size]);
    // }

    // if($this->runflat){
    //   $query->andFilterWhere(['runflat' => 1]);
    // }
    // if($this->ship){
    //   $query->andFilterWhere(['ship' => 1]);
    // }
    if($this->code){
      $query->andFilterWhere(['like','code',$this->code]);
      // $query->andFilterWhere(['like','model',$this->code]);
    }
    if($this->size != 'all'){
    	// var_dump($this->size);
      $query->andFilterWhere(['size' => $this->size]);
    }
    if($this->borer != 'all'){
      $query->andFilterWhere(['borer' => $this->borer]);
    }
    if($this->radius != 'all'){
      $query->andFilterWhere(['radius' => $this->radius]);
    }
    if($this->stupica != 'all'){
      $query->andFilterWhere(['stupica' => $this->stupica]);
    }
    if($this->color != 'all'){
      $query->andFilterWhere(['color' => $this->color]);
    }
    if($this->manufacturer != 'all'){
      $query->andFilterWhere(['manufacturer' => $this->manufacturer]);
    }

    if($this->type != 'all'){
      $query->andFilterWhere(['type' => $this->type]);
    }
    if($this->brend != 'all'){
      $query->andFilterWhere(['parent_brend_id' => $this->brend]);
    }

    return $dataProvider;
  }
}
