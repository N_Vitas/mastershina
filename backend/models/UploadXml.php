<?php

namespace backend\models;

use yii\base\Model;
// use yii\web\UploadedFile;

class UploadXml extends Model
{
    /**
     * @var UploadedFile
     */
    public $xmlFile;

    public function rules()
    {
        return [

            [['xmlFile'], 'file'],
            [['xmlFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xml',
            'checkExtensionByMimeType' => false],
        ];
            
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->xmlFile->saveAs('import/xml/import.' . $this->xmlFile->extension);
            return true;
        } else {
            return false;
        }
    }
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
        'xmlFile'=>'Выбрать файл импорта',
        ]);
    }
}