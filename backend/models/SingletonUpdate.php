<?php
namespace backend\models;

class SingletonUpdate
{
    private $session;
    public function __construct($session, $dbnew, $dbold) {
        if($session->get('exists_update')) {
            return $this->session;
        }
        $session->set('exists_update', true);
        $result = $dbold->createCommand('UPDATE `i_cat_elements` SET `tire_kol` = 0, `disk_kol` = 0, `a_kol` = 0, `m_kol` = 0')->execute();
        $dbnew->createCommand('UPDATE `shiny` SET `status` = 0')->execute();
        $dbnew->createCommand('UPDATE `disc` SET `status` = 0')->execute();
        $dbnew->createCommand('UPDATE `oil` SET `status` = 0')->execute();
        $dbnew->createCommand('UPDATE `battery` SET `status` = 0')->execute();
        $this->session = $result;
        return $this->session;
    }
    
    public static function destroy($session) {
        if($session->get('exists_update')) {
            $session->remove('exists_update');
        }        
    }
}