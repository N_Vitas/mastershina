<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Client;
use common\models\User;

class SearchClient extends Client
{
  public $list=false;
  public $closed=false;
  public function rules()
  {
    return [
      [['name', 'phone', 'address', 'manager', 'data'],'safe']
    ];
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'list' => 'Все заявки',
    ]);
  }

  public function search($params,$userId) {

    $query = Client::find();
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 25,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['id' => SORT_DESC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->load($params)) {
        // $query->andFilterWhere(['author'=>$userId]);
        // $query->andFilterWhere(['!=','status',2]);
        return $dataProvider;
    }
    // if($this->closed == 'false' && $this->status != 2){
    //   $query->andFilterWhere(['!=','status',2]);
    // }
    // if($this->list == 'false' || $this->list == false){
    //   $query->where(['author'=>$userId]);
    // }
    // if($this->created_at){
    //   $min = strtotime($this->created_at);
    //   $max = strtotime($this->created_at) + (60 * 60 * 24);
    //   $query->andFilterWhere(['between','created_at',$min,$max]);
    // }
    // if($this->author){
    //   $user = User::find()->where(['like','username',$this->author])->one();
    //   if($user){
    //     $query->andFilterWhere(['like','author',$user->id]);  
    //   }else{
    //     $query->andFilterWhere(['like','author',0]);
    //   }
    // }
    // if($this->manager_crm){
    //   $user = User::find()->where(['like','username',$this->manager_crm])->one();
    //   if($user){
    //     $query->andFilterWhere(['like','manager_crm',$user->id]);  
    //   }else{
    //     $query->andFilterWhere(['like','manager_crm',0]);
    //   }
    // }
    // if($this->name){
    //   $query->andFilterWhere(['like','name',$this->name]);
    // }
    // if($this->email){
    //   $query->andFilterWhere(['like','email',$this->email]);
    // }
    // if($this->price){
    //   $query->andFilterWhere(['like','price',$this->price]);
    // }
    // if($this->phone){
    //   $query->andFilterWhere(['like','phone',$this->phone]);
    // }
    // if($this->comment){
    //   $query->andFilterWhere(['like','comment',$this->comment]);
    // }
    // if($this->theme){
    //   $query->andFilterWhere(['like','theme',$this->theme]);
    // }
    // if($this->address){
    //   $query->andFilterWhere(['like','address',$this->address]);
    // }
    // if($this->status != -1){
    //   $query->andFilterWhere(['status'=>$this->status]);
    // }
    // if($this->status_delivery != -1){
    //   $query->andFilterWhere(['status_delivery'=>$this->status_delivery]);
    // }
    // if($this->status_payment != -1){
    //   $query->andFilterWhere(['status_payment'=>$this->status_payment]);
    // }

    return $dataProvider;
  }
}
