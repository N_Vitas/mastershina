<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Oil;
use common\models\Brend;

class SearchOil extends Oil
{
  public $brend;
  public function rules()
  {
    return [
      [['type_fluid', 'brend', 'type_engine', 'size', 'viscosity_1','code'],'save']
    ];
  }
  public function init(){
    return parent::init();
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'brend'=>'Бренд',
    ]);
  }

  public function getType_fluid(){
    return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['type_fluid'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->andWhere(['!=','type_fluid',''])->groupBy('type_fluid')->all(),'type_fluid','type_fluid');     
  }
  public function getType_engine(){
    return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['type_engine'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->andWhere(['!=','type_engine',''])->groupBy('type_engine')->all(),'type_engine','type_engine');  
  }
  public function getSize(){
    return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['size'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->andWhere(['!=','size',''])->groupBy('size')->all(),'size','size');
  }
  public function getViscosity_1(){
    return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['viscosity_1'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->andWhere(['!=','viscosity_1',''])->groupBy('viscosity_1')->all(),'viscosity_1','viscosity_1');
  }
  public function getBrends(){
    return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['parent_brend_id'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('parent_brend_id')->all(),'parent_brend_id',function($model){
        return $model->brend->title;
      });
  }

  public function search($params) {

    $query = Oil::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['price' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->load($params)) {
        return $dataProvider;
    }
    if($this->code){
      $query->andFilterWhere(['like','code',$this->code]);
    }
    if($this->type_fluid != 'all'){
      // var_dump($this->type_fluid);
      $query->andFilterWhere(['type_fluid' => $this->type_fluid]);
    }
    if($this->type_engine != 'all'){
      $query->andFilterWhere(['type_engine' => $this->type_engine]);
    }
    if($this->size != 'all'){
      $query->andFilterWhere(['size' => $this->size]);
    }
    if($this->viscosity_1 != 'all'){
      $query->andFilterWhere(['viscosity_1' => $this->viscosity_1]);
    }
    if($this->brend != 'all'){
      $query->andFilterWhere(['parent_brend_id' => $this->brend]);
    }

    return $dataProvider;
  }
}
