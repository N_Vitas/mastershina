<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Battery;
use common\models\Vehicle;

class SearchBattery extends Battery
{
  public $brend;
  public function rules()
  {
    return [
      [['capacity', 'brend', 'current', 'polarity', 'voltage','code'],'save']
    ];
  }
  public function init(){
    return parent::init();
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'brend'=>'Бренд',
    ]);
  }

  public function getCapacity(){
    return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select(['capacity'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('capacity')->all(),'capacity','capacity');     
  }
  public function getCurrent(){
    return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select(['current'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('current')->all(),'current','current');  
  }
  public function getPolarity(){
    return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select(['polarity'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('polarity')->all(),'polarity','polarity');
  }
  public function getVoltage(){
    return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select(['voltage'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->andWhere(['!=','voltage',''])->groupBy('voltage')->all(),'voltage','voltage');
  }
  public function getBrends(){
    return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select(['parent_brend_id'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('parent_brend_id')->all(),'parent_brend_id',function($model){
        return $model->brend->title;
      });
  }

  public function search($params) {

    $query = Battery::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['price' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->load($params)) {
        return $dataProvider;
    }
    if($this->code){
      $query->andFilterWhere(['like','code',$this->code]);
    }
    if($this->capacity != 'all'){
      // var_dump($this->capacity);
      $query->andFilterWhere(['capacity' => $this->capacity]);
    }
    if($this->current != 'all'){
      $query->andFilterWhere(['current' => $this->current]);
    }
    if($this->polarity != 'all'){
      $query->andFilterWhere(['polarity' => $this->polarity]);
    }
    if($this->voltage != 'all'){
      $query->andFilterWhere(['voltage' => $this->voltage]);
    }
    if($this->brend != 'all'){
      $query->andFilterWhere(['parent_brend_id' => $this->brend]);
    }

    return $dataProvider;
  }
}
