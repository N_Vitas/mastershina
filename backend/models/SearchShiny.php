<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Shiny;
use common\models\Vehicle;

class SearchShiny extends Shiny
{
  // Добавляем автомобили в шины
  public $vendor,$car,$year,$modification,$brend;
  private $where=['>','price',0,'>','ostatok',0];
  public function rules()
  {
    return [
      [['width','height','radius', 'brend', 'season','season','loads','speed','typeTC','vendor','car','year','modification','code'],'save']
    ];
  }

  public function init(){
    return parent::init();
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'sorter'=>'Сортировать по',
      'vendor'=>'Марка',
      'car'=>'Модель',
      'year'=>'Год',
      'modification'=>'Модификация',
      'brend'=>'Бренд',
    ]);
  }

  public function getWidth(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['width'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->groupBy('width')->all(),'width','width');     
  }
  public function getHeight(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['height'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->groupBy('height')->all(),'height','height');  
  }
  public function getRadius(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['radius'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->groupBy('radius')->all(),'radius',
      	function($model){
    			return str_replace('R', '', $model->radius);
    		});
  }
  public function getSeason(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['season'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','season',''])->groupBy('season')->all(),'season','season');
  }
  public function getLoads(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['loads'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','loads',''])->groupBy('loads')->all(),'loads','loads');
  }
  public function getSpeed(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['speed'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','speed',''])->groupBy('speed')->all(),'speed','speed');
  }
  public function getTypeTC(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['typeTC'])->where(['>','price',0])
    	->andWhere(['>','ostatok',0])->andWhere(['!=','typeTC',''])->groupBy('typeTC')->all(),'typeTC','typeTC');
  }

  public function getVendor($where=[]){
    if(isset($where['vendor']))unset($where['vendor']);
    $this->_vendor = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['vendor'])->where($where)->groupBy('vendor')->all(),function($model){return $this->replaceKey($model,'vendor');},'vendor');
    return $this->_vendor;
  }
  public function getCar($where=[]){
    if(isset($where['car']))unset($where['car']);
    $this->_car = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['car'])->where($where)->groupBy('car')->all(),function($model){return $this->replaceKey($model,'car');},'car');
    return $this->_car;
  }
  public function getYear($where=[]){
    if(isset($where['year']))unset($where['year']);
    $this->_year = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),function($model){return $this->replaceKey($model,'year');},'year');
    return $this->_year;
    // return ['all'=>'Все',ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),'year','year')];
  }
  public function getModification($where=[]){
    if(isset($where['modification']))unset($where['modification']);
    $this->_modification = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['modification'])->where($where)->groupBy('modification')->all(),function($model){return $this->replaceKey($model,'modification');},'modification');
    return $this->_modification;
  }
  public function getBrends(){
    return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['parent_brend_id'])->where(['>','price',0])
      ->andWhere(['>','ostatok',0])->groupBy('parent_brend_id')->all(),'parent_brend_id',function($model){
        return $model->brend->title;
      });
  }
  public function replaceKey($model,$key){
    $r = new Shiny();
    return $r->generateUrl($model->$key);
  }
  // public function scenarios()
  // {  
  //     return Model::scenarios();
  // }

  public function search($params) {

    $query = Shiny::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['price' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->load($params)) {
        return $dataProvider;
    }
    if($this->code){
      $query->andFilterWhere(['like','code',$this->code]);
    }
    $auto=[];
    if($this->vendor != '' && $this->vendor != 'all'){$auto['vendor']=$this->_vendor[$this->vendor];}
    if($this->car != '' && $this->car != 'all'){$auto['car']=$this->_car[$this->car];}
    if($this->year != '' && $this->year != 'all'){$auto['year']=$this->_year[$this->year];}
    if($this->modification != '' && $this->modification != 'all'){$auto['modification']=$this->_modification[$this->modification];}
    if(count($auto)>0){
      $vehicle = Vehicle::find()->select(['tyres_factory'])->where($auto)->groupBy('tyres_factory')->asArray()->all();
      if(count($vehicle) > 0)
      $size=[];
      foreach ($vehicle as $key => $value) {
        if($value['tyres_factory'] == ''){
          continue;
        }
        $size = array_merge($size,explode("|", $value['tyres_factory']));
      }
      if(count($size)>0)
        $query->andFilterWhere(['in','size',$size]);
    }

    if($this->runflat){
      $query->andFilterWhere(['runflat' => 1]);
    }
    if($this->ship){
      $query->andFilterWhere(['ship' => 1]);
    }
    if($this->width != 'all'){
    	// var_dump($this->width);
      $query->andFilterWhere(['width' => $this->width]);
    }
    if($this->height != 'all'){
      $query->andFilterWhere(['height' => $this->height]);
    }
    if($this->radius != 'all'){
      $query->andFilterWhere(['radius' => $this->radius]);
    }
    if($this->season != 'all'){
      $query->andFilterWhere(['season' => $this->season]);
    }
    if($this->loads != 'all'){
      $query->andFilterWhere(['loads' => $this->loads]);
    }
    if(is_array($this->speed) && count($this->speed) > 0){
      foreach ($this->speed as $value) {
        if($value == '')continue;
        $w[] = $this->_speed[$value];        
      }
      $query->andFilterWhere(['speed' => $w]);//this->_speed[str_replace('speed-','',$this->speed)]]);
    }

    if($this->typeTC != 'all'){
      $query->andFilterWhere(['typeTC' => $this->typeTC]);
    }
    if($this->brend != 'all'){
      $query->andFilterWhere(['parent_brend_id' => $this->brend]);
    }
    // if(is_array($this->typeTC) && count($this->typeTC) > 0){
    //   foreach ($this->typeTC as $value) {
    //     $w[] = $this->_typeTC[$value];        
    //   }
    //   $query->andFilterWhere(['typeTC' => $w]);
    // }

    return $dataProvider;
  }
}
