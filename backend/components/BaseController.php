<?php
namespace backend\components;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class BaseController extends Controller{

  public $layout = 'main';

	public function init(){
		return parent::init();    
	}
	/**
   * @inheritdoc
   */
  public function behaviors()
  {
  	// public $layout = 'landing';
    return array_merge(parent::behaviors(), [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['login', 'error','check-list'],
            'allow' => true,
          ],
          [
            'actions' => ['logout', 'index', 'create','success','update','view','test'],
            'allow' => true,
            'roles' => ['admin','manager','dealer','operator'],
          ],
          [
            'actions' => ['signup'],
            'allow' => true,
            'roles' => ['admin'],
          ],
        ],
      ],
    ]);
  }

  public function beforeAction($action)
	{
    date_default_timezone_set('Asia/Almaty');
    // Подгрузка JS файлов
    $jsFile = 'js/controllers/' . $this->id . '.js';
    if (is_file($jsFile)) {
      $this->view->registerJsFile($jsFile, ['depends' => '\backend\assets\AppAsset']);
    }
    $jsFile = 'js/controllers/' . $this->id . '.' . $action->id . '.js';
    if (is_file($jsFile)) {
      $this->view->registerJsFile($jsFile, ['depends' => '\backend\assets\AppAsset']);
    }
		return parent::beforeAction($action);
	}
  /**
   * @inheritdoc
   */
  public function actions()
  {
    return array_merge(parent::actions(), [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ]);
  }
}
?>