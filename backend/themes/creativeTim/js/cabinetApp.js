var app = angular.module('cabinetApp', ['simple-autocomplete','ngSelectable']);

app.controller('AppController',function($scope,$http,$window){
	$http.defaults.headers.post["Content-Type"] = "application/json";
	$http.get('/api/basket').then(function(succes){
		if(succes.status == 200){
			$scope.basket.positions = succes.data;
			// обновляем на случай если колличество перевалит за наличие
			for (i in $scope.basket.positions){
				$scope.basket.updateCart($scope.basket.positions[i]);
			}
			
		}
	},
	function(err){
		console.log(err);
	}
	)
	$scope.t = function(x) {
    x = x.toString();var p = /(-?\d+)(\d{3})/; while(p.test(x))x = x.replace(p, '$1 $2');return x+" тг.";
  }
  $scope.getItem = function(params,value){
  	if($window.localStorage.getItem(params) == null)
  		$window.localStorage.setItem(params,value)
  	return $window.localStorage.getItem(params);
  }

	$scope.pagemodal = {
		show:function(){$('#myModal').modal('show')},
		hide:function(){$('#myModal').modal('hide')},
		setTitle:function(text){ $('.modal-title').text(text)},
		setBody:function(body){ $('.modal-body').html(body)},
		setFooter:function(footer){ $('.modal-footer').html(footer)},
	}

  $scope.show_filter = false,

	$scope.toggle = function(){
		$scope.show_filter = !$scope.show_filter;
	}
/****AUTOCOMPLETE MODULE****/
	// FILTER SHINY
	// Нам нужны в фильтре Шин Ширина Высота Диаметр Бренд Модель Сезонность Индекс скорости Индекс нагрузки Тип Шипы (Марка авто Модель авто Год под вопросом)
	// Нам нужны в фильтре Дисков Радиус Ширина Кол отверстий Расстояние отверстий Диаметр ступицы Вылет Производитель Название Тип Цвет (Марка авто Модель авто Год под вопросом)
	// Нам нужны в фильтре Масла Вязкость 1 Вязкость 2 Тип жидкости Объем Тип Двигателя Бренд Название
	// Нам нужны в фильтре Аккумуляторы Артикул Ёмкость Стартовый ток Полярность Бренд Название Длина Ширина Высота Страна Гарантия Напряжение
	// Инициализация фильтра по умолчанию
	$scope.initialShiny = function(){
		// поля дисков
		$scope.s_disc_size_r = null; // Радиус
		$scope.s_disc_size_w = null; // Ширина
		$scope.s_disc_borer_c = null; // Кол отверстий
		$scope.s_disc_borer_d = null; // Расстояние отверстий
		$scope.s_disc_stupica = null; // Диаметр ступицы
		$scope.s_disc_radius = null; // Вылет
		$scope.s_disc_brend = null; // Производитель
		$scope.s_disc_title = null; // Название
		$scope.s_disc_type = null; // Тип
		$scope.s_disc_color = null; // Цвет
		// поля шин
		$scope.s_shiny_width = null;
		$scope.s_shiny_height = null;
		$scope.s_shiny_radius = null;
		$scope.s_shiny_brend = null;
		$scope.s_shiny_title = null;
		$scope.s_shiny_loads = null;
		$scope.s_shiny_speed = null;
		$scope.s_shiny_typeTC = null;
		$scope.s_shiny_vendor = null;
		$scope.s_shiny_car = null;
		$scope.s_shiny_year = null;
		// поля масел 
		$scope.s_oil_viscosity_1 = null; // Вязкость 1
		$scope.s_oil_viscosity_2 = null; // Вязкость 2
		$scope.s_oil_type_fluid = null; // Тип жидкости
		$scope.s_oil_size = null; // Объем
		$scope.s_oil_type_engine = null; // Тип Двигателя
		$scope.s_oil_brend = null; // Бренд
		$scope.s_oil_title = null; // Название
		// поля для аккумуляторов
    $scope.s_battery_article = null; // Артикул
    $scope.s_battery_capacity = null; // Ёмкость
    $scope.s_battery_current = null; // Стартовый ток
    $scope.s_battery_polarity = null; // Полярность
    $scope.s_battery_brend = null; // Бренд
    $scope.s_battery_title = null; // Название
    $scope.s_battery_length = null; // Длина
    $scope.s_battery_width = null; // Ширина
    $scope.s_battery_height = null; // Высота
    $scope.s_battery_country = null; // Страна
    $scope.s_battery_assurance = null; // Гарантия
    $scope.s_battery_voltage = null; // Напряжение
		// Галочки
		$scope.s_ostatok = true;
		$scope.s_sklad = true;		
		$scope.s_shiny_season_sun = true;
		$scope.s_shiny_season_win = false;
		$scope.s_shiny_season_full = true;
		$scope.s_shiny_ship = false;	
	}
	$scope.initialShiny();
	// скопы для автокомплита дисков
	$scope.d_disc_size_r = [];
	$scope.d_disc_size_w = [];
	$scope.d_disc_borer_c = [];
	$scope.d_disc_borer_d = [];
	$scope.d_disc_stupica = [];
	$scope.d_disc_radius = [];
	$scope.d_disc_brend = [];
	$scope.d_disc_title = [];
	$scope.d_disc_type = [];
	$scope.d_disc_color = [];
	// скопы для автокомплита шин
	$scope.d_shiny_width = [];
	$scope.d_shiny_height = [];
	$scope.d_shiny_radius = [];
	$scope.d_shiny_brend = [];
	$scope.d_shiny_title = [];
	$scope.d_shiny_loads = [];
	$scope.d_shiny_speed = [];
	$scope.d_shiny_typeTC = [];
	$scope.d_shiny_vendor = [];
	$scope.d_shiny_car = [];
	$scope.d_shiny_car_filter = [];
	$scope.d_shiny_year = [];
	// скопы для автокомплита масел
	$scope.d_oil_viscosity_1 = [];
	$scope.d_oil_viscosity_2 = [];
	$scope.d_oil_type_fluid = [];
	$scope.d_oil_size = [];
	$scope.d_oil_type_engine = [];
	$scope.d_oil_brend = [];
	$scope.d_oil_title = [];
	// скопы для автокомплита аккумуляторов
	$scope.d_battery_article = [];
	$scope.d_battery_capacity = [];
	$scope.d_battery_current = [];
	$scope.d_battery_polarity = [];
	$scope.d_battery_brend = [];
	$scope.d_battery_title = [];
	$scope.d_battery_length = [];
	$scope.d_battery_width = [];
	$scope.d_battery_height = [];
	$scope.d_battery_country = [];
	$scope.d_battery_assurance = [];
	$scope.d_battery_voltage = [];
	// функции выбора шин
	$scope.onSelectShinyWidth = function(s){$scope.s_shiny_width = s;console.log(s)};
	$scope.onSelectShinyHeight = function(s){$scope.s_shiny_height = s};
	$scope.onSelectShinyRadius = function(s){$scope.s_shiny_radius = s};
	$scope.onSelectShinyBrend = function(s){$scope.s_shiny_brend = s};
	$scope.onSelectShinyTitle = function(s){$scope.s_shiny_title = s};
	$scope.onSelectShinyLoads = function(s){$scope.s_shiny_loads = s};
	$scope.onSelectShinySpeed = function(s){$scope.s_shiny_speed = s};
	$scope.onSelectShinyTypeTC = function(s){$scope.s_shiny_typeTC = s};
	$scope.onSelectShinyVendor = function(s){
		$scope.s_shiny_vendor = s;
		$scope.d_shiny_car_filter = $scope.d_shiny_car.filter(function(option){
			console.log('onSelectShinyVendor',$scope.d_shiny_car_filter.length,$scope.d_shiny_car.length)			
			return option.vendor == s.vendor;
		})		
	};
	$scope.onSelectShinyCar = function(s){$scope.s_shiny_car = s};
	$scope.onSelectShinyYear = function(s){$scope.s_shiny_year = s};
	// функуии выбора дисков
	$scope.onSelectDiscSizeR = function(s){$scope.s_disc_size_r = s};
	$scope.onSelectDiscSizeW = function(s){$scope.s_disc_size_w = s};
	$scope.onSelectDiscBorerC = function(s){$scope.s_disc_borer_c = s};
	$scope.onSelectDiscBorerD = function(s){$scope.s_disc_borer_d = s};
	$scope.onSelectDiscStupica = function(s){$scope.s_disc_stupica = s};
	$scope.onSelectDiscRadius = function(s){$scope.s_disc_radius = s};
	$scope.onSelectDiscBrend = function(s){$scope.s_disc_brend = s};
	$scope.onSelectDiscTitle = function(s){$scope.s_disc_title = s};
	$scope.onSelectDiscType = function(s){$scope.s_disc_type = s};
	$scope.onSelectDiscColor = function(s){$scope.s_disc_color = s};
	// функции выбора масел
	$scope.onSelectOilViscosity_1 = function(s){$scope.s_oil_viscosity_1 = s};
	$scope.onSelectOilViscosity_2 = function(s){$scope.s_oil_viscosity_2 = s};
	$scope.onSelectOilTypeFluid = function(s){$scope.s_oil_type_fluid = s};
	$scope.onSelectOilSize = function(s){$scope.s_oil_size = s};
	$scope.onSelectOilTypeEngine = function(s){$scope.s_oil_type_engine = s};
	$scope.onSelectOilBrend = function(s){$scope.s_oil_brend = s};
	$scope.onSelectOilTitle = function(s){$scope.s_oil_title = s};
	// функции выбора аккумуляторов
	$scope.onSelectBatteryArticle = function(s){$scope.s_battery_article = s};
	$scope.onSelectBatteryCapacity = function(s){$scope.s_battery_capacity = s};
	$scope.onSelectBatteryCurrent = function(s){$scope.s_battery_current = s};
	$scope.onSelectBatteryPolarity = function(s){$scope.s_battery_polarity = s};
	$scope.onSelectBatteryBrend = function(s){$scope.s_battery_brend = s};
	$scope.onSelectBatteryTitle = function(s){$scope.s_battery_title = s};
	$scope.onSelectBatteryLength = function(s){$scope.s_battery_length = s};
	$scope.onSelectBatteryWidth = function(s){$scope.s_battery_width = s};
	$scope.onSelectBatteryHeight = function(s){$scope.s_battery_height = s};
	$scope.onSelectBatteryCountry = function(s){$scope.s_battery_country = s};
	$scope.onSelectBatteryAssurance = function(s){$scope.s_battery_assurance = s};
	$scope.onSelectBatteryVoltage = function(s){$scope.s_battery_voltage = s};
	// кеширование и заполнение данных автокомплита
	$scope.loadAutocomplit = function(){
		if($scope.d_shiny_width.length == 0){
			$http.get('/api/shiny-width').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_width = succes.data;	
				}},function(err){console.log('d_shiny_width',err);})
		}
		if($scope.d_shiny_height.length == 0){
			$http.get('/api/shiny-height').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_height = succes.data;	
				}},function(err){console.log('d_shiny_height',err);})
		}
		if($scope.d_shiny_radius.length == 0){
			$http.get('/api/shiny-radius').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_radius = succes.data;	
				}},function(err){console.log('d_shiny_radius',err);})
		}
		if($scope.d_shiny_brend.length == 0){
			$http.get('/api/shiny-brend').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_brend = succes.data;	
				}},function(err){console.log('d_shiny_brend',err);})
		}
		if($scope.d_shiny_title.length == 0){
			$http.get('/api/shiny-title').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_title = succes.data;	
				}},function(err){console.log('d_shiny_title',err);})
		}
		if($scope.d_shiny_loads.length == 0){
			$http.get('/api/shiny-loads').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_loads = succes.data;	
				}},function(err){console.log('d_shiny_loads',err);})
		}
		if($scope.d_shiny_speed.length == 0){
			$http.get('/api/shiny-speed').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_speed = succes.data;	
				}},function(err){console.log('d_shiny_speed',err);})
		}
		if($scope.d_shiny_typeTC.length == 0){
			$http.get('/api/shiny-typetc').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_typeTC = succes.data;	
				}},function(err){console.log('d_shiny_typeTC',err);})
		}
		if($scope.d_shiny_vendor.length == 0){
			$http.get('/api/shiny-vendor').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_vendor = succes.data;	
				}},function(err){console.log('d_shiny_vendor',err);})
		}
		if($scope.d_shiny_car.length == 0){
			$http.get('/api/shiny-car').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_car = succes.data;
					$scope.d_shiny_car_filter = succes.data;
				}},function(err){console.log('d_shiny_car',err);})
		}
		if($scope.d_shiny_year.length == 0){
			$http.get('/api/shiny-year').then(function(succes){
				if(succes.status == 200){
					$scope.d_shiny_year = succes.data;	
				}},function(err){console.log('d_shiny_year',err);})
		}
		if($scope.d_disc_size_r.length == 0){
			$http.get('/api/disc-size-r').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_size_r = succes.data;
				}},function(err){console.log('d_disc_size_r',err);})
		} 
		if($scope.d_disc_size_w.length == 0){
			$http.get('/api/disc-size-w').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_size_w = succes.data;
				}},function(err){console.log('d_disc_size_w',err);})
		} 
		if($scope.d_disc_borer_c.length == 0){
			$http.get('/api/disc-borer-c').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_borer_c = succes.data;
				}},function(err){console.log('d_disc_borer_c',err);})
		} 
		if($scope.d_disc_borer_d.length == 0){
			$http.get('/api/disc-borer-d').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_borer_d = succes.data;
				}},function(err){console.log('d_disc_borer_d',err);})
		} 
		if($scope.d_disc_stupica.length == 0){
			$http.get('/api/disc-stupica').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_stupica = succes.data;
				}},function(err){console.log('d_disc_stupica',err);})
		} 
		if($scope.d_disc_radius.length == 0){
			$http.get('/api/disc-radius').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_radius = succes.data;
				}},function(err){console.log('d_disc_radius',err);})
		} 
		if($scope.d_disc_brend.length == 0){
			$http.get('/api/disc-brend').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_brend = succes.data;
				}},function(err){console.log('d_disc_brend',err);})
		} 
		if($scope.d_disc_title.length == 0){
			$http.get('/api/disc-title').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_title = succes.data;
				}},function(err){console.log('d_disc_title',err);})
		} 
		if($scope.d_disc_type.length == 0){
			$http.get('/api/disc-type').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_type = succes.data;
				}},function(err){console.log('d_disc_type',err);})
		} 
		if($scope.d_disc_color.length == 0){
			$http.get('/api/disc-color').then(function(succes){
				if(succes.status == 200){
					$scope.d_disc_color = succes.data;
				}},function(err){console.log('d_disc_color',err);})
		} 
		if($scope.d_oil_viscosity_1.length == 0){
			$http.get('/api/oil-viscosity1').then(function(succes){
				if(succes.status == 200){
					console.log('loadAutocomplit $scope.d_oil_viscosity_1',$scope.d_oil_viscosity_1)
					$scope.d_oil_viscosity_1 = succes.data;
				}},function(err){console.log('d_oil_viscosity_1',err);})
		}
		if($scope.d_oil_viscosity_2.length == 0){
			$http.get('/api/oil-viscosity2').then(function(succes){
				if(succes.status == 200){
					$scope.d_oil_viscosity_2 = succes.data;
				}},function(err){console.log('d_oil_viscosity_2',err);})
		}
		if($scope.d_oil_type_fluid.length == 0){
			$http.get('/api/oil-type-fluid').then(function(succes){
				if(succes.status == 200){
					$scope.d_oil_type_fluid = succes.data;
				}},function(err){console.log('d_oil_type_fluid',err);})
		}
		if($scope.d_oil_size.length == 0){
			$http.get('/api/oil-size').then(function(succes){
				if(succes.status == 200){
					$scope.d_oil_size = succes.data;
				}},function(err){console.log('d_oil_size',err);})
		}
		if($scope.d_oil_type_engine.length == 0){
			$http.get('/api/oil-type-engine').then(function(succes){
				if(succes.status == 200){
					$scope.d_oil_type_engine = succes.data;
				}},function(err){console.log('d_oil_type_engine',err);})
		}
		if($scope.d_oil_brend.length == 0){
			$http.get('/api/oil-brend').then(function(succes){
				if(succes.status == 200){
					$scope.d_oil_brend = succes.data;
				}},function(err){console.log('d_oil_brend',err);})
		}
		if($scope.d_oil_title.length == 0){
			$http.get('/api/oil-title').then(function(succes){
				if(succes.status == 200){
					$scope.d_oil_title = succes.data;
				}},function(err){console.log('d_oil_title',err);})
		}
		if($scope.d_battery_article.length == 0){
			$http.get('/api/battery-article').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_article = succes.data;
				}},function(err){console.log('d_battery_article',err);})
		}
		if($scope.d_battery_capacity.length == 0){
			$http.get('/api/battery-capacity').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_capacity = succes.data;
				}},function(err){console.log('d_battery_capacity',err);})
		}
		if($scope.d_battery_current.length == 0){
			$http.get('/api/battery-current').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_current = succes.data;
				}},function(err){console.log('d_battery_current',err);})
		}
		if($scope.d_battery_polarity.length == 0){
			$http.get('/api/battery-polarity').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_polarity = succes.data;
				}},function(err){console.log('d_battery_polarity',err);})
		}
		if($scope.d_battery_brend.length == 0){
			$http.get('/api/battery-brend').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_brend = succes.data;
				}},function(err){console.log('d_battery_brend',err);})
		}
		if($scope.d_battery_title.length == 0){
			$http.get('/api/battery-title').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_title = succes.data;
				}},function(err){console.log('d_battery_title',err);})
		}
		if($scope.d_battery_length.length == 0){
			$http.get('/api/battery-length').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_length = succes.data;
				}},function(err){console.log('d_battery_length',err);})
		}
		if($scope.d_battery_width.length == 0){
			$http.get('/api/battery-width').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_width = succes.data;
				}},function(err){console.log('d_battery_width',err);})
		}
		if($scope.d_battery_height.length == 0){
			$http.get('/api/battery-height').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_height = succes.data;
				}},function(err){console.log('d_battery_height',err);})
		}
		if($scope.d_battery_country.length == 0){
			$http.get('/api/battery-country').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_country = succes.data;
				}},function(err){console.log('d_battery_country',err);})
		}
		if($scope.d_battery_assurance.length == 0){
			$http.get('/api/battery-assurance').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_assurance = succes.data;
				}},function(err){console.log('d_battery_assurance',err);})
		}
		if($scope.d_battery_voltage.length == 0){
			$http.get('/api/battery-voltage').then(function(succes){				
				if(succes.status == 200){
					$scope.d_battery_voltage = succes.data;
				}},function(err){console.log('d_battery_voltage',err);})
		}
	}
	// END FILTER SHINY
	// FILTER DISC

	// END FILTER DISC
	$scope.clearInput = function() {
		$scope.$broadcast('simple-autocomplete:clearInput');
		$scope.initialShiny();
		$scope.createorder.querySearch();
	};
/****AUTOCOMPLETE MODULE END****/
	$scope.createorder = {
		positions : [],
		page:1,
		pages:1,
		category:'shiny',
		search:'',
    addItems(){
      $scope.pagemodal.setTitle('Выберите категорию!');
      $scope.pagemodal.show();
      $scope.createorder.loadList();
    },
    pageBack:function(){
    	$scope.createorder.page--;
      $scope.createorder.loadList();
    },
		pageForward:function(){
    	$scope.createorder.page++;
      $scope.createorder.loadList();
		},
		firstPage:function(){
			return $scope.createorder.page == 1;
		},
		lastPage:function(){
			return $scope.createorder.page == $scope.createorder.pages;
		},
		loadList:function(){
			$http.get($scope.createorder.getUrl()).then(function(succes){
				if(succes.status == 200){
					$scope.createorder.positions = succes.data.items;
					$scope.createorder.pages = succes.data.pages;
				}
			});
			console.log('loadList');
			$scope.loadAutocomplit();
		},
		querySearch:function(){
			$scope.createorder.page = 1;
			$http.get($scope.createorder.getUrl()).then(function(succes){
				if(succes.status == 200){
					$scope.createorder.positions = succes.data.items;
					$scope.createorder.pages = succes.data.pages;
				}
			})
		},
		setCategory(category){
			$scope.createorder.category = category;
			$scope.clearInput();
			$scope.createorder.loadList();
		},
		isActive:function(category){
			if($scope.createorder.category == category)
				return 'btn-primary';
			return 'btn-default';
		},
		getUrl:function(){
			// Общий 
			var url = '/api/'+$scope.createorder.category+'-all?page='+$scope.createorder.page;
			if($scope.createorder.search.length > 3){
				url += '&search='+$scope.createorder.search;
			}
			$scope.s_ostatok ? url += '&ostatok=4' : url += '&ostatok=0';
			// Параметры для шин
			if($scope.s_shiny_season_sun){url += '&sun=Летние';}
			if($scope.s_shiny_season_win){url += '&win=Зимние';}
			if($scope.s_shiny_season_full){url += '&full=Всесезонные';}
			if($scope.s_shiny_ship){url += '&ship=1';}
			if($scope.s_shiny_ship){url += '&ship=1';}
			if($scope.s_sklad){url += '&sklad=1';}
			if($scope.s_shiny_width != null){url += '&width='+$scope.s_shiny_width.width;}
			if($scope.s_shiny_height != null){url += '&height='+$scope.s_shiny_height.height;}
			if($scope.s_shiny_radius != null){url += '&radius='+$scope.s_shiny_radius.radius;}
			if($scope.s_shiny_brend != null){url += '&brend='+$scope.s_shiny_brend.title;}
			if($scope.s_shiny_title != null){url += '&title='+$scope.s_shiny_title.title;}
			if($scope.s_shiny_loads != null){url += '&loads='+$scope.s_shiny_loads.loads;}
			if($scope.s_shiny_speed != null){url += '&speed='+$scope.s_shiny_speed.speed;}
			if($scope.s_shiny_typeTC != null){url += '&typeTC='+$scope.s_shiny_typeTC.typeTC;}
			if($scope.s_shiny_vendor != null){url += '&vendor='+$scope.s_shiny_vendor.vendor;}
			if($scope.s_shiny_car != null){url += '&car='+$scope.s_shiny_car.car;}
			if($scope.s_shiny_year != null){url += '&year='+$scope.s_shiny_year.year;}
			// Параметры для дисков
			if($scope.s_disc_size_r != null){url += '&size_r='+$scope.s_disc_size_r.size_r;}
			if($scope.s_disc_size_w != null){url += '&size_w='+$scope.s_disc_size_w.size_w;}
			if($scope.s_disc_borer_c != null){url += '&borer_c='+$scope.s_disc_borer_c.borer_c;}
			if($scope.s_disc_borer_d != null){url += '&borer_d='+$scope.s_disc_borer_d.borer_d;}
			if($scope.s_disc_stupica != null){url += '&stupica='+$scope.s_disc_stupica.stupica;}
			if($scope.s_disc_radius != null){url += '&radius='+$scope.s_disc_radius.radius;}
			if($scope.s_disc_brend != null){url += '&brend='+$scope.s_disc_brend.brend;}
			if($scope.s_disc_title != null){url += '&title='+$scope.s_disc_title.title;}
			if($scope.s_disc_type != null){url += '&type='+$scope.s_disc_type.type;}
			if($scope.s_disc_color != null){url += '&color='+$scope.s_disc_color.color;}
			// Параметры для масла
			if($scope.s_oil_viscosity_1 != null){url += '&viscosity_1='+$scope.s_oil_viscosity_1.viscosity_1;}
			if($scope.s_oil_viscosity_2 != null){url += '&viscosity_2='+$scope.s_oil_viscosity_2.viscosity_2;}
			if($scope.s_oil_type_fluid != null){url += '&type_fluid='+$scope.s_oil_type_fluid.type_fluid;}
			if($scope.s_oil_size != null){url += '&size='+$scope.s_oil_size.size;}
			if($scope.s_oil_type_engine != null){url += '&type_engine='+$scope.s_oil_type_engine.type_engine;}
			if($scope.s_oil_brend != null){url += '&brend='+$scope.s_oil_brend.title;}
			if($scope.s_oil_title != null){url += '&title='+$scope.s_oil_title.title;}
			// Параметры для аккумуляторов
			if($scope.s_battery_article != null){url += '&article='+$scope.s_battery_article.article}
			if($scope.s_battery_capacity != null){url += '&capacity='+$scope.s_battery_capacity.capacity}
			if($scope.s_battery_current != null){url += '&current='+$scope.s_battery_current.current}
			if($scope.s_battery_polarity != null){url += '&polarity='+$scope.s_battery_polarity.polarity}
			if($scope.s_battery_brend != null){url += '&brend='+$scope.s_battery_brend.title}
			if($scope.s_battery_title != null){url += '&title='+$scope.s_battery_title.title}
			if($scope.s_battery_length != null){url += '&length='+$scope.s_battery_length.length}
			if($scope.s_battery_width != null){url += '&width='+$scope.s_battery_width.width}
			if($scope.s_battery_height != null){url += '&height='+$scope.s_battery_height.height}
			if($scope.s_battery_country != null){url += '&country='+$scope.s_battery_country.country}
			if($scope.s_battery_assurance != null){url += '&assurance='+$scope.s_battery_assurance.assurance}
			if($scope.s_battery_voltage != null){url += '&voltage='+$scope.s_battery_voltage.voltage}
			// Параметры для дисков
			return url;
		}
	}

	$scope.basket = {
		timer : null,
		positions : [],
		updatePositions : [],
    load:false,
    credit:false,
		delivery: $scope.getItem('delivery','samovyvos'),
		city:'Алматинская область',
		type:'Post Exspres',   
    payment:'Наличными',
    deliveryTotal:0,
    priceTotal:0,
    delivery_label:{
      samovyvos:'Самовывоз',
      city_square:'По Алматы - 2 000тг.',
      region:'В другие города',
      type:'Сервис доставки',
      city:'Выберите город',
    },
    delivery_time:{
    	'Актау':'от 5 до 6 дней',
			'Актобе':'от 5 до 6 дней',
			'Алматинская область':'от 6 до 8 дней',
			'Астана':'от 2 до 3 дней',
			'Атырау':'от 5 до 6 дней',
			'Балхаш':'от 2 до 3 дней',
			'Жезказган':'от 5 до 6 дней',
			'Караганда':'от 2 до 3 дней',
			'Кокшетау':'от 3 до 4 дней',
			'Костанай':'от 3 до 4 дней',
			'Кызылорда':'от 3 до 5 дней',
			'Павлодар':'от 4 до 5 дней',
			'Петропавловск':'от 4 до 5 дней',
			'Семей':'от 4 до 5 дней',
			'Талдыкорган':'2 дня',
			'Тараз':'2 дня',
			'Темиртау':'от 2 до 3 дней',
			'Уральск':'от 5 до 6 дней',
			'Усть-Каменогорск':'от 4 до 5 дней',
			'Шымкент':'от 2 до 3 дней',
			'Экибастуз':'от 4 до 5 дней',
    },    
    delivery_option:{
      samovyvos:0,
      city_square:2000,
      region:[
      	{title:'Актау','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Актобе','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Алматинская область','Post Exspres':1300,DPD:1500,srok:'от 6 до 8 дней'},
				{title:'Астана','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Атырау','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Балхаш','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Жезказган','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Караганда','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Кокшетау','Post Exspres':1300,DPD:1500,srok:'от 3 до 4 дней'},
				{title:'Костанай','Post Exspres':1300,DPD:1500,srok:'от 3 до 4 дней'},
				{title:'Кызылорда','Post Exspres':1300,DPD:1500,srok:'от 3 до 5 дней'},
				{title:'Павлодар','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Петропавловск','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Семей','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Талдыкорган','Post Exspres':1300,DPD:1500,srok:'2 дня'},
				{title:'Тараз','Post Exspres':1300,DPD:1500,srok:'2 дня'},
				{title:'Темиртау','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Уральск','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Усть-Каменогорск','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Шымкент','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Экибастуз','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
      ],
    },
    setLoadStatus:function(){
    	return $scope.basket.positions.length == 0;
    },
		total:function(){	
			$window.localStorage.setItem('delivery',$scope.basket.delivery)
			$scope.basket.priceTotal = 0;
			$scope.basket.deliveryTotal = 0;	
			for (i in $scope.basket.positions){
				if($scope.basket.credit){
					$scope.basket.priceTotal += $scope.basket.positions[i].old_price * $scope.basket.positions[i].count;
					$scope.basket.payment = 'В кредит';
				}else{					
					$scope.basket.priceTotal += $scope.basket.positions[i].price * $scope.basket.positions[i].count;
					$scope.basket.payment = 'Наличными';
				}if($scope.basket.delivery == 'region')
					$scope.basket.deliveryTotal += $scope.basket.delivery_option.region[0][$scope.basket.type] * $scope.basket.positions[i].count;
			}
			if($scope.basket.delivery != 'region' && $scope.basket.priceTotal <= 50000)
				$scope.basket.deliveryTotal += $scope.basket.delivery_option[$scope.basket.delivery];
			return $scope.basket.priceTotal;
		},
		putCart:function(id,cat,quantity=1){
			$http({method: 'GET', url: '/api/basket-put',params:{id:id,cat:cat,quantity:quantity}}).
			then(function(success) {
				if(success.status == 200){
					$scope.basket.positions = success.data;
					$scope.basket.showNotification('bottom','right');					
				}
			},
			function(err){console.log('putCart',err)});
		},
		addCart:function(id,cat,quantity=1){
			$http({method: 'GET', url: '/api/order-put',params:{id:id,cat:cat,quantity:quantity}}).
			then(function(success) {
				if(success.status == 200){
					$scope.basket.updatePositions.push(success.data);
					// $scope.basket.showNotification('bottom','right');					
				}
			},
			function(err){console.log('putCart',err)});
		},
		showNotification: function(from, align){
    	color = Math.floor((Math.random() * 4) + 1);
    	$.notify({
        	icon: "add_shopping_cart",
        	message: "Товар <b>Успешно</b> добавлен в корзину."

        },{
            type: type[color],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
		},
		deleteCart:function(position){
			$http({method: 'GET', url: '/api/basket-delete',params:{position:position}}).
			then(function(success) {
				if(success.status == 200){
					$scope.basket.positions = success.data;
					
				}
			},
			function(err){console.log(err)});
		},
		updateCart:function(item){
			if(parseInt(item.ostatok) > 0 && parseInt(item.count) >= parseInt(item.ostatok)){
				item.count = parseInt(item.ostatok)					
			}
			if(parseInt(item.count) > parseInt(item.ostatok))
				return
			if($scope.basket.timer != null){
				clearTimeout($scope.basket.timer);				
			}
			$scope.basket.timer = setTimeout(function(){
				if(item.count > 0)
					$http({method: 'GET', url: '/api/basket-update',params:item});
			},300)			
		},
		up:function(item,quantity = 1){
			if(parseInt(item.count) >= parseInt(item.ostatok))
				return
			item.count = parseInt(item.count) + quantity;
			$scope.basket.updateCart(item);
		},
		down:function(item,quantity = 1){
			item.count = parseInt(item.count) - quantity;
			$scope.basket.updateCart(item);
		}
	};
})