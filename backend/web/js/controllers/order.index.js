app.controller("orderList",function($scope,$window){
	var pagemodal = $('#myModal');
	var modal_title = $('.modal-title');
	var modal_body = $('.modal-body');
	var modal_footer = $('.modal-footer');
	$scope.items = {
		check:false,
		closed:false,
		execute:function(json,credit){
			var body='<table class="table table-hover">'+
      '<thead class="text-warning">'+
      '<tr><th>Наименование</th>'+
    	'<th>Кол.</th>'+
    	'<th>Цена</th>'+
    	'<th>Сумма</th>'+
      '</tr></thead>'+
      '<tbody>';
      var total = 0;
			for(var i in json){
				if(credit){					
					total += json[i].count * json[i].old_price;
					body += '<tr><td>'+json[i].title+'</td><td>'+json[i].count+'</td><td>'+$scope.t(json[i].old_price)+'</td><td>'+$scope.t(json[i].count * json[i].old_price)+'</td></tr>';
				}
				else{					
					total += json[i].count * json[i].price;
					body += '<tr><td>'+json[i].title+'</td><td>'+json[i].count+'</td><td>'+$scope.t(json[i].price)+'</td><td>'+$scope.t(json[i].count * json[i].price)+'</td></tr>';				
				}
			}   
			body += '<tr><th colspan="3">Итого</th><th>'+$scope.t(total)+'</th></tr>';   
			body += '</tbody></table>';
			$scope.pagemodal.setTitle('Список товаров!');
			$scope.pagemodal.setBody(body);
			$scope.pagemodal.show();
		},
		checkbox:function(){
			$window.location.search = $window.location.search+'&SearchOrder%5Blist%5D='+$scope.items.check;
		},
		boxclosed:function(){
			if(!$scope.items.closed)
				$window.location.search = $window.location.search+'&SearchOrder%5Bclosed%5D=true';
			else
				$window.location.search = $window.location.search+'&SearchOrder%5Bclosed%5D=false';
		}
	}
})