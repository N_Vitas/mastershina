// $("#phone").mask("+77777777777",{placeholder:" "});
app.controller('BasketForm',function($scope,$http){
  $scope.timer = null;
	$scope.orderform = {
    send:false,
		name:"",
		email:"",
		phone:"",
		address:"",
    comment:"",
    client:false,
    theme:0,
    clIndex:0,
    selectedClient:0,
    clients:[{name:"Контрагент не выбран"}],
    fClients:{
      name:[],
      email:[],
      phone:[],
      address:[],
    },
    errors:{
      name:true,
      email:true,
      phone:true,
      address:true,
    },
    themes:[
      {id:0,value:'Шины'},
      {id:1,value:'Диски'},
      {id:2,value:'Стационарный шиномонтаж'},
      {id:3,value:'Выездной шиномонтаж'},
      {id:4,value:'Сезонное хранение'},
      {id:5,value:'Бухгалтерия'},
      {id:6,value:'Аккумулятор'},
      {id:7,value:'в кредит и рассрочку консультация'},
      {id:8,value:'Вопросы не по существу'},
      {id:9,value:'Годовой абонемент в рассрочку'},
      {id:10,value:'Грузовые шины'},
      {id:11,value:'Диски и Шины'},
      {id:12,value:'Как проехать'},
      {id:13,value:'Консультация клиента'},
      {id:14,value:'Масла и жидкости'},
      {id:15,value:'Нет В наличии'},
      {id:16,value:'Переадресация на другого менеджера'},
      {id:17,value:'Повторный звонок'},
      {id:18,value:'СВП - система выравнивания плитки'},
      {id:19,value:'Служебный звонок'},
      {id:20,value:'Тендер'},
      {id:21,value:'Утилизация'}
    ],
    validateForm:function(){
      // console.log('validateForm', $scope.orderform.errors);
      if($scope.basket.delivery == 'samovyvos')
        $scope.orderform.errors.address = true;
      else{
        $scope.orderform.errors.address = $scope.orderform.address.length > 6
      }
      if($scope.orderform.errors.email.length > 0){
        $scope.orderform.errors.email = /^([\w+\.+\-+]{3,})@([\w+]{3,})\.([\w+]{2,3})$/g.test($scope.orderform.email)
      }else{
        $scope.orderform.errors.email = true;
      }
      $scope.orderform.errors.phone = /^(\+7)?([\d]{7,11})$/.test($scope.orderform.phone)
      $scope.orderform.errors.name = $scope.orderform.name.length > 1
      if($scope.orderform.errors.address && $scope.orderform.errors.name && $scope.orderform.errors.email && $scope.orderform.errors.phone)
        return true
      else
        return false;
    },
		sendBasketForm:function(){
      if($scope.orderform.send){
        return;
      }
      if($scope.orderform.validateForm()){
        $scope.basket.load = true;
        $scope.orderform.send = true;
				var data = $("#basketForm").serializeArray();
				console.log(data)
				$.ajax({
					type:'POST',
					url:'/api/order-create',
					data:data
				}).done(function(data){
					if(data.result){
            $scope.basket.positions = [];
            window.location.pathname = "/order/index"
          }
				});
			}
		},
    createClient:function(){
      $scope.orderform.name = "";
      $scope.orderform.email = "";
      $scope.orderform.phone = "";
      $scope.orderform.address = "";
      $scope.orderform.client = false;
    },
    changeTheme:function(value){
      // console.log('changeClient',$scope.orderform.theme);
      // $scope.orderform.theme = $scope.orderform.theme;
    },
    changeClient:function(){
      $scope.orderform.clients.filter(function(item){
        if(item.id === $scope.orderform.selectedClient){
          $scope.orderform.name = item.name;
          $scope.orderform.email = item.email;
          $scope.orderform.phone = item.phone;
          $scope.orderform.address = item.address;
          return false;        
        }
        return false;
      })
      $scope.orderform.validateForm();
    },
    loadClient:function(){
      $.ajax({
        type:'GET',
        url:'/api/clients',
      }).done(function(data){
        if(data.length > 0){
          $scope.orderform.clients = data;
          // $scope.orderform.selectedClient = $scope.orderform.clients[0];
          $scope.orderform.client = true;
        }
      });
    },
    setClient:function(item){
      $scope.orderform.name = item.name;
      $scope.orderform.email = item.email;
      $scope.orderform.phone = item.phone;
      $scope.orderform.address = item.address;
      $scope.orderform.fClients = {name:[],email:[],phone:[],address:[]};
      $scope.orderform.validateForm();
    },
    searchClient:function(search=null,params='name'){
      if (search) {
        $scope.orderform.fClients[params] = $scope.orderform.clients.filter(function(item){
          var lowerCaseOption = search.toString().toLowerCase();
          var lowerCaseTerm = item[params].toString().toLowerCase();
          return lowerCaseTerm.indexOf(lowerCaseOption) != -1;
        });
      }else{
        $scope.orderform.fClients = {name:[],email:[],phone:[],address:[]};
      }
    },
    changename:function(){
      $scope.orderform.searchClient($scope.orderform.name,'name');
      $scope.orderform.validateForm();
    },
    changemail:function(){
      $scope.orderform.searchClient($scope.orderform.email,'email');
      $scope.orderform.validateForm();
    },
    changephone:function(){
      $scope.orderform.searchClient($scope.orderform.phone,'phone');
      $scope.orderform.validateForm();
    },
    changeaddress:function(){
      $scope.orderform.validateForm();
    },
	}
});

app.directive('changename', function () {
  return {
    require: 'ngModel',
     	link: function(scope, elm, attrs, ctrl) {     		
        ctrl.$parsers.unshift(function(data) {
          ctrl.$setValidity('changename', data.length >= 5);
          return data        
        });
    }
  };
});