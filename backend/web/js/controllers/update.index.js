app.controller('Update',function($scope, $http, $timeout, fileReader){
    $scope.obj = "shina";
    $scope.xml = {
        shina: [],
        disk: [],
        maslo: [],
        akum: [],
        shinapage: 0,
        diskpage: 0,
        maslopage: 0,
        akumpage: 0,
        limit: 15,
        search: '',
        progress: 0,
    };
    // Парсим файл импорта
    $scope.xml.init = function(xml = 'import.xml') {
        const x2js = new X2JS();
    	$('.loader').show();
        $.ajax({
			type:'GET',
            url:`/import/xml/${xml}`,
            dataType: 'XML',  
            success: function(data) {
                const xmlText = data; // XML
                $scope.each(x2js.xml2json(xmlText));
            } 
		}).fail(function(data){
            console.log(data)
            $scope.obj = "shina";
            $('.loader').hide();		
		});
    };
    $scope.xml.togleProduct = function(product) {
        $scope.xml[$scope.obj] = $scope.xml[$scope.obj].map(function(item) {
            if (item.model === product.model) {
                item.send = !product.send;
            }
            return item;
        });
    }
    $scope.xml.deleteProduct = function(product) {
        $scope.xml[$scope.obj] = $scope.xml[$scope.obj].filter(function(item) {
            return item.model !== product.model
        });
    }
    $scope.xml.changeProduct = function(product, callback) {
        if (typeof callback === 'function') {
            callback(product);
        }
    }
    $scope.xml.next = function() {
        if ($scope.xml[`${$scope.obj}page`] >= $scope.xml[$scope.obj].length) {
            return
        }
        if ($scope.xml[`${$scope.obj}page`] <= 0) {
            $scope.xml[`${$scope.obj}page`] = ($scope.xml[`${$scope.obj}page`] + 1) + $scope.xml.limit - 1
        } else {
            $scope.xml[`${$scope.obj}page`] = $scope.xml[`${$scope.obj}page`] + $scope.xml.limit;
        }
        // $scope.xml.clear()
    }
    $scope.xml.back = function() {
        if ($scope.xml[`${$scope.obj}page`] <= 0) {
            $scope.xml[`${$scope.obj}page`] = 0
        } else {
            $scope.xml[`${$scope.obj}page`] = $scope.xml[`${$scope.obj}page`] - $scope.xml.limit;
        }
        // $scope.xml.clear()
    }
    $scope.xml.navigation = function() {
        const pages = Math.ceil($scope.xml[$scope.obj].length / $scope.xml.limit);
        const page = Math.ceil($scope.xml[`${$scope.obj}page`] / $scope.xml.limit) + 1;
        if (page >= pages) {
            return `Показано ${pages} из ${pages} ст.`;
        }
        return `Показано ${page} из ${pages} ст.`;
    }
    $scope.xml.category = function(obj = 'shina') {
        $scope.obj = obj;
    }
    $scope.xml.clear = function() {
        $scope.xml.search = '';
    }
    $scope.xml.getProgress = function() {
        return `${$scope.xml.progress}%`;
    }
    // Распределение товаров по категории
	$scope.each = function(xmlText){
        const { offers: { offer } } = xmlText.shop;
        if (Array.isArray(offer)) {
            for(let product of offer){
                // console.log(product)
                for (let param of product.param){
                    switch(param._name) {
                        case 'Сезонность': 
                            $scope.xml.shina.push({...product, send: true})
                            break;
                        case 'Сверловка': 
                            $scope.xml.disk.push({...product, send: true})
                            break;
                        case 'ТипЖидкости':
                            $scope.xml.maslo.push({...product, send: true})
                            break;
                        case 'Ёмкость':
                            $scope.xml.akum.push({...product, send: true})
                            break;
                        default:
                    }	
                }
            }
        }
        $scope.xml.category();
        $scope.$apply();
        $('.loader').hide();
    }
    $scope.xml.alert = function() {
        $('#updateModal').modal('show')
    }
    $scope.xml.remove = function(product) {
      const filter = function(item){
        return product !== item;
      }
      $scope.xml.shina = $scope.xml.shina.filter(filter.bind(this))
      $scope.xml.disk = $scope.xml.disk.filter(filter.bind(this))
      $scope.xml.maslo = $scope.xml.maslo.filter(filter.bind(this))
      $scope.xml.akum = $scope.xml.akum.filter(filter.bind(this))
    }
    $scope.xml.upload = function() {
        const total = $scope.xml.akum.concat( $scope.xml.maslo, $scope.xml.shina, $scope.xml.disk);
        const count = total.length;
        const progressBar = function(index = 0) {
            if (index < count) {
                $.ajax({
                    type:'POST',
                    url:'/update/create',
                    dataType: 'JSON',
                    data: total[index],
                    success: function(data) {
                      if(data.success) {
                        $scope.xml.remove(total[index]);
                      }
                      $scope.xml.progress = Math.round((index / count) * 100)
                      $timeout(function(){progressBar(index + 1)});

                    } 
                }).fail(function(data){
                  console.log(data)
                  $scope.xml.progress = Math.round((index / count) * 100)
                  $timeout(function(){progressBar(index + 1)});	
                });
            }else{
              $scope.xml.progress = 0;
            }
        }
        progressBar();
    }
    $scope.images = { loading: false, model: null, files: { file: null, data: null } };
    $scope.images.init = function(model) {
        $scope.images.model = model;
        $scope.images.show();
    }
    $scope.images.show = function() {
        $('#editImageModal').modal('show')
    }
    $scope.images.change = function(e) {
      $scope.images.files.data = null;
      const timeout = function() {
        if ($scope.images.model && $scope.images.files.data) {
            $scope.xml[$scope.obj].map(item => {
                if (item.model === $scope.images.model.model) {
                    item.picture = $scope.images.files.data;
                }
                return item;
            });
            $scope.$apply();
            return;
        }
        $timeout(timeout, 250)
      }
      timeout();
    }
    $scope.images.upload = function() {
      if ($scope.images.files.data && !$scope.images.loading) {
        $scope.images.loading = true;
        var form = new FormData();
        form.append("ImagesFile[file]", $scope.images.files.file);
        $http.post('/api/upload-image', form, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        }).then(function (res) {
          if(res.data.result){
            $scope.xml[$scope.obj].map(item => {
              if (item.model === $scope.images.model.model) {
                item.picture = `/upload/${$scope.images.files.file.name}`;
              }
              return item;
            });
            $scope.images.files.file = null;
            $scope.images.files.data = null;
            $scope.images.loading = false;
            $scope.$apply();
          }
        });
      }
    }
})

app.directive("ngFileSelect", function(fileReader, $timeout) {
    return {
      scope: {
        ngModel: '='
      },
      link: function($scope, el) {
        function getFile(file) {
          fileReader.readAsDataUrl(file, $scope)
            .then(function(result) {
              $timeout(function() {
                $scope.ngModel = { file, data: result };
              });
            });
        }

        el.bind("change", function(e) {
          var file = (e.srcElement || e.target).files[0];
          getFile(file);
        });
      }
    };
  });

app.factory("fileReader", function($q, $log) {
  const onLoad = function(reader, deferred, scope) {
    return function() {
      scope.$apply(function() {
        deferred.resolve(reader.result);
      });
    };
  };

  const onError = function(reader, deferred, scope) {
    return function() {
      scope.$apply(function() {
        deferred.reject(reader.result);
      });
    };
  };

  const onProgress = function(reader, scope) {
    return function(event) {
      scope.$broadcast("fileProgress", {
        total: event.total,
        loaded: event.loaded
      });
    };
  };

  const getReader = function(deferred, scope) {
    var reader = new FileReader();
    reader.onload = onLoad(reader, deferred, scope);
    reader.onerror = onError(reader, deferred, scope);
    reader.onprogress = onProgress(reader, scope);
    return reader;
  };

  const readAsDataURL = function(file, scope) {
    var deferred = $q.defer();

    var reader = getReader(deferred, scope);
    reader.readAsDataURL(file);

    return deferred.promise;
  };

  return {
    readAsDataUrl: readAsDataURL
  };
});