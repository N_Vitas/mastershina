// $("#phone").mask("+77777777777",{placeholder:" "});
app.controller('BasketForm',function($scope,$http,$window){
  $scope.timer = null;
	$scope.orderform = {
    initial:function(model){
      $scope.orderform.id = model.id;
      $scope.orderform.name = model.name;
      $scope.orderform.email = model.email;
      $scope.orderform.phone = model.phone;
      $scope.orderform.address = model.address;
      $scope.orderform.payment = model.payment;
      $scope.orderform.credit =  model.payment != 'Наличными'; 
      switch(model.delivery){
        case $scope.orderform.delivery_label.samovyvos:
          $scope.orderform.delivery = 'samovyvos';
          break;
        case $scope.orderform.delivery_label.city_square:
          $scope.orderform.delivery = 'city_square';
          break;
        case $scope.orderform.delivery_label.region:
          $scope.orderform.delivery = 'region';
          break;
        default:
          $scope.orderform.delivery = 'samovyvos';
          break;
      }   
      if(model.comment != null){
        $scope.orderform.comment = model.comment[model.comment.length-1].comment;
      }
      $scope.orderform.client = model.client;
      $scope.orderform.theme = parseInt(model.theme);
      $scope.basket.updatePositions = model.data;
      for (i in $scope.basket.updatePositions){
        $scope.orderform.updateCart($scope.basket.updatePositions[i]);
      }
      console.log(model)
    },
    /************************/
    timer : null,
    positions : [],
    load:false,
    credit:false,
    delivery: $scope.getItem('delivery','samovyvos'),
    city:'Алматинская область',
    type:'Post Exspres',   
    payment:'Наличными',
    deliveryTotal:0,
    priceTotal:0,
    delivery_label:{
      samovyvos:'Самовывоз',
      city_square:'По Алматы - 2 000тг.',
      region:'В другие города',
      type:'Сервис доставки',
      city:'Выберите город',
    },
    delivery_time:{
      'Актау':'от 5 до 6 дней',
      'Актобе':'от 5 до 6 дней',
      'Алматинская область':'от 6 до 8 дней',
      'Астана':'от 2 до 3 дней',
      'Атырау':'от 5 до 6 дней',
      'Балхаш':'от 2 до 3 дней',
      'Жезказган':'от 5 до 6 дней',
      'Караганда':'от 2 до 3 дней',
      'Кокшетау':'от 3 до 4 дней',
      'Костанай':'от 3 до 4 дней',
      'Кызылорда':'от 3 до 5 дней',
      'Павлодар':'от 4 до 5 дней',
      'Петропавловск':'от 4 до 5 дней',
      'Семей':'от 4 до 5 дней',
      'Талдыкорган':'2 дня',
      'Тараз':'2 дня',
      'Темиртау':'от 2 до 3 дней',
      'Уральск':'от 5 до 6 дней',
      'Усть-Каменогорск':'от 4 до 5 дней',
      'Шымкент':'от 2 до 3 дней',
      'Экибастуз':'от 4 до 5 дней',
    },    
    delivery_option:{
      samovyvos:0,
      city_square:2000,
      region:[
        {title:'Актау','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
        {title:'Актобе','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
        {title:'Алматинская область','Post Exspres':1300,DPD:1500,srok:'от 6 до 8 дней'},
        {title:'Астана','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
        {title:'Атырау','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
        {title:'Балхаш','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
        {title:'Жезказган','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
        {title:'Караганда','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
        {title:'Кокшетау','Post Exspres':1300,DPD:1500,srok:'от 3 до 4 дней'},
        {title:'Костанай','Post Exspres':1300,DPD:1500,srok:'от 3 до 4 дней'},
        {title:'Кызылорда','Post Exspres':1300,DPD:1500,srok:'от 3 до 5 дней'},
        {title:'Павлодар','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
        {title:'Петропавловск','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
        {title:'Семей','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
        {title:'Талдыкорган','Post Exspres':1300,DPD:1500,srok:'2 дня'},
        {title:'Тараз','Post Exspres':1300,DPD:1500,srok:'2 дня'},
        {title:'Темиртау','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
        {title:'Уральск','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
        {title:'Усть-Каменогорск','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
        {title:'Шымкент','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
        {title:'Экибастуз','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
      ],
    },
    setLoadStatus:function(){
      return $scope.basket.updatePositions.length == 0;
    },
    total:function(){ 
      $window.localStorage.setItem('delivery',$scope.orderform.delivery)
      $scope.orderform.priceTotal = 0;
      $scope.orderform.deliveryTotal = 0;  
      for (i in $scope.basket.updatePositions){
        if($scope.orderform.credit){
          $scope.orderform.priceTotal += $scope.basket.updatePositions[i].old_price * $scope.basket.updatePositions[i].count;
          $scope.orderform.payment = 'В кредит';
        }else{          
          $scope.orderform.priceTotal += $scope.basket.updatePositions[i].price * $scope.basket.updatePositions[i].count;
          $scope.orderform.payment = 'Наличными';
        }if($scope.orderform.delivery == 'region')
          $scope.orderform.deliveryTotal += $scope.orderform.delivery_option.region[0][$scope.orderform.type] * $scope.basket.updatePositions[i].count;
      }
      if($scope.orderform.delivery != 'region' && $scope.orderform.priceTotal <= 50000)
        $scope.orderform.deliveryTotal += $scope.orderform.delivery_option[$scope.orderform.delivery];
      return $scope.orderform.priceTotal;
    },
    deleteCart:function(position){
      $scope.basket.updatePositions = $scope.basket.updatePositions.filter(function(item){
        return item.position != position;
      });
    },
    updateCart:function(item){
      if(parseInt(item.ostatok) > 0 && parseInt(item.count) >= parseInt(item.ostatok)){
        item.count = parseInt(item.ostatok)         
      }
      if(parseInt(item.count) > parseInt(item.ostatok))
        return
      if($scope.orderform.timer != null){
        clearTimeout($scope.orderform.timer);        
      }
      // $scope.orderform.timer = setTimeout(function(){
      //   if(item.count > 0)
      //     $http({method: 'GET', url: '/api/basket-update',params:item});
      // },300)      
    },
    up:function(item,quantity = 1){
      if(parseInt(item.count) >= parseInt(item.ostatok))
        return
      item.count = parseInt(item.count) + quantity;
      $scope.orderform.updateCart(item);
    },
    down:function(item,quantity = 1){
      item.count = parseInt(item.count) - quantity;
      $scope.orderform.updateCart(item);
    },
    /************************/
    id:0,
		name:"",
		email:"",
		phone:"",
		address:"",
    comment:"",
    client:false,
    theme:0,
    clIndex:0,
    selectedClient:0,
    clients:[{name:"Контрагент не выбран"}],
    fClients:{
      name:[],
      email:[],
      phone:[],
      address:[],
    },
    errors:{
      name:true,
      email:true,
      phone:true,
      address:true,
    },
    themes:[
      {id:0,value:'Шины'},
      {id:1,value:'Диски'},
      {id:2,value:'Стационарный шиномонтаж'},
      {id:3,value:'Выездной шиномонтаж'},
      {id:4,value:'Сезонное хранение'},
      {id:5,value:'Бухгалтерия'},
      {id:6,value:'Аккумулятор'},
      {id:7,value:'в кредит и рассрочку консультация'},
      {id:8,value:'Вопросы не по существу'},
      {id:9,value:'Годовой абонемент в рассрочку'},
      {id:10,value:'Грузовые шины'},
      {id:11,value:'Диски и Шины'},
      {id:12,value:'Как проехать'},
      {id:13,value:'Консультация клиента'},
      {id:14,value:'Масла и жидкости'},
      {id:15,value:'Нет В наличии'},
      {id:16,value:'Переадресация на другого менеджера'},
      {id:17,value:'Повторный звонок'},
      {id:18,value:'СВП - система выравнивания плитки'},
      {id:19,value:'Служебный звонок'},
      {id:20,value:'Тендер'},
      {id:21,value:'Утилизация'}
    ],
    validateForm:function(){
      // console.log('validateForm', $scope.orderform.errors);
      if($scope.orderform.delivery == 'samovyvos')
        $scope.orderform.errors.address = true;
      else{
        $scope.orderform.errors.address = $scope.orderform.address.length > 6
      }
      if($scope.orderform.errors.email.length > 0){
        $scope.orderform.errors.email = /^([\w+\.+\-+]{3,})@([\w+]{3,})\.([\w+]{2,3})$/g.test($scope.orderform.email)
      }else{
        $scope.orderform.errors.email = true;
      }
      $scope.orderform.errors.phone = /^(\+7)?([\d]{7,11})$/.test($scope.orderform.phone)
      $scope.orderform.errors.name = $scope.orderform.name.length > 4
      if($scope.orderform.errors.address && $scope.orderform.errors.name && $scope.orderform.errors.email && $scope.orderform.errors.phone)
        return true
      else
        return false;
    },
		sendBasketForm:function(){
      if($scope.orderform.validateForm()){
        $scope.orderform.load = true;
				var data = $("#basketForm").serializeArray();
				console.log(data)
				$.ajax({
					type:'POST',
					url:'/api/order-update',
					data:data
				}).done(function(data){
					if(data.result){
            $scope.basket.updatePositions = [];
            window.location.pathname = "/order/index"
          }
				});
			}
		},
    createClient:function(){
      $scope.orderform.name = "";
      $scope.orderform.email = "";
      $scope.orderform.phone = "";
      $scope.orderform.address = "";
      $scope.orderform.client = false;
    },
    changeTheme:function(value){
      // console.log('changeClient',$scope.orderform.theme);
      // $scope.orderform.theme = $scope.orderform.theme;
    },
    changeClient:function(){
      $scope.orderform.clients.filter(function(item){
        if(item.id === $scope.orderform.selectedClient){
          $scope.orderform.name = item.name;
          $scope.orderform.email = item.email;
          $scope.orderform.phone = item.phone;
          $scope.orderform.address = item.address;
          return false;        
        }
        return false;
      })
      $scope.orderform.validateForm();
    },
    loadClient:function(){
      $.ajax({
        type:'GET',
        url:'/api/clients',
      }).done(function(data){
        if(data.length > 0){
          $scope.orderform.clients = data;
          // $scope.orderform.selectedClient = $scope.orderform.clients[0];
          $scope.orderform.client = true;
        }
      });
    },
    setClient:function(item){
      $scope.orderform.name = item.name;
      $scope.orderform.email = item.email;
      $scope.orderform.phone = item.phone;
      $scope.orderform.address = item.address;
      $scope.orderform.fClients = {name:[],email:[],phone:[],address:[]};
      scope.orderform.validateForm();
    },
    searchClient:function(search=null,params='name'){
      if (search) {
        $scope.orderform.fClients[params] = $scope.orderform.clients.filter(function(item){
          var lowerCaseOption = search.toString().toLowerCase();
          var lowerCaseTerm = item[params].toString().toLowerCase();
          return lowerCaseTerm.indexOf(lowerCaseOption) != -1;
        });
      }else{
        $scope.orderform.fClients = {name:[],email:[],phone:[],address:[]};
      }
    },
    changename:function(){
      let scope = $scope;
      $scope.orderform.searchClient($scope.orderform.name,'name');
      if(scope.timer != null){
        clearTimeout(scope.timer);
      }
      $scope.timer = setTimeout(function(){scope.orderform.validateForm()},300);
    },
    changemail:function(){
      let scope = $scope;
      $scope.orderform.searchClient($scope.orderform.email,'email');
      if(scope.timer != null){
        clearTimeout(scope.timer);
      }
      scope.timer = setTimeout(function(){scope.orderform.validateForm()},300);
    },
    changephone:function(){
      let scope = $scope;
      $scope.orderform.searchClient($scope.orderform.phone,'phone');
      if(scope.timer != null){
        clearTimeout(scope.timer);
      }
      scope.timer = setTimeout(function(){scope.orderform.validateForm()},300);
    },
    changeaddress:function(){
      let scope = $scope;
      if(scope.timer != null){
        clearTimeout(scope.timer);
      }
      scope.timer = setTimeout(function(){scope.orderform.validateForm()},300);
    },
	}
});