app.controller('Test',function($scope,$http){
  $scope.selection = true;
  $scope.selected = [];  
  $scope.finished = [];
  $scope.updateMore = [];
  $scope.selectUpdateClient = "";
  $scope.cr = {
		open:false,
		view:{},
    clients:[{name:"Контрагент не выбран"}],
    client:false,
    client_id:0,
    auto:[],
    selectedAuto:null,
		n_inumber:null,
		n_model:null,
		n_size:null,
		n_inumber_err:false,
		n_model_err:false,
		n_size_err:false,
    fClients:{
      name:[],
      phone:[],
      address:[],
    },
    errors:{
      name:true,
      phone:true,
      address:true,
      validate:[],
    },
		getStatus:function(){
			if($scope.model.type == "stacionar"){
				return 'Занято'
			}else{
				return 'ОжидаетПодтверждения'
			}
		},
    clearClient:function(){
    	$scope.cr.fClients = {name:[],phone:[],address:[]};
    },
    validateForm:function(){
      $scope.cr.errors.phone = /^(\+7)?([\d]{7,11})$/.test($scope.sendData.phone)
      $scope.cr.errors.name = $scope.sendData.name.length > 1
      if($scope.cr.errors.address && $scope.cr.errors.name && $scope.cr.errors.phone)
        return true
      else
        return false;
    },
    searchClient:function(search=null,params='name'){
      if (search) {
        $scope.cr.fClients[params] = $scope.cr.clients.filter(function(item){
          var lowerCaseOption = search.toString().toLowerCase();
          var lowerCaseTerm = item[params].toString().toLowerCase();
          return lowerCaseTerm.indexOf(lowerCaseOption) != -1;
        });
      }else{
        $scope.cr.fClients = {name:[],phone:[],address:[]};
      }
    },
    changename:function(){
      $scope.cr.searchClient($scope.sendData.name,'name');
      $scope.cr.validateForm();
    },
    changephone:function(){
      $scope.cr.searchClient($scope.sendData.phone,'phone');
      $scope.cr.validateForm();
    },
    changeaddress:function(){
      $scope.cr.validateForm();
    },    
    loadClient:function(){
    	$('.loader').show();
    	$http({
			  method: 'GET',
			  url: '/api/clients',
			  headers: {
					'Content-Type': 'json',
				},
			}).then(function (response) {
        if(response.data.length > 0){
          $scope.cr.clients = response.data;
          $scope.cr.client = true;
					$('.loader').hide();	
        }				
	    }).catch(function(error) {
				console.log(error);
				$('.loader').hide();
    	});
    },
    modelSelect:function(){
      $scope.sendData.inumber = $scope.cr.selectedAuto.inumber
			$scope.sendData.model = $scope.cr.selectedAuto.model
			$scope.sendData.size = $scope.cr.selectedAuto.size
    },
    setClient:function(item){
    	console.log(item)
      $scope.sendData.name = item.name;
      $scope.sendData.phone = item.phone;
      $scope.sendData.address = item.address;
      if(item.tiresinstorage.toLowerCase() != 'нет')
      	$scope.sendData.storage = true;
    	else
      	$scope.sendData.storage = false;
      if(item.tc != null && item.tc.length > 0){
      	$scope.cr.auto = item.tc
	      $scope.sendData.inumber = item.tc[0]['inumber']
				$scope.sendData.model = item.tc[0]['model']
				$scope.sendData.size = item.tc[0]['size']
				$scope.cr.selectedAuto = item.tc[0]
      }else{
      	$scope.cr.auto = []
	      $scope.sendData.inumber = "не выбран"
				$scope.sendData.model = "не выбран"
				$scope.sendData.size = "не выбран"
				$scope.cr.selectedAuto = null
      }
      $scope.cr.fClients = {name:[],phone:[],address:[]};
      $scope.cr.validateForm();
    },
    getStorage:function(){    	
    	return $scope.sendData.storage ? 'Да' : 'Нет' ;
    },
    addAuto:function(){
    	if($scope.cr.n_inumber == null || $scope.cr.n_inumber.length < 5){
    		$scope.cr.n_inumber_err = true
    		return
    	}else{
    		$scope.cr.n_inumber_err = false
    	}
			if($scope.cr.n_model == null || $scope.cr.n_model.length < 3){
				$scope.cr.n_model_err = true
				return
			}else{
				$scope.cr.n_model_err = false
			}
			if($scope.cr.n_size == null || $scope.cr.n_size.length < 5){
				$scope.cr.n_size_err = true
				return
			}else{
				$scope.cr.n_size_err = false
			}
			var newauto = {
				inumber:$scope.cr.n_inumber,
				model:$scope.cr.n_model,
				size:$scope.cr.n_size,					
			}
			$scope.cr.n_inumber = null
			$scope.cr.n_model = null
			$scope.cr.n_size = null
			$scope.sendData.inumber = newauto.inumber
			$scope.sendData.model = newauto.model
			$scope.sendData.size = newauto.size
			$scope.cr.selectedAuto = newauto
			$scope.cr.auto.push(newauto)
    },
  };
  $scope.sendData = {
  	author:null,
		meneger_crm:null,
		confirmed:null,
		status:null,
		storage:null,
		comment:null,
    inumber:"не выбран",
		model:"не выбран",
		size:"не выбран",
		address:null,
		company:null,
		name:null,
		phone:null
  };
	$scope.model = {
		list:[],
		page:0,
		stacionar:{title:"Стационар",crm:"СШМ",sun:[],mon:[],tue:[],wed:[],thu:[],fri:[],sat:[]},
		exitday:{title:"Выездной дневной",crm:"ВШМ дневной",sun:[],mon:[],tue:[],wed:[],thu:[],fri:[],sat:[]},
		exitnight:{title:"Выездной круглосуточный",crm:"ВШМ круглосуточно",sun:[],mon:[],tue:[],wed:[],thu:[],fri:[],sat:[]},
		type:"stacionar",
		types:["stacionar","exitday","exitnight"],
		days:["mon","tue","wed","thu","fri","sat","sun"],
		day:"mon",
		style:{
			create:"btn-default",
			update:"btn-default",
			delete:"btn-default",
			change:"btn-default",
			preview:"btn-default",
			next:"btn-info",
		},
		method:null,
		init:function(data){
				var s = $scope; 
				if(s.model.page > 0){
					s.model.style.preview = "btn-info"
				}else{					
					s.model.style.preview = "btn-default"
				}
				// $.getJSON( "/data.json", function( data ) {
				var stacionar = s.filterType(data,"СШМ");
				var exitday   = s.filterType(data,"ВШМ дневной");
				var exitnight = s.filterType(data,"ВШМ круглосуточно");

				s.model.stacionar.mon = s.cocktailSort(s.filterDay(stacionar,1));
				s.model.stacionar.tue = s.cocktailSort(s.filterDay(stacionar,2));
				s.model.stacionar.wed = s.cocktailSort(s.filterDay(stacionar,3));
				s.model.stacionar.thu = s.cocktailSort(s.filterDay(stacionar,4));
				s.model.stacionar.fri = s.cocktailSort(s.filterDay(stacionar,5));
				s.model.stacionar.sat = s.cocktailSort(s.filterDay(stacionar,6));
				s.model.stacionar.sun = s.cocktailSort(s.filterDay(stacionar,0));

				s.model.exitday.mon = s.cocktailSort(s.filterDay(exitday,1));
				s.model.exitday.tue = s.cocktailSort(s.filterDay(exitday,2));
				s.model.exitday.wed = s.cocktailSort(s.filterDay(exitday,3));
				s.model.exitday.thu = s.cocktailSort(s.filterDay(exitday,4));
				s.model.exitday.fri = s.cocktailSort(s.filterDay(exitday,5));
				s.model.exitday.sat = s.cocktailSort(s.filterDay(exitday,6));
				s.model.exitday.sun = s.cocktailSort(s.filterDay(exitday,0));

				s.model.exitnight.mon = s.cocktailSort(s.filterDay(exitnight,1));
				s.model.exitnight.tue = s.cocktailSort(s.filterDay(exitnight,2));
				s.model.exitnight.wed = s.cocktailSort(s.filterDay(exitnight,3));
				s.model.exitnight.thu = s.cocktailSort(s.filterDay(exitnight,4));
				s.model.exitnight.fri = s.cocktailSort(s.filterDay(exitnight,5));
				s.model.exitnight.sat = s.cocktailSort(s.filterDay(exitnight,6));
				s.model.exitnight.sun = s.cocktailSort(s.filterDay(exitnight,0));
				s.model.day = s.getNowDay();
				  // s.model.list = sun;
				// })		
		}
	}
	$scope.getList = function(){
		// $scope.cr.loadClient()
		// $scope.model.init(json.list)
		var url = "/tirefitting/index?page="+$scope.model.page;
		$http({
		  method: 'GET',
		  url: url,
		  headers: {
				'Content-Type': 'json',
			},
		}).then(function (response) {
			$scope.model.init(response.data.list);
  		$('.loader').hide();
    }).catch(function(error) {
			console.log(error);
  		$('.loader').hide();
  	});
	};
	$scope.nextPage = function(){
  	$('.loader').show();
		$scope.model.page = $scope.model.page+1;
		var url = "/tirefitting/index?page="+$scope.model.page;
		$http({
		  method: 'GET',
		  url: url,
		  headers: {
				'Content-Type': 'json',
			},
		}).then(function (response) {
			$scope.model.init(response.data.list);
			$('.loader').hide();	
    }).catch(function(error) {
			console.log(error);
			$('.loader').hide();	
  	});
	}
	$scope.prevPage = function(){
  	$('.loader').show();
		$scope.model.page = $scope.model.page-1;
		var url = "/tirefitting/index?page="+$scope.model.page;
		$http({
		  method: 'GET',
		  url: url,
		  headers: {
				'Content-Type': 'json',
			},
		}).then(function (response) {
			$scope.model.init(response.data.list);
			$('.loader').hide();	
    }).catch(function(error) {
			console.log(error);
  		$('.loader').hide();
  	});
	}
	// Функция подготовки данных к отправке создания обновления или удаления
	$scope.prepareData = function(){
		var c = 0;
		var s = $scope;
		var h = false;
		var d = null;
		var index = []
		s.model.style.create = "btn-default";
		s.model.style.change = "btn-default";
		s.model.style.update = "btn-default";
		s.model.style.delete = "btn-default";
		s.model.method = null;
		s.changeList = [];
		if(s.finished.length > 0){
			for(i in s.finished) {
				if(d == null){
					r = moment(s.finished[i].time, "DD.MM.YYYY H:m:s")
					d	= r.format("ddd").toLowerCase()	
				}				
				if(s.finished[i].name != null){
					c++;
					if(s.model.type != "stacionar"){
						s.model.stacionar[d].filter(function(item){							
							if(item.time == s.finished[i].time){
								h = true;
								if(index.indexOf("stacionar") == -1){
									index.push("stacionar");
									s.changeList.push({type:"stacionar",crm:s.model.stacionar.crm,title:s.model.stacionar.title})
								}
							}
							return false
						})
					}
					if(s.model.type != "exitday"){
						s.model.exitday[d].filter(function(item){							
							if(item.time == s.finished[i].time){
								h = true;
								if(index.indexOf("exitday") == -1){
									index.push("exitday");
									s.changeList.push({type:"exitday",crm:s.model.exitday.crm,title:s.model.exitday.title})
								}
							}
							return false
						})
					}
					if(s.model.type != "exitnight"){
						s.model.exitnight[d].filter(function(item){							
							if(item.time == s.finished[i].time){
								h = true;
								if(index.indexOf("exitnight") == -1){
									index.push("exitnight");
									s.changeList.push({type:"exitnight",crm:s.model.exitnight.crm,title:s.model.exitnight.title})
								}
							}
							return false
						})
					}
				}
			}
			if(h)
				s.model.style.change = "btn-success";
			if(c > 0){
				s.model.style.update = "btn-success";
				s.model.style.delete = "btn-danger";
				s.model.method = "update";
			}else{
				s.model.style.create = "btn-success";	
				s.model.method = "create";
			}
		}
	}
	$scope.getDayTitle = function(day,obj){
		if(obj != undefined){
			d = moment(obj.time,"DD.MM.YYYY HH:mm:ss");
			if(d.isValid()){
				var days = {
					mon:"Пн "+d.format("DD.MM.YYYY"),
					tue:"Вт "+d.format("DD.MM.YYYY"),
					wed:"Ср "+d.format("DD.MM.YYYY"),
					thu:"Чт "+d.format("DD.MM.YYYY"),
					fri:"Пт "+d.format("DD.MM.YYYY"),
					sat:"Сб "+d.format("DD.MM.YYYY"),
					sun:"Вс "+d.format("DD.MM.YYYY")
				}
				return days[day];					
			}
		}
		var days = {
			mon:"Понедельник",
			tue:"Вторник",
			wed:"Среда",
			thu:"Четверг",
			fri:"Пятница",
			sat:"Суббота",
			sun:"Воскресенье"
		}
		return days[day];			
	}
	$scope.getNowDay = function(){
		d = moment();
		return $scope.model.days[d.day() != 0 ? d.day()-1 : 6];
	}
	$scope.setDay = function(day){
		$scope.model.day = day;
	}
	$scope.setType = function(type){
		$scope.model.type = type;
		// localStorage.setItem('type',type)
	}
	$scope.filterType = function(data,type){
		return data.filter(function(item){return item.box.indexOf(type) != -1;})
	}
	$scope.filterDay = function(data,day){
		return data.filter(function(item){
			d = moment(item.time, "DD.MM.YYYY H:m:s")
			return d.day() == day;
		})
	}
	$scope.selectionStart = function(){
  };
  $scope.selectionStop = function(selected){
  	var index = [];
  	$scope.finished = selected.filter(function(item){
  		if(index.indexOf(item.time) == -1){
  			index.push(item.time);
				return $scope.wronckdate(item,$scope.model.day,$scope.model.type) != 'bussy';  		  			
  		}
  		return false;
  	});

  	$scope.prepareData()
  };
  $scope.wronckdate = function(item,weekend,type){
  	d = moment(item.time, "DD.MM.YYYY H:m:s")
  	if(moment().isAfter(d))
  		return 'bussy';
  	if(type != 'exitnight' && (weekend == 'sat' || weekend == 'sun')){
	  	var fir = d.get('hour') < 10;
	  	var las = d.get('hour') > 17;
	  	if(las || fir)
  			return 'bussy';

	  	if(item.name != null){
	  		if($scope.model.type == 'stacionar')
	  			return 'success';
	  		if(item.confirmed == 'Нет' || item.confirmed == 'нет'){
	  			return 'warning';
	  		}
	  		return 'success';
	  	}
  		return;  		
  	}
  	if(item.name != null){
  		if($scope.model.type == 'stacionar')
  			return 'success';
  		if(item.confirmed == 'Нет' || item.confirmed == 'нет'){
  			return 'warning';
  		}
  		return 'success';
  	}
  	return
  };
  // Открытие формы для создания
  $scope.openFormCreate = function(){
  	if($scope.model.style.create != "btn-default"){
	  	$("#serviceCreate").modal('show');
			$('.modal-backdrop').hide();  		
  	}
  }
  // Отправка формы создания
	$scope.createTir = function(){
		var m = $('#tirefitForm').serializeArray()
		var s = $scope.sendData;
		for(i in m){
			s[m[i]['name']] = m[i]['value'];
		}
		$scope.cr.errors.validate = []
		if(s.inumber == "не выбран")
			$scope.cr.errors.validate.push("Гос. номер отсутствует")
		if(s.model == "не выбран")
			$scope.cr.errors.validate.push("Модель автомобиля отсутствует")
		if(s.size == "не выбран")
			$scope.cr.errors.validate.push("Размер шин отсутствует")
		if(s.name.length == 0)
			$scope.cr.errors.validate.push("Имя не должно быть пустым")
		if(s.phone.length == 0)
			$scope.cr.errors.validate.push("Телефон не должен быть пустым")
		if(s.address.length == 0 && $scope.model.type != "stacionar")
			$scope.cr.errors.validate.push("Адрес не должен быть пустым")
		if($scope.cr.errors.validate.length > 0)			
			return
		// После проверки обьединяем выбраный перриод с данными
		for (var i = 0; i < $scope.finished.length; i++) {
			$scope.finished[i].author = s.author;
			$scope.finished[i].meneger_crm = s.meneger_crm;
			$scope.finished[i].confirmed = s.confirmed;
			$scope.finished[i].status = s.status;
			$scope.finished[i].storage = s.storage;
			$scope.finished[i].comment = s.comment;
			$scope.finished[i].inumber = s.inumber;
			$scope.finished[i].model = s.model;
			$scope.finished[i].size = s.size;
			$scope.finished[i].address = s.address;
			$scope.finished[i].company = s.company;
			$scope.finished[i].name = s.name;
			$scope.finished[i].phone = s.phone;
		}
		var dataList = {list:$scope.finished};
		$('.loader').show();
		$.ajax({
			type:'POST',
			url:'/tirefitting/create',
			dataType: "json",
			data:dataList
		}).done(function(data){
			if(data.status == 'ОК'){
				$scope.getList();
				$("#serviceCreate").modal('hide');
			}else{
				$('.loader').hide();
				$scope.cr.errors.validate = data
			}
		}).fail(function(data){
			$('.loader').hide();				
			$scope.cr.errors.validate = data
			console.log(data);
		});
	}
	// Проверка на разницу клиентов перед обновлением
	$scope.updateConfirm = function(){	
		var index = [];	
		$scope.updateMore = $scope.finished.filter(function(item){
			if(item.name != null && index.indexOf(item.name) == -1){
  			index.push(item.name);
				return true;
			}
			return false
		});
		if($scope.updateMore.length == 0){
			return		
		}
		if($scope.updateMore.length > 1){
			$("#serviceUpdate").modal("show");		
			$('.modal-backdrop').hide();
			return		
		}
		if($scope.updateMore.length == 1){
			$scope.selectUpdateClient = $scope.updateMore[0];
			$scope.updateTir();
		}
	}
	// Продление записи
	$scope.updateTir = function(){
		var s = $scope.selectUpdateClient;
		if(s != null){			
			for (var i = 0; i < $scope.finished.length; i++) {
				$scope.finished[i].author = s.author;
				$scope.finished[i].meneger_crm = s.meneger_crm;
				$scope.finished[i].confirmed = s.confirmed;
				$scope.finished[i].status = s.status;
				$scope.finished[i].storage = s.storage;
				$scope.finished[i].comment = s.comment;
				$scope.finished[i].inumber = s.inumber;
				$scope.finished[i].model = s.model;
				$scope.finished[i].size = s.size;
				$scope.finished[i].address = s.address;
				$scope.finished[i].company = s.company;
				$scope.finished[i].name = s.name;
				$scope.finished[i].phone = s.phone;
			}
			s = null;
  		$scope.updateMore = [];
			var dataList = {list:$scope.finished};
			$('.loader').show();
			$.ajax({
				type:'POST',
				url:'/tirefitting/update',
				dataType: "json",
				data:dataList
			}).done(function(data){
				if(data.status == 'ОК'){
					$scope.getList();
					$("#serviceUpdate").modal('hide');
				}else{
					$('.loader').hide();
					$scope.cr.errors.validate = data
				}
			}).fail(function(data){
			$('.loader').hide();			
				$scope.cr.errors.validate = data
				console.log(data);
			});
		}
	}
	// Подтверждение удаления
	$scope.deleteConfirm = function(){
		var deleteData = $scope.finished.filter(function(item){
			return item.name != null;
		});
		if(deleteData.length == 0){
			return		
		}
		$("#serviceDelete").modal("show");		
		$('.modal-backdrop').hide();
	}
	// Удаление записей
	$scope.deleteTir = function(){
		var deleteData = $scope.finished.filter(function(item){
			return item.name != null;
		});
		$('.loader').show();
		var dataList = {list:deleteData};
		$.ajax({
			type:'POST',
			url:'/tirefitting/delete',
			dataType: "json",
			data:dataList
		}).done(function(data){
			if(data.status == 'ОК'){
				$scope.getList();
				$("#serviceDelete").modal('hide');
			}else{
				$('.loader').hide();
				$scope.cr.errors.validate = data
			}
		}).fail(function(data){
			$('.loader').hide();				
			$scope.cr.errors.validate = data
			console.log(data);
		});
	}
	// Формат времени
	$scope.formatDate = function(time){
		d = moment(time,"DD.MM.YYYY H:mm:ss");
		if(d.isValid()){
			return d.format("HH:mm");
		}
		return time;
	}
	// Формат даты
	$scope.formatDateTime = function(time){
		d = moment(time,"DD.MM.YYYY H:mm:ss");
		if(d.isValid()){
			return d.format("DD.MM.YYYY H:mm:ss");
		}
		return time;
	}
	// Подробная информация
	$scope.openView = function(item){
		if(item.name != null){
			$scope.cr.view = item;
			$scope.cr.view.time = $scope.formatDateTime(item.time);
			$('#serviceViev').modal('show')
			$('.modal-backdrop').hide();
		}
	}
	// Шейкерная сортировка обьектов по дате
	$scope.cocktailSort = function(A)    //Также называют ShakerSort.
	{
    var i = 0, j = A.length-1, s = true, t;
    // console.log('До сортировки',A)
    // Тут смотрим признак выхода и если начало стало больше конца масива
    while (i < j && s)
    { 
    	s = false; // готовим выход из цикла
      // Проходим от начала в конец
      for (var k = i; k < j; k++){
      	// Используя впомогательную функцию смотрим надо ли менять местами обьекты
      	if($scope.isAfterTime(A[k]['time'],A[k+1]['time'])){ //A[k] > A[k+1]
      		// console.log('Заменили с начала в конец',A[k]['time'],A[k+1]['time'])
      		t = A[k]; // Создаем копию обьекта
      		A[k] = A[k+1]; // Переписываем обьект следующим по очереди
      		A[k+1] = t; // Переписываем следующий обьект созданой копией
      		s = true; // Отменяем выход из цикла
      	}
      }
      // Уменьшаем конец масива на одну позицию и забываем про последнюю
      j--;
      // Если есть выход из цикла то завершаем сортировку
      if (s){
      	s = false; // готовим выход из цикла
      	// проходим с конца в начало масива
				for (var k = j; k > i; k--){
					// Используя впомогательную функцию смотрим надо ли менять местами обьекты
					if($scope.isAfterTime(A[k-1]['time'],A[k]['time'])){ //A[k] < A[k-1]
      			// console.log('Заменили с конца в начало',A[k]['time'],A[k-1]['time'])
						t = A[k]; // Создаем копию обьекта
						A[k] = A[k-1]; // Переписываем обьект предыдущим по очереди
						A[k-1] = t; // Переписываем предыдущий обьект созданой копией
						s = true; // Отменяем выход из цикла
					}
				}
      }
      // Увеличиваем начало масива на одну позицию и забываем про предыдущюю
      i++;
   	}
   	// console.log('После сортировки',A)
	   return A;
	}
	// Вспомогательная функция конвертации даты
	$scope.isAfterTime = function(t1,t2){
		t1 = moment(t1, "DD.MM.YYYY H:m:s")
		t2 = moment(t2, "DD.MM.YYYY H:m:s")
		// console.log('isAfterTime',t1.isBefore(t2))
  	return t1.isAfter(t2);
	}
	// Очистка выбранного когда выбирается другой блок
	$scope.clearSelected = function(){
		$("li").removeClass("ui-selected");	
		$("span").removeClass("ui-selected");	
	}
/****************************************************************************/
/*                                                                          */
/* O O O  OO   OO  O O O  O   O  O O O    O O O  O O O  O   O  O O O  O O O */
/* O   O  O O O O  O      O   O  O   O    O      O   O  O   O  O   O  O   O */
/* O      O  O  O  O O    O O O  O O O    O O O  O   O  O O    O      O O O */
/* O   O  O     O  O      O   O  O   O    O   O  O   O  O   O  O   O  O   O */
/* O O O  O     O  O O O  O   O  O   O    O O O  O O O  O   O  O O O  O   O */
/*                                                                          */
/****************************************************************************/
	$scope.changeList = [];
	$scope.selectChangeList = {};
	// функция смены бокса
	$scope.changeBox = function(){
		if($scope.changeList.length > 1){
			$('#serviceChange').modal('show')
			$('.modal-backdrop').hide();
		}
	}
	$scope.changeTir = function(){
		var s = $scope;
		var dataListChange = [];
		var dataListDelete = s.finished.filter(function(item){
			return item.name != null;
		}) 
		for (var i = 0; i < dataListDelete.length; i++) {
			dataListChange[i] = {
				time:dataListDelete[i].time,
				box:s.selectChangeList.crm,
				author:dataListDelete[i].author,
				meneger_crm:dataListDelete[i].meneger_crm,
				confirmed:dataListDelete[i].confirmed,
				status:dataListDelete[i].status,
				storage:dataListDelete[i].storage,
				comment:dataListDelete[i].comment,
				inumber:dataListDelete[i].inumber,
				model:dataListDelete[i].model,
				size:dataListDelete[i].size,
				address:dataListDelete[i].address,
				company:dataListDelete[i].company,
				name:dataListDelete[i].name,
				phone:dataListDelete[i].phone
			}
		}			
		var dataList = {list:dataListChange};
		$('.loader').show();
		$("#serviceChange").modal('hide');
		$.ajax({
			type:'POST',
			url:'/tirefitting/create',
			dataType: "json",
			data:dataList
		}).done(function(data){
			if(data.status == 'ОК'){
				s.deleteTir();
			}else{
				s.openAlert("Запись на выбранный вами бокс уже занята");
				$('.loader').hide();
			}
		}).fail(function(data){
			s.openAlert(data.toString());			
			$('.loader').hide();		
		});
	}
	$scope.openAlert = function(text){
		$('.warning-text').text(text)
		$('.alert-warning').removeClass('hidden')
		setTimeout(function(){
		$('.warning-text').text('')
			$('.alert-warning').addClass('hidden')
		},5000);
	}
})