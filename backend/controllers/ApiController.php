<?php
namespace backend\controllers;

use yii;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\httpclient\Exception;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;
use common\models\Order;
use common\models\ClientApi;
use common\models\Vehicle;
use common\models\Brend;
use common\models\Tirefitting;
use common\models\ImagesFile;
/**
 * Oil controller
 */
class ApiController extends yii\web\Controller
{

  public $sklads = ['МШ Доставка','МШ К какой-то с дисками','МШ К6','МШ с дисками Lenso','МШ С11','МШ С12','МШ С13','МШ С14','МШ С16','МШ С19','МШ С2 клиентский','МШ С20','МШ С3 клиентский','МШ С4','МШ С5','МШ С6','МШ С7','МШ торговый зал'];
  public function behaviors()
  {
      $behaviors = parent::behaviors();
      // $behaviors['authenticator'] = [
      //     'class' => 'frontend\components\HttpHeaderAuth',
      //     'except' => ['index'],
      // ];
      $behaviors['corsFilter'] = [
          'class' => \yii\filters\Cors::className(),
          'cors' => [
              'Origin' => ['*'],
          ],

      ];
      return $behaviors;
  }

  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ]
    ];
  }

  public function init(){
    Yii::$app->response->format = Response::FORMAT_JSON; 
  }

  public function actionShinyAll($page,$search=null,$ostatok=4,$sun=null,$win=null,$full=null,$ship=null,$width=null, $height=null,$radius=null,$brend=null, $title=null, $loads=null, $speed=null, $typeTC=null,$vendor=null,$car=null, $year=null,$sklad=null){
    $start = $page * 10;
    $season = [];
    $models = Shiny::find();
    $models->select(['id','model','price','old_price','ostatok','sklad']);
    $models->where(['>','ostatok',$ostatok]);
    $models->andWhere(['>','price',0]);
    $models->andWhere(['>','old_price',0]);
    // отменить поиск по ширине высоте и диаметру если есть параметры авто
    if($vendor != null || $car != null || $year != null){
      $where = $this->getSearchAutoArray(['vendor'=>$vendor,'car'=>$car,'year'=>$year,'select'=>'tyres_factory']);
      $models->andWhere(['in','width',$where['width']]);
      $models->andWhere(['in','height',$where['height']]);
      $models->andWhere(['in','radius',$where['radius']]);
    }else{
      if($width != null){$models->andWhere(['like','width',$width]);}
      if($height != null){$models->andWhere(['like','height',$height]);}
      if($radius != null){$models->andWhere(['like','radius',$radius]);}
    }
    if($sklad != null){
      $models->andWhere(['in','sklad',$this->sklads]);
    }
    if($brend != null){
      if($id = Brend::find()->where(['title'=>$brend])->one())
        $models->andWhere(['parent_brend_id'=>$id]);
    }
    if($title != null){$models->andWhere(['like','title',$title]);}
    if($loads != null){$models->andWhere(['like','loads',$loads]);}
    if($speed != null){$models->andWhere(['like','speed',$speed]);}
    if($typeTC != null){$models->andWhere(['like','typeTC',$typeTC]);}
    if($search !=null){
      $models->andWhere(['like','model',$search]);
      $models->orWhere(['code'=>$search]);
    }
    if($sun != null){$season[] = "Летние";}
    if($win != null){$season[] = "Зимние";}
    if($full != null){$season[] = "Всесезонные";}
    if($ship !=null){$models->andWhere(['ship'=>1]);}
    if(count($season) > 0){$models->andWhere(['in','season',$season]);}
    $pages = $models;
    $models->offset($start -10);
    $models->limit(10);
    return [
    'pages' => ceil($pages->count() / 10),//intval($pages->count() / 10),
    'items' => $models->all(),
    ];
  }
  public function actionDiscAll($page,$search=null,$ostatok=4,$sklad = null,$size_r=null,$size_w=null,$borer_c=null,$borer_d=null,$stupica=null,$radius=null,$brend=null,$title=null,$type=null,$color=null){
    $start = $page * 10;
    $season = [];
    $models = Disc::find();
    $models->select(['id','model','price','old_price','ostatok','sklad']);
    $models->where(['>','ostatok',0]);
    $models->andWhere(['>','price',0]);
    $models->andWhere(['>','old_price',0]);
    if($search !=null){
      $models->andWhere(['like','model',$search]);   
      $models->orWhere(['code'=>$search]);
    }
    if($sklad != null){
      $models->andWhere(['in','sklad',$this->sklads]);
    }
    if($size_r != null){$models->andWhere(['like','size',$size_r.'x']);}
    if($size_w != null){$models->andWhere(['like','size','x'.$size_w]);}
    if($borer_c != null){$models->andWhere(['like','borer',$borer_c.'x']);}
    if($borer_d != null){$models->andWhere(['like','borer','x'.$borer_d]);}
    if($stupica != null){$models->andWhere(['like','stupica',$stupica]);}
    if($radius != null){$models->andWhere(['like','radius',$radius]);}
    if($brend != null){
      if($id = Brend::find()->where(['title'=>$brend])->one())
        $models->andWhere(['parent_brend_id'=>$id]);
    }
    if($title != null){$models->andWhere(['like','title',$title]);}
    if($type != null){$models->andWhere(['like','type',$type]);}
    if($color != null){$models->andWhere(['like','color',$color]);}
    $pages = $models;
    $models->offset($start -10);
    $models->limit(10);
    return [
    'pages' => ceil($pages->count() / 10),
    'items' => $models->all(),
    ];
  }
  public function actionOilAll($page,$search=null,$ostatok=4,$sklad = null,$viscosity_1=null,$viscosity_2=null,$type_fluid=null,$size=null,$type_engine=null,$brend=null,$title=null){
    $start = $page * 10;
    $season = [];
    $models = Oil::find();
    $models->select(['id','model','price','old_price','ostatok','sklad']);
    $models->where(['>','ostatok',0]);
    $models->andWhere(['>','price',0]);
    $models->andWhere(['>','old_price',0]);
    if($search !=null){
      $models->andWhere(['like','model',$search]);   
      $models->orWhere(['code'=>$search]);
    }
    // if($sklad != null){
    //   $models->andWhere(['in','sklad',$this->sklads]);
    // }
    if($viscosity_1 != null){$models->andWhere(['like','viscosity_1',$viscosity_1]);}
    if($viscosity_2 != null){$models->andWhere(['like','viscosity_2',$viscosity_2]);}
    if($type_fluid != null){$models->andWhere(['like','type_fluid',$type_fluid]);}
    if($size != null){$models->andWhere(['like','size',$size]);}
    if($type_engine != null){$models->andWhere(['like','type_engine',$type_engine]);}
    if($brend != null){
      if($id = Brend::find()->where(['title'=>$brend])->one())
        $models->andWhere(['parent_brend_id'=>$id]);
    }
    if($title != null){$models->andWhere(['like','title',$title]);}   
    $pages = $models;
    $models->offset($start -10);
    $models->limit(10);
    return [
    'pages' => ceil($pages->count() / 10),
    'items' => $models->all(),
    ];
  }
  public function actionBatteryAll($page,$search=null,$ostatok=4,$sklad = null,$article=null,$capacity=null,$current=null,$polarity=null,$brend=null,$title=null,$length=null,$width=null,$height=null,$country=null,$assurance=null,$voltage=null){
    $start = $page * 10;
    $season = [];
    $models = Battery::find();
    $models->select(['id','model','price','old_price','ostatok','sklad']);
    $models->where(['>','ostatok',0]);
    $models->andWhere(['>','price',0]);
    $models->andWhere(['>','old_price',0]);
    if($search !=null){
      $models->andWhere(['like','model',$search]);     
      $models->orWhere(['code'=>$search]);
    }
    // if($sklad != null){
    //   $models->andWhere(['in','sklad',$this->sklads]);
    // }
    if($article != null){$models->andWhere(['like','article',$article]);}
    if($capacity != null){$models->andWhere(['like','capacity',$capacity]);}
    if($current != null){$models->andWhere(['like','current',$current]);}
    if($polarity != null){$models->andWhere(['like','polarity',$polarity]);}
    if($brend != null){
      if($id = Brend::find()->where(['title'=>$brend])->one())
        $models->andWhere(['parent_brend_id'=>$id]);
    }
    if($title != null){$models->andWhere(['like','title',$title]);}
    if($length != null){$models->andWhere(['like','length',$length]);}
    if($width != null){$models->andWhere(['like','width',$width]);}
    if($height != null){$models->andWhere(['like','height',$height]);}
    if($country != null){$models->andWhere(['like','country',$country]);}
    if($assurance != null){$models->andWhere(['like','assurance',$assurance]);}
    if($voltage != null){$models->andWhere(['like','voltage',$voltage]);}
    $pages = $models;
    $models->offset($start -10);
    $models->limit(10);
    return [
    'pages' => ceil($pages->count() / 10),
    'items' => $models->all(),
    ];
  }
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex($params='')
  {   
    if(empty($params))
    return ['site'=>yii::$app->name];
    return ['site'=>yii::$app->name,'params'=>$params];
  }
  public function actionShinyWidth(){
    return Shiny::find()->select(['width'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('width')->all();
  }
  public function actionShinyHeight(){
    return Shiny::find()->select(['height'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('height')->all();
  }
  public function actionShinyRadius(){
    return Shiny::find()->select(['radius'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('radius')->all();
  }
  public function actionShinyBrend(){
    return Shiny::find()->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->joinWith(['brend'])->select(['brend.title'])->groupBy('brend.title')->all();
  }
  public function actionShinyTitle(){
    return Shiny::find()->select(['title'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('title')->all();
  }
  public function actionShinySeason(){
    return Shiny::find()->select(['season'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('season')->all();
  }
  public function actionShinyLoads(){
    return Shiny::find()->select(['loads'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('loads')->all();
  }
  public function actionShinySpeed(){
    return Shiny::find()->select(['speed'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('speed')->all();
  }
  public function actionShinyTypetc(){
    return Shiny::find()->select(['typeTC'])->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->groupBy('typeTC')->all();
  }
  public function actionShinyVendor(){
    return Vehicle::find()->select(['vendor'])->groupBy('vendor')->all();
  }
  public function actionShinyCar(){
    return Vehicle::find()->select(['vendor','car'])->groupBy('car')->all();
  }
  public function actionShinyYear(){
    return Vehicle::find()->select(['year'])->groupBy('year')->all();
  }

  public function actionDiscSizeR(){
    $size = Disc::find()->select(['size'])->where(['!=','size',''])->groupBy('size')->all();
    foreach ($size as $value) {
      $a = explode("x", $value->size);
      if($a[0]=='')
        continue;
      $res[] = ['size_r'=>trim($a[0])];
    }
    $res = array_unique($res,SORT_REGULAR);
    // $res = array_map( 'unserialize', array_unique( 
    //   array_map( 'serialize', $res)
    // ));
    sort($res);
    return $res;
  }
  public function actionDiscSizeW(){
    $size = Disc::find()->select(['size'])->where(['!=','size',''])->groupBy('size')->all();
    foreach ($size as $value) {
      $a = explode("x", $value->size);
      if($a[1]=='')
        continue;
      $res[] = ['size_w'=>trim($a[1])];
    }
    $res = array_unique($res,SORT_REGULAR);
    sort($res);
    return $res;
  }
  public function actionDiscBorerC(){
    $borer = Disc::find()->select(['borer'])->where(['!=','borer',''])->groupBy('borer')->all();
    foreach ($borer as $value) {
      $a = explode("x", $value->borer);
      if($a[0]=='')
        continue;
      $res[] = ['borer_c'=>trim($a[0])];
    }
    $res = array_unique($res,SORT_REGULAR);
    sort($res);
    return $res;
  }
  public function actionDiscBorerD(){
    $borer = Disc::find()->select(['borer'])->where(['!=','borer',''])->groupBy('borer')->all();
    foreach ($borer as $value) {
      $a = explode("x", $value->borer);
      if($a[1]=='')
        continue;
      $res[] = ['borer_d'=>trim($a[1])];
    }
    $res = array_unique($res,SORT_REGULAR);
    sort($res);
    return $res;
  }
  public function actionDiscStupica(){
    return Disc::find()->select(['stupica'])->where(['!=','stupica',''])->groupBy('stupica')->all();
  }
  public function actionDiscRadius(){
    return Disc::find()->select(['radius'])->where(['!=','radius',''])->groupBy('radius')->all();
  }
  public function actionDiscBrend(){
    return Disc::find()->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->joinWith(['brend'])->select(['brend.title'])->groupBy('brend.title')->all();
  }
  public function actionDiscTitle(){
    return Disc::find()->select(['title'])->where(['!=','title',''])->groupBy('title')->all();
  }
  public function actionDiscType(){
    return Disc::find()->select(['type'])->where(['!=','type',''])->groupBy('type')->all();
  }
  public function actionDiscColor(){
    return Disc::find()->select(['color'])->where(['!=','color',''])->groupBy('color')->all();
  }

  public function actionOilViscosity1(){
    return Oil::find()->select(['viscosity_1'])->where(['!=','viscosity_1',''])->groupBy('viscosity_1')->all();
  }
  public function actionOilViscosity2(){
    return Oil::find()->select(['viscosity_2'])->where(['!=','viscosity_2',''])->groupBy('viscosity_2')->all();
  }
  public function actionOilTypeFluid(){
    return Oil::find()->select(['type_fluid'])->groupBy('type_fluid')->all();
  }
  public function actionOilSize(){
    return Oil::find()->select(['size'])->groupBy('size')->all();
  }
  public function actionOilTypeEngine(){
    return Oil::find()->select(['type_engine'])->groupBy('type_engine')->all();
  }
  public function actionOilBrend(){
    return Oil::find()->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->joinWith(['brend'])->select(['brend.title'])->groupBy('brend.title')->all();
  }
  public function actionOilTitle(){
    return Oil::find()->select(['title'])->groupBy('title')->all();
  }
  public function actionBatteryArticle(){
    return Battery::find()->select(['article'])->where(['!=','article',''])->groupBy('article')->all();
  }
  public function actionBatteryCapacity(){
    return Battery::find()->select(['capacity'])->where(['!=','capacity',''])->groupBy('capacity')->all();
  }
  public function actionBatteryCurrent(){
    return Battery::find()->select(['current'])->where(['!=','current',''])->groupBy('current')->all();
  }
  public function actionBatteryPolarity(){
    return Battery::find()->select(['polarity'])->where(['!=','polarity',''])->groupBy('polarity')->all();
  }
  public function actionBatteryBrend(){
    return Battery::find()->where(['>','price',0])->andWhere(['>','ostatok',0])->andWhere(['>','old_price',0])->joinWith(['brend'])->select(['brend.title'])->groupBy('brend.title')->all();
  }
  public function actionBatteryTitle(){
    return Battery::find()->select(['title'])->where(['!=','title',''])->groupBy('title')->all();
  }
  public function actionBatteryLength(){
    return Battery::find()->select(['length'])->where(['!=','length',''])->groupBy('length')->all();
  }
  public function actionBatteryWidth(){
    return Battery::find()->select(['width'])->where(['!=','width',''])->groupBy('width')->all();
  }
  public function actionBatteryHeight(){
    return Battery::find()->select(['height'])->where(['!=','height',''])->groupBy('height')->all();
  }
  public function actionBatteryCountry(){
    return Battery::find()->select(['country'])->where(['!=','country',''])->groupBy('country')->all();
  }
  public function actionBatteryAssurance(){
    return Battery::find()->select(['assurance'])->where(['!=','assurance',''])->groupBy('assurance')->all();
  }
  public function actionBatteryVoltage(){
    return Battery::find()->select(['voltage'])->where(['!=','voltage',''])->groupBy('voltage')->all();
  }
  public function actionClients()
  {   
    return ClientApi::find()->orderBy('name')->all();
  }

  public function actionBasket($params='')
  {
    // Yii::$app->cart->removeAll();   
    // $shiny = Shiny::find()->where(['>','price',0])->limit(6)->all();
    // if($shiny){
    //   foreach ($shiny as $model) {
    //     $i = 1;
    //     Yii::$app->cart->put($model, $i);
    //     if($i > 2)$i=0;
    //     $i++;
    //   }      
    // }
    $basket = [];
    if(Yii::$app->cart->getCount()){
      foreach (Yii::$app->cart->getPositions() as $id => $model) {
        $basket[] = [
          'position' => $id,
          'code' => $model->code,
          'sklad' => $model->sklad, 
          'title' => $model->model,
          'url' => $model->picture,
          'count' => $model->getQuantity(),
          'price' => $model->price,
          'old_price' => $model->old_price,
          'ostatok' => $model->ostatok,
        ];        
      }
    }

    return $basket;
  }

  public function actionBasketPut($id,$cat,$quantity=1)
  {
    if($id && $cat){
      switch ($cat) {
        case 'shiny':
          $model = Shiny::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'disc':
          $model = Disc::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'battery':
          $model = Battery::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'oil':
          $model = Oil::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        
        default:
          $model = false;
          break;
      }
      // $shiny = Shiny::find()->where(['>','price',0])->limit(6)->all();
      if($model){
        Yii::$app->cart->put($model, $quantity);
      }
      $basket = [];
      if(Yii::$app->cart->getCount()){
        foreach (Yii::$app->cart->getPositions() as $id => $model) {
          $basket[] = [
            'position' => $id,
            'code' => $model->code,
            'sklad' => $model->sklad, 
            'title' => $model->model,
            'url' => $model->picture,
            'count' => $model->getQuantity(),
            'price' => $model->price,
            'old_price' => $model->old_price,
            'ostatok' => $model->ostatok,
          ];        
        }
      }

      return $basket;      
    }
    throw new yii\web\HttpException(403,"неверные данные");
    
    // Yii::$app->cart->removeAll();   
  }
  public function actionOrderPut($id,$cat,$quantity=1)
  {
    if($id && $cat){
      switch ($cat) {
        case 'shiny':
          $model = Shiny::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'disc':
          $model = Disc::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'battery':
          $model = Battery::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'oil':
          $model = Oil::find()->select(['id','sklad','code','model','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        
        default:
          $model = false;
          break;
      }
      // $shiny = Shiny::find()->where(['>','price',0])->limit(6)->all();
      $basket = [];
      if($model){
        $basket = [
            'position' => $model->getId(),
            'code' => $model->code,
            'sklad' => $model->sklad, 
            'title' => $model->model,
            'url' => $model->picture,
            'count' => 1,
            'price' => $model->price,
            'old_price' => $model->old_price,
            'ostatok' => $model->ostatok,
          ];
      }
      return $basket;      
    }
    throw new yii\web\HttpException(403,"неверные данные");
    
    // Yii::$app->cart->removeAll();   
  }

  public function actionBasketDelete($position)
  {
    if($position){      
      $basket = [];
      if(Yii::$app->cart->getCount()){
        Yii::$app->cart->removeById($position);
        foreach (Yii::$app->cart->getPositions() as $id => $model) {
          $basket[] = [
            'position' => $id,
            'code' => $model->code,
            'sklad' => $model->sklad, 
            'title' => $model->model,
            'url' => $model->picture,
            'count' => $model->getQuantity(),
            'price' => $model->price,
            'old_price' => $model->old_price,
            'ostatok' => $model->ostatok,
          ];        
        }
      }

      return $basket;      
    }
    throw new yii\web\HttpException(404,"неверные данные");
    
    // Yii::$app->cart->removeAll();   
  }

  public function actionBasketUpdate($position, $count){
    Yii::$app->cart->update(Yii::$app->cart->getPositionById($position),$count);
  }

  public function actionOrderCreate(){      
    $order = new Order();
    $order->attributes = Yii::$app->request->post();
    if($order->comment){
      $order->comment = json_encode([[
        'author'=>$order->author,
        'comment'=>$order->comment,
        'created_at'=>time()
      ]],JSON_UNESCAPED_UNICODE);
    }
    if($order->save()){
      Yii::$app->cart->removeAll();
      return ['result'=>true,'id'=>$order->id];      
    }
    else
      return array_merge(['result'=>false],$order->errors);
  }
  public function actionOrderUpdate(){
    $query = Yii::$app->request->post();
    if($order = Order::findOne($query['id'])){
      $comment = json_decode($order->comment,true);
      $order->attributes = $query;
      if(is_array($comment) && count($comment)){
        $comment[] = ['author'=>$order->author,'comment'=>$order->comment,'created_at'=>time()];
        $order->comment = json_encode($comment,JSON_UNESCAPED_UNICODE);
      }
      if($order->save()){
        return ['result'=>true,'id'=>$order->id];      
      }
      else
        return array_merge(['result'=>false],$order->errors);
    }else{
      return ['result'=>false];
    }
  }
  /**
   * @params tyres_factory поиск стандартных шин
   * @params tyres_replace поиск для замены
   * @params tyres_tuning  поиск для тюнинга
   * Возвращает массив для поиска
   */
  private function getSearchAutoArray($params = ['vendor'=>null,'car'=>null,'year'=>null,'mod'=>null,'select'=>'tyres_factory']){
    $query = [];
    if($params['vendor'] != null){$query['vendor'] = $params['vendor'];}
    if($params['car'] != null){$query['car'] = $params['car'];}
    if($params['year'] != null){$query['year'] = $params['year'];}
    if($params['mod'] != null){$query['modification'] = $params['mod'];}
    if(count($query) == 0){return $query;} // выход если нет условий поиска
    $auto = Vehicle::find();
    $auto->select([$params['select']]);
    $auto->where(['!=',$params['select'],'']);
    $auto->andWhere($query);
    $auto->groupBy($params['select']);
    $auto->asArray();
    if($query = $auto->all()){
      $temp = [];
      foreach ($query as $items) {
        $a = explode("|", $items[$params['select']]);
        $temp = array_merge($a,$temp);
      }
      $b = [];
      foreach ($temp as $items) {
        $a = explode("/", $items);
        $b = array_merge($a,$b);
      }
      $c = [];
      foreach ($b as $items) {
        $a = explode(" ", $items);
        $c = array_merge($a,$c);
      }
      unset($temp,$a,$b); // освобождаем память от мусора      
      $i=0;
      foreach ($c as $item) {
        switch ($i) {
          case 0:
            $where['height'][] = intval(str_replace('R','',$item));
            break;
          case 1:
            $where['radius'][]= intval(str_replace('R','',$item));
            break;
          case 2:
            $where['width'][] = intval(str_replace('R','',$item));
            break;
        }
        $i++;
        if($i == 3){$i=0;}
      }
      return $where;
    }
    return array();
  }

  public function actionSendChecklist(){
    // return yii::getAlias('@ftp');
    if(yii::$app->request->post("id")){
      if($model = Order::find()->where('id=:id',[':id'=>yii::$app->request->post("id")])->asArray()->one()){
        $model["comment"] = json_decode($model["comment"],true);
        $model["data"] = json_decode($model["data"],true);
        $send = Yii::$app->mailer
        ->compose(
            ['html' => 'credit-html'],
            ['model' => $model]
        )
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
        ->setTo($model["email"])
        ->setSubject("Чек-заявка для оформления кредита №".$model["id"])
        ->send();
        return ["result"=>$send]; 
      }
      return ["result"=>false];  
    }
    return ["result"=>false];
  }

  public function actionUploadImage()
  {
    $model = new ImagesFile();
    if (Yii::$app->request->isPost) {
        $model->file = UploadedFile::getInstance($model, 'file');
        if ($model->upload()) {
            // file is uploaded successfully
            return ["result" => true];
        }
    }

    return $model;
  }
}