<?php
namespace backend\controllers;

use Yii;
use backend\models\SearchBattery;
use common\models\Disc;
use common\models\Oil;
use common\models\Battery;
use backend\components\BaseController;

/**
 * Battery controller
 */
class BatteryController extends BaseController
{
  public function actionIndex()
  {
    $searchModel = new SearchBattery();
    $models = $searchModel->search(Yii::$app->request->get());  
    return $this->render('index',compact(['models','searchModel']));
  }
}
