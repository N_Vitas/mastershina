<?php
namespace backend\controllers;

use yii;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\httpclient\Exception;
use yii\helpers\ArrayHelper;
use common\models\Tirefitting;
/**
 * Oil controller
 */
class TirefittingController extends yii\web\Controller
{

  public function behaviors()
  {
      $behaviors = parent::behaviors();
      // $behaviors['authenticator'] = [
      //     'class' => 'frontend\components\HttpHeaderAuth',
      //     'except' => ['index'],
      // ];
      $behaviors['corsFilter'] = [
          'class' => \yii\filters\Cors::className(),
          'cors' => [
              'Origin' => ['*'],
          ],

      ];
      return $behaviors;
  }

  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ]
    ];
  }

  public function init(){
    Yii::$app->response->format = Response::FORMAT_JSON; 
  }

  public function actionIndex(){

    if(yii::$app->request->get('page')){
      $time = strtotime('monday this week '.yii::$app->request->get('page').' week');
      $datetime = date("Y-m-d H:i:s", $time);
    }
    else{
      $time = strtotime('monday this week');
      $datetime = date("Y-m-d H:i:s", $time);
    }
    $tirefitting = $this->findTirefittings($datetime);  
    try{
      $data = $this->queryCRM(yii::$app->params["base_crm_url"].'/ForJivositeMastershina/tire?time='.date("Ymd", $time));
      // return $data;
      if($data){
        $tirefitting->data = json_encode($data,JSON_UNESCAPED_UNICODE);
        $tirefitting->save();
      }      
      return $data;
    }
    catch(Exception $e){
      return $tirefitting->data;
    }
    throw new NotFoundHttpException("Заявки $id не найдено.");      
  }
  
  public function actionPlus(){
    $tirefitting = $this->findTirefittings();  
    if($tirefitting->data){
      $data = $tirefitting->data;
      foreach ($data["list"] as $key => $obj) {
        // 21.05.2017 12:00:00
        $time = strtotime($obj["time"]);
        $newtime = strtotime("+7 day", strtotime($obj["time"]));
        $day = date("d.m.Y H:i:s",$time);
        $newdate = date("d.m.Y H:i:s",$newtime);
        $data["list"][$key]["time"] = $newdate;
      }
      $tirefitting->data = json_encode($data,JSON_UNESCAPED_UNICODE);
      if($tirefitting->save()){
        return $data;        
      }else{
        return $tirefitting->errors;
      }
    }
    return [];
  }

  public function actionCreate(){
    if(!yii::$app->request->post("list")){
      return yii::$app->request->post();
    }    
    $list = yii::$app->request->post("list");
      // var_dump($list[0]);die;
    $client = \common\models\Client::find()
    ->where(['like','name',$list[0]["name"]])
    ->andWhere(['like','phone',substr($list[0]["phone"],2)])
    ->one();
    if($client){
      $tc = json_decode($client->tc,true);
      $client->manager = yii::$app->user->identity->username;
      $contacts = json_decode($client->contacts,true);
      if($client->address != $list[0]["address"]){
        $contacts[0]["address"] = $list[0]["address"];
        $client->address = $list[0]["address"];
        $client->contacts = json_encode($contacts,JSON_UNESCAPED_UNICODE);
        $client->save();       
      }
      $add = false;
      if(is_array($tc)){
        foreach ($tc as $auto) {
          if($auto["inumber"] == $list[0]["inumber"]){
            $add = false;
            break;
          }
          $add=true;
        }
        if($add){
          $tc[] = [
           "inumber" => $list[0]["inumber"],
           "model" => $list[0]["model"],
           "size" => $list[0]["size"],
          ];
          $client->tc = json_encode($tc);
          $client->save();
        }
      }else{
        $tc[] = [
           "inumber" => $list[0]["inumber"],
           "model" => $list[0]["model"],
           "size" => $list[0]["size"],
        ];
        $client->tc = json_encode($tc,JSON_UNESCAPED_UNICODE);
        $client->save();
      }
    }else{
      $client = new \common\models\Client();
      $client->manager = yii::$app->user->identity->username;
      $client->name = $list[0]["name"];
      $client->tiresinstorage = $list[0]["storage"];
      $client->phone = $list[0]["phone"]; 
      $client->address = $list[0]["address"];           
      $contacts[] = [
        'phone'=>$list[0]["phone"],
        'address'=>$list[0]["address"]
      ];
      $tc[] = [
        "inumber" => $list[0]["inumber"],
        "model" => $list[0]["model"],
        "size" => $list[0]["size"],
      ];
      $client->contacts = json_encode($contacts,JSON_UNESCAPED_UNICODE);
      $client->tc = json_encode($tc,JSON_UNESCAPED_UNICODE);   
      $client->save();
    }
    $params = ["list"=>$list];
    try{
      $data = $this->queryCRM(yii::$app->params["base_crm_url"].'/ForJivositeMastershina/tire','post',$params);
      return $data;
    }
    catch(Exception $e){
      return ["status"=>"Error","messages"=>$e->getMessage()];
    }
  }

  public function actionUpdate(){    
    if(!yii::$app->request->post("list")){
      return yii::$app->request->post();
    }    
    $list = yii::$app->request->post("list");
    $params = ["list"=>$list];
    try{
      $data = $this->queryCRM(yii::$app->params["base_crm_url"].'/ForJivositeMastershina/deletetire','post',$params);
      $data = $this->queryCRM(yii::$app->params["base_crm_url"].'/ForJivositeMastershina/tire','post',$params);
      // $data = $this->queryCRM(yii::$app->params["base_crm_url"].'/ForJivositeMastershina/changetire','post',$params);
      return $data;
    }
    catch(Exception $e){
      return ["status"=>"Error","messages"=>$e->getMessage()];
    }
  }

  public function actionDelete(){    
    if(!yii::$app->request->post("list")){
      return yii::$app->request->post();
    }    
    $list = yii::$app->request->post("list");
    $params = ["list"=>$list];
    try{
      $data = $this->queryCRM(yii::$app->params["base_crm_url"].'/ForJivositeMastershina/deletetire','post',$params);
      return $data;
    }
    catch(Exception $e){
      return ["status"=>"Error","messages"=>$e->getMessage()];
    }
  }

  private function findTirefittings($time){
    if(!$model = Tirefitting::find()->where(["time"=>$time])->one()){
      $model = new Tirefitting(); 
    }
    $model->time = $time;
    return $model;
  }

  private function queryCRM($url,$method='get',$params=null){
    $client = new \yii\httpclient\Client();
    if($params != null){
      $response = $client->createRequest()
      ->setMethod($method)
      ->addHeaders(['content-type' => 'application/json'])
      ->setFormat(\yii\httpclient\Client::FORMAT_JSON)
      // ->setOptions(['timeout' => 5])
      ->setUrl($url)
      ->setContent(json_encode($params,JSON_UNESCAPED_UNICODE))
      ->send();  
    }else{      
      $response = $client->createRequest()->setMethod($method)->setOptions(['timeout' => 5])->setUrl($url)->send();
    }
      return $response->data;
    // if($response->isOk) {
    // }else{
    //   return false;
    // }
  }
}