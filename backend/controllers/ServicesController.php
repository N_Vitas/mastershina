<?php
namespace backend\controllers;

use Yii;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use backend\models\SearchOrder;
use common\models\Client;
use common\models\Order;


/**
 * Site controller
 */
class ServicesController extends BaseController
{
  public function actionIndex(){
  	$this->view->RegisterJsFile('/js/moment.js');
  	return $this->render('index');
  }
  public function actionOld(){
  	$this->view->RegisterJsFile('/js/moment.js');
  	return $this->render('old');
  }
}