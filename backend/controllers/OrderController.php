<?php
namespace backend\controllers;

use Yii;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use backend\models\SearchOrder;
use common\models\Client;
use common\models\Order;


/**
 * Site controller
 */
class OrderController extends BaseController
{
  public function actionIndex()
  { 
    $searchModel = new SearchOrder();
    $dataProvider = $searchModel->search(yii::$app->request->get(),yii::$app->user->id);
    return $this->render('index',compact(['searchModel','dataProvider']));
  }
  public function actionCreate()
  {
    return $this->render('create');
  }
  public function actionSuccess()
  { 
    return $this->render('success');
  }
  public function actionUpdate($id)
  { 
    if($model = Order::findOne($id)){
      return $this->render('update',compact('model'));      
    }else{
      throw new NotFoundHttpException("Заявки $id не найдено.");
    }
  }
  public function actionView($id)
  { 
    if($model = Order::findOne($id)){
      if(yii::$app->request->post("comment")){
        $comments = json_decode($model->comment,true);
        $comments[] = ["author"=>yii::$app->request->post("author"),"comment"=>yii::$app->request->post("comment"),"created_at"=>time()];
        $model->comment = json_encode($comments,JSON_UNESCAPED_UNICODE);
        $model->save();
      }
      return $this->render('view',compact('model'));      
    }
    else{
      throw new NotFoundHttpException("Заявки $id не найдено.");
    }
  }
  public function actionCheckList($id){
    $model = Order::find()->where('id=:id',[':id'=>$id])->asArray()->one();
    $model["comment"] = json_decode($model["comment"],true);
    $model["data"] = json_decode($model["data"],true);
    // return $this->render('checklist',compact(['model']));
    $content = $this->renderPartial('checklist',compact(['model']));
    $pdf = Yii::$app->pdf;
    $pdf->content = $content;
    return $pdf->render(); 
  }
}
