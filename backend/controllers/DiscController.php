<?php
namespace backend\controllers;

use Yii;
use backend\models\SearchDisc;
use common\models\Disc;
use common\models\Oil;
use common\models\Battery;
use backend\components\BaseController;

/**
 * Disc controller
 */
class DiscController extends BaseController
{
  public function actionIndex()
  {
    $searchModel = new SearchDisc();
    $models = $searchModel->search(Yii::$app->request->get());  
    return $this->render('index',compact(['models','searchModel']));
  }
}
