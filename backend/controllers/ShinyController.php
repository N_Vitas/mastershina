<?php
namespace backend\controllers;

use Yii;
use backend\models\SearchShiny;
use common\models\Disc;
use common\models\Oil;
use common\models\Battery;
use backend\components\BaseController;

/**
 * Shiny controller
 */
class ShinyController extends BaseController
{
  public function actionIndex()
  {
    $searchModel = new SearchShiny();
    $models = $searchModel->search(Yii::$app->request->get());  
    return $this->render('index',compact(['models','searchModel']));
  }
}
