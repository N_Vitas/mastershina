<?php
namespace backend\controllers;

use Yii;
use backend\components\BaseController;
use backend\models\UploadXml;
use backend\models\CheckFile;
use backend\models\SingletonUpdate;
use yii\web\UploadedFile;
use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;
use common\models\ICatElements;

/**
 * Site controller
 */
class UpdateController extends BaseController
{
  public function actionIndex(){
    $filemtime = 0;
    if (file_exists('import/xml/import.xml')) {
      $filemtime = filemtime('import/xml/import.xml');
    }
    $model = new UploadXml();
    if (Yii::$app->request->isPost) {
        $model->xmlFile = UploadedFile::getInstance($model, 'xmlFile');
        if ($model->upload()) {
            $this->view->RegisterJsFile('/js/xml2json.js');
            return $this->render('progress', ['model' => $model, 'filemtime' => $filemtime]);
        }
        if(Yii::$app->request->post('noFile') && $filemtime){
          $model->xmlFile = new CheckFile('import.xml');
          $this->view->RegisterJsFile('/js/xml2json.js');
          return $this->render('progress', ['model' => $model, 'filemtime' => $filemtime]);
        }
    }
    SingletonUpdate::destroy(Yii::$app->session);
    return $this->render('index', ['model' => $model, 'filemtime' => $filemtime]);
  }

  public function actionCreate() {
    if (Yii::$app->request->isPost) {
      new SingletonUpdate(Yii::$app->session, Yii::$app->db, Yii::$app->db2);
      $model = ICatElements::find()
      ->where(['1c_code' => Yii::$app->request->post('_id')])
      ->orWhere(['1c_code_disk' => Yii::$app->request->post('_id')])
      ->orWhere(['a_1c' => Yii::$app->request->post('_id')])
      ->orWhere(['m_1c' => Yii::$app->request->post('_id')])
      ->one();
      if ($model) {
        $model2 = null;
        switch($model->getType()){
          case ICatElements::SHINY:
            $model->updateShiny(Yii::$app->request->post());
            $model2 = Shiny::find()->where(['code'=>$model['1c_code']])->one();
            if(!$model2){
              $model2 = new Shiny();
            }
            $model2->loadPitstop($model);
            if(!$model2->save(false)){
              return($model2->errors);
            }
            break;
          case ICatElements::DISC:
            $model->updateDisc(Yii::$app->request->post());
            $model2 = Disc::find()->where(['code'=>$model['1c_code_disk']])->one();
            if(!$model2){
              $model2 = new Disc();
            }
            $model2->loadPitstop($model);
            if(!$model2->save(false)){
              return($model2->errors);
            }
            break;
          case ICatElements::OIL:
            $model->updateOil(Yii::$app->request->post());
            $model2 = Oil::find()->where(['code'=>$model->m_1c])->one();
            if(!$model2){
              $model2 = new Oil();
            }
            $model2->loadPitstop($model);
            if(!$model2->save(false)){
              return($model2->errors);
            }
            break;
          case ICatElements::BATTERY:
            $model->updateBattery(Yii::$app->request->post());
            $model2 = Battery::find()->where(['code'=>$model->a_1c])->one();
            if(!$model2){
              $model2 = new Battery();
            }
            $model2->loadPitstop($model);
            if(!$model2->save(false)){
              return($model2->errors);
            }
            break;
        }
        if($model->save(false)) {
          return json_encode(['success' => true, 'model' => $model], JSON_UNESCAPED_UNICODE);
        }
      } else {
        $model = new ICatElements();
        $model->id_section = 1;
        $model->active  = 1;
        $model->version = 'ru';
        $data = Yii::$app->request->post();
        foreach($data as $key => $value){
          if($key === 'param'){
            foreach($value as $k => $val){
              switch($val['_name']) {
                case 'Сезонность':
                  $model->updateShiny(Yii::$app->request->post());
                  $model2 = Shiny::find()->where(['code'=>$model['1c_code']])->one();
                  if(!$model2){
                    $model2 = new Shiny();
                  }
                  $model2->loadPitstop($model);
                  if(!$model2->save(false)){
                    return($model2->errors);
                  }
                  break;
                case 'Сверловка':
                  $model->updateDisc(Yii::$app->request->post());
                  $model2 = Disc::find()->where(['code'=>$model['1c_code_disk']])->one();
                  if(!$model2){
                    $model2 = new Disc();
                  }
                  $model2->loadPitstop($model);
                  if(!$model2->save(false)){
                    return($model2->errors);
                  }
                  break;
                case 'ТипЖидкости':
                  $model->updateOil(Yii::$app->request->post());
                  $model2 = Oil::find()->where(['code'=>$model->m_1c])->one();
                  if(!$model2){
                    $model2 = new Oil();
                  }
                  $model2->loadPitstop($model);
                  if(!$model2->save(false)){
                    return($model2->errors);
                  }
                  break;
                case 'Ёмкость':
                  $model->updateBattery(Yii::$app->request->post());
                  $model2 = Battery::find()->where(['code'=>$model->a_1c])->one();
                  if(!$model2){
                    $model2 = new Battery();
                  }
                  $model2->loadPitstop($model);
                  if(!$model2->save(false)){
                    return($model2->errors);
                  }
                  break;
              }	
            }
          }
        }
        if($model->save(false)) {
          return json_encode(['success' => true, 'model' => $model], JSON_UNESCAPED_UNICODE);
        }
      }
      return json_encode(['error' => 'Не удалось сохранить товар'], JSON_UNESCAPED_UNICODE);
    }
    return json_encode(['error' => 'Разрешен только метод POST'], JSON_UNESCAPED_UNICODE);
  }
}