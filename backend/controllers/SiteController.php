<?php
namespace backend\controllers;

use Yii;
// use yii\web\Controller;

use backend\components\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\SignupForm;
use kartik\mpdf\Pdf;

/**
 * Site controller
 */

class SiteController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTest(){
        $this->view->RegisterJsFile('/js/moment.js');
        return $this->render('test');
    }

    public function actionSignup()
    {
      $model = new SignupForm();
      if ($model->load(Yii::$app->request->post())) {
        if ($user = $model->signup()) {
          Yii::$app->session->setFlash('success', 'Пользователь успешно создан.');
        }
      }

      return $this->render('signup', [
        'model' => $model,
      ]);
    }
}
