<?php
namespace backend\controllers;

use Yii;
// use yii\web\Controller;

use backend\components\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Client;
use backend\models\SearchClient;

/**
 * Client controller
 */
class ClientController extends BaseController
{
    public function actionIndex()
    {
    	$clients = [
	[
		"clientID" =>"CRM052699",
		"name" => "Айнур  (фольксваген)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771711155",
		],
		"TCs" => [
			[
				"brand" => "VWPolo",
				"size" => "185/60/15",
				"gosNomer" => "050FFA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053812",
		"name" => "Алексей (на Лексусе 470)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77477088402",
		],
		"TCs" => [
			[
				"brand" => "Lexus470",
				"size" => "285/50/20",
				"gosNomer" => "555AOB/02",
			],
		],
	],
	[
		"clientID" =>"CRM048878",
		"name" => "Алмас (тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018409460",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "215/60/16",
				"gosNomer" => "A077BHN",
			],
		],
	],
	[
		"clientID" =>"CRM056416",
		"name" =>"ТОО « Марасант»",
		"attribute" => "Физ. лицо",
		"IIN" => "061240007136",
		"contacts" => [
			"E-Mail" => "marasant_mto@mail.ru",
			"mobile" => "+77021370347",
			"adress" => "090000, ЗКО, г.Уральск, ул. С. Жаксыгулова 1/2",
		],
	],
	[
		"clientID" =>"CRM056417",
		"name" =>"Искаков Мирас",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77754457777",
		],
	],
	[
		"clientID" =>"CRM053245",
		"name" => "Сергей  (на тойоте)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772309661",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "215/65/16",
				"gosNomer" => "A818WHO",
			],
		],
	],
	[
		"clientID" =>"CRM029496",
		"name" =>"Вдовцева  Юлия",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77071880110",
		],
		"TCs" => [
			[
				"brand" => "NissanMurano",
				"size" => "255/55/18",
				"gosNomer" => "A387XCO",
			],
			[
				"brand" => "Nissan  Murano",
				"size" => "235/60/18",
				"gosNomer" => "A387XCO",
			],
		],
	],
	[
		"clientID" =>"CRM054107",
		"name" => "Эльдар  (тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018885747",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHighlander",
				"size" => "245/55/19",
				"gosNomer" => "782CAA/02",
			],
		],
	],
	[
		"clientID" =>"CRM056872",
		"name" =>"не читаемо",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77717751566",
		],
		"TCs" => [
			[
				"brand" => "Chrysler",
				"size" => "215/70/15",
				"gosNomer" => "A065XMO",
			],
		],
	],
	[
		"clientID" =>"CRM048879",
		"name" => "Айгуль  (Hyundai)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77785510950",
		],
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "195/65/15",
				"gosNomer" => "210PPA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053354",
		"name" => "Михаил  (Тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771826727",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "185/65/15",
				"gosNomer" => "472BPA/02",
			],
		],
	],
	[
		"clientID" =>"CRM030639",
		"name" =>"Голованёва  Ольга  Алексеевна",
		"attribute" => "Физ. лицо",
		"IIN" => "590702400320",
		"contacts" => [
			"mobile" => "+77017137261",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi",
				"size" => "215/60/17",
				"gosNomer" => "730DKA/02",
			],
			[
				"brand" => "MMC ASX ",
				"gosNomer" => "730 DKA  0",
			],
		],
	],
	[
		"clientID" =>"CRM059053",
		"name" =>"Вадим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "denis7772017@gmail.com",
			"mobile" => "+77012500263",
		],
	],
	[
		"clientID" =>"CRM059067",
		"name" =>"ИП Dastarkhan Industry",
		"attribute" => "Физ. лицо",
		"IIN" => "870728301155",
		"contacts" => [
			"E-Mail" => "tohtabakiev.samir@gmail.com",
			"mobile" => "+77027255554",
			"adress" => "г.Алматы, Турксибский р-н, ул. Соболева/Иванова 22/4",
		],
	],
	[
		"clientID" =>"CRM059077",
		"name" =>"Ерлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "xman777@list.ru",
			"mobile" => "+7707799977",
		],
	],
	[
		"clientID" =>"CRM059085",
		"name" =>"Мурат Vita Botlers",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775525983",
		],
	],
	[
		"clientID" =>"CRM059073",
		"name" =>"ТОО Ситикаб / Низам",
		"attribute" => "Физ. лицо",
		"IIN" => "130240018045",
		"contacts" => [
			"mobile" => "+77017955512",
		],
	],
	[
		"clientID" =>"CRM059084",
		"name" =>"Садыков Артур Саясатович",
		"attribute" => "Физ. лицо",
		"IIN" => "631118300194",
		"contacts" => [
			"mobile" => "+77072305094",
		],
	],
	[
		"clientID" =>"CRM059088",
		"name" =>"Владимир СВП",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77086442406",
		],
	],
	[
		"clientID" =>"CRM059087",
		"name" =>"Меиржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273781071",
			
		],
	],
	[
		"clientID" =>"CRM059105",
		"name" =>"дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772159595",
		],
	],
	[
		"clientID" =>"CRM059109",
		"name" =>"Иващенко Виктор Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "590202302848",
		"contacts" => [
			"mobile" => "+77055705635",
		],
	],
	[
		"clientID" =>"CRM059113",
		"name" =>"Константин",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kks-doktor@list.ru",
			"mobile" => "+77056520302",
			"adress" => "Усть-Каменогорск",
		],
	],
	[
		"clientID" =>"CRM059120",
		"name" =>"Нурлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052711117",
		],
	],
	[
		"clientID" =>"CRM059125",
		"name" =>"Альбина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057844026",
		],
	],
	[
		"clientID" =>"CRM059136",
		"name" =>"Валерий СВП",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77477454626",
		],
	],
	[
		"clientID" =>"CRM059143",
		"name" =>"Алтынбекова Сауле Габасовна",
		"attribute" => "Физ. лицо",
		"IIN" => "850825400256",
		"contacts" => [
			"mobile" => "+77018805060",
		],
	],
	[
		"clientID" =>"CRM059148",
		"name" =>"Темирлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076531313",
			"adress" => "Семск",
		],
	],
	[
		"clientID" =>"CRM059158",
		"name" =>"Мэлсулы Марат",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM059168",
		"name" =>"ТОО Alina Management",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77474283554",
		],
	],
	[
		"clientID" =>"CRM059195",
		"name" =>"Кульметов Мухит Джарылкасилович",
		"attribute" => "Физ. лицо",
		"IIN" => "881224301734",
		"contacts" => [
			"mobile" => "+77026484088",
		],
	],
	[
		"clientID" =>"CRM059227",
		"name" =>"Сидоренко Ольга Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "870906450038",
		"contacts" => [
			"mobile" => "+77017836274",
		],
		"TCs" => [
			[
				"brand" => "Volkswagen",
				"size" => "185/65/15",
				"gosNomer" => "H9198/02",
			],
		],
	],
	[
		"clientID" =>"CRM059185",
		"name" =>"Эльдар 225/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77073299781",
		],
	],
	[
		"clientID" =>"CRM059198",
		"name" =>"Вадим МТ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vad265@mail.ru",
			"mobile" => "+77773029882",
		],
	],
	[
		"clientID" =>"CRM059209",
		"name" =>"Клиент не определен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072588808",
		],
	],
	[
		"clientID" =>"CRM059190",
		"name" =>"Cеменов Виталий Валерьевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77774437620",
		],
	],
	[
		"clientID" =>"CRM059218",
		"name" =>"Дамир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77073257675",
		],
	],
	[
		"clientID" =>"CRM059238",
		"name" =>"Андрей КО2",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072630520",
		],
	],
	[
		"clientID" =>"CRM059240",
		"name" =>"Сабир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777668046",
		],
	],
	[
		"clientID" =>"CRM059261",
		"name" =>"TOO “KTK SERVICE TRUNKS”",
		"attribute" => "Физ. лицо",
		"IIN" => "080240017580",
		"contacts" => [
			"E-Mail" => "mmukashev@mail.ru",
			"mobile" =>"+77272459034",
			"mobile" => "+77272459034",
			"adress" => " г. Алматы, мкр. Айгерим-1, ул. МТФ-1, д.62",
		],
	],
	[
		"clientID" =>"CRM059264",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272945827",
			"mobile" => "+77272945827",
		],
	],
	[
		"clientID" =>"CRM059270",
		"name" =>"Джумбетов Жаксылык Кенжетайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "850901300371",
		"contacts" => [
			"mobile" => "+77073181819",
		],
	],
	[
		"clientID" =>"CRM059269",
		"name" =>"ТОО Химзащита Групп / Андрей",
		"attribute" => "Физ. лицо",
		"IIN" => "080940017962",
		"contacts" => [
			"mobile" => "+77015213189",
		],
	],
	[
		"clientID" =>"CRM059277",
		"name" =>"Сергей Валерьевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013229333",
		],
	],
	[
		"clientID" =>"CRM059285",
		"name" =>"Абылбаев Талгат Кундызбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "710331302222",
		"contacts" => [
			"mobile" => "+77776626270",
		],
	],
	[
		"clientID" =>"CRM059288",
		"name" =>"Дима",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM059305",
		"name" =>"Естжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771278765",
		],
	],
	[
		"clientID" =>"CRM059304",
		"name" =>"Яна в пятницу",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77080448027",
		],
	],
	[
		"clientID" =>"CRM059308",
		"name" =>"Александр Атырау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019620461",
		],
	],
	[
		"clientID" =>"CRM059347",
		"name" =>"Рустем",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77023555231",
		],
	],
	[
		"clientID" =>"CRM059348",
		"name" =>"Андрей Караганда",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "1andru80@mail.ru",
			"mobile" => "+77004554183",
		],
	],
	[
		"clientID" =>"CRM059338",
		"name" =>"Бирюков Юрий Дмитриевич",
		"attribute" => "Физ. лицо",
		"IIN" => "630114302592",
		"contacts" => [
			"mobile" => "+77714093472",
		],
	],
	[
		"clientID" =>"CRM059354",
		"name" =>"Женис 19.5",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018988931",
		],
	],
	[
		"clientID" =>"CRM059362",
		"name" =>"Максим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "maks-90_90@mail.ru",
			"mobile" => "+77022923069",
		],
	],
	[
		"clientID" =>"CRM059363",
		"name" =>"Фархат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "bagautdinov_farhat@mail.ru",
			"mobile" => "+77074501050",
		],
	],
	[
		"clientID" =>"CRM059384",
		"name" =>"Женя шины",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "JohnAngel@mail.ru",
			"mobile" => "+77772906629",
		],
	],
	[
		"clientID" =>"CRM059391",
		"name" =>"Турагул Жаркын Журсинулы",
		"attribute" => "Физ. лицо",
		"IIN" => "931104350696",
		"contacts" => [
			"mobile" => "+77077251904",
		],
	],
	[
		"clientID" =>"CRM059396",
		"name" =>"РОМАН",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "rkazakov7@gmail.com",
			"mobile" => "+77087264356",
		],
	],
	[
		"clientID" =>"CRM059397",
		"name" =>"Борис",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77009618453",
		],
	],
	[
		"clientID" =>"CRM059410",
		"name" =>"Сергей Субару",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77056604656",
		],
	],
	[
		"clientID" =>"CRM059398",
		"name" =>"Нургазы",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77075141070",
		],
	],
	[
		"clientID" =>"CRM059422",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "l.w.w@bk.ru",
			"mobile" =>"+77172430359",
			"mobile" => "+77172430359",
		],
	],
	[
		"clientID" =>"CRM026790",
		"name" =>"Кенжетаев  Руслан  Альбертович",
		"attribute" => "Физ. лицо",
		"IIN" => "850223300247",
		"contacts" => [
			"mobile" =>"+77052220932",
			"mobile" => "+77776441024",
		],
		"TCs" => [
			[
				"brand" => "HummerH3",
				"size" => "285/75/16",
				"gosNomer" => "A326CXP",
			],
			[
				"brand" => "DaewooNexia",
				"size" => "185/60/14",
				"gosNomer" => "A618HF",
			],
		],
	],
	[
		"clientID" =>"CRM048132",
		"name" => "Тимур",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77028237070",
		],
		"TCs" => [
			[
				"brand" => "LandCruiser",
				"size" => "275/55/20",
				"gosNomer" => "055TTC",
			],
		],
	],
	[
		"clientID" =>"CRM047337",
		"name" =>"АрманТестРуста",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"46546446",
		],
	],
	[
		"clientID" =>"CRM057594",
		"name" =>"Кабиров Руслан Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "760713303107",
		"contacts" => [
			"mobile" => "+77051898497",
		],
	],
	[
		"clientID" =>"CRM057589",
		"name" =>"Болат Бастаевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273077900",
		],
	],
	[
		"clientID" =>"CRM057610",
		"name" =>"195/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77273846468",
		],
	],
	[
		"clientID" =>"CRM057614",
		"name" =>"Севиль",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772218549",
		],
	],
	[
		"clientID" =>"CRM057612",
		"name" =>"Чуигараев Болат Кожагелдиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "670401302272",
		"contacts" => [
			"mobile" => "+77089139926",
		],
	],
	[
		"clientID" =>"CRM057626",
		"name" =>"Акмиш Галым Орастаевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77074342153",
		],
	],
	[
		"clientID" =>"CRM057634",
		"name" =>"ТОО \"Performance\"",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+7707855070",
		],
	],
	[
		"clientID" =>"CRM057633",
		"name" =>"Инцаки Александр Леонидович",
		"attribute" => "Физ. лицо",
		"IIN" => "730428301949",
		"contacts" => [
			"mobile" => "+77773890300",
		],
	],
	[
		"clientID" =>"CRM057657",
		"name" =>"Сакен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77715521985",
		],
	],
	[
		"clientID" =>"CRM057688",
		"name" =>"Мельченко Андрей Валентинович",
		"attribute" => "Физ. лицо",
		"IIN" => "810905301720",
		"contacts" => [
			"mobile" => "+77078760013",
		],
	],
	[
		"clientID" =>"CRM057703",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774000155",
		],
	],
	[
		"clientID" =>"CRM057713",
		"name" =>"Дорошенко Андрей Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "880310300760",
		"contacts" => [
			"mobile" => "+77474274666",
		],
	],
	[
		"clientID" =>"CRM057720",
		"name" =>"Акционерное общество \"Центральный депозитарий ценных бумаг\"",
		"attribute" => "Физ. лицо",
		"IIN" => "970740000154",
		"contacts" => [
			"mobile" =>"+77272620309",
		],
	],
	[
		"clientID" =>"CRM057721",
		"name" =>"Виталий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771010050",
		],
	],
	[
		"clientID" =>"CRM057723",
		"name" =>"Ломагин Константин",
		"attribute" => "Физ. лицо",
		"IIN" => "850227300685",
		"contacts" => [
			"mobile" => "+77017408486",
			"adress" => "г.Астана ул, Пушкина 75 ТОО \"Компания Рокос\"",
		],
	],
	[
		"clientID" =>"CRM057735",
		"name" =>"Татьяна караганда 1 диск",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "tata.ru73@mail.ru",
			"mobile" => "+77019885951",
		],
	],
	[
		"clientID" =>"CRM057736",
		"name" =>"ТОО Ландшафт плюс",
		"attribute" => "Физ. лицо",
		"IIN" => "100440001508",
		"contacts" => [
			"E-Mail" => "firmland@mail.ru",
			"adress" => "050053 г,Алматы ,ул Тепличное хозяйство Дом 1,кв1",
		],
	],
	[
		"clientID" =>"CRM057751",
		"name" =>"Анатолий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77059043001",
		],
	],
	[
		"clientID" =>"CRM057772",
		"name" =>"Жалгас",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017102905",
		],
	],
	[
		"clientID" =>"CRM057784",
		"name" =>"Александр 185/70/14",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM057790",
		"name" =>"Александр 185/70/14",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77472807610",
		],
	],
	[
		"clientID" =>"CRM057800",
		"name" =>"Антон",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777797688",
		],
	],
	[
		"clientID" =>"CRM057791",
		"name" =>"Антон 77015668833",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015668833",
		],
	],
	[
		"clientID" =>"CRM057804",
		"name" =>"Думан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052333949",
		],
	],
	[
		"clientID" =>"CRM057833",
		"name" =>"Общественный Фонд \"Поддержки программы Президента Республики Казахстан\"",
		"attribute" => "Физ. лицо",
		"IIN" => "100240000136",
		"contacts" => [
			"mobile" => "+77012228270",
		],
		"TCs" => [
			[
				"brand" => "Toyota ",
				"size" => "285/60/18",
				"gosNomer" => "200WW/02",
			],
		],
	],
	[
		"clientID" =>"CRM057848",
		"name" =>"Нахипов Арыстан Кулмахонбетович",
		"attribute" => "Физ. лицо",
		"IIN" => "810526301245",
		"contacts" => [
			"mobile" => "+77026111614",
		],
	],
	[
		"clientID" =>"CRM057849",
		"name" =>"Жанна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773687768",
		],
	],
	[
		"clientID" =>"CRM057838",
		"name" =>"Шалкар 235/55/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77019730101",
		],
	],
	[
		"clientID" =>"CRM057857",
		"name" =>"Александр 265/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77770575700",
		],
	],
	[
		"clientID" =>"CRM057862",
		"name" =>"Пензин Дмитрий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "850411300427",
		"contacts" => [
			"mobile" =>"+77273005533",
			"mobile" => "+77012452050",
		],
	],
	[
		"clientID" =>"CRM057872",
		"name" =>"Петропалов Олег Олегович",
		"attribute" => "Физ. лицо",
		"IIN" => "630124300015",
		"contacts" => [
			"mobile" => "+77773575282",
		],
	],
	[
		"clientID" =>"CRM057877",
		"name" =>"Берик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77086504951",
		],
	],
	[
		"clientID" =>"CRM057881",
		"name" =>"Константин",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "konstantin.khmelev@gmail.com",
			"mobile" => "+77077804869",
		],
	],
	[
		"clientID" =>"CRM057889",
		"name" =>"Максим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017655953",
		],
	],
	[
		"clientID" =>"CRM057896",
		"name" =>"Рустам",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77010367777",
		],
	],
	[
		"clientID" =>"CRM057916",
		"name" =>"ТОО Служба сервиса Добрыня",
		"attribute" => "Физ. лицо",
		"IIN" => "080240002196",
		"contacts" => [
			"mobile" => "+77762248896",
		],
	],
	[
		"clientID" =>"CRM057898",
		"name" =>"Жанара",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77079808085",
		],
	],
	[
		"clientID" =>"CRM057909",
		"name" =>"рашид",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771621213",
		],
	],
	[
		"clientID" =>"CRM057910",
		"name" =>"Шарипова Майнур Чабадекова",
		"attribute" => "Физ. лицо",
		"IIN" => "650523401230",
		"contacts" => [
			"mobile" => "+77078205488",
		],
	],
	[
		"clientID" =>"CRM057946",
		"name" =>"Кушкинова Марал Мураткызы",
		"attribute" => "Физ. лицо",
		"IIN" => "880508401018",
		"contacts" => [
			"E-Mail" => "maral.kuzhaspai@gmail.com",
			"mobile" => "+77717060605",
			"adress" => "Мангистауская область, г.Актау, мунайлинский район, с.Атамекен, ул.Жалын, дом 265.",
		],
	],
	[
		"clientID" =>"CRM057970",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "aav2005@mail.ru",
			"mobile" => "+77021466687",
		],
	],
	[
		"clientID" =>"CRM057983",
		"name" =>"Павел",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77054977044",
		],
	],
	[
		"clientID" =>"CRM057985",
		"name" =>"Сейлбек Тайышов Тайышов ИП \"ASL KELISIM\"",
		"attribute" => "Физ. лицо",
		"IIN" => "920329400344",
		"contacts" => [
			"E-Mail" => "taiyshovseiilbek@mail.ru",
			"adress" => "г. Алматы, ул. Жанкожа батыра, д. 26",
		],
	],
	[
		"clientID" =>"CRM057963",
		"name" =>"ТОО \"Скиф Трейд\"",
		"attribute" => "Физ. лицо",
		"IIN" => "020340002288",
		"contacts" => [
			"E-Mail" => "pd03@small.kz",
			"mobile" =>"+77272986451",
			"mobile" => "+77717093920",
			"adress" => "Санжар Жанузаков менеджер отдела закупок",
		],
	],
	[
		"clientID" =>"CRM057968",
		"name" =>"Олжас naiman.on86@gmail.com",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "naiman.on86@gmail.com",
			"mobile" =>"+77755444961",
		],
	],
	[
		"clientID" =>"CRM057975",
		"name" =>"Габит Goodyear Wrangler DuraTrac",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77074169290",
		],
	],
	[
		"clientID" =>"CRM057987",
		"name" =>"Владислав",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"E-Mail" => "vladislav.tolmachev.1979@mail.ru",
			"mobile" => "+77772748395",
		],
	],
	[
		"clientID" =>"CRM057988",
		"name" =>"Закир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vonsanov.zakir@mail.ru",
			"mobile" => "+77024010014",
		],
	],
	[
		"clientID" =>"CRM057989",
		"name" =>"Константинов Евгений Федорович",
		"attribute" => "Физ. лицо",
		"IIN" => "900128300490",
		"contacts" => [
			"mobile" => "+77772364595",
			"adress" => "Ташкентская 481 В уг Саина",
		],
	],
	[
		"clientID" =>"CRM057992",
		"name" =>"Пинских Василий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073887961",
		],
	],
	[
		"clientID" =>"CRM057995",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772212115",
		],
	],
	[
		"clientID" =>"CRM058034",
		"name" =>"Евгений УАЗ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024721555",
		],
	],
	[
		"clientID" =>"CRM058022",
		"name" =>"Бержанов Жанибек Байманович",
		"attribute" => "Физ. лицо",
		"IIN" => "590303302699",
		"contacts" => [
			"mobile" => "+77011259134",
		],
	],
	[
		"clientID" =>"CRM058030",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sergey7428@mail.ru",
			"mobile" => "+77015286501",
		],
	],
	[
		"clientID" =>"CRM058035",
		"name" =>"Бекжанов Мерхат Куандыкович",
		"attribute" => "Физ. лицо",
		"IIN" => "910905302462",
		"contacts" => [
			"mobile" => "+77017886092",
		],
		"TCs" => [
			[
				"brand" => "BMW-520",
				"size" => "195/65/15",
				"gosNomer" => "B098ZSN",
			],
		],
	],
	[
		"clientID" =>"CRM058039",
		"name" =>"денис",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019888454",
		],
	],
	[
		"clientID" =>"CRM058042",
		"name" =>"Захидов Абдужалил Шодиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "660703399067",
		"contacts" => [
			"mobile" => "+77013202266",
		],
	],
	[
		"clientID" =>"CRM058071",
		"name" =>"Ставицкий Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051888815",
		],
		"TCs" => [
			[
				"brand" => "MERСEDES-BENZ W124",
				"size" => "195/65/15",
				"gosNomer" => "804XMA/02",
			],
		],
	],
	[
		"clientID" =>"CRM058074",
		"name" =>"Виктор",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vols_ushtobe@mail.ru",
			"mobile" => "+77771362414",
		],
	],
	[
		"clientID" =>"CRM058078",
		"name" =>"инкор актау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77292301339",
		],
	],
	[
		"clientID" =>"CRM058101",
		"name" =>"Жанна",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77017995999",
		],
	],
	[
		"clientID" =>"CRM058086",
		"name" =>"Улан Нива",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073794296",
		],
	],
	[
		"clientID" =>"CRM058092",
		"name" =>"Клиентка на чехлы",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077232637",
		],
	],
	[
		"clientID" =>"CRM058102",
		"name" =>"Виталий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77715011714",
		],
	],
	[
		"clientID" =>"CRM058105",
		"name" =>"Петков Анатолий Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "650716301808",
		"contacts" => [
			"mobile" =>"+77017264733",
			"mobile" => "+77077264733",
			"adress" => "г. Актау, 11 мкр, д. 10, кв. 95",
		],
	],
	[
		"clientID" =>"CRM058114",
		"name" =>"Максат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ish-maksat@ya.ru",
			"mobile" => "+77767956573",
		],
	],
	[
		"clientID" =>"CRM058123",
		"name" =>"Панамарев Иван Викторович ТОО \"ИзотопСнабСервис\"",
		"attribute" => "Юр. лицо",
		"IIN" => "100940013699",
		"contacts" => [
			"mobile" => "+77772459734",
			"adress" => "Полежаева 92 А  по Сейфулина  ниже Такентской ТЕЦ 1",
		],
	],
	[
		"clientID" =>"CRM058131",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272345678",
			"adress" => "no_121212@gmail.com",
		],
	],
	[
		"clientID" =>"CRM058132",
		"name" =>"ТОО ДИС",
		"attribute" => "Физ. лицо",
		"IIN" => "990140001045",
		"contacts" => [
			"E-Mail" => "too.dis@mail.ru",
			"mobile" => "+77015338341",
		],
	],
	[
		"clientID" =>"CRM058133",
		"name" =>"ТОО \"Раслав Три Т\"",
		"attribute" => "Физ. лицо",
		"IIN" => "11084000243",
	],
	[
		"clientID" =>"CRM058134",
		"name" =>"Индивидуальный предприниматель Фролов В.М.",
		"attribute" => "Физ. лицо",
		"IIN" => "780515399042",
	],
	[
		"clientID" =>"CRM058146",
		"name" =>"ТОО Центр Технической Безопасности /Андрей/",
		"attribute" => "Физ. лицо",
		"IIN" => "150100247806",
		"contacts" => [
			"mobile" =>"+77015237573",
			"mobile" => "+77272522546",
		],
	],
	[
		"clientID" =>"CRM058163",
		"name" =>"Канат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kan3245@mail.ru",
			"mobile" => "+77025557728",
		],
	],
	[
		"clientID" =>"CRM058164",
		"name" =>"Сайнулла",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "saynulla@list.ru",
			"mobile" => "+77073888380",
		],
	],
	[
		"clientID" =>"CRM058158",
		"name" =>"Олег 205/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714156264",
		],
	],
	[
		"clientID" =>"CRM058171",
		"name" =>"АСАД",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011175176",
		],
	],
	[
		"clientID" =>"CRM058174",
		"name" =>"Есболов Жанибек Кипчакбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820202300247",
		"contacts" => [
			"mobile" => "+77019555584",
		],
	],
	[
		"clientID" =>"CRM058170",
		"name" =>"Рахимов Айкын",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "AIKUN_R@mail.ru",
			"mobile" => "+77772982202",
		],
	],
	[
		"clientID" =>"CRM058183",
		"name" =>"Адиль Атырау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77010277000",
		],
	],
	[
		"clientID" =>"CRM058198",
		"name" =>"Адиль 225/55/18",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77010277009",
		],
	],
	[
		"clientID" =>"CRM058208",
		"name" =>"ТОО LCI Сервис",
		"attribute" => "Физ. лицо",
		"IIN" => "080740016411",
		"contacts" => [
			"mobile" => "+77052079309",
		],
	],
	[
		"clientID" =>"CRM058215",
		"name" =>"Арман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "armanbeyatm@gmail.com",
			"mobile" => "+77017308096",
		],
	],
	[
		"clientID" =>"CRM056674",
		"name" =>"диски приора",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77755795999",
		],
	],
	[
		"clientID" =>"CRM056685",
		"name" =>"Алишер Капчагай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073773237",
		],
	],
	[
		"clientID" =>"CRM056704",
		"name" =>"Володя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773242046",
		],
	],
	[
		"clientID" =>"CRM056713",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072110903",
		],
	],
	[
		"clientID" =>"CRM056743",
		"name" =>"ТОО \"BP InvestGroup\"",
		"attribute" => "Физ. лицо",
		"IIN" => "080640005096",
		"contacts" => [
			"E-Mail" => "adilbeknurgaliev@mail.ru",
			"mobile" =>"+77272540396",
			"mobile" => "+77777708700",
		],
	],
	[
		"clientID" =>"CRM056767",
		"name" =>"Виталий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77087711044",
		],
	],
	[
		"clientID" =>"CRM056769",
		"name" =>"Курганский Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015086776",
		],
	],
	[
		"clientID" =>"CRM056770",
		"name" =>"Карлыгаш Карагандинская Область",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051215690",
		],
	],
	[
		"clientID" =>"CRM056781",
		"name" =>"Султанбаев Даулыбай Даркенбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "600623300766",
		"contacts" => [
			"E-Mail" => "janbota75@Gmail.com",
			"mobile" =>"+77786702055",
			"mobile" => "+77786702055",
		],
	],
	[
		"clientID" =>"CRM056788",
		"name" =>"Абдрахманов Асет Махмутович",
		"attribute" => "Физ. лицо",
		"IIN" => "660609300205",
		"contacts" => [
			"mobile" =>"+77272468704",
			"mobile" => "+77272468704",
			"adress" => "Жамакаева 274 , Маркова выше Аль-Фараби",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "215/60/16",
				"gosNomer" => "A555YEO",
			],
		],
	],
	[
		"clientID" =>"CRM056795",
		"name" =>"Галиева Динара ТОО \"LEAD INDUSTRY\"",
		"attribute" => "Физ. лицо",
		"IIN" => "151140005214",
		"contacts" => [
			"E-Mail" => "lm.snabzhenie@gmail.com",
			"mobile" => "+77772500367",
			"adress" => "г.Алматы, ул. Раймбека 212/1 офис 202.",
		],
	],
	[
		"clientID" =>"CRM056798",
		"name" =>"Мирзоева Алевтина Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "690905401728",
		"contacts" => [
			"mobile" => "+77771770384",
		],
	],
	[
		"clientID" =>"CRM056841",
		"name" =>"Сыдыков Берик Калшабекович",
		"attribute" => "Физ. лицо",
		"IIN" => "791027302108",
		"contacts" => [
			"mobile" => "+77019000553",
		],
	],
	[
		"clientID" =>"CRM056846",
		"name" =>"Шамин Николай Николаевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073647888",
		],
		"TCs" => [
			[
				"brand" => "ToyotaSequoia",
				"size" => "275/70/16",
				"gosNomer" => "421LDA/02",
			],
		],
	],
	[
		"clientID" =>"CRM056853",
		"name" =>"Онбаев Кумаркан Дуйсенович",
		"attribute" => "Физ. лицо",
		"IIN" => "571023301579",
		"contacts" => [
			"mobile" => "+77052104937",
		],
	],
	[
		"clientID" =>"CRM056875",
		"name" =>"Тулакбаева Жанета Мамырхановна",
		"attribute" => "Физ. лицо",
		"IIN" => "851115401188",
		"contacts" => [
			"mobile" => "+77077181510",
		],
	],
	[
		"clientID" =>"CRM056883",
		"name" =>"Алтынбек, механик Арман ТОО СК \"УзеньСтройСервис\"",
		"attribute" => "Физ. лицо",
		"IIN" => "020440000999",
		"contacts" => [
			"E-Mail" => "zharasov.1963@mail.ru",
			"mobile" => "+77015124475",
			"adress" => "по Суюнбая вниз, ориентир новый мост",
		],
	],
	[
		"clientID" =>"CRM056898",
		"name" =>"Александр 205/55/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024042445",
		],
	],
	[
		"clientID" =>"CRM056901",
		"name" =>"Рауан 17,5-25",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77782901800",
		],
	],
	[
		"clientID" =>"CRM056902",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272467645",
		],
	],
	[
		"clientID" =>"CRM056911",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77779382525",
		],
	],
	[
		"clientID" =>"CRM056935",
		"name" =>"Шерзад 7785831417",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kadirovs7@mail.ru",
			"mobile" => "+77785831417",
		],
	],
	[
		"clientID" =>"CRM056948",
		"name" =>"ТОО \"КазЕвроСтрой и К\" ( Аян)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052220883",
		],
	],
	[
		"clientID" =>"CRM056950",
		"name" =>"Бектемиров Нураскар Алтыбайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "911226350325",
		"contacts" => [
			"mobile" => "+77011071474",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "215/60/16",
				"gosNomer" => "Z564YZH",
			],
		],
	],
	[
		"clientID" =>"CRM056971",
		"name" =>"Виталик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vitalikzkn203@bk.ru",
			"mobile" => "+77057713486",
		],
	],
	[
		"clientID" =>"CRM056990",
		"name" =>"Улан диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078651565",
		],
	],
	[
		"clientID" =>"CRM057001",
		"name" =>"Игорь",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+7771495122",
		],
	],
	[
		"clientID" =>"CRM057053",
		"name" =>"Шалашнёва Надежда Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "810131400206",
		"contacts" => [
			"E-Mail" => "nadede@mail.ru",
			"mobile" =>"+77272333083",
			"mobile" => "+77272333083",
		],
	],
	[
		"clientID" =>"CRM057060",
		"name" =>"Гульнара",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012139989",
		],
	],
	[
		"clientID" =>"CRM057018",
		"name" =>"Третьяков Владимир Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "590906301222",
	],
	[
		"clientID" =>"CRM057051",
		"name" =>"TOO Light Deco",
		"attribute" => "Физ. лицо",
		"IIN" => "160740013652",
		"contacts" => [
			"mobile" =>"+77272478630",
		],
	],
	[
		"clientID" =>"CRM057087",
		"name" =>"Лоскутов Денис Викторович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017439677",
		],
	],
	[
		"clientID" =>"CRM057088",
		"name" =>"Талгат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "talgat_a.u@mail.ru",
			"mobile" => "+77017696000",
		],
	],
	[
		"clientID" =>"CRM057092",
		"name" =>"вадим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273770105",
			"mobile" => "+77273770105",
		],
	],
	[
		"clientID" =>"CRM057130",
		"name" =>"Валиханов Арыстан Худжат-Эль-Исмаилович",
		"attribute" => "Физ. лицо",
		"IIN" => "550226301447",
		"contacts" => [
			
		],
	],
	[
		"clientID" =>"CRM057153",
		"name" =>"Мальсагова Кристина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77785747924",
		],
	],
	[
		"clientID" =>"CRM057142",
		"name" =>"Аскар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272458833",
		],
	],
	[
		"clientID" =>"CRM057144",
		"name" =>"Лариса",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013554693",
		],
	],
	[
		"clientID" =>"CRM057175",
		"name" =>"Каржау Руслан Аманбекович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ru.ka1977@mail.ru",
			"mobile" => "+77009696797",
		],
	],
	[
		"clientID" =>"CRM057169",
		"name" =>"Ким Юлия Анатольевна",
		"attribute" => "Физ. лицо",
		"IIN" => "910620400760",
		"contacts" => [
			"mobile" => "+77078172017",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "215/65/16",
				"gosNomer" => "592TBA/02",
			],
		],
	],
	[
		"clientID" =>"CRM057202",
		"name" =>"Ярошенко Наталья Ивановна",
		"attribute" => "Физ. лицо",
		"IIN" => "600820403034",
		"contacts" => [
			"mobile" => "+77015338931",
			"adress" => "г. Актау, 7 мкр., д. 14, кв. 51",
		],
	],
	[
		"clientID" =>"CRM057204",
		"name" =>"Эрик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77712227777",
		],
	],
	[
		"clientID" =>"CRM057234",
		"name" =>"Кажгалиев Азамат Серикболатович",
		"attribute" => "Физ. лицо",
		"IIN" => "810601303388",
		"contacts" => [
			"mobile" => "+77089676309",
		],
		"TCs" => [
			[
				"brand" => "Subaru ",
				"size" => "215/55/17",
				"gosNomer" => "661WEA05",
			],
		],
	],
	[
		"clientID" =>"CRM057263",
		"name" =>"ТОО \"Хозяйственное управление аппарата акима Южно-Казахстанской области\"",
		"attribute" => "Физ. лицо",
		"IIN" => "060140003890",
		"contacts" => [
			"mobile" => "+77753314040",
		],
	],
	[
		"clientID" =>"CRM057264",
		"name" =>"Акимов Егор Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "870114300179",
	],
	[
		"clientID" =>"CRM057257",
		"name" =>"Мельников Виктор Борисович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772104657",
		],
	],
	[
		"clientID" =>"CRM057261",
		"name" =>"Тест для счета",
		"attribute" => "Физ. лицо",
		"IIN" => "557788996633",
		"contacts" => [
			"E-Mail" => "kakdk@nvkjs.ry",
			"mobile" =>"+77275486547",
			"mobile" => "+75472648465",
			"adress" => "РК, г. Талдыкорган, ул. Мира, д. 7 РК, г. Талдыкорган, пр. Победы, д. 9",
		],
	],
	[
		"clientID" =>"CRM057265",
		"name" =>"Гульжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057367358",
		],
	],
	[
		"clientID" =>"CRM057281",
		"name" =>"Нуржан 1 или 2 шины",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777323520",
		],
	],
	[
		"clientID" =>"CRM057344",
		"name" =>"Айбар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778060528",
		],
		"TCs" => [
			[
				"brand" => "Toyota Land Cruiser Prado",
				"size" => "266/65/17",
				"gosNomer" => "249TEA/02",
			],
		],
	],
	[
		"clientID" =>"CRM057342",
		"name" =>"Горшков",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772899550",
		],
		"TCs" => [
			[
				"brand" => "Honda RVR",
				"size" => "215/60/16",
				"gosNomer" => "A782AUP",
			],
		],
	],
	[
		"clientID" =>"CRM052571",
		"name" => "Асылбек  (лексус)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77028008885",
		],
		"TCs" => [
			[
				"brand" => "Lexus",
				"size" => "265/60/18",
				"gosNomer" => "923YHA/02",
			],
		],
	],
	[
		"clientID" =>"CRM057355",
		"name" =>"Арман Toyota Camry",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77478456556",
		],
		"TCs" => [
			[
				"brand" => "Toyota Camry",
				"size" => "205/65/15",
				"gosNomer" => "908YEO/09",
			],
		],
	],
	[
		"clientID" =>"CRM057341",
		"name" =>"Данияр Mercedes",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013009889",
		],
		"TCs" => [
			[
				"brand" => "Mercedes",
				"size" => "235/60/18",
				"gosNomer" => "708FCA/02",
			],
		],
	],
	[
		"clientID" =>"CRM057301",
		"name" =>"Рустам Хаммер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012277999",
		],
	],
	[
		"clientID" =>"CRM057332",
		"name" =>"ТОО \"КазТехноPol Алматы\"",
		"attribute" => "Физ. лицо",
		"IIN" => "050140006269",
		"contacts" => [
			"mobile" => "+77017698036",
		],
	],
	[
		"clientID" =>"CRM057334",
		"name" =>"Нурлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019853242",
		],
	],
	[
		"clientID" =>"CRM057335",
		"name" =>"Максим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073542040",
		],
	],
	[
		"clientID" =>"CRM057327",
		"name" =>"Фролов Евгений Геннадьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "740908301503",
		"contacts" => [
			"mobile" => "+77772346677",
		],
		"TCs" => [
			[
				"brand" => "Nissan R`nessa",
				"size" => "225/60/16",
				"gosNomer" => "A175MHO",
			],
		],
	],
	[
		"clientID" =>"CRM057330",
		"name" =>"Ильяс диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775503113",
		],
	],
	[
		"clientID" =>"CRM057338",
		"name" =>"Сущенко Георгий Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "940331301797",
		"contacts" => [
			
			"mobile" => "87078300215",
		],
	],
	[
		"clientID" =>"CRM057376",
		"name" =>"Литвинов Владимир Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "700827302227",
		"contacts" => [
			"mobile" => "+77773520122",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiPajero",
				"size" => "265/70/16",
				"gosNomer" => "A652EFO",
			],
		],
	],
	[
		"clientID" =>"CRM057377",
		"name" =>"Литвинов Владимир Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "700827302227",
		"contacts" => [
			"mobile" => "+77773520120",
		],
	],
	[
		"clientID" =>"CRM057379",
		"name" =>"Литвинов Владимир Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "700827302227",
		"contacts" => [
			"mobile" => "+77773520122",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiPajero",
				"size" => "265/70/16",
				"gosNomer" => "A652EFO",
			],
		],
	],
	[
		"clientID" =>"CRM057366",
		"name" =>"Нургалиев Вениамин Негметулаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "940513302030",
		"contacts" => [
			"mobile" => "+77078738838",
		],
	],
	[
		"clientID" =>"CRM057395",
		"name" =>"Кулакова Дарья Дмитриевна",
		"attribute" => "Физ. лицо",
		"IIN" => "930419401610",
		"contacts" => [
			"mobile" => "+77052624165",
		],
	],
	[
		"clientID" =>"CRM057396",
		"name" =>"Александр 6 б",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772450065",
		],
	],
	[
		"clientID" =>"CRM057430",
		"name" =>"Евгений хюндай215/70 R16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "bogdanov1745@mail.ru",
			"mobile" => "+77027781482",
		],
	],
	[
		"clientID" =>"CRM057426",
		"name" =>"Абдуллаева Лейла Сабировна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77711667474",
		],
	],
	[
		"clientID" =>"CRM057442",
		"name" =>"ТОО «Компания Тамыр»	",
		"attribute" => "Юр. лицо",
		"IIN" => "001040000299",
		"contacts" => [
			"mobile" => "+77015018667",
			"adress" => "Павлодарская обл., Экибастуз, ул. Абая 175	",
		],
	],
	[
		"clientID" =>"CRM057444",
		"name" =>"Кабылбеков Дархан Мергенбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "881020300698",
		"contacts" => [
			"mobile" => "+77027274046",
		],
	],
	[
		"clientID" =>"CRM057447",
		"name" =>"Николай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "azorg79@mail.ru",
			
			"mobile" => "+77772483726",
		],
	],
	[
		"clientID" =>"CRM057455",
		"name" =>"Ризабек",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"E-Mail" => "zhakupov_rizabek@list.ru¶",
			"mobile" => "+77056088818",
		],
	],
	[
		"clientID" =>"CRM057490",
		"name" =>"Артём",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77058408553",
		],
	],
	[
		"clientID" =>"CRM057486",
		"name" =>"Бакытова Айжан Бакытовна",
		"attribute" => "Физ. лицо",
		"IIN" => "901001400326",
		"contacts" => [
			"mobile" => "+77022186808",
		],
	],
	[
		"clientID" =>"CRM057496",
		"name" =>"Женис",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77756088225",
		],
	],
	[
		"clientID" =>"CRM057503",
		"name" =>"Эрик шины на кару",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77079550132",
		],
	],
	[
		"clientID" =>"CRM057508",
		"name" =>"Батыр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272609829",
		],
	],
	[
		"clientID" =>"CRM057524",
		"name" =>"Ким Дмитрий Георгиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "790215300190",
		"contacts" => [
			"mobile" => "+77777838300",
		],
	],
	[
		"clientID" =>"CRM057551",
		"name" =>"Саян шины автобус",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77714109831",
		],
	],
	[
		"clientID" =>"CRM057536",
		"name" =>"Хабиев Данияр Сырымович",
		"attribute" => "Физ. лицо",
		"IIN" => "820115302706",
	],
	[
		"clientID" =>"CRM057550",
		"name" =>"Адильжанов Аман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77089409495",
		],
	],
	[
		"clientID" =>"CRM057554",
		"name" =>"Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => " zalesov_2002@mail.ru",
			"mobile" => "+77754593159",
		],
	],
	[
		"clientID" =>"CRM057558",
		"name" =>"Турганов Сырым Амангельдиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "720716301655",
		"contacts" => [
			"mobile" => "+77777797077",
		],
	],
	[
		"clientID" =>"CRM057565",
		"name" =>"ТОО \"TEPLOSTAR-KAZ\"",
		"attribute" => "Физ. лицо",
		"IIN" => "150840023413",
		"contacts" => [
			"mobile" =>"+77051900043",
			"mobile" => "+77051900043",
		],
		"TCs" => [
			[
				"size" => "205/75/15",
			],
		],
	],
	[
		"clientID" =>"CRM057578",
		"name" =>"Рахимай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273987738",
			"mobile" => "+77273987738",
		],
	],
	[
		"clientID" =>"CRM057336",
		"name" =>"Касим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77778392992",
			"mobile" => "+77778392992",
		],
		"TCs" => [
			[
				"brand" => "BMW 7",
				"size" => "245/40/18",
				"gosNomer" => "1333ZUA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053103",
		"name" => "Ильяс (тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017109957",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "265/60/18",
				"gosNomer" => "260HNA/02",
			],
		],
	],
	[
		"clientID" =>"CRM031413",
		"name" =>"Прудник Валерий  Сименс",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77017343557",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "265/70/16",
				"gosNomer" => "H4838/02",
			],
			[
				"size" => "195/65/15",
				"gosNomer" => "H807059",
			],
			[
				"brand" => "Skoda",
				"size" => "205/55/16",
				"gosNomer" => "H4698/02",
			],
			[
				"brand" => "Toyota   Hilux",
				"size" => "225/70/15",
				"gosNomer" => "H5763/02",
			],
			[
				"brand" => "ŠKODAOctavia",
				"size" => "195/65/15",
				"gosNomer" => "C1573/02",
			],
		],
	],
	[
		"clientID" =>"CRM055830",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012129191",
		],
	],
	[
		"clientID" =>"CRM055831",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77089717174",
		],
	],
	[
		"clientID" =>"CRM055825",
		"name" =>"Константин",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77763031020",
		],
	],
	[
		"clientID" =>"CRM055844",
		"name" =>"Нурахметов Мухтар Габдулович",
		"attribute" => "Физ. лицо",
		"IIN" => "650628300049",
		"contacts" => [
			"mobile" => "+77775985555",
		],
		"TCs" => [
			[
				"brand" => "ToyotaFortuner",
				"size" => "265/65/17",
				"gosNomer" => "675XEA/05",
			],
			[
				"brand" => "Toyota",
				"size" => "265/65/17",
				"gosNomer" => "645XEA/05",
			],
		],
	],
	[
		"clientID" =>"CRM055847",
		"name" =>"Эдуард",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078224001",
		],
	],
	[
		"clientID" =>"CRM055863",
		"name" =>"Адырбеков Жаксылык Берикович",
		"attribute" => "Физ. лицо",
		"IIN" => "890809302751",
		"contacts" => [
			"mobile" => "+77018880109",
		],
	],
	[
		"clientID" =>"CRM055875",
		"name" =>"ТOO КАЗИНЖЭНЕРГОПРОЕКТ",
		"attribute" => "Физ. лицо",
		"IIN" => "140240032667",
		"contacts" => [
			"mobile" =>"+77272474182",
			"mobile" => "+77053398984",
		],
		"TCs" => [
			[
				"brand" => "ToyotaTundra",
				"size" => "235/75/15",
				"gosNomer" => "269CM/02",
			],
		],
	],
	[
		"clientID" =>"CRM055898",
		"name" =>"ИП \"МАТРИЦА.KZ\"",
		"attribute" => "Физ. лицо",
		"IIN" => "920502401243",
		"contacts" => [
			"mobile" => "+77053080767",
		],
	],
	[
		"clientID" =>"CRM055907",
		"name" =>"Канат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77079302209",
		],
	],
	[
		"clientID" =>"CRM055917",
		"name" =>"Кенжегалиева Айнур Жумабаевна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Ainur.Kenzhegaliyeva@westdala.isker.kz",
			"mobile" => "+77013305244",
		],
	],
	[
		"clientID" =>"CRM055928",
		"name" =>"Валентин",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ayupov.valentin@gmail.com",
			"mobile" => "+77017275352",
		],
	],
	[
		"clientID" =>"CRM055938",
		"name" =>"Филиал Стрела",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "filstrela@mail.ru",
		],
	],
	[
		"clientID" =>"CRM055943",
		"name" =>"Канат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "k_bukenov@mail.ru",
			"mobile" => "+77752798670",
		],
	],
	[
		"clientID" =>"CRM055951",
		"name" =>"Бауржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77717591444",
		],
	],
	[
		"clientID" =>"CRM056012",
		"name" =>"Еркен Хайлендер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017331280",
		],
	],
	[
		"clientID" =>"CRM056015",
		"name" =>"Ерик антифриз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "biokz@yandex.ru",
			"mobile" => "+77053756831",
		],
	],
	[
		"clientID" =>"CRM056010",
		"name" =>"Жанибеков Саламат",
		"attribute" => "Физ. лицо",
		"IIN" => "940616350318",
		"contacts" => [
			"E-Mail" => "	zhan_sala@mail.ru",
			"mobile" => "+77759979523",
			"adress" => "	Мангистауская область,Актау, Шыгыс 2-224-16, 130000",
		],
	],
	[
		"clientID" =>"CRM056032",
		"name" =>"Баймурзин Алмаз Аманжолович",
		"attribute" => "Физ. лицо",
		"IIN" => "730422302042",
		"contacts" => [
			"mobile" => "+77771124444",
		],
		"TCs" => [
			[
				"brand" => "Mercedes-bensS300",
				"size" => "275/45/18",
				"gosNomer" => "070HXA",
			],
		],
	],
	[
		"clientID" =>"CRM056040",
		"name" =>"Ким Николай Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "491003300861",
		"contacts" => [
			"mobile" => "+77773993402",
		],
	],
	[
		"clientID" =>"CRM056056",
		"name" =>"Салтанат 1 баллон",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77022089580",
		],
	],
	[
		"clientID" =>"CRM056067",
		"name" =>"Вадим сату",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vadimeastwings@mail.ru",
			"mobile" => "+77015553761",
		],
	],
	[
		"clientID" =>"CRM056088",
		"name" =>"Зинин Андрей",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77019633991",
		],
	],
	[
		"clientID" =>"CRM056086",
		"name" =>"Калдарбеков Нуржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77788042954",
		],
	],
	[
		"clientID" =>"CRM048884",
		"name" => "Равшан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017199677",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCruiser",
				"size" => "285/65/18",
				"gosNomer" => "575NFA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053113",
		"name" =>"Ахмедов Ханали Темирович",
		"attribute" => "Физ. лицо",
		"IIN" => "620612300165",
		"contacts" => [
			"mobile" => "+77014497900",
		],
	],
	[
		"clientID" =>"CRM053114",
		"name" =>"Меткалф Наталья Михайловна",
		"attribute" => "Физ. лицо",
		"IIN" => "870606400182",
		"contacts" => [
			"mobile" => "+77771559900",
		],
		"TCs" => [
			[
				"brand" => "TOYOTACorolla",
				"size" => "185/65/14",
				"gosNomer" => "470 NOA",
			],
		],
	],
	[
		"clientID" =>"CRM053119",
		"name" =>"Юрий 225/60/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273972415",
		],
	],
	[
		"clientID" =>"CRM059905",
		"name" =>"Сандыбек организация",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272938319",
		],
	],
	[
		"clientID" =>"CRM059909",
		"name" =>"Жан 275/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77015512225",
		],
	],
	[
		"clientID" =>"CRM059911",
		"name" =>"Юрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ura_63@inbox.ru",
			"mobile" => "+77074437259",
		],
	],
	[
		"clientID" =>"CRM059913",
		"name" =>"Владимир 285/50/20",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77014158558",
		],
	],
	[
		"clientID" =>"CRM059914",
		"name" =>"Галина Григорьевна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772283549",
		],
	],
	[
		"clientID" =>"CRM059916",
		"name" =>"Марат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017480050",
		],
	],
	[
		"clientID" =>"CRM059928",
		"name" =>"Вячеслав 285/60/18",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017210670",
		],
	],
	[
		"clientID" =>"CRM059934",
		"name" =>"Гульмира",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "almaty_gulmira@mail.ru",
			"mobile" => "+77770159402",
		],
	],
	[
		"clientID" =>"CRM059938",
		"name" =>"Глущенко Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "glushenko_vladimir@mail.ru",
			"mobile" => "+77772294401",
		],
	],
	[
		"clientID" =>"CRM059941",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+",
		],
	],
	[
		"clientID" =>"CRM059942",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+",
		],
	],
	[
		"clientID" =>"CRM059943",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+",
		],
	],
	[
		"clientID" =>"CRM059953",
		"name" =>"Михаил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772153218",
		],
	],
	[
		"clientID" =>"CRM059977",
		"name" =>"Бахыт",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "imanakunovb@mail.ru",
			"mobile" => "+77785444419",
		],
	],
	[
		"clientID" =>"CRM059978",
		"name" =>"Тимур",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "timur_erimov@mail.ru",
			"mobile" => "+77077450937",
		],
	],
	[
		"clientID" =>"CRM059980",
		"name" =>"Ануар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "filtriakssesuary@mail.ru",
			"mobile" => "+77750005080",
		],
	],
	[
		"clientID" =>"CRM059992",
		"name" =>"Герман Игнатьевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772264031",
		],
	],
	[
		"clientID" =>"CRM059981",
		"name" =>"Маторо Юрий Иванович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072504046",
		],
	],
	[
		"clientID" =>"CRM059995",
		"name" =>"Жолдасов Нурлан Орынтаевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272555230",
			"mobile" => "+77072228279",
		],
	],
	[
		"clientID" =>"CRM060002",
		"name" =>"Еркежан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77081775550",
		],
	],
	[
		"clientID" =>"CRM060003",
		"name" =>"Олег",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+7777632125",
		],
	],
	[
		"clientID" =>"CRM059998",
		"name" =>"Марат диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773552015",
		],
	],
	[
		"clientID" =>"CRM060015",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+",
		],
	],
	[
		"clientID" =>"CRM060011",
		"name" =>"Дидар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77079320414",
		],
	],
	[
		"clientID" =>"CRM060017",
		"name" =>"Руслан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077001397",
		],
	],
	[
		"clientID" =>"CRM060018",
		"name" =>"Маргулан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772295000",
		],
	],
	[
		"clientID" =>"CRM060019",
		"name" =>"Nurik",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060024",
		"name" =>"Иванов",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "maildwi996@gmail.com",
			"mobile" => "+77051292778",
		],
	],
	[
		"clientID" =>"CRM060032",
		"name" =>"Адиль",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77023547498",
		],
	],
	[
		"clientID" =>"CRM060040",
		"name" =>"Ник",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "gefestik@mail.ru",
			"mobile" => "+77778204422",
		],
	],
	[
		"clientID" =>"CRM060033",
		"name" =>"Тебек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017558145",
		],
	],
	[
		"clientID" =>"CRM060038",
		"name" =>"Марат 60",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "info@lexway.kz",
			"mobile" => "+77023334429",
		],
	],
	[
		"clientID" =>"CRM060044",
		"name" =>"Хребтов Николай Павлович",
		"attribute" => "Физ. лицо",
		"IIN" => "450525300999",
		"contacts" => [
			"mobile" => "+77059661651",
		],
		"TCs" => [
			[
				"brand" => "Honda ",
				"size" => "215/70/15",
				"gosNomer" => "A749HDO",
			],
		],
	],
	[
		"clientID" =>"CRM060049",
		"name" =>"Азат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057262626",
		],
	],
	[
		"clientID" =>"CRM060060",
		"name" =>"87001167378",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77001167378",
		],
	],
	[
		"clientID" =>"CRM060052",
		"name" =>"Толя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "atakaent@mail.ru",
		],
	],
	[
		"clientID" =>"CRM060054",
		"name" =>"Алексей 221",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "alex060173@mail.ru",
			"mobile" => "+77711919491",
		],
	],
	[
		"clientID" =>"CRM060080",
		"name" =>"Шохерив Николай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77776400695",
		],
	],
	[
		"clientID" =>"CRM060077",
		"name" =>"Гуков Владимир Владимирович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771625406",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiDelica",
				"size" => "235/75/15",
				"gosNomer" => "A911URN",
			],
		],
	],
	[
		"clientID" =>"CRM060082",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "maiski176@gmail.com",
			"mobile" => "+77770081801",
		],
	],
	[
		"clientID" =>"CRM060092",
		"name" =>"lamborghini diablo 2016",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060098",
		"name" =>"Валерий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "v.mihey@mail.ru",
			"mobile" => "+77071438126",
		],
	],
	[
		"clientID" =>"CRM060108",
		"name" =>"Асель R13",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016855252",
		],
	],
	[
		"clientID" =>"CRM060109",
		"name" =>"Амир Golf 7",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073932700",
		],
	],
	[
		"clientID" =>"CRM060111",
		"name" =>"Иван Мазда Дуратрак",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013505055",
		],
	],
	[
		"clientID" =>"CRM060126",
		"name" =>"Камиль 31",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77479013147",
		],
	],
	[
		"clientID" =>"CRM060135",
		"name" =>"Ербол камри 50",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77085150484",
		],
	],
	[
		"clientID" =>"CRM060154",
		"name" =>"Джумагазиев Арсланбек Абдулхазиретович",
		"attribute" => "Физ. лицо",
		"IIN" => "821107302207",
		"contacts" => [
			"mobile" => "+77017388853",
			"adress" => "Актау",
		],
	],
	[
		"clientID" =>"CRM060180",
		"name" =>"Ильященко Сергей Иванович",
		"attribute" => "Физ. лицо",
		"IIN" => "680421300792",
		"contacts" => [
			"E-Mail" => "variteks.kz@mail.ru",
			"mobile" =>"+",
			"mobile" => "+77772355069",
			"adress" => "Пушкина 13. Но въезд с улицы Валиханова Чуть ниже Маметовой на право. Телефон кто встретит и заберёт +77071800302 Павел ",
		],
	],
	[
		"clientID" =>"CRM060201",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012453301",
		],
	],
	[
		"clientID" =>"CRM060206",
		"name" =>"Исаев Петр Яковливич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017329148",
		],
	],
	[
		"clientID" =>"CRM060212",
		"name" =>"игорь",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77476266596",
		],
	],
	[
		"clientID" =>"CRM060221",
		"name" =>"Векелин Алексей Александрович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vekelin_aa@mail.ru",
			"mobile" => "+77013958041",
		],
	],
	[
		"clientID" =>"CRM060225",
		"name" =>"Козюрин Марк Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760124302028",
		"contacts" => [
			"mobile" => "+77059820604",
			"adress" => "Астана",
		],
	],
	[
		"clientID" =>"CRM060228",
		"name" =>"Вольняга Василий Сергеевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77059653370",
			"adress" => "Нижняя Пятилетка, уг Тополеского- Суворова 126",
		],
	],
	[
		"clientID" =>"CRM060232",
		"name" =>"Медет",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "medet056@gmail.com",
			"mobile" => "+77024744383",
		],
	],
	[
		"clientID" =>"CRM060236",
		"name" =>"Павел",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "pak123@mail.kz",
			"mobile" =>"+77272275822",
		],
	],
	[
		"clientID" =>"CRM060238",
		"name" =>"87763301020 костя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77763301020",
		],
	],
	[
		"clientID" =>"CRM060239",
		"name" =>"87763301020 костя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77763301020",
		],
	],
	[
		"clientID" =>"CRM060240",
		"name" =>"87763301020 костя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77763301020",
		],
	],
	[
		"clientID" =>"CRM060241",
		"name" =>"Костя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77763301020",
		],
	],
	[
		"clientID" =>"CRM060256",
		"name" =>"Жанара",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zhanar.janakova@airastana.com",
			"mobile" => "+77017209115",
		],
	],
	[
		"clientID" =>"CRM060259",
		"name" =>"Кали Прадо",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773920707",
		],
	],
	[
		"clientID" =>"CRM060260",
		"name" =>"Кали Прадо",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773920707",
		],
	],
	[
		"clientID" =>"CRM060262",
		"name" =>"Иван",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "karataev@bt24.kz",
			"mobile" => "+77017427132",
		],
	],
	[
		"clientID" =>"CRM060272",
		"name" =>"Олег Ниссан Нот",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "olmark_93@mail.ru",
			"mobile" => "+77777017177",
		],
	],
	[
		"clientID" =>"CRM060273",
		"name" =>"Шнякин Николай Васильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840124300594",
		"contacts" => [
			"E-Mail" => "euro-truckkz@mail.ru",
			"mobile" =>"+77770969229",
			"adress" => "г. Шымкент, ул. Акназар хана 8",
		],
	],
	[
		"clientID" =>"CRM060296",
		"name" =>"Слава",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77071100229",
		],
	],
	[
		"clientID" =>"CRM060297",
		"name" =>"Слава",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77071100229",
		],
	],
	[
		"clientID" =>"CRM060298",
		"name" =>"Слава",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77071100229",
		],
	],
	[
		"clientID" =>"CRM060299",
		"name" =>"Родичев Вячеслав Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "870607300327",
		"contacts" => [
			"mobile" =>"+77071100229",
		],
	],
	[
		"clientID" =>"CRM060285",
		"name" =>"Сандыбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072001565",
		],
	],
	[
		"clientID" =>"CRM060303",
		"name" =>"Михаил лансер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773153218",
		],
	],
	[
		"clientID" =>"CRM060305",
		"name" =>"Александр Нексен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773595910",
		],
	],
	[
		"clientID" =>"CRM060319",
		"name" =>"Болат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ernar.orymbayev@gmail.com",
			"mobile" => "+77017655716",
			"adress" => "Астана, иманова 18/1 кв 28",
		],
	],
	[
		"clientID" =>"CRM060312",
		"name" =>"Лисунов Иван Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "840325300379",
		"contacts" => [
			"mobile" =>"+77012201256",
		],
	],
	[
		"clientID" =>"CRM060315",
		"name" =>"Ахметов Анатолий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060316",
		"name" =>"Ахметов Анатолий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060317",
		"name" =>"Ахметов Анатолий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060325",
		"name" =>"Михаил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Tolstunov.m@yandex.ru",
			"mobile" =>"+77029993523",
		],
	],
	[
		"clientID" =>"CRM060326",
		"name" =>"Асель кредит",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "s.assel.85@mail.ru",
			"mobile" => "+77081083198",
		],
	],
	[
		"clientID" =>"CRM060327",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060328",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060329",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060330",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060331",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM060332",
		"name" =>"Кузьменко Дмитрий Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "740120302607",
		"contacts" => [
			"mobile" => "+77475220788",
		],
	],
	[
		"clientID" =>"CRM060337",
		"name" =>"Геннадий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "utyf777.7070@mail.ru",
			"mobile" =>"+77253638800",
			"mobile" => "+77057222123",
		],
	],
	[
		"clientID" =>"CRM060336",
		"name" =>"Тимур 275/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77775552533",
		],
	],
	[
		"clientID" =>"CRM060339",
		"name" =>"Нурбол 215/60/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77011680141",
		],
	],
	[
		"clientID" =>"CRM060344",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77711918191",
			"adress" => "7711918191",
		],
	],
	[
		"clientID" =>"CRM060345",
		"name" =>"Равиль",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sol-minor@mail.ru",
			"mobile" =>"+77711918191",
		],
	],
	[
		"clientID" =>"CRM060352",
		"name" =>"Роман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013252582",
		],
	],
	[
		"clientID" =>"CRM060367",
		"name" =>"Николай Григорьевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011316354",
			"adress" => "Шымкент Тюлькубастский р-н, село Турар Рыскулова, ул Биназар Батыра 127",
		],
	],
	[
		"clientID" =>"CRM060371",
		"name" =>"Анатолий  скад",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773908644",
		],
	],
	[
		"clientID" =>"CRM060372",
		"name" =>"Барабаш Юрий Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "800921350417",
		"contacts" => [
			"E-Mail" => "elk-ya@mail.ru",
			"mobile" =>"+77712193596",
			"mobile" => "+77710810160",
			"adress" => "г. Петропавловск",
		],
	],
	[
		"clientID" =>"CRM060385",
		"name" =>"Пётр 245 55 19",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772559618",
		],
	],
	[
		"clientID" =>"CRM060387",
		"name" =>"Галина Эйкос",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77023222205",
		],
	],
	[
		"clientID" =>"CRM060388",
		"name" =>"Галина Эйкос",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77023222205",
		],
	],
	[
		"clientID" =>"CRM060389",
		"name" =>"Скутин Андрей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "720808300230",
		"contacts" => [
			"E-Mail" => "andrej-shkutin@yandex.ru",
			"mobile" =>"+77013872546",
			"adress" => "г. Алматы, Майлина 85",
		],
	],
	[
		"clientID" =>"CRM060382",
		"name" =>"Камиля",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kturekhanova@gmail.com",
			"mobile" => "+77777101247",
		],
	],
	[
		"clientID" =>"CRM060390",
		"name" =>"ТОО \"ТЕПЛОСЕТЬСТРОЙ\"",
		"attribute" => "Юр. лицо",
		"IIN" => "050540001391",
		"contacts" => [
			"mobile" => "+77077144144",
		],
	],
	[
		"clientID" =>"CRM060396",
		"name" =>"Даутов Ринат Сауытович",
		"attribute" => "Физ. лицо",
		"IIN" => "770617302931",
		"contacts" => [
			"mobile" => "+77014787776",
		],
	],
	[
		"clientID" =>"CRM060421",
		"name" =>"Дуйсебаев Даурен Кенжакынович",
		"attribute" => "Физ. лицо",
		"IIN" => "741023302256",
		"contacts" => [
			"mobile" => "+77016101425",
		],
	],
	[
		"clientID" =>"CRM060431",
		"name" =>"Ержан 5 бал",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773984639",
		],
	],
	[
		"clientID" =>"CRM060432",
		"name" =>"Ержан 5 бал",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773984639",
		],
	],
	[
		"clientID" =>"CRM060433",
		"name" =>"Ержан 5 бал",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773984639",
		],
	],
	[
		"clientID" =>"CRM060438",
		"name" =>"Онербек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015441430",
		],
	],
	[
		"clientID" =>"CRM060445",
		"name" =>"Жамагали камри",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075440554",
		],
	],
	[
		"clientID" =>"CRM060451",
		"name" =>"315/70/22,5",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014655555",
		],
	],
	[
		"clientID" =>"CRM060439",
		"name" =>"Чингиз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072360136",
		],
	],
	[
		"clientID" =>"CRM060440",
		"name" =>"Яков Иванович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772444063",
		],
	],
	[
		"clientID" =>"CRM060448",
		"name" =>"Карпунин Олег",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017417857",
		],
		"TCs" => [
			[
				"brand" => "VWCaddy",
				"size" => "195/65/15",
				"gosNomer" => "H350402",
			],
		],
	],
	[
		"clientID" =>"CRM060454",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77079701334",
		],
	],
	[
		"clientID" =>"CRM060457",
		"name" =>"Рыжова Александра Анатольевна",
		"attribute" => "Физ. лицо",
		"IIN" => "880923401153",
		"contacts" => [
			"mobile" => "+77022350272",
		],
	],
	[
		"clientID" =>"CRM060474",
		"name" =>"Данила Атырау тендер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77783192755",
		],
	],
	[
		"clientID" =>"CRM060483",
		"name" =>"Шилдебаев Алинур",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771791700",
		],
	],
	[
		"clientID" =>"CRM060503",
		"name" =>"Кудайберген",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77074933337",
		],
	],
	[
		"clientID" =>"CRM060504",
		"name" =>"Владислав",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772120016",
		],
	],
	[
		"clientID" =>"CRM054108",
		"name" => "Тамара  (РАФ-4)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77474615761",
		],
		"TCs" => [
			[
				"brand" => "RAV-4",
				"size" => "225/70/16",
				"gosNomer" => "135",
			],
		],
	],
	[
		"clientID" =>"CRM014192",
		"name" =>"Аллагулов Маршал",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77016666008",
		],
		"TCs" => [
			[
				"brand" => "BMW X5",
				"size" => "325/30/21",
				"gosNomer" => "199BZA",
			],
		],
	],
	[
		"clientID" =>"CRM052982",
		"name" => "Руслан  (хонда цивик)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774019437",
		],
		"TCs" => [
			[
				"brand" => "HondaCivic",
				"size" => "195/60/15",
				"gosNomer" => "384LHA/05",
			],
		],
	],
	[
		"clientID" =>"CRM048914",
		"name" => "Екатерина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052755566",
		],
		"TCs" => [
			[
				"brand" => "Skoda",
				"size" => "185/65/15",
				"gosNomer" => "177UTA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052584",
		"name" => "Азат (вшм)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77027404060",
		],
		"TCs" => [
			[
				"brand" => "Prado200",
				"size" => "285/60/15",
				
			],
		],
	],
	[
		"clientID" =>"CRM057354",
		"name" =>"Должонкова Наталия",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77074700022",
		],
		"TCs" => [
			[
				"brand" => "INFINITI",
				"size" => "245/50/20",
				"gosNomer" => "678PCA02",
			],
		],
	],
	[
		"clientID" =>"000001997",
		"name" =>"ЦОЙ ВЯЧЕСЛАВ ГРИГОРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "570817301929",
	],
	[
		"clientID" =>"000001998",
		"name" =>"КУРБАНОВ МОЛДИЯР БАХИТЖАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860327302855",
	],
	[
		"clientID" =>"000001999",
		"name" =>"МУХИН ЕВГЕНИЙ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "701013301978",
		"contacts" => [
			"mobile" => "+77019534031",
		],
	],
	[
		"clientID" =>"000002000",
		"name" =>"РАДЖАНОВ ДИЛШАД ШАКИРОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "850317301187",
	],
	[
		"clientID" =>"000002001",
		"name" =>"ЩЕРБИНИН ВИКТОР АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "660614301687",
		"contacts" => [
			"mobile" => "+77012229567",
		],
	],
	[
		"clientID" =>"000002002",
		"name" =>"Ершов Андрей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "880212300341",
		"contacts" => [
			"mobile" => "+77052618235",
		],
	],
	[
		"clientID" =>"000002003",
		"name" =>"Андреянова Зоя Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "871112400474",
		"contacts" => [
			"mobile" => "+77018386036",
		],
	],
	[
		"clientID" =>"000002004",
		"name" =>"Жакыпбаев Кумыролда Толеуович",
		"attribute" => "Физ. лицо",
		"IIN" => "690903300259",
		"contacts" => [
			"mobile" => "+77474962515",
		],
	],
	[
		"clientID" =>"000002005",
		"name" =>"Таубалдиева Жанна Жанатовна",
		"attribute" => "Физ. лицо",
		"IIN" => "811126400985",
		"contacts" => [
			"mobile" => "+77077527722",
		],
		"TCs" => [
			[
				"brand" => "ToyotaLandCruiserPrado",
				"size" => "265/65/17",
				"gosNomer" => "192 TKA/05",
			],
		],
	],
	[
		"clientID" =>"000002006",
		"name" =>"Базарова Алия Маратовна",
		"attribute" => "Физ. лицо",
		"IIN" => "770805400160",
	],
	[
		"clientID" =>"000002007",
		"name" =>"Ло Владимир Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "750303300388",
	],
	[
		"clientID" =>"000002008",
		"name" =>"Сайфутдинов Рауан Фаридович",
		"attribute" => "Физ. лицо",
		"IIN" => "910929300279",
	],
	[
		"clientID" =>"000002009",
		"name" =>"Худенко Артем Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "830722301561",
	],
	[
		"clientID" =>"000002010",
		"name" =>"Шипилов Олег Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780730300288",
		"contacts" => [
			"mobile" => "+77772231310",
		],
	],
	[
		"clientID" =>"000002011",
		"name" =>"Клочков Артем Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "890201300224",
		"contacts" => [
			"mobile" => "+77013165330",
		],
	],
	[
		"clientID" =>"000002014",
		"name" =>"Ерниязов Асет Бакытулы",
		"attribute" => "Физ. лицо",
		"IIN" => "950912300134",
		"contacts" => [
			"mobile" => "+77077297989",
		],
	],
	[
		"clientID" =>"000002012",
		"name" =>"Кутлиметов Ренат Рашидович",
		"attribute" => "Физ. лицо",
		"IIN" => "690819301687",
	],
	[
		"clientID" =>"000002013",
		"name" =>"Ахметкалиева   Раушан Сагидолдаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "730227401457",
		"contacts" => [
			"mobile" => "+77772558381",
		],
		"TCs" => [
			[
				"brand" => "BMWX3",
				"size" => "215/60/17",
				"gosNomer" => "A273EAP",
			],
		],
	],
	[
		"clientID" =>"000002015",
		"name" =>"Байбекова  Салтанат ",
		"attribute" => "Физ. лицо",
		"IIN" => "811002450818",
		"contacts" => [
			"mobile" =>"+77021002878",
			"mobile" => "+77021002878",
		],
		"TCs" => [
			[
				"brand" => "LexusRX350",
				"size" => "235/60/18",
				"gosNomer" => "953 DXA/02",
			],
		],
	],
	[
		"clientID" =>"000002016",
		"name" =>"Беженов Руслан Бейсенович",
		"attribute" => "Физ. лицо",
		"IIN" => "851003300927",
	],
	[
		"clientID" =>"000002017",
		"name" =>"Гришанов Дмитрий Николаевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77770242440",
		],
	],
	[
		"clientID" =>"000002018",
		"name" =>"Щепина Анна Юрьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "830716450474",
	],
	[
		"clientID" =>"000002019",
		"name" =>"Абенов Гани Абаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "801217302155",
	],
	[
		"clientID" =>"000002020",
		"name" =>"Мустафина Айнур Маркеновна",
		"attribute" => "Физ. лицо",
		"IIN" => "720626401424",
		"contacts" => [
			"mobile" =>"+77272257256",
			"mobile" => "+77272257256",
		],
	],
	[
		"clientID" =>"000002021",
		"name" =>"Зиябек Ансар Нурланулы",
		"attribute" => "Физ. лицо",
		"IIN" => "930531300080",
		"contacts" => [
			"mobile" => "+77770879292",
		],
	],
	[
		"clientID" =>"000002022",
		"name" =>"Бобер Дмитрий Мичиславович",
		"attribute" => "Физ. лицо",
		"IIN" => "781117301434",
		"contacts" => [
			"mobile" => "+77770103501",
		],
	],
	[
		"clientID" =>"000002024",
		"name" =>"Карпов Андрей Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "771216300774",
		"contacts" => [
			"mobile" => "+77714081836",
		],
	],
	[
		"clientID" =>"000002026",
		"name" =>"Мурат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714084995",
		],
	],
	[
		"clientID" =>"000002028",
		"name" =>"Тимофеева Александра Сергеевна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012080197",
		],
	],
	[
		"clientID" =>"000002029",
		"name" =>"Савченко Елена Витальевна",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000002030",
		"name" =>"Арман Астана",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77788000666",
		],
	],
	[
		"clientID" =>"000002031",
		"name" =>"Кенжебаев Алтай Абдрахманович",
		"attribute" => "Физ. лицо",
		"IIN" => "770126300976",
	],
	[
		"clientID" =>"000002032",
		"name" =>"Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775115100",
		],
	],
	[
		"clientID" =>"000002033",
		"name" =>"Тлегенов Берик Тлегенулы",
		"attribute" => "Физ. лицо",
		"IIN" => "720826300724",
		"contacts" => [
			"mobile" => "+77012606317",
		],
	],
	[
		"clientID" =>"000002034",
		"name" =>"Марат",
		"attribute" => "Физ. лицо",
		"IIN" => "710409301228",
		"contacts" => [
			"mobile" => "+77015180111",
		],
	],
	[
		"clientID" =>"000002036",
		"name" =>"Алишева Диана Ермековна",
		"attribute" => "Физ. лицо",
		"IIN" => "880420400011",
	],
	[
		"clientID" =>"000002037",
		"name" =>"Оразбаев Гани Халилуллович",
		"attribute" => "Физ. лицо",
		"IIN" => "741121300887",
		"contacts" => [
			"mobile" =>"+77017995951",
			"mobile" => "+77017995951",
		],
	],
	[
		"clientID" =>"000002038",
		"name" =>"Сатымбекова Адэль Манасовна",
		"attribute" => "Физ. лицо",
		"IIN" => "900505400595",
		"contacts" => [
			"mobile" => "+77055537722",
		],
	],
	[
		"clientID" =>"000002039",
		"name" =>"Абжанова Лилия Аскеровна",
		"attribute" => "Физ. лицо",
		"IIN" => "590702401874",
		"contacts" => [
			"mobile" => "+77018488272",
		],
	],
	[
		"clientID" =>"000002040",
		"name" =>"Погорелов Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017110442",
		],
	],
	[
		"clientID" =>"000002042",
		"name" =>"Югай Эдуард",
		"attribute" => "Юр. лицо",
		"IIN" => "561123399026",
		"contacts" => [
			"mobile" => "+77772520757",
		],
	],
	[
		"clientID" =>"000002043",
		"name" =>"Анар",
		"attribute" => "Физ. лицо",
		"IIN" => "731025399048",
		"contacts" => [
			"mobile" => "+77012441701",
		],
	],
	[
		"clientID" =>"000002044",
		"name" =>"Шапошников  Игорь  Витальевич",
		"attribute" => "Физ. лицо",
		"IIN" => "691226301245",
		"contacts" => [
			"mobile" => "+77014244417",
		],
		"TCs" => [
			[
				"brand" => "KIA",
				"size" => "225/60/17",
				"gosNomer" => "017EMA/01",
			],
		],
	],
	[
		"clientID" =>"000002045",
		"name" =>"Караваев Дмитрий Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "830228350118",
		"contacts" => [
			"mobile" => "+77015187221",
		],
	],
	[
		"clientID" =>"000002048",
		"name" =>"Булатова Мария Сыдыхановна",
		"attribute" => "Физ. лицо",
		"IIN" => "740719400306",
		"contacts" => [
			"mobile" => "+77012039727",
		],
	],
	[
		"clientID" =>"000002049",
		"name" =>"Дробышев Иван Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "830928351200",
		"contacts" => [
			"mobile" => "+77774123113",
		],
	],
	[
		"clientID" =>"000002050",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		"IIN" => "830305303104",
		"contacts" => [
			"mobile" => "+77772945094",
		],
	],
	[
		"clientID" =>"000002051",
		"name" =>"Усенов Ержан Асылбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "780317300044",
		"contacts" => [
			"mobile" => "+77018001703",
		],
	],
	[
		"clientID" =>"000002054",
		"name" =>"Даурбеков Магомед Русланович",
		"attribute" => "Физ. лицо",
		"IIN" => "930223300185",
		"contacts" => [
			"mobile" => "+77018114444",
		],
	],
	[
		"clientID" =>"000002057",
		"name" =>"Матвеев  Сергей  Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760822300402",
		"contacts" => [
			"mobile" => "+77017685781",
		],
		"TCs" => [
			[
				"brand" => "ToyotaLC150",
				"size" => "265/65/17",
				"gosNomer" => "883 VCA/02",
			],
		],
	],
	[
		"clientID" =>"000002058",
		"name" =>"Зублюк Денис Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "800924302582",
		"contacts" => [
			"mobile" => "+77078861003",
		],
	],
	[
		"clientID" =>"000002060",
		"name" =>"Бегляков Виктор",
		"attribute" => "Физ. лицо",
		"IIN" => "890428300992",
		"contacts" => [
			"mobile" => "+77052236216",
		],
	],
	[
		"clientID" =>"000002061",
		"name" =>"Владимир",
		"attribute" => "Физ. лицо",
		"IIN" => "650107300899",
		"contacts" => [
			"mobile" => "+77015317261",
		],
	],
	[
		"clientID" =>"000002062",
		"name" =>"Цой Артур Афанасьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "771118300727",
		"contacts" => [
			"mobile" => "+77017779754",
		],
	],
	[
		"clientID" =>"000002063",
		"name" =>"Силин Игорь",
		"attribute" => "Физ. лицо",
		"IIN" => "770731300013",
		"contacts" => [
			"mobile" => "+77017316053",
		],
	],
	[
		"clientID" =>"000002065",
		"name" =>"Прахов Станислав Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "721031300228",
	],
	[
		"clientID" =>"000002066",
		"name" =>"Абдуллаев Алмат Гуванбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760506300226",
		"contacts" => [
			"mobile" => "+77051741401",
		],
	],
	[
		"clientID" =>"000002067",
		"name" =>"Ким Сергей Эдуардович",
		"attribute" => "Физ. лицо",
		"IIN" => "711105399030",
		"contacts" => [
			"mobile" => "+77015115140",
		],
		"TCs" => [
			[
				"brand" => "ChevroletLaccetti",
				"size" => "195/55/15",
				"gosNomer" => "A 539 ZSO",
			],
		],
	],
	[
		"clientID" =>"000002069",
		"name" =>"Бобрик  Владислав  Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "741031300898",
		"contacts" => [
			"mobile" => "+77017801574",
		],
		"TCs" => [
			[
				"brand" => "ToyotaLC",
				"size" => "265/70/16",
				"gosNomer" => "A355YSN",
			],
			[
				"brand" => "Toyota LC",
				"size" => "265/70/16",
				"gosNomer" => "A355YSN",
			],
			[
				"brand" => "Toyota Land Cruiser Prado",
				"size" => "265/65/17",
				"gosNomer" => "A 355 YSN",
			],
		],
	],
	[
		"clientID" =>"000002070",
		"name" =>"Еремин Александр Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760831300233",
		"contacts" => [
			"mobile" => "+77772558432",
		],
	],
	[
		"clientID" =>"000002071",
		"name" =>"Кулекеев Бахытбек Абдрахметович",
		"attribute" => "Физ. лицо",
		"IIN" => "600315300395",
		"contacts" => [
			"mobile" => "+77017330726",
		],
	],
	[
		"clientID" =>"000002072",
		"name" =>"Кенжитаева Лана",
		"attribute" => "Физ. лицо",
		"IIN" => "680720400863",
		"contacts" => [
			"mobile" => "+77017528497",
		],
	],
	[
		"clientID" =>"000002073",
		"name" =>"Куринов Дмитрий",
		"attribute" => "Физ. лицо",
		"IIN" => "751204301223",
	],
	[
		"clientID" =>"000002075",
		"name" =>"Есентимиров Аскер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017255102",
		],
		"TCs" => [
			[
				"brand" => "NissanVersa",
				"size" => "185/65/15",
				"gosNomer" => "792 DTA/02",
			],
		],
	],
	[
		"clientID" =>"000002076",
		"name" =>"Дрожжин Илья Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "790524301367",
		"contacts" => [
			"mobile" => "+77775250565",
		],
	],
	[
		"clientID" =>"000002077",
		"name" =>"Орынбеков Ербол Орыебекович",
		"attribute" => "Физ. лицо",
		"IIN" => "740412302565",
		"contacts" => [
			"mobile" => "+77072253135",
		],
	],
	[
		"clientID" =>"000002078",
		"name" =>"Кабдолданов Аслан Пальмирович",
		"attribute" => "Физ. лицо",
		"IIN" => "920805350748",
		"contacts" => [
			"mobile" => "+77757262727",
		],
	],
	[
		"clientID" =>"000002079",
		"name" =>"Бапишев Бахыт",
		"attribute" => "Физ. лицо",
		"IIN" => "580327300275",
		"contacts" => [
			"mobile" => "+77783557278",
		],
	],
	[
		"clientID" =>"000002080",
		"name" =>"Габбасов Аскар Калиолланович",
		"attribute" => "Физ. лицо",
		"IIN" => "781215302831",
		"contacts" => [
			"mobile" => "+77074805286",
		],
	],
	[
		"clientID" =>"000002085",
		"name" =>"Каипбай Бауржан Абдалиулы",
		"attribute" => "Физ. лицо",
		"IIN" => "800717300035",
		"contacts" => [
			"mobile" => "+77752812493",
		],
	],
	[
		"clientID" =>"000002086",
		"name" =>"Туржекенов Хайдар Оразович",
		"attribute" => "Физ. лицо",
		"IIN" => "731121300370",
		"contacts" => [
			"mobile" => "+77073515881",
		],
	],
	[
		"clientID" =>"000002088",
		"name" =>"Вячеслав",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013912845",
		],
	],
	[
		"clientID" =>"000002091",
		"name" =>"Мартынов Андрей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "880109300024",
		"contacts" => [
			"mobile" => "+77019934299",
		],
	],
	[
		"clientID" =>"000002093",
		"name" =>"Турганов Ришат Джалалдинович",
		"attribute" => "Физ. лицо",
		"IIN" => "840724302473",
		"contacts" => [
			"mobile" => "+77075505057",
		],
	],
	[
		"clientID" =>"000002098",
		"name" =>"Атикеев Руслан Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "740613350598",
	],
	[
		"clientID" =>"000002115",
		"name" =>"Кривенко Светлана Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "770126400030",
		"contacts" => [
			"mobile" => "+77019242421",
		],
	],
	[
		"clientID" =>"000002119",
		"name" =>"Якупов Саят Хамитович",
		"attribute" => "Физ. лицо",
		"IIN" => "860128303118",
		"contacts" => [
			"mobile" => "+77018887812",
		],
	],
	[
		"clientID" =>"000002121",
		"name" =>"Лущиков Павел Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "890207300168",
		"contacts" => [
			"mobile" => "+77071937393",
		],
	],
	[
		"clientID" =>"000002125",
		"name" =>"Кельвер Виктор Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "880103300150",
		"contacts" => [
			"mobile" => "+77776920642",
		],
	],
	[
		"clientID" =>"000002128",
		"name" =>"Айтбаев Булатбек Кабланбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "750101306142",
		"contacts" => [
			"mobile" => "+77015200200",
		],
	],
	[
		"clientID" =>"000002129",
		"name" =>"Иминов Тельман Зарипович",
		"attribute" => "Физ. лицо",
		"IIN" => "660903301678",
		"contacts" => [
			"mobile" => "+77018836060",
		],
	],
	[
		"clientID" =>"000000216",
		"name" =>"Копылов Денис Владимирович",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000017",
		"name" =>"Копенкин А.Н.",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000907",
		"name" =>"Ивакин Владимир Михайлович",
		"attribute" => "Юр. лицо",
		"IIN" => "52032730154",
		"contacts" => [
			"mobile" => "+77014349845",
		],
	],
	[
		"clientID" =>"000000911",
		"name" =>"СИМОНОВ ЮРИЙ МИХАЙЛОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "500701300520",
		"contacts" => [
			"mobile" => "+77777907777",
		],
	],
	[
		"clientID" =>"000000912",
		"name" =>"Погудин Павел Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "701011300118",
		"contacts" => [
			"mobile" => "+77017254594",
		],
	],
	[
		"clientID" =>"000000914",
		"name" =>"КАРТМАНБЕТОВА ЭЛЬМИРА МАРАТОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "760420450281",
		"contacts" => [
			"mobile" => "+77019087434",
		],
	],
	[
		"clientID" =>"000000915",
		"name" =>"Сергей",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77076069107",
		],
	],
	[
		"clientID" =>"000000918",
		"name" => "Борис  (Ниссан А429)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772126938",
		],
		"TCs" => [
			[
				"brand" => "Nissan",
				"size" => "235/75/15",
				"gosNomer" => "A429YIM",
			],
		],
	],
	[
		"clientID" =>"000000921",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77058626294",
		],
	],
	[
		"clientID" =>"000000923",
		"name" =>"Мачин  Геннадий  Николаевич (Дорград)",
		"attribute" => "Физ. лицо",
		"IIN" => "490120302257",
		"contacts" => [
			"mobile" => "+77023330734",
		],
		"TCs" => [
			[
				"brand" => "Honda",
				"size" => "235/70/16",
				"gosNomer" => "A389HM",
			],
		],
	],
	[
		"clientID" =>"000000924",
		"name" =>"Абдуали Абдугали Амангельдыулы",
		"attribute" => "Физ. лицо",
		"IIN" => "780928302082",
		"contacts" => [
			"mobile" => "+77789358635",
		],
	],
	[
		"clientID" =>"000000644",
		"name" =>"Омирсерикова Айнаш",
		"attribute" => "Юр. лицо",
		"IIN" => "840420402191",
		"contacts" => [
			"mobile" => "+77019595937",
		],
		"TCs" => [
			[
				"brand" => "Shevrole Cruze",
				"size" => "215/50/17",
				"gosNomer" => "187 LRA/02",
			],
		],
	],
	[
		"clientID" =>"000000645",
		"name" =>"Матвеева Наталья",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772269668",
		],
	],
	[
		"clientID" =>"000000648",
		"name" =>"Садыков Марат Гарифович",
		"attribute" => "Физ. лицо",
		"IIN" => "820617399024",
		"contacts" => [
			"mobile" => "+77017614045",
		],
		"TCs" => [
			[
				"brand" => "ToyotaSurf",
				"size" => "265/70/16",
				"gosNomer" => "H 779690",
			],
		],
	],
	[
		"clientID" =>"000000651",
		"name" =>"Менсеитов Талгат",
		"attribute" => "Физ. лицо",
		"IIN" => "800131300218",
		"contacts" => [
			"mobile" => "+77019854990",
		],
	],
	[
		"clientID" =>"000000654",
		"name" =>"Калмурзин Данияр Аскарович",
		"attribute" => "Физ. лицо",
		"IIN" => "830917300216",
		"contacts" => [
			"mobile" => "+77774917630",
		],
	],
	[
		"clientID" =>"000000655",
		"name" =>"Полянский Сергей Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "790918301735",
		"contacts" => [
			"mobile" => "870277711104",
		],
		"TCs" => [
			[
				"brand" => "DaewooCentra",
				"size" => "195/55/15",
				"gosNomer" => "478 MFA/02",
			],
		],
	],
	[
		"clientID" =>"000000656",
		"name" =>"Ахметов Арман Абушарипович",
		"attribute" => "Физ. лицо",
		"IIN" => "730711300255",
		"contacts" => [
			"mobile" => "+77012118172",
		],
	],
	[
		"clientID" =>"000000657",
		"name" =>"Алхаров Азамат Абдалимбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "860522303477",
		"contacts" => [
			"mobile" => "+77017652354",
		],
	],
	[
		"clientID" =>"000000658",
		"name" =>"Нурлан Улан Нурланулы",
		"attribute" => "Физ. лицо",
		"IIN" => "880411300521",
		"contacts" => [
			"mobile" => "+77714127468",
		],
	],
	[
		"clientID" =>"000000659",
		"name" =>"Абильдинов Дархан",
		"attribute" => "Физ. лицо",
		"IIN" => "810324350136",
		"contacts" => [
			"mobile" =>"+77012230701",
			"mobile" => "+77012230701",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCorolla",
				"size" => "205/55/16",
				"gosNomer" => "383 CZA/02",
			],
			[
				"brand" => "Toyota Land Cruiser",
				"size" => "275/65/17",
				"gosNomer" => "016 NRA/02",
			],
		],
	],
	[
		"clientID" =>"000000660",
		"name" =>"Салимов  Ертай",
		"attribute" => "Физ. лицо",
		"IIN" => "740408300011",
		"contacts" => [
			"mobile" => "+77772115449",
		],
		"TCs" => [
			[
				
				"size" => "285/50/20",
				
			],
			[
				"brand" => "Lexus570",
				"size" => "285/50/20",
				"gosNomer" => "337CVA/02",
			],
		],
	],
	[
		"clientID" =>"000000661",
		"name" =>"Алибеков  Марат",
		"attribute" => "Физ. лицо",
		"IIN" => "830815300618",
		"contacts" => [
			"mobile" => "+77017708800",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "215/60/16",
				"gosNomer" => "548DMA",
			],
		],
	],
	[
		"clientID" =>"000000662",
		"name" =>"Жуматулы Кылыш",
		"attribute" => "Физ. лицо",
		"IIN" => "780505302984",
		"contacts" => [
			"mobile" => "+77751110001",
		],
		"TCs" => [
			[
				"brand" => "AUDIA6",
				"size" => "215/55/16",
				"gosNomer" => "A 772 DOP",
			],
		],
	],
	[
		"clientID" =>"000000663",
		"name" =>"Гашникова  Александра",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773783325",
		],
		"TCs" => [
			[
				"brand" => "NissanQashqai",
				"size" => "215/65/16",
				"gosNomer" => "783DPA/02",
			],
		],
	],
	[
		"clientID" =>"000000664",
		"name" =>"Дронин Александр Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "820523300966",
		"contacts" => [
			"mobile" => "+77773555577",
		],
	],
	[
		"clientID" =>"000000665",
		"name" =>"Горкунова Виктория Валерьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "670318401705",
		"contacts" => [
			"mobile" => "+77071155154",
		],
	],
	[
		"clientID" =>"000000666",
		"name" =>"Аманбаев Ерлан Абдирахманович",
		"attribute" => "Физ. лицо",
		"IIN" => "841018300967",
		"contacts" => [
			"mobile" => "+77077555448",
		],
	],
	[
		"clientID" =>"000000667",
		"name" =>"Кокшинова   Оксана Юрьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "720420402472",
		"contacts" => [
			"mobile" => "+77051994361",
		],
		"TCs" => [
			[
				"brand" => "ToyotaPrius",
				"size" => "185/65/15",
				"gosNomer" => "A744XUO",
			],
		],
	],
	[
		"clientID" =>"000000668",
		"name" =>"Отаров Ербол",
		"attribute" => "Физ. лицо",
		"IIN" => "880724300058",
		"contacts" => [
			"mobile" => "870167444449",
		],
	],
	[
		"clientID" =>"000000670",
		"name" =>"Терещенко Алексей",
		"attribute" => "Физ. лицо",
		"IIN" => "520422300369",
		"contacts" => [
			"mobile" => "+77778010290",
		],
		"TCs" => [
			[
				"brand" => "HondaCR-V",
				"size" => "225/60/18",
				"gosNomer" => "795 FZA/02",
			],
		],
	],
	[
		"clientID" =>"000000671",
		"name" =>"Балтабаев Ерболат",
		"attribute" => "Физ. лицо",
		"IIN" => "750908302018",
		"contacts" => [
			"mobile" =>"+77273078035",
			"mobile" => "+77017472080",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "215/55/17",
				"gosNomer" => "101CEA/05",
			],
		],
	],
	[
		"clientID" =>"000000672",
		"name" =>"Муслим Рашид",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77785395199",
		],
	],
	[
		"clientID" =>"000000673",
		"name" =>"Подберезкина Ольга Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "890120400528",
		"contacts" => [
			"mobile" => "+77075256666",
		],
	],
	[
		"clientID" =>"000000674",
		"name" =>"Хорешко Владислав Владимирович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078292555",
		],
	],
	[
		"clientID" =>"000000676",
		"name" =>"Умыткажиев Куандык Серриканович",
		"attribute" => "Физ. лицо",
		"IIN" => "890302301053",
		"contacts" => [
			"mobile" => "+77011262948",
		],
	],
	[
		"clientID" =>"000000677",
		"name" =>"Ирина     (Алекса Сергей Андреевич) ",
		"attribute" => "Физ. лицо",
		"IIN" => "851109300438",
		"contacts" => [
			"mobile" => "+77772357301",
		],
		"TCs" => [
			[
				"brand" => "SkodaRapid",
				"size" => "195/60/15",
				"gosNomer" => "112ZLA/02",
			],
			[
				"brand" => "Toyota",
				"size" => "195/60/15",
				"gosNomer" => "A080BPO",
			],
		],
	],
	[
		"clientID" =>"000000678",
		"name" =>"КАДЫРОВ ИЛЬДАР НАДЖАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "720708301265",
		"contacts" => [
			"mobile" => "+77019777070",
		],
	],
	[
		"clientID" =>"000000679",
		"name" =>"Кушкарова Альбина",
		"attribute" => "Физ. лицо",
		"IIN" => "900527400034",
		"contacts" => [
			"mobile" => "+77011008718",
		],
		"TCs" => [
			[
				"brand" => "Honda Tucson",
				"size" => "225/60/17",
				"gosNomer" => "303 KUA/02",
			],
		],
	],
	[
		"clientID" =>"000000680",
		"name" =>"АБИЛОВ КАЙРАТ ТУРСУНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "710802301849",
		"contacts" => [
			"mobile" => "+77025555262",
		],
	],
	[
		"clientID" =>"000000685",
		"name" =>"Ерофеева  Маргарита  Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "690110400373",
		"contacts" => [
			"mobile" => "+77772925692",
		],
		"TCs" => [
			[
				"brand" => "ToyotaSpacio",
				"size" => "185/65/14",
				"gosNomer" => "A287NTN",
			],
			[
				"brand" => "ToyotaSpacio",
				"size" => "185/60/14",
				"gosNomer" => "A287NTN",
			],
		],
	],
	[
		"clientID" =>"000000687",
		"name" =>"СТРУТОВСКИЙ ИГОРЬ ЮРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "840520300745",
		"contacts" => [
			"E-Mail" => "igor-s-@mail.ru",
			"mobile" =>"+77057123255",
			"mobile" => "+77057123255",
		],
	],
	[
		"clientID" =>"000000689",
		"name" =>"Жаркова Галина   (тойота)",
		"attribute" => "Физ. лицо",
		"IIN" => "630805403018",
		"contacts" => [
			"mobile" => "+77777050443",
		],
		"TCs" => [
			[
				"brand" => "ToyotaSpacio",
				"size" => "185/65/14",
				"gosNomer" => "A250LSN",
			],
			[
				
				"size" => "185/65/14",
				
			],
		],
	],
	[
		"clientID" =>"000000690",
		"name" =>"Алимов Улан Ермуханович",
		"attribute" => "Физ. лицо",
		"IIN" => "850625301200",
		"contacts" => [
			"mobile" => "+77015352034",
		],
	],
	[
		"clientID" =>"000000765",
		"name" =>"БЕКТУРЕЕВА АТИНА АУЕЛБЕКОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "750314401700",
		"contacts" => [
			"mobile" => "8777261080287772610805",
		],
	],
	[
		"clientID" =>"000000766",
		"name" =>"Жубанкулов Амир Муратович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019912060",
		],
	],
	[
		"clientID" =>"000000769",
		"name" =>"Шаповалова Юлия Валерьевна",
		"attribute" => "Юр. лицо",
		"IIN" => "770714402896",
	],
	[
		"clientID" =>"000000771",
		"name" =>"Хегай Валентин Владирович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771455200",
		],
	],
	[
		"clientID" =>"000000773",
		"name" =>"Свирский    Андрей   Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "582121519655",
		"contacts" => [
			"mobile" => "+77051708129",
		],
		"TCs" => [
			[
				"brand" => "SUZUKI",
				"size" => "225/65/17",
				"gosNomer" => "A140VVO",
			],
		],
	],
	[
		"clientID" =>"000000774",
		"name" =>"Поздняков Евгений",
		"attribute" => "Физ. лицо",
		"IIN" => "820111300609",
		"contacts" => [
			"mobile" => "+77015318063",
		],
	],
	[
		"clientID" =>"000000775",
		"name" =>"Феклистов Роман Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "831110300093",
		"contacts" => [
			"mobile" => "+77775481508",
		],
	],
	[
		"clientID" =>"000000777",
		"name" =>"Итыбаева Асем",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "8702999755487029990559",
		],
	],
	[
		"clientID" =>"000000779",
		"name" =>"Ан Владимир Владимирович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011115802",
		],
	],
	[
		"clientID" =>"000000780",
		"name" =>"Резников Дмитрий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "730510300321",
		"contacts" => [
			"mobile" => "+77053302121",
		],
	],
	[
		"clientID" =>"000000782",
		"name" =>"Шопанов Санжар Серикович",
		"attribute" => "Физ. лицо",
		"IIN" => "890427300362",
		"contacts" => [
			"mobile" => "+77013559754",
		],
	],
	[
		"clientID" =>"000000783",
		"name" =>"Василенко Виктор",
		"attribute" => "Физ. лицо",
		"IIN" => "621110300327",
		"contacts" => [
			"mobile" => "+77017116789",
		],
	],
	[
		"clientID" =>"000000784",
		"name" =>"Махметова Мариам Танаткановна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "8701210633887015435755",
		],
		"TCs" => [
			[
				"brand" => "RangeRover",
				"size" => "275/45/20",
				"gosNomer" => "837KMA02",
			],
		],
	],
	[
		"clientID" =>"000000785",
		"name" =>"Агибаев Малик Калиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "770406301351",
		"contacts" => [
			"mobile" => "+77017554901",
		],
	],
	[
		"clientID" =>"000000788",
		"name" =>"Хоботов Евгений Игоревич",
		"attribute" => "Физ. лицо",
		"IIN" => "871230300162",
		"contacts" => [
			"mobile" => "+77771811840",
		],
	],
	[
		"clientID" =>"000000789",
		"name" =>"Нигмеджанова Толкин",
		"attribute" => "Физ. лицо",
		"IIN" => "671017400044",
		"contacts" => [
			"mobile" => "+77017228641",
		],
	],
	[
		"clientID" =>"000000791",
		"name" =>"Богачев Владимир Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "691001300496",
		"contacts" => [
			"mobile" => "+77017540322",
		],
	],
	[
		"clientID" =>"000000792",
		"name" =>"Ходченко Ольга",
		"attribute" => "Физ. лицо",
		"IIN" => "690929401999",
		"contacts" => [
			"mobile" => "+77017634920",
		],
	],
	[
		"clientID" =>"000000795",
		"name" =>"Ильинская Виктория",
		"attribute" => "Юр. лицо",
		"IIN" => "840904402061",
	],
	[
		"clientID" =>"000000796",
		"name" =>"Рыжов Александр Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "791027300518",
		"contacts" => [
			"mobile" => "+77017870058",
		],
	],
	[
		"clientID" =>"000000798",
		"name" =>"Зольников Юрий Игоревич",
		"attribute" => "Физ. лицо",
		"IIN" => "760623303089",
		"contacts" => [
			"mobile" => "+77714037351",
		],
	],
	[
		"clientID" =>"000000799",
		"name" =>"Ботбаев Даняир Байбулатович",
		"attribute" => "Физ. лицо",
		"IIN" => "790907301775",
		"contacts" => [
			"mobile" => "+77775520264",
		],
		"TCs" => [
			[
				"brand" => "KIASportage",
				"size" => "225/60/16",
				"gosNomer" => "442LZA",
			],
		],
	],
	[
		"clientID" =>"000000800",
		"name" =>"Халилов Марат Фазылович",
		"attribute" => "Физ. лицо",
		"IIN" => "470724300637",
		"contacts" => [
			"mobile" => "+77051845728",
		],
	],
	[
		"clientID" =>"000000801",
		"name" =>"Орыщенко Владимир Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "670902300202",
		"contacts" => [
			"E-Mail" => "v_kox@mail.ru",
			"mobile" =>"+77019576875",
			"mobile" => "+77019576875",
		],
	],
	[
		"clientID" =>"000000804",
		"name" =>"Кадыраджиев Ильшат Гапурович",
		"attribute" => "Физ. лицо",
		"IIN" => "640615300393",
		"contacts" => [
			"mobile" => "+77772146720",
		],
	],
	[
		"clientID" =>"000000805",
		"name" =>"Кетегенов Бекайдар Сагиндыклвич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073839999",
		],
	],
	[
		"clientID" =>"000000806",
		"name" =>"Титимов Саят",
		"attribute" => "Физ. лицо",
		"IIN" => "860602351322",
		"contacts" => [
			"mobile" => "+77013535741",
		],
	],
	[
		"clientID" =>"000000807",
		"name" =>"Стефанович Алла Амангельдиновна",
		"attribute" => "Физ. лицо",
		"IIN" => "831211400366",
		"contacts" => [
			"mobile" => "+77777288884",
		],
	],
	[
		"clientID" =>"000000808",
		"name" =>"Космынин Виталий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "830212300055",
		"contacts" => [
			"mobile" => "+77071056129",
		],
	],
	[
		"clientID" =>"000000809",
		"name" =>"Нанаров Ерлан",
		"attribute" => "Физ. лицо",
		"IIN" => "850811302051",
		"contacts" => [
			"mobile" => "+77019678560",
		],
	],
	[
		"clientID" =>"000000810",
		"name" =>"ИНТЕМИРОВ ЕРЖАН КЕНЖЕБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860731300518",
		"contacts" => [
			"mobile" => "+77777823011",
		],
	],
	[
		"clientID" =>"000000811",
		"name" =>"Такабаев Тимур",
		"attribute" => "Физ. лицо",
		"IIN" => "881122300018",
		"contacts" => [
			"mobile" => "+77011115924",
		],
	],
	[
		"clientID" =>"000000815",
		"name" =>"Кенштыбаев Айдар",
		"attribute" => "Физ. лицо",
		"IIN" => "831202302624",
		"contacts" => [
			"mobile" => "+77072244133",
		],
	],
	[
		"clientID" =>"000000816",
		"name" =>"Абдугазиев Мажит Рашидович",
		"attribute" => "Физ. лицо",
		"IIN" => "861118301794",
		"contacts" => [
			"mobile" => "+77777050989",
		],
	],
	[
		"clientID" =>"000000817",
		"name" =>"Тоиров Алишер",
		"attribute" => "Физ. лицо",
		"IIN" => "780104399072",
		"contacts" => [
			"mobile" => "+77751122222",
		],
	],
	[
		"clientID" =>"000000818",
		"name" =>"Заноза Валерий Иванович",
		"attribute" => "Физ. лицо",
		"IIN" => "580107300833",
		"contacts" => [
			"mobile" => "+77058612417",
		],
	],
	[
		"clientID" =>"000000819",
		"name" =>"Карамурзанов Ануар Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "791215302060",
	],
	[
		"clientID" =>"000000820",
		"name" =>"Кадыров Бахытжан Орманович",
		"attribute" => "Физ. лицо",
		"IIN" => "820505300899",
	],
	[
		"clientID" =>"000000613",
		"name" =>"Погуляев Олег Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "851226301303",
		"contacts" => [
			"mobile" => "+77059923465",
		],
	],
	[
		"clientID" =>"000000614",
		"name" =>"Конаныхин Илья Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "850821300199",
		"contacts" => [
			"mobile" => "+77052240258",
		],
	],
	[
		"clientID" =>"000000615",
		"name" =>"Аманжанов Саламат Жумагалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "640116301814",
		"contacts" => [
			"mobile" => "+77711211217",
		],
		"TCs" => [
			[
				"brand" => "ToyotaRAV_4",
				"size" => "225/70/16",
				"gosNomer" => "075 KHA/02",
			],
		],
	],
	[
		"clientID" =>"000000616",
		"name" =>"Искаков Илья Надирович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77053134646",
		],
	],
	[
		"clientID" =>"000000618",
		"name" =>"Абдикаримов Сакен Миркашевич",
		"attribute" => "Физ. лицо",
		"IIN" => "730617300353",
		"contacts" => [
			"mobile" => "+77013497399",
		],
	],
	[
		"clientID" =>"000000619",
		"name" =>"Жунускалиев Азамат Серикович",
		"attribute" => "Физ. лицо",
		"IIN" => "890116301439",
		"contacts" => [
			"mobile" => "+77774006535",
		],
	],
	[
		"clientID" =>"000000620",
		"name" =>"Васкиев Михаил Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "620303300274",
		"contacts" => [
			"mobile" => "+77772102105",
		],
		"TCs" => [
			[
				"brand" => "InfinitiJX35",
				"size" => "235/55/20",
				"gosNomer" => "841 BUA/02",
			],
		],
	],
	[
		"clientID" =>"000000621",
		"name" =>"Усаков Сергей Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "800405301710",
		"contacts" => [
			"mobile" => "+77017220215",
		],
	],
	[
		"clientID" =>"000000622",
		"name" =>"Щукин Олег Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "650501302669",
		"contacts" => [
			"mobile" => "+77078370017",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiASX",
				"size" => "215/60/17",
				"gosNomer" => "730 DKA/02",
		],
	],
	[
		"clientID" =>"000000624",
		"name" =>"Каштанов Вадим Анатольевич",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000000625",
		"name" =>"Иванова Мария Леонидовна",
		"attribute" => "Физ. лицо",
		"IIN" => "821218400283",
		"contacts" => [
			"mobile" => "+77772300251",
		],
	],
	[
		"clientID" =>"000000626",
		"name" =>"Санников Игорь Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "750228300431",
		"contacts" => [
			"mobile" => "+77772416628",
		],
	],
	[
		"clientID" =>"000000627",
		"name" =>"Рогозин  Иван  Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "480407300952",
		"contacts" => [
			"mobile" => "+77017324662",
		],
		"TCs" => [
			[
				"brand" => "KiaCerato",
				"size" => "185/65/15",
				"gosNomer" => "A 839 OSO",
			],
		],
	],
	[
		"clientID" =>"000000630",
		"name" =>"Круковский Александр Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "861227300194",
		"contacts" => [
			"mobile" => "+77052668656",
		],
	],
	[
		"clientID" =>"000000631",
		"name" =>"Мамбеталиев Камчыбек",
		"attribute" => "Физ. лицо",
		"IIN" => "720408399077",
		"contacts" => [
			"mobile" => "+77013325211",
		],
		"TCs" => [
			[
				
				"size" => "225/70/16",
				
			],
		],
	],
	[
		"clientID" =>"000000632",
		"name" =>"Раимбекова  Жанна  Еренгаифовна",
		"attribute" => "Физ. лицо",
		"IIN" => "790224401759",
		"contacts" => [
			"mobile" =>"+77056614444",
			"mobile" => "+77056614444",
		],
		"TCs" => [
			[
				"brand" => "ToyotaVenza",
				"size" => "245/55/19",
				"gosNomer" => "691PVA/02",
			],
		],
	],
	[
		"clientID" =>"000000633",
		"name" =>"Базарбеков Кайрат Маратович",
		"attribute" => "Физ. лицо",
		"IIN" => "860507300586",
		"contacts" => [
			"mobile" => "+77019387310",
		],
	],
	[
		"clientID" =>"000000635",
		"name" =>"Шапошникова Ирина Евгеньевна",
		"attribute" => "Физ. лицо",
		"IIN" => "830918401008",
		"contacts" => [
			"mobile" => "+77017530614",
		],
		"TCs" => [
			[
				"brand" => "MiniCooperS",
				"size" => "205/45/17",
				"gosNomer" => "966 KEA/02",
			],
		],
	],
	[
		"clientID" =>"000000636",
		"name" =>"Сулименко Георгий Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "910413301053",
		"contacts" => [
			"mobile" => "+77781041312",
		],
	],
	[
		"clientID" =>"000000637",
		"name" =>"Хан   Сергей Геннадьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780510300192",
		"contacts" => [
			"mobile" => "+77017820305",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHiluxSurf",
				"size" => "265/70/16",
				"gosNomer" => "A801PMO",
			],
		],
	],
	[
		"clientID" =>"000000638",
		"name" =>"Темирбаев Алибек Кутжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "921211350018",
		"contacts" => [
			"mobile" => "+77017079292",
		],
		"TCs" => [
			[
				"brand" => "Toyota Camry",
				"size" => "215/60/16",
				"gosNomer" => "Z 808 YVM",
			],
		],
	],
	[
		"clientID" =>"000000639",
		"name" =>"Старцева  Элеонора  Константиновна",
		"attribute" => "Физ. лицо",
		"IIN" => "531201400438",
		"contacts" => [
			"mobile" => "+77772172000",
		],
		"TCs" => [
			[
				"brand" => "LexusRX300",
				"size" => "215/70/16",
				"gosNomer" => "A 818 NPO",
			],
		],
	],
	[
		"clientID" =>"000000640",
		"name" =>"Атмаджа Елена Васильевна",
		"attribute" => "Физ. лицо",
		"IIN" => "741003400574",
		"contacts" => [
			"mobile" => "+77018684017",
		],
		"TCs" => [
			[
				"brand" => "FORDMaverick",
				"size" => "265/70/15",
				"gosNomer" => "A052ESN",
			],
			[
				"brand" => "FORD Maverick",
				"size" => "311/05/01",
				"gosNomer" => "A052ESN",
			],
		],
	],
	[
		"clientID" =>"000000641",
		"name" =>"Айткужин  Кобланды  Казиханович",
		"attribute" => "Физ. лицо",
		"IIN" => "840210300214",
		"contacts" => [
			"mobile" => "+77713002694",
		],
		"TCs" => [
			[
				"brand" => "KIA",
				"size" => "195/50/15",
				"gosNomer" => "392MXA/02",
			],
			[
				"brand" => "KiaSportage",
				"size" => "225/60/17",
				"gosNomer" => "704TOA/02",
			],
			[
				"brand" => "KiaPicanto",
				"size" => "165/60/14",
				"gosNomer" => "392MXA/02",
			],
		],
	],
	[
		"clientID" =>"000001859",
		"name" =>"ЕСКАЛИЕВ НУРЛАН КУЗЕМБАЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "740731300850",
	],
	[
		"clientID" =>"000001860",
		"name" =>"ЛИ ЕВГЕНИЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "800103399026",
		"contacts" => [
			"mobile" => "+77017911818",
		],
	],
	[
		"clientID" =>"000001861",
		"name" =>"Ловков Владимир Георгиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "640312300207",
		"contacts" => [
			"mobile" => "+77017661380",
		],
	],
	[
		"clientID" =>"000001862",
		"name" =>"Емелеев Рашид Рауфович",
		"attribute" => "Юр. лицо",
		"IIN" => "820828399100",
		"contacts" => [
			"mobile" => "+77027836040",
		],
	],
	[
		"clientID" =>"000001863",
		"name" =>"Бекбосынов Кайрат Серикович",
		"attribute" => "Физ. лицо",
		"IIN" => "801028300895",
		"contacts" => [
			"mobile" => "+77017690657",
		],
	],
	[
		"clientID" =>"000001864",
		"name" =>"БАЧУРИНСКИЙ ДЕНИС ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "840519350667",
		"contacts" => [
			"mobile" => "+77051187448",
		],
	],
	[
		"clientID" =>"000001865",
		"name" =>"Буденная Людмила Кузьминична",
		"attribute" => "Юр. лицо",
		"IIN" => "600829401562",
		"contacts" => [
			"mobile" => "+77478697132",
		],
	],
	[
		"clientID" =>"000001878",
		"name" =>"ДЮСЕКЕНОВ ЕРЛАН ЕРГАНАТОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "750523300148",
	],
	[
		"clientID" =>"000001879",
		"name" =>"АБДРАХМАНОВ ИСРАИЛ ИСМАИЛОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "780501300024",
		"contacts" => [
			"mobile" => "+77019101212",
		],
		"TCs" => [
			[
				"brand" => "SuzukiGrandVitara",
				"size" => "225/70/16",
				"gosNomer" => "612 PYA/02",
			],
			[
				"brand" => "MitsubishiPajera",
				"size" => "265/65/17",
				"gosNomer" => "242TNA/02",
			],
		],
	],
	[
		"clientID" =>"000001881",
		"name" =>"КОЧЕРИДИ НИКОЛАЙ МИХАЙЛОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "900817300012",
	],
	[
		"clientID" =>"000001882",
		"name" =>"Гусев Александр Геннадьевич",
		"attribute" => "Юр. лицо",
		"IIN" => "880310300225",
	],
	[
		"clientID" =>"000001880",
		"name" =>"Елубакиева Гульнара Камбаровна",
		"attribute" => "Физ. лицо",
		"IIN" => "640203403146",
		"contacts" => [
			"mobile" => "+77022374323",
		],
	],
	[
		"clientID" =>"000001883",
		"name" =>"ГАММЕРШМИДТ ВЛАДИМИР КОНСТАНТИНОВИЧ",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000001884",
		"name" =>"ГОШУРЕНКО ВИТАЛИЙ АЛЕКСЕЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "700201303073",
	],
	[
		"clientID" =>"000001885",
		"name" =>"АВЕРИН МИХАИЛ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "510913301305",
		"contacts" => [
			"mobile" => "+77077320630",
		],
	],
	[
		"clientID" =>"000001887",
		"name" =>"ТЫСЫМБАЕВА САНДУГАШ РАУФОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "580413400625",
		"contacts" => [
			"E-Mail" => "aser-2002@mail.ru",
			"mobile" =>"+77476782778",
			"mobile" => "+77779845158",
		],
	],
	[
		"clientID" =>"000001888",
		"name" =>"Пивоваров Владимир Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "770331300514",
		"contacts" => [
			"mobile" => "+77017146643",
		],
	],
	[
		"clientID" =>"000001890",
		"name" =>"САЙФУТДИНОВ РАУАН ФАРИДОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "910929300279",
		"contacts" => [
			"mobile" => "+77717536405",
		],
	],
	[
		"clientID" =>"000001891",
		"name" =>"Пономарев Григорий Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840402302670",
	],
	[
		"clientID" =>"000001893",
		"name" =>"ИЗОТОВ ВИТАЛИЙ НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "620203301734",
	],
	[
		"clientID" =>"000001894",
		"name" =>"Шустиков Александр Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "570319301496",
		"contacts" => [
			"mobile" => "+77771141200",
		],
	],
	[
		"clientID" =>"000001895",
		"name" =>"АРТМАН АЛЕКСАНДР АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820213300930",
	],
	[
		"clientID" =>"000001896",
		"name" =>"КАЙРАНБАЕВ РУСТЕМ КУДАЙБЕРГЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "730724301561",
	],
	[
		"clientID" =>"000001897",
		"name" =>"ГУБАЙДУЛИН РУСТАМ ИБРАХИМОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820521399022",
	],
	[
		"clientID" =>"000001898",
		"name" =>"БОРАНГАЗИЕВ ФАРХАТ СЕРИКОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "770331302025",
	],
	[
		"clientID" =>"000001899",
		"name" =>"АЛЕКСЕЕВ ОЛЕГ БОРИСОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "461109300604",
	],
	[
		"clientID" =>"000001900",
		"name" =>"МАХАНОВ ДАНИЯР СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870101300079",
	],
	[
		"clientID" =>"000001901",
		"name" =>"ЮСУПОВ ИСЛАМ АЗРАТАЛЫЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "890626301144",
		"contacts" => [
			"mobile" => "+77012215057",
		],
	],
	[
		"clientID" =>"000001902",
		"name" =>"Досымбеков Тынышбай",
		"attribute" => "Юр. лицо",
		"IIN" => "520120300010",
	],
	[
		"clientID" =>"000001903",
		"name" =>"ХУСHУТДИHОВ ВИКТОР ГЕОРГИЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "530730301126",
	],
	[
		"clientID" =>"000001904",
		"name" =>"Жадеева Юлия Леонидовна",
		"attribute" => "Физ. лицо",
		"IIN" => "751111402748",
		"contacts" => [
			"mobile" => "+77017307411",
		],
		"TCs" => [
			[
				"brand" => "Toyota Rav-4",
				"size" => "215/70/16",
				"gosNomer" => "A 582VSO",
			],
			[
				"brand" => "Volkswagen",
				"size" => "195/60/15",
				"gosNomer" => "996TNA/02",
			],
		],
	],
	[
		"clientID" =>"000001905",
		"name" =>"Мауленкулов Арай Куандыкович",
		"attribute" => "Физ. лицо",
		"IIN" => "710704300045",
		"contacts" => [
			"mobile" => "+77772275377",
		],
	],
	[
		"clientID" =>"000001910",
		"name" =>"Ибрагимов Ашимжан Насыржанович",
		"attribute" => "Физ. лицо",
		"IIN" => "590216300025",
		"contacts" => [
			"mobile" => "+77019528114",
		],
	],
	[
		"clientID" =>"000001911",
		"name" =>"БЕРЕСТОВОЙ АНАТОЛИЙ ВАЛЕРИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "780330300550",
		"contacts" => [
			"mobile" => "+77076000553",
		],
	],
	[
		"clientID" =>"000001913",
		"name" =>"ИСЛАМХАН САНЖАР МӘДИҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "960620300139",
	],
	[
		"clientID" =>"000001914",
		"name" =>"ХАСЕН АСХАТ БАХЫДЖАНҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "860317301387",
	],
	[
		"clientID" =>"000001915",
		"name" =>"Долгопятов Максим Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "810417300056",
	],
	[
		"clientID" =>"000001916",
		"name" =>"Корнилов Кирилл Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "880801399055",
	],
	[
		"clientID" =>"000001917",
		"name" =>"Телешев Абзал Ныгметович",
		"attribute" => "Физ. лицо",
		"IIN" => "880707301033",
	],
	[
		"clientID" =>"000001918",
		"name" =>"Суворов Андрей Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "830528300162",
		"contacts" => [
			"mobile" => "+77777730299",
		],
	],
	[
		"clientID" =>"000001919",
		"name" =>"Жаркимбаев Тимур Аскатович",
		"attribute" => "Физ. лицо",
		"IIN" => "880530300707",
		"contacts" => [
			"mobile" => "+77078259513",
		],
	],
	[
		"clientID" =>"000001920",
		"name" =>"Инибаев Амангали Бейбитханулы",
		"attribute" => "Физ. лицо",
		"IIN" => "910116300348",
		"contacts" => [
			"mobile" => "+77072127028",
		],
	],
	[
		"clientID" =>"000001921",
		"name" =>"Курманов Арман Талгатович",
		"attribute" => "Физ. лицо",
		"IIN" => "900105300900",
		"contacts" => [
			"mobile" => "+77075039315",
		],
	],
	[
		"clientID" =>"000001922",
		"name" =>"АЗИЕВ САЛАМБЕК СУЛТАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "600713399060",
		"contacts" => [
			"mobile" => "+77752081160",
		],
	],
	[
		"clientID" =>"000001923",
		"name" =>"Абдикаримова  Алия  Алтынбековна",
		"attribute" => "Физ. лицо",
		"IIN" => "650314400068",
		"contacts" => [
			"mobile" => "+77017385698",
		],
		"TCs" => [
			[
				"brand" => "SubaruImpreza",
				"size" => "195/60/15",
				"gosNomer" => "A 779 ELP",
			],
		],
	],
	[
		"clientID" =>"000001924",
		"name" =>"ЕМЕТЬЯРОВ ДОСЫМ НУРКАШЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "760610300043",
	],
	[
		"clientID" =>"000001925",
		"name" =>"КАЛЕНДАРЁВА НАДЕЖДА ВАЛЕРЬЕВНА",
		"attribute" => "Юр. лицо",
		"IIN" => "870508400890",
	],
	[
		"clientID" =>"000001926",
		"name" =>"Тихомиров Илья Васильевич",
		"attribute" => "Юр. лицо",
		"IIN" => "940331300342",
	],
	[
		"clientID" =>"000001927",
		"name" =>"БИТИЕВ АЛЕКСЕЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860510350136",
	],
	[
		"clientID" =>"000001928",
		"name" =>"Ибраймов Жандос Байкадамович",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000000122",
		"name" =>"Жалмухамедов Ержан",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000822",
		"name" =>"Манохина Валентина",
		"attribute" => "Физ. лицо",
		"IIN" => "880925400285",
		"TCs" => [
			[
				"brand" => "Huyndai Getz",
				"size" => "175/70/14",
				"gosNomer" => "798 FYA/02",
			],
		],
	],
	[
		"clientID" =>"000000824",
		"name" =>"Трашков Владимир Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "610318300135",
	],
	[
		"clientID" =>"000000825",
		"name" =>"Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "840619350145",
	],
	[
		"clientID" =>"000000826",
		"name" =>"Самойлов Олег Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "850426301487",
	],
	[
		"clientID" =>"000000827",
		"name" =>"Таипов Р.У",
		"attribute" => "Физ. лицо",
		"IIN" => "911121300116",
	],
	[
		"clientID" =>"000000830",
		"name" =>"Искендыров Марат Бейсенкулович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014369101",
		],
	],
	[
		"clientID" =>"000000832",
		"name" =>"Беккулов Аскар",
		"attribute" => "Физ. лицо",
		"IIN" => "800605300938",
		"contacts" => [
			"mobile" => "+77015386201",
		],
	],
	[
		"clientID" =>"000001797",
		"name" =>"СПИРИНА ИРИНА МИХАЙЛОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "730429401346",
		"contacts" => [
			"mobile" =>"+77071180450",
			"mobile" => "+77051180450",
		],
	],
	[
		"clientID" =>"000001798",
		"name" =>"БЕКСАРЫ ЖАҢАБАЙ МҰРАТБЕКҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "650107300284",
	],
	[
		"clientID" =>"000001799",
		"name" =>"КОРШИКОВ НИКОЛАЙ АЛЕКСЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "761218301423",
		"contacts" => [
			"mobile" => "+77012887019",
		],
	],
	[
		"clientID" =>"000001800",
		"name" =>"КОЖАНОВ АРСЛАН ДЖАМБУЛОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "820512301845",
	],
	[
		"clientID" =>"000001801",
		"name" =>"Диханбаев Талгат Нуркуатович",
		"attribute" => "Физ. лицо",
		"IIN" => "670529301588",
		"contacts" => [
			"E-Mail" => "talgin_dih@mail.ru",
			"mobile" =>"+77017385287",
			"mobile" => "+77017385287",
		],
		"TCs" => [
			[
				"brand" => "VOLVO",
				"size" => "235/65/17",
				"gosNomer" => "052PBA/05",
			],
		],
	],
	[
		"clientID" =>"000001802",
		"name" =>"Жанузаков Ержан Мейрманович",
		"attribute" => "Физ. лицо",
		"IIN" => "700702302128",
		"contacts" => [
			"mobile" => "+77015527522",
		],
	],
	[
		"clientID" =>"000001803",
		"name" =>"Бочкарев Александр Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "760820300025",
		"contacts" => [
			"mobile" => "+77019622200",
		],
		"TCs" => [
			[
				"brand" => "NissanX-Trail",
				"size" => "215/65/16",
				"gosNomer" => "A187DMP",
			],
		],
	],
	[
		"clientID" =>"000001804",
		"name" =>"Любохинец Александр Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "760408301668",
		"contacts" => [
			"mobile" => "+77089729440",
		],
	],
	[
		"clientID" =>"000001805",
		"name" =>"ИВАНОВ АНДРЕЙ ФЕДОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "830505300711",
		"contacts" => [
			"mobile" => "+77019445937",
		],
	],
	[
		"clientID" =>"000001807",
		"name" =>"Якушкин Владислав Тимофеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "490325300374",
		"contacts" => [
			"mobile" => "+77019596253",
		],
	],
	[
		"clientID" =>"000001811",
		"name" =>"ИВАНОВА ЛАРИСА ВЛАДИМИРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "601203400612",
	],
	[
		"clientID" =>"000001812",
		"name" =>"Иванников Дмитрий Яковлевич",
		"attribute" => "Физ. лицо",
		"IIN" => "500817300284",
		"contacts" => [
			"mobile" => "+77079501003",
		],
	],
	[
		"clientID" =>"000001813",
		"name" =>"ИМАНБЕКОВ МЕДЕТ АЛЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "791104301455",
		"contacts" => [
			"mobile" => "+77027773141",
		],
	],
	[
		"clientID" =>"000001814",
		"name" =>"Рахимов Ермек Турсынович",
		"attribute" => "Физ. лицо",
		"IIN" => "670506301959",
		"contacts" => [
			"mobile" => "+77072340735",
		],
	],
	[
		"clientID" =>"000001815",
		"name" =>"НАЗАРОВА ТАТЬЯНА АЛЕКСАНДРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "840528450315",
		"contacts" => [
			"mobile" => "+77015595009",
		],
	],
	[
		"clientID" =>"000001816",
		"name" =>"Бихасимов Махамбет  Нурболатович",
		"attribute" => "Физ. лицо",
		"IIN" => "880221300281",
		"contacts" => [
			"mobile" => "+77775985555",
		],
		"TCs" => [
			[
				"brand" => "MercedesBenz",
				"size" => "275/35/20",
				"gosNomer" => "007KLA/02",
			],
			[
				"brand" => "MB",
				"size" => "245/40/20",
				"gosNomer" => "007KLA/02",
			],
			[
				"brand" => "Hyundai stark",
				"size" => "195/70/16",
				"gosNomer" => "457",
			],
		],
	],
	[
		"clientID" =>"000001817",
		"name" =>"ОМАРОВ МАУЛЕН МУРАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820216301251",
		"contacts" => [
			"mobile" => "+77757255530",
		],
	],
	[
		"clientID" =>"000001819",
		"name" =>"ЖОЛАНБАЕВ МУХТАР ОРЫНБАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "810616301680",
		"contacts" => [
			"mobile" => "+77014779781",
		],
	],
	[
		"clientID" =>"000001820",
		"name" =>"ВОРОНКОВ АНАТОЛИЙ ВАСИЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "510405300226",
		"contacts" => [
			"mobile" => "+77017152647",
		],
	],
	[
		"clientID" =>"000001821",
		"name" =>"АМАНЖОЛОВ ЕРЕН АЛТАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "710303300124",
		"contacts" => [
			"mobile" => "+77772225041",
		],
	],
	[
		"clientID" =>"000001823",
		"name" =>"Эм Родион Николаевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "8996555009599",
		],
	],
	[
		"clientID" =>"000001824",
		"name" =>"ШВАРЦ ЕЛЕНА ЯКОВЛЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "860410400102",
		"contacts" => [
			"mobile" => "+77016450900",
		],
	],
	[
		"clientID" =>"000001825",
		"name" =>"АБЕНОВА ЗАУРЕШ МОЛДАБАЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "590918401204",
		"contacts" => [
			"mobile" => "+77755296645",
		],
	],
	[
		"clientID" =>"000001827",
		"name" =>"КАРАВАН ИЛЬЯ ЯРОСЛАВОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "850116300196",
		"contacts" => [
			"mobile" => "+77057274400",
		],
		"TCs" => [
			[
				"brand" => "BMW 525 Xi",
				"size" => "225/50/17",
				"gosNomer" => "195 OFA/02",
			],
		],
	],
	[
		"clientID" =>"000001828",
		"name" =>"СМАГУЛОВ НУРТАЙ МУРАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "811014300315",
		"contacts" => [
			"mobile" => "+77072279779",
		],
	],
	[
		"clientID" =>"000001829",
		"name" =>"СЕРГЕЕВ ДМИТРИЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820912301364",
	],
	[
		"clientID" =>"000001830",
		"name" =>"САДЫКОВ ТАИР КУДАЙБЕРГЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "841209300559",
	],
	[
		"clientID" =>"000001831",
		"name" =>"ГЕРШГОРИН ПАВЕЛ",
		"attribute" => "Юр. лицо",
		"IIN" => "720204300425",
	],
	[
		"clientID" =>"000001832",
		"name" =>"ҚАЛМОМЫНЫОВ БЕКАРЫС ПЕРДЕХАНҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "920820302006",
		"contacts" => [
			"mobile" => "+77470380337",
		],
	],
	[
		"clientID" =>"000001833",
		"name" =>"ПАХОМОВ АЛЕКСАНДР НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "650528399074",
		"contacts" => [
			"mobile" => "+77017211876",
		],
	],
	[
		"clientID" =>"000001834",
		"name" =>"КУДИНОВ ВИТАЛИЙ ВИКТОРОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "800505302069",
	],
	[
		"clientID" =>"000001835",
		"name" =>"НУРЖАНОВ МАКСАТ НУРЖАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "781006300175",
		"contacts" => [
			"mobile" => "+77714121825",
		],
	],
	[
		"clientID" =>"000001836",
		"name" =>"ЖАМИДУЛЛИН КАРИМБЕК МАМЫРБЕКОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "870617300857",
	],
	[
		"clientID" =>"000001837",
		"name" =>"ФЕДОРЕНКО АНТОН ЕВГЕНЬЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "900312300847",
	],
	[
		"clientID" =>"000001838",
		"name" =>"Степанов Александр Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "850916399065",
		"contacts" => [
			"mobile" => "+77077597999",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiLancer",
				"size" => "205/60/16",
				"gosNomer" => "022OLA/02",
			],
			[
				"brand" => "MMC Lancer",
				"size" => "205/60/16",
				"gosNomer" => "022 OLA  0",
			],
			[
				"brand" => "MMC Lancer",
				"size" => "205/60/16",
				"gosNomer" => "022 OLA  0",
			],
			[
				"brand" => "MMC Lancer",
				"size" => "205/60/16",
				"gosNomer" => "022 OLA  0",
			],
			[
				"brand" => "MMC Lancer",
				"size" => "205/60/16",
				"gosNomer" => "022 OLA  0",
			],
			[
				"brand" => "MMC Lancer",
				"size" => "205/60/16",
				"gosNomer" => "022 OLA  0",
			],
		],
	],
	[
		"clientID" =>"000001839",
		"name" =>"Мухамедиев  Марат  Берикович",
		"attribute" => "Физ. лицо",
		"IIN" => "820512301429",
		"contacts" => [
			"mobile" => "+77762069931",
		],
		"TCs" => [
			[
				"brand" => "BMW525i",
				"size" => "225/40/18",
				"gosNomer" => "630KFA/02",
			],
			[
				"brand" => "BMW  525i",
				"size" => "245/40/18",
				"gosNomer" => "630KFA/02",
			],
		],
	],
	[
		"clientID" =>"000001840",
		"name" =>"Бектемиров  Едиль Алтынбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840313350328",
		"contacts" => [
			
			"mobile" => "+77012240949",
		],
		"TCs" => [
			[
				"brand" => "BMW     X5",
				"size" => "225/50/19",
				"gosNomer" => "E145XT/70",
			],
			[
				"brand" => "MAZDA    CX5",
				"size" => "225/55/19",
				"gosNomer" => "371KEA/02",
			],
			[
				"brand" => "BMW     X 5",
				"size" => "275/40/20",
				"gosNomer" => "E145 XT/7",
			],
			[
				"brand" => "не поняла",
				"size" => "215/65/16",
				"gosNomer" => "385PBA/02",
			],
			[
				"brand" => "Mazda   CX 5",
				"size" => "255/55/19",
				"gosNomer" => "371KEA/02",
			],
			[
				"brand" => "BMWX5",
				"size" => "255/50/19",
				"gosNomer" => "893PXA/02",
			],
		],
	],
	[
		"clientID" =>"000001841",
		"name" =>"ПЕТУХОВ СЕРГЕЙ ЮРЬЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "611202300018",
	],
	[
		"clientID" =>"000001842",
		"name" =>"МАРЧУК ЮЛИЯ СЕРГЕЕВНА",
		"attribute" => "Юр. лицо",
		"IIN" => "890422401270",
	],
	[
		"clientID" =>"000001843",
		"name" =>"САРЫБАЕВ ТУРСЫНГАЛИ АКИМЖАНОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "520317300646",
		"contacts" => [
			"mobile" => "+77755828664",
		],
	],
	[
		"clientID" =>"000001844",
		"name" =>"Бахтыбаев Алихан Серикович",
		"attribute" => "Физ. лицо",
		"IIN" => "910818300483",
		"contacts" => [
			"mobile" => "+77077789789",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry30",
				"size" => "215/60/16",
				"gosNomer" => "313BMA02",
			],
			[
				"brand" => "ToyotaCamry 30",
				"size" => "205/65/15",
				"gosNomer" => "313BMA02",
			],
			[
				"brand" => "Toyota Camry",
				"size" => "205/65/15",
				"gosNomer" => "313 BMA02",
			],
		],
	],
	[
		"clientID" =>"000001845",
		"name" =>"ЛАПШИН ЕВГЕНИЙ ВЛДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => " 76080739906",
	],
	[
		"clientID" =>"000001846",
		"name" =>"ЧУВАСОВ ДМИТРИЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860526300464",
	],
	[
		"clientID" =>"000001847",
		"name" =>"УРАЗАЛИНА ЗАУРЕ КАЖИГАЛИЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "511112401825",
		"contacts" => [
			"mobile" => "+77019292672",
		],
	],
	[
		"clientID" =>"000001850",
		"name" =>"НАМ ДЕНИС АФАНАСЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "811127301191",
	],
	[
		"clientID" =>"000001851",
		"name" =>"ДЖАБАГЕНОВ ЕВГЕНИЙ ЕРБОЛАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "851224350707",
	],
	[
		"clientID" =>"000001554",
		"name" =>"Ли Илларион Геннадьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "700503300249",
		"contacts" => [
			"mobile" => "+77017111373",
		],
	],
	[
		"clientID" =>"000001559",
		"name" =>"ПАЛМУРАТОВ ЖАНИБЕК СЕРИКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "940527300020",
		"contacts" => [
			"mobile" => "+77011118830",
		],
	],
	[
		"clientID" =>"000001560",
		"name" =>"Кайсиди Иоаннис Георгиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "870717301829",
		"contacts" => [
			"mobile" => "+77074091075",
		],
	],
	[
		"clientID" =>"000001561",
		"name" =>"Черновский Роман Олегович",
		"attribute" => "Физ. лицо",
		"IIN" => "810629300772",
		"contacts" => [
			"mobile" => "+77773631168",
		],
	],
	[
		"clientID" =>"000001562",
		"name" =>"Леденёв Владимир Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "881023300476",
		"contacts" => [
			"mobile" => "+77019140441",
		],
	],
	[
		"clientID" =>"000001563",
		"name" =>"Сарсенкулов Куаныш Жаппарулы",
		"attribute" => "Физ. лицо",
		"IIN" => "780619302726",
		"contacts" => [
			"mobile" => "+77021932527",
		],
	],
	[
		"clientID" =>"000000869",
		"name" =>"Горинов Олег",
		"attribute" => "Юр. лицо",
		"IIN" => "810914300454",
	],
	[
		"clientID" =>"000000875",
		"name" =>"Немудров Сергей Юрьевич",
		"attribute" => "Юр. лицо",
		"IIN" => "710120301900",
	],
	[
		"clientID" =>"000000876",
		"name" =>"Нусупбеков Ернар Ербатырович",
		"attribute" => "Физ. лицо",
		"IIN" => "840306300553",
	],
	[
		"clientID" =>"000000877",
		"name" =>"Шапошникова  Марина  Михайловна",
		"attribute" => "Физ. лицо",
		"IIN" => "580706401458",
		"contacts" => [
			"mobile" => "+77017530615",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "205/65/15",
				"gosNomer" => "086LYA/02",
			],
			[
				"brand" => "Honda  CR-V",
				"size" => "225/60/18",
				"gosNomer" => "704SEA/02",
			],
			[
				"brand" => "Mini  Cooper",
				"size" => "205/45/17",
				"gosNomer" => "966KEA/02",
			],
		],
	],
	[
		"clientID" =>"000000878",
		"name" =>"Бертаева Сауле",
		"attribute" => "Физ. лицо",
		"IIN" => "710614402178",
	],
	[
		"clientID" =>"000000879",
		"name" =>"Алмас",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000884",
		"name" =>"Бекиров Руслан",
		"attribute" => "Физ. лицо",
		"IIN" => "900515300563",
		"contacts" => [
			"mobile" => "+77079900515",
		],
	],
	[
		"clientID" =>"000000885",
		"name" =>"Пахирдинов Азиз Камирдинович",
		"attribute" => "Физ. лицо",
		"IIN" => "630709301579",
		"contacts" => [
			"mobile" => "+77017663430",
		],
	],
	[
		"clientID" =>"000000886",
		"name" =>"Дюсембаева Жанар Джетписбаева",
		"attribute" => "Физ. лицо",
		"IIN" => "760926402202",
		"contacts" => [
			"mobile" => "+77017860957",
		],
	],
	[
		"clientID" =>"000000887",
		"name" =>"Корабельников Вадим Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "751013301334",
		"contacts" => [
			"mobile" => "+77057757875",
		],
	],
	[
		"clientID" =>"000001565",
		"name" =>"Карибаева Салима Негметуловна",
		"attribute" => "Юр. лицо",
		"IIN" => "840521402149",
		"contacts" => [
			"mobile" => "+77082212255",
		],
	],
	[
		"clientID" =>"000001566",
		"name" =>"Муканов Ислам",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77029611193",
		],
	],
	[
		"clientID" =>"000001567",
		"name" =>"Ким Эдуард Эдуардович",
		"attribute" => "Физ. лицо",
		"IIN" => "791106300399",
		"contacts" => [
			"mobile" => "+77010370377",
		],
		"TCs" => [
			[
				"brand" => "Toyota 4 Runner",
				"size" => "265/70/17",
				"gosNomer" => "A 566 BRO",
			],
		],
	],
	[
		"clientID" =>"000001568",
		"name" =>"МАЛЫШКИН ОЛЕГ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "780921301274",
		"contacts" => [
			"mobile" => "+77772399857",
		],
	],
	[
		"clientID" =>"000001569",
		"name" =>"КОЛОМЫТЦЕВ ВАСИЛИЙ ВАСИЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "811224300842",
		"contacts" => [
			"mobile" => "+77017862124",
		],
	],
	[
		"clientID" =>"000001571",
		"name" =>"Жуманов Биржан",
		"attribute" => "Физ. лицо",
		"IIN" => "890523302033",
		"contacts" => [
			"mobile" => "+77012949446",
		],
	],
	[
		"clientID" =>"000001572",
		"name" =>"Козыканов Мауетбек Сакышевич",
		"attribute" => "Физ. лицо",
		"IIN" => "671202300039",
		"contacts" => [
			"mobile" =>"+77776482348",
			"mobile" => "+77776482348",
		],
	],
	[
		"clientID" =>"000001574",
		"name" =>"Ульданов Андрей Дамирович",
		"attribute" => "Физ. лицо",
		"IIN" => "710218350377",
		"contacts" => [
			"mobile" => "+77017368725",
		],
	],
	[
		"clientID" =>"000001575",
		"name" =>"Дутов Юрий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "850703301591",
		"contacts" => [
			"mobile" => "+77775035557",
		],
	],
	[
		"clientID" =>"000001578",
		"name" =>"Белясников Денис Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "820716301041",
		"contacts" => [
			"mobile" => "+77777059485",
		],
	],
	[
		"clientID" =>"000001579",
		"name" =>"Кадырханов Нуржан Нурланович",
		"attribute" => "Физ. лицо",
		"IIN" => "960106350208",
		"contacts" => [
			"mobile" => "+77766444999",
		],
	],
	[
		"clientID" =>"000001581",
		"name" =>"Айдаралиев Медет Каныбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "871007301970",
		"contacts" => [
			"mobile" => "+77471191349",
		],
	],
	[
		"clientID" =>"000001584",
		"name" =>"Абдимадиев Ермек Калыкович",
		"attribute" => "Физ. лицо",
		"IIN" => "670619301824",
		"contacts" => [
			"mobile" => "+77782167750",
		],
	],
	[
		"clientID" =>"000001585",
		"name" =>"ТОЛЕМИСОВ ТАЛГАТ ЧАЛГЫНБАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "670226301660",
		"contacts" => [
			"mobile" => "+77773888886",
		],
	],
	[
		"clientID" =>"000001587",
		"name" =>"Горбунов Юрий Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "590705300162",
		"contacts" => [
			"mobile" => "+77772130859",
		],
	],
	[
		"clientID" =>"000001589",
		"name" =>"ЖУЖГИН ВИТАЛИЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "660706300054",
	],
	[
		"clientID" =>"000001590",
		"name" =>"РОМАНОВ РОМАН АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860522300412",
		"contacts" => [
			"mobile" => "+77779851313",
		],
	],
	[
		"clientID" =>"000001592",
		"name" =>"Мродчик Олег Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "770625301858",
		"contacts" => [
			"mobile" => "+77772423034",
		],
	],
	[
		"clientID" =>"000001593",
		"name" =>"ЖУКОВ АЛЕКСЕЙ ЛЕОНИДОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870402302626",
		"contacts" => [
			"mobile" => "+77477707000",
		],
	],
	[
		"clientID" =>"000001596",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019188712",
		],
	],
	[
		"clientID" =>"000001600",
		"name" =>"МАКСИМОВ РЕНАТ ИЗАМУДДИНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "780810302318",
		"contacts" => [
			"mobile" => "+77781388222",
		],
	],
	[
		"clientID" =>"000001604",
		"name" =>"Ладгин Владимир Вилович",
		"attribute" => "Физ. лицо",
		"IIN" => "541206399023",
		"contacts" => [
			"mobile" => "+77017823788",
		],
	],
	[
		"clientID" =>"000001606",
		"name" =>"Стандицкий Виталий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "931011300323",
		"contacts" => [
			"mobile" => "+77753194858",
		],
	],
	[
		"clientID" =>"000001607",
		"name" =>"Ержиенов Талант Чакржанович",
		"attribute" => "Физ. лицо",
		"IIN" => "880524300883",
		"contacts" => [
			"mobile" => "+77057576090",
		],
	],
	[
		"clientID" =>"000001611",
		"name" =>"Коренчук Дмитрий Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "650902301452",
		"contacts" => [
			"mobile" => "+77772147513",
		],
	],
	[
		"clientID" =>"000001615",
		"name" =>"Алибеков Асхат Омарович",
		"attribute" => "Физ. лицо",
		"IIN" => "750303301586",
	],
	[
		"clientID" =>"000001619",
		"name" =>"КИМ ВАДИМ БОРИСОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "700804300691",
		"contacts" => [
			"mobile" => "+77013143159",
		],
	],
	[
		"clientID" =>"000001621",
		"name" =>"Нуретденулы Толеген",
		"attribute" => "Физ. лицо",
		"IIN" => "810617350080",
		"contacts" => [
			"mobile" => "+77752462025",
		],
	],
	[
		"clientID" =>"000001628",
		"name" =>"Сабдалина Даимхан Смадияровна",
		"attribute" => "Физ. лицо",
		"IIN" => "490219401570",
		"contacts" => [
			"mobile" => "+77012275097",
		],
	],
	[
		"clientID" =>"000001630",
		"name" =>"ҚҰСАЙЫН АРМАН МҰРАТҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "880216300105",
		"contacts" => [
			"mobile" => "+77017998762",
		],
	],
	[
		"clientID" =>"000001632",
		"name" =>"Байсакалов Адай Бегенович",
		"attribute" => "Физ. лицо",
		"IIN" => "590817301080",
		"contacts" => [
			"mobile" => "+77017500059",
		],
	],
	[
		"clientID" =>"000001634",
		"name" =>"Тимур,  Садвахасова Анара Утегеновна ",
		"attribute" => "Физ. лицо",
		"IIN" => "790302401118",
		"contacts" => [
			"mobile" => "+77015008466",
		],
		"TCs" => [
			[
				"brand" => "LexusRX350",
				"size" => "235/55/19",
				"gosNomer" => "E350HTM",
			],
		],
	],
	[
		"clientID" =>"000001635",
		"name" =>"АБРАЕВ ХАБДИЛАХАТ ХАННЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "600208300660",
		"contacts" => [
			"mobile" => "+77772869719",
		],
	],
	[
		"clientID" =>"000001636",
		"name" =>"Софиенко Александр Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "751115302248",
		"contacts" => [
			"mobile" => "+77073686053",
		],
	],
	[
		"clientID" =>"000001637",
		"name" =>"ТАЖЕДИН ӘСЕЛ ИСАҚЫЗЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "840313400538",
		"contacts" => [
			"mobile" => "+77075078883",
		],
	],
	[
		"clientID" =>"000001638",
		"name" =>"Сулейменов Артур Юресович",
		"attribute" => "Физ. лицо",
		"IIN" => "790715301246",
		"contacts" => [
			"mobile" => "+77015669225",
		],
		"TCs" => [
			[
				"brand" => "LEXUS",
				"size" => "215/55/17",
				"gosNomer" => "160AN/02",
			],
		],
	],
	[
		"clientID" =>"000001639",
		"name" =>"Дилманов Ерлан Калабаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780710300576",
		"contacts" => [
			"mobile" => "+77014254686",
		],
	],
	[
		"clientID" =>"000001640",
		"name" =>"КИРСАНОВ ИВАН ГРИГОРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "811210350773",
		"contacts" => [
			"mobile" => "+77017352853",
		],
	],
	[
		"clientID" =>"000001642",
		"name" =>"Хегай Елена Андреевна",
		"attribute" => "Физ. лицо",
		"IIN" => "710324499104",
		"contacts" => [
			"mobile" => "+77017713459",
		],
	],
	[
		"clientID" =>"000001644",
		"name" =>"БАБАЕВ КОНСТАНТИН ВАЛЕРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "770308300142",
		"contacts" => [
			"mobile" => "+77770142507",
		],
	],
	[
		"clientID" =>"000001645",
		"name" =>"ШАВРИН АНДРЕЙ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "830103300321",
		"contacts" => [
			"mobile" => "8777630246887756596922",
		],
	],
	[
		"clientID" =>"000001647",
		"name" =>"АЛТЫНБАЕВ БАКЫТ КУАНДЫКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "751016302130",
		"contacts" => [
			"mobile" => "+77021591660",
		],
	],
	[
		"clientID" =>"000001648",
		"name" =>"ПОСЛЕДОВА ВЕРОНИКА ВАЛЕРЬЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "681227401606",
		"contacts" => [
			"mobile" => "+77078919290",
		],
	],
	[
		"clientID" =>"000001649",
		"name" =>"ПОПОВ ДМИТРИЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860107300515",
		"contacts" => [
			"mobile" => "+77772430794",
		],
	],
	[
		"clientID" =>"000001651",
		"name" =>"Жумагалиева Фариза Еркекалиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "920101400235",
	],
	[
		"clientID" =>"000001655",
		"name" =>"РАДИОБАЙЛАНЫС ТОО",
		"attribute" => "Юр. лицо",
		"IIN" => "011040005639",
	],
	[
		"clientID" =>"000001668",
		"name" =>"Овчинников Валентин Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780618301663",
		"contacts" => [
			"mobile" => "+77052030792",
		],
	],
	[
		"clientID" =>"000001670",
		"name" =>"МУСАЕВ САКЕН АБЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "661104301538",
		"contacts" => [
			"mobile" => "+77014696167",
		],
	],
	[
		"clientID" =>"000001672",
		"name" =>"Кужилин Виктор",
		"attribute" => "Физ. лицо",
		"IIN" => "540411399045",
		"contacts" => [
			"mobile" => "8777726260054",
		],
	],
	[
		"clientID" =>"000001673",
		"name" =>"БЕКСУЛТАНОВА ДАКЕН КУДАЙБЕРГЕНОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "820419450438",
		"contacts" => [
			"mobile" => "+77019909053",
		],
	],
	[
		"clientID" =>"000001675",
		"name" =>"АМАНБАЕВ ТОЙЖАН СЕРИКОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "830310300018",
		"contacts" => [
			"mobile" => "+77022203620",
		],
	],
	[
		"clientID" =>"000001676",
		"name" =>"ЖАНЕНОВ АЗАМАТ АДИЛОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870314301431",
		"contacts" => [
			"mobile" => "+77053553535",
		],
	],
	[
		"clientID" =>"000001677",
		"name" =>"Сатов Ержан Мухамбетович",
		"attribute" => "Юр. лицо",
		"IIN" => "780405301714",
		"contacts" => [
			"mobile" => "+77017470717",
		],
	],
	[
		"clientID" =>"000001680",
		"name" =>"ТКАЧЕНКО ВЛАДИМИР МИХАЙЛОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "551203399049",
		"contacts" => [
			"mobile" => "+77772371697",
		],
	],
	[
		"clientID" =>"000001681",
		"name" =>"НУРЖАНОВ ЖАСУЛАН ДАУРЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870206300505",
		"contacts" => [
			"mobile" => "+77018880404",
		],
	],
	[
		"clientID" =>"000001683",
		"name" =>"СВИСТУЛЕНКО ВЕРА МИХАЙЛОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "560518450321",
		"contacts" => [
			"mobile" => "8777134545787771098526",
		],
	],
	[
		"clientID" =>"000001684",
		"name" =>"Лезарев Евгений Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "880603300119",
		"contacts" => [
			"mobile" => "+77051333028",
		],
	],
	[
		"clientID" =>"000001687",
		"name" =>"КУЖАНТАЕВ АСАН ЖУМАБАЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "820627300842",
		"contacts" => [
			"mobile" => "+77017362019",
		],
	],
	[
		"clientID" =>"000001690",
		"name" =>"КАСИМОВА АСЯ БОЛАТОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "801021401504",
		"contacts" => [
			"mobile" => "+77017774709",
		],
	],
	[
		"clientID" =>"000001691",
		"name" =>"ШАГИН АЛЕКСАНДР ГЕРМАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "630604301307",
		"contacts" => [
			"mobile" => "+77085231637",
		],
	],
	[
		"clientID" =>"000001692",
		"name" =>"Милешин Евгений Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840807302965",
		"contacts" => [
			"mobile" => "+77013553443",
		],
	],
	[
		"clientID" =>"000001693",
		"name" =>"Алдашбеков  Жангельды  Мухаметжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "730323302966",
		"contacts" => [
			"mobile" => "+77017188503",
		],
		"TCs" => [
			[
				"brand" => "ToyotaLandCruiser",
				"size" => "275/70/16",
				"gosNomer" => "H920NZM",
			],
			[
				"brand" => "BMW",
				"size" => "215/60/16",
				"gosNomer" => "A128CAP",
			],
			[
				"brand" => "ToyotaLandCruiser",
				"size" => "275/70/16",
				"gosNomer" => "H 920 NZM",
			],
		],
	],
	[
		"clientID" =>"000001696",
		"name" =>"БАЛҒАБАЕВ БОЛАТ ДІНІСЛАМҰЛЫ",
		"attribute" => "Юр. лицо",
		"IIN" => "700521301374",
		"contacts" => [
			"mobile" => "+77024813737",
		],
	],
	[
		"clientID" =>"000001697",
		"name" =>"ТЮЛЬБАЕВ ЕРМУХАНБЕТ КАЛМУХАМБЕТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "770908301915",
		"contacts" => [
			"mobile" => "+77017390023",
		],
	],
	[
		"clientID" =>"000001698",
		"name" =>"ТРУНОВ ВЛАДИМИР ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "470407300415",
		"contacts" => [
			"mobile" => "+77014680587",
		],
	],
	[
		"clientID" =>"000001043",
		"name" =>"Петров  Дмитрий  Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "890405300530",
		"contacts" => [
			"mobile" => "+77019555002",
		],
		"TCs" => [
			[
				"brand" => "VWTouareg",
				"size" => "235/65/17",
				"gosNomer" => "244CMA/02",
			],
		],
	],
	[
		"clientID" =>"000001046",
		"name" =>"КОННОВА ВАЛЕРИЯ СЕРГЕЕВНА",
		"attribute" => "Юр. лицо",
		"IIN" => "890111400132",
	],
	[
		"clientID" =>"000001058",
		"name" =>"Чуричев Максим Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "850507300072",
		"contacts" => [
			"mobile" => "+77012326250",
		],
	],
	[
		"clientID" =>"000001065",
		"name" =>"Горняков  Евгений  Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "751129301652",
		"contacts" => [
			"mobile" => "+77016775005",
		],
		"TCs" => [
			[
				"brand" => "AudiA6",
				"size" => "215/60/16",
				"gosNomer" => "A181SNO",
			],
			[
				"brand" => "MercedesBenz",
				"size" => "255/50/19",
				"gosNomer" => "581MLA/02",
			],
			[
				"brand" => "Mersedes Benz ML 350",
				"size" => "255/50/19",
				"gosNomer" => "581 MLA/02",
			],
		],
	],
	[
		"clientID" =>"000001066",
		"name" =>"Корнео Оксана Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "701127400799",
		"contacts" => [
			"mobile" => "+77017256862",
		],
	],
	[
		"clientID" =>"000001071",
		"name" =>"Богданов Александр Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "880819351138",
		"contacts" => [
			"mobile" => "+77055187501",
		],
	],
	[
		"clientID" =>"000001084",
		"name" =>"Алиев Талгат",
		"attribute" => "Физ. лицо",
		"IIN" => "750707300217",
		"contacts" => [
			"mobile" => "+77017640041",
		],
		"TCs" => [
			[
				"brand" => "WW Tiguan",
				"size" => "235/55/17",
				"gosNomer" => "A463FCP",
			],
		],
	],
	[
		"clientID" =>"000001086",
		"name" =>"Овчинников Алексей Русланович",
		"attribute" => "Физ. лицо",
		"IIN" => "840323301297",
		"contacts" => [
			"mobile" => "+77059008861",
		],
	],
	[
		"clientID" =>"000001087",
		"name" =>"Матвеев  Валерий  Павлович",
		"attribute" => "Физ. лицо",
		"IIN" => "500224300419",
		"contacts" => [
			"mobile" => "+77772779943",
		],
		"TCs" => [
			[
				"brand" => "HyundaiTucson",
				"size" => "225/60/17",
				"gosNomer" => "933 FUA/02",
			],
		],
	],
	[
		"clientID" =>"000001089",
		"name" =>"Коптелый Вадим Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "881009350733",
	],
	[
		"clientID" =>"000001091",
		"name" =>"Жубанов Жайык Ертаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840417300963",
		"contacts" => [
			"mobile" => "+77073577905",
		],
	],
	[
		"clientID" =>"000001093",
		"name" =>"Спанов Айбек Жасуланович",
		"attribute" => "Физ. лицо",
		"IIN" => "900209350556",
		"contacts" => [
			"mobile" => "+77018699669",
		],
	],
	[
		"clientID" =>"000001101",
		"name" =>"Бейсенбаев Бахыт Сапабекович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024303060",
		],
	],
	[
		"clientID" =>"000001106",
		"name" =>"Бочарова Елена Юрьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "590831401717",
		"contacts" => [
			"mobile" => "+77013724590",
		],
	],
	[
		"clientID" =>"000001107",
		"name" =>"Толеу Алем Муратулы",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77473482465",
		],
	],
	[
		"clientID" =>"000001118",
		"name" =>"Ибрагимов Рашид Мирзабалаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "680811301641",
		"contacts" => [
			"mobile" => "+77777898745",
		],
	],
	[
		"clientID" =>"000001121",
		"name" =>"ШЕПЕЛЬ ВЯЧЕСЛАВ СТАНИСЛАВОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "741031301003",
		"contacts" => [
			"mobile" => "+77772184355",
		],
	],
	[
		"clientID" =>"000001122",
		"name" =>"Согамбаева Галия Бердибековна",
		"attribute" => "Юр. лицо",
		"IIN" => "740331401147",
		"contacts" => [
			"mobile" => "+77772045514",
		],
		"TCs" => [
			[
				"brand" => "Toyota Yaris",
				"size" => "195/60/15",
				"gosNomer" => "254 EKA/02",
			],
		],
	],
	[
		"clientID" =>"000001123",
		"name" =>"Жанабаев Али Калиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "710813301492",
		"contacts" => [
			"mobile" => "+77772118063",
		],
	],
	[
		"clientID" =>"000001129",
		"name" =>"Занин Дмитрий",
		"attribute" => "Физ. лицо",
		"IIN" => "831214300306",
		"contacts" => [
			"mobile" => "+77014601072",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi Pajero",
				"size" => "265/65/17",
				"gosNomer" => "496 KMA/02",
			],
		],
	],
	[
		"clientID" =>"000001131",
		"name" =>"Хайларова  Гульфайруз",
		"attribute" => "Физ. лицо",
		"IIN" => "730515440005",
		"contacts" => [
			"mobile" => "+77017141076",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHighlander",
				"size" => "245/65/17",
				"gosNomer" => "621CHA/02",
			],
			[
				"brand" => "ToyotaHighlandar",
				"size" => "245/65/17",
				"gosNomer" => "621 CHA/02",
			],
		],
	],
	[
		"clientID" =>"000001135",
		"name" =>"Сукнёв Вячеслав Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "750310300843",
		"contacts" => [
			"mobile" => "+77777077771",
		],
	],
	[
		"clientID" =>"000001136",
		"name" =>"Нурсеитова Молдир Амангельдыевна",
		"attribute" => "Физ. лицо",
		"IIN" => "880725403034",
		"contacts" => [
			"mobile" => "+77751408999",
		],
	],
	[
		"clientID" =>"000001137",
		"name" =>"Амиржанов Чингис Есетович",
		"attribute" => "Физ. лицо",
		"IIN" => "860130300491",
		"contacts" => [
			"mobile" => "+77017953195",
		],
	],
	[
		"clientID" =>"000001138",
		"name" =>"ФРАНЦЕВ АЛЕКСАНДР ПАВЛОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "440530300384",
		"contacts" => [
			"mobile" => "+77017235049",
		],
	],
	[
		"clientID" =>"000001140",
		"name" =>"Калжанов Абат Аитмаганбетович",
		"attribute" => "Физ. лицо",
		"IIN" => "690501304944",
		"contacts" => [
			"mobile" => "+77024314311",
		],
	],
	[
		"clientID" =>"000001141",
		"name" =>"Кабдоллаев Жомарт Галымович",
		"attribute" => "Физ. лицо",
		"IIN" => "860323301131",
		"contacts" => [
			"mobile" => "+77078351155",
		],
	],
	[
		"clientID" =>"000001143",
		"name" =>"ПАВЛОВ ВАДИМ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77770584865",
		],
	],
	[
		"clientID" =>"000001144",
		"name" =>"ПОДТЕСОВ ДМИТРИЙ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "751027301489",
		"contacts" => [
			"mobile" => "+77076744398",
		],
	],
	[
		"clientID" =>"000001145",
		"name" =>"ИВАНОВ БОРИС БОРИСОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "890408301901",
		"contacts" => [
			"mobile" => "+77714245463",
		],
	],
	[
		"clientID" =>"000001146",
		"name" =>"Падерин Максим Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "800825303146",
		"contacts" => [
			"mobile" => "+77013400454",
		],
		"TCs" => [
			[
				"brand" => "HondaCR-V",
				"size" => "205/70/15",
				"gosNomer" => "B 071 XAN",
			],
		],
		/*
			<subscription>
				<nomer>371</nomer>
				<date>14.04.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001163",
		"name" =>"Чарушев Сергей Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "800330300051",
		"contacts" => [
			"mobile" => "+77772689774",
		],
	],
	[
		"clientID" =>"000001165",
		"name" =>"Сон Владимир Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "801115301192",
		"contacts" => [
			"mobile" => "+77013209696",
		],
	],
	[
		"clientID" =>"000001166",
		"name" =>"Абдыбаев Ашим Зарпенович",
		"attribute" => "Физ. лицо",
		"IIN" => "580401301057",
		"contacts" => [
			"mobile" => "+77017293021",
		],
	],
	[
		"clientID" =>"000001167",
		"name" =>"Аманкулов  Максат (Hyundai)",
		"attribute" => "Физ. лицо",
		"IIN" => "87011301157",
		"contacts" => [
			"mobile" => "+77017952064",
		],
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "185/65/15",
				"gosNomer" => "327CTA/02",
			],
		],
	],
	[
		"clientID" =>"000001168",
		"name" =>"Камшибаев Нурлыбек Калимбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "790110300615",
		"contacts" => [
			"mobile" => "+77717541571",
		],
	],
	[
		"clientID" =>"000001169",
		"name" =>"Лукин Геннадий Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "890315303147",
		"contacts" => [
			"mobile" => "+77770141477",
		],
	],
	[
		"clientID" =>"000001170",
		"name" =>"Поляков Сергей Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "601229300192",
		"contacts" => [
			"mobile" => "+77773949405",
		],
	],
	[
		"clientID" =>"000001171",
		"name" =>"Ким Виктория Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "790905540145",
		"contacts" => [
			"mobile" => "+77017270227",
		],
		"TCs" => [
			[
				"brand" => "LexusRX300",
				"size" => "225/70/16",
				"gosNomer" => "A 156 DVO",
			],
		],
	],
	[
		"clientID" =>"000001172",
		"name" =>"Аубакирова Гульнур Ермековна",
		"attribute" => "Физ. лицо",
		"IIN" => "840521404844",
		"contacts" => [
			"mobile" => "+77071584207",
		],
	],
	[
		"clientID" =>"000001173",
		"name" =>"Омарова Кульбарам Абдрахмановна",
		"attribute" => "Физ. лицо",
		"IIN" => "660412402594",
		"contacts" => [
			"mobile" => "+77072204907",
		],
	],
	[
		"clientID" =>"000001174",
		"name" =>"ЛЕОНТЬЕВА ОЛЬГА СЕРГЕЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "851122400643",
		"contacts" => [
			"mobile" => "+77773190885",
		],
	],
	[
		"clientID" =>"000001175",
		"name" =>"Шорабекова Маргарита Тимуровна",
		"attribute" => "Физ. лицо",
		"IIN" => "940915400096",
		"contacts" => [
			"mobile" => "+77078433686",
		],
	],
	[
		"clientID" =>"000001176",
		"name" =>"Жетенов  Марат  Наушбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "810217300247",
		"contacts" => [
			"mobile" => "+77071122881",
		],
		"TCs" => [
			[
				"brand" => "HYUNDAIACCENT",
				"size" => "185/65/15",
				"gosNomer" => "299WPA/02",
			],
			[
				"brand" => "Tototacamry",
				"size" => "215/55/17",
				"gosNomer" => "717MBA/02",
			],
		],
	],
	[
		"clientID" =>"000001178",
		"name" =>"Костюшок Анатолий Григорьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "700525303104",
		"contacts" => [
			"mobile" => "+77012226886",
		],
	],
	[
		"clientID" =>"000001179",
		"name" =>"КУШЕКБАЕВ ЭРИК БАУЫРЖАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "910929301019",
		"contacts" => [
			"mobile" => "+77777775656",
		],
	],
	[
		"clientID" =>"000001180",
		"name" =>"Аитова Галия Хакимовна",
		"attribute" => "Физ. лицо",
		"IIN" => "650617401856",
		"contacts" => [
			"mobile" => "+77078343815",
		],
		"TCs" => [
			[
				"brand" => "Hyundai Accent",
				"size" => "185/65/15",
				"gosNomer" => "955 MBA/02",
			],
		],
	],
	[
		"clientID" =>"000001181",
		"name" =>"ШИЖАЕВ АЛЕКСАНДР АРКАДЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "831027300716",
		"contacts" => [
			"mobile" => "+77078371620",
		],
	],
	[
		"clientID" =>"000001182",
		"name" =>"БЕРИКБАЕВ АЙДОС МЕЙРАМБАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "840412302090",
		"contacts" => [
			"mobile" => "+77018759999",
		],
		"TCs" => [
			[
				"brand" => "InfinitiFX35",
				"size" => "265/60/18",
				"gosNomer" => "B 888 FFN",
			],
		],
	],
	[
		"clientID" =>"000001183",
		"name" =>"Серикпаев Рахат Омурзакович",
		"attribute" => "Физ. лицо",
		"IIN" => "750605301012",
		"contacts" => [
			"mobile" => "+77014777773",
		],
	],
	[
		"clientID" =>"000001184",
		"name" =>"Бекходжаев Нурлан Нурланулы",
		"attribute" => "Физ. лицо",
		"IIN" => "891226301320",
		"contacts" => [
			"mobile" => "+77056263030",
		],
	],
	[
		"clientID" =>"000001185",
		"name" =>"Пак Елена Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "661024400086",
		"contacts" => [
			"mobile" => "+77017299604",
		],
	],
	[
		"clientID" =>"000001187",
		"name" =>"Кудинов Артур Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "910131301155",
		"contacts" => [
			"mobile" => "+77478065119",
		],
	],
	[
		"clientID" =>"000001190",
		"name" =>"Жиенбаев Батырбек Райсбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "730315301815",
		"contacts" => [
			"mobile" => "+77055756365",
		],
	],
	[
		"clientID" =>"000001195",
		"name" =>"Мангитов Арман Ахтуреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "811018300862",
		"contacts" => [
			"mobile" => "+77017348654",
		],
		"TCs" => [
			[
				"brand" => "Nissan Patfinder",
				"size" => "235/60/18",
				"gosNomer" => "775 OWA/02",
			],
			[
				"brand" => "SubaruLegasy",
				"size" => "215/45/17",
				"gosNomer" => "A 488 RNN",
			],
			[
				"brand" => "SubaruLegacy",
				"size" => "215/45/17",
				"gosNomer" => "A488RNN",
			],
		],
		/*
			<subscription>
				<nomer>0822</nomer>
				<date>06.12.2015 0:00:00</date>
				<srok>11.11.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>308</nomer>
				<date>11.07.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001196",
		"name" =>"Оспанова Акгуль",
		"attribute" => "Физ. лицо",
		"IIN" => "871213400087",
		"contacts" => [
			"mobile" => "+77017799021",
		],
	],
	[
		"clientID" =>"000001218",
		"name" =>"БАЙЖАНОВ ЖАНИБЕК КАДЫРБАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "850910303305",
		"contacts" => [
			"mobile" => "+77029499985",
		],
	],
	[
		"clientID" =>"000001219",
		"name" =>"ПАРХОМЕНКО АНДРЕЙ ЯКОВЛЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "420916300265",
		"contacts" => [
			"mobile" => "+77018210794",
		],
	],
	[
		"clientID" =>"000001220",
		"name" =>"Ахатаев Нурадил Имашевич",
		"attribute" => "Физ. лицо",
		"IIN" => "631027301752",
		"contacts" => [
			"mobile" => "+77770027619",
		],
	],
	[
		"clientID" =>"000001221",
		"name" =>"Акпеисов Бейбит Разакович",
		"attribute" => "Физ. лицо",
		"IIN" => "510401300610",
		"contacts" => [
			"mobile" => "+77019625036",
		],
	],
	[
		"clientID" =>"000001226",
		"name" =>"Зевахин Александр Григорьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "480801302396",
		"contacts" => [
			"mobile" => "+77077890423",
		],
	],
	[
		"clientID" =>"000001229",
		"name" =>"Ушуров Малик Исмарович",
		"attribute" => "Физ. лицо",
		"IIN" => "630415301719",
		"contacts" => [
			"mobile" => "+77011393414",
		],
	],
	[
		"clientID" =>"000001230",
		"name" =>"Лукьянова Виктория",
		"attribute" => "Физ. лицо",
		"IIN" => "850511450433",
		"contacts" => [
			"mobile" => "+77012570606",
		],
	],
	[
		"clientID" =>"000001234",
		"name" =>"Радаев Евгений Викторович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775294467",
		],
	],
	[
		"clientID" =>"000001235",
		"name" =>"Сартолов Даурен Медетханович",
		"attribute" => "Физ. лицо",
		"IIN" => "850112300668",
		"contacts" => [
			"mobile" => "+77078238479",
		],
	],
	[
		"clientID" =>"000001236",
		"name" =>"Пылаев Александр Семенович",
		"attribute" => "Физ. лицо",
		"IIN" => "641230300036",
		"contacts" => [
			"mobile" => "+77772353553",
		],
	],
	[
		"clientID" =>"000001239",
		"name" =>"Михаил  (Аширбаев Нурлан Мухаметгалымович)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016000952",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry40",
				"size" => "215/60/16",
				"gosNomer" => "A252YTN",
			],
		],
	],
	[
		"clientID" =>"000001240",
		"name" =>"Бородавин Георгий Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "670620302124",
		"contacts" => [
			"mobile" => "+77013562413",
		],
	],
	[
		"clientID" =>"000001241",
		"name" =>"Шахов Захар Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "800703300940",
		"contacts" => [
			"mobile" => "+77073111305",
		],
	],
	[
		"clientID" =>"000001242",
		"name" =>"Матаев Еркин Омирбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "721007301524",
		"contacts" => [
			"mobile" => "+77772992537",
		],
	],
	[
		"clientID" =>"000001243",
		"name" =>"Ахметов Асхат Еркинович",
		"attribute" => "Физ. лицо",
		"IIN" => "720207301053",
		"contacts" => [
			"mobile" => "+77778069624",
		],
	],
	[
		"clientID" =>"000001244",
		"name" =>"Аскаров Болат Альбертович",
		"attribute" => "Физ. лицо",
		"IIN" => "870407350029",
	],
	[
		"clientID" =>"000001246",
		"name" =>"Жапарова Бакыткуль Зинельгабиденовна",
		"attribute" => "Физ. лицо",
		"IIN" => "700823401451",
	],
	[
		"clientID" =>"000001248",
		"name" =>"Дюнина  Алина  Николаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "850816400465",
		"contacts" => [
			"mobile" => "+77010887700",
		],
		"TCs" => [
			[
				"brand" => "KIACEED",
				"size" => "205/55/16",
				"gosNomer" => "388LHA/02",
			],
		],
		/*
			<subscription>
				<nomer>0803</nomer>
				<date>26.03.2016 0:00:00</date>
				<srok>07.11.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>1081</nomer>
				<date>27.10.2016 0:00:00</date>
				<srok>08.11.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001249",
		"name" =>"ВЕСЕЛОВА ЮЛИЯ ВЛАДИМИРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "820702450258",
		"contacts" => [
			"mobile" => "+77023481004",
		],
	],
	[
		"clientID" =>"000001254",
		"name" =>"Кабдыкешов Санжар Сериккалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "890410300181",
		"contacts" => [
			"mobile" => "+77784502707",
		],
	],
	[
		"clientID" =>"000001258",
		"name" =>"Еркетаев Максат Жексембекович",
		"attribute" => "Физ. лицо",
		"IIN" => "780708303252",
	],
	[
		"clientID" =>"000001260",
		"name" =>"Садындыкова Самал Имуратовна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017867870",
		],
	],
	[
		"clientID" =>"000001262",
		"name" =>"Ильина Райхан Избасканкызы",
		"attribute" => "Физ. лицо",
		"IIN" => "850421400666",
		"contacts" => [
			"mobile" => "+77077397911",
		],
	],
	[
		"clientID" =>"000001265",
		"name" =>"ДУЙСЕНҒАЛИЕВ РУСЛАН ШАЙДОЛДАҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "770925300325",
		"contacts" => [
			"mobile" => "+77753916181",
		],
	],
	[
		"clientID" =>"000001266",
		"name" =>"Бахтин Александр Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "630214301746",
	],
	[
		"clientID" =>"000001268",
		"name" =>"Налобина Ирина Васильевна",
		"attribute" => "Физ. лицо",
		"IIN" => "641015400836",
		"contacts" => [
			"mobile" => "+77013227561",
		],
	],
	[
		"clientID" =>"000001281",
		"name" =>"Нурдаулетов Галым Амангелдиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "740103300261",
		"contacts" => [
			"mobile" => "+77773070604",
		],
	],
	[
		"clientID" =>"000001283",
		"name" =>"Мамыков Ержан Ахметканович",
		"attribute" => "Физ. лицо",
		"IIN" => "690428302510",
		"contacts" => [
			"mobile" => "+77077219779",
		],
	],
	[
		"clientID" =>"000001285",
		"name" =>"Жорабаев Нурсултан Муратулы",
		"attribute" => "Физ. лицо",
		"IIN" => "910407301249",
		"contacts" => [
			"mobile" => "+77077786670",
		],
	],
	[
		"clientID" =>"000001288",
		"name" =>"Алмабеков Азамат Абдикаримович",
		"attribute" => "Физ. лицо",
		"IIN" => "901206300625",
		"contacts" => [
			"mobile" => "+77475858668",
		],
	],
	[
		"clientID" =>"000001289",
		"name" =>"Идыров Анвар Арзу-Оглы",
		"attribute" => "Физ. лицо",
		"IIN" => "911126300058",
		"contacts" => [
			"mobile" => "+77071901918",
		],
	],
	[
		"clientID" =>"000001290",
		"name" =>"Тулеева Улжалгас Кыдыралиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "871027402934",
		"contacts" => [
			"mobile" => "+77073606087",
		],
	],
	[
		"clientID" =>"000001291",
		"name" =>"Нестеренков Александр Геннадбевич",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77772602832",
		],
	],
	[
		"clientID" =>"000001292",
		"name" =>"Болат Ерлiбек",
		"attribute" => "Физ. лицо",
		"IIN" => "821220302968",
		"contacts" => [
			"mobile" => "+77019078689",
		],
	],
	[
		"clientID" =>"000001294",
		"name" =>"Мажитов Алишер Фахритдинович",
		"attribute" => "Юр. лицо",
		"IIN" => "820915300987",
		"contacts" => [
			"mobile" => "+77777888430",
		],
	],
	[
		"clientID" =>"000001295",
		"name" =>"Тулеужанова Кымбат Есембаевна",
		"attribute" => "Юр. лицо",
		"IIN" => "731124402339",
		"contacts" => [
			"mobile" => "+77771867555",
		],
	],
	[
		"clientID" =>"000001296",
		"name" =>"Джаксылыкова Сабина Аскаровна",
		"attribute" => "Физ. лицо",
		"IIN" => "920118400119",
		"contacts" => [
			"mobile" => "+77022678706",
		],
	],
	[
		"clientID" =>"000001302",
		"name" =>"Ким Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773300003",
		],
	],
	[
		"clientID" =>"000001304",
		"name" =>"Ахметова Эльвира",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015154247",
		],
		"TCs" => [
			[
				"brand" => "ScionXA",
				"size" => "195/60/15",
				"gosNomer" => "A075TKO",
			],
		],
	],
	[
		"clientID" =>"000001306",
		"name" =>"Аветисян Георгий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017332130",
		],
	],
	[
		"clientID" =>"000001310",
		"name" =>"Ушкемпиров Ельдос Жаксылыкулы",
		"attribute" => "Физ. лицо",
		"IIN" => "781201301183",
		"contacts" => [
			"mobile" => "+77765777011",
		],
	],
	[
		"clientID" =>"000001318",
		"name" =>"Лерх Надежда Николаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "850702402560",
		"contacts" => [
			"mobile" => "+77054153232",
		],
	],
	[
		"clientID" =>"000001321",
		"name" =>"Потураев Александр Алексеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "541006301471",
		"contacts" => [
			"mobile" => "+77017168825",
		],
	],
	[
		"clientID" =>"000001324",
		"name" =>"Исмаилов Сейiлхан Киiкбайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "700317301474",
		"contacts" => [
			"mobile" => "+77017336541",
		],
	],
	[
		"clientID" =>"000001340",
		"name" =>"Абесов Нурсултан Жаркынулы",
		"attribute" => "Физ. лицо",
		"IIN" => "920926301708",
		"contacts" => [
			"mobile" => "+77785275778",
		],
	],
	[
		"clientID" =>"000001341",
		"name" =>"Кириченко Петр Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "880223300193",
		"contacts" => [
			"mobile" => "+77052228352",
		],
	],
	[
		"clientID" =>"000001342",
		"name" =>"Азаматова Индира Толеубековна",
		"attribute" => "Физ. лицо",
		"IIN" => "660520400025",
		"contacts" => [
			"mobile" => "+77773887883",
		],
		"TCs" => [
			[
				"brand" => "NissanQashqai",
				"size" => "215/65/16",
				"gosNomer" => "859 KBA/02",
			],
		],
	],
	[
		"clientID" =>"000001344",
		"name" =>"Бекенов Ержан Сериккалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760601300629",
		"contacts" => [
			"mobile" => "+77018485074",
		],
	],
	[
		"clientID" =>"000001346",
		"name" =>"Бутинчинов Жандос Намазбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820424300383",
		"contacts" => [
			"mobile" => "+77770907733",
		],
	],
	[
		"clientID" =>"000001348",
		"name" =>"Майлыбаев Елдос Серикханович",
		"attribute" => "Физ. лицо",
		"IIN" => "801218302061",
	],
	[
		"clientID" =>"000001349",
		"name" =>"Габитова Марина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014943849",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHighlander",
				"size" => "225/70/16",
				"gosNomer" => "A 561 PZO",
			],
			[
				"brand" => "ToyotaRav4",
				"size" => "225/65/17",
				"gosNomer" => "A692PXA/02",
			],
		],
	],
	[
		"clientID" =>"000001350",
		"name" =>"Кусков Иван Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780531301343",
		"contacts" => [
			"mobile" =>"+77020930024",
			"mobile" => "+77052095096",
		],
	],
	[
		"clientID" =>"000001351",
		"name" =>"Калиев Жасулан Орынбасарович",
		"attribute" => "Физ. лицо",
		"IIN" => "810524302183",
		"contacts" => [
			"mobile" => "+77077030211",
		],
	],
	[
		"clientID" =>"000001354",
		"name" =>"ОСТРОВСКИЙ ВАЛЕРИЙ РОМАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "480313300436",
		"contacts" => [
			"mobile" => "+77074525276",
		],
	],
	[
		"clientID" =>"000001356",
		"name" =>"Жанабайулы Еркин",
		"attribute" => "Физ. лицо",
		"IIN" => "890220301533",
		"contacts" => [
			"mobile" => "+77752326262",
		],
	],
	[
		"clientID" =>"000001357",
		"name" =>"Сафарова Каусар",
		"attribute" => "Физ. лицо",
		"IIN" => "901028400397",
		"contacts" => [
			"mobile" => "+77029264442",
		],
	],
	[
		"clientID" =>"000001358",
		"name" =>"Дмитриев Алексей Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "670113301625",
		"contacts" => [
			"mobile" => "+77776692977",
		],
	],
	[
		"clientID" =>"000001360",
		"name" =>"Аубакиров Ерлан Елемесович",
		"attribute" => "Физ. лицо",
		"IIN" => "811031301029",
		"contacts" => [
			"mobile" => "+77018471066",
		],
	],
	[
		"clientID" =>"000001362",
		"name" =>"Кисалиев Дархан Абдраимович",
		"attribute" => "Физ. лицо",
		"IIN" => "810828300962",
		"contacts" => [
			"mobile" => "+77475640663",
		],
	],
	[
		"clientID" =>"000001363",
		"name" =>"НҰРЖІГІТ АЙДЫН АБДУЛЛАҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "900207300579",
		"contacts" => [
			"mobile" => "+77714636618",
		],
	],
	[
		"clientID" =>"000001364",
		"name" =>"Ерболатов Арнур Ержанович",
		"attribute" => "Физ. лицо",
		"IIN" => "810318300830",
		"contacts" => [
			"mobile" => "+77760100009",
		],
	],
	[
		"clientID" =>"000001365",
		"name" =>"Корчиев Бунёд Абдувалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820814301103",
		"contacts" => [
			"mobile" => "+77052424245",
		],
	],
	[
		"clientID" =>"000001366",
		"name" =>"Хайрутдинов Руслан Харасович",
		"attribute" => "Физ. лицо",
		"IIN" => "890815301334",
		"contacts" => [
			"mobile" => "+77771396929",
		],
	],
	[
		"clientID" =>"000001367",
		"name" =>"МУРЗАГУЛОВ АЯН КЕНЕСОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "840619301701",
		"contacts" => [
			"mobile" => "+77018948161",
		],
	],
	[
		"clientID" =>"000001368",
		"name" =>"Калымкулова Саида",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014445590",
		],
	],
	[
		"clientID" =>"000001369",
		"name" =>"ВИЛЛЕНА ЛАНЗИ ОЛЬГА БОРИСОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "800525403716",
		"contacts" => [
			"mobile" => "+77013928292",
		],
	],
	[
		"clientID" =>"000001370",
		"name" =>"Конысбаев Омир Елеусизович",
		"attribute" => "Физ. лицо",
		"IIN" => "791020303584",
		"contacts" => [
			"mobile" => "+77786059264",
		],
	],
	[
		"clientID" =>"000001371",
		"name" =>"Кожахметов Искандер Султанулы",
		"attribute" => "Физ. лицо",
		"IIN" => "780601300048",
		"contacts" => [
			"mobile" => "+77774279007",
		],
	],
	[
		"clientID" =>"000001374",
		"name" =>"Шкенов Марат Алимханович",
		"attribute" => "Физ. лицо",
		"IIN" => "840613302539",
		"contacts" => [
			"mobile" => "+77017179095",
		],
	],
	[
		"clientID" =>"000001375",
		"name" =>"Калдыбаев Данияр",
		"attribute" => "Физ. лицо",
		"IIN" => "760415302911",
		"contacts" => [
			"mobile" => "+77019996556",
		],
	],
	[
		"clientID" =>"000001376",
		"name" =>"Османов Рустам Мухамедович",
		"attribute" => "Физ. лицо",
		"IIN" => "860502300295",
		"contacts" => [
			"mobile" => "+77026517506",
		],
	],
	[
		"clientID" =>"000001377",
		"name" =>"МЕНЕЕВ АРТУР АБАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "800126302009",
		"contacts" => [
			"mobile" => "+77771397556",
		],
	],
	[
		"clientID" =>"000001378",
		"name" =>"ИСАЕВА АЛИЯ ДАНИЯРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "730418400912",
		"contacts" => [
			"mobile" => "+77778079911",
		],
	],
	[
		"clientID" =>"000001380",
		"name" =>"Жалымбетова Айгуль Кудайбергеновна",
		"attribute" => "Физ. лицо",
		"IIN" => "780530403360",
		"contacts" => [
			"mobile" => "+77017455940",
		],
	],
	[
		"clientID" =>"000001382",
		"name" =>"Эсте Лаудер Казахстан ТОО",
		"attribute" => "Юр. лицо",
		"IIN" => "080840013718",
		"contacts" => [
			"mobile" => "+77770021941",
		],
	],
	[
		"clientID" =>"000001385",
		"name" =>"Савинова Наталья Сергеевна",
		"attribute" => "Физ. лицо",
		"IIN" => "820811401252",
		"contacts" => [
			"mobile" => "+77078329955",
		],
	],
	[
		"clientID" =>"000001386",
		"name" =>"Мусиев Талгат Дюсумбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "870525300916",
	],
	[
		"clientID" =>"000001387",
		"name" =>"Гордеев  Иосиф  Вадимович",
		"attribute" => "Физ. лицо",
		"IIN" => "800306300596",
		"contacts" => [
			"mobile" => "+77775140870",
		],
		"TCs" => [
			[
				"brand" => "ToyotaMarino",
				"size" => "185/65/14",
				"gosNomer" => "A931EYO",
			],
		],
		/*
			<subscription>
				<nomer>0864</nomer>
				<date>26.10.2016 0:00:00</date>
				<srok>14.03.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001389",
		"name" =>"Момбаев Ержан Сапарханович",
		"attribute" => "Физ. лицо",
		"IIN" => "890327301773",
		"contacts" => [
			"mobile" => "+77027467090",
		],
	],
	[
		"clientID" =>"000001390",
		"name" =>"Калымов Рашит Бахытович",
		"attribute" => "Физ. лицо",
		"IIN" => "761118300418",
		"contacts" => [
			"mobile" => "+77017467338",
		],
	],
	[
		"clientID" =>"000001391",
		"name" =>"Батырханов Муратбек Сулейменович",
		"attribute" => "Физ. лицо",
		"IIN" => "701028301496",
		"contacts" => [
			"mobile" => "+77717490861",
		],
	],
	[
		"clientID" =>"000001392",
		"name" =>"Онгаров Нариман Булатович",
		"attribute" => "Физ. лицо",
		"IIN" => "811012301917",
		"contacts" => [
			"mobile" => "+77475259388",
		],
	],
	[
		"clientID" =>"000001394",
		"name" =>"Цымбалов Кирилл Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "900404350398",
		"contacts" => [
			"mobile" => "+77471344845",
		],
	],
	[
		"clientID" =>"000001395",
		"name" =>"КАСЫМОВ ЕРЖАН КАКИМОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "640908300049",
		"contacts" => [
			"mobile" => "+77711169976",
		],
	],
	[
		"clientID" =>"000001396",
		"name" =>"Игликов  Тимур  Даулетович",
		"attribute" => "Физ. лицо",
		"IIN" => "910222300423",
		"contacts" => [
			"mobile" => "+77012188418",
		],
		"TCs" => [
			[
				"brand" => "Mercedes",
				"size" => "205/55/16",
				"gosNomer" => "649MCA/02",
			],
		],
	],
	[
		"clientID" =>"000001398",
		"name" =>"Тойжанов Женис Аманжолович",
		"attribute" => "Физ. лицо",
		"IIN" => "710917300340",
		"contacts" => [
			"mobile" => "+77011113418",
		],
	],
	[
		"clientID" =>"000001399",
		"name" =>"Кузьмин Виктор Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "810508302295",
		"contacts" => [
			"mobile" => "+77077003637",
		],
	],
	[
		"clientID" =>"000001400",
		"name" =>"Баталов Бахытжан Кажгалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "871002301715",
		"contacts" => [
			"mobile" => "+77772226056",
		],
		"TCs" => [
			[
				"brand" => "NissanTeana",
				"size" => "225/55/17",
				"gosNomer" => "A 959 VWO",
			],
		],
	],
	[
		"clientID" =>"000001401",
		"name" =>"Толочев Леонид",
		"attribute" => "Физ. лицо",
		"IIN" => "580308300529",
		"contacts" => [
			"mobile" => "+77773959199",
		],
	],
	[
		"clientID" =>"000001402",
		"name" =>"Шкердин  Олег   (Toyota)",
		"attribute" => "Физ. лицо",
		"IIN" => "680609301306",
		"contacts" => [
			"mobile" => "+77772696706",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHighlander",
				"size" => "225/65/17",
				"gosNomer" => "A824WRO",
			],
		],
	],
	[
		"clientID" =>"000001403",
		"name" =>"Урымханова Галия Бауржановна",
		"attribute" => "Физ. лицо",
		"IIN" => "860319401651",
		"contacts" => [
			"mobile" => "+77019477199",
		],
	],
	[
		"clientID" =>"000001404",
		"name" =>"Толегенов Кайрат Маратулы",
		"attribute" => "Физ. лицо",
		"IIN" => "930426301305",
		"contacts" => [
			"mobile" => "+77029247534",
		],
	],
	[
		"clientID" =>"000001406",
		"name" =>"Колякова Сауле",
		"attribute" => "Физ. лицо",
		"IIN" => "710529400328",
		"contacts" => [
			"mobile" => "+77014167699",
		],
	],
	[
		"clientID" =>"000001407",
		"name" =>"Данагулов Ерлан",
		"attribute" => "Физ. лицо",
		"IIN" => "820628301437",
		"contacts" => [
			"mobile" => "+77021777325",
		],
	],
	[
		"clientID" =>"000001409",
		"name" =>"Базаев Буркутгазы Кутчанович",
		"attribute" => "Физ. лицо",
		"IIN" => "620319300757",
		"contacts" => [
			"mobile" => "+77771912042",
		],
	],
	[
		"clientID" =>"000001410",
		"name" =>"Ахмедшина Данира Наибовна",
		"attribute" => "Физ. лицо",
		"IIN" => "750423402098",
		"contacts" => [
			"mobile" => "+77772223554",
		],
		"TCs" => [
			[
				"brand" => "RangeRover",
				"size" => "235/60/18",
				"gosNomer" => "215 KTA02",
			],
		],
		/*
			<subscription>
				<nomer>0305</nomer>
				<date>22.04.2015 0:00:00</date>
				<srok>01.12.2015 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>1179</nomer>
				<date>15.11.2016 0:00:00</date>
				<srok>03.12.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001411",
		"name" =>"Шманов Адэль Умирзакович",
		"attribute" => "Физ. лицо",
		"IIN" => "780827301540",
		"contacts" => [
			"mobile" => "+77772762696",
		],
	],
	[
		"clientID" =>"000001412",
		"name" =>"Мустафаев Ринат Талгатович",
		"attribute" => "Физ. лицо",
		"IIN" => "861217300388",
		"contacts" => [
			"mobile" => "+77474545612",
		],
	],
	[
		"clientID" =>"000001413",
		"name" =>"Азикеев Рустам Сулейманович",
		"attribute" => "Физ. лицо",
		"IIN" => "850610301417",
		"contacts" => [
			"mobile" => "+77072552266",
		],
	],
	[
		"clientID" =>"000001414",
		"name" =>"Симонов Алексей Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "670623399064",
		"contacts" => [
			"mobile" => "+77012666732",
		],
	],
	[
		"clientID" =>"000001415",
		"name" =>"ЕЛУБАЕВ БЕГАРС СМАГУЛОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "741210301690",
		"contacts" => [
			"mobile" => "+77777717250",
		],
	],
	[
		"clientID" =>"000001418",
		"name" =>"Пак Борис",
		"attribute" => "Физ. лицо",
		"IIN" => "430317300303",
	],
	[
		"clientID" =>"000001419",
		"name" =>"Ковалева Светлана Валерьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "780330400804",
		"contacts" => [
			"mobile" => "+77772380728",
		],
	],
	[
		"clientID" =>"000001421",
		"name" =>"Береговенко Александр Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "760714300336",
		"contacts" => [
			"mobile" => "+77012668551",
		],
	],
	[
		"clientID" =>"000001422",
		"name" =>"ЕФИМОВ СЕРГЕЙ АНАТОЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "770617300589",
		"contacts" => [
			"mobile" => "+77089080000",
		],
	],
	[
		"clientID" =>"000001424",
		"name" =>"КУЛЬЖАНОВ АЛИБЕК КАНАТБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "890326300373",
		"contacts" => [
			"mobile" => "+77019516170",
		],
	],
	[
		"clientID" =>"000001426",
		"name" =>"Пырликов Андрей Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "711020300743",
		"contacts" => [
			"mobile" => "+77017772829",
		],
	],
	[
		"clientID" =>"000001427",
		"name" =>"ЛОЗИЦКИЙ АРТЕМ ОЛЕГОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "901114300060",
		"contacts" => [
			"mobile" =>"+77016110502",
			"mobile" => "+77016110502",
		],
	],
	[
		"clientID" =>"000001428",
		"name" =>"Тастанбеков Руслан Демократович",
		"attribute" => "Физ. лицо",
		"IIN" => "830209301374",
		"contacts" => [
			"mobile" => "+77079004083",
		],
	],
	[
		"clientID" =>"000001430",
		"name" =>"Пржекенов Сарсенбай Маханбетович",
		"attribute" => "Физ. лицо",
		"IIN" => "541019301175",
	],
	[
		"clientID" =>"000001432",
		"name" =>"ЖИЛКИБАЕВ ДАНИЯР БАКЫТБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "830212350943",
		"contacts" => [
			"mobile" => "+77015500552",
		],
	],
	[
		"clientID" =>"000001434",
		"name" =>"Муканов Нуржан Нуриденович",
		"attribute" => "Физ. лицо",
		"IIN" => "910622300567",
		"contacts" => [
			"mobile" => "+77013537117",
		],
	],
	[
		"clientID" =>"000001435",
		"name" =>"Ламбекова Тогжан Ерсайыновна",
		"attribute" => "Физ. лицо",
		"IIN" => "750417400198",
		"contacts" => [
			"mobile" => "+77017335125",
		],
	],
	[
		"clientID" =>"000001436",
		"name" =>"Жумашев Ерлан Кожахметович",
		"attribute" => "Физ. лицо",
		"IIN" => "710629300503",
		"contacts" => [
			"mobile" => "+77012928477",
		],
	],
	[
		"clientID" =>"000001438",
		"name" =>"Самидинов Казбек Мурадинович",
		"attribute" => "Физ. лицо",
		"IIN" => "900507302308",
		"contacts" => [
			"mobile" => "+77479011430",
		],
	],
	[
		"clientID" =>"000001439",
		"name" =>"Максумов Рахимжан Маликович",
		"attribute" => "Физ. лицо",
		"IIN" => "840821302004",
		"contacts" => [
			"mobile" => "+77077707132",
		],
	],
	[
		"clientID" =>"000001442",
		"name" =>"Арзиев Жарасбай Муратбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860722303890",
		"contacts" => [
			"mobile" => "+77714540401",
		],
	],
	[
		"clientID" =>"000001443",
		"name" =>"Алагузов Утеули",
		"attribute" => "Физ. лицо",
		"IIN" => "660514301297",
		"contacts" => [
			"mobile" => "+77016276549",
		],
	],
	[
		"clientID" =>"000001444",
		"name" =>"Абалуев Константин Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "890427301717",
		"contacts" => [
			"mobile" => "+77771711439",
		],
	],
	[
		"clientID" =>"000001445",
		"name" =>"Кудряшов Константин",
		"attribute" => "Физ. лицо",
		"IIN" => "930103300093",
	],
	[
		"clientID" =>"000001446",
		"name" =>"Камалов Замир",
		"attribute" => "Физ. лицо",
		"IIN" => "860630301349",
		"contacts" => [
			"mobile" => "+77012274309",
		],
	],
	[
		"clientID" =>"000001448",
		"name" =>"Рыжов Роман Львович",
		"attribute" => "Физ. лицо",
		"IIN" => "840310301206",
		"contacts" => [
			"mobile" =>"+77078408580",
			"mobile" => "+77078408580",
		],
	],
	[
		"clientID" =>"000001450",
		"name" =>"Абибов Вадим Вагифович",
		"attribute" => "Физ. лицо",
		"IIN" => "880929300042",
		"contacts" => [
			"mobile" => "+77017778007",
		],
	],
	[
		"clientID" =>"000001452",
		"name" =>"Тасболатулы Дастан",
		"attribute" => "Физ. лицо",
		"IIN" => "881119300359",
		"contacts" => [
			"mobile" => "+77717438980",
		],
	],
	[
		"clientID" =>"000001453",
		"name" =>"Копжасаров Мади Маулетович",
		"attribute" => "Физ. лицо",
		"IIN" => "801111301745",
	],
	[
		"clientID" =>"000001454",
		"name" =>"Мосев Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "651108301617",
		"contacts" => [
			"mobile" => "+77017858637",
		],
	],
	[
		"clientID" =>"000001455",
		"name" =>"Симонов Виталий",
		"attribute" => "Физ. лицо",
		"IIN" => "741105300443",
		"contacts" => [
			"mobile" => "+77714737468",
		],
	],
	[
		"clientID" =>"000001456",
		"name" =>"Негаметзянов Гарифзян Мухамедзянович",
		"attribute" => "Физ. лицо",
		"IIN" => "391113300016",
		"contacts" => [
			"mobile" => "+77051820183",
		],
	],
	[
		"clientID" =>"000001457",
		"name" =>"Василова Мубаряк Махмутжановна",
		"attribute" => "Физ. лицо",
		"IIN" => "840122401815",
		"contacts" => [
			"mobile" => "+77023159313",
		],
	],
	[
		"clientID" =>"000001458",
		"name" =>"Пимакин Андрей Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "841129300158",
		"contacts" => [
			"mobile" => "+77773552375",
		],
	],
	[
		"clientID" =>"000001459",
		"name" =>"Ахметчеев Данияр Исмаилович",
		"attribute" => "Физ. лицо",
		"IIN" => "720528300744",
		"contacts" => [
			"mobile" => "+77017408072",
		],
	],
	[
		"clientID" =>"000001460",
		"name" =>"Бегенов Бауржан Аульканович",
		"attribute" => "Физ. лицо",
		"IIN" => "731101302307",
		"contacts" => [
			"mobile" => "+77016882233",
		],
	],
	[
		"clientID" =>"000001461",
		"name" =>"Мажитов Саттар Фазылович",
		"attribute" => "Юр. лицо",
		"IIN" => "640615301807",
		"contacts" => [
			"mobile" => "+77013277614",
		],
	],
	[
		"clientID" =>"000001462",
		"name" =>"Ергужиева Майра",
		"attribute" => "Физ. лицо",
		"IIN" => "680501403659",
		"contacts" => [
			"mobile" => "+77078201622",
		],
	],
	[
		"clientID" =>"000000087",
		"name" =>"Назаренко Игорь Владимирович",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000131",
		"name" =>"Мусаева Тахмина",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000000532",
		"name" =>"Клиент",
		
		
	],
	[
		"clientID" =>"000000533",
		"name" =>"Третьякова Ольга Васильевна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075698724",
		],
		"TCs" => [
			[
				"brand" => "LEXUS RX 270",
				"size" => "235/55/19",
				"gosNomer" => "860PUA/02",
			],
		],
	],
	[
		"clientID" =>"000000534",
		"name" =>"Ваничкин  Максим  Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820118301318",
		"contacts" => [
			"mobile" => "+77051890890",
		],
		"TCs" => [
			[
				"brand" => "LexusRX330",
				"size" => "235/55/18",
				"gosNomer" => "A132XMO",
			],
		],
	],
	[
		"clientID" =>"000000535",
		"name" =>"Касенгали  Динара  Орынбайкызы",
		"attribute" => "Физ. лицо",
		"IIN" => "850823400740",
		"contacts" => [
			"mobile" => "+77012229356",
		],
		"TCs" => [
			[
				"brand" => "Infiniti",
				"size" => "255/55/18",
				"gosNomer" => "233YUA/02",
			],
		],
		/*
			<subscription>
				<nomer>1083</nomer>
				<date>01.01.0001 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000537",
		"name" =>"БОРИСКИН АЛЕКСЕЙ НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "770527302685",
		"contacts" => [
			"mobile" => "+77015219598",
		],
	],
	[
		"clientID" =>"000000538",
		"name" =>"Аттакуров Бектияр Каныбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "881013302180",
		"contacts" => [
			"mobile" => "+77023139869",
		],
	],
	[
		"clientID" =>"000000539",
		"name" =>"Цой Карина Анатольевна",
		"attribute" => "Физ. лицо",
		"IIN" => "910708450021",
		"contacts" => [
			"mobile" => "+77015444998",
		],
		"TCs" => [
			[
				
				"size" => "215/60/17",
				
			],
		],
		/*
			<subscription>
				<nomer>395</nomer>
				<date>19.11.2014 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000540",
		"name" =>"Адамбеков Кайрат Ильясович",
		"attribute" => "Физ. лицо",
		"IIN" => "480103301849",
		"contacts" => [
			"mobile" => "+77013671891",
		],
		"TCs" => [
			[
				
				"size" => "205/55/16",
				
			],
		],
		/*
			<subscription>
				<nomer>245</nomer>
				<date>03.11.2014 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000541",
		"name" =>"Жумабаева  Айгерим  Багдатовна",
		"attribute" => "Физ. лицо",
		"IIN" => "841022400182",
		"contacts" => [
			"mobile" => "+77015519333",
		],
		"TCs" => [
			[
				"brand" => "ToyotaAvensis",
				"size" => "205/60/16",
				"gosNomer" => "894 EYA/02",
			],
		],
	],
	[
		"clientID" =>"000000542",
		"name" =>"Алпысбаева Базаркуль Панатовна",
		"attribute" => "Физ. лицо",
		"IIN" => "501213401036",
		"contacts" => [
			"mobile" => "+77783232060",
		],
		"TCs" => [
			[
				"brand" => "NISSAN Note",
				"size" => "175/65/15",
				"gosNomer" => "A 882 EVP",
			],
		],
	],
	[
		"clientID" =>"000000543",
		"name" =>"КАРИМУЛЛИН АСКАР АМАНГЕЛЬДИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "761227300014",
		"contacts" => [
			"mobile" => "+77759999154",
		],
	],
	[
		"clientID" =>"000000544",
		"name" =>"ТЛЕУБЕРГЕНОВ МАРЛЕН СМАНТАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "660716300504",
		"contacts" => [
			"mobile" => "+77010179997",
		],
	],
	[
		"clientID" =>"000000545",
		"name" =>"Дауылбаев Ерулан Сейсеханович",
		"attribute" => "Физ. лицо",
		"IIN" => "501103300845",
		"contacts" => [
			"mobile" => "+77013116232",
		],
		"TCs" => [
			[
				"brand" => "ToyotaSequoia",
				"size" => "265/70/18",
				"gosNomer" => "A978XLO",
			],
			[
				"brand" => "Toyota   Sequoia",
				"size" => "275/65/18",
				"gosNomer" => "A978XLO",
			],
			[
				"brand" => "Toyota   RAV-4",
				"size" => "225/65/17",
				"gosNomer" => "Z566WSM",
			],
		],
	],
	[
		"clientID" =>"000000547",
		"name" =>"НАРЖАНОВ АРМАН БАУРЖАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "830327300146",
		"contacts" => [
			"mobile" => "+77017873002",
		],
	],
	[
		"clientID" =>"000000548",
		"name" =>"Галкин Евгений Евгеньевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860730000088",
		"contacts" => [
			"mobile" => "+77012227660",
		],
	],
	[
		"clientID" =>"000000549",
		"name" =>"Носко Александр Иванович",
		"attribute" => "Физ. лицо",
		"IIN" => "571224300959",
	],
	[
		"clientID" =>"000000550",
		"name" =>"Бегалиев Маратхан",
		"attribute" => "Физ. лицо",
		"IIN" => "740922301321",
		"contacts" => [
			"mobile" => "+77011107772",
		],
		"TCs" => [
			[
				"brand" => "LexusRX400h",
				"size" => "235/55/18",
				"gosNomer" => "324 BTA/02",
			],
		],
	],
	[
		"clientID" =>"000000551",
		"name" =>"Адилханова Жанна Адилхановна",
		"attribute" => "Физ. лицо",
		"IIN" => "830919400302",
		"contacts" => [
			"mobile" => "+77777021577",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCorolla",
				"size" => "205/55/16",
				"gosNomer" => "614 BLA/02",
			],
		],
		/*
			<subscription>
				<nomer>1001</nomer>
				<date>13.06.2016 0:00:00</date>
				<srok>13.06.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>534</nomer>
				<date>25.01.2016 0:00:00</date>
				<srok>13.06.2016 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000555",
		"name" =>"Касперский Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "851013350145",
	],
	[
		"clientID" =>"000001931",
		"name" =>"ЗОЛЬНИКОВА ЮЛИАННА ОЛЕГОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "791113400086",
		"contacts" => [
			"mobile" => "+77714037355",
		],
	],
	[
		"clientID" =>"000001932",
		"name" =>"Нусипбеков Естемес",
		"attribute" => "Физ. лицо",
		"IIN" => "710215302132",
		"contacts" => [
			"mobile" => "+77025700000",
		],
	],
	[
		"clientID" =>"000001933",
		"name" =>"Петров Виталий Георгиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "520401300503",
		"contacts" => [
			"mobile" => "+77051482733",
		],
	],
	[
		"clientID" =>"000001934",
		"name" =>"АРХИПОВ СЕРГЕЙ ПРОКОФЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "541021300524",
	],
	[
		"clientID" =>"000001935",
		"name" =>"Амиров Батырбек Джуманович",
		"attribute" => "Физ. лицо",
		"IIN" => "740129302428",
		"contacts" => [
			"mobile" => "+77015780509",
		],
	],
	[
		"clientID" =>"000001936",
		"name" =>"Панагушин Сергей Валентинович",
		"attribute" => "Физ. лицо",
		"IIN" => "831202301468",
		"contacts" => [
			"mobile" => "+77026025424",
		],
	],
	[
		"clientID" =>"000001937",
		"name" =>"КОМИССАРОВ МИХАИЛ МИХАЙЛОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "900929351559",
	],
	[
		"clientID" =>"000001938",
		"name" =>"Зандаров Камал Ферамозович",
		"attribute" => "Физ. лицо",
		"IIN" => "841202300977",
	],
	[
		"clientID" =>"000001939",
		"name" =>"УМАРОВА САУЛЕЖАН КУАНЫШЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "740221400745",
	],
	[
		"clientID" =>"000001940",
		"name" =>"ЖУЛИЙ ДЕНИС АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "780322300448",
	],
	[
		"clientID" =>"000001941",
		"name" =>"БЕЛОУСОВ АЛЕКСАНДР АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820918301525",
	],
	[
		"clientID" =>"000001942",
		"name" =>"КАН АННА АЛЕКСАНДРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "811231400689",
	],
	[
		"clientID" =>"000001943",
		"name" =>"РАССКАЗОВ ВИТАЛИЙ НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "630415399056",
	],
	[
		"clientID" =>"000001944",
		"name" =>"Косбаев Ержан Манапович",
		"attribute" => "Физ. лицо",
		"IIN" => "691104300822",
	],
	[
		"clientID" =>"000001945",
		"name" =>"ХМЫРОВ ВИКТОР АНАТОЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "530615301705",
	],
	[
		"clientID" =>"000001946",
		"name" =>"КАМЫТБЕКОВ СЕЙТЖАН БАХЫТЖАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "900220300877",
	],
	[
		"clientID" =>"000001947",
		"name" =>"Бектемиров Едил",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000001948",
		"name" =>"НУРМУХАНБЕТОВА ДИНАРА КЕНЖЕЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "650802402126",
		"contacts" => [
			"mobile" => "+77016020882",
		],
	],
	[
		"clientID" =>"000001951",
		"name" =>"САМАКОВ САМАТ ТАЗАБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820701302127",
		"contacts" => [
			"mobile" => "+77012140530",
		],
	],
	[
		"clientID" =>"000001953",
		"name" =>"МУСИН РУСЛАН ЕРЛАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "890709301086",
	],
	[
		"clientID" =>"000001955",
		"name" =>"Бектасов Темирхан Жилкайдарович",
		"attribute" => "Физ. лицо",
		"IIN" => "560807301378",
		"contacts" => [
			"mobile" => "+77013689564",
		],
	],
	[
		"clientID" =>"000001959",
		"name" =>"Гаврилов Сергей Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "871122300670",
		"contacts" => [
			"mobile" => "+77777059496",
		],
	],
	[
		"clientID" =>"000001960",
		"name" =>"АБДРАЗАКОВ ДАНИЯР МИНИЯРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "620805350021",
		"contacts" => [
			"mobile" => "+77051542366",
		],
	],
	[
		"clientID" =>"000001961",
		"name" =>"Александров Сергей Георгиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "560622301491",
		"contacts" => [
			"mobile" => "+77076405906",
		],
	],
	[
		"clientID" =>"000001962",
		"name" =>"Нұрқасым Олжас Айдарұлы",
		"attribute" => "Физ. лицо",
		"IIN" => "930531300191",
		"contacts" => [
			"mobile" => "+77013692222",
		],
	],
	[
		"clientID" =>"000001963",
		"name" =>"Ерлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017206046",
		],
	],
	[
		"clientID" =>"000001964",
		"name" =>"Балгимбаев Даурен Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "830319300453",
		"contacts" => [
			"mobile" => "+77782800841",
		],
	],
	[
		"clientID" =>"000001965",
		"name" =>"Алмуханбетов Ринат Жармуханбетович",
		"attribute" => "Физ. лицо",
		"IIN" => "820625301215",
		"contacts" => [
			"mobile" => "+77475435183",
		],
	],
	[
		"clientID" =>"000001966",
		"name" =>"Набиева Алмагул Амангелдиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "780615401079",
		"contacts" => [
			"mobile" => "+77013858009",
		],
	],
	[
		"clientID" =>"000001969",
		"name" =>"Нуржанов Тулеген Серикович",
		"attribute" => "Физ. лицо",
		"IIN" => "860210300535",
		"contacts" => [
			"mobile" => "+77017885755",
		],
	],
	[
		"clientID" =>"000001970",
		"name" =>"Жиенкул Абылайхан Оралбекұлы",
		"attribute" => "Физ. лицо",
		"IIN" => "930604301401",
		"contacts" => [
			"mobile" => "+77026051521",
		],
	],
	[
		"clientID" =>"000001971",
		"name" =>"Ильина Елена Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "680821400199",
		"contacts" => [
			"mobile" => "+77054004040",
		],
	],
	[
		"clientID" =>"000001972",
		"name" =>"Шенгоф Павел Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "820705300757",
		"contacts" => [
			"mobile" => "+77012775259",
		],
	],
	[
		"clientID" =>"000001973",
		"name" =>"Смайлова Назым Даулетовна",
		"attribute" => "Физ. лицо",
		"IIN" => "780205400923",
		"contacts" => [
			"mobile" => "+77077212850",
		],
	],
	[
		"clientID" =>"000001974",
		"name" =>"Цой Дмитрий Анатольевич",
		"attribute" => "Юр. лицо",
		"IIN" => "680907301445",
		"contacts" => [
			"mobile" => "+77773388444",
		],
	],
	[
		"clientID" =>"000001975",
		"name" =>"Егисбаев Нуржан Оспанханович",
		"attribute" => "Физ. лицо",
		"IIN" => "580324301178",
		"contacts" => [
			"mobile" => "+77052204209",
		],
	],
	[
		"clientID" =>"000001976",
		"name" =>"Луценко Георгий Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "880808300024",
		"contacts" => [
			"mobile" => "+77072762768",
		],
	],
	[
		"clientID" =>"000000093",
		"name" =>"КИМ ВЕРА ВАЛЕРЬЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "851126400902",
		"contacts" => [
			"mobile" => "+77077742220",
		],
	],
	[
		"clientID" =>"000000094",
		"name" =>"МОЛДАЖАНОВ ЕРЖАН ЕРИКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "830211301167",
		"contacts" => [
			"mobile" => "+77015383860",
		],
	],
	[
		"clientID" =>"000000834",
		"name" =>"РУМЯНЦЕВ ВАЛЕРИЙ ДМИТРИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "471110300275",
		"contacts" => [
			"mobile" => "+77772954786",
		],
	],
	[
		"clientID" =>"000000835",
		"name" =>"Рябкин  Владимир",
		"attribute" => "Физ. лицо",
		"IIN" => "740417399139",
		"contacts" => [
			"mobile" => "+77022146917",
		],
		"TCs" => [
			[
				"brand" => "HyundaiSantaFe",
				"size" => "235/60/18",
				"gosNomer" => "F0906/02",
			],
			[
				"brand" => "LexusRX300",
				"size" => "235/70/16",
				"gosNomer" => "H 784685",
			],
		],
		/*
			<subscription>
				<nomer>297</nomer>
				<date>22.03.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000836",
		"name" =>"ЖАНАХУТДИНОВ ГАФУР ВАХИТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "560314300072",
		"contacts" => [
			"mobile" => "+77787533143",
		],
	],
	[
		"clientID" =>"000000837",
		"name" =>"АСТАФЬЕВ ПАВЕЛ ГЕННАДЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "770825300103",
	],
	[
		"clientID" =>"000000838",
		"name" =>"Цай Дмитрий  Моисеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820427399021",
		"contacts" => [
			"mobile" => "+77019303636",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "275/65/17",
				"gosNomer" => "645WAA/02",
			],
			[
				"brand" => "Лексус470",
				"size" => "275/65/17",
				
			],
			[
				"brand" => "Toyota",
				"size" => "215/55/17",
				"gosNomer" => "935VHA/02",
			],
		],
	],
	[
		"clientID" =>"000000840",
		"name" =>"Сайбулатова Люция Ержановна",
		"attribute" => "Физ. лицо",
		"IIN" => "821027399065",
		"contacts" => [
			"mobile" => "+77773151569",
		],
		"TCs" => [
			[
				"brand" => "ToyotaYaris",
				"size" => "175/65/14",
				"gosNomer" => "281EOA/02",
			],
		],
	],
	[
		"clientID" =>"000000841",
		"name" =>"ТОКСАНБАЕВ ЖАСЛАН МУРАТКАЛИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "851024301662",
		"contacts" => [
			"mobile" => "+77757108560",
		],
	],
	[
		"clientID" =>"000000842",
		"name" =>"Галкин Максим Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "850410351139",
	],
	[
		"clientID" =>"000000844",
		"name" =>"Башлаева Татьяна",
		"attribute" => "Физ. лицо",
		"IIN" => "771028401019",
	],
	[
		"clientID" =>"000000845",
		"name" =>"Омарбаев Санат",
		"attribute" => "Физ. лицо",
		"IIN" => "780827300403",
	],
	[
		"clientID" =>"000000846",
		"name" =>"ДЖАЛКАЙДАРОВА КАРЛЫГАШ ОМАРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "810422402716",
	],
	[
		"clientID" =>"000000847",
		"name" =>"Ткачев Игорь Михайлович",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000001930",
		"name" =>"Айнабеков Серик Абаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860115300638",
		"contacts" => [
			"mobile" => "+77019591214",
		],
	],
	[
		"clientID" =>"000001700",
		"name" =>"МУХАМЕДЖАНОВ ВАЛИАХМЕТ НУРИАХМЕТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "411207300589",
		"contacts" => [
			"mobile" => "+77017818367",
		],
	],
	[
		"clientID" =>"000001701",
		"name" =>"БЕКОЖАНОВА ЗАРА МЕЙРМАНОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "671124401505",
		"contacts" => [
			"mobile" => "+77772019300",
		],
	],
	[
		"clientID" =>"000001704",
		"name" =>"Трюнин Михаил Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "750506301341",
		"contacts" => [
			"mobile" => "+77013199988",
		],
	],
	[
		"clientID" =>"000001705",
		"name" =>"ЖУМАСЕИТОВ ЕРБОЛ МЕНДГАЛИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "761123300089",
		"contacts" => [
			"mobile" => "+77053933143",
		],
	],
	[
		"clientID" =>"000001707",
		"name" =>"СУГИРБЕКОВ ЕРБОЛ КОЙШЫБАЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "771219302435",
		"contacts" => [
			"mobile" => "+77017980102",
		],
	],
	[
		"clientID" =>"000001710",
		"name" =>"Утельбаев Елдос Бекжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "900103301016",
		"contacts" => [
			"mobile" => "+77077855556",
		],
	],
	[
		"clientID" =>"000001721",
		"name" =>"КУРМАЕВ МАРСЕЛЬ ИЛЬДАРОВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "900130300035",
		"contacts" => [
			"mobile" => "+77777217358",
		],
	],
	[
		"clientID" =>"000001722",
		"name" =>"ЧАГАТАЕВ АЙДЫН ИШЕМБАЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "860620301730",
		"contacts" => [
			"mobile" => "+77022702061",
		],
	],
	[
		"clientID" =>"000001723",
		"name" =>"Нуркабеков Нурбол Нурахметович",
		"attribute" => "Физ. лицо",
		"IIN" => "780612300583",
		"contacts" => [
			"mobile" => "+77078292782",
		],
	],
	[
		"clientID" =>"000001724",
		"name" =>"Белоголовцев Тимур Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "880313301438",
		"contacts" => [
			"mobile" => "+77018619495",
		],
	],
	[
		"clientID" =>"000001725",
		"name" =>"Тудахунов  Артур  Нуридинович",
		"attribute" => "Физ. лицо",
		"IIN" => "800908300013",
		"contacts" => [
			"mobile" =>"+77010181018",
			"mobile" => "+77010181018",
		],
		"TCs" => [
			[
				"brand" => "Мазда",
				"size" => "245/40/19",
				"gosNomer" => "505CVA/02",
			],
			[
				"brand" => "HyundaiSantafe",
				"size" => "235/60/18",
				"gosNomer" => "387 BAA/02",
			],
			[
				"brand" => "Мазда",
				"size" => "225/55/17",
				"gosNomer" => "505CVA02",
			],
		],
	],
	[
		"clientID" =>"000001726",
		"name" =>"АЛМУХАНБЕТОВ НУРПЕЙС ЕСКАЛИЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "890801350986",
		"contacts" => [
			"mobile" => "+77025250700",
		],
	],
	[
		"clientID" =>"000001727",
		"name" =>"Захаров Евгений Олегович",
		"attribute" => "Физ. лицо",
		"IIN" => "900412300513",
		"contacts" => [
			"mobile" => "+77051442689",
		],
	],
	[
		"clientID" =>"000001728",
		"name" =>"ДАЛБАЕВ ЕРУЛАН СЕЙСЕХАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "501103300845",
		"contacts" => [
			"mobile" => "+77013113262",
		],
	],
	[
		"clientID" =>"000001731",
		"name" =>"Кихаев Антон",
		"attribute" => "Юр. лицо",
		"IIN" => "860212300198",
		"contacts" => [
			"mobile" => "870750288000",
		],
	],
	[
		"clientID" =>"000001733",
		"name" =>"ЗАГИНАЙКО АЛЕКСЕЙ АЛЕКСЕЕВИЧ",
		"attribute" => "Юр. лицо",
		"IIN" => "761010303019",
		"contacts" => [
			"mobile" => "8701575120287075751202",
		],
	],
	[
		"clientID" =>"000001734",
		"name" =>"Копжанов Бауржан Копжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "741023300011",
	],
	[
		"clientID" =>"000001735",
		"name" =>"Рахманов Евгений Уралович",
		"attribute" => "Физ. лицо",
		"IIN" => "720822302243",
		"contacts" => [
			"mobile" => "+77055210733",
		],
	],
	[
		"clientID" =>"000001736",
		"name" =>"Ковалевич Виталий Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "700728301538",
		"contacts" => [
			"mobile" => "+77757989453",
		],
	],
	[
		"clientID" =>"000001980",
		"name" =>"МИСЩЕНЧУК АЛЕКСАНДР ВИТАЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "640919350174",
		"contacts" => [
			"mobile" => "+77776240125",
		],
	],
	[
		"clientID" =>"000001981",
		"name" =>"РАШЕВСКИЙ АНДРЕЙ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "910808300101",
		"contacts" => [
			"mobile" => "+77057402527",
		],
	],
	[
		"clientID" =>"000001983",
		"name" =>"Райымов Жандарбек Рысбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "791201303627",
		"contacts" => [
			"mobile" => "+77017116360",
		],
	],
	[
		"clientID" =>"000001985",
		"name" =>"Голяндин Илья Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840623300091",
		"contacts" => [
			"mobile" => "+77015840823",
		],
		"TCs" => [
			[
				"brand" => "Газель",
				"size" => "185/75/16C",
				"gosNomer" => "A36AKP",
			],
		],
	],
	[
		"clientID" =>"000001986",
		"name" =>"КУРБАНБАЕВ КАНАТ БИЗАКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "850216301475",
	],
	[
		"clientID" =>"000001988",
		"name" =>"ГРАЧЕВСКИЙ АНАТОЛИЙ ГРИГОРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "470723300850",
	],
	[
		"clientID" =>"000001991",
		"name" =>"ЧАБДАНОВ КАЗБЕК СЕРИКБОСЫНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "700517302717",
	],
	[
		"clientID" =>"000001993",
		"name" =>"КОЛОДИЙ АЛЕКСЕЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "580117301668",
	],
	[
		"clientID" =>"000001994",
		"name" =>"ДОШОВА АКЕРКЕ ДАУТБАЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "900927402675",
	],
	[
		"clientID" =>"000001465",
		"name" =>"Исмаилов Марат Нуркасымович",
		"attribute" => "Физ. лицо",
		"IIN" => "800417300629",
		"contacts" => [
			"mobile" => "+77778037000",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "275/70/16",
				"gosNomer" => "941BSA/02",
			],
			[
				"brand" => "KIA",
				"size" => "185/65/15",
				"gosNomer" => "292KNA/02",
			],
		],
	],
	[
		"clientID" =>"000001467",
		"name" =>"Динара Жакылбаева",
		"attribute" => "Физ. лицо",
		"IIN" => "750112402156",
		"contacts" => [
			"mobile" => "+77714518047",
		],
	],
	[
		"clientID" =>"000001468",
		"name" =>"Ступин Олег Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "720226301720",
		"contacts" => [
			"mobile" => "+77016284848",
		],
	],
	[
		"clientID" =>"000001469",
		"name" =>"Крупицкий Юрий Иванович",
		"attribute" => "Физ. лицо",
		"IIN" => "791214350025",
		"contacts" => [
			"mobile" => "+77772363736",
		],
	],
	[
		"clientID" =>"000001470",
		"name" =>"Брежнева Татьяна Викторовна",
		"attribute" => "Физ. лицо",
		"IIN" => "790826403410",
		"contacts" => [
			"mobile" => "+77772887783",
		],
	],
	[
		"clientID" =>"000001471",
		"name" =>"ДРОЗДОВ ГРИГОРИЙ ПЕТРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "760216301684",
		"contacts" => [
			"mobile" => "+77051959276",
		],
	],
	[
		"clientID" =>"000001473",
		"name" =>"Кухарский Анатолий Павлович",
		"attribute" => "Физ. лицо",
		"IIN" => "490711300154",
		"contacts" => [
			"mobile" => "+77022101060",
		],
	],
	[
		"clientID" =>"000001476",
		"name" =>"Жайылган Алмас Нуралыулы",
		"attribute" => "Физ. лицо",
		"IIN" => "740707300483",
		"contacts" => [
			"mobile" => "+77017116302",
		],
	],
	[
		"clientID" =>"000001475",
		"name" =>"Аханов Женис Толеубаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "770509303388",
		"contacts" => [
			"mobile" => "+77789511813",
		],
	],
	[
		"clientID" =>"000001479",
		"name" =>"Буряк Константин Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "710928301448",
		"contacts" => [
			"mobile" => "+77013838396",
		],
	],
	[
		"clientID" =>"000001480",
		"name" =>"Омаров Ерсен Елтайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "911105300836",
		"contacts" => [
			"mobile" => "+77755690556",
		],
	],
	[
		"clientID" =>"000001481",
		"name" =>"Шназбаев Еркен Ногаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "770124300599",
		"contacts" => [
			"mobile" => "+77012110267",
		],
	],
	[
		"clientID" =>"000001482",
		"name" =>"Бардов Александр Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "840404300080",
		"contacts" => [
			"mobile" => "+77016767680",
		],
	],
	[
		"clientID" =>"000001483",
		"name" =>"Габдуллин Темирлан Нурланович",
		"attribute" => "Физ. лицо",
		"IIN" => "820107300022",
		"contacts" => [
			"mobile" => "+77051181007",
		],
	],
	[
		"clientID" =>"000001487",
		"name" =>"Барбаков Артем",
		"attribute" => "Физ. лицо",
		"IIN" => "860519300812",
		"contacts" => [
			"mobile" => "+77479642782",
		],
	],
	[
		"clientID" =>"000001489",
		"name" =>"Метнев Антон",
		"attribute" => "Физ. лицо",
		"IIN" => "851126303029",
		"contacts" => [
			"mobile" => "+77024749540",
		],
	],
	[
		"clientID" =>"000001493",
		"name" =>"Сарсембаев Нуртас Ондасинович",
		"attribute" => "Физ. лицо",
		"IIN" => "771028302398",
		"contacts" => [
			"mobile" => "+77714736412",
		],
	],
	[
		"clientID" =>"000001499",
		"name" =>"Горбунов Виктор Леонидович",
		"attribute" => "Физ. лицо",
		"IIN" => "540428300752",
		"contacts" => [
			"mobile" =>"+77272433364",
			"mobile" => "+77272433364",
		],
	],
	[
		"clientID" =>"000001500",
		"name" =>"Юнусов Руслан",
		"attribute" => "Физ. лицо",
		"IIN" => "920817301100",
		"contacts" => [
			"mobile" => "+77473459081",
		],
	],
	[
		"clientID" =>"000001501",
		"name" =>"КЛИМЕНКО ВЛАДИСЛАВ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "711004302425",
		"contacts" => [
			"mobile" => "+77071761736",
		],
	],
	[
		"clientID" =>"000001503",
		"name" =>"Кузнецова Марина Васильевна",
		"attribute" => "Физ. лицо",
		"IIN" => "840225450675",
	],
	[
		"clientID" =>"000001504",
		"name" =>"ОЛЖАЕВ АЙБАР КАЙНАРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "830801300245",
		"contacts" => [
			"mobile" => "+77054552058",
		],
	],
	[
		"clientID" =>"000001505",
		"name" =>"Абашкин Борис Егорович",
		"attribute" => "Физ. лицо",
		"IIN" => "390520300839",
		"contacts" => [
			"mobile" => "+77013055678",
		],
	],
	[
		"clientID" =>"000001506",
		"name" =>"Курманбаева Шынар Жумагазыевна",
		"attribute" => "Физ. лицо",
		"IIN" => "75056401535",
		"contacts" => [
			"mobile" => "+77017797881",
		],
		"TCs" => [
			[
				"brand" => "Subaru Impreza XV",
				"size" => "205/60/16",
				"gosNomer" => "385 DPA/02",
			],
		],
	],
	[
		"clientID" =>"000001507",
		"name" =>"Медведев Иван Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "831003300150",
		"contacts" => [
			"mobile" => "+77017899143",
		],
	],
	[
		"clientID" =>"000001508",
		"name" =>"Уйкасбаева Майгуль Кабылжановна",
		"attribute" => "Физ. лицо",
		"IIN" => "680604400921",
		"contacts" => [
			"mobile" => "+77479077030",
		],
	],
	[
		"clientID" =>"000001510",
		"name" =>"Акперова Зауре Сейткановна",
		"attribute" => "Физ. лицо",
		"IIN" => "850304402715",
		"contacts" => [
			"mobile" => "+77021015999",
		],
	],
	[
		"clientID" =>"000001512",
		"name" =>"НУРДИНГОЖАЕВ ДАРХАН СУЛТАНМУРАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "861015301156",
		"contacts" => [
			"mobile" =>"+77272253928",
			"mobile" => "+77012082000",
		],
	],
	[
		"clientID" =>"000001509",
		"name" =>"Алимгожаев Еркебулан Тимурович",
		"attribute" => "Физ. лицо",
		"IIN" => "861118302138",
		"contacts" => [
			"mobile" => "+77715027572",
		],
	],
	[
		"clientID" =>"000001511",
		"name" =>"Ибраев Талгат Рамазанович",
		"attribute" => "Физ. лицо",
		"IIN" => "680427301961",
	],
	[
		"clientID" =>"000001513",
		"name" =>"ВЛАСОВ ВЛАДИМИР ГЕННАДЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "811118301061",
	],
	[
		"clientID" =>"000001514",
		"name" =>"Фоминых Денис",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019154864",
		],
	],
	[
		"clientID" =>"000001516",
		"name" =>"Есмуканов Талгат Арыстанбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "910227300222",
		"contacts" => [
			"mobile" => "+77079099102",
		],
	],
	[
		"clientID" =>"000001518",
		"name" =>"Абуаллабан Айман Ахмад",
		"attribute" => "Физ. лицо",
		"IIN" => "710501399030",
		"contacts" => [
			"mobile" => "+77013999000",
		],
	],
	[
		"clientID" =>"000001519",
		"name" =>"Вичев Алексей Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "790623302076",
		"contacts" => [
			"mobile" => "+77052207400",
		],
	],
	[
		"clientID" =>"000001522",
		"name" =>"ТЛЕПОВ РАУЛЬ РУСТАМОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "900113300240",
		"contacts" => [
			"mobile" => "+77770222001",
		],
	],
	[
		"clientID" =>"000001521",
		"name" =>"Уалиханов Габит Жумабекович",
		"attribute" => "Физ. лицо",
		"IIN" => "861117301838",
		"contacts" => [
			"mobile" => "+77081067078",
		],
	],
	[
		"clientID" =>"000001523",
		"name" =>"Кыдыралиев Нурсултан Базарбекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "900317301426",
		"contacts" => [
			"mobile" => "+77025629109",
		],
	],
	[
		"clientID" =>"000001524",
		"name" =>"Ен Вячеслав Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "650424302294",
		"contacts" => [
			"mobile" => "+77079989526",
		],
	],
	[
		"clientID" =>"000001526",
		"name" =>"ДОСЫБЕКОВ ЕРТАЙ САРТАЙҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "700615300466",
		"contacts" => [
			"mobile" => "+77017702662",
		],
	],
	[
		"clientID" =>"000001528",
		"name" =>"СУХАНОВ АНАТОЛИЙ АНАТОЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "570505301952",
		"contacts" => [
			"mobile" => "+77012180177",
		],
	],
	[
		"clientID" =>"000001530",
		"name" =>"Пак Константин Ревович",
		"attribute" => "Физ. лицо",
		"IIN" => "800514399070",
		"contacts" => [
			"mobile" => "+77052306000",
		],
	],
	[
		"clientID" =>"000001531",
		"name" =>"Мусаев Кумар Канатович",
		"attribute" => "Физ. лицо",
		"IIN" => "80512300764",
		"contacts" => [
			"mobile" => "+77772568887",
		],
	],
	[
		"clientID" =>"000001532",
		"name" =>"Бишигаева Найля Дарушовна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772101382",
		],
	],
	[
		"clientID" =>"000001533",
		"name" =>"Кужель Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "710123301095",
		"contacts" => [
			"mobile" => "+77770572865",
		],
	],
	[
		"clientID" =>"000001535",
		"name" =>"Нуржанов Жангали Нуржанулы",
		"attribute" => "Физ. лицо",
		"IIN" => "920601301969",
		"contacts" => [
			"mobile" => "+77019513047",
		],
	],
	[
		"clientID" =>"000001538",
		"name" =>"Кабдуллин Айдар Маратович",
		"attribute" => "Физ. лицо",
		"IIN" => "840221350022",
		"contacts" => [
			"mobile" => "+77055555666",
		],
	],
	[
		"clientID" =>"000001539",
		"name" =>"ИСТАЕВА ЖУМАБИКЕ",
		"attribute" => "Физ. лицо",
		"IIN" => "560210403236",
		"contacts" => [
			"mobile" => "+77013442077",
		],
	],
	[
		"clientID" =>"000001540",
		"name" =>"ЮГАЙ СЕРГЕЙ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "690228300705",
		"contacts" => [
			"mobile" => "+77017801551",
		],
	],
	[
		"clientID" =>"000001541",
		"name" =>"Орехов Олег Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "710501302414",
		"contacts" => [
			"mobile" => "+77773654143",
		],
	],
	[
		"clientID" =>"000001542",
		"name" =>"Крат Анатолий Васильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840731302401",
		"contacts" => [
			"E-Mail" => "krat-1984@mail.ru",
			"mobile" =>"+77071904090",
			"mobile" => "+77071904090",
		],
	],
	[
		"clientID" =>"000001543",
		"name" =>"Сачков Павел Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "791014300051",
		"contacts" => [
			"mobile" => "+77017630783",
		],
	],
	[
		"clientID" =>"000001544",
		"name" =>"Тулегенов Мейрам Еркинович",
		"attribute" => "Физ. лицо",
		"IIN" => "621230300855",
		"contacts" => [
			"mobile" => "+77785747337",
		],
	],
	[
		"clientID" =>"000001545",
		"name" =>"Талипбаев Улан Максатович",
		"attribute" => "Физ. лицо",
		"IIN" => "770113302486",
		"contacts" => [
			"mobile" => "+77056125268",
		],
	],
	[
		"clientID" =>"000001546",
		"name" =>"Мадымухамбетов Ринат Мухаметкалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "880209301809",
		"contacts" => [
			"mobile" => "+77018989770",
		],
	],
	[
		"clientID" =>"000001547",
		"name" =>"Зайнолда Азамат Орманбекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "900922301039",
		"contacts" => [
			"mobile" => "+77777493535",
		],
	],
	[
		"clientID" =>"000001548",
		"name" =>"Нуралиева Асылгуль Урынбасаровна",
		"attribute" => "Физ. лицо",
		"IIN" => "810925400024",
		"contacts" => [
			"mobile" => "+77017989815",
		],
	],
	[
		"clientID" =>"000001550",
		"name" =>"Искаков Канат Болатович",
		"attribute" => "Физ. лицо",
		"IIN" => "830329300646",
		"contacts" => [
			"mobile" => "+77075199707",
		],
	],
	[
		"clientID" =>"000001551",
		"name" =>"Адаричев Алексей Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "840821302956",
		"contacts" => [
			"mobile" => "+77714030001",
		],
	],
	[
		"clientID" =>"000001552",
		"name" =>"Нусупов Багдад Сатыбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "650730300856",
		"contacts" => [
			"mobile" => "+77013391034",
		],
	],
	[
		"clientID" =>"000001553",
		"name" =>"Ким Николай Русланович",
		"attribute" => "Физ. лицо",
		"IIN" => "820508399071",
		"contacts" => [
			"mobile" => "+77773692030",
		],
	],
	[
		"clientID" =>"000000692",
		"name" =>"Ким Владимир Олегович",
		"attribute" => "Физ. лицо",
		"IIN" => "830522399053",
		"contacts" => [
			"mobile" => "+77786204208",
		],
		"TCs" => [
			[
				"brand" => "BMW 328",
				"size" => "205/55/15",
				"gosNomer" => "0",
			],
		],
	],
	[
		"clientID" =>"000000693",
		"name" =>"Татаренко Николай Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "750706300350",
		"contacts" => [
			"mobile" => "+77273904490",
		],
	],
	[
		"clientID" =>"000000694",
		"name" =>"Булгакбаева   Зауре",
		"attribute" => "Физ. лицо",
		"IIN" => "790624400081",
		"contacts" => [
			"mobile" => "+77017666667",
		],
		"TCs" => [
			[
				"brand" => "NissanQuashai",
				"size" => "215/65/16",
				"gosNomer" => "A 105 BLO",
			],
		],
		/*
			<subscription>
				<nomer>1072</nomer>
				<date>24.10.2016 0:00:00</date>
				<srok>04.11.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000695",
		"name" =>"Калиева  Рауза  Досжановна",
		"attribute" => "Физ. лицо",
		"IIN" => "810925401012",
		"contacts" => [
			"mobile" => "+77017685076",
		],
		"TCs" => [
			[
				"brand" => "HyundaiLavita",
				"size" => "195/50/15",
				"gosNomer" => "785MRA/02",
			],
			[
				"brand" => "HYUNDAI LAVITA",
				"size" => "195/55/15",
				"gosNomer" => "785 MRA",
			],
		],
	],
	[
		"clientID" =>"000000696",
		"name" =>"Левашкина Наталья Митрофановна",
		"attribute" => "Физ. лицо",
		"IIN" => "511005400548",
		"contacts" => [
			"mobile" => "+77013180879",
		],
		"TCs" => [
			[
				"brand" => "Volkswagen",
				"size" => "235/65/17",
				"gosNomer" => "521TKA/02",
			],
		],
	],
	[
		"clientID" =>"000000697",
		"name" =>"Ткаличь Вячеслав",
		"attribute" => "Физ. лицо",
		"IIN" => "720618350303",
		"contacts" => [
			"mobile" => "+77019912845",
		],
	],
	[
		"clientID" =>"000000698",
		"name" =>"Кабетенов Аскар",
		"attribute" => "Физ. лицо",
		"IIN" => "780923350194",
		"contacts" => [
			"mobile" => "+77017173421",
		],
	],
	[
		"clientID" =>"000000699",
		"name" =>"Молибоженко Наталья",
		"attribute" => "Физ. лицо",
		"IIN" => "780115400518",
		"contacts" => [
			"mobile" => "+77772303010",
		],
		"TCs" => [
			[
				"brand" => "BMWX3",
				"size" => "235/55/17",
				"gosNomer" => "A198DPP",
			],
		],
	],
	[
		"clientID" =>"000000700",
		"name" =>"Буднянская Лия",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000701",
		"name" =>"Суюндыков Руслан",
		"attribute" => "Физ. лицо",
		"IIN" => "691218300053",
		"contacts" => [
			"mobile" => "+77778318989",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi Pajero",
				"size" => "265/70/16",
				"gosNomer" => "A 516 RRN",
			],
		],
	],
	[
		"clientID" =>"000000704",
		"name" =>"Пак Николай Вячеславович",
		"attribute" => "Физ. лицо",
		"IIN" => "790105300211",
		"contacts" => [
			"mobile" =>"+77017101466",
			"mobile" => "+77017101466",
		],
		"TCs" => [
			[
				"brand" => "WolksvagenEOS",
				"size" => "235/45/17",
				"gosNomer" => "348ZUM",
			],
		],
	],
	[
		"clientID" =>"000000707",
		"name" =>"Аширбекова А",
		"attribute" => "Юр. лицо",
		
	],
	[
		"clientID" =>"000000708",
		"name" =>"Каптаиров Кайрат Кизатович",
		"attribute" => "Юр. лицо",
		"IIN" => "660130301884",
		"contacts" => [
			"mobile" => "+77017175902",
		],
	],
	[
		"clientID" =>"000000709",
		"name" =>"Кравченко Николай Викторович",
		"attribute" => "Юр. лицо",
		"IIN" => "851202302458",
	],
	[
		"clientID" =>"000000710",
		"name" =>"Амиров Шарафаддин Ембергенович",
		"attribute" => "Физ. лицо",
		"IIN" => "600613300731",
		"contacts" => [
			"mobile" => "+77017075195",
		],
	],
	[
		"clientID" =>"000000711",
		"name" =>"Камбаркызы Сауле",
		"attribute" => "Физ. лицо",
		"IIN" => "860717401685",
		"contacts" => [
			"mobile" =>"+77058617721",
			"mobile" => "+77058617721",
		],
	],
	[
		"clientID" =>"000000712",
		"name" =>"Даирова Меруерт Меделхановна",
		"attribute" => "Физ. лицо",
		"IIN" => "870830401443",
		"contacts" => [
			"mobile" => "+77073636336",
		],
	],
	[
		"clientID" =>"000000713",
		"name" =>"Нигматулин Рустам Рафаэлович",
		"attribute" => "Физ. лицо",
		"IIN" => "760123303388",
		"contacts" => [
			"mobile" => "+77752286055",
		],
	],
	[
		"clientID" =>"000000714",
		"name" =>"Никитин  Денис Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "780327301136",
		"contacts" => [
			"E-Mail" => "dennik_1978@rambler.ru",
			"mobile" => "+77082218280",
		],
		"TCs" => [
			[
				"brand" => "AcuraMDX",
				"size" => "255/55/18",
				"gosNomer" => "358MSA/02",
			],
		],
	],
	[
		"clientID" =>"000000715",
		"name" =>"Имангазиев Мурат Абилмажинович",
		"attribute" => "Физ. лицо",
		"IIN" => "660824300363",
		"contacts" => [
			"mobile" => "+77026681732",
		],
	],
	[
		"clientID" =>"000000716",
		"name" =>"Айтмагамбетов Ануар Усербаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "870209350677",
		"contacts" => [
			"mobile" => "+77772722238",
		],
	],
	[
		"clientID" =>"000000717",
		"name" =>"Кебекбаев Бакыт Турсунович",
		"attribute" => "Физ. лицо",
		"IIN" => "710830301306",
		"contacts" => [
			"mobile" => "+77771215511",
		],
	],
	[
		"clientID" =>"000000718",
		"name" =>"Макроусова Людмила Петровна",
		"attribute" => "Физ. лицо",
		"IIN" => "481003401567",
		"contacts" => [
			"mobile" => "+77775912558",
		],
	],
	[
		"clientID" =>"000000719",
		"name" =>"Клочкова Екатерина Михайловна",
		"attribute" => "Физ. лицо",
		"IIN" => "800509403025",
		"contacts" => [
			"mobile" => "+77762225774",
		],
		"TCs" => [
			[
				"brand" => "Audi A4",
				"size" => "195/65/15",
				"gosNomer" => "A835COP",
			],
		],
	],
	[
		"clientID" =>"000000720",
		"name" =>"Усманов Сергей Акрамович",
		"attribute" => "Физ. лицо",
		"IIN" => "740609302477",
		"contacts" => [
			"mobile" => "+77772318795",
		],
	],
	[
		"clientID" =>"000000724",
		"name" =>"Абдул Малик Мадина Абдукадыровна",
		"attribute" => "Физ. лицо",
		"IIN" => "771124400558",
		"contacts" => [
			"mobile" => "+77011336446",
		],
	],
	[
		"clientID" =>"000000728",
		"name" =>"Оразбаев Ержан Маратович",
		"attribute" => "Юр. лицо",
		"IIN" => "841121302389",
	],
	[
		"clientID" =>"000000729",
		"name" =>"Кызайбаев Ергазы Тенликович",
		"attribute" => "Физ. лицо",
		"IIN" => "740102303141",
		"contacts" => [
			"mobile" => "+77714902676",
		],
	],
	[
		"clientID" =>"000000730",
		"name" =>"САМИГУЛИН ХАСАН АХМЕДЖАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "630309302512",
		"contacts" => [
			"mobile" => "+77772469932",
		],
	],
	[
		"clientID" =>"000000731",
		"name" =>"Билибаев Асет Курманбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "800906300161",
		"contacts" => [
			"mobile" => "+77017977031",
		],
	],
	[
		"clientID" =>"000000732",
		"name" =>"СМОРОДИННИКОВА ЕЛЕНА ЮРЬЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "770803400189",
		"contacts" => [
			"mobile" => "+77778586132",
		],
		"TCs" => [
			[
				"brand" => "Lexus470",
				"gosNomer" => "345RBA16",
			],
			[
				"brand" => "ToyotaRAV-4",
				"size" => "215/65/16",
				"gosNomer" => "F 978 FHN",
			],
		],
		/*
			<subscription>
				<nomer>0287</nomer>
				<date>22.10.2015 0:00:00</date>
				<srok>22.04.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>1099</nomer>
				<date>04.11.2016 0:00:00</date>
				<srok>04.11.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000733",
		"name" =>"БЫЧКОВ ЭДУАРД ВИТАЛЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "620507300065",
		"contacts" => [
			"mobile" => "+77017135521",
		],
	],
	[
		"clientID" =>"000000734",
		"name" =>"Кельбуганов  Асет",
		"attribute" => "Физ. лицо",
		"IIN" => "840626300494",
		"contacts" => [
			"mobile" => "+77017209667",
		],
		"TCs" => [
			[
				"brand" => "MersedesBenz280",
				"size" => "215/55/16",
				"gosNomer" => "869DNA/02",
			],
			[
				"brand" => "Mersedes-benze280",
				"size" => "215/55/16",
				"gosNomer" => "869 DNA/02",
			],
		],
		/*
			<subscription>
				<nomer>1040</nomer>
				<date>18.10.2016 0:00:00</date>
				<srok>23.06.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000735",
		"name" =>"Оспанов Булат Спабекович",
		"attribute" => "Физ. лицо",
		"IIN" => "820916300177",
		"contacts" => [
			"mobile" => "+77017293862",
		],
	],
	[
		"clientID" =>"000000738",
		"name" =>"БИГУСТИНОВ БОЛАТ КАБДОЛДИНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "580426300233",
		"contacts" => [
			"mobile" => "+77072229669",
		],
	],
	[
		"clientID" =>"000000739",
		"name" =>"БЕРДАЛИЕВ АЙДИН",
		"attribute" => "Физ. лицо",
		"IIN" => "920210302545",
		"contacts" => [
			"mobile" => "+77010880066",
		],
		"TCs" => [
			[
				"brand" => "Mercedes",
				"size" => "225/45/17",
				"gosNomer" => "499FNA02",
			],
		],
	],
	[
		"clientID" =>"000000741",
		"name" =>"Бабакин Виктор",
		"attribute" => "Физ. лицо",
		"IIN" => "551216301049",
		"contacts" => [
			"mobile" => "+77017443049",
		],
	],
	[
		"clientID" =>"000000742",
		"name" =>"Луговой Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012110328",
		],
	],
	[
		"clientID" =>"000000743",
		"name" =>"Тумарбаев Досым Нургудурович",
		"attribute" => "Физ. лицо",
		"IIN" => "830316302364",
	],
	[
		"clientID" =>"000000745",
		"name" =>"Рудько Александр Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "791107301818",
		"contacts" => [
			"mobile" => "+77777471967",
		],
	],
	[
		"clientID" =>"000000747",
		"name" =>"Крюков Илья Геннадьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780810300738",
		"contacts" => [
			"mobile" => "+77776699440",
		],
		"TCs" => [
			[
				"brand" => "Mazda CX-7",
				"size" => "235/60/18",
				"gosNomer" => "A833YAO",
			],
			[
				"brand" => "KIASportage",
				"size" => "225/60/17",
				"gosNomer" => "837LBA/02",
			],
		],
	],
	[
		"clientID" =>"000000749",
		"name" =>"НУСИПХОДЖАЕВ СЕРИК МУРАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "810906300460",
		"contacts" => [
			"mobile" => "+77019850101",
		],
	],
	[
		"clientID" =>"000000750",
		"name" =>"Самаренко Олег Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "650624300265",
		"contacts" => [
			"mobile" => "+77057131647",
		],
	],
	[
		"clientID" =>"000000751",
		"name" =>"Ивасенко Иван Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "830522300040",
		"contacts" => [
			"mobile" => "+77051850214",
		],
	],
	[
		"clientID" =>"000000752",
		"name" =>"Нурахметов Димаш Серикжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "900112300087",
		"contacts" => [
			"mobile" => "+77017225542",
		],
		"TCs" => [
			[
				"brand" => "Mersedes Benz C 220",
				"size" => "195/65/15",
				"gosNomer" => "A 505 ZDN",
			],
		],
	],
	[
		"clientID" =>"000000755",
		"name" =>"Кулумбетова Сауле Курмашева",
		"attribute" => "Юр. лицо",
		"IIN" => "580525403074",
		"contacts" => [
			"mobile" => "+77017073117",
		],
	],
	[
		"clientID" =>"000000757",
		"name" =>"Федотов Владимир Петрович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052272217",
		],
	],
	[
		"clientID" =>"000000758",
		"name" =>"Ахметкалиев Азамат Хамитулы",
		"attribute" => "Физ. лицо",
		"IIN" => "900224300195",
		"contacts" => [
			"mobile" => "+77017774907",
		],
	],
	[
		"clientID" =>"000000759",
		"name" =>"Конакбаев Айбол Абдрашидович",
		"attribute" => "Физ. лицо",
		"IIN" => "760803301000",
		"contacts" => [
			"mobile" => "+77772133057",
		],
	],
	[
		"clientID" =>"000000761",
		"name" =>"Разакберлина Асель Кабиденовна",
		"attribute" => "Физ. лицо",
		"IIN" => "790319401725",
		"contacts" => [
			"mobile" => "+77714006997",
		],
		"TCs" => [
			[
				"brand" => "ToyotaPrado",
				"size" => "265/60/18",
				"gosNomer" => "847SBA/02",
			],
			[
				"brand" => "Toyota Land Cruiser",
				"size" => "265/65/17",
				"gosNomer" => "A141WWO",
			],
		],
		/*
			<subscription>
				<nomer>0982</nomer>
				<date>27.02.2016 0:00:00</date>
				<srok>15.11.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>1141</nomer>
				<date>16.11.2016 0:00:00</date>
				<srok>16.11.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>328</nomer>
				<date>19.04.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000762",
		"name" =>"Зулутбеков Нургали Бауыржанович",
		"attribute" => "Физ. лицо",
		"IIN" => "761104300035",
		"contacts" => [
			"mobile" => "+77073000888",
		],
	],
	[
		"clientID" =>"000000763",
		"name" =>"ТАЛДЫБАЕВ НУРКЕН МУРАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "850530300533",
		"contacts" => [
			"mobile" => "+77077779866",
		],
	],
	[
		"clientID" =>"000000856",
		"name" =>"Мусин Ержан",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000000932",
		"name" =>"Сансызбаев Данияр Кайнарбекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "930117300644",
	],
	[
		"clientID" =>"000000937",
		"name" =>"ТРОЯН АНТОНИНА ВИКТОРОВНА ИП",
		"attribute" => "Физ. лицо",
		"IIN" => "620220450147",
	],
	[
		"clientID" =>"000000939",
		"name" =>"Асельбекова Наталья",
		"attribute" => "Физ. лицо",
		"IIN" => "590511401797",
		"contacts" => [
			"mobile" => "+77013851306",
		],
		"TCs" => [
			[
				"brand" => "ToyotaRAV4",
				"size" => "225/65/17",
				"gosNomer" => "945STA/02",
			],
		],
	],
	[
		"clientID" =>"000000941",
		"name" =>"ХАЙБУЛЛИН ТАХИР РАУФОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "560629300312",
		"contacts" => [
			"mobile" => "+77017550344",
		],
	],
	[
		"clientID" =>"000000943",
		"name" =>"Мерзлов Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "900613300318",
		"contacts" => [
			"mobile" => "+77016267506",
		],
	],
	[
		"clientID" =>"000000945",
		"name" =>"Садыков Жомарт",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"000000946",
		"name" =>"Тен Денис Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "810311301820",
		"contacts" => [
			"mobile" => "+77019590565",
		],
	],
	[
		"clientID" =>"000000948",
		"name" =>"Плугина  Ирина",
		"attribute" => "Физ. лицо",
		"IIN" => "860519400343",
		"contacts" => [
			"mobile" => "+77052077190",
		],
		"TCs" => [
			[
				"brand" => "SubaruImpreza",
				"size" => "195/65/15",
				"gosNomer" => "917TOA/02",
			],
		],
		/*
			<subscription>
				<nomer>1194</nomer>
				<date>26.11.2016 0:00:00</date>
				<srok>26.11.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000949",
		"name" =>"Есболов Еркегали Омаргалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "850929300601",
		"contacts" => [
			"mobile" => "870166321677",
		],
	],
	[
		"clientID" =>"000000971",
		"name" =>"Вечерковская Галина Ивановна",
		"attribute" => "Физ. лицо",
		"IIN" => "611004400523",
		"contacts" => [
			"mobile" => "+77073828252",
		],
	],
	[
		"clientID" =>"000000973",
		"name" =>"Алимбаева Адилия",
		"attribute" => "Юр. лицо",
		"IIN" => "880328301370",
		"contacts" => [
			"mobile" => "+77784888624",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishiairtrek",
				"size" => "215/65/16",
				"gosNomer" => "A 277 AOP",
			],
			[
				"brand" => "ToyotaCamry",
				"size" => "215/55/17",
				"gosNomer" => "057FEA/08",
			],
			[
				"brand" => " Toyota",
				"size" => "215/55/17",
				"gosNomer" => "057AEA08",
			],
		],
		/*
			<subscription>
				<nomer>00001</nomer>
				<date>04.03.2017 0:00:00</date>
				<srok>01.12.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>&lt;&gt;</nomer>
				<date>04.03.2017 0:00:00</date>
				<srok>01.12.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000975",
		"name" =>"Сауле",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072767361",
		],
	],
	[
		"clientID" =>"000000976",
		"name" =>"БАТЫРШИН СЕРГЕЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870109399120",
		"contacts" => [
			"mobile" => "+77778063376",
		],
	],
	[
		"clientID" =>"000000987",
		"name" =>"Красков Станислав Евгеньевич",
		"attribute" => "Юр. лицо",
		"IIN" => "790209301502",
	],
	[
		"clientID" =>"000000988",
		"name" =>"Пушмин Андрей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "600314300111",
		"contacts" => [
			"mobile" => "+77772160953",
		],
	],
	[
		"clientID" =>"000000990",
		"name" =>"Жарылгасов Нурболат Тасбулатович",
		"attribute" => "Юр. лицо",
		"IIN" => "820528301505",
		"contacts" => [
			"mobile" => "+77011600207",
		],
	],
	[
		"clientID" =>"000000994",
		"name" =>"Шуиншибаев Медет Айтбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "850525302532",
		"contacts" => [
			"mobile" => "+77776406421",
		],
	],
	[
		"clientID" =>"000001000",
		"name" =>"Бугубаев Нуртай Муратович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017775133",
		],
	],
	[
		"clientID" =>"000001001",
		"name" =>"Бородулин Кирилл  Кириллович",
		"attribute" => "Физ. лицо",
		"IIN" => "660917301793",
		"contacts" => [
			"mobile" => "+77071267267",
		],
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "185/65/15",
				"gosNomer" => "855UHA",
			],
		],
	],
	[
		"clientID" =>"000001015",
		"name" =>"Шаповалова  Юлия  Сергеевна",
		"attribute" => "Физ. лицо",
		"IIN" => "760121400448",
		"contacts" => [
			"mobile" => "+77772305041",
		],
		"TCs" => [
			[
				"brand" => "NissanJuke",
				"size" => "215/55/17",
				"gosNomer" => "225RXA/02",
			],
		],
		/*
			<subscription>
				<nomer>1036</nomer>
				<date>01.01.0001 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001740",
		"name" =>"ЧЕРКАШИН АНДРЕЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "800318300204",
		"contacts" => [
			"mobile" => "+77015707695",
		],
	],
	[
		"clientID" =>"000001741",
		"name" =>"ПРОВОДИН ЮРИЙ МИХАЙЛОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820624300797",
		"contacts" => [
			"mobile" => "+77771867376",
		],
	],
	[
		"clientID" =>"000001742",
		"name" =>"УРТЕМБАЕВ АДИЛЬБЕК СУЛЕЙМЕНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "470811300025",
		"contacts" => [
			"mobile" => "+77017994806",
		],
	],
	[
		"clientID" =>"000001743",
		"name" =>"ОСИПОВ МИХАИЛ НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "831214350729",
		"contacts" => [
			"mobile" => "+77475016038",
		],
	],
	[
		"clientID" =>"000001745",
		"name" =>"ПАЖЕТНЫХ АНДРЕЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "611211300136",
	],
	[
		"clientID" =>"000001746",
		"name" =>"СТАРОДУБЦЕВ АНДРЕЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "790801300854",
		"contacts" => [
			"mobile" => "+77057112495",
		],
	],
	[
		"clientID" =>"000001747",
		"name" =>"ШЕВЧЕНКО СЕРГЕЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "620203301556",
	],
	[
		"clientID" =>"000001748",
		"name" =>"ДАВЛЕТШИН РИНАТ РАМИЛЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "600827399020",
		"contacts" => [
			"mobile" => "+77022145161",
		],
	],
	[
		"clientID" =>"000001750",
		"name" =>"Муслимов Рашид Фаикович",
		"attribute" => "Физ. лицо",
		"IIN" => "791007300455",
		"contacts" => [
			"mobile" => "+77022160012",
		],
		"TCs" => [
			[
				"brand" => "SubaruForester",
				"size" => "215/60/16",
				"gosNomer" => "A303BNO",
			],
			[
				"brand" => "Lexus    RX 300",
				"size" => "225/70/16",
				"gosNomer" => "075EHA/02",
			],
			[
				"brand" => "LexusRX300",
				"size" => "225/70/16",
				"gosNomer" => "075 EHA/02",
			],
		],
		/*
			<subscription>
				<nomer>0417</nomer>
				<date>27.11.2015 0:00:00</date>
				<srok>27.03.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>0863</nomer>
				<date>18.11.2016 0:00:00</date>
				<srok>24.03.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>417</nomer>
				<date>08.03.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>0646</nomer>
				<date>25.03.2017 0:00:00</date>
				<srok>25.03.2018 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001752",
		"name" =>"Капаров Канат Жетписович",
		"attribute" => "Юр. лицо",
		"IIN" => "720323350373",
		"contacts" => [
			"mobile" => "+77011748402",
		],
	],
	[
		"clientID" =>"000001753",
		"name" =>"РЯЗАНОВ ВАЛЕРИЙ НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "570816350109",
		"contacts" => [
			"mobile" => "+77475116412",
		],
	],
	[
		"clientID" =>"000001754",
		"name" =>"КОСЖАНОВ МУХИТ БАЙСАФАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "760115301368",
		"contacts" => [
			"mobile" => "+77015261595",
		],
	],
	[
		"clientID" =>"000001755",
		"name" =>"ДАРЕНСКАЯ СВЕТЛАНА ВЛАДИМИРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "740726402975",
		"contacts" => [
			"mobile" => "+77775869985",
		],
	],
	[
		"clientID" =>"000001756",
		"name" =>"Зимин Олег Витальевич",
		"attribute" => "Юр. лицо",
		"IIN" => "670706300055",
		"contacts" => [
			"mobile" => "+77772203694",
		],
	],
	[
		"clientID" =>"000001757",
		"name" =>"Ветренцева Елена Робертовна",
		"attribute" => "Физ. лицо",
		"IIN" => "741230400380",
		"contacts" => [
			"mobile" => "+77715017733",
		],
	],
	[
		"clientID" =>"000001758",
		"name" =>"БАЙКАДАМОВ АЗИЛЬХАН ТАЛГАТБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "620726300998",
		"contacts" => [
			"mobile" => "+77018861949",
		],
	],
	[
		"clientID" =>"000001760",
		"name" =>"ИЛЬИЧЁВ ДМИТРИЙ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "710207301676",
	],
	[
		"clientID" =>"000001761",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057941555",
		],
	],
	[
		"clientID" =>"000001762",
		"name" =>"АКУЛЕНКО ИРИНА ВЛАДИМИРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "771204400463",
		"contacts" => [
			"mobile" => "+77772366613",
		],
	],
	[
		"clientID" =>"000001763",
		"name" =>"КУАНЫШБЕКОВ ИЛЬЯС КАСЫМОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "871214300244",
		"contacts" => [
			"mobile" => "871214300244",
		],
	],
	[
		"clientID" =>"000001764",
		"name" =>"САКИРОВА БАЗАРГУЛЬ МУХАНБЕДЖАНОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "710110402980",
		"contacts" => [
			"mobile" => "+77773916085",
		],
	],
	[
		"clientID" =>"000001765",
		"name" =>"Сыздыкова Гаухар Ережепбаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "840607401257",
		"contacts" => [
			"mobile" => "+77017583272",
		],
		"TCs" => [
			[
				"brand" => "KIARIO",
				"size" => "195/55/16",
				"gosNomer" => "994ODA/02",
			],
		],
		/*
			<subscription>
				<nomer>1197</nomer>
				<date>02.08.2016 0:00:00</date>
				<srok>02.08.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000001766",
		"name" =>"ФОМИН ИГОРЬ ФЕДОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "670216301546",
		"contacts" => [
			"mobile" => "+77012571440",
		],
	],
	[
		"clientID" =>"000001767",
		"name" =>"РЫСБАЕВ АРДАК ОРЫНБАСАРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "721226301456",
		"contacts" => [
			"mobile" => "+77017367789",
		],
	],
	[
		"clientID" =>"000001779",
		"name" =>"Миндубаев Рафаиль Набиуллович",
		"attribute" => "Физ. лицо",
		"IIN" => "480629300107",
	],
	[
		"clientID" =>"000001780",
		"name" =>"Данильченко Сергей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "950103300790",
		"contacts" => [
			"mobile" => "+77078109335",
		],
	],
	[
		"clientID" =>"000001783",
		"name" =>"МАКАРОВ АЛЕКСАНДР СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "891007300094",
		"contacts" => [
			"mobile" => "+77075313064",
		],
	],
	[
		"clientID" =>"000001784",
		"name" =>"НИЯЗОВ РУСТАМ ШЕРМУХАМЕДОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "770919301856",
		"contacts" => [
			"mobile" => "+77012038490",
		],
	],
	[
		"clientID" =>"000001785",
		"name" =>"РОМАНОВА ЕВГЕНИЯ ВЛАДИМИРОВНА",
		"attribute" => "Юр. лицо",
		"IIN" => "861004400930",
		"contacts" => [
			"mobile" =>"+77079901099",
			"mobile" => "+77079901099",
		],
	],
	[
		"clientID" =>"000001786",
		"name" =>"КУРДАНОВ СЕРГЕЙ ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "771001301779",
		"contacts" => [
			"mobile" => "+77072605960",
		],
	],
	[
		"clientID" =>"000001787",
		"name" =>"Кусбеков Нуржан Келисович",
		"attribute" => "Физ. лицо",
		"IIN" => "840307300975",
		"contacts" => [
			"mobile" => "+77772733321",
		],
	],
	[
		"clientID" =>"000001788",
		"name" =>"ИГНАТЧЕНКО ЮРИЙ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870109300974",
		"contacts" => [
			"mobile" => "+77714650463",
		],
	],
	[
		"clientID" =>"000001789",
		"name" =>"Кищук Петр Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "821024300480",
		"contacts" => [
			"mobile" => "+77013353344",
		],
	],
	[
		"clientID" =>"000001790",
		"name" =>"АЛТУХОВА НАТАЛЬЯ АЛЕКСАНДРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "800526400668",
		"contacts" => [
			"mobile" => "+77774166354",
		],
	],
	[
		"clientID" =>"000001791",
		"name" =>"КАРАМУРЗИЕВ БЕРКИН КУЛКАШЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "680406300245",
		"contacts" => [
			"mobile" => "+77015220242",
		],
	],
	[
		"clientID" =>"000001792",
		"name" =>"Пряников Валерий Валентинович",
		"attribute" => "Физ. лицо",
		"IIN" => "570209300013",
		"contacts" => [
			"mobile" => "+77012210230",
		],
	],
	[
		"clientID" =>"000001793",
		"name" =>"ЧЕРНЯВСКИЙ ЕВГЕНИЙ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860709301127",
		"contacts" => [
			"mobile" => "+77713891501",
		],
	],
	[
		"clientID" =>"000001794",
		"name" =>"КУЗЬМЕНКО АЛЕКСАНДР СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "750525300179",
		"contacts" => [
			"mobile" => "+77011119892",
		],
	],
	[
		"clientID" =>"000001795",
		"name" =>"ЕРГЕШЕВ АКИМ РУСЛАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "970901300138",
		"contacts" => [
			"mobile" => "+77003943121",
		],
	],
	[
		"clientID" =>"000001796",
		"name" =>"ОБОЛОНКОВА КСЕНИЯ ВЛАДИМИРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "851018400221",
		"contacts" => [
			"mobile" => "+77777919727",
		],
	],
	[
		"clientID" =>"000000889",
		"name" =>"Серикбаев Меркилик Курашевич",
		"attribute" => "Физ. лицо",
		"IIN" => "580111301348",
	],
	[
		"clientID" =>"000000890",
		"name" =>"МОРОЗОВ АНДРЕЙ ВАЛЕРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "871001300503",
		"contacts" => [
			"mobile" => "+77773692767",
		],
	],
	[
		"clientID" =>"000000895",
		"name" =>"Мягчилов Максим Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860906301913",
	],
	[
		"clientID" =>"000000558",
		"name" =>"Мулдашев Арман",
		"attribute" => "Физ. лицо",
		"IIN" => "750629301300",
	],
	[
		"clientID" =>"000000560",
		"name" =>"Старцева  Яна  Аркадьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "790826401206",
		"contacts" => [
			"mobile" => "+77017271110",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "215/55/17",
				"gosNomer" => "935MPA/02",
			],
		],
	],
	[
		"clientID" =>"000000561",
		"name" =>"Ким Олег",
		"attribute" => "Физ. лицо",
		"IIN" => "630502300901",
		"contacts" => [
			"mobile" => "+77017104565",
		],
		"TCs" => [
			[
				"brand" => "Porsche Cayenne",
				"size" => "275/45/20",
				"gosNomer" => "699 OYA/02",
			],
		],
	],
	[
		"clientID" =>"000000562",
		"name" =>"Мендибаева Эльмира Узбековна",
		"attribute" => "Физ. лицо",
		"IIN" => "781027403507",
		"contacts" => [
			"mobile" => "+77012210821",
		],
	],
	[
		"clientID" =>"000000564",
		"name" =>"Айгараев Диас Мухитович",
		"attribute" => "Физ. лицо",
		"IIN" => "901027300127",
		"contacts" => [
			"mobile" => "+77019338316",
		],
	],
	[
		"clientID" =>"000000565",
		"name" =>"Ким Ольга Сенденовна",
		"attribute" => "Физ. лицо",
		"IIN" => "740801400222",
		"contacts" => [
			"mobile" => "+77057752279",
		],
		"TCs" => [
			[
				"brand" => "KiaCerato",
				"size" => "205/55/16",
				"gosNomer" => "626DKA/02",
			],
		],
		/*
			<subscription>
				<nomer>1097</nomer>
				<date>01.11.2016 0:00:00</date>
				<srok>13.07.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000566",
		"name" =>"Серикказы Ринат",
		"attribute" => "Физ. лицо",
		"IIN" => "811106300711",
		"contacts" => [
			"mobile" => "+77017531595",
		],
	],
	[
		"clientID" =>"000000574",
		"name" =>"Бегимбетов Максут Белдикпаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "730523301509",
		"contacts" => [
			"mobile" => "+77019706446",
		],
	],
	[
		"clientID" =>"000000575",
		"name" =>"Мамай Анастасия Сергеевна",
		"attribute" => "Физ. лицо",
		"IIN" => "930224400078",
		"contacts" => [
			"mobile" => "+77777042060",
		],
		"TCs" => [
			[
				"brand" => "NissanX-Trail",
				"size" => "225/60/18",
				"gosNomer" => "707RMS/02",
			],
		],
		/*
			<subscription>
				<nomer>0604</nomer>
				<date>20.03.2016 0:00:00</date>
				<srok>31.10.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>1110</nomer>
				<date>12.01.2017 0:00:00</date>
				<srok>01.11.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000577",
		"name" =>"Кубеев Серик Камитович",
		"attribute" => "Физ. лицо",
		"IIN" => "720403301514",
		"contacts" => [
			"mobile" => "+77072404072",
		],
	],
	[
		"clientID" =>"000000578",
		"name" =>"ЕнинДмитрий Васильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "750413300942",
		"contacts" => [
			"mobile" => "+77775883330",
		],
	],
	[
		"clientID" =>"000000579",
		"name" =>"Кенжетаев  Кайрат",
		"attribute" => "Физ. лицо",
		"IIN" => "520415300301",
		"contacts" => [
			"E-Mail" => "kkairat52@mail.ru",
			"mobile" => "+77017120289",
		],
		"TCs" => [
			[
				"brand" => "HyundaiAvante",
				"size" => "205/60/16",
				"gosNomer" => "364 CTA/02",
			],
		],
		/*
			<subscription>
				<nomer>0913</nomer>
				<date>19.04.2016 0:00:00</date>
				<srok>19.04.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>418</nomer>
				<date>01.03.2017 0:00:00</date>
				<srok>19.04.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000580",
		"name" =>"Зинович   Татьяна",
		"attribute" => "Физ. лицо",
		"IIN" => "811117400718",
		"contacts" => [
			"mobile" => "+77017634843",
		],
		"TCs" => [
			[
				"brand" => "SuzukiGrandVitara",
				"size" => "225/65/17",
				"gosNomer" => "643 CRA/02",
			],
		],
		/*
			<subscription>
				<nomer>0544</nomer>
				<date>12.12.2015 0:00:00</date>
				<srok>12.12.2015 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>0910</nomer>
				<date>24.11.2016 0:00:00</date>
				<srok>20.07.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000581",
		"name" =>"Куланбаев Бахтияр Даниярович",
		"attribute" => "Физ. лицо",
		"IIN" => "810617300559",
		"contacts" => [
			"mobile" => "+77772816252",
		],
		"TCs" => [
			[
				"brand" => "ToyotaAvensis",
				"size" => "195/60/15",
				"gosNomer" => "F 553 FZN",
			],
		],
		/*
			<subscription>
				<nomer>0795</nomer>
				<date>11.02.2017 0:00:00</date>
				<srok>11.02.2018 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000582",
		"name" =>"Исмаилов Санжар Булатович",
		"attribute" => "Физ. лицо",
		"IIN" => "731207300025",
		"contacts" => [
			"mobile" => "+77772740887",
		],
	],
	[
		"clientID" =>"000000583",
		"name" =>"Илющенко Алексей",
		"attribute" => "Физ. лицо",
		"IIN" => "850618300600",
		"contacts" => [
			"mobile" => "+77051571022",
		],
	],
	[
		"clientID" =>"000000585",
		"name" =>"Тулеу Карина",
		"attribute" => "Физ. лицо",
		"IIN" => "741018301637",
		"contacts" => [
			"mobile" => "+77017772461",
		],
	],
	[
		"clientID" =>"000000586",
		"name" =>"Жумахметова Лиза",
		"attribute" => "Физ. лицо",
		"IIN" => "750603401404",
		"contacts" => [
			"mobile" => "+77017577480",
		],
		"TCs" => [
			[
				"brand" => "ToyotaFortuner",
				"size" => "265/65/17",
				"gosNomer" => "583WWA/02",
			],
			[
				"brand" => "Suzuki  Grand  Vitara",
				"size" => "225/70/16",
				"gosNomer" => "075NSA/02",
			],
			[
				"brand" => "ToyotaFortuner",
				"size" => "265/65/17",
				"gosNomer" => "583 WWA/02",
			],
			[
				"brand" => "Suzuki Grand Vitara",
				"size" => "225/70/16",
				"gosNomer" => "075 NSA/02",
			],
		],
		/*
			<subscription>
				<nomer>1094</nomer>
				<date>31.10.2016 0:00:00</date>
				<srok>31.10.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>515</nomer>
				<date>01.11.2015 0:00:00</date>
				<srok>01.05.2016 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000587",
		"name" =>"Горохов  Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "880602300450",
		"contacts" => [
			"mobile" => "+77075500112",
		],
		"TCs" => [
			[
				"brand" => "SuzukiGrandVitara",
				"size" => "225/60/18",
				"gosNomer" => "762 FZA/02",
			],
		],
	],
	[
		"clientID" =>"000000588",
		"name" =>"Чалбасов Нургали Бейсетайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "850324302450",
		"contacts" => [
			"mobile" => "+77789996785",
		],
	],
	[
		"clientID" =>"000000589",
		"name" =>"Есбулатов  Керимкул  Кайратович",
		"attribute" => "Физ. лицо",
		"IIN" => "820410300051",
		"contacts" => [
			"mobile" => "+77015012007",
		],
		"TCs" => [
			[
				"brand" => "BMW760Li",
				"size" => "245/50/18",
				"gosNomer" => "A010 MVO",
			],
		],
		/*
			<subscription>
				<nomer>1022</nomer>
				<date>11.10.2016 0:00:00</date>
				<srok>11.10.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000590",
		"name" =>"Кадирова  Анель  Кайратовна",
		"attribute" => "Физ. лицо",
		"IIN" => "910918401283",
		"contacts" => [
			"mobile" => "+77785285580",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCorolla",
				"size" => "195/60/15",
				"gosNomer" => "A215ZZO",
			],
		],
		/*
			<subscription>
				<nomer>0981</nomer>
				<date>23.03.2016 0:00:00</date>
				<srok>15.11.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>253</nomer>
				<date>04.04.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000592",
		"name" =>"Калмурзаева Вероника Минуаровна",
		"attribute" => "Физ. лицо",
		"IIN" => "771215400939",
		"contacts" => [
			"mobile" => "+77772359172",
		],
		"TCs" => [
			[
				"brand" => "TOYOTAHighlander",
				"size" => "245/55/19",
				"gosNomer" => "A030ECP",
			],
		],
		/*
			<subscription>
				<nomer>0823</nomer>
				<date>14.11.2016 0:00:00</date>
				<srok>01.03.2017 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>0651</nomer>
				<date>01.03.2017 0:00:00</date>
				<srok>01.03.2018 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000593",
		"name" =>"Дуйсебаева Зауре Бакитжановна",
		"attribute" => "Физ. лицо",
		"IIN" => "820419400451",
		"contacts" => [
			"mobile" => "+77018751300",
		],
	],
	[
		"clientID" =>"000000594",
		"name" =>"Останин Геннадий Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820603300217",
		"contacts" => [
			"mobile" => "+77011012008",
		],
	],
	[
		"clientID" =>"000000595",
		"name" =>"Блюмкин Тимофей Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "821027399065",
	],
	[
		"clientID" =>"000000596",
		"name" =>"Алибеков Марат Рымбекович",
		"attribute" => "Юр. лицо",
		"IIN" => "770417300145",
		"contacts" => [
			"mobile" => "+77789532760",
		],
		"TCs" => [
			[
				
				"size" => "225/55/17",
				
			],
			[
				"brand" => "Toyota Camry",
				"size" => "225/55/16",
				"gosNomer" => "548DMA/02",
			],
		],
		/*
			<subscription>
				<nomer>257</nomer>
				<date>10.11.2014 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000598",
		"name" =>"Малтугелов Арман Абдибекович",
		"attribute" => "Физ. лицо",
		"IIN" => "780706300148",
		"contacts" => [
			"mobile" => "+77772484653",
		],
		"TCs" => [
			[
				"brand" => "MercedesBensML350",
				"size" => "265/65/17",
				"gosNomer" => "B121EOO",
			],
			[
				"brand" => "MersedesBenzML350",
				"size" => "275/55/17",
				"gosNomer" => "B 121 EOO",
			],
			[
				
				"size" => "245/70/16",
				
			],
		],
	],
	[
		"clientID" =>"000000599",
		"name" =>"Веляева Светлана Петровна",
		"attribute" => "Физ. лицо",
		"IIN" => "710430401308",
		"contacts" => [
			"mobile" => "+77014233331",
		],
	],
	[
		"clientID" =>"000000601",
		"name" =>"Баданова Жулдызай Кушимбековна",
		"attribute" => "Физ. лицо",
		"IIN" => "660207401839",
		"contacts" => [
			"mobile" => "+77053304467",
		],
		"TCs" => [
			[
				"brand" => "SubaruLegasy",
				"size" => "195/65/15",
				"gosNomer" => "653 BZA/02",
			],
		],
	],
	[
		"clientID" =>"000000602",
		"name" =>"Будиянский  Соломон Леонидович",
		"attribute" => "Физ. лицо",
		"IIN" => "501228300384",
		"contacts" => [
			"mobile" =>"+77077127640",
		],
		"TCs" => [
			[
				"brand" => "Peugeot  301",
				"size" => "185/65/15",
				"gosNomer" => "538HYA/02",
			],
			[
				"brand" => "Nissan  Juke",
				"size" => "215/55/17",
				"gosNomer" => "097PFA/02",
			],
		],
		/*
			<subscription>
				<nomer>0495</nomer>
				<date>05.11.2015 0:00:00</date>
				<srok>05.06.2016 0:00:00</srok>
			</subscription>
			<subscription>
				<nomer>0918</nomer>
				<date>18.10.2016 0:00:00</date>
				<srok>04.11.2016 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000000603",
		"name" =>"Мельникова Валерия Валерьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "850725400252",
		"contacts" => [
			"mobile" => "+77771181313",
		],
	],
	[
		"clientID" =>"000000604",
		"name" =>"Байхонов Абилхайыр Мухтарович",
		"attribute" => "Физ. лицо",
		"IIN" => "790101309967",
	],
	[
		"clientID" =>"000000605",
		"name" =>"Чужембаев Данияр Кажигалиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "831228300293",
		"contacts" => [
			"mobile" => "+77072086273",
		],
	],
	[
		"clientID" =>"000000606",
		"name" =>"Слюсарев Сергей",
		"attribute" => "Физ. лицо",
		"IIN" => "650508000097",
		"contacts" => [
			"mobile" => "+77017827266",
		],
	],
	[
		"clientID" =>"000000609",
		"name" =>"Гритчин Владимир",
		"attribute" => "Физ. лицо",
		"IIN" => "790906300177",
		"contacts" => [
			"mobile" => "+77015341542",
		],
	],
	[
		"clientID" =>"000000610",
		"name" =>"Хорошаев Евгений Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "821112350693",
		"contacts" => [
			"mobile" => "+77078416190",
		],
	],
	[
		"clientID" =>"000000611",
		"name" =>"Тусупбеков Габит Тохтарбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "710223301603",
		"contacts" => [
			"mobile" => "+77781895880",
		],
	],
	[
		"clientID" =>"000002100",
		"name" =>"Хон Арсен Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "791014300606",
		"contacts" => [
			"mobile" => "+77055275000",
		],
	],
	[
		"clientID" =>"000002101",
		"name" =>"Торбенко Татьяна Евгеньевна",
		"attribute" => "Физ. лицо",
		"IIN" => "731012400402",
		"contacts" => [
			"mobile" => "+77773588007",
		],
	],
	[
		"clientID" =>"000002103",
		"name" =>"Ильченко Евгений Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "701102300251",
		"contacts" => [
			"mobile" => "+77023560962",
		],
	],
	[
		"clientID" =>"000002104",
		"name" =>"Виницкий Сергей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "721101301159",
		"contacts" => [
			"mobile" => "+77772715060",
		],
	],
	[
		"clientID" =>"000002105",
		"name" =>"Абдусалиев Мамуржан Мутанович",
		"attribute" => "Физ. лицо",
		"IIN" => "640807399019",
		"contacts" => [
			"mobile" => "+77052303394",
		],
	],
	[
		"clientID" =>"000002106",
		"name" =>"Власов Сергей Леонидович",
		"attribute" => "Физ. лицо",
		"IIN" => "891108300152",
		"contacts" => [
			"mobile" => "+77072141611",
		],
	],
	[
		"clientID" =>"000002107",
		"name" =>"Кузембаев  Тимур  Махмудович",
		"attribute" => "Физ. лицо",
		"IIN" => "830521300095",
		"contacts" => [
			"mobile" => "+77772682226",
		],
		"TCs" => [
			[
				"brand" => "KIARIO",
				"size" => "185/65/15",
				"gosNomer" => "370SOA/02",
			],
			[
				"brand" => "KIA  RIO",
				"size" => "185/65/15",
				"gosNomer" => "A748RBP",
			],
		],
	],
	[
		"clientID" =>"000002109",
		"name" =>"Кочнев Иван Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "890606302311",
		"contacts" => [
			"mobile" => "+77057101095",
		],
	],
	[
		"clientID" =>"000002113",
		"name" =>"Туленбаев Жанат Сауранбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "540930300276",
		"contacts" => [
			"mobile" => "+77017249655",
		],
	],
	[
		"clientID" =>"000002138",
		"name" =>"Петрова Олеся Викторовна",
		"attribute" => "Физ. лицо",
		"IIN" => "880321400915",
		"contacts" => [
			"mobile" => "+77770651413",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "205/60/16",
				"gosNomer" => "F845DTN",
			],
		],
		/*
			<subscription>
				<nomer>0615</nomer>
				<date>10.03.2016 0:00:00</date>
				<srok>01.11.2016 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000002139",
		"name" =>"Харитонов Александр Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840204300608",
	],
	[
		"clientID" =>"000002141",
		"name" =>"Токарев Константин Вячеславович",
		"attribute" => "Юр. лицо",
		"IIN" => "911220301245",
		"contacts" => [
			"mobile" => "+77079661368",
		],
	],
	[
		"clientID" =>"000002142",
		"name" =>"Яриков Дмитрий Алексеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "940411300614",
		"contacts" => [
			"mobile" => "+77072626813",
		],
	],
	[
		"clientID" =>"000002143",
		"name" =>"ЗАЙЦЕВ МАКСИМ ЮРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "750704399100",
		"contacts" => [
			"mobile" => "+77017339253",
		],
	],
	[
		"clientID" =>"000002150",
		"name" =>"АБДЕНОВ АРМАН АСАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "901101350333",
		"contacts" => [
			"mobile" => "+77782026002",
		],
	],
	[
		"clientID" =>"000002151",
		"name" =>"Щекотихин   Евгений   Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "841129300158",
		"contacts" => [
			"mobile" => "+77017184886",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiPajero",
				"size" => "265/70/16",
				"gosNomer" => "B676ABO",
			],
		],
		/*
			<subscription>
				<nomer>0884</nomer>
				<date>04.12.2016 0:00:00</date>
				<srok>11.04.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000002152",
		"name" =>"ВАКПАЕВ СЕРГЕЙ НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "630405301794",
		"contacts" => [
			"mobile" => "+77015431003",
		],
	],
	[
		"clientID" =>"000002155",
		"name" =>"Ким Игорь",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77021916150",
		],
	],
	[
		"clientID" =>"000002153",
		"name" =>"Жолдасбаева Айзада Есенгуловна",
		"attribute" => "Физ. лицо",
		"IIN" => "790105400763",
		"contacts" => [
			"mobile" => "+77015994434",
		],
	],
	[
		"clientID" =>"000002154",
		"name" =>"ВОРОНИН ИВАН РОМАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "780820300900",
		"contacts" => [
			"mobile" => "+77773636836",
		],
	],
	[
		"clientID" =>"000002156",
		"name" =>"Усиков   Александр Дмитриевич",
		"attribute" => "Физ. лицо",
		"IIN" => "580319300251",
		"contacts" => [
			"mobile" =>"+77778234088",
			"mobile" => "+77778234088",
		],
		"TCs" => [
			[
				"brand" => "непонятно",
				"size" => "215/60/16",
				"gosNomer" => "350YRA/02",
			],
		],
	],
	[
		"clientID" =>"000002157",
		"name" =>"ГАЙНУТДИНОВА ЮЛИЯ СТАНИСЛАВОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "850128400019",
		"contacts" => [
			"mobile" => "+77773713770",
		],
	],
	[
		"clientID" =>"000002158",
		"name" =>"Дунаев Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017173301",
			"mobile" => "+77272216036",
		],
	],
	[
		"clientID" =>"000002159",
		"name" =>"Калданов Эрик Дастанулы",
		"attribute" => "Физ. лицо",
		"IIN" => "820222301492",
		"contacts" => [
			"mobile" => "+77005510651",
		],
	],
	[
		"clientID" =>"000002160",
		"name" =>"Булекбаев Акпар Бектурганулы",
		"attribute" => "Физ. лицо",
		"IIN" => "700101305353",
		"contacts" => [
			"mobile" => "+77017363636",
		],
	],
	[
		"clientID" =>"000002161",
		"name" =>"Земцов Илья Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "871210300510",
		"contacts" => [
			"mobile" => "+77771901305",
		],
	],
	[
		"clientID" =>"000002162",
		"name" =>"Райхель Сергей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "860918301285",
		"contacts" => [
			"mobile" => "+77772008823",
		],
	],
	[
		"clientID" =>"000002163",
		"name" =>"ГАПУРОВ БЕРИК САТЫБАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "701018301520",
		"contacts" => [
			"mobile" => "+77057161570",
		],
	],
	[
		"clientID" =>"000002164",
		"name" =>"Ахаев Жарылкасын Жаканович",
		"attribute" => "Физ. лицо",
		"IIN" => "840406302583",
		"contacts" => [
			"mobile" => "+77052299779",
		],
	],
	[
		"clientID" =>"000002165",
		"name" =>"Байбатыров Асхат Інкәрбекұлы",
		"attribute" => "Физ. лицо",
		"IIN" => "870503301984",
		"contacts" => [
			"mobile" => "+77028233333",
		],
	],
	[
		"clientID" =>"000002166",
		"name" =>"Аблимитов Радулин Абдуллинович",
		"attribute" => "Физ. лицо",
		"IIN" => "921208300917",
		"contacts" => [
			"mobile" => "+77076450908",
		],
	],
	[
		"clientID" =>"000002168",
		"name" =>"Акимов Антон Леонидович",
		"attribute" => "Физ. лицо",
		"IIN" => "830226300061",
		"contacts" => [
			"mobile" => "+77078373003",
		],
	],
	[
		"clientID" =>"000002169",
		"name" =>"КАРАТАЕВ МИХАИЛ СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860830300249",
		"contacts" => [
			"mobile" => "+77072011777",
		],
		"TCs" => [
			[
				"brand" => "LEXUS RX300",
				"size" => "215/70/16",
				"gosNomer" => "KZ727CNA/0",
			],
			[
				"brand" => "LEXUS RX300",
				"size" => "235/60/18",
				"gosNomer" => "KZ727CNA/0",
			],
		],
	],
	[
		"clientID" =>"000002172",
		"name" =>"Жаныкулов Ильяс Еркимбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "790921300703",
	],
	[
		"clientID" =>"000002173",
		"name" =>"БУРАК ЕЛЕНА АЛЕКСАНДРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "840312401888",
		"contacts" => [
			"mobile" => "+77081068506",
		],
	],
	[
		"clientID" =>"000002174",
		"name" =>"Суханов Евгений Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "880713303518",
		"contacts" => [
			"mobile" => "+77777267766",
		],
	],
	[
		"clientID" =>"000002177",
		"name" =>"Кирюшков Сергей Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "821225300867",
		"contacts" => [
			"mobile" => "+77077357432",
		],
	],
	[
		"clientID" =>"000002179",
		"name" =>"Молдаханов Тилеукабыл Ергенбайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "940321301465",
		"contacts" => [
			"mobile" => "+77022648484",
		],
	],
	[
		"clientID" =>"000002181",
		"name" =>"Бородулина Елена Кирилловна",
		"attribute" => "Физ. лицо",
		"IIN" => "890613401289",
		"contacts" => [
			"mobile" => "+77051267267",
		],
	],
	[
		"clientID" =>"000002182",
		"name" =>"Крюкова Ирина Евгеньевна",
		"attribute" => "Физ. лицо",
		"IIN" => "730115403325",
	],
	[
		"clientID" =>"000002185",
		"name" =>"Пак Валерий Васильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "590611301803",
		"contacts" => [
			"mobile" => "+77017143282",
		],
		"TCs" => [
			[
				"brand" => "NissanTerrano",
				"size" => "265/70/15",
				"gosNomer" => "A017OMN",
			],
		],
	],
	[
		"clientID" =>"000002186",
		"name" =>"Амиров Усен Бексултанович",
		"attribute" => "Физ. лицо",
		"IIN" => "671018300247",
		"contacts" => [
			"mobile" => "+77714434660",
		],
	],
	[
		"clientID" =>"000002187",
		"name" =>"Гильмудинов Рустам Мнирович",
		"attribute" => "Физ. лицо",
		"IIN" => "590303300236",
		"contacts" => [
			"mobile" => "+77017137359",
		],
	],
	[
		"clientID" =>"000002188",
		"name" =>"Есимов Мухтар Оразалиевич",
		"attribute" => "Юр. лицо",
		"IIN" => "790505301793",
		"contacts" => [
			"mobile" => "+77773840708",
		],
	],
	[
		"clientID" =>"000002189",
		"name" =>"Айтмуханов Алишер",
		"attribute" => "Физ. лицо",
		"IIN" => "870309300053",
	],
	[
		"clientID" =>"000002190",
		"name" =>"Артамонов Алексей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "800617300616",
		"contacts" => [
			"mobile" => "+77017549674",
		],
	],
	[
		"clientID" =>"000002191",
		"name" =>"Ускембаев Шынгыс Муратханович",
		"attribute" => "Физ. лицо",
		"IIN" => "881210301897",
		"contacts" => [
			"mobile" => "+77073143934",
		],
	],
	[
		"clientID" =>"000002193",
		"name" =>"Тузенбаев Ринат Токтасынович",
		"attribute" => "Физ. лицо",
		"IIN" => "781022300102",
		"contacts" => [
			"mobile" => "+77077411978",
		],
	],
	[
		"clientID" =>"000002192",
		"name" =>"Еркебулан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714484855",
		],
	],
	[
		"clientID" =>"000002194",
		"name" =>"Гусев Валерий Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "671006301676",
		"contacts" => [
			"mobile" => "+77014080071",
		],
	],
	[
		"clientID" =>"000002195",
		"name" =>"Каленов Серикбай Альбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "810208301444",
		"contacts" => [
			"mobile" => "+77751339138",
		],
	],
	[
		"clientID" =>"000002196",
		"name" =>"Луговцов Александр Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "921106300174",
		"contacts" => [
			"mobile" => "+77089151659",
		],
	],
	[
		"clientID" =>"000002197",
		"name" =>"Нам Михайл Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "820808300360",
		"contacts" => [
			"mobile" => "+77017310320",
		],
	],
	[
		"clientID" =>"000002198",
		"name" =>"Оспанов Айдар Кайратович",
		"attribute" => "Физ. лицо",
		"IIN" => "890517300410",
		"contacts" => [
			"mobile" => "+77077826676",
		],
	],
	[
		"clientID" =>"000001737",
		"name" =>"Сатарбаев Бахытбек Нурпазилович",
		"attribute" => "Физ. лицо",
		"IIN" => "690112302146",
		"contacts" => [
			"mobile" => "+77752288260",
		],
	],
	[
		"clientID" =>"000001738",
		"name" =>"Ержан",
		"attribute" => "Юр. лицо",
		"IIN" => "901212302515",
	],
	[
		"clientID" =>"000000864",
		"name" =>"Алимбекова Асель",
		"attribute" => "Физ. лицо",
		"IIN" => "880221451143",
		"contacts" => [
			"mobile" => "+77072110928",
		],
		"TCs" => [
			[
				"brand" => "Toyota Hichlander",
				"size" => "225/70/16",
				"gosNomer" => "Z 267 XEM",
			],
		],
	],
	[
		"clientID" =>"000000866",
		"name" =>"Галиев Фархад Хамитович",
		"attribute" => "Юр. лицо",
		"IIN" => "650615302547",
	],
	[
		"clientID" =>"CRM048916",
		"name" =>"Талипжан  Альпен Фарма",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77075989876",
		],
		"TCs" => [
			[
				"brand" => "Nissan",
				"size" => "185/70/14",
				"gosNomer" => "942NVA/05",
			],
		],
	],
	[
		"clientID" =>"CRM057358",
		"name" =>"Ботагоз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77752369914",
		],
		"TCs" => [
			[
				"brand" => "Toyota Corolla",
				"size" => "185/65/14",
				"gosNomer" => "536ААА/04",
			],
		],
	],
	[
		"clientID" =>"CRM056434",
		"name" =>"ТОО «Корпорация «Прогресс»",
		"attribute" => "Физ. лицо",
		"IIN" => "041240004495",
		"contacts" => [
			"mobile" => "+77777666671",
		],
	],
	[
		"clientID" =>"CRM056441",
		"name" =>"Абзал",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056442",
		"name" =>"Абзал",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056443",
		"name" =>"Абзал",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056431",
		"name" =>"Дима",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017567310",
		],
	],
	[
		"clientID" =>"CRM056435",
		"name" =>"Молчанов Руслан Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "901220300730",
		"contacts" => [
			"mobile" => "+77086613942",
		],
	],
	[
		"clientID" =>"CRM057343",
		"name" =>"Анатолий Toyota",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013412062",
		],
		"TCs" => [
			[
				"brand" => "Toyota Ipsum",
				"size" => "185/70/14",
				"gosNomer" => "A743HCN",
			],
		],
	],
	[
		"clientID" =>"CRM052541",
		"name" =>"Елена 215/65/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273111505",
			"mobile" => "+77273111505",
		],
	],
	[
		"clientID" =>"CRM052550",
		"name" =>"Грибушкин Виктор Алексеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "610430301370",
		"contacts" => [
			"mobile" => "+77755997213",
		],
	],
	[
		"clientID" =>"CRM052558",
		"name" =>"Усенбаев Баккелди Толегенович",
		"attribute" => "Физ. лицо",
		"IIN" => "820717301631",
		"contacts" => [
			"mobile" => "+77011089824",
		],
	],
	[
		"clientID" =>"CRM052552",
		"name" =>"Мухамет",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018885225",
		],
	],
	[
		"clientID" =>"CRM052598",
		"name" =>"Усиков  Юрий  Петрович",
		"attribute" => "Физ. лицо",
		"IIN" => "661220300253",
		"contacts" => [
			"mobile" => "+77017282123",
		],
		"TCs" => [
			[
				"brand" => "RenaultAster",
				"size" => "215/65/16",
				"gosNomer" => "958WSA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052601",
		"name" =>"Азаренко  Светлана  Всеволодовна",
		"attribute" => "Физ. лицо",
		"IIN" => "630730402324",
		"contacts" => [
			"mobile" => "+77025294748",
		],
		"TCs" => [
			[
				"brand" => "BMWX5",
				"size" => "245/65/17",
				"gosNomer" => "099ZKA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052611",
		"name" =>"Аккушиков Кайрат Темирханович",
		"attribute" => "Физ. лицо",
		"IIN" => "710523302335",
		"contacts" => [
			"mobile" => "+77013622624",
		],
	],
	[
		"clientID" =>"CRM052624",
		"name" =>"Айгуль",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77272273734",
			"mobile" => "+77272273734",
		],
	],
	[
		"clientID" =>"CRM052631",
		"name" =>"Балмашев Талгат Каракбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "790703303029",
		"contacts" => [
			"mobile" => "+77758686868",
		],
	],
	[
		"clientID" =>"CRM052681",
		"name" =>"Мурат Кадар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "murat.kadar@mail.ru",
			"mobile" => "+77017173379",
		],
	],
	[
		"clientID" =>"CRM052645",
		"name" =>"Акатаев Бауржан Амангельдыевич",
		"attribute" => "Физ. лицо",
		"IIN" => "791120300711",
		"contacts" => [
			"mobile" => "+77077057120",
		],
	],
	[
		"clientID" =>"CRM052644",
		"name" =>"Буланов  Игорь  Дмитриевич",
		"attribute" => "Физ. лицо",
		"IIN" => "851008300578",
		"contacts" => [
			"E-Mail" => "i.bulanov@astel.kz",
			"mobile" =>"+77774705775",
			"mobile" => "+77774705775",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "185/70/14",
				"gosNomer" => "940EKA/02",
			],
			[
				
				"size" => "185/65/14",
				
			],
		],
	],
	[
		"clientID" =>"CRM052654",
		"name" =>"215/60/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77056753636",
		],
	],
	[
		"clientID" =>"CRM052666",
		"name" =>"Дмитрий 235/75/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273975267",
		],
	],
	[
		"clientID" =>"CRM052673",
		"name" =>"Инжу",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77755640604",
		],
	],
	[
		"clientID" =>"CRM052677",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77007833735",
		],
	],
	[
		"clientID" =>"CRM052683",
		"name" =>"ТОО «AIG»",
		"attribute" => "Физ. лицо",
		"IIN" => "050140000329",
		"contacts" => [
			
		],
	],
	[
		"clientID" =>"CRM052688",
		"name" =>"ТОО «AIG»",
		"attribute" => "Физ. лицо",
		"IIN" => "050140000329",
		"contacts" => [
			
		],
	],
	[
		"clientID" =>"CRM052689",
		"name" =>"ТОО «AIG»",
		"attribute" => "Физ. лицо",
		"IIN" => "050140000329",
		"contacts" => [
			
		],
	],
	[
		"clientID" =>"CRM052696",
		"name" =>"Берестов Ростислав Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "870426300185",
		"contacts" => [
			"mobile" => "+77022165166",
		],
	],
	[
		"clientID" =>"CRM052736",
		"name" =>"ТОО «Postman»",
		"attribute" => "Юр. лицо",
		"IIN" => "071240016167",
		"contacts" => [
			"mobile" => "+77770072512",
		],
	],
	[
		"clientID" =>"CRM052741",
		"name" =>"Одиноков  Сергей  Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "630921301757",
		"contacts" => [
			"mobile" => "+77051878614",
		],
		"TCs" => [
			[
				"brand" => "ToyotaIpsum",
				"size" => "185/65/14",
				"gosNomer" => "A127FRN",
			],
		],
	],
	[
		"clientID" =>"CRM052734",
		"name" => "Алмагуль из Таугуля",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772338022",
			"adress" => "Коктем 1 , дом 2",
		],
		"TCs" => [
			[
				"brand" => "RenaultSandero",
				"size" => "185/65/15",
				"gosNomer" => "568KBA",
			],
		],
	],
	[
		"clientID" =>"CRM052754",
		"name" =>"Алатау Жарык Компания",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019880750",
		],
	],
	[
		"clientID" =>"CRM052782",
		"name" =>"235/65/17",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM052783",
		"name" =>"235/65/17",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM052800",
		"name" =>"Савин Николай  Михайлович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77056662839",
		],
		"TCs" => [
			[
				"brand" => "Daewoo",
				"size" => "185/60/14",
				"gosNomer" => "A795DHP",
			],
		],
	],
	[
		"clientID" =>"CRM052845",
		"name" =>"Карбозов Олжас Абызович",
		"attribute" => "Физ. лицо",
		"IIN" => "890523302300",
	],
	[
		"clientID" =>"CRM052834",
		"name" =>"Касенов Нурмагамбет Нургалиевич",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77017409784",
		],
	],
	[
		"clientID" =>"CRM052837",
		"name" =>"Козлова Светлана Юрьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "81022300375",
		"contacts" => [
			"E-Mail" => "svet_a19-81@mail.ru",
			"mobile" => "+77077200305",
		],
	],
	[
		"clientID" =>"CRM052875",
		"name" =>"рустемов жандос есенулы",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
			"mobile" => "+77712511634",
		],
	],
	[
		"clientID" =>"CRM052888",
		"name" =>"Нурлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012328332",
		],
	],
	[
		"clientID" =>"CRM052873",
		"name" =>"Джумабаев Серик Сабитович",
		"attribute" => "Физ. лицо",
		"IIN" => "800920301493",
		"contacts" => [
			"E-Mail" => "dss_1980@mail.ru",
			"mobile" => "+77010124444",
			"adress" => "Актау, 13-39-12, 130000",
		],
	],
	[
		"clientID" =>"CRM052877",
		"name" =>"Нуржан Султанович",
		"attribute" => "Физ. лицо",
		"IIN" => "730717300024",
		"contacts" => [
			"E-Mail" => "	nurzhan.sultanulu@gmail.com",
			"mobile" => "+77012233737",
		],
	],
	[
		"clientID" =>"CRM052894",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77089721949",
		],
	],
	[
		"clientID" =>"CRM052915",
		"name" =>"Косолапов  Иван  Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "91041630088",
		"contacts" => [
			"mobile" => "+77778210003",
		],
		"TCs" => [
			[
				"brand" => "KIASorento",
				"size" => "235/60/18",
				"gosNomer" => "721ZZA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052932",
		"name" =>"Манкусова Альфия Амантаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "830801401696",
		"contacts" => [
			"mobile" => "+77059906255",
		],
	],
	[
		"clientID" =>"CRM052929",
		"name" =>"Шалгумбаев Бактыбек Турсунбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "640128301176",
		"contacts" => [
			"mobile" => "+77471519974",
		],
	],
	[
		"clientID" =>"CRM052936",
		"name" =>"Тындыбаева Динара Акназаровна",
		"attribute" => "Физ. лицо",
		"IIN" => "860529400427",
		"contacts" => [
			"mobile" => "+77073150244",
		],
	],
	[
		"clientID" =>"CRM052945",
		"name" =>"Акылбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012041005",
		],
	],
	[
		"clientID" =>"CRM052956",
		"name" =>"Хан  Андрей   Геннадьевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057758681",
		],
		"TCs" => [
			[
				"brand" => "ToyotaFortuner",
				"size" => "265/70/16",
				"gosNomer" => "048TPA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052993",
		"name" =>"Алибек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016752525",
		],
	],
	[
		"clientID" =>"CRM053026",
		"name" =>"Кабжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013354527",
		],
	],
	[
		"clientID" =>"CRM053014",
		"name" =>"Олжас",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018889238",
		],
	],
	[
		"clientID" =>"CRM053045",
		"name" =>"Даутов  Азатжан  Аволосянович",
		"attribute" => "Физ. лицо",
		"IIN" => "730201301796",
		"contacts" => [
			"mobile" => "+77083121210",
		],
		"TCs" => [
			[
				"brand" => "Toyota4*4",
				"size" => "215/70/16",
				"gosNomer" => "A936CZP",
			],
		],
	],
	[
		"clientID" =>"CRM053072",
		"name" =>"Рем Никита Евгеньевич",
		"attribute" => "Физ. лицо",
		"IIN" => "920530300281",
		"contacts" => [
			"mobile" => "+77072112041",
		],
	],
	[
		"clientID" =>"CRM053050",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77785545651",
		],
	],
	[
		"clientID" =>"CRM053062",
		"name" =>"Игорь 77017860836",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017860836",
		],
	],
	[
		"clientID" =>"CRM053096",
		"name" =>"Руслан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77088470152",
		],
	],
	[
		"clientID" =>"CRM053078",
		"name" =>"Николай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77083504150",
			"adress" => "Байсеитовой дом 1 ,уг. ул. Жамбыла ",
		],
	],
	[
		"clientID" =>"CRM055506",
		"name" =>"Клиент не определен",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77272536147",
		],
	],
	[
		"clientID" =>"CRM055524",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
		],
	],
	[
		"clientID" =>"CRM055533",
		"name" =>"Альжан 265/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013696565",
		],
	],
	[
		"clientID" =>"CRM055536",
		"name" =>"Алла грузовые шины",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77013288208",
		],
	],
	[
		"clientID" =>"CRM055542",
		"name" =>"Амансиков Калибек Ринатович",
		"attribute" => "Физ. лицо",
		"IIN" => "911030302101",
		"contacts" => [
			"mobile" => "+77475620850",
		],
	],
	[
		"clientID" =>"CRM055546",
		"name" =>"Имашов Данияр",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM055586",
		"name" =>"Базилова Шолпан Шабановна",
		"attribute" => "Физ. лицо",
		"IIN" => "620911400880",
	],
	[
		"clientID" =>"CRM055587",
		"name" =>"Базилова Шолпан Шабановна",
		"attribute" => "Физ. лицо",
		"IIN" => "620911400880",
		"contacts" => [
			"mobile" => "+77771749334",
		],
		"TCs" => [
			[
				"brand" => "Volkswagen",
				"size" => "205/55/16",
				"gosNomer" => "653OXA/02",
			],
		],
	],
	[
		"clientID" =>"CRM055557",
		"name" =>"диски на бмв",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073793039",
		],
	],
	[
		"clientID" =>"CRM055606",
		"name" =>"Бауыржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77781108595",
		],
	],
	[
		"clientID" =>"CRM055612",
		"name" =>"Акметбаева С С",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019999100",
		],
		"TCs" => [
			[
				"brand" => "LEXSUSRX200",
				"size" => "235/55/20",
				"gosNomer" => "999 ACE 02",
			],
		],
	],
	[
		"clientID" =>"CRM055625",
		"name" =>"Мейрамбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016548894",
		],
	],
	[
		"clientID" =>"CRM055626",
		"name" =>"Саду",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => " saduakas.7997@mail.ru",
			"mobile" => "+77716568228",
		],
	],
	[
		"clientID" =>"CRM055685",
		"name" =>"Доларбек Толеген",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77071162666",
		],
	],
	[
		"clientID" =>"CRM055712",
		"name" =>"Омаров Ерлан",
		"attribute" => "Юр. лицо",
		"contacts" => [
			"mobile" => "+77076129977",
		],
	],
	[
		"clientID" =>"CRM055728",
		"name" =>"Товарищество с ограниченной ответственностью \"АманАс Транс\"",
		"attribute" => "Физ. лицо",
		"IIN" => "070740014334",
		"contacts" => [
			"E-Mail" => "munal55@mail.ru",
			"mobile" =>"+77017111166",
			"mobile" => "+77017111166",
			"adress" => "Атырауская область, Жылыойский район, г. Кульсары, мкр Прикаспий, д 7/3",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "285/65/17",
				"gosNomer" => "E822BU",
			],
		],
	],
	[
		"clientID" =>"CRM055738",
		"name" =>"Олжас 12С",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77021069994",
		],
	],
	[
		"clientID" =>"CRM055745",
		"name" =>"МАКС ПЕРЕКУПЩИК С АКТАУ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "maks.toyota@mail.ru",
			"mobile" => "+77075186123",
		],
	],
	[
		"clientID" =>"CRM055755",
		"name" =>"Райхан Атырау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77021077737",
		],
	],
	[
		"clientID" =>"CRM055775",
		"name" =>"265/70/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072181707",
		],
	],
	[
		"clientID" =>"CRM055779",
		"name" =>"285/45/22",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014977099",
		],
	],
	[
		"clientID" =>"CRM055771",
		"name" =>"Джураева Светлана Михайловна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77055186535",
		],
	],
	[
		"clientID" =>"CRM055774",
		"name" =>"Изатбеков Женис Амангелдиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "800509303217",
		"contacts" => [
			"mobile" => "+77079216480",
		],
	],
	[
		"clientID" =>"CRM055780",
		"name" =>"ТОО НИИ ТК",
		"attribute" => "Физ. лицо",
		"IIN" => "040740002513",
		"contacts" => [
			"mobile" =>"+77273761147",
			"mobile" => "+77273761147",
		],
	],
	[
		"clientID" =>"CRM055781",
		"name" =>"Михаил 225/60/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "mv.sedof@gmail.com",
			"mobile" =>"+77017152970",
			"mobile" => "+77017152970",
		],
	],
	[
		"clientID" =>"CRM057310",
		"name" =>"Анна Кефер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77019931763",
			"mobile" => "+77019931763",
		],
		"TCs" => [
			[
				"brand" => "ToyotaFGCruiser",
				"size" => "285/75/16",
				"gosNomer" => "A440ZKO",
			],
		],
	],
	[
		"clientID" =>"CRM053192",
		"name" =>"Ким Виталий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77759428870",
			"adress" => "г. Алматы, ул. Герцена д. 45 (р-н Рускулова Джангильдина)",
		],
	],
	[
		"clientID" =>"CRM053207",
		"name" =>"Жубаниязов",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053208",
		"name" =>"Жубаниязов",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053209",
		"name" =>"Жубаниязов",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053210",
		"name" =>"Жубаниязов Мурат",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053203",
		"name" =>"Танабеков  Кайрат  Кабдрашевич",
		"attribute" => "Физ. лицо",
		"IIN" => "910613300280",
		"contacts" => [
			"mobile" => "+77075202205",
		],
		"TCs" => [
			[
				"brand" => "Audi",
				"size" => "195/65/15",
				"gosNomer" => "102WRA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053216",
		"name" =>"Аденов Бауржан Усенович",
		"attribute" => "Физ. лицо",
		"IIN" => "821224300081",
		"contacts" => [
			"mobile" => "+77017691311",
		],
	],
	[
		"clientID" =>"CRM053226",
		"name" =>"БМВ-5",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053227",
		"name" =>"БМВ-5",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053200",
		"name" =>"Нурлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077220068",
		],
	],
	[
		"clientID" =>"CRM053254",
		"name" =>"Николай +77052182206",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052182206",
		],
	],
	[
		"clientID" =>"CRM053266",
		"name" =>"АЛПЫС",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076365370",
		],
	],
	[
		"clientID" =>"CRM053269",
		"name" =>"Хикалова Ольга Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "790830400844",
		"contacts" => [
			"mobile" => "+77071144935",
		],
	],
	[
		"clientID" =>"CRM053281",
		"name" =>"Павел",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272601804",
			"mobile" => "+77272601804",
		],
	],
	[
		"clientID" =>"CRM053284",
		"name" =>"Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012253822",
		],
	],
	[
		"clientID" =>"CRM053289",
		"name" =>"Мереке +77775184177",
		"attribute" => "Физ. лицо",
		"IIN" => "+77775184177",
		"contacts" => [
			"mobile" => "+77775184177",
		],
	],
	[
		"clientID" =>"CRM053286",
		"name" =>"Жанкелди Адилет",
		"attribute" => "Физ. лицо",
		"IIN" => "911211301523",
	],
	[
		"clientID" =>"CRM053333",
		"name" =>"Кайрат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011425366",
		],
	],
	[
		"clientID" =>"CRM053317",
		"name" =>"Кузнецов Алексей Леонидович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019954294",
		],
	],
	[
		"clientID" =>"CRM053329",
		"name" =>"Дмитрий 235/75/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273410257",
			"mobile" => "+77273410257",
		],
	],
	[
		"clientID" =>"CRM053318",
		"name" =>"Улан грузовые шины",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016900523",
		],
	],
	[
		"clientID" =>"CRM053334",
		"name" =>"Ильясов Талгат",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053335",
		"name" =>"Ильясов Талгат",
		"attribute" => "Физ. лицо",
	],
	[
		"clientID" =>"CRM053336",
		"name" =>"Ильясов Талгат",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053342",
		"name" =>"Искакова Ильзира",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013529824",
		],
	],
	[
		"clientID" =>"CRM053371",
		"name" =>"Жасулан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77781717057",
		],
	],
	[
		"clientID" =>"CRM053377",
		"name" =>"Бауржан +77014549435",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014549435",
		],
	],
	[
		"clientID" =>"CRM053397",
		"name" =>"Сапаралиев Нурберген Кендебайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "940826302072",
		"contacts" => [
			"mobile" => "+77027771236",
		],
	],
	[
		"clientID" =>"CRM053763",
		"name" =>"Серик +77018806668",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018806668",
			"adress" => "г. Астана",
		],
	],
	[
		"clientID" =>"CRM053821",
		"name" =>"Дархан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017225222",
		],
	],
	[
		"clientID" =>"CRM053519",
		"name" => "Толеген  (Тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077050594",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "175/70/14",
				"gosNomer" => "B567EHN",
			],
		],
	],
	[
		"clientID" =>"CRM056238",
		"name" =>"Токен Куаныш",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77019330190",
		],
		"TCs" => [
			[
				"brand" => "CADILLAC ESCALADE",
				"size" => "275/60/18",
				"gosNomer" => "F909BON",
			],
		],
	],
	[
		"clientID" =>"CRM056452",
		"name" =>"Кама",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77711164666",
		],
	],
	[
		"clientID" =>"CRM056446",
		"name" =>"Кенжебеков Ерлан Маликович",
		"attribute" => "Физ. лицо",
		"IIN" => "801103302373",
		"contacts" => [
			"mobile" => "+77027655330",
		],
	],
	[
		"clientID" =>"CRM056451",
		"name" =>"Ринат 235/75/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77786773007",
		],
	],
	[
		"clientID" =>"CRM056461",
		"name" =>"Самат 225/70/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013642737",
		],
	],
	[
		"clientID" =>"CRM056464",
		"name" =>"Данияр Грузовые 315/60/22.5",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77027246677",
		],
	],
	[
		"clientID" =>"CRM056469",
		"name" =>"Алия Астана",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77028751495",
		],
	],
	[
		"clientID" =>"CRM056486",
		"name" =>"Митус Валеерий Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "800208302877",
		"contacts" => [
			"mobile" => "+77478891861",
		],
	],
	[
		"clientID" =>"CRM056508",
		"name" =>"Ринат 7017680311",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056509",
		"name" =>"Ринат 7017680311",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056510",
		"name" =>"Ринат 7017680311",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056511",
		"name" =>"Ринат 7017680311",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056517",
		"name" =>"Канат 77022152375",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77022152375",
		],
	],
	[
		"clientID" =>"CRM056540",
		"name" =>"Бекбаев Талгат Канатович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77766100225",
		],
	],
	[
		"clientID" =>"CRM056548",
		"name" =>"Абзал Актау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771934343",
		],
	],
	[
		"clientID" =>"CRM056565",
		"name" =>"Малика",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77021185853",
		],
	],
	[
		"clientID" =>"CRM056567",
		"name" =>"Дмитрий Прадо",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015727070",
		],
	],
	[
		"clientID" =>"CRM056601",
		"name" =>"Егор Москва",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056602",
		"name" =>"Егор Москва",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056603",
		"name" =>"Егор Москва",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056611",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017372981",
		],
	],
	[
		"clientID" =>"CRM056607",
		"name" =>"Кубетенов Аскар Тулегенович",
		"attribute" => "Физ. лицо",
		"IIN" => "780923350194",
	],
	[
		"clientID" =>"CRM056623",
		"name" =>"Павел",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "pavel80olimp@mail.ru",
			"mobile" => "+77086168360",
		],
	],
	[
		"clientID" =>"CRM056649",
		"name" =>"Егор",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056650",
		"name" =>"Егор",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056656",
		"name" =>"Луконькин Владимир Константинович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+74957777733",
			"adress" => "Москва, Рублевское шоссе дом 64 корпус 2 ",
		],
	],
	[
		"clientID" =>"CRM054115",
		"name" => "Айтбай (спейс вагон)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051493373",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiSpaceWagon",
				"size" => "185/70/14",
				"gosNomer" => "872TKA/02",
			],
		],
	],
	[
		"clientID" =>"CRM048118",
		"name" =>"Тохтасын  нархоз",
		"attribute" => "Юр. лицо",
		
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "195/65/15",
				"gosNomer" => "A096EZ",
			],
		],
	],
	[
		"clientID" =>"CRM043688",
		"name" =>"Талгат водитель",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM057356",
		"name" =>"Сакен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017314834",
			"mobile" => "+7(701)731-48-34",
		],
		"TCs" => [
			[
				
				"size" => "245/40/20",
				"gosNomer" => "279BA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053338",
		"name" => "Меирман  (Mitsubishi)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775470006",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi",
				"size" => "265/70/15",
				"gosNomer" => "073WLA/02",
			],
		],
	],
	[
		"clientID" =>"CRM054009",
		"name" =>"Ernat Nurgeldy",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ernat.nurgeldy@khorgosgateway.com",
			"mobile" => "+77789588808",
		],
	],
	[
		"clientID" =>"CRM054023",
		"name" =>"Смирнов Дмитрий Александрович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77762272808",
		],
	],
	[
		"clientID" =>"CRM054032",
		"name" =>"Имидж-MB",
		"attribute" => "Физ. лицо",
		"IIN" => "070640008217",
		"contacts" => [
			"mobile" =>"+77172526474",
		],
	],
	[
		"clientID" =>"CRM054040",
		"name" =>"Александр 185/70",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "smirnov_69alex@mail.ru",
			"mobile" => "+77074241016",
		],
	],
	[
		"clientID" =>"CRM054043",
		"name" =>"Избасаров Улан Ералиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "791227303094",
		"contacts" => [
			"mobile" => "+77027255204",
		],
		"TCs" => [
			[
				"brand" => "Toyota Camry",
				"size" => "215/55/17",
				"gosNomer" => "512KEA/08",
			],
		],
	],
	[
		"clientID" =>"CRM054042",
		"name" =>"ТОО Winter Energy Servise (WES)",
		"attribute" => "Физ. лицо",
		"IIN" => "071140008569",
		"contacts" => [
			"mobile" => "+77086013027",
		],
	],
	[
		"clientID" =>"CRM047588",
		"name" => "Афанасий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77789547380",
		],
		"TCs" => [
			[
				
				"size" => "205/60/16",
				"gosNomer" => "H0137/08",
			],
		],
	],
	[
		"clientID" =>"CRM054044",
		"name" =>"Бирючков Александр Дмитриевич",
		"attribute" => "Физ. лицо",
		"IIN" => "540724301086",
		"contacts" => [
			"mobile" => "+77071020766",
		],
	],
	[
		"clientID" =>"CRM054050",
		"name" =>"ТОО \"Hyundai Premium Almaty\"",
		"attribute" => "Физ. лицо",
		"IIN" => "081040019780",
		"contacts" => [
			"E-Mail" => "ahmetov.1971@bk.ru",
			"mobile" => "+77011013979",
			"adress" => "Республика Казахстан, Алматы г., Райымбека, дом № 115/23",
		],
	],
	[
		"clientID" =>"CRM054080",
		"name" =>"Кузембаев Азат Найманович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77477320709",
			"mobile" => "+77477320709",
		],
		"TCs" => [
			[
				"brand" => "Subaru Outback",
				"size" => "195/65/15",
				"gosNomer" => "106PTA/05",
			],
		],
	],
	[
		"clientID" =>"CRM054099",
		"name" =>"Азамат Павлодар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012444262",
		],
	],
	[
		"clientID" =>"CRM054087",
		"name" =>"КайырбекДемегенов",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"E-Mail" => "daurenuat@gmail.com",
			"mobile" =>"+77715503543",
		],
	],
	[
		"clientID" =>"CRM054103",
		"name" =>"Товарищество с Ограниченной Ответственностью «Производственная Инновационная Компания «Ютария ltd»,",
		"attribute" => "Физ. лицо",
		"IIN" => "020540001663",
		"contacts" => [
			"mobile" => "+77058293390",
		],
	],
	[
		"clientID" =>"CRM054124",
		"name" =>"Каримов Дулат Актанович",
		"attribute" => "Физ. лицо",
		"IIN" => "620714302275",
		"contacts" => [
			"mobile" => "+77016509559",
		],
	],
	[
		"clientID" =>"CRM054137",
		"name" =>"Михаил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "mikhail.kazutin@mail.ru",
			"mobile" => "+77017665189",
		],
	],
	[
		"clientID" =>"CRM054128",
		"name" =>"От Шефа",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054133",
		"name" =>"Галина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273828899",
			"mobile" => "+77273828899",
		],
	],
	[
		"clientID" =>"CRM054153",
		"name" =>"Замира",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273841798",
			"mobile" => "+77273841798",
		],
	],
	[
		"clientID" =>"CRM054219",
		"name" =>"Ефременко Александр Иванович",
		"attribute" => "Физ. лицо",
		"IIN" => "710330302455",
		"contacts" => [
			"mobile" => "+77777796841",
		],
	],
	[
		"clientID" =>"CRM054221",
		"name" =>"Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013523439",
		],
	],
	[
		"clientID" =>"CRM054225",
		"name" =>"Мухидов Феруз Абдурасулович",
		"attribute" => "Физ. лицо",
		"IIN" => "011191000111",
		"contacts" => [
			"mobile" => "+77055630306",
		],
		"TCs" => [
			[
				"brand" => "AUDI",
				"size" => "255/35/19",
				"gosNomer" => "A842EA /77",
			],
		],
	],
	[
		"clientID" =>"CRM054239",
		"name" =>"Шарипов Марис Равильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "891219300186",
		"contacts" => [
			"mobile" => "+77073888379",
		],
		"TCs" => [
			[
				"brand" => "Volkswagen Golf I",
				"size" => "185/65/14",
				"gosNomer" => "A681MVM",
			],
		],
	],
	[
		"clientID" =>"CRM054270",
		"name" =>"Нургалиулы Айдос",
		"attribute" => "Физ. лицо",
		"IIN" => "860406300838",
		"contacts" => [
			"mobile" => "+77017319066",
		],
	],
	[
		"clientID" =>"CRM054298",
		"name" =>"Нуржан IG50",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013459505",
		],
	],
	[
		"clientID" =>"CRM054302",
		"name" =>"Мухамедияров Едиль Тулендиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "601231300470",
		"contacts" => [
			"mobile" => "+77475998794",
		],
		"TCs" => [
			[
				"brand" => "Honda",
				"size" => "175/65/14",
				"gosNomer" => "KGFH1942",
			],
		],
	],
	[
		"clientID" =>"CRM054350",
		"name" =>"Елена 195/55 /15",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054343",
		"name" =>"Князев Роман Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "891201301919",
		"contacts" => [
			"mobile" => "+77051840238",
		],
	],
	[
		"clientID" =>"CRM054353",
		"name" =>"Владимир 1 диск",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774770914",
		],
	],
	[
		"clientID" =>"CRM054366",
		"name" =>"Гайыпбаев Даурен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "daurenuat@gmail.com",
			"mobile" => "+77017971416",
			"adress" => "г. Жанаозен мкр.Рахат1 - ул. Кызгалдак (12) дом. 65",
		],
	],
	[
		"clientID" =>"CRM054393",
		"name" =>"Котенёв Константин Алексеевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77071058584",
		],
	],
	[
		"clientID" =>"CRM054411",
		"name" =>"Вера 275/70/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Veraglazova71@mail.ru",
			"mobile" =>"+77013894308",
			"mobile" => "+77013894308",
		],
	],
	[
		"clientID" =>"CRM054413",
		"name" =>"Ернар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017888897",
		],
	],
	[
		"clientID" =>"CRM054422",
		"name" =>"Ибраимов Жумабек",
		"attribute" => "Физ. лицо",
		"IIN" => "480810301644",
		"contacts" => [
			"mobile" => "+77757066320",
		],
	],
	[
		"clientID" =>"CRM054435",
		"name" =>"Корж Виктор Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "920611300876",
		"contacts" => [
			"mobile" => "+77775486888",
		],
	],
	[
		"clientID" =>"CRM054448",
		"name" =>"Ильдос",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013505138",
		],
	],
	[
		"clientID" =>"CRM054449",
		"name" =>"Елена 11 22,5",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774512630",
		],
	],
	[
		"clientID" =>"CRM054467",
		"name" =>"коврики на  RAV",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054468",
		"name" =>"коврики на  RAV",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054469",
		"name" =>"rav4",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054475",
		"name" =>"Кудайбергенов Азат Берикович",
		"attribute" => "Физ. лицо",
		"IIN" => "840127302309",
		"contacts" => [
			"mobile" => "+77019409384",
		],
		"TCs" => [
			[
				"brand" => "Kia Sorento",
				"size" => "265/70/16",
				"gosNomer" => "979ZVA/02",
			],
		],
	],
	[
		"clientID" =>"CRM054500",
		"name" =>"Эльман от Тани",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078198578",
		],
	],
	[
		"clientID" =>"CRM054508",
		"name" => "ТОО  «Гунн  Строй  сервис»",
		"attribute" => "Физ. лицо",
		"IIN" => "060240002985",
		"contacts" => [
			"mobile" => "+77051804294",
		],
	],
	[
		"clientID" =>"CRM054516",
		"name" =>"Асель",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77757486887",
		],
	],
	[
		"clientID" =>"CRM054525",
		"name" =>"Юрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
		],
	],
	[
		"clientID" =>"CRM054533",
		"name" =>"Ирина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77476598508",
		],
	],
	[
		"clientID" =>"CRM054535",
		"name" =>"Тусупбеков Алькен Газизович",
		"attribute" => "Физ. лицо",
		"IIN" => "700918301293",
		"contacts" => [
			"mobile" => "+77001117873",
		],
	],
	[
		"clientID" =>"CRM054527",
		"name" =>"Жаксыбаева Назерке Кынатовна",
		"attribute" => "Физ. лицо",
		"IIN" => "810625400965",
		"contacts" => [
			"mobile" => "+77772265904",
			"mobile" => "+77012265904",
		],
	],
	[
		"clientID" =>"CRM054540",
		"name" =>"Канат 2 б",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017801570",
		],
	],
	[
		"clientID" =>"CRM054549",
		"name" =>"Малыганов Александр",
		"attribute" => "Физ. лицо",
		"IIN" => "720712302354",
		"contacts" => [
			"mobile" => "+77776815070",
			"adress" => "Алматы, Айнабулак-3, 107",
		],
	],
	[
		"clientID" =>"CRM054565",
		"name" =>"Умит",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016826600",
		],
	],
	[
		"clientID" =>"CRM054580",
		"name" =>"Миронов Сергей Сергеевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77058059393",
		],
		"TCs" => [
			[
				"brand" => "Audi",
				"size" => "205/65/15",
				"gosNomer" => "A771FTN",
			],
		],
	],
	[
		"clientID" =>"CRM054575",
		"name" =>"Михаил +77052012228",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77052012228",
		],
	],
	[
		"clientID" =>"CRM054586",
		"name" =>"Асумутдин",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77055778277",
		],
	],
	[
		"clientID" =>"CRM054597",
		"name" =>"Имашов Данияр Магдиярович",
		"attribute" => "Физ. лицо",
		"IIN" => "860618303568",
		"contacts" => [
			"mobile" => "+77077266086",
		],
	],
	[
		"clientID" =>"CRM054599",
		"name" =>"Александр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014657070",
		],
	],
	[
		"clientID" =>"CRM054602",
		"name" =>"Жандырбаев Нурбек Ералиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "850619301298",
		"contacts" => [
			"mobile" => "+77758280688",
		],
	],
	[
		"clientID" =>"CRM054615",
		"name" =>"Гуменнов Олег Витальевич",
		"attribute" => "Физ. лицо",
		"IIN" => "651009301976",
		"contacts" => [
			"mobile" => "+77017332942",
		],
	],
	[
		"clientID" =>"CRM054608",
		"name" =>"Тимур Азбенов",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019520553",
		],
	],
	[
		"clientID" =>"CRM054652",
		"name" =>"Спандияр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024305189",
		],
	],
	[
		"clientID" =>"CRM054759",
		"name" =>"Мурзагалиева Алия Утеповна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273091724",
		],
	],
	[
		"clientID" =>"CRM054779",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272960428",
			"mobile" => "+77272960428",
		],
	],
	[
		"clientID" =>"CRM054789",
		"name" =>"Болат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
			"mobile" => "+77029777772",
		],
	],
	[
		"clientID" =>"CRM054793",
		"name" =>"Ержан",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054800",
		"name" =>"Кабетенов Аскар Тулегенович",
		"attribute" => "Физ. лицо",
		"IIN" => "780923350194",
		"contacts" => [
			"mobile" => "+77017173421",
		],
		"TCs" => [
			[
				"brand" => "NISSANPatrol",
				"size" => "275/70/16",
				"gosNomer" => "A 398RBP",
			],
		],
	],
	[
		"clientID" =>"CRM054826",
		"name" =>"Иващенко Владимир Ганиевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77475509412",
		],
		"TCs" => [
			[
				"brand" => "Honda",
				"size" => "195/65/15",
				"gosNomer" => "A640LSO",
			],
		],
	],
	[
		"clientID" =>"CRM054819",
		"name" =>"Кредит",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77751013122",
		],
	],
	[
		"clientID" =>"CRM054830",
		"name" =>"Садвакасова Жанара Айтжановна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77016489019",
		],
		"TCs" => [
			[
				"brand" => "Honda",
				"size" => "215/70/15",
				"gosNomer" => "B771ZKN",
			],
		],
	],
	[
		"clientID" =>"CRM054851",
		"name" =>"Кристина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017768008",
		],
	],
	[
		"clientID" =>"CRM054855",
		"name" =>"Жасулан",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM054867",
		"name" =>"Асель Саржигитова тендер",
		"attribute" => "Юр. лицо",
		"contacts" => [
			"E-Mail" => "nurlantai78@mail.ru",
			"mobile" =>"+77077881111",
			"mobile" => "+77077881111",
		],
	],
	[
		"clientID" =>"CRM054863",
		"name" =>"195/60/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77070481778",
		],
	],
	[
		"clientID" =>"CRM054899",
		"name" =>"Харин Дмитрий Анатольевич",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77717531868",
		],
	],
	[
		"clientID" =>"CRM054902",
		"name" =>"Замир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77010422220",
		],
	],
	[
		"clientID" =>"CRM054903",
		"name" =>"Куат 195/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018489188",
		],
	],
	[
		"clientID" =>"CRM054905",
		"name" =>"Общественный фонд Фонд выпускников РСФМШИ",
		"attribute" => "Физ. лицо",
		"IIN" => "991240010318",
		"contacts" => [
			"mobile" => "+77018699993",
		],
	],
	[
		"clientID" =>"CRM054927",
		"name" =>"Изотова Оксана Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "761103401768",
		"contacts" => [
			"mobile" => "+77017265695",
		],
	],
	[
		"clientID" =>"CRM054914",
		"name" =>"Мерей тоя",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "merei_033@mail.ru",
			"mobile" => "+77756213311",
		],
	],
	[
		"clientID" =>"CRM054931",
		"name" =>"Оман 7.00 R16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77075118724",
			"mobile" => "+77015118724",
		],
	],
	[
		"clientID" =>"CRM054954",
		"name" =>"Талгат 225/70/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77475001571",
		],
	],
	[
		"clientID" =>"CRM055036",
		"name" =>"Владимир Борисович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075009999",
			"adress" => "г. Алматы, Кульджинский тракт, 4а (возле развязки Восточка Ташкентская",
		],
	],
	[
		"clientID" =>"CRM055088",
		"name" =>"Сагиндикова Асемкул Садуакасовна",
		"attribute" => "Физ. лицо",
		"IIN" => "640405400060",
		"contacts" => [
			"mobile" => "+77714144396",
		],
	],
	[
		"clientID" =>"CRM055093",
		"name" =>"Тагибаев Асет Гулярханович",
		"attribute" => "Физ. лицо",
		"IIN" => "841107301493",
		"contacts" => [
			"mobile" => "+77012901615",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"gosNomer" => "690YSA",
			],
		],
	],
	[
		"clientID" =>"CRM055109",
		"name" =>"Фёдоров Генадий Васильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "521028300251",
		"contacts" => [
			"mobile" => "+77012130872",
		],
		"TCs" => [
			[
				"brand" => "Chevrolet Cruze",
				"size" => "205/60/16",
				"gosNomer" => "942SXA/02",
			],
		],
	],
	[
		"clientID" =>"CRM055142",
		"name" =>"Садыкова Галия Ильясовна",
		"attribute" => "Физ. лицо",
		"IIN" => "720211402387",
		"contacts" => [
			"mobile" => "+77772348940",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiDelica",
				"size" => "235/75/15",
				"gosNomer" => "A253PMO",
			],
		],
	],
	[
		"clientID" =>"CRM055145",
		"name" =>"Бохаев Сагынай Малтыкбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "660606300833",
		"contacts" => [
			"E-Mail" => "sagynay55@gmail.com",
			"mobile" =>"+77078912906",
			"mobile" => "+77078912906",
		],
	],
	[
		"clientID" =>"CRM055151",
		"name" =>"Оразбаев Аслан Ганиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "981004300077",
		"contacts" => [
			"E-Mail" => "arystanorazbaev@icloud.com",
			"mobile" =>"+77025987768",
			"mobile" => "+77025987768",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "175/70/14",
				"gosNomer" => "087",
			],
		],
	],
	[
		"clientID" =>"CRM055192",
		"name" =>"Олег. Рудный",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "feol2000@yandex.ru",
			"mobile" => "+77015331605",
		],
	],
	[
		"clientID" =>"CRM055248",
		"name" =>"195/65 R15 Maxxis",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076665527",
		],
	],
	[
		"clientID" =>"CRM055251",
		"name" =>"ТОО \"Аман-Курылыс\"",
		"attribute" => "Физ. лицо",
		"IIN" => "040340003246",
		"contacts" => [
			"E-Mail" => "Aman_Kurylys@mail.ru",
			"mobile" => "+77018821344",
			"adress" => "г. Кызылорда, ул. Сужикова №1",
		],
		"TCs" => [
			[
				"brand" => "Toyota Land Cruiser",
				"size" => "285/50/20",
				"gosNomer" => "нет",
			],
		],
	],
	[
		"clientID" =>"CRM055283",
		"name" =>"Курбанов Исайжан Якубович",
		"attribute" => "Физ. лицо",
		"IIN" => "610921301865",
		"contacts" => [
			"mobile" => "+77772369073",
		],
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "215/65/16",
				"gosNomer" => "558ZLA/02",
			],
		],
	],
	[
		"clientID" =>"CRM055289",
		"name" =>"Ерем",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77058055893",
		],
	],
	[
		"clientID" =>"CRM055291",
		"name" =>"Билей Андрей Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "751021300111",
		"contacts" => [
			"mobile" => "+77772143094",
		],
	],
	[
		"clientID" =>"CRM055319",
		"name" =>"ТОО \"IRON Technology\"",
		"attribute" => "Физ. лицо",
		"IIN" => "050940007771",
		"contacts" => [
			"E-Mail" => "iron_tech_@mail.ru",
			"mobile" => "+77759485315",
		],
	],
	[
		"clientID" =>"CRM055327",
		"name" =>"Тлеуов Еркин Елубаевич Индивидуальный предприниматель",
		"attribute" => "Физ. лицо",
		"IIN" => "800804301373",
		"contacts" => [
			
			"mobile" => "+77072002070",
		],
	],
	[
		"clientID" =>"CRM055321",
		"name" =>"Сергей 2 компл",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272715084",
		],
	],
	[
		"clientID" =>"CRM055352",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015465703",
		],
	],
	[
		"clientID" =>"CRM055354",
		"name" =>"Виктор Р15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77755731056",
		],
	],
	[
		"clientID" =>"CRM055355",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273798337",
		],
	],
	[
		"clientID" =>"CRM055364",
		"name" =>"minerva",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77089848584",
		],
	],
	[
		"clientID" =>"CRM055368",
		"name" =>"Андрей диски р17",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM055396",
		"name" =>"Клиент не определен",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77772340107",
		],
	],
	[
		"clientID" =>"CRM055417",
		"name" =>"ТОО «ABC (Эй Би Си)»",
		"attribute" => "Физ. лицо",
		"IIN" => "160940014330",
		"contacts" => [
			"mobile" =>"+79035945060",
			"mobile" => "+77777642664",
			"adress" => "г.Атырау, проспект Абилхайыр хана, строение 22",
		],
	],
	[
		"clientID" =>"CRM055436",
		"name" =>"Джусупбеков Даулет Калибекович",
		"attribute" => "Физ. лицо",
		"IIN" => "610101301575",
		"contacts" => [
			
			"mobile" => "+77011700801",
		],
		"TCs" => [
			[
				"brand" => "Toyota Land Cruiser",
				"size" => "275/65/17",
				"gosNomer" => "858UYA/02",
			],
		],
	],
	[
		"clientID" =>"CRM055451",
		"name" =>"Василий",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77017583915",
		],
	],
	[
		"clientID" =>"CRM055475",
		"name" =>"Берик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018831339",
		],
	],
	[
		"clientID" =>"CRM055479",
		"name" =>"калибек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77785120057",
		],
	],
	[
		"clientID" =>"CRM055495",
		"name" =>"нестле",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM048922",
		"name" =>"Екатерина Берлин Хеми",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77017214731",
		],
		"TCs" => [
			[
				"brand" => "Skoda",
				"size" => "195/65/15",
				"gosNomer" => "H205483",
			],
		],
	],
	[
		"clientID" =>"CRM056224",
		"name" =>"Путинцей Андрей Петрович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775985555",
		],
		"TCs" => [
			[
				"brand" => "Nissan ",
				"size" => "185/65/14",
				"gosNomer" => "A450YNM",
			],
		],
	],
	[
		"clientID" =>"CRM056265",
		"name" =>"Омиртай Аскар",
		"attribute" => "Физ. лицо",
		
		"TCs" => [
			[
				
				"size" => "265/60/18",
				"gosNomer" => "888FEB/02",
			],
		],
	],
	[
		"clientID" =>"CRM015756",
		"name" =>"Пуляхин  Игорь",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77071345901",
		],
		"TCs" => [
			[
				"brand" => "HuyndaiGetz",
				"size" => "175/65/14",
				"gosNomer" => "798 FYA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053857",
		"name" => "Дмитрий  (фольксваген)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77722934117",
		],
		"TCs" => [
			[
				"brand" => "VWGolf",
				"size" => "195/65/15",
				"gosNomer" => "A058LDO",
			],
		],
	],
	[
		"clientID" =>"000002371",
		"name" =>"Фаизов Джалиль Анварович",
		"attribute" => "Физ. лицо",
		"IIN" => "670103300335",
		"contacts" => [
			"mobile" => "+77072499558",
		],
	],
	[
		"clientID" =>"000002372",
		"name" =>"Сатылганов Уалихан Алпамышович",
		"attribute" => "Физ. лицо",
		"IIN" => "700901300529",
		"contacts" => [
			"mobile" =>"+77017175749",
			"mobile" => "+77017175749",
		],
	],
	[
		"clientID" =>"000002374",
		"name" =>"ФИТКУЛЛИН СЕРГЕЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "850422301356",
		"contacts" => [
			"E-Mail" => "fitkulin.s@mail.ru",
			"mobile" =>"+77013328999",
			"mobile" => "+77013328999",
		],
	],
	[
		"clientID" =>"000002375",
		"name" =>"Аскаров Рустам Сегимбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840802302605",
		"contacts" => [
			"mobile" => "+77077599332",
		],
	],
	[
		"clientID" =>"000002378",
		"name" =>"БЕЙСАТОВ СЕРИК ДОСУМХАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "540319300086",
		"contacts" => [
			"E-Mail" => "&lt;serikdosum@mail.ru&gt;",
			"mobile" =>"+77015334754",
			"mobile" => "+77015334754",
		],
	],
	[
		"clientID" =>"000002379",
		"name" =>"ВИТЧЕНКО ЕВГЕНИЙ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "730720301817",
		"contacts" => [
			"mobile" => "+77077990032",
		],
	],
	[
		"clientID" =>"000002380",
		"name" =>"Играшин Жасулан Мухтарович",
		"attribute" => "Физ. лицо",
		"IIN" => "940326351528",
		"contacts" => [
			"mobile" => "+77018914294",
		],
	],
	[
		"clientID" =>"000002382",
		"name" =>"ХАЛИЛОВ НУРИМАН СУЛЕЙМАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "580302301415",
		"contacts" => [
			"mobile" => "+77772278776",
		],
	],
	[
		"clientID" =>"000002384",
		"name" =>"РАХИМБЕРДИНОВ МУРАТКАЗЫ ТУРГАЗИНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "560709301365",
		"contacts" => [
			"mobile" => "+77013667515",
		],
	],
	[
		"clientID" =>"000002386",
		"name" =>"Маденова Меруерт Маратовна",
		"attribute" => "Физ. лицо",
		"IIN" => "890222450478",
		"contacts" => [
			"mobile" => "+77787882819",
		],
	],
	[
		"clientID" =>"000002331",
		"name" =>"ДУСКАЛЕВА АЛИЯ НИКОЛАЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "770621402958",
		"contacts" => [
			"E-Mail" => "a.duskaleva@mail.ru",
			"mobile" => "+77015191634",
		],
	],
	[
		"clientID" =>"000002332",
		"name" =>"ТОЛЫБАЕВ ИСЛАМ САНДИБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "940622350033",
		"contacts" => [
			"mobile" => "+77015442282",
		],
	],
	[
		"clientID" =>"000002334",
		"name" =>"Альсеров Жасулан Айбарович",
		"attribute" => "Физ. лицо",
		"IIN" => "900118300614",
		"contacts" => [
			"mobile" => "+77018487622",
		],
	],
	[
		"clientID" =>"000002338",
		"name" =>"Геринг Сергей Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "920502300516",
		"contacts" => [
			"mobile" => "+77029099998",
		],
	],
	[
		"clientID" =>"000002339",
		"name" =>"АБДИРОВ ЕРЛАН МАХСОТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "680711301192",
		"contacts" => [
			"E-Mail" => "jangala_kurilis@mail.ru",
			"mobile" =>"+77011412072",
			"mobile" => "+77011412072",
		],
	],
	[
		"clientID" =>"000002340",
		"name" =>"Филимонов Алексей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "641208301410",
		"contacts" => [
			"mobile" => "+77772516595",
		],
	],
	[
		"clientID" =>"000002342",
		"name" =>"СЕМЕНИЩЕВ ВЛАДИМИР ВЛАДИМИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "800516300429",
	],
	[
		"clientID" =>"000002344",
		"name" =>"ТЕМИРГАЛИЕВ ТАЛГАТ КЕНЖЕГАЛИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "680405300379",
	],
	[
		"clientID" =>"000002345",
		"name" =>"ПОЛЕВОВ ИГОРЬ ЛЕОНИДОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "660413301129",
		"contacts" => [
			"mobile" => "+77052570330",
		],
	],
	[
		"clientID" =>"000002346",
		"name" =>"ТАБЫНБАЕВА АЛИМА НАЖМАТДИНОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "770612403394",
	],
	[
		"clientID" =>"000002347",
		"name" =>"ГАЛКИНА ТАТЬЯНА АНАТОЛЬЕВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "741102401124",
		"contacts" => [
			"mobile" => "+77017459269",
		],
	],
	[
		"clientID" =>"000002349",
		"name" =>"Старыш Анатолий Трифонович",
		"attribute" => "Физ. лицо",
		"IIN" => "470803301169",
	],
	[
		"clientID" =>"000002350",
		"name" =>"Попов Виталий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "850825300100",
		"contacts" => [
			"E-Mail" => "ivzore@gmail.com",
			"mobile" => "+77773000800",
		],
	],
	[
		"clientID" =>"000002352",
		"name" =>"КОНЯЕВ НИКОЛАЙ ЮРЬЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "660325300539",
		"contacts" => [
			"mobile" => "+77782688863",
		],
	],
	[
		"clientID" =>"000002353",
		"name" =>"ШАЙДАРБЕКОВ БЕЙСЕН ОРУМБЕКОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "600518301109",
		"contacts" => [
			"mobile" => "+77077250797",
		],
	],
	[
		"clientID" =>"000002354",
		"name" =>"Айкенов Медет",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77782900004",
		],
	],
	[
		"clientID" =>"000002355",
		"name" =>"ЧЕН АЛЕКСАНДР ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "870926300103",
	],
	[
		"clientID" =>"000002359",
		"name" =>"КУРАМШИН ТИМУР БУЛАТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "731203300018",
		"contacts" => [
			"mobile" => "+77017681803",
		],
	],
	[
		"clientID" =>"000002361",
		"name" =>"СУЛТАНОВА ЭЛЬНУРА РИФКАТОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "690729400379",
		"contacts" => [
			"E-Mail" => "alnura69@mail.ru",
			"mobile" => "+77778030241",
		],
	],
	[
		"clientID" =>"000002362",
		"name" =>"НУРЛАНОВ ГАЛЫМ АБДЫКАДЫРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "591028300284",
		"contacts" => [
			"mobile" => "+77012221652",
		],
	],
	[
		"clientID" =>"000002363",
		"name" =>"КИМ МИХАИЛ АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "820418399022",
		"contacts" => [
			"mobile" =>"+77273023993",
			"mobile" => "+77073210182",
		],
		"TCs" => [
			[
				
				"size" => "235/55/18",
				
			],
		],
	],
	[
		"clientID" =>"000002364",
		"name" =>"Кселл АО",
		"attribute" => "Юр. лицо",
		"IIN" => "980540002879",
		"contacts" => [
			"adress" => "050051, Республика Казахстан, г. Алматы, мкн. Самал-2, 100",
		],
	],
	[
		"clientID" =>"000002367",
		"name" =>"ЖУМАРТОВ ТЛЕГЕН БЕКСУЛТАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "880901350482",
		"contacts" => [
			"mobile" =>"+77775716700",
			"mobile" => "+77775716700",
		],
	],
	[
		"clientID" =>"000002368",
		"name" =>"МАМЕДОВА ЛЕЙЛА ИСРАФИЛОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "741215402385",
		"contacts" => [
			"mobile" => "+77714555043",
		],
	],
	[
		"clientID" =>"000002369",
		"name" =>"Гевич Дмитрий Степанович",
		"attribute" => "Физ. лицо",
		"IIN" => "751028300279",
		"contacts" => [
			"mobile" => "+77017664689",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiASX",
				"size" => "215/65/16",
				"gosNomer" => "924WBA/02",
			],
		],
	],
	[
		"clientID" =>"000002370",
		"name" =>"Сугралинова  Айгерим  Асылбековна",
		"attribute" => "Физ. лицо",
		"IIN" => "870720450037",
		"contacts" => [
			"mobile" =>"+77012225987",
			"mobile" => "+77012225987",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "205/55/16",
				"gosNomer" => "709TRA/02",
			],
		],
	],
	[
		"clientID" =>"CRM056863",
		"name" =>"Павел",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075907151",
		],
		"TCs" => [
			[
				"brand" => "Nissan TERRANO",
				"size" => "31/10,5/15",
				"gosNomer" => "A371EZP",
			],
		],
	],
	[
		"clientID" =>"CRM059897",
		"name" =>"Иван 225/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77781266293",
		],
	],
	[
		"clientID" =>"CRM059900",
		"name" =>"Анатолий 195/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77014298377",
		],
	],
	[
		"clientID" =>"CRM059901",
		"name" =>"Евсеенко Александр Андреевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sacha067@mail.ru",
			"mobile" =>"+77021457837",
		],
	],
	[
		"clientID" =>"CRM059902",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772752670",
		],
	],
	[
		"clientID" =>"CRM060520",
		"name" =>"Бауржан БМВ диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77079111163",
		],
	],
	[
		"clientID" =>"CRM060544",
		"name" =>"Юля 285/75/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774700659",
		],
	],
	[
		"clientID" =>"CRM060545",
		"name" =>"Юля 285/75/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774700659",
		],
	],
	[
		"clientID" =>"CRM060547",
		"name" =>"Керимбеков Максат Ануарулы",
		"attribute" => "Физ. лицо",
		"IIN" => "931123300719",
		"contacts" => [
			"mobile" => "+77026494959",
		],
	],
	[
		"clientID" =>"CRM060521",
		"name" =>"Бахыт",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77273014798",
		],
	],
	[
		"clientID" =>"CRM060543",
		"name" =>"Ахметова Толкын Токсанбаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "740119402053",
		"contacts" => [
			"mobile" =>"+77019880936",
		],
	],
	[
		"clientID" =>"CRM060565",
		"name" =>"Абраменко Вячеслав Олегович",
		"attribute" => "Физ. лицо",
		"IIN" => "750115302661",
		"contacts" => [
			"mobile" =>"+77078222888",
			"mobile" => "+77072209337",
		],
	],
	[
		"clientID" =>"CRM060569",
		"name" =>"Сергей попарки",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019301515",
		],
	],
	[
		"clientID" =>"CRM060572",
		"name" =>"Толеген Санг Енг",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012201978",
		],
	],
	[
		"clientID" =>"CRM060573",
		"name" =>"Вадим Мистраль",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076127868",
		],
	],
	[
		"clientID" =>"CRM060600",
		"name" =>"Медеу",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "medeu-chsi001@mail.ru",
			"mobile" => "+77085203138",
		],
	],
	[
		"clientID" =>"CRM060602",
		"name" =>"Хафизов Нуржан Серикбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "840825303571",
		"contacts" => [
			"mobile" => "+77010359175",
		],
	],
	[
		"clientID" =>"CRM060621",
		"name" =>"Дутова Ольга Григорьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "560312104379",
		"contacts" => [
			"mobile" => "+77772007612",
		],
	],
	[
		"clientID" =>"CRM060615",
		"name" =>"Абай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017839347",
		],
	],
	[
		"clientID" =>"CRM060652",
		"name" =>"Анощенко Вячеслав Станиславович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771113118",
		],
	],
	[
		"clientID" =>"CRM060671",
		"name" =>"ТОО IQS Engineering",
		"attribute" => "Физ. лицо",
		"IIN" => "060840010707",
		"contacts" => [
			"mobile" => "+77017653868",
		],
	],
	[
		"clientID" =>"CRM060683",
		"name" =>"Вячеслав",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "slavik256@inbox.ru",
			"mobile" => "+77768085022",
		],
	],
	[
		"clientID" =>"CRM060718",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77071666679",
		],
	],
	[
		"clientID" =>"CRM060722",
		"name" =>"Кузнецов Сергей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "710806300199",
	],
	[
		"clientID" =>"CRM060699",
		"name" =>"Лесниченко Александр Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "851024301028",
		"contacts" => [
			"mobile" => "+77072350570",
		],
	],
	[
		"clientID" =>"CRM060700",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772866777",
		],
	],
	[
		"clientID" =>"CRM060725",
		"name" =>"Маргарита",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77770100750",
		],
	],
	[
		"clientID" =>"CRM060771",
		"name" =>"Досмухан Субару",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078236323",
		],
	],
	[
		"clientID" =>"CRM060762",
		"name" =>"ТОО Специализированный комбинат ритуальных услуг г Алматы",
		"attribute" => "Физ. лицо",
		"IIN" => "960240000695",
		"contacts" => [
			"mobile" => "+77055000101",
		],
	],
	[
		"clientID" =>"CRM060766",
		"name" =>"Оналбаев Улан Айтидантович",
		"attribute" => "Физ. лицо",
		"IIN" => "881017301570",
		"contacts" => [
			"mobile" => "+77017200616",
		],
	],
	[
		"clientID" =>"CRM060778",
		"name" =>"Мадина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77786155557",
		],
	],
	[
		"clientID" =>"CRM060781",
		"name" =>"Ерлан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077588676",
		],
	],
	[
		"clientID" =>"CRM060797",
		"name" =>"Дания ВШМ",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77273376648",
			"mobile" => "+77773878300",
		],
	],
	[
		"clientID" =>"CRM060800",
		"name" =>"Амангельды 225/65/17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772627614",
		],
	],
	[
		"clientID" =>"CRM060819",
		"name" =>"Аймгожанов Шалкар Мухтарович",
		"attribute" => "Физ. лицо",
		"IIN" => "830115302619",
		"contacts" => [
			"mobile" => "+77017472756",
		],
	],
	[
		"clientID" =>"CRM060833",
		"name" =>"Олжабеков Олжас Сембекович",
		"attribute" => "Физ. лицо",
		"IIN" => "911023300098",
		"contacts" => [
			"mobile" =>"+77762002022",
		],
	],
	[
		"clientID" =>"CRM060855",
		"name" =>"Сангинов Дулер",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77054150300",
		],
	],
	[
		"clientID" =>"CRM060872",
		"name" =>"Александр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "psv-86-86@mail.ru",
			"mobile" => "+7075211110",
		],
	],
	[
		"clientID" =>"CRM060873",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "servis.moy.su@mail.ru",
			"mobile" => "+77076033215",
		],
	],
	[
		"clientID" =>"CRM060874",
		"name" =>"Нуржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "isaev048@mail.ru",
			"mobile" => "+77018307979",
		],
	],
	[
		"clientID" =>"CRM060875",
		"name" =>"Кирилл",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kirill.bumagin@kz.atlascopco.com",
			"mobile" => "+77017669256",
		],
	],
	[
		"clientID" =>"CRM060876",
		"name" =>"Den",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "asdfasda@asfdasf.kz",
			"mobile" => "+73412414123",
		],
	],
	[
		"clientID" =>"CRM060877",
		"name" =>"Дос",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "adam_k80@mail.ru",
			"mobile" => "+77072024949",
		],
	],
	[
		"clientID" =>"CRM060878",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sheriff.o.k@mail.ru",
			"mobile" => "+77012105939",
		],
	],
	[
		"clientID" =>"CRM060879",
		"name" =>"Асхат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Akhatov81@mail.ru",
			"mobile" => "+77079480777",
		],
	],
	[
		"clientID" =>"CRM060880",
		"name" =>"Михиал",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "medvedev.23.04@gmail.com",
			"mobile" => "+77077299006",
		],
	],
	[
		"clientID" =>"CRM060881",
		"name" =>"михаил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "limorenkoolj@mail.ru",
			"mobile" => "+77054154054",
		],
	],
	[
		"clientID" =>"CRM060882",
		"name" =>"Али",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "koksai.mail.ru@mail.ru",
			"mobile" => "+77081054247",
		],
	],
	[
		"clientID" =>"CRM060883",
		"name" =>"Andrey",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "andreydisel@mail.ru",
			"mobile" => "+77057343788",
		],
	],
	[
		"clientID" =>"CRM060884",
		"name" =>"Арман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "aro82@mail.ru",
			"mobile" => "+77016267740",
		],
	],
	[
		"clientID" =>"CRM060885",
		"name" =>"кайсар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "suindikovkaisar1990@gmail.com",
			"mobile" => "+77474692944",
		],
	],
	[
		"clientID" =>"CRM060886",
		"name" =>"Раим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "raim.uz.kz@mail.ru",
			"mobile" => "+77009743648",
		],
	],
	[
		"clientID" =>"CRM060887",
		"name" =>"СУЛТАН",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sultantdekna@mail.ru",
			"mobile" => "996770336666",
		],
	],
	[
		"clientID" =>"CRM060888",
		"name" =>"Марат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kskgalim@yandex.kz",
			"mobile" => "3286067",
		],
	],
	[
		"clientID" =>"CRM060889",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sadfsdf@mail.ru",
			"mobile" => "+77718883024",
		],
	],
	[
		"clientID" =>"CRM060890",
		"name" =>"ОЛЕГ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "oleg24shevchenko@gmail.com",
			"mobile" => "+77776321255",
		],
	],
	[
		"clientID" =>"CRM060891",
		"name" =>"Алмас",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "professor_z@mail.ru",
			"mobile" => "+77072514581",
		],
	],
	[
		"clientID" =>"CRM060892",
		"name" =>"Артём",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "puh.80@mail.ru",
			"mobile" => "+77058107689",
		],
	],
	[
		"clientID" =>"CRM060893",
		"name" =>"Канат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "rus_m1975@mail.ru",
			"mobile" => "+77010719889",
		],
	],
	[
		"clientID" =>"CRM060894",
		"name" =>"Адият",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zhumatayev_a@mail.ru",
			"mobile" => "+77075550823",
		],
	],
	[
		"clientID" =>"CRM060895",
		"name" =>"тимур",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "mega.nuralin@bk.ru",
			"mobile" => "+77088962093",
		],
	],
	[
		"clientID" =>"CRM060896",
		"name" =>"Муратжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "hozstroysnab@mail.ru",
			"mobile" => "+77017580174",
		],
	],
	[
		"clientID" =>"CRM060897",
		"name" =>"Аян",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ayan.izbasarov@mail.ru",
			"mobile" => "+77022976667",
		],
	],
	[
		"clientID" =>"CRM060898",
		"name" =>"Василий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "fantom_v60@mail.ru",
			"mobile" => "+77773516731",
		],
	],
	[
		"clientID" =>"CRM060899",
		"name" =>"Вячеслав",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "bednyak91@mail.ru",
			"mobile" => "+77078108525",
		],
	],
	[
		"clientID" =>"CRM060900",
		"name" =>"Еркебулан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ytoilubayev@kkb.kz",
			"mobile" => "+77753338750",
		],
	],
	[
		"clientID" =>"CRM060901",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ak.tau@mail.ru",
			"mobile" => "+77714989149",
		],
	],
	[
		"clientID" =>"CRM060902",
		"name" =>"амшель",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "РєРєРєРєРєРєРєРєРєРєРєРєРє@hhhhhhh.ru",
			"mobile" => "88888888888888",
		],
	],
	[
		"clientID" =>"CRM060903",
		"name" =>"дархан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "D-a-r-h-a-n.91@mail.ru",
			"mobile" => "+77477133139",
		],
	],
	[
		"clientID" =>"CRM060904",
		"name" =>"нурсултан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "nursultan.kazkorgroup@mail.ru",
			"mobile" => "+77016006012",
		],
	],
	[
		"clientID" =>"CRM060905",
		"name" =>"нурахмет",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Р°РјСЂР°РµРІ1959@mail.ru",
			"mobile" => "+77072666707",
		],
	],
	[
		"clientID" =>"CRM060907",
		"name" =>"Максат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "email@email.email",
			"mobile" => "+77753021942",
		],
	],
	[
		"clientID" =>"CRM060908",
		"name" =>"Али",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "y.kurbanali@mail.ru",
			"mobile" => "+7017011122",
		],
	],
	[
		"clientID" =>"CRM060909",
		"name" =>"Гамзат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "gamzat.sagandykov@mail.ru",
			"mobile" => "374752",
		],
	],
	[
		"clientID" =>"CRM060910",
		"name" =>"руслан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "pshon74@mail.ru",
			"mobile" => "+77078392119",
		],
	],
	[
		"clientID" =>"CRM060911",
		"name" =>"Азамат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "san_light@mail.ru",
			"mobile" => "996772503509",
		],
	],
	[
		"clientID" =>"CRM060912",
		"name" =>"Ринат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "rinatmamutov94@gmail.com",
			"mobile" => "+77071380898",
		],
	],
	[
		"clientID" =>"CRM060913",
		"name" =>"Дамир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "damir_t.b@mail.ru",
			"mobile" => "+77284040013",
		],
	],
	[
		"clientID" =>"CRM060914",
		"name" =>"Ниикта",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "nikita.1996.ru@mail.ru",
			"mobile" => "+77474820581",
		],
	],
	[
		"clientID" =>"CRM060915",
		"name" =>"Arman",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "arman.baev@mail.ru",
			"mobile" => "+7773840004",
		],
	],
	[
		"clientID" =>"CRM060916",
		"name" =>"Игорь",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "popoff_i@mail.ru",
			"mobile" => "+77017501741",
		],
	],
	[
		"clientID" =>"CRM060917",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "tazovod2017@mail.ru",
			"mobile" => "+77088013199",
		],
	],
	[
		"clientID" =>"CRM060918",
		"name" =>"ertisbek",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ertisbek@mail.ru",
			"mobile" => "+77477050120",
		],
	],
	[
		"clientID" =>"CRM060919",
		"name" =>"Виталий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "master-sveta@inbox.ru",
			"mobile" => "+77013184679",
		],
	],
	[
		"clientID" =>"CRM060920",
		"name" =>"Наталья",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Ngriz07@mail.ru",
			"mobile" => "+77474608850",
		],
	],
	[
		"clientID" =>"CRM060921",
		"name" =>"Георгий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "lavrienko1972@mail.ru",
			"mobile" => "+77054076869",
		],
	],
	[
		"clientID" =>"CRM060922",
		"name" =>"Ерсултан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "erema_91@mail.ru",
			"mobile" => "+77023889878",
		],
	],
	[
		"clientID" =>"CRM060923",
		"name" =>"Жора",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zhuan_88@mail.ru",
			"mobile" => "3780811",
		],
	],
	[
		"clientID" =>"CRM060925",
		"name" =>"Санат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sanat.abraliev@mail.ru",
			"mobile" => "+77013960020",
		],
	],
	[
		"clientID" =>"CRM060926",
		"name" =>"Руслан Михетов",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "mikhetov@bk.ru",
			"mobile" => "+77787307630",
		],
	],
	[
		"clientID" =>"CRM060927",
		"name" =>"Мадияр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kazok91@mail.ru",
			"mobile" => "+77783320716",
		],
	],
	[
		"clientID" =>"CRM060928",
		"name" =>"Galymjan",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "galo92.92@mail.ru",
			"mobile" => "+77028555652",
		],
	],
	[
		"clientID" =>"CRM060929",
		"name" =>"Бауыржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "baur_baiga@mail.ru",
			"mobile" => "+77025000586",
		],
	],
	[
		"clientID" =>"CRM060930",
		"name" =>"Санжар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sanzhik001@mail.ru",
			"mobile" => "+77012229777",
		],
	],
	[
		"clientID" =>"CRM060931",
		"name" =>"Айман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "aiman_kz67@mail.ru",
			"mobile" => "+77057243033",
		],
	],
	[
		"clientID" =>"CRM060932",
		"name" =>"Денис",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "denisuskaman@mail.ru",
			"mobile" => "+77051832108",
		],
	],
	[
		"clientID" =>"CRM060933",
		"name" =>"ЭЛЬМАР",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "munar@yandex.ru",
			"mobile" => "+77013409953",
		],
	],
	[
		"clientID" =>"CRM060934",
		"name" =>"Ольга",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "933015@mail.ru",
			"mobile" => "+77014764216",
		],
	],
	[
		"clientID" =>"CRM060935",
		"name" =>"Sergey",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "an_sergey@mail.ru",
			"mobile" => "+77019717070",
		],
	],
	[
		"clientID" =>"CRM060936",
		"name" =>"Зульяр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zulkin84@mail.ru",
			"mobile" => "+77054436787",
		],
	],
	[
		"clientID" =>"CRM060937",
		"name" =>"Nurlan",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "87004567123n@gmail.com",
			"mobile" => "+77012203729",
		],
	],
	[
		"clientID" =>"CRM060938",
		"name" =>"Кайсар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kaisar.medelbek@mail.ru",
			"mobile" => "+77081990306",
		],
	],
	[
		"clientID" =>"CRM060939",
		"name" =>"Айдар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "aidar.berik@mail.ru",
			"mobile" => "+77077002202",
		],
	],
	[
		"clientID" =>"CRM060940",
		"name" =>"Омар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "abuomar91@mail.ru",
			"mobile" => "+77753413161",
		],
	],
	[
		"clientID" =>"CRM060941",
		"name" =>"Мади",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "tc2009@mail.ru",
			"mobile" => "+77082336789",
		],
	],
	[
		"clientID" =>"CRM060942",
		"name" =>"Olqa",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "olga_larina_vko@mail.ru",
			"mobile" => "+77055272152",
		],
	],
	[
		"clientID" =>"CRM060943",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "post@magnet.kz",
			"mobile" => "+77772463005",
		],
	],
	[
		"clientID" =>"CRM060944",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "mk-232@mail.ru",
			"mobile" => "+77051079457",
		],
	],
	[
		"clientID" =>"CRM060945",
		"name" =>"Шындауыл",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kassymov_73@mail.ru",
			"mobile" => "+77015143302",
		],
	],
	[
		"clientID" =>"CRM060946",
		"name" =>"куанышбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "albygrupp@gmail.com",
			"mobile" => "+77718417777",
		],
	],
	[
		"clientID" =>"CRM060947",
		"name" =>"Бауржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "almas03@mail.ru",
			"mobile" => "+77071440005",
		],
	],
	[
		"clientID" =>"CRM060976",
		"name" =>"Адиль диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77077286232",
		],
	],
	[
		"clientID" =>"CRM060953",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "samoilov.a@mail.ru",
			"mobile" => "+77772419903",
		],
	],
	[
		"clientID" =>"CRM060988",
		"name" =>"Дамир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "dksan_@mail.ru",
			"mobile" => "+77012704608",
		],
	],
	[
		"clientID" =>"CRM060992",
		"name" =>"Ананько Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "anankoaleksey@gmail.com",
			"mobile" => "+77026882499",
		],
	],
	[
		"clientID" =>"CRM061019",
		"name" =>"Жумагали",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017290662",
		],
	],
	[
		"clientID" =>"CRM061023",
		"name" =>"Инна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "exzorcist666@mail.ru",
			"mobile" => "+77772085176",
		],
	],
	[
		"clientID" =>"CRM053537",
		"name" => "Мейирман (лексус)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77007070404",
		],
		"TCs" => [
			[
				"brand" => "Lexus300",
				"size" => "235/55/18",
				"gosNomer" => "673CZA/02",
			],
		],
	],
	[
		"clientID" =>"CRM048127",
		"name" => "Максим (ниссан)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077888998",
		],
		"TCs" => [
			[
				"brand" => "Nissan",
				"size" => "215/55/17",
				"gosNomer" => "499VYA/02",
			],
		],
	],
	[
		"clientID" =>"CRM032391",
		"name" =>"Виктор",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772370067",
		],
		"TCs" => [
			[
				"brand" => "Mercedes 4*4",
				"size" => "295/40/21",
				"gosNomer" => "777BV/02",
			],
			[
				"brand" => "Skoda",
				"size" => "205/55/16",
				"gosNomer" => "552BR02",
			],
		],
	],
	[
		"clientID" =>"CRM057309",
		"name" =>"Стас Toyota Land Cruiser",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77012281212",
			"mobile" => "+77012281212",
		],
		"TCs" => [
			[
				"brand" => "Toyota Land Cruiser 100",
				"size" => "275/65/17",
				"gosNomer" => "A099KEO",
			],
		],
	],
	[
		"clientID" =>"CRM048589",
		"name" =>"225/45/17",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM048590",
		"name" =>"225/45/17",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM048595",
		"name" =>"Ким  Олег  Георгиевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017550529",
		],
		"TCs" => [
			[
				"brand" => "Lexus",
				"size" => "235/55/18",
				"gosNomer" => "A772WUO",
			],
		],
	],
	[
		"clientID" =>"CRM048631",
		"name" =>"Коломийцев Вячеслав Валерьевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77770154406",
		],
	],
	[
		"clientID" =>"CRM048688",
		"name" =>"Виктор",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017126904",
		],
	],
	[
		"clientID" =>"CRM048713",
		"name" =>"Беисова Ажар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77028498054",
		],
	],
	[
		"clientID" =>"CRM048767",
		"name" =>"Чукмаитов  Дулат  Сламбекович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013297444",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiPajero",
				"size" => "265/70/16",
				"gosNomer" => "A757HEO",
			],
		],
	],
	[
		"clientID" =>"CRM048771",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772290111",
		],
	],
	[
		"clientID" =>"CRM048797",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775951028",
		],
	],
	[
		"clientID" =>"CRM048816",
		"name" =>"Евгений",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77759686012",
			"mobile" => "+77759686012",
		],
	],
	[
		"clientID" =>"CRM053182",
		"name" =>"Берик  Азия газ",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77078205120",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHiace",
				"size" => "215/70/15C",
				"gosNomer" => "A412FD",
			],
		],
	],
	[
		"clientID" =>"CRM048883",
		"name" => "Диас  (мерседес) ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77001000605",
		],
		"TCs" => [
			[
				"brand" => "Mercedes",
				"size" => "245/45/19",
				"gosNomer" => "550RR/02",
			],
		],
	],
	[
		"clientID" =>"CRM053784",
		"name" => "Айнаш  (Тойота королла)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017318101",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCorolla",
				"size" => "205/55/16",
				"gosNomer" => "381BXA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053804",
		"name" =>"Азамат Перно Рикар",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77017417856",
		],
		"TCs" => [
			[
				"brand" => "Renault",
				"size" => "185/70/14",
				"gosNomer" => "H764124",
			],
		],
	],
	[
		"clientID" =>"CRM053143",
		"name" =>"Жексенбыев Дархан Жексенбыулы",
		"attribute" => "Физ. лицо",
		"IIN" => "840217300847",
		"contacts" => [
			"mobile" => "+77785278390",
		],
	],
	[
		"clientID" =>"CRM053160",
		"name" =>"Айнабеков Береке Нуртасович",
		"attribute" => "Физ. лицо",
		"IIN" => "870710301474",
		"contacts" => [
			"mobile" => "+77016209020",
		],
	],
	[
		"clientID" =>"CRM053149",
		"name" =>"Сабыр Кабулович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017430614",
		],
	],
	[
		"clientID" =>"CRM053174",
		"name" =>"Перевертайло Максим Юрьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "921230350091",
		"contacts" => [
			"mobile" => "+77476561827",
		],
	],
	[
		"clientID" =>"CRM053175",
		"name" =>"Калиев Бакарыс Мухамбетович",
		"attribute" => "Физ. лицо",
		"IIN" => "880810300917",
		"contacts" => [
			"mobile" => "+77073712078",
		],
	],
	[
		"clientID" =>"CRM056098",
		"name" =>"Головнева любовь Сергеевна",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77011084264",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "195/60/14",
				"gosNomer" => "764KAA/02",
			],
		],
	],
	[
		"clientID" =>"CRM056146",
		"name" =>"Талгат 185/65/14",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056147",
		"name" =>"Талгат 185/65/14",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM056156",
		"name" =>"Жангалиев Бахытжан Жасуланович",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77089494009",
			"adress" => "911027300169",
		],
		"TCs" => [
			[
				"brand" => "УАЗ Патриот",
				"size" => "245/70/16",
				"gosNomer" => "006ZAV",
			],
		],
	],
	[
		"clientID" =>"CRM056139",
		"name" =>"Али СВП",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772288828",
		],
	],
	[
		"clientID" =>"CRM056176",
		"name" =>"Нурбол Матиз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77079421076",
		],
	],
	[
		"clientID" =>"CRM056168",
		"name" =>"Михаил СВП",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072252131",
		],
	],
	[
		"clientID" =>"CRM056179",
		"name" =>"ТОО Complex supply of materials and servise",
		"attribute" => "Физ. лицо",
		"IIN" => "140140017997",
		"contacts" => [
			"mobile" => "+77772429338",
		],
	],
	[
		"clientID" =>"CRM056160",
		"name" =>"Гопаков Артём",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714126779",
		],
	],
	[
		"clientID" =>"CRM056194",
		"name" =>"Представительство акционерного общества Дельта Медикал Промоушин АГ (Швецария)",
		"attribute" => "Физ. лицо",
		"IIN" => "121242004949",
		"contacts" => [
			"mobile" => "+77017267554",
		],
	],
	[
		"clientID" =>"CRM056218",
		"name" =>"Данияр сату",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "danik28@mail.ru",
			"mobile" => "+77023333389",
		],
	],
	[
		"clientID" =>"CRM059448",
		"name" =>"Копжанова Ольга Габитовна",
		"attribute" => "Физ. лицо",
		"IIN" => "800630401691",
		"contacts" => [
			"mobile" =>"+77273729241",
			"mobile" => "+77019524370",
		],
		"TCs" => [
			[
				"brand" => "Нива",
				"size" => "185/70/16",
				"gosNomer" => "A960HU",
			],
		],
	],
	[
		"clientID" =>"CRM059451",
		"name" =>"Вадим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vadim.dreev@mail.ru",
			"mobile" => "+77717722668",
		],
	],
	[
		"clientID" =>"CRM059460",
		"name" =>"Немцов Владимир Иванович",
		"attribute" => "Физ. лицо",
		"IIN" => "671109350406",
		"contacts" => [
			"E-Mail" => "5238744@mail.ru",
			"mobile" => "+77755061015",
			"adress" => "Карагандинская облсть, г Шахтинск, ул Парковая 11А, кв 13",
		],
	],
	[
		"clientID" =>"CRM059463",
		"name" =>"Александр Масло",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273904334",
			"mobile" => "+77770211111",
		],
	],
	[
		"clientID" =>"CRM059468",
		"name" =>"дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "s_dv1978@mail.ru",
			"mobile" => "+77711315920",
		],
	],
	[
		"clientID" =>"CRM059471",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272270428",
		],
	],
	[
		"clientID" =>"CRM059472",
		"name" =>"Владимир 265/70 R16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777337177",
		],
	],
	[
		"clientID" =>"CRM059473",
		"name" =>"Зуфар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zyfar611@mail.ru",
			"mobile" => "+77072418084",
		],
	],
	[
		"clientID" =>"CRM059474",
		"name" =>"Исаева Светлана Викторовна",
		"attribute" => "Физ. лицо",
		"IIN" => "750326402764",
		"contacts" => [
			"mobile" => "+77776024307",
			"adress" => "Шымкент",
		],
	],
	[
		"clientID" =>"CRM059475",
		"name" =>"Берик Атырау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77750009307",
		],
	],
	[
		"clientID" =>"CRM059476",
		"name" =>"Марлен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072336047",
		],
	],
	[
		"clientID" =>"CRM059492",
		"name" =>"Александр Мишлен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073878874",
		],
	],
	[
		"clientID" =>"CRM059513",
		"name" =>"Айдар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "aidoha-93@mail.ru",
			"mobile" => "+77025017040",
		],
	],
	[
		"clientID" =>"CRM059507",
		"name" =>"Роман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "r.martynov.86@gmail.com",
			"mobile" => "+77057003024",
		],
	],
	[
		"clientID" =>"CRM059511",
		"name" =>"Бедняк Вячеслав Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "910410300326",
		"contacts" => [
			"mobile" => "+77078108526",
		],
	],
	[
		"clientID" =>"CRM059516",
		"name" =>"Махмуд",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024828333",
		],
	],
	[
		"clientID" =>"CRM059522",
		"name" =>"Ясанин Михаил Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760202301826",
		"contacts" => [
			"mobile" => "+77757475780",
		],
	],
	[
		"clientID" =>"CRM059525",
		"name" =>"Ли Виктор Павлович",
		"attribute" => "Физ. лицо",
		"IIN" => "790909300837",
		"contacts" => [
			"mobile" => "+77772252231",
		],
	],
	[
		"clientID" =>"CRM059546",
		"name" =>"Эрик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011000446",
		],
	],
	[
		"clientID" =>"CRM059548",
		"name" =>"Марина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "marinavoronova15@gmail.com",
			"mobile" => "+77027655348",
		],
	],
	[
		"clientID" =>"CRM059551",
		"name" =>"Кан Евгений Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "550201300968",
		"contacts" => [
			"mobile" => "+77471836040",
		],
	],
	[
		"clientID" =>"CRM059557",
		"name" =>"орал",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77022797266",
		],
	],
	[
		"clientID" =>"CRM059562",
		"name" =>"Алмат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77475581545",
		],
	],
	[
		"clientID" =>"CRM059565",
		"name" =>"Карекенов Асан Серикович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "asansholpan@mail.ru",
			"mobile" => "+77785455529",
			"adress" => "Уральск, Курмангазы 175 кв 16",
		],
	],
	[
		"clientID" =>"CRM059564",
		"name" =>"Александр Захаров",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272742079",
			"mobile" => "+77772488767",
		],
	],
	[
		"clientID" =>"CRM059575",
		"name" =>"Балабек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077101773",
		],
	],
	[
		"clientID" =>"CRM059579",
		"name" =>"Ернар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078899262",
		],
	],
	[
		"clientID" =>"CRM059581",
		"name" =>"Жанат инфинити",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012230128",
		],
	],
	[
		"clientID" =>"CRM059582",
		"name" =>"Ельнур Прадо",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77026702223",
		],
	],
	[
		"clientID" =>"CRM059588",
		"name" =>"Денис Монтеро",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017626787",
		],
	],
	[
		"clientID" =>"CRM059597",
		"name" =>"Сергей Нива МТ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777486512",
		],
	],
	[
		"clientID" =>"CRM059605",
		"name" =>"КайратДуйсеков",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kairat2210@mail.ru",
			"mobile" => "+77073703730",
		],
	],
	[
		"clientID" =>"CRM059606",
		"name" =>"Strat Nicolai",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "nicolai_strat@mail.ru",
			"mobile" =>"+6909802",
			"mobile" => "+6909802",
			"adress" => "Молдова	+690(9)802",
		],
	],
	[
		"clientID" =>"CRM059607",
		"name" =>"Mers",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM059620",
		"name" =>"Азама 195/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773303096",
		],
	],
	[
		"clientID" =>"CRM059622",
		"name" =>"Коваль Данил Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "741024350360",
		"contacts" => [
			"E-Mail" => "dane4ka44ts@gmail.com",
			"mobile" => "+77081094163",
		],
	],
	[
		"clientID" =>"CRM059624",
		"name" =>"Кунесбаев Серик Байдельханович",
		"attribute" => "Физ. лицо",
		"IIN" => "740510300224",
		"contacts" => [
			"mobile" => "+77473254550",
		],
	],
	[
		"clientID" =>"CRM059631",
		"name" =>"Сергей СВП",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078866549",
		],
	],
	[
		"clientID" =>"CRM059629",
		"name" =>"Финенко Максим Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "810905300891",
		"contacts" => [
			"mobile" =>"+77083374757",
		],
	],
	[
		"clientID" =>"CRM059632",
		"name" =>"Бисембаев Рустам",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77781777383",
		],
	],
	[
		"clientID" =>"CRM059638",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272540447",
		],
	],
	[
		"clientID" =>"CRM059649",
		"name" =>"Безруков Андрей Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "830502301218",
		"contacts" => [
			"mobile" => "+77015154420",
		],
	],
	[
		"clientID" =>"CRM059652",
		"name" =>"Ербол",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77757727701",
		],
	],
	[
		"clientID" =>"CRM059662",
		"name" =>"Корнилов Александр Вячеславович",
		"attribute" => "Физ. лицо",
		"IIN" => "791022302606",
		"contacts" => [
			"mobile" => "+77072255142",
		],
	],
	[
		"clientID" =>"CRM059663",
		"name" =>"Василий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273755762",
		],
	],
	[
		"clientID" =>"CRM059672",
		"name" =>"ИП MGM COMPANY",
		"attribute" => "Физ. лицо",
		"IIN" => "580527401425",
		"contacts" => [
			"mobile" => "+77088987770",
		],
	],
	[
		"clientID" =>"CRM059691",
		"name" =>"Фархат Тараз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772378719",
		],
	],
	[
		"clientID" =>"CRM059694",
		"name" =>"Ольга Ниссан Нот Р15?",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272630612",
		],
	],
	[
		"clientID" =>"CRM059704",
		"name" =>"ТОО РСУ и К",
		"attribute" => "Физ. лицо",
		"IIN" => "010640000687",
		"contacts" => [
			"E-Mail" => "rsu.ukg@gmail.com",
			"mobile" => "+77772151312",
			"adress" => "Усть-Каменогорск, Промышленная 1, 070000",
		],
	],
	[
		"clientID" =>"CRM059706",
		"name" =>"Виктор 225/70/16",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77078184061",
		],
	],
	[
		"clientID" =>"CRM059712",
		"name" =>"Миримжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77478649013",
		],
	],
	[
		"clientID" =>"CRM059707",
		"name" =>"Денис",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kotenev@gmail.com",
			"mobile" => "+77012727050",
		],
	],
	[
		"clientID" =>"CRM059709",
		"name" =>"Сарванова Наталья Викторовна",
		"attribute" => "Физ. лицо",
		"IIN" => "581226400611",
		"contacts" => [
			"mobile" =>"+77077227978",
			"mobile" => "+77017227978",
			"adress" => "Гоголя 87 Б уг Панфилова кв 10",
		],
	],
	[
		"clientID" =>"CRM059715",
		"name" =>"Евгений 195/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77714435680",
		],
	],
	[
		"clientID" =>"CRM059717",
		"name" =>"77778764747.",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778764747",
		],
	],
	[
		"clientID" =>"CRM059732",
		"name" =>"Тимур УК",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "timur.itn@gmail.com",
			"mobile" => "+77053188247",
		],
	],
	[
		"clientID" =>"CRM059722",
		"name" =>"ТОО “Iluka Exploration (Kazakhstan)” (“Илюка Эксплорейшн (Казахстан)”)",
		"attribute" => "Физ. лицо",
		"IIN" => "150540024331",
		"contacts" => [
			"E-Mail" => "yuliya.mironova@iluka.com",
			"mobile" =>"+77773435625",
			"mobile" => "+77273356593",
			"adress" => "г.Алматы, 050059, пр. Аль-Фараби 13, б.ц. Нурлы Тау, блок 2В, 8 этаж, офис 805.",
		],
	],
	[
		"clientID" =>"CRM059744",
		"name" =>"Муса",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015505572",
		],
	],
	[
		"clientID" =>"CRM059746",
		"name" =>"Виктро 205",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Ygab-ivs@MAIL.RU",
			"mobile" => "+77772123634",
		],
	],
	[
		"clientID" =>"CRM059748",
		"name" =>"ВАЗ диски Торос",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024480777",
		],
	],
	[
		"clientID" =>"CRM059766",
		"name" =>"Ним Ярослав Илларионович",
		"attribute" => "Физ. лицо",
		"IIN" => "680613301596",
		"contacts" => [
			"mobile" => "+77761952777",
		],
	],
	[
		"clientID" =>"CRM059759",
		"name" =>"Василий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77775870100",
		],
	],
	[
		"clientID" =>"CRM059770",
		"name" =>"Анатолий актау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Aktau_1957@mail.ru",
			"mobile" => "+77012465766",
		],
	],
	[
		"clientID" =>"CRM059806",
		"name" =>"Евгений 235/75/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77773200356",
		],
	],
	[
		"clientID" =>"CRM059845",
		"name" =>"Алексей Тиана",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077630368",
		],
	],
	[
		"clientID" =>"CRM059851",
		"name" =>"Ордас Камри",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013761989",
		],
	],
	[
		"clientID" =>"CRM059842",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77011114274",
		],
	],
	[
		"clientID" =>"CRM059856",
		"name" =>"Даурен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "botakanov@inbox.ru",
			"mobile" => "+77470849522",
			"adress" => "Усть-Каменогорск",
		],
	],
	[
		"clientID" =>"CRM059859",
		"name" =>"Алибек Туарег",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011113786",
		],
	],
	[
		"clientID" =>"CRM059860",
		"name" =>"Иван Караганда опт",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014259609",
		],
	],
	[
		"clientID" =>"CRM059891",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075556866",
		],
	],
	[
		"clientID" =>"CRM059894",
		"name" =>"Сузуки 92",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM052867",
		"name" => "Павел (тойота без госномера)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017289936",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHilux",
				"size" => "265/65/17",
				"gosNomer" => "F16202/02",
			],
		],
	],
	[
		"clientID" =>"CRM051119",
		"name" =>"Дугашев Руслан Елдашевич",
		"attribute" => "Физ. лицо",
		"IIN" => "810316301164",
		"contacts" => [
			"mobile" =>"+77272778489",
			"mobile" => "+77017471117",
		],
		"TCs" => [
			[
				"brand" => "MB",
				"size" => "245/45/17",
				"gosNomer" => "044 BTA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051124",
		"name" =>"Кириллова Наталья Глебовна",
		"attribute" => "Физ. лицо",
		"IIN" => "590701403230",
		"contacts" => [
			"mobile" =>"+77273788408",
			"mobile" => "+77772341919",
		],
	],
	[
		"clientID" =>"CRM051127",
		"name" =>"Акжол Сардар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77088990000",
		],
	],
	[
		"clientID" =>"CRM051184",
		"name" =>"Клиент не определен",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77273302013",
		],
	],
	[
		"clientID" =>"CRM051187",
		"name" =>"Альмешев Ермек Акимович",
		"attribute" => "Физ. лицо",
		"IIN" => "590112302234",
		"contacts" => [
			"mobile" => "+77051304132",
		],
	],
	[
		"clientID" =>"CRM051198",
		"name" =>"ТОО «НПЦ «Геокен»",
		"attribute" => "Физ. лицо",
		"IIN" => "970840000425",
	],
	[
		"clientID" =>"CRM051164",
		"name" =>"Смагулов Диас Сейфуллаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860606300394",
		"contacts" => [
			"mobile" => "+77000009876",
		],
	],
	[
		"clientID" =>"CRM051234",
		"name" =>"Умарбеков Адлет Адылжанулы",
		"attribute" => "Физ. лицо",
		"IIN" => "911223300442",
	],
	[
		"clientID" =>"CRM051240",
		"name" =>"Исаев Жанибек Ныгызбулы",
		"attribute" => "Физ. лицо",
		"IIN" => "930821300146",
		"contacts" => [
			"mobile" => "+77475558486",
		],
	],
	[
		"clientID" =>"CRM051219",
		"name" =>"Роман Костанай",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77757312579",
		],
	],
	[
		"clientID" =>"CRM051224",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272742919",
		],
	],
	[
		"clientID" =>"CRM051270",
		"name" =>"Иван",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073599996",
		],
	],
	[
		"clientID" =>"CRM051282",
		"name" =>"Жаксыбаев  Бекзат  Серикулы",
		"attribute" => "Физ. лицо",
		"IIN" => "920328301549",
		"contacts" => [
			"E-Mail" => "zhand_89.b@mail.ru",
			"mobile" => "+77076786166",
		],
		"TCs" => [
			[
				"brand" => "CHEVROLET",
				"size" => "205/55/16",
				"gosNomer" => "019 LNA/05",
			],
		],
	],
	[
		"clientID" =>"CRM051277",
		"name" =>"Ефимова  Алина  Александровна",
		"attribute" => "Физ. лицо",
		"IIN" => "750331401832",
		"contacts" => [
			"mobile" => "+77772679283",
		],
		"TCs" => [
			[
				"brand" => "ToyotaRAV-4",
				"size" => "215/70/16",
				"gosNomer" => "974DCA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051293",
		"name" =>"Джанайдарова Толкын Сагантаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "730328400705",
		"contacts" => [
			
			"mobile" => "+77770177888",
		],
	],
	[
		"clientID" =>"CRM051284",
		"name" =>"Скаков Бакытбек Тукушевич",
		"attribute" => "Физ. лицо",
		"IIN" => "670701301453",
		"contacts" => [
			"mobile" => "+77025886176",
		],
	],
	[
		"clientID" =>"CRM051298",
		"name" => "Роман  (KIA 213)",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77771670077",
		],
		"TCs" => [
			[
				"brand" => "KIA",
				"size" => "195/65/15",
				"gosNomer" => "213AK/02",
			],
		],
	],
	[
		"clientID" =>"CRM051300",
		"name" =>"Чондыбаев  Ермек  Сарсенбаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "601009300077",
		"contacts" => [
			"mobile" => "+77078358721",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "205/65/16",
				"gosNomer" => "181SCA/05",
			],
		],
	],
	[
		"clientID" =>"CRM051338",
		"name" =>"Кадиркулов Даулет Мукашевич",
		"attribute" => "Физ. лицо",
		"IIN" => "810111300845",
		"contacts" => [
			"mobile" => "+77051103782",
		],
	],
	[
		"clientID" =>"CRM051357",
		"name" =>"Ерлан МAXXIS SS01",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019845026",
		],
	],
	[
		"clientID" =>"CRM051334",
		"name" =>"Садырбеков Максат Талапович",
		"attribute" => "Физ. лицо",
		"IIN" => "911102302063",
		"contacts" => [
			"mobile" => "+77071918291",
		],
	],
	[
		"clientID" =>"CRM051345",
		"name" =>"Силков Игорь Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "690315301766",
		"contacts" => [
			"mobile" => "+77778129023",
		],
	],
	[
		"clientID" =>"CRM051337",
		"name" =>"Мусатаев Ануар (митсубиши)",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77775148573",
		],
		"TCs" => [
			[
				"brand" => "MitsubishiASX",
				"size" => "215/60/17",
				"gosNomer" => "576OPA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051356",
		"name" =>"Ляля",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051792767",
		],
		"TCs" => [
			[
				"brand" => "Mercedes",
				"gosNomer" => "221",
			],
		],
	],
	[
		"clientID" =>"CRM051358",
		"name" =>"Сузуки",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778909292",
		],
	],
	[
		"clientID" =>"CRM051360",
		"name" =>"Елена",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015557678",
		],
	],
	[
		"clientID" =>"CRM051367",
		"name" =>"Айдын",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075550021",
		],
	],
	[
		"clientID" =>"CRM051374",
		"name" =>"Кабдрахманов Ербол (КИА)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77088208871",
		],
		"TCs" => [
			[
				"brand" => "KIASportage",
				"size" => "225/65/17",
				"gosNomer" => "867FVA/05",
			],
		],
	],
	[
		"clientID" =>"CRM051385",
		"name" =>"вапар",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM051469",
		"name" =>"Уанов Рустем Рамазанович",
		"attribute" => "Физ. лицо",
		"IIN" => "850915301354",
		"contacts" => [
			"mobile" => "+77074540371",
		],
	],
	[
		"clientID" =>"CRM051445",
		"name" =>"Бейсембаев  Нуржан  Акимович",
		"attribute" => "Физ. лицо",
		"IIN" => "550426300139",
		"contacts" => [
			"mobile" =>"+77051845690",
			"mobile" => "+77017869212",
		],
		"TCs" => [
			[
				"brand" => "SuzukiVitara",
				"size" => "225/65/17",
				"gosNomer" => "084DRA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051458",
		"name" =>"Чернышев Иван Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "740726301643",
		"contacts" => [
			"mobile" => "+77770216101",
		],
	],
	[
		"clientID" =>"CRM051470",
		"name" =>"Кубарёв Александр Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "730816301908",
		"contacts" => [
			"mobile" => "+77012102994",
		],
	],
	[
		"clientID" =>"CRM051486",
		"name" =>"Александр Кевлар 18",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773254444",
		],
	],
	[
		"clientID" =>"CRM051547",
		"name" =>"Евгений бридж",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77772741800",
		],
	],
	[
		"clientID" =>"CRM051515",
		"name" =>"Ернур",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024503329",
		],
	],
	[
		"clientID" =>"CRM051520",
		"name" =>"Арман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77086169444",
		],
	],
	[
		"clientID" =>"CRM051523",
		"name" =>"Мартынов   Андрей  Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "880109900024",
		"contacts" => [
			"mobile" => "+77079934299",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "205/55/16",
				"gosNomer" => "725AOA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051533",
		"name" =>"Ринат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015721673",
		],
	],
	[
		"clientID" =>"CRM051574",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77082208866",
		],
	],
	[
		"clientID" =>"CRM051582",
		"name" =>"Кадыр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019991456",
		],
	],
	[
		"clientID" =>"CRM051587",
		"name" => "Эльбар (Тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "tokshambayev@mail.ru",
			"mobile" =>"+77777464444",
			"mobile" => "+77777464444",
			"adress" => "граница Алматы и Алматинской области",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "265/60/18",
				"gosNomer" => "660BL/02",
			],
		],
		/*
			<subscription>
				<nomer>0856</nomer>
				<date>23.11.2016 0:00:00</date>
				<srok>22.03.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"CRM051607",
		"name" =>"Антонов Павел Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "930331301205",
		"contacts" => [
			"E-Mail" => "antyan_ass@mail.ru",
			"mobile" =>"+77010144232",
		],
	],
	[
		"clientID" =>"CRM051618",
		"name" =>"Булат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273306919",
		],
	],
	[
		"clientID" =>"CRM051643",
		"name" =>"дана",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM051585",
		"name" =>"Александр",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77751299379",
		],
	],
	[
		"clientID" =>"CRM051592",
		"name" =>"Белокопытов Дмитрий Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "870801302407",
		"contacts" => [
			"mobile" => "+77079935550",
		],
	],
	[
		"clientID" =>"CRM051612",
		"name" =>"Тулебаев  Елжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "tulebaev28@mail.ru",
			"mobile" =>"+77055550108",
			"mobile" => "+77055550108",
		],
		"TCs" => [
			[
				"brand" => "LandCruiser",
				"size" => "285/50/20",
				"gosNomer" => "007TZA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051620",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77774887632",
		],
	],
	[
		"clientID" =>"CRM051572",
		"name" =>"195/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77021596002",
		],
	],
	[
		"clientID" =>"CRM051613",
		"name" =>"Вадим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051233000",
		],
	],
	[
		"clientID" =>"CRM051637",
		"name" =>"Джумабаева Наталья Гуляровна",
		"attribute" => "Физ. лицо",
		"IIN" => "760108401435",
		"contacts" => [
			"E-Mail" => " ",
			"mobile" => "+77051378892",
		],
	],
	[
		"clientID" =>"CRM051657",
		"name" =>"Казакова Ирина Васильевна",
		"attribute" => "Физ. лицо",
		"IIN" => "550620402502",
		"contacts" => [
			"mobile" => "+77028405080",
		],
	],
	[
		"clientID" =>"CRM051697",
		"name" =>"Каупымбаев  Ерзат  Алиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "831006301244",
		"contacts" => [
			"mobile" => "+77082864244",
		],
		"TCs" => [
			[
				"brand" => "Skoda",
				"size" => "205/55/16",
				"gosNomer" => "497FOA/02",
			],
		],
	],
	[
		"clientID" =>"CRM051635",
		"name" =>"Макулбеков Данияр Анварбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "831102301860",
		"contacts" => [
			"mobile" => "+77078401384",
		],
	],
	[
		"clientID" =>"CRM051646",
		"name" =>"Бродовский Валерий Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "690220301412",
		"contacts" => [
			"mobile" => "+77073271155",
		],
	],
	[
		"clientID" =>"CRM051722",
		"name" =>"Сансызбай Ержан Кайырулы",
		"attribute" => "Физ. лицо",
		"IIN" => "970410300852",
		"contacts" => [
			"mobile" => "+77475347720",
		],
	],
	[
		"clientID" =>"CRM051669",
		"name" =>"Кутымбетова Динара Кулмаханова",
		"attribute" => "Физ. лицо",
		"IIN" => "881110400693",
	],
	[
		"clientID" =>"CRM051731",
		"name" =>"Бакеева Айтолкын Ектайевна",
		"attribute" => "Физ. лицо",
		"IIN" => "750525402311",
		"contacts" => [
			"mobile" => "+77473810857",
		],
	],
	[
		"clientID" =>"CRM051746",
		"name" =>"Габиден",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018790707",
		],
	],
	[
		"clientID" =>"CRM051756",
		"name" =>"Марина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77074443511",
		],
	],
	[
		"clientID" =>"CRM051747",
		"name" =>"Искаков Олжас Маликулы",
		"attribute" => "Физ. лицо",
		"IIN" => "900707302157",
		"contacts" => [
			"mobile" => "+77021360565",
		],
	],
	[
		"clientID" =>"CRM051752",
		"name" =>"Веселов Евгений Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "851204300860",
		"contacts" => [
			"mobile" => "+77717654126",
		],
	],
	[
		"clientID" =>"CRM051732",
		"name" =>"Александра 77751236852",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77751236852",
		],
	],
	[
		"clientID" =>"CRM051737",
		"name" =>"Айжан +77017363138",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017363138",
		],
	],
	[
		"clientID" =>"CRM051871",
		"name" =>"Ступаков Вадим Вячеславович",
		"attribute" => "Физ. лицо",
		"IIN" => "870221300041",
		"contacts" => [
			"mobile" => "+77779631155",
		],
	],
	[
		"clientID" =>"CRM051900",
		"name" =>"Табылбекова Салтанат Серикхановна",
		"attribute" => "Физ. лицо",
		"IIN" => "880602400378",
		"contacts" => [
			"mobile" => "+77071967999",
		],
	],
	[
		"clientID" =>"CRM051944",
		"name" =>"Александр235/55/19",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057180600",
		],
	],
	[
		"clientID" =>"CRM051983",
		"name" =>"Кирил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015388325",
		],
	],
	[
		"clientID" =>"CRM051959",
		"name" =>"Абдыманыш Алибек Аленулы",
		"attribute" => "Физ. лицо",
		"IIN" => "940814300912",
		"contacts" => [
			"E-Mail" => "Alibekdiamant@gmali.com",
			"mobile" =>"+77784182096",
		],
	],
	[
		"clientID" =>"CRM051972",
		"name" =>"Белых  Олег  Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "770314302161",
		"contacts" => [
			"mobile" => "+77773004224",
		],
		"TCs" => [
			[
				"brand" => "BMW",
				"size" => "225/55/16",
				"gosNomer" => "A823BOO",
			],
		],
	],
	[
		"clientID" =>"CRM051980",
		"name" =>"Анаров Ерлан Нурахметович",
		"attribute" => "Физ. лицо",
		"IIN" => "870927399019",
		"contacts" => [
			"mobile" => "+77781628787",
		],
	],
	[
		"clientID" =>"CRM052022",
		"name" =>"Расима",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77022396109",
		],
	],
	[
		"clientID" =>"CRM052023",
		"name" =>"Досыбаева  Сымбат   Рсалиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "760602402637",
		"contacts" => [
			"mobile" => "+77011937557",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi",
				"size" => "195/65/15",
				"gosNomer" => "263HBA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052181",
		"name" =>"Ябеков  Нурланбек  Мерекебекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "690422302667",
		"contacts" => [
			"mobile" => "+77013562025",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry040",
				"size" => "215/60/16",
				"gosNomer" => "A141XEN",
			],
		],
	],
	[
		"clientID" =>"CRM052189",
		"name" =>"Тимур",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77273870368",
			"mobile" => "+77017838985",
		],
	],
	[
		"clientID" =>"CRM052194",
		"name" =>"Бисекен Айтжан",
		"attribute" => "Физ. лицо",
		"IIN" => "850524400140",
		"contacts" => [
			"mobile" => "+77017846436",
		],
	],
	[
		"clientID" =>"CRM052204",
		"name" =>"Ирина",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77075825070",
		],
	],
	[
		"clientID" =>"CRM052276",
		"name" =>"Елена",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77772544429",
		],
	],
	[
		"clientID" =>"CRM052220",
		"name" =>"Духанин Вадим Тимурович",
		"attribute" => "Физ. лицо",
		"IIN" => "900109300125",
		"contacts" => [
			"mobile" => "+77075455514",
		],
	],
	[
		"clientID" =>"CRM052243",
		"name" =>"Буденова Людмила Кузьминична",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77059883273",
		],
	],
	[
		"clientID" =>"CRM052283",
		"name" =>"Айсулу",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778233888",
		],
	],
	[
		"clientID" =>"CRM052292",
		"name" =>"Нурсултан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77752237765",
		],
	],
	[
		"clientID" =>"CRM052293",
		"name" =>"Эльмира",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019082949",
		],
	],
	[
		"clientID" =>"CRM052335",
		"name" =>"Азизова Молдир Акилбековна",
		"attribute" => "Физ. лицо",
		"IIN" => "920827401210",
		"contacts" => [
			"mobile" => "+77015279967",
		],
	],
	[
		"clientID" =>"CRM052369",
		"name" =>"ИП ЗАНГАР",
		"attribute" => "Физ. лицо",
		"IIN" => "660502400626",
	],
	[
		"clientID" =>"CRM052371",
		"name" =>"ИП ЗАНГАР",
		"attribute" => "Физ. лицо",
		"IIN" => "660502400626",
		"contacts" => [
			"mobile" => "+77775708000",
		],
	],
	[
		"clientID" =>"CRM052287",
		"name" =>"Шамиль",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77077880655",
		],
	],
	[
		"clientID" =>"CRM052348",
		"name" =>"Любовь",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078363603",
		],
	],
	[
		"clientID" =>"CRM052289",
		"name" =>"Кемен 285/60/18",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77018611156",
		],
	],
	[
		"clientID" =>"CRM052319",
		"name" =>"Арман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772821653",
		],
	],
	[
		"clientID" =>"CRM052387",
		"name" =>"Бапиев Махсат Оралханович",
		"attribute" => "Физ. лицо",
		"IIN" => "810607300930",
		"contacts" => [
			"mobile" => "+77476405901",
		],
	],
	[
		"clientID" =>"CRM052358",
		"name" =>"Осташов Михаил Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "781224303135",
		"contacts" => [
			"E-Mail" => "ksenia_lbs@mail.ru",
			"mobile" =>"+77078916201",
			"mobile" => "+77078916201",
		],
	],
	[
		"clientID" =>"CRM052399",
		"name" =>"Кусимбеков Отабек",
		"attribute" => "Физ. лицо",
		"IIN" => "961126301025",
		"contacts" => [
			"mobile" => "+77755610141",
		],
	],
	[
		"clientID" =>"CRM052411",
		"name" =>"Тулегенова Алина Алиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "851002401490",
	],
	[
		"clientID" =>"CRM056369",
		"name" =>"Виталий грузовые шины",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77272480985",
		],
	],
	[
		"clientID" =>"CRM056377",
		"name" =>"Виктор 7714483500",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714483500",
		],
	],
	[
		"clientID" =>"CRM053807",
		"name" => "Акимжан (мерседес)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015658004",
		],
		"TCs" => [
			[
				"brand" => "MB222",
				"size" => "285/40/19",
				"gosNomer" => "021NYA",
			],
		],
	],
	[
		"clientID" =>"CRM052866",
		"name" =>"Темирлан  Art Light",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77015220546",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCamry",
				"size" => "215/55/17",
				"gosNomer" => "088AH/12",
			],
		],
	],
	[
		"clientID" =>"CRM053457",
		"name" => "Геннадий   (Тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051819928",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "185/70/14",
				"gosNomer" => "A869VEO",
			],
		],
	],
	[
		"clientID" =>"CRM057347",
		"name" =>"Ольга",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072002006",
		],
		"TCs" => [
			[
				"brand" => "Lexus es 300",
				"size" => "215/60/16",
				"gosNomer" => "A921KTA",
			],
		],
	],
	[
		"clientID" =>"CRM048838",
		"name" =>"Даурен",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77272300986",
		],
	],
	[
		"clientID" =>"CRM048843",
		"name" =>"Плотникова Ирина Геннадьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "590706400015",
		"contacts" => [
			"mobile" =>"+77471855007",
		],
	],
	[
		"clientID" =>"CRM048854",
		"name" =>"Ирина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051855007",
		],
	],
	[
		"clientID" =>"CRM048859",
		"name" =>"Жасулан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77755227788",
		],
	],
	[
		"clientID" =>"CRM048898",
		"name" =>"еуст от рингостат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77478115330",
		],
	],
	[
		"clientID" =>"CRM048934",
		"name" =>"Таджимуратов Тимур Бауржанович",
		"attribute" => "Физ. лицо",
		"IIN" => "860203300240",
		"contacts" => [
			"mobile" =>"+77019302342",
		],
	],
	[
		"clientID" =>"CRM048958",
		"name" =>"Тест Тестович",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+74785236985",
		],
	],
	[
		"clientID" =>"CRM048995",
		"name" =>"Шиян Иван Михайлович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77054600142",
		],
	],
	[
		"clientID" =>"CRM048991",
		"name" =>"Есербаева Айдана Калдыбеккызы",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77071330328",
		],
	],
	[
		"clientID" =>"CRM049016",
		"name" =>"Али",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" =>"+77470597819",
		],
	],
	[
		"clientID" =>"CRM049047",
		"name" =>"НИЯЗ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078120099",
		],
	],
	[
		"clientID" =>"CRM049056",
		"name" =>"215/60/16",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM049064",
		"name" =>"Уалиев Улан Советбекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "901031300367",
		"contacts" => [
			"mobile" => "+77718464593",
		],
	],
	[
		"clientID" =>"CRM049065",
		"name" =>"Умбетбаев Асхат Алибекович",
		"attribute" => "Физ. лицо",
		"IIN" => "891020302656",
		"contacts" => [
			"mobile" => "+77476243565",
		],
	],
	[
		"clientID" =>"CRM049112",
		"name" =>"Артур",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77057750735",
		],
	],
	[
		"clientID" =>"CRM049126",
		"name" =>"Нариман каспи рассрочка",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078387658",
		],
	],
	[
		"clientID" =>"CRM049141",
		"name" =>"Нургубулов Ербол Кадырович",
		"attribute" => "Физ. лицо",
		"IIN" => "890412300996",
		"contacts" => [
			"mobile" => "+77077928707",
		],
	],
	[
		"clientID" =>"CRM049143",
		"name" =>"Нургабулов Ербол Кадырович",
		"attribute" => "Физ. лицо",
		"IIN" => "890414300996",
	],
	[
		"clientID" =>"CRM049181",
		"name" =>"Укуманов Рустам",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM049194",
		"name" =>"Павел Галочкин Жезказган",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77477704056",
		],
	],
	[
		"clientID" =>"CRM049197",
		"name" =>"АЙДАНА +77019338587",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019338587",
		],
	],
	[
		"clientID" =>"CRM049237",
		"name" =>"КУАНЫШ",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77029115244",
		],
	],
	[
		"clientID" =>"CRM049259",
		"name" =>"Жунусбекова Анара Кайнаровна",
		"attribute" => "Физ. лицо",
		"IIN" => "771208400871",
		"contacts" => [
			"mobile" => "+77772584862",
		],
	],
	[
		"clientID" =>"CRM049316",
		"name" =>"Ярова Енлик Маратовна",
		"attribute" => "Физ. лицо",
		"IIN" => "820827400797",
		"contacts" => [
			"E-Mail" => "yana8227@mail.ru",
			"mobile" => "+77771216552",
		],
	],
	[
		"clientID" =>"CRM049322",
		"name" =>"Есымбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77779994008",
		],
	],
	[
		"clientID" =>"CRM049338",
		"name" =>"Бахадыр",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77018034757",
		],
	],
	[
		"clientID" =>"CRM049358",
		"name" =>"Бесенали Алдияр Мажитулы",
		"attribute" => "Физ. лицо",
		"IIN" => "941123300334",
		"contacts" => [
			"mobile" => "+77017702740",
		],
	],
	[
		"clientID" =>"CRM049375",
		"name" =>"Малик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017337524",
		],
	],
	[
		"clientID" =>"CRM049377",
		"name" =>"Елдос",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019878987",
		],
	],
	[
		"clientID" =>"CRM049404",
		"name" =>"Шарипов Бахтияр Рахимжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "860507301994",
		"contacts" => [
			"mobile" => "+77051111760",
		],
	],
	[
		"clientID" =>"CRM049386",
		"name" =>"Еркежан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77074635353",
		],
	],
	[
		"clientID" =>"CRM049391",
		"name" =>"Айдын",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272378049",
		],
	],
	[
		"clientID" =>"CRM049412",
		"name" =>"Амангайша",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77781490101",
		],
	],
	[
		"clientID" =>"CRM049464",
		"name" =>"Макаров Дмитрий Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "850629300244",
		"contacts" => [
			"mobile" => "+77017133288",
		],
	],
	[
		"clientID" =>"CRM049474",
		"name" =>"Раева Галина Васильевна",
		"attribute" => "Физ. лицо",
		"IIN" => "600516402350",
		"contacts" => [
			"mobile" => "+77014599520",
		],
	],
	[
		"clientID" =>"CRM049519",
		"name" =>"Гадабошев Махмед-Гирей Магомедович",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77017766111",
		],
	],
	[
		"clientID" =>"CRM049534",
		"name" =>"Азиз т 3111800",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
			"mobile" => "+77052226100",
		],
	],
	[
		"clientID" =>"CRM049535",
		"name" =>"Кубесов  Жанибек  Нуржанович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017440798",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "215/55/17",
				"gosNomer" => "638WOA/02",
			],
		],
	],
	[
		"clientID" =>"CRM049536",
		"name" =>"Омаров Аскар Муратбекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "920606600938",
		"contacts" => [
			"mobile" => "+77782195177",
		],
	],
	[
		"clientID" =>"CRM049556",
		"name" =>"ТОО «Тулпар-Тальго»",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019827304",
		],
	],
	[
		"clientID" =>"CRM049565",
		"name" =>"Алексндр 175/50 R15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "alex.biketov@mail.ru",
			"mobile" =>"+77028891764",
			"mobile" => "+77028891764",
		],
	],
	[
		"clientID" =>"CRM049611",
		"name" =>"Жанысжанов Мурат Куанышович",
		"attribute" => "Физ. лицо",
		"IIN" => "740114301408",
		"contacts" => [
			"mobile" => "+77077288784",
		],
	],
	[
		"clientID" =>"CRM049615",
		"name" =>"Аманжолов Елдес Канатбекулы",
		"attribute" => "Физ. лицо",
		"IIN" => "840828302647",
		"contacts" => [
			"mobile" => "+77015036387",
		],
	],
	[
		"clientID" =>"CRM049632",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77087639777",
		],
	],
	[
		"clientID" =>"CRM049694",
		"name" =>"Репало Антон Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "870823301101",
		"contacts" => [
			"mobile" => "+77773523887",
		],
	],
	[
		"clientID" =>"CRM049693",
		"name" =>"Виктор",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77059137789",
		],
	],
	[
		"clientID" =>"CRM049713",
		"name" =>"Ушаков Олег  Нестле",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" =>"+77017610487",
		],
		"TCs" => [
			[
				"brand" => "Skoda",
				"size" => "205/65/15",
				"gosNomer" => "H7056",
			],
		],
	],
	[
		"clientID" =>"CRM049775",
		"name" =>"Марал",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017970401",
		],
	],
	[
		"clientID" =>"CRM049740",
		"name" =>"Зайцев Павел Леонидович",
		"attribute" => "Физ. лицо",
		"IIN" => "941030300963",
		"contacts" => [
			"mobile" => "+77078092373",
		],
	],
	[
		"clientID" =>"CRM049758",
		"name" =>"Бекжанов Ерлан Каипжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "810101301709",
		"contacts" => [
			"mobile" => "+77476275016",
		],
	],
	[
		"clientID" =>"CRM049719",
		"name" =>"ТОО «FET-Group»",
		"attribute" => "Физ. лицо",
		"IIN" => "090740009470",
		"contacts" => [
			"mobile" => "+77017379486",
		],
	],
	[
		"clientID" =>"CRM049751",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77273756633",
		],
	],
	[
		"clientID" =>"CRM049782",
		"name" =>"Александр Актау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078091962",
		],
	],
	[
		"clientID" =>"CRM049786",
		"name" =>"Алдашев  Даурен  Нуралиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "800523301524",
		"contacts" => [
			"mobile" => "+77773856901",
		],
		"TCs" => [
			[
				"brand" => "AUDIA4",
				"size" => "225/55/16",
				"gosNomer" => "604XPA/02",
			],
		],
		/*
			<subscription>
				<nomer>1175</nomer>
				<date>14.11.2016 0:00:00</date>
				<srok>14.11.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"CRM049790",
		"name" =>"Тюлеков Акжол Исмагулович",
		"attribute" => "Физ. лицо",
		"IIN" => "640501304207",
		"contacts" => [
			"mobile" => "+77076337045",
		],
	],
	[
		"clientID" =>"CRM049814",
		"name" =>"Смаил Женис Орнекбайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "950429350046",
		"contacts" => [
			"mobile" =>"+77078170513",
			"mobile" => "+77078170513",
		],
	],
	[
		"clientID" =>"CRM049822",
		"name" => "Яна  (тойота РАФ4)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017138159",
			"mobile" => "+77017138159",
			"adress" => "3 мкр .18 б ",
		],
		"TCs" => [
			[
				"brand" => "ToyotaRAV-4",
				"size" => "215/65/16",
				"gosNomer" => "A828WSM",
			],
		],
	],
	[
		"clientID" =>"CRM049821",
		"name" =>"Коваль Дмитрий Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "741123302217",
		"contacts" => [
			"mobile" => "+77772934117",
		],
	],
	[
		"clientID" =>"CRM049828",
		"name" =>"Иманбаев Кайрат Советович",
		"attribute" => "Физ. лицо",
		"IIN" => "620622301356",
		"contacts" => [
			"mobile" => "+77017283829",
		],
	],
	[
		"clientID" =>"CRM049844",
		"name" =>"Серик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77077383025",
		],
	],
	[
		"clientID" =>"CRM049858",
		"name" =>"Супиев Алимжан Касымжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "841221301057",
		"contacts" => [
			"mobile" => "+77016047460",
		],
	],
	[
		"clientID" =>"CRM050008",
		"name" =>"Даурен",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM049869",
		"name" =>"Алишер Асимжанов",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078194621",
		],
	],
	[
		"clientID" =>"CRM049876",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77771299092",
		],
	],
	[
		"clientID" =>"CRM049865",
		"name" =>"Актоты",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77474515761",
		],
	],
	[
		"clientID" =>"CRM049900",
		"name" =>"Дауренбаев Дастан Шекаринович",
		"attribute" => "Физ. лицо",
		"IIN" => "840330301720",
		"contacts" => [
			"mobile" => "+77071177000",
			"adress" => "ул. Суюнбая 159, (уг. Рыскулова)",
		],
	],
	[
		"clientID" =>"CRM049958",
		"name" =>"Александр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075998630",
		],
	],
	[
		"clientID" =>"CRM050044",
		"name" =>"Юрий Мерседес Тюнинг",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017869916",
		],
	],
	[
		"clientID" =>"CRM049946",
		"name" =>"Лискин  Петр  Григорьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "520126301180",
		"contacts" => [
			"mobile" => "+77775847603",
		],
		"TCs" => [
			[
				"brand" => "AudaA8i",
				"size" => "265/40/20",
				"gosNomer" => "983HDA/02",
			],
		],
	],
	[
		"clientID" =>"CRM049921",
		"name" =>"Алмат",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77017946624",
		],
	],
	[
		"clientID" =>"CRM049926",
		"name" =>"Петр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77007777707",
		],
	],
	[
		"clientID" =>"CRM049947",
		"name" =>"Дмитрий    (Иванова Лариса)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273132012",
			"mobile" => "+77017588025",
		],
		"TCs" => [
			[
				"brand" => "AkuraMPX",
				"size" => "235/65/17",
				"gosNomer" => "A249TYO",
			],
		],
		/*
			<subscription>
				<nomer>0938</nomer>
				<date>01.01.0001 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"CRM050010",
		"name" =>"Максим",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM049992",
		"name" =>"Мирсанов  Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77027775558",
		],
		"TCs" => [
			[
				"brand" => "MercedesS250",
				"size" => "245/40/18",
				"gosNomer" => "888CD/02",
			],
		],
	],
	[
		"clientID" =>"CRM050003",
		"name" =>"Бычков Алесандр Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780316300306",
		"contacts" => [
			"mobile" => "+77772131383",
		],
	],
	[
		"clientID" =>"CRM050016",
		"name" =>"Елена",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM050019",
		"name" =>"Елена",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM050020",
		"name" =>"Елена",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM050018",
		"name" =>"Нурбол",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015664162",
		],
	],
	[
		"clientID" =>"CRM050066",
		"name" =>"Полина",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM050070",
		"name" =>"Турдакын Улан Кенесканулы",
		"attribute" => "Физ. лицо",
		"IIN" => "910905300555",
		"contacts" => [
			"mobile" => "+77470344354",
		],
	],
	[
		"clientID" =>"CRM050074",
		"name" =>"Савченко Антон Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "940123300123",
		"contacts" => [
			"mobile" => "+77477212492",
		],
	],
	[
		"clientID" =>"CRM050101",
		"name" =>"Галина грузовые шины",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77475337992",
		],
	],
	[
		"clientID" =>"CRM050095",
		"name" =>"Амраев Рабин Ахметжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "871106302868",
		"contacts" => [
			"mobile" => "+77019328041",
		],
		"TCs" => [
			[
				"brand" => "HyundaiElantra",
				"size" => "215/45/17",
				"gosNomer" => "208WXA/02",
			],
		],
	],
	[
		"clientID" =>"CRM050158",
		"name" =>"Ирина",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM050190",
		"name" =>"байсеитов Мухтар Тактасынович",
		"attribute" => "Физ. лицо",
		"IIN" => "650811300350",
	],
	[
		"clientID" =>"CRM050207",
		"name" =>"\"Спец Тех Сервис\"",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM050175",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015510109",
		],
	],
	[
		"clientID" =>"CRM050244",
		"name" =>"Мардан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017275228",
		],
	],
	[
		"clientID" =>"CRM050254",
		"name" =>"Кадравин Мурат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77014016695",
		],
	],
	[
		"clientID" =>"CRM050226",
		"name" =>"Абуталиев Адилбай",
		"attribute" => "Физ. лицо",
		"IIN" => "670205303017",
		"contacts" => [
			"mobile" => "+77058088372",
		],
	],
	[
		"clientID" =>"CRM050230",
		"name" =>"Жартуылов Канат Хамитович",
		"attribute" => "Физ. лицо",
		"IIN" => "700526302786",
		"contacts" => [
			"mobile" => "+77751399804",
		],
	],
	[
		"clientID" =>"CRM050283",
		"name" =>"Кульзада",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017345706",
		],
	],
	[
		"clientID" =>"CRM050224",
		"name" =>"Магмияв Ратбек Бекболатулы",
		"attribute" => "Физ. лицо",
		"IIN" => "940228300848",
		"contacts" => [
			"mobile" => "+77079192930",
		],
	],
	[
		"clientID" =>"CRM050253",
		"name" =>"Середа Роман Сергеевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "roman_sereda@mail.ru",
			"mobile" =>"+77073832034",
			"mobile" => "+77073832034",
		],
	],
	[
		"clientID" =>"CRM050262",
		"name" =>"Вахит",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77024122227",
		],
	],
	[
		"clientID" =>"CRM050294",
		"name" =>"Алим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015430208",
		],
	],
	[
		"clientID" =>"CRM050306",
		"name" =>"Андрей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772173555",
		],
	],
	[
		"clientID" =>"CRM050329",
		"name" =>"Тойгулов Болат Тахтарович",
		"attribute" => "Физ. лицо",
		"IIN" => "680419300632",
		"contacts" => [
			"mobile" => "+77773272819",
		],
	],
	[
		"clientID" =>"CRM050330",
		"name" =>"ТОО \"KYK GIYIM (КИК ГИЙИМ)\"",
		"attribute" => "Физ. лицо",
		"IIN" => "160640022747",
	],
	[
		"clientID" =>"CRM050332",
		"name" =>"Аскарулы Асхат",
		"attribute" => "Физ. лицо",
		"IIN" => "750806302427",
		"contacts" => [
			"mobile" =>"+77508063024",
			"mobile" =>"+77772757577",
		],
	],
	[
		"clientID" =>"CRM050309",
		"name" =>"Иван",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777440446",
		],
	],
	[
		"clientID" =>"CRM050325",
		"name" =>"Поолина 7771299176",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77711299176",
		],
	],
	[
		"clientID" =>"CRM050346",
		"name" =>"Серик Атырау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77122214290",
		],
	],
	[
		"clientID" =>"CRM050347",
		"name" =>"Майкотов Айдос Асылханович",
		"attribute" => "Физ. лицо",
		"IIN" => "890928301265",
		"contacts" => [
			"mobile" => "+77473997823",
		],
	],
	[
		"clientID" =>"CRM050377",
		"name" =>"Салимов Адилжан Касимжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "660620302659",
		"contacts" => [
			"mobile" => "+77028709995",
		],
	],
	[
		"clientID" =>"CRM050414",
		"name" =>"Евгения",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273911134",
		],
	],
	[
		"clientID" =>"CRM050426",
		"name" =>"Налимова Олеся Николаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "800330403092",
		"contacts" => [
			"mobile" => "+77076795661",
		],
	],
	[
		"clientID" =>"CRM050564",
		"name" =>"Фархат",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77072288671",
		],
	],
	[
		"clientID" =>"CRM050616",
		"name" =>"Глушко Александр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272344644",
			"mobile" => "+77010277411",
		],
	],
	[
		"clientID" =>"CRM050631",
		"name" =>"Ибрагимов Рашид Мирзабалаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "680811301641",
		"contacts" => [
			"mobile" => "+77077898745",
		],
	],
	[
		"clientID" =>"CRM050678",
		"name" =>"Сотниченко  Александр  Сергеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "890724300486",
		"contacts" => [
			"mobile" => "+77475900218",
		],
		"TCs" => [
			[
				"brand" => "VWGolf",
				"size" => "175/65/14",
				"gosNomer" => "B926EKO",
			],
		],
	],
	[
		"clientID" =>"CRM050682",
		"name" =>"Коваленко Юрий Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "791001303095",
		"contacts" => [
			"mobile" => "+77473230087",
		],
	],
	[
		"clientID" =>"CRM050767",
		"name" =>"Сатаев Руслан Дадилович",
		"attribute" => "Физ. лицо",
		"IIN" => "840912302772",
		"contacts" => [
			"mobile" => "+77021109066",
		],
	],
	[
		"clientID" =>"CRM050791",
		"name" =>"Абдуллаев Алымжан Аюпжанович",
		"attribute" => "Физ. лицо",
		"IIN" => "870830300885",
		"contacts" => [
			"mobile" => "+77071878444",
		],
	],
	[
		"clientID" =>"CRM050774",
		"name" =>"Бердалиева АсельТургынбаевна",
		"attribute" => "Физ. лицо",
		"IIN" => "861120402363",
		"contacts" => [
			"mobile" => "+77019637673",
		],
	],
	[
		"clientID" =>"CRM050794",
		"name" =>"Сабалиев  Зейнулла  Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "930605300855",
		"contacts" => [
			"mobile" => "+77075553585",
		],
		"TCs" => [
			[
				"brand" => "Audi",
				"size" => "195/65/15",
				"gosNomer" => "986VMA/02",
			],
		],
	],
	[
		"clientID" =>"CRM050797",
		"name" =>"Перно Рикар Казахстан ТОО",
		"attribute" => "Юр. лицо",
		"IIN" => "050840001045",
		"contacts" => [
			"mobile" => "+77017417893",
		],
		"TCs" => [
			[
				"brand" => "SubaruForester",
				"size" => "225/55/18",
				"gosNomer" => "H1764/02",
			],
			[
				"brand" => "SubaruForester",
				"size" => "225/55/18",
				"gosNomer" => "H1767/02",
			],
		],
	],
	[
		"clientID" =>"CRM050831",
		"name" =>"Кулумгариева  Алуа  Сакеновна",
		"attribute" => "Физ. лицо",
		"IIN" => "910424400091",
		"contacts" => [
			"mobile" =>"+77272247815",
			"mobile" => "+77789784595",
		],
		"TCs" => [
			[
				"brand" => "Nissan",
				"size" => "215/55/17",
				"gosNomer" => "439URA",
			],
		],
	],
	[
		"clientID" =>"CRM050875",
		"name" =>"Червонный  Михаил  Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "870410300158",
		"contacts" => [
			"mobile" =>"+77272530820",
		],
		"TCs" => [
			[
				"brand" => "Tranzit",
				"size" => "215/75/16",
				"gosNomer" => "720HNA/02",
			],
		],
	],
	[
		"clientID" =>"CRM050886",
		"name" =>"Бирюкова  Татьяна  Евгеньевна",
		"attribute" => "Физ. лицо",
		"IIN" => "690619400083",
		"contacts" => [
			"mobile" => "+77077510468",
		],
		"TCs" => [
			[
				"brand" => "MazdaTribute",
				"size" => "245/70/16",
				"gosNomer" => "A431EWP",
			],
		],
	],
	[
		"clientID" =>"CRM050923",
		"name" =>"Алимушкин Илья Дмитриевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77781919784",
		],
	],
	[
		"clientID" =>"CRM051017",
		"name" =>"Данияр S klass",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772443780",
		],
	],
	[
		"clientID" =>"CRM051061",
		"name" =>"Зейнолдина Багдагуль Кажыкеновна",
		"attribute" => "Физ. лицо",
		"IIN" => "701102402076",
		"contacts" => [
			"mobile" => "+77477675251",
		],
	],
	[
		"clientID" =>"CRM051035",
		"name" =>"Васильева Наталья Васильевна",
		"attribute" => "Физ. лицо",
		"IIN" => "780405400653",
	],
	[
		"clientID" =>"CRM051055",
		"name" =>"Полина",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
			"mobile" => "+77017055961",
			"adress" => "ул . Шатского 42",
		],
	],
	[
		"clientID" =>"CRM051064",
		"name" =>"Вера",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012115005",
		],
	],
	[
		"clientID" =>"CRM051067",
		"name" =>"Бекжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
			"mobile" => "+77011117982",
			"adress" => "Толебаева 175,уг ул Курмангазы",
		],
	],
	[
		"clientID" =>"CRM032386",
		"name" => "Александр  (на BMW и Hyundai)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77717792993",
		],
		"TCs" => [
			[
				"brand" => "HyundaiSantaFe",
				"size" => "235/60/18",
				"gosNomer" => "903EOA/02",
			],
			[
				"brand" => "BMWX5",
				"size" => "235/65/17",
				"gosNomer" => "A095AHO",
			],
		],
	],
	[
		"clientID" =>"CRM052588",
		"name" => "Гульнара  (БМВ)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013355443",
		],
		"TCs" => [
			[
				"brand" => "BMW",
				"gosNomer" => "312ANA/02",
			],
		],
	],
	[
		"clientID" =>"CRM031776",
		"name" =>"Курмангали",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77762191327",
		],
		"TCs" => [
			[
				"brand" => "Ягуар",
				"size" => "245/45/18",
				"gosNomer" => "055RRB/02",
			],
		],
	],
	[
		"clientID" =>"CRM058246",
		"name" =>"Самко Константин Антонович",
		"attribute" => "Физ. лицо",
		"IIN" => "911222300079",
		"contacts" => [
			"mobile" => "+77472221123",
		],
	],
	[
		"clientID" =>"CRM058270",
		"name" =>"Курамалиев Нурсултан Заханович",
		"attribute" => "Физ. лицо",
		"IIN" => "901222300642",
		"contacts" => [
			"E-Mail" => "nursultfn22.90@mail.ru",
			"mobile" => "+77476279463",
			"adress" => "Южно-Казахстанская область, Шымкент, сарыагаш , 160918",
		],
	],
	[
		"clientID" =>"CRM058282",
		"name" =>"Denis Vanchev",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "vandeni@mail.ru",
		],
	],
	[
		"clientID" =>"CRM058290",
		"name" =>"Токаев Олжас Мухтарович",
		"attribute" => "Физ. лицо",
		"IIN" => "890807300299",
		"contacts" => [
			"mobile" => "+77766710067",
		],
	],
	[
		"clientID" =>"CRM058291",
		"name" =>"Радионов Николай Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "740324301727",
		"contacts" => [
			"E-Mail" => "mikolka7474@mail.ru",
			"mobile" => "+77479366018",
		],
	],
	[
		"clientID" =>"CRM058294",
		"name" =>"Еламан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77758307077",
		],
	],
	[
		"clientID" =>"CRM058321",
		
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714669188",
		],
	],
	[
		"clientID" =>"CRM058333",
		"name" =>"Маханова Багила Маликовна",
		"attribute" => "Физ. лицо",
		"IIN" => "640912402589",
		"contacts" => [
			"mobile" => "+77028254107",
		],
	],
	[
		"clientID" =>"CRM058329",
		"name" =>"Максим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "maxim@mail.ru",
		],
	],
	[
		"clientID" =>"CRM058355",
		"name" =>"Сарсенбаев Ернар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078505070",
		],
	],
	[
		"clientID" =>"CRM058350",
		"name" =>"Хасен Асхат Бахыджанулы",
		"attribute" => "Физ. лицо",
		"IIN" => "860317301387",
		"contacts" => [
			"mobile" => "+77019824923",
		],
		"TCs" => [
			[
				"brand" => "ToyotaHighlander",
				"size" => "245/55/19",
				"gosNomer" => "484 ZXA 02",
			],
		],
	],
	[
		"clientID" =>"CRM058388",
		
		"attribute" => "Физ. лицо",
		"IIN" => "090440029619",
	],
	[
		"clientID" =>"CRM058390",
		"name" =>"Левицкий Евгений Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "800130301449",
		"contacts" => [
			"mobile" => "+77018947505",
		],
	],
	[
		"clientID" =>"CRM058395",
		"name" =>"Евгений кумхо",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77059160410",
		],
	],
	[
		"clientID" =>"CRM058401",
		"name" =>"Гохгалтер Игорь Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "710718300202",
		"contacts" => [
			"mobile" => "+77770023308",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "205/65/15",
				"gosNomer" => "872CVA/02",
			],
		],
	],
	[
		"clientID" =>"CRM058400",
		"name" =>"Митсубиси Диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017575716",
		],
	],
	[
		"clientID" =>"CRM058406",
		"name" =>"215/65/16C Корейские",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77476159056",
		],
	],
	[
		"clientID" =>"CRM058409",
		"name" =>"Черемнов Вадим Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "720725300557",
		"contacts" => [
			"mobile" => "+77056607055",
		],
	],
	[
		"clientID" =>"CRM058421",
		"name" =>"Абылайхан Дауренбек",
		"attribute" => "Физ. лицо",
		"IIN" => "790806303564",
		"contacts" => [
			"mobile" => "+77474030035",
		],
	],
	[
		"clientID" =>"CRM058418",
		"name" =>"Кузовкин Олег",
		"attribute" => "Физ. лицо",
		"IIN" => "680227302556",
		"contacts" => [
			"mobile" => "+77758462030",
		],
		"TCs" => [
			[
				"brand" => "Lexus",
				"size" => "285/60/18",
				"gosNomer" => "494ZUA/02",
			],
		],
	],
	[
		"clientID" =>"CRM058420",
		"name" =>"Ерболат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273449900",
			"mobile" => "+77273449900",
		],
	],
	[
		"clientID" =>"CRM058439",
		"name" =>"камеры",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77088087788",
		],
	],
	[
		"clientID" =>"CRM058444",
		"name" =>"Исмаилова Мерей Эриккызы",
		"attribute" => "Физ. лицо",
		"IIN" => "940728400535",
		"contacts" => [
			"mobile" => "+77022605332",
		],
	],
	[
		"clientID" =>"CRM058464",
		"name" =>"Тимченко Лилия Данияловна",
		"attribute" => "Физ. лицо",
		"IIN" => "700508401508",
		"contacts" => [
			"mobile" => "+77016475588",
		],
	],
	[
		"clientID" =>"CRM058445",
		"name" =>"Амирев Рустем",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057980767",
		],
	],
	[
		"clientID" =>"CRM058449",
		"name" =>"Заитов Александр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zaitov1987@gmail.com",
			"mobile" => "+77017301721",
		],
	],
	[
		"clientID" =>"CRM058458",
		"name" =>"Muratkhan Dauylbaev",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "dauyl.m.a@icloud.com",
		],
	],
	[
		"clientID" =>"CRM058460",
		"name" =>"Айдар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073014487",
		],
	],
	[
		"clientID" =>"CRM058465",
		"name" =>"Раимбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77070499006",
		],
	],
	[
		"clientID" =>"CRM058466",
		"name" =>"Адильханов Раимбек Ерланович",
		"attribute" => "Физ. лицо",
		"IIN" => "950831300283",
		"contacts" => [
			"mobile" => "+77070469006",
		],
	],
	[
		"clientID" =>"CRM058473",
		"name" =>"Максим",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057788539",
		],
	],
	[
		"clientID" =>"CRM058478",
		"name" =>"Барлыбеков Куанышбек Жумадилович",
		"attribute" => "Физ. лицо",
		"IIN" => "801111300698",
		"contacts" => [
			"mobile" => "+77017427067",
			"adress" => "Майлина 130, Нарильская на развязке ВАЗА вдоль дороги магазин КАЗ-АВТО Желтое здание",
		],
	],
	[
		"clientID" =>"CRM058488",
		"name" =>"Денис С Актобе",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "balgesheev@gmail.com",
			"mobile" => "+77770316501",
		],
	],
	[
		"clientID" =>"CRM058484",
		"name" =>"ТОО \"DLED\"",
		"attribute" => "Физ. лицо",
		"IIN" => "110140007300",
		"contacts" => [
			"mobile" => "+77023938700",
			"adress" => "РК г Алматы, пр. Аль-Фараби 30Б",
		],
	],
	[
		"clientID" =>"CRM058485",
		"name" =>"ТОО \"Импорт ЭксРесурс\"",
		"attribute" => "Физ. лицо",
		"IIN" => "121140009935",
		"contacts" => [
			"mobile" => "+77071090502",
			"adress" => "Богенбай батыра 89, уг Пушкина кв 47",
		],
	],
	[
		"clientID" =>"CRM058492",
		"name" =>"Яблонский Юрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273740626",
			"mobile" => "+77078824295",
		],
	],
	[
		"clientID" =>"CRM058493",
		"name" =>"Ержан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77007157777",
		],
	],
	[
		"clientID" =>"CRM058494",
		"name" =>"Дмитрий Зять Сан Саныча",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77055431591",
		],
	],
	[
		"clientID" =>"CRM058496",
		"name" =>"Жанибек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "zzflores98@gmail.com",
			"mobile" => "+77083331023",
		],
	],
	[
		"clientID" =>"CRM058508",
		"name" =>"Азамат Вольво",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77051512070",
		],
	],
	[
		"clientID" =>"CRM058509",
		"name" =>"Валерий GX 470",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778395315",
		],
	],
	[
		"clientID" =>"CRM058510",
		"name" =>"Виктория Актобе",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77024640708",
			"mobile" => "+77024266240",
		],
	],
	[
		"clientID" =>"CRM058507",
		"name" =>"МеломанHome Video ТОО /Валерий",
		"attribute" => "Физ. лицо",
		"IIN" => "040840001482",
		"contacts" => [
			"mobile" => "+77015169084",
		],
	],
	[
		"clientID" =>"CRM058512",
		"name" =>"Ильдус",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "ildus64@mail.ru",
			"mobile" => "+77024838064",
		],
	],
	[
		"clientID" =>"CRM058513",
		"name" =>"Дамир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "almaty-aspekt@mail.ru",
			"mobile" =>"+77022222135",
		],
	],
	[
		"clientID" =>"CRM058520",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77004512260",
		],
	],
	[
		"clientID" =>"CRM058521",
		"name" =>"Кайрат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77785278677",
		],
	],
	[
		"clientID" =>"CRM058522",
		"name" =>"Евгений",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078788067",
		],
	],
	[
		"clientID" =>"CRM058523",
		"name" =>"Куаныш",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075556874",
		],
	],
	[
		"clientID" =>"CRM058525",
		"name" =>"Александр Астана Грузовые",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013998427",
		],
	],
	[
		"clientID" =>"CRM058532",
		"name" =>"Василий 235/75/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273877871",
		],
	],
	[
		"clientID" =>"CRM058535",
		"name" =>"Дмитрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "Specialoneforever@mail.ru",
			"mobile" => "+77774113348",
		],
	],
	[
		"clientID" =>"CRM058518",
		"name" =>"Джумагулова Анаркуль Стамбековна",
		"attribute" => "Физ. лицо",
		"IIN" => "590428401558",
		"contacts" => [
			"mobile" => "+77759373984",
		],
	],
	[
		"clientID" =>"CRM058526",
		"name" =>"Дархан 5w30",
		"attribute" => "Физ. лицо",
		"IIN" => "dark-han@mai",
		"contacts" => [
			"mobile" => "+77074941416",
		],
	],
	[
		"clientID" =>"CRM058564",
		"name" =>"Владислав",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "hardvlad@gmail.com",
			"mobile" => "+77015359900",
		],
	],
	[
		"clientID" =>"CRM058577",
		"name" =>"Алексей Уаз Патриот",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017320993",
		],
	],
	[
		"clientID" =>"CRM058591",
		"name" =>"Сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "seregasss1991@mail.ru",
			"mobile" => "+77477906460",
		],
	],
	[
		"clientID" =>"CRM058599",
		"name" =>"Касымбеков Галымжан Алпысбайулы",
		"attribute" => "Физ. лицо",
		"IIN" => "771108302332",
		"contacts" => [
			"mobile" => "+77013887794",
		],
	],
	[
		"clientID" =>"CRM058603",
		"name" =>"Юрий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77717793949",
		],
	],
	[
		"clientID" =>"CRM058600",
		"name" =>"Марат диски титановые",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778098355",
		],
	],
	[
		"clientID" =>"CRM058604",
		"name" =>"Серик 185/70/14",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076026484",
		],
	],
	[
		"clientID" =>"CRM058610",
		"name" =>"Ердос",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "yerdos_adambekov@mail.ru",
			"mobile" => "+77015092288",
		],
	],
	[
		"clientID" =>"CRM058616",
		"name" =>"+77013898196",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77013898196",
		],
	],
	[
		"clientID" =>"CRM058629",
		"name" =>"Машинский Владимир Викторович",
		"attribute" => "Физ. лицо",
		"IIN" => "791014301476",
		"contacts" => [
			"mobile" => "+77772563664",
		],
	],
	[
		"clientID" =>"CRM058648",
		"name" =>"Асаубаев Руслан Болатович",
		"attribute" => "Физ. лицо",
		"IIN" => "840324300204",
		"contacts" => [
			"mobile" => "+77013336326",
		],
	],
	[
		"clientID" =>"CRM058659",
		"name" =>"Манас Ford Picup",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017229046",
		],
	],
	[
		"clientID" =>"CRM058665",
		"name" =>"Огай Даниил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778422021",
		],
	],
	[
		"clientID" =>"CRM058664",
		"name" =>"Ильдар",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			
			"mobile" => "+77015189005",
		],
	],
	[
		"clientID" =>"CRM058699",
		"name" =>"Владимир",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "stariy1924@mail.ru",
			"mobile" => "+77772588089",
		],
	],
	[
		"clientID" =>"CRM058706",
		"name" =>"Сергей Тойота Сиена",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77474611515",
		],
	],
	[
		"clientID" =>"CRM058727",
		"name" =>"Сергей  265/70/16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kinofil1@mail.ru",
			"mobile" => "+77471064287",
		],
	],
	[
		"clientID" =>"CRM058734",
		"name" =>"Ганькин Дмитрий Михайлович",
		"attribute" => "Физ. лицо",
		"IIN" => "730725300747",
		"contacts" => [
			"mobile" =>"+77772160836",
			"mobile" => "+77772160836",
		],
	],
	[
		"clientID" =>"CRM058730",
		"name" =>"Александр Васильевич",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772487848",
		],
	],
	[
		"clientID" =>"CRM058751",
		"name" =>"Ислам по инстаграмм",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77014559009",
		],
	],
	[
		"clientID" =>"CRM058749",
		"name" =>"Арман",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015193987",
		],
	],
	[
		"clientID" =>"CRM058760",
		"name" =>"Влад",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "byvlad@list.ru",
			"mobile" => "+77027504488",
		],
	],
	[
		"clientID" =>"CRM058762",
		"name" =>"Диски на ниву",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77473620568",
		],
	],
	[
		"clientID" =>"CRM058764",
		"name" =>"Нуржанов Багдаулет Нурдилдаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860810301674",
		"contacts" => [
			"mobile" => "+77012671144",
		],
	],
	[
		"clientID" =>"CRM058779",
		"name" =>"Алексей 1 шина 225/75/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273283902",
		],
	],
	[
		"clientID" =>"CRM058806",
		"name" =>"Андрей 205/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77075617557",
		],
	],
	[
		"clientID" =>"CRM058789",
		"name" =>"Серебренников Дмитрий Дмитриевич",
		"attribute" => "Физ. лицо",
		"IIN" => "750911303321",
		"contacts" => [
			"mobile" => "+77772618317",
		],
	],
	[
		"clientID" =>"CRM058838",
		"name" =>"Елена",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076028155",
		],
	],
	[
		"clientID" =>"CRM058826",
		"name" =>"Assanali",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "nur.assanali@gmail.com",
			"mobile" => "+77078168278",
		],
	],
	[
		"clientID" =>"CRM058829",
		"name" =>"Доронин Виктор Алексанрович",
		"attribute" => "Физ. лицо",
		"IIN" => "730716301657",
		"contacts" => [
			"mobile" =>"+77277428599",
			"mobile" => "+77277428599",
		],
	],
	[
		"clientID" =>"CRM058840",
		"name" =>"Алексей Костанай Груз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772844812",
		],
	],
	[
		"clientID" =>"CRM058845",
		"name" =>"Амшель Мейсон",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017454576",
		],
	],
	[
		"clientID" =>"CRM058848",
		"name" =>"Махамбет Камри",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773020649",
		],
	],
	[
		"clientID" =>"CRM058868",
		"name" =>"Айдос 205/65/15",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77017747001",
		],
	],
	[
		"clientID" =>"CRM058878",
		"name" =>"Гафурова Юлия Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "860117402611",
		"contacts" => [
			"E-Mail" => "albina@7line.kz",
			"mobile" =>"+77752877378",
			"mobile" => "+77787943490",
		],
	],
	[
		"clientID" =>"CRM058881",
		"name" =>"НУРЖАН Актау",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "nureke84@mail.ru",
			"mobile" => "+77018396455",
		],
	],
	[
		"clientID" =>"CRM058877",
		"name" =>"Ерик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77054948886",
		],
	],
	[
		"clientID" =>"CRM058886",
		"name" =>"Федор шины и диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "hold9b.f.g.72@gmail.com",
			"mobile" =>"+77023469312",
		],
	],
	[
		"clientID" =>"CRM058899",
		"name" =>"Анатолий",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77273791455",
		],
	],
	[
		"clientID" =>"CRM058901",
		"name" =>"Сидоренко Даниил Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "951023300451",
		"contacts" => [
			"mobile" => "+77770072882",
		],
		"TCs" => [
			[
				"brand" => "SUBARU",
				"gosNomer" => "203SHA/02",
			],
		],
	],
	[
		"clientID" =>"CRM058902",
		"name" =>"Меирхан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77018878666",
		],
	],
	[
		"clientID" =>"CRM058916",
		"name" =>"талдак",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77474255131",
		],
	],
	[
		"clientID" =>"CRM058956",
		"name" =>"сергей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "sojuz_2008@mail.ru",
			"mobile" => "+77089085442",
		],
	],
	[
		"clientID" =>"CRM058961",
		"name" =>"Андреенко Валерий Алексанрович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017106570",
		],
	],
	[
		"clientID" =>"CRM058964",
		"name" =>"Нурлан Prado",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77053393792",
		],
	],
	[
		"clientID" =>"CRM058971",
		"name" =>"Шварцман Владимир Ефимович",
		"attribute" => "Физ. лицо",
		"IIN" => "560930300236",
		"contacts" => [
			"mobile" => "+77077115735",
		],
		"TCs" => [
			[
				"brand" => "ToyotaPrevia",
				"size" => "205/65/15",
				"gosNomer" => "A761HFM",
			],
		],
	],
	[
		"clientID" =>"CRM058981",
		"name" =>"Сакен кредит",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017856684",
		],
	],
	[
		"clientID" =>"CRM058978",
		"name" =>"Игорь FJ Cruizer шины и диски",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77772904045",
		],
	],
	[
		"clientID" =>"CRM059019",
		"name" =>"Хаждан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77272699930",
		],
	],
	[
		"clientID" =>"CRM059020",
		"name" =>"Узен",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "жена Дамеля +7 (778) 146-53-70",
			"mobile" =>"+77789857863",
			
		],
	],
	[
		"clientID" =>"CRM059036",
		"name" =>"Сугуров Тимур Булатович",
		"attribute" => "Физ. лицо",
		"IIN" => "830122301517",
		"contacts" => [
			"mobile" => "+77023331360",
		],
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "185/65/14",
				"gosNomer" => "541UNA02",
			],
		],
	],
	[
		"clientID" =>"CRM059042",
		"name" =>"anatolstrelkov@mail.ru",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "anatolstrelkov@mail.ru",
			"mobile" =>"+77017348420",
		],
	],
	[
		"clientID" =>"CRM059046",
		"name" =>"ТОО «Гүлденустрой»",
		"attribute" => "Физ. лицо",
		"IIN" => "100440006886",
		"contacts" => [
			"mobile" =>"+77774395388",
			"adress" => "Айжамал контактное лицо",
		],
	],
	[
		"clientID" =>"CRM053813",
		"name" =>"Джамбазишвили-Юджер София",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77714909944",
		],
		"TCs" => [
			[
				"brand" => "NissanPatrol",
				"size" => "275/70/16",
				"gosNomer" => "B0450",
			],
		],
	],
	[
		"clientID" =>"CRM000389",
		"name" =>"Талгат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"77028409991",
		],
	],
	[
		"clientID" =>"CRM048888",
		"name" =>"Данияр  КазЭкспо",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77477083880",
		],
		"TCs" => [
			[
				"brand" => "ToyotaPrado",
				"size" => "265/65/17",
				"gosNomer" => "H761028",
			],
		],
	],
	[
		"clientID" =>"CRM056266",
		"name" =>"Ларинани Надежда",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772482484",
		],
		"TCs" => [
			[
				"brand" => "Lexus NX200",
				"size" => "225/60/18",
				"gosNomer" => "1333ZUA/02",
			],
		],
	],
	[
		"clientID" =>"CRM060093",
		"name" =>"Акционерное общество \"Западно-Казахстанская распределительная электросетевая компания\"",
		"attribute" => "Юр. лицо",
		"IIN" => "961140000153",
		"contacts" => [
			"mobile" => "87112540177
",
		],
	],
	[
		"clientID" =>"CRM054111",
		"name" => "Даурен  (Range Rover)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77077109115",
		],
		"TCs" => [
			[
				"brand" => "RangeRover",
				"size" => "255/50/20",
				"gosNomer" => "149KBA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053104",
		"name" => "Антон (БМВ   Х6)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072228880",
		],
		"TCs" => [
			[
				"brand" => "BMWX6",
				"size" => "255/50/19",
				"gosNomer" => "555VFA/02",
			],
		],
	],
	[
		"clientID" =>"CRM048886",
		"name" => "Олжас  (Mitsubishi)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77087711132",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi",
				"size" => "265/70/16",
				"gosNomer" => "746ACZ/05",
			],
		],
	],
	[
		"clientID" =>"CRM048882",
		"name" => "Ирина  (ниссан)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77072454440",
		],
		"TCs" => [
			[
				"brand" => "Nissan",
				"size" => "205/65/15",
				"gosNomer" => "275DDA/02",
			],
		],
	],
	[
		"clientID" =>"000002285",
		"name" =>"Табылдиев Бакытжан Адибаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "520306300270",
		"contacts" => [
			"E-Mail" => "bt-55@mail.ru",
			"mobile" =>"+77772378129",
			"mobile" => "+77272377121",
		],
	],
	[
		"clientID" =>"000002199",
		"name" =>"Киреев Виталий Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "860525301070",
		"contacts" => [
			"mobile" => "+77053888037",
		],
	],
	[
		"clientID" =>"000002201",
		"name" =>"Сейтахметов Серикжан",
		"attribute" => "Юр. лицо",
		"IIN" => "771124301222",
		"contacts" => [
			"mobile" => "+77785858847",
		],
	],
	[
		"clientID" =>"000002202",
		"name" =>"Арефьев Аркадий Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "650119300953",
		"contacts" => [
			"mobile" => "+77012245268",
		],
	],
	[
		"clientID" =>"000002204",
		"name" =>"Мухамед",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77059104020",
		],
	],
	[
		"clientID" =>"000002205",
		"name" =>"Балабатыров Нурбек",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019997185",
		],
	],
	[
		"clientID" =>"000002208",
		"name" =>"Белых Павел Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "791017300583",
		"contacts" => [
			"mobile" => "+77777198681",
		],
	],
	[
		"clientID" =>"000002209",
		"name" =>"Архангельский Георгий Александрович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773850047",
		],
	],
	[
		"clientID" =>"000002211",
		"name" =>"Ефанов Сергей Валерьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "751221300082",
		"contacts" => [
			"mobile" => "+77776885139",
		],
	],
	[
		"clientID" =>"000002212",
		"name" =>"Денисенко Виктория Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "711215499035",
	],
	[
		"clientID" =>"000002213",
		"name" =>"Сулейменов Даулет Сакенович",
		"attribute" => "Физ. лицо",
		"IIN" => "830718301044",
		"contacts" => [
			"mobile" => "+77029099995",
		],
	],
	[
		"clientID" =>"000002214",
		"name" =>"Косатый Виктор Витальевич",
		"attribute" => "Физ. лицо",
		"IIN" => "500512300048",
	],
	[
		"clientID" =>"000002217",
		"name" =>"Мереева Гульназ Оралхановна",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77773599811",
		],
	],
	[
		"clientID" =>"000002219",
		"name" =>"Наурызалиева Каныскул Кенжалиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "630917401378",
		"contacts" => [
			"mobile" => "+77017631485",
		],
	],
	[
		"clientID" =>"000002220",
		"name" =>"Вдовцев Андрей Павлович",
		"attribute" => "Физ. лицо",
		"IIN" => "711022301524",
		"contacts" => [
			"mobile" => "+77072111000",
		],
		"TCs" => [
			[
				"brand" => "NissanMurano",
				"size" => "255/55/18",
				"gosNomer" => "A 387 XCO",
			],
		],
	],
	[
		"clientID" =>"000002223",
		"name" =>"Прехно Алексей Васильевич",
		"attribute" => "Физ. лицо",
		"IIN" => "84022300824",
		"contacts" => [
			"mobile" => "+77711135244",
		],
	],
	[
		"clientID" =>"000002225",
		"name" =>"Липов Евгений Станиславович",
		"attribute" => "Физ. лицо",
		"IIN" => "830417301850",
		"contacts" => [
			"mobile" => "+77075911101",
		],
	],
	[
		"clientID" =>"000002228",
		"name" =>"Воробьёва Юлия Константиновна",
		"attribute" => "Юр. лицо",
		"IIN" => "911022400267",
		"contacts" => [
			"mobile" => "+77078305070",
		],
	],
	[
		"clientID" =>"000002229",
		"name" =>"Станислав Кызылорда",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075944000",
		],
	],
	[
		"clientID" =>"000002230",
		"name" =>"Лосев Вадим Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "730308302964",
		"contacts" => [
			"mobile" => "+77019802273",
		],
	],
	[
		"clientID" =>"000002231",
		"name" =>"Торкин Михаил Борисович",
		"attribute" => "Физ. лицо",
		"IIN" => "610907300315",
		"contacts" => [
			"mobile" => "+77770449944",
		],
	],
	[
		"clientID" =>"000002232",
		"name" =>"Никита",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77051962345",
		],
	],
	[
		"clientID" =>"000002233",
		"name" =>"Богданов Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772778623",
		],
	],
	[
		"clientID" =>"000002234",
		"name" =>"Сасов Владимир Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "760503301931",
		"contacts" => [
			"mobile" => "+77772450531",
		],
	],
	[
		"clientID" =>"000002235",
		"name" =>"Алиев Эльдар Вейсалович",
		"attribute" => "Физ. лицо",
		"IIN" => "700602302681",
		"contacts" => [
			"mobile" => "+77751013977",
		],
	],
	[
		"clientID" =>"000002236",
		"name" =>"Соколова Елена Владимировна",
		"attribute" => "Физ. лицо",
		"IIN" => "630317400635",
		"contacts" => [
			"mobile" => "+77772332711",
		],
	],
	[
		"clientID" =>"000002237",
		"name" =>"Валиев Султан Нурович",
		"attribute" => "Физ. лицо",
		"IIN" => "600508301626",
	],
	[
		"clientID" =>"000002238",
		"name" =>"Юрова Дарья Юрьевна",
		"attribute" => "Физ. лицо",
		"IIN" => "900408400578",
		"contacts" => [
			"mobile" => "+77076117163",
		],
	],
	[
		"clientID" =>"000002239",
		"name" =>"АЛЕКСАНДРОВ АРТУР СЕРГЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "891011300303",
		"contacts" => [
			"mobile" => "+77019004096",
		],
	],
	[
		"clientID" =>"000002240",
		"name" =>"Амарбекова Рaушангуль Ахметжановна",
		"attribute" => "Физ. лицо",
		"IIN" => "760926400672",
		"contacts" => [
			"mobile" => "+77058777000",
		],
	],
	[
		"clientID" =>"000002241",
		"name" =>"КУЛАКИДИ ПАВЕЛ ИВАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "910120300012",
		"contacts" => [
			"mobile" => "+77774034977",
		],
	],
	[
		"clientID" =>"000002242",
		"name" =>"Шамнамазова Индира Айткалиевна",
		"attribute" => "Физ. лицо",
		"IIN" => "840106400750",
		"contacts" => [
			"mobile" => "840106400750",
		],
	],
	[
		"clientID" =>"000002244",
		"name" =>"Самикова Рано Аутжановна",
		"attribute" => "Физ. лицо",
		"IIN" => "790919400253",
		"contacts" => [
			"mobile" => "+77756904469",
		],
		"TCs" => [
			[
				"brand" => "HondaCR-V",
				"size" => "225/60/18",
				"gosNomer" => "734 DMA/02",
			],
		],
	],
	[
		"clientID" =>"000002246",
		"name" =>"Тельхин Константин Геннадьевич",
		"attribute" => "Физ. лицо",
		"IIN" => "790327302176",
		"contacts" => [
			"mobile" => "+77014348084",
		],
	],
	[
		"clientID" =>"000002249",
		"name" =>"Сычев Александр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77057131649",
		],
	],
	[
		"clientID" =>"000002253",
		"name" =>"АМАНЧИН АЙДАР БЕРХАИРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "680627301235",
		"contacts" => [
			"mobile" => "+77017727373",
		],
	],
	[
		"clientID" =>"000002254",
		"name" =>"БАТЫРГОЖИН НУРЛАН КАБАШОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "630308300184",
		"contacts" => [
			"mobile" => "+77772108440",
		],
	],
	[
		"clientID" =>"000002256",
		"name" =>"НУКЕНОВ АРМАН САПАРГАЛИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "800701301630",
	],
	[
		"clientID" =>"000002258",
		"name" =>"ДВУМАРОВ АРЛИ ИСМАРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "890318300121",
		"contacts" => [
			"mobile" => "+77754702222",
		],
	],
	[
		"clientID" =>"000002260",
		"name" =>"ЕРКІНОВ ЕРНАР ЕРКІНҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "871022301090",
		"contacts" => [
			"E-Mail" => "SHIIKOO@MAIL.RU",
			"mobile" =>"+77021070377",
			"mobile" => "+77021070377",
			"adress" => "Алматинская область, Кульсары",
		],
	],
	[
		"clientID" =>"000002268",
		"name" =>"МУХАМЕДСАЛИЕВ МАРАТ ОХАПОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "730927301278",
		"contacts" => [
			"E-Mail" => "mom73@mail.ru",
			"mobile" => "+77769994099",
		],
	],
	[
		"clientID" =>"000002270",
		"name" =>"МИРВАЗЕТДИНОВ КАИРКАН",
		"attribute" => "Физ. лицо",
		"IIN" => "541012301315",
		"contacts" => [
			"mobile" => "+77775432056",
		],
	],
	[
		"clientID" =>"000002272",
		"name" =>"Темiр Муслим ТОО",
		"attribute" => "Юр. лицо",
		"IIN" => "110140008407",
		"contacts" => [
			"adress" => "050056, РК, г. Алматы, ул. Герцена, д. 186",
		],
	],
	[
		"clientID" =>"000002273",
		"name" =>"ОСМОЛОВСКИЙ АНАТОЛИЙ ВИКТОРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "660228302052",
		"contacts" => [
			"mobile" => "+77772235927",
		],
	],
	[
		"clientID" =>"000002278",
		"name" =>"Сатаев Ардак Мейрамович",
		"attribute" => "Физ. лицо",
		"IIN" => "801122301021",
		"contacts" => [
			"mobile" => "+77015000916",
		],
		"TCs" => [
			[
				"brand" => "TOYOTA LC PRADO",
				"size" => "265/60/18",
				"gosNomer" => "926NNA/02",
			],
		],
	],
	[
		"clientID" =>"000002279",
		"name" =>"Степаненко Александр Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "860917350773",
		"contacts" => [
			"mobile" => "+77053381028",
		],
	],
	[
		"clientID" =>"000002280",
		"name" =>"Демин Виталий Алексеевич",
		"attribute" => "Физ. лицо",
		"IIN" => "771011302276",
	],
	[
		"clientID" =>"000002281",
		"name" =>"ТАТКЕЕВА МАДИНА МАЛГАЖДАРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "890218450120",
	],
	[
		"clientID" =>"000002283",
		"name" =>"Муханова Мадина Сериковна",
		"attribute" => "Физ. лицо",
		"IIN" => "851213450486",
		"contacts" => [
			"mobile" => "870178265500",
		],
	],
	[
		"clientID" =>"000002284",
		"name" =>"ҚУАНДЫҚ МҰХАММЕДСЕРІ МУХТАРҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "940627301607",
	],
	[
		"clientID" =>"000002300",
		"name" =>"МАНАХОВ ДАМИР РАШИТОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "860910301073",
		"contacts" => [
			"mobile" => "+77053280888",
		],
	],
	[
		"clientID" =>"000002302",
		"name" =>"Игорь",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77778051842",
		],
	],
	[
		"clientID" =>"000002303",
		"name" =>"Чумаков Андрей Георгиевич",
		"attribute" => "Физ. лицо",
		"IIN" => "910103300109",
		"contacts" => [
			"mobile" => "+77772722122",
		],
	],
	[
		"clientID" =>"000002304",
		"name" =>"Муровец Юрий Львович",
		"attribute" => "Физ. лицо",
		"IIN" => "581110300199",
		"contacts" => [
			"mobile" => "+77016129661",
		],
	],
	[
		"clientID" =>"000002305",
		"name" =>"ХАРИТОНОВ АРТЁМ ЮРИЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "790215303066",
		"contacts" => [
			"mobile" =>"+77022560615",
		],
	],
	[
		"clientID" =>"000002306",
		"name" =>"Карпова Ольга Алексеевна",
		"attribute" => "Физ. лицо",
		"IIN" => "640415402646",
		"contacts" => [
			"mobile" => "+77017207719",
		],
		"TCs" => [
			[
				"brand" => "SubaruOutback",
				"size" => "215/55/17",
				"gosNomer" => "A419EYP",
			],
		],
		/*
			<subscription>
				<nomer>1150</nomer>
				<date>01.01.0001 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000002310",
		"name" =>"Плотников Сергей Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "770228302184",
		"contacts" => [
			"mobile" =>"+77017105036",
		],
	],
	[
		"clientID" =>"000002311",
		"name" =>"ЗАХАРОВ ВЛАДИМИР АЛЕКСАНДРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "890629300465",
		"contacts" => [
			"mobile" => "+77052111421",
		],
	],
	[
		"clientID" =>"000002313",
		"name" =>"Константинов Сергей Владимирович",
		"attribute" => "Физ. лицо",
		"IIN" => "730316302452",
		"contacts" => [
			"mobile" => "+77073565460",
		],
		"TCs" => [
			[
				"brand" => "Mitsubishi Pajero II",
				"gosNomer" => " 145 SLA/0",
			],
		],
		/*
			<subscription>
				<nomer>523</nomer>
				<date>29.05.2015 0:00:00</date>
				<srok>01.01.0001 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"000002317",
		"name" =>"МӘДЕНИЕТ БАҚЫТ МӘДЕНИЕТҰЛЫ",
		"attribute" => "Физ. лицо",
		"IIN" => "720712300150",
	],
	[
		"clientID" =>"000002286",
		"name" =>"ТОО Хлебобараночный комбинат «Аксай»",
		"attribute" => "Юр. лицо",
		"IIN" => "970340000625",
		"contacts" => [
			"adress" => "050031, РК, г. Алматы, м/рн Аксай-2, ул. Б.Момыш улы 36А",
		],
	],
	[
		"clientID" =>"000002287",
		"name" =>"ТРУШНИКОВ ВЯЧЕСЛАВ ВАЛЕНТИНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "821008301045",
		"contacts" => [
			"mobile" => "+77017188080",
		],
	],
	[
		"clientID" =>"000002291",
		"name" =>"ТАЩЕЕВ АЛЕКСАНДР АЛЕКСЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "730627350136",
		"contacts" => [
			"E-Mail" => "tas4eev@gmail.com",
			"mobile" =>"+77273306896",
			"mobile" => "+77273306896",
		],
	],
	[
		"clientID" =>"000002292",
		"name" =>"КУРИЛО АЛЕКСАНДР НИКОЛАЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "560506301084",
	],
	[
		"clientID" =>"000002293",
		"name" =>"БОГОМАЗОВА НАТАЛЬЯ ВЛАДИМИРОВНА",
		"attribute" => "Физ. лицо",
		"IIN" => "781230400010",
		"contacts" => [
			"E-Mail" => "Nataliy_angel@list.ru",
			"mobile" => "+77775754447",
		],
	],
	[
		"clientID" =>"000002297",
		"name" =>"ПАНГЕРЕЕВ НУРБОЛ АДИЛГЕРЕЕВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "761110302004",
		"contacts" => [
			"mobile" => "870747600008",
		],
	],
	[
		"clientID" =>"000002299",
		"name" =>"Шуканов Сергей Шаяхметович",
		"attribute" => "Физ. лицо",
		"IIN" => "691111302202",
		"contacts" => [
			"mobile" =>"+77772569581",
			"mobile" => "+77772569581",
		],
	],
	[
		"clientID" =>"000002325",
		"name" =>"ТРИАНДОФИЛИДИ АЛЕКСАНДР ИВАНОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "670111301486",
		"contacts" => [
			"mobile" => "+77772424244",
		],
	],
	[
		"clientID" =>"000002327",
		"name" =>"Каимбаев Рахимбай Абдраманулы",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772224025",
		],
	],
	[
		"clientID" =>"000002328",
		"name" =>"АУБАКИРОВ ЕРБОЛАТ ШАЙКЕРОВИЧ",
		"attribute" => "Физ. лицо",
		"IIN" => "730330301272",
		"contacts" => [
			"mobile" => "+77011111315",
		],
	],
	[
		"clientID" =>"CRM053964",
		"name" =>"клиент",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM057349",
		"name" =>"Данияр",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77759698136",
		],
		"TCs" => [
			[
				"brand" => "ToyotaFortuner",
				"size" => "265/65/17",
				"gosNomer" => "822TVA/02",
			],
		],
	],
	[
		"clientID" =>"CRM048880",
		"name" =>"Дильшат  Евразия",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77475221051",
		],
		"TCs" => [
			[
				"brand" => "Lada4*4",
				"size" => "215/60/16",
				"gosNomer" => "H2212/02",
			],
			[
				"brand" => "Nexia",
				"size" => "185/65/14",
				"gosNomer" => "H803647",
			],
		],
	],
	[
		"clientID" =>"CRM053511",
		"name" => "Акжан (Ауди)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77015754646",
		],
		"TCs" => [
			[
				"brand" => "Audi",
				"size" => "195/65/15",
				"gosNomer" => "A557UAO",
			],
		],
	],
	[
		"clientID" =>"CRM006455",
		"name" =>"Макаров Евгений Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "750404350098",
	],
	[
		"clientID" =>"CRM023985",
		"name" =>"Ахметов Аскар Болатович",
		"attribute" => "Физ. лицо",
		"IIN" => "870523300320",
		"contacts" => [
			"mobile" => "+77017104525",
		],
		"TCs" => [
			[
				"brand" => "Nissan Teana",
				"size" => "215/55/17",
				"gosNomer" => "086 CCA/11",
			],
			[
				"brand" => "Nissan Teana",
				"size" => "215/55/17",
				"gosNomer" => "086 CCA/11",
			],
			[
				"brand" => "Nissan Teana",
				"size" => "215/55/17",
				"gosNomer" => "086 CCA/11",
			],
		],
	],
	[
		"clientID" =>"CRM056125",
		"name" =>"Бекхожин Адлет",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77759761522",
		],
		"TCs" => [
			[
				"brand" => "Mercedes-Benz W221",
				"size" => "255/45/18",
				"gosNomer" => "874WXA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053833",
		"name" =>"серик 215/55 17",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77707221299",
		],
	],
	[
		"clientID" =>"CRM053860",
		"name" =>"Жумалиев Айкен Досанулы",
		"attribute" => "Физ. лицо",
		"IIN" => "930828300085",
		"contacts" => [
			"mobile" => "+77020007760",
		],
	],
	[
		"clientID" =>"CRM053886",
		"name" =>"ТОО Физико-технический институт",
		"attribute" => "Физ. лицо",
		"IIN" => "071140011330",
		"contacts" => [
			"E-Mail" => "gkoroleva67@mail.ru",
			"mobile" => "+77472727328",
		],
	],
	[
		"clientID" =>"CRM053912",
		"name" =>"275/70R16",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77021029111",
		],
	],
	[
		"clientID" =>"CRM053916",
		"name" =>"Алексей",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77777797794",
		],
	],
	[
		"clientID" =>"CRM053934",
		"name" =>"Берик",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77026730811",
		],
	],
	[
		"clientID" =>"CRM057353",
		"name" =>"Роман Toyota Land Cruiser",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77084033248",
			"mobile" => "+77084033248",
		],
		"TCs" => [
			[
				"brand" => "Toyota Land Cruiser 100",
				"size" => "275/65/17",
				"gosNomer" => "A099KEO",
			],
			[
				"brand" => "Toyota Land Cruiser 100",
				"size" => "265/75/16",
				"gosNomer" => "966SYA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053108",
		"name" => "Алик  (Камри)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77718561113",
		],
		"TCs" => [
			[
				"brand" => "Camry",
				"size" => "215/60/16",
				"gosNomer" => "055AVA/02",
			],
		],
	],
	[
		"clientID" =>"CRM048134",
		"name" => "Юлия (на крузере)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772500223",
		],
		"TCs" => [
			[
				"brand" => "LandCruiser",
				"size" => "275/65/17",
				"gosNomer" => "A799UCN",
			],
		],
	],
	[
		"clientID" =>"CRM053512",
		"name" => "Абдразак (Киа)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017107416",
		],
		"TCs" => [
			[
				"brand" => "KIA",
				"size" => "195/65/15",
				"gosNomer" => "854MCA/01",
			],
		],
	],
	[
		"clientID" =>"CRM049503",
		"name" =>"Сатаева Гульжамал Казангаповна",
		"attribute" => "Физ. лицо",
		"IIN" => "530217400432",
	],
	[
		"clientID" =>"CRM052414",
		"name" =>"Тимофеев Илья Николаевич",
		"attribute" => "Физ. лицо",
		"IIN" => "860331300741",
		"contacts" => [
			"mobile" => "+77772888676",
		],
	],
	[
		"clientID" =>"CRM052427",
		"name" =>"Малмаков  Жанибек  Умарбекович",
		"attribute" => "Физ. лицо",
		"IIN" => "790726300763",
		"contacts" => [
			"mobile" => "+77777052649",
		],
		"TCs" => [
			[
				"brand" => "Hyundai",
				"size" => "185/65/15",
				"gosNomer" => "981LUA/02",
			],
		],
	],
	[
		"clientID" =>"CRM052433",
		"name" =>"Саша",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77071727242",
		],
	],
	[
		"clientID" =>"CRM052424",
		"name" =>"Джундыбаев  Булат  Бекбулатович",
		"attribute" => "Физ. лицо",
		"IIN" => "821225301362",
		"contacts" => [
			
			"mobile" => "+77015519777",
		],
		"TCs" => [
			[
				"brand" => "Lexus",
				"size" => "265/70/16",
				"gosNomer" => "470LRN",
			],
		],
	],
	[
		"clientID" =>"CRM052419",
		"name" =>"Ержан",
		"attribute" => "Физ. лицо",
		"contacts" => [
			"mobile" => "+77017506045",
		],
	],
	[
		"clientID" =>"CRM052417",
		"name" =>"Альменбетов Нурбол Нурсултанович",
		"attribute" => "Физ. лицо",
		"IIN" => "830807302214",
		"contacts" => [
			
			"mobile" => "+77087227279",
		],
	],
	[
		"clientID" =>"CRM053818",
		"name" => "Нурия (абонемент)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77076104938",
		],
		"TCs" => [
			[
				"brand" => "HyundaiAccent",
				"size" => "195/55/16",
				"gosNomer" => "058BFA/02",
			],
		],
		/*
			<subscription>
				<nomer>0903</nomer>
				<date>26.11.2016 0:00:00</date>
				<srok>24.04.2017 0:00:00</srok>
			</subscription>
		*/
	],
	[
		"clientID" =>"CRM057333",
		"name" =>"Ильяс Toyota",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012049355",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "275/65/17",
				"gosNomer" => "134YMA/02",
			],
		],
	],
	[
		"clientID" =>"CRM056257",
		"name" =>"Ален груз",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772240707",
		],
	],
	[
		"clientID" =>"CRM056239",
		"name" =>"Павел Караганда",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017316690",
		],
	],
	[
		"clientID" =>"CRM056263",
		"name" =>"Коршунов Михаил Витальевич",
		"attribute" => "Физ. лицо",
		"IIN" => "900413300221",
		"contacts" => [
			"mobile" => "+77471944448",
		],
	],
	[
		"clientID" =>"CRM056293",
		"name" =>"Фархат",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772081977",
		],
	],
	[
		"clientID" =>"CRM056297",
		"name" =>"Ким Руслан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77022223308",
		],
	],
	[
		"clientID" =>"CRM048887",
		"name" => "Владимир (Pajero)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77011848278",
		],
		"TCs" => [
			[
				"brand" => "Pajero",
				"size" => "265/60/18",
				"gosNomer" => "669UUA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053107",
		"name" => "Николай (прадо)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77781111685",
		],
		"TCs" => [
			[
				"brand" => "Prado",
				"size" => "225/65/17",
				"gosNomer" => "606VKA/02",
			],
		],
	],
	[
		"clientID" =>"CRM057351",
		"name" =>"Отыншиева Асем",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" =>"+77075059003",
		],
		"TCs" => [
			[
				"brand" => "KIA",
				"gosNomer" => "M022879",
			],
		],
	],
	[
		"clientID" =>"CRM052587",
		"name" =>"Пучкова Людмила (тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017274848",
		],
		"TCs" => [
			[
				"brand" => "ToyotaCorolla",
				"size" => "205/55/17",
				"gosNomer" => "017XWA/02",
			],
		],
	],
	[
		"clientID" =>"CRM048897",
		"name" => "Тимур  (тойота)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77717427609",
		],
		"TCs" => [
			[
				"brand" => "Toyota",
				"size" => "205/55/16",
				"gosNomer" => "258MWA/02",
			],
		],
	],
	[
		"clientID" =>"CRM057352",
		"name" =>"Буланбаев Адил",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77012271797",
		],
		"TCs" => [
			[
				"brand" => "Lexus RX 300",
				"size" => "235/55/18",
				"gosNomer" => "A909UOO",
			],
		],
	],
	[
		"clientID" =>"CRM032136",
		"name" =>"АО Лок Алатау",
		"attribute" => "Юр. лицо",
		
		"contacts" => [
			"mobile" => "+77071969630",
		],
		"TCs" => [
			[
				"brand" => "Lexus",
				"size" => "285/60/R18",
				"gosNomer" => "KZ296UD/02",
			],
			[
				"brand" => "Mersedes",
				"size" => "215/55/R16",
				"gosNomer" => "A974FZ",
			],
		],
	],
	[
		"clientID" =>"CRM048885",
		"name" => "Куаныш",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77019087458",
		],
		"TCs" => [
			[
				"brand" => "LandCruiser",
				"size" => "275/65/17",
				"gosNomer" => "M025RCM",
			],
		],
	],
	[
		"clientID" =>"CRM053445",
		"name" =>"Казаков Дмитрий Александрович",
		"attribute" => "Физ. лицо",
		"IIN" => "790426300039",
		"contacts" => [
			"mobile" => "+77014217321",
		],
	],
	[
		"clientID" =>"CRM053451",
		"name" =>"Бахытжан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77027733003",
		],
	],
	[
		"clientID" =>"CRM053443",
		"name" =>"Шайбеков Ансаган Берикович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77075001640",
			"adress" => "г. Актау",
		],
	],
	[
		"clientID" =>"CRM053468",
		"name" =>"Жумабеков Сергей Иватуллович",
		"attribute" => "Физ. лицо",
		"IIN" => "790704303738",
		"contacts" => [
			"mobile" => "+77012122632",
		],
	],
	[
		"clientID" =>"CRM053477",
		"name" =>"евгений акумулятор",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053470",
		"name" =>"Асет +77017329073",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77017329073",
		],
	],
	[
		"clientID" =>"CRM053476",
		"name" =>"Игнатьев Алексей Николаевич",
		"attribute" => "Юр. лицо",
		"IIN" => "871115300197",
		"contacts" => [
			"mobile" => "+77777018408",
		],
		"TCs" => [
			[
				"brand" => "Subaru",
				"size" => "225/55/17",
				"gosNomer" => "711MTA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053501",
		"name" =>"Аблай forward a12",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77027170191",
		],
	],
	[
		"clientID" =>"CRM053482",
		"name" =>"Виктор мерседес",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053483",
		"name" =>"Виктор мерседес",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053516",
		"name" =>"Калыгулова Салтанат Бекболатовна",
		"attribute" => "Физ. лицо",
		"IIN" => "891005402224",
		"contacts" => [
			"mobile" => "+77072066671",
		],
	],
	[
		"clientID" =>"CRM053627",
		"name" =>"Владимир Сату",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77056098998",
		],
	],
	[
		"clientID" =>"CRM053633",
		"name" =>"Nikita",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"E-Mail" => "kot.pisem.net@mail.ru",
			
			"mobile" => "+77779622221",
		],
	],
	[
		"clientID" =>"CRM053690",
		"name" =>"Искаков Сырамбет",
		"attribute" => "Физ. лицо",
		
	],
	[
		"clientID" =>"CRM053691",
		"name" =>"Игембеков Нуурлан Булатович",
		"attribute" => "Физ. лицо",
		"IIN" => "910107301406",
		"contacts" => [
			"mobile" => "+77073294442",
		],
	],
	[
		"clientID" =>"CRM053720",
		"name" =>"Беспаев Батырбек Охасович",
		"attribute" => "Физ. лицо",
		"IIN" => "610317301871",
		"contacts" => [
			"mobile" => "+77025234265",
		],
	],
	[
		"clientID" =>"CRM053738",
		"name" =>"Варрик   Михаил  Андреевич",
		"attribute" => "Физ. лицо",
		"IIN" => "881217300361",
		"contacts" => [
			"mobile" => "+77016687007",
		],
		"TCs" => [
			[
				"brand" => "LexusIS300",
				"size" => "235/45/17",
				
			],
		],
	],
	[
		"clientID" =>"CRM053740",
		"name" =>"Тенесбаева Индира Куанышовна",
		"attribute" => "Физ. лицо",
		"IIN" => "811206400721",
		"contacts" => [
			"mobile" => "+77012251931",
		],
	],
	[
		"clientID" =>"CRM053748",
		"name" =>"Титов  Сергей  Анатольевич",
		"attribute" => "Физ. лицо",
		"IIN" => "780829301869",
		"contacts" => [
			"mobile" => "+77074804450",
		],
		"TCs" => [
			[
				
				"size" => "215/65/16",
				"gosNomer" => "776ZSA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053750",
		"name" =>"Новосёлов Виталий Олегович",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77053275237",
		],
	],
	[
		"clientID" =>"CRM053758",
		"name" =>"Имиржан",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77073105510",
		],
	],
	[
		"clientID" =>"CRM053824",
		"name" => "Владимир (КИА Сефия)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77776990144",
		],
		"TCs" => [
			[
				"brand" => "KIASephia",
				"size" => "175/65/14",
				"gosNomer" => "A325NHO",
			],
		],
	],
	[
		"clientID" =>"CRM052983",
		"name" => "Светлана  (Хонда)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77772752775",
		],
		"TCs" => [
			[
				"brand" => "HondaCRV",
				"size" => "205/70/15",
				"gosNomer" => "890OXA/02",
			],
		],
	],
	[
		"clientID" =>"CRM053786",
		"name" => "Ленар (вольво и прадо)",
		"attribute" => "Физ. лицо",
		
		"contacts" => [
			"mobile" => "+77078625315",
		],
		"TCs" => [
			[
				"brand" => "Prado",
				"size" => "25/65/17",
				"gosNomer" => "064BF/02",
			],
			[
				"brand" => "VolvoXC70",
				"size" => "235/55/17",
				"gosNomer" => "H579TM/97",
			],
		],
	],
]];
			// foreach ($clients as $item) {
			// 	$client = new Client();
			// 	$client->name = $item['name'];
			// 	$client->phone = $item['contacts']['mobile'];
			// 	if($item['contacts']['E-Mail'] != null)
			// 		$client->email = $item['contacts']['E-Mail'];
			// 	if($item['contacts']['adress'] != null)
			// 	$client->address = $item['contacts']['adress'];
			// 	else
			// 	$client->address = "нет";
			// 	$client->manager = "Система";
			// 	$client->data = json_encode([
			// 		"order_id"=>"0",
			// 		"clientID"=>$item['clientID'],
			// 		"TCs" => $item["TCs"]
			// 	]);
			// 	if($item['attribute'] != null)
			// 	$client->type = $item['attribute'];
			// 	if($item['IIN'] != null)
			// 	$client->iin = $item['IIN'];
			// 	if(!$client->save()){
			// 		print_r($client->errors);
			// 	}
			// }	
	    $searchModel = new SearchClient();
	    $dataProvider = $searchModel->search(yii::$app->request->get(),yii::$app->user->id);
      return $this->render('index',compact(['searchModel','dataProvider']));
    }
}
