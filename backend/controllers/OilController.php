<?php
namespace backend\controllers;

use Yii;
use backend\models\SearchOil;
use common\models\Disc;
use common\models\Oil;
use common\models\Battery;
use backend\components\BaseController;

/**
 * Oil controller
 */
class OilController extends BaseController
{
  public function actionIndex()
  {
    $searchModel = new SearchOil();
    $models = $searchModel->search(Yii::$app->request->get());  
    return $this->render('index',compact(['models','searchModel']));
  }
}
