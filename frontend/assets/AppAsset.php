<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 *  <link href="css/font-awesome.css" rel="stylesheet">
 *  <link href="css/bootstrap-select.css" rel="stylesheet">
 *  <link href="css/owl.carousel.css" rel="stylesheet">
 *  <link href="css/owl.theme.default.css" rel="stylesheet">
 *  <link href="css/style.teal.flat.css" rel="stylesheet" id="theme">
 *  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 *  <script src="js/jquery.js"></script>
 *  <!-- Include all compiled plugins (below), or include individual files as needed -->
 *  <script src="bootstrap/js/bootstrap.js"></script>
 *  <!-- Plugins -->
 *  <script src="js/bootstrap-select.js"></script>
 *  <script src="js/owl.carousel.js"></script>
 *  <script src="js/mimity.js"></script>
 *</body>
 */
class AppAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public static $style = 'red';
  public $css = [
    'css/site.css',
    'css/jquery-ui.min.css',
    'css/font-awesome.min.css',
    'css/bootstrap-select.min.css',
    'css/owl.carousel.min.css',
    'css/owl.theme.default.min.css',
    'css/style.red.flat.min.css',
    'css/jquery.bootstrap-touchspin.min.css',
    'css/style.red.rounded.min.css'
  ];
  public $js = [
    "js/jquery-ui.min.js",
    "js/bootstrap-select.min.js",
    "js/owl.carousel.min.js",
    // "js/nouislider.js",
    // "js/mimity.detail.js",
    // '/js/jquery.ez-plus.min.js',
    '/js/jquery.bootstrap-touchspin.min.js',
    // 'js/jquery.raty-fa.min.js',
    "js/mimity.js",
    "js/angular.min.js",
    "js/mastershinaApp.js",
    // "js/mimity.filter-sidebar.js",
  ];
  public $depends = [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
  ];
  public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
