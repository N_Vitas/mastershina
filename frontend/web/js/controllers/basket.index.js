mastershina.controller('BasketForm',function($scope,$http){
	$scope.orderform = {
		name:"",
		email:"",
		phone:"",
		address:"",
    comment:"",
		sendBasketForm:function(){
			if($scope.basketForm.$valid){
				var data = $("#basketForm").serializeArray();
				console.log(data)
				$.ajax({
					type:'POST',
					url:'/api/order-create',
					data:data
				}).done(function(data){
					if(data.result){
            $scope.basket.positions = [];
            window.location.pathname = "/basket/success"
          }
				});
			}
		},
	}
});

mastershina.directive('changename', function () {
  return {
    require: 'ngModel',
     	link: function(scope, elm, attrs, ctrl) {     		
        ctrl.$parsers.unshift(function(data) {
          ctrl.$setValidity('changename', data.length >= 5);
          return data        
        });
    }
  };
});

mastershina.directive('changemail', function () {
  return {
    require: 'ngModel',
     	link: function(scope, elm, attrs, ctrl) {     		
        ctrl.$parsers.unshift(function(data) {					
          ctrl.$setValidity('changemail', /^([\w+\.+\-+]{3,})@([\w+]{3,})\.([\w+]{2,3})$/g.test(data));
          return data 
        });
    }
  };
});

mastershina.directive('changephone', function () {
  return {
    require: 'ngModel',
     	link: function(scope, elm, attrs, ctrl) {     		
        ctrl.$parsers.unshift(function(data) {
          ctrl.$setValidity('changephone', /^(\+7)?([\d]{7,11})$/.test(data));
          return data 
        });
    }
  };
});

mastershina.directive('changeaddress', function () {
  return {
    require: 'ngModel',
     	link: function(scope, elm, attrs, ctrl) {     		
        ctrl.$parsers.unshift(function(data) {					
          if (data.length > 6) {
            ctrl.$setValidity('changeaddress', true);
          } else {
            ctrl.$setValidity('changeaddress', false);
          }
          return data 
        });
    }
  };
});