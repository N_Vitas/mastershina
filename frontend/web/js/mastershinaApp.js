var mastershina = angular.module('mastershinaApp', []);

mastershina.controller('AppController',function($scope,$http,$window){
	$http.defaults.headers.post["Content-Type"] = "application/json";
	$http.get('/api/basket').then(function(succes){
		if(succes.status == 200){
			$scope.basket.positions = succes.data;
			// обновляем на случай если колличество перевалит за наличие
			for (i in $scope.basket.positions){
				$scope.basket.updateCart($scope.basket.positions[i]);
			}
		}
	},
	function(err){
		console.log(err);
	}
	)
	$scope.t = function(x) {
    x = x.toString();var p = /(-?\d+)(\d{3})/; while(p.test(x))x = x.replace(p, '$1 $2');return x+" тг.";
  }
	$scope.basket = {
		timer : null,
		positions : [],
		delivery: $window.localStorage.getItem('delivery','samovyvos'),
		city:'Алматинская область',
		type:'Post Exspres',   
    payment:'cash',
    payment_label:{
			cash:'Наличными',
			credit:'В кредит',
    },
    deliveryTotal:0,
    priceTotal:0,
    delivery_label:{
      samovyvos:'Самовывоз',
      city_square:'По Алматы - 2 000тг.',
      region:'В другие города',
      type:'Сервис доставки',
      city:'Выберите город',
    },
    delivery_time:{
    	'Актау':'от 5 до 6 дней',
			'Актобе':'от 5 до 6 дней',
			'Алматинская область':'от 6 до 8 дней',
			'Астана':'от 2 до 3 дней',
			'Атырау':'от 5 до 6 дней',
			'Балхаш':'от 2 до 3 дней',
			'Жезказган':'от 5 до 6 дней',
			'Караганда':'от 2 до 3 дней',
			'Кокшетау':'от 3 до 4 дней',
			'Костанай':'от 3 до 4 дней',
			'Кызылорда':'от 3 до 5 дней',
			'Павлодар':'от 4 до 5 дней',
			'Петропавловск':'от 4 до 5 дней',
			'Семей':'от 4 до 5 дней',
			'Талдыкорган':'2 дня',
			'Тараз':'2 дня',
			'Темиртау':'от 2 до 3 дней',
			'Уральск':'от 5 до 6 дней',
			'Усть-Каменогорск':'от 4 до 5 дней',
			'Шымкент':'от 2 до 3 дней',
			'Экибастуз':'от 4 до 5 дней',
    },    
    delivery_option:{
      samovyvos:0,
      city_square:2000,
      region:[
      	{title:'Актау','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Актобе','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Алматинская область','Post Exspres':1300,DPD:1500,srok:'от 6 до 8 дней'},
				{title:'Астана','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Атырау','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Балхаш','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Жезказган','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Караганда','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Кокшетау','Post Exspres':1300,DPD:1500,srok:'от 3 до 4 дней'},
				{title:'Костанай','Post Exspres':1300,DPD:1500,srok:'от 3 до 4 дней'},
				{title:'Кызылорда','Post Exspres':1300,DPD:1500,srok:'от 3 до 5 дней'},
				{title:'Павлодар','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Петропавловск','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Семей','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Талдыкорган','Post Exspres':1300,DPD:1500,srok:'2 дня'},
				{title:'Тараз','Post Exspres':1300,DPD:1500,srok:'2 дня'},
				{title:'Темиртау','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Уральск','Post Exspres':1300,DPD:1500,srok:'от 5 до 6 дней'},
				{title:'Усть-Каменогорск','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
				{title:'Шымкент','Post Exspres':1300,DPD:1500,srok:'от 2 до 3 дней'},
				{title:'Экибастуз','Post Exspres':1300,DPD:1500,srok:'от 4 до 5 дней'},
      ],
    },
		total:function(){	
			$window.localStorage.setItem('delivery',$scope.basket.delivery)
			$scope.basket.priceTotal = 0;
			$scope.basket.deliveryTotal = 0;	
			for (i in $scope.basket.positions){
				if($scope.basket.payment == 'cash')
					$scope.basket.priceTotal += $scope.basket.positions[i].price * $scope.basket.positions[i].count;
				else
					$scope.basket.priceTotal += $scope.basket.positions[i].old_price * $scope.basket.positions[i].count;
				if($scope.basket.delivery == 'region')
					$scope.basket.deliveryTotal += $scope.basket.delivery_option.region[0][$scope.basket.type] * $scope.basket.positions[i].count;
			}
			if($scope.basket.delivery != 'region' && $scope.basket.priceTotal <= 50000)
				$scope.basket.deliveryTotal += $scope.basket.delivery_option[$scope.basket.delivery];
			return $scope.basket.priceTotal;
		},
		putCart:function(id,cat,quantity=1){
			$http({method: 'GET', url: '/api/basket-put',params:{id:id,cat:cat,quantity:quantity}}).
			then(function(success) {
				if(success.status == 200){
					$scope.basket.positions = success.data;
				}
			},
			function(err){console.log('putCart',err)});
		},
		deleteCart:function(position){
			$http({method: 'GET', url: '/api/basket-delete',params:{position:position}}).
			then(function(success) {
				console.log(success)
				if(success.status == 200){
					$scope.basket.positions = success.data;
				}
			},
			function(err){console.log(err)});
		},
		updateCart:function(item){
			if(parseInt(item.ostatok) > 0 && parseInt(item.count) >= parseInt(item.ostatok)){
				item.count = parseInt(item.ostatok)					
			}
			if(parseInt(item.count) > parseInt(item.ostatok))
				return
			console.log(item)
			if($scope.basket.timer != null){
				clearTimeout($scope.basket.timer);				
			}
			$scope.basket.timer = setTimeout(function(){
				if(item.count > 0)
					$http({method: 'GET', url: '/api/basket-update',params:item});
			},300)			
		},
		up:function(item,quantity = 1){
			console.log(item)
			if(parseInt(item.count) >= parseInt(item.ostatok))
				return
			item.count = parseInt(item.count) + quantity;
			$scope.basket.updateCart(item);
		},
		down:function(item,quantity = 1){
			item.count = parseInt(item.count) - quantity;
			$scope.basket.updateCart(item);
		}
		// sendData : function(t) {
		// 	var data = $("#order-form").serialize();
		// 	$http.post("/cart/create/", data).
		// 	success(function(data, status, headers, config) {
		// 		if (data.errors != undefined){
		// 			$scope.cart.errors = data.errors;
		// 		}
		// 		else {
		// 			document.location.href='/thank-you/';
		// 		}
		// 	});
		// }
	};
})