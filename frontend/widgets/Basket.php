<?php

namespace frontend\widgets;
use \yii\web\View;

class Basket extends \yii\base\Widget
{
    public function run()
    {
      return $this->render('basket');
    }
}