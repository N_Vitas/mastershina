<?php

namespace frontend\widgets;

use Yii;
use common\models\SearchDisc;

class SearchFormDisc extends \yii\base\Widget
{
	public $model;

  public function init(){
  	if($this->model == null)
  		$this->model = new SearchDisc();
  }

	public function run()
	{
		return $this->render("searchFormDisc",[
			'model'=>$this->model,
		]);
	}
}