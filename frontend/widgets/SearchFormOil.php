<?php

namespace frontend\widgets;

use Yii;
use common\models\SearchOil;

class SearchFormOil extends \yii\base\Widget
{
	public $model;

  public function init(){
  	if($this->model == null)
  		$this->model = new SearchOil();
  }

	public function run()
	{
		return $this->render("searchFormOil",[
			'model'=>$this->model,
		]);
	}
}