<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php 
//начало многосточной строки, можно использовать любые кавычки
$script = "
  var availableTags = ".json_encode($model->getBrand()).";
  console.log('TEST',availableTags)
  $( '#changebrend' ).autocomplete({
    source: availableTags
  });

  var form_data = $('#search-oil-form');
  var sorter = $('#changesort');
	var size = $('#changesize');
	var viscosity_1 = $('#changeviscosity_1');
	var type_fluid = $('#changetype_fluid');
	var type_engine = $('#changefacturer');
  form_data.on('change',function(e){
    
    var url = '/oil/filter/'+
		'?sorter='+sorter.val()+
		'&size='+size.val()+
		'&viscosity_1='+viscosity_1.val()+
		'&type_fluid='+type_fluid.val()+
		'&type_engine='+type_engine.val();

		$.get(url , function(response){
      var r = JSON.parse(response);
      var sorter_html,size_html,viscosity_1_html,type_fluid_html,type_engine_html;

      for(var i in r.sorter){var sorter_sel = sorter.val() == r.sorter[i]['key'] ? 'selected' : ''; sorter_html += '<option '+sorter_sel+' value=\"'+r.sorter[i]['key']+'\">'+r.sorter[i]['val']+'</option>';}
			for(var i in r.size){var size_sel = size.val() == r.size[i]['key'] ? 'selected' : ''; size_html += '<option '+size_sel+' value=\"'+r.size[i]['key']+'\">'+r.size[i]['val']+'</option>';}
			for(var i in r.viscosity_1){var viscosity_1_sel = viscosity_1.val() == r.viscosity_1[i]['key'] ? 'selected' : ''; viscosity_1_html += '<option '+viscosity_1_sel+' value=\"'+r.viscosity_1[i]['key']+'\">'+r.viscosity_1[i]['val']+'</option>';}
			for(var i in r.type_fluid){var type_fluid_sel = type_fluid.val() == r.type_fluid[i]['key'] ? 'selected' : ''; type_fluid_html += '<option '+type_fluid_sel+' value=\"'+r.type_fluid[i]['key']+'\">'+r.type_fluid[i]['val']+'</option>';}
			for(var i in r.type_engine){var type_engine_sel = type_engine.val() == r.type_engine[i]['key'] ? 'selected' : ''; type_engine_html += '<option '+type_engine_sel+' value=\"'+r.type_engine[i]['key']+'\">'+r.type_engine[i]['val']+'</option>';}
   		for(var i in r.width){var width_sel = width.val() == r.width[i]['key'] ? 'selected': ''; width_html += '<option '+width_sel+' value=\"'+r.width[i]['key']+'\">'+r.width[i]['val']+'</option>';}	
			
			if(/*e.target.name != 'sorter' &&*/ sorter.val() == 'all'){sorter.html(sorter_html)}
			if(/*e.target.name != 'size' &&*/ size.val() == 'all'){size.html(size_html)}
			if(/*e.target.name != 'viscosity_1' &&*/ viscosity_1.val() == 'all'){viscosity_1.html(viscosity_1_html)}
			if(/*e.target.name != 'type_fluid' &&*/ type_fluid.val() == 'all'){type_fluid.html(type_fluid_html)}
			if(/*e.target.name != 'type_engine' &&*/ type_engine.val() == 'all'){type_engine.html(type_engine_html)}
    });
  })
  $('#submit').on('click',function(){
  	var sorter = $('#changesort').val();
		var size = $('#changesize').val();
		var viscosity_1 = $('#changeviscosity_1').val();
		var type_fluid = $('#changetype_fluid').val();
		var type_engine = $('#changefacturer').val();
		var brend = $('#changebrend').val();

    var url = '/oil/';
    url += 'sorter-'+sorter+';'
		if(size != 'all'){url += 'size-'+size+';'}
		if(viscosity_1 != 'all'){url += 'viscosity_1-'+viscosity_1+';'}
		if(type_fluid != 'all'){url += 'type_fluid-'+type_fluid+';'}
		if(type_engine != 'all'){url += 'type_engine-'+type_engine+';'}
    if(brend != ''){url += 'brend-'+brend+';'}
    window.location.pathname = url;
  })
  // Перезагрузка страницы
  $('.reset').on('click',function(){
    window.location.pathname = '/oil/all'
  })

";
$this->registerJs($script, yii\web\View::POS_READY);
?>
<div class="title"><span>Подобрать масло</span><span class="pull-right reset">Сбросить фильтр</span></div>
<?= Html::beginForm(Url::toRoute(['/oil/index']), 'get', ['id' => 'search-oil-form']) ?>
<div class="tab-content tab-content-detail">
  <div class="tab-pane fade in active" id="params">
    <div class="well">
      <div class="ui-widget">
        <div class="form-group">
          <label><?= $model->getAttributeLabel('brend')?></label>
          <input type="text" name="brand" id="changebrend" class="form-control" value="<?= $model->brend;?>" />
        </div>
      </div>
	    <div class="row">
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('size'), 'size') ?>
						<?= Html::dropDownList('size', $model->size, $model->getSize(),['class' => 'form-control','id'=>'changesize']) ?>
	        </div>    
	      </div>

	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('viscosity_1'), 'viscosity_1') ?>
						<?= Html::dropDownList('viscosity_1', $model->viscosity_1, $model->getViscosity(),['class' => 'form-control','id'=>'changeviscosity_1']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('type_fluid'), 'type_fluid') ?>
						<?= Html::dropDownList('type_fluid', $model->type_fluid, $model->getType(),['class' => 'form-control','id'=>'changetype_fluid']) ?>
	        </div>    
	      </div>

	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('type_engine'), 'type_engine') ?>
						<?= Html::dropDownList('type_engine', $model->type_engine, $model->getTypeEngine(),['class' => 'form-control','id'=>'changefacturer']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('sorter'), 'sorter') ?>
						<?= Html::dropDownList('sorter', $model->sorter, $model->getSort(),['class' => 'form-control','id'=>'changesort']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3 pull-right">
	        <?= Html::button('Подобрать',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'submit']) ?>
	      </div>
	    </div>
	    <div class="row">
	    </div>
	  </div>
  </div>
</div>
<?= Html::endForm() ?>