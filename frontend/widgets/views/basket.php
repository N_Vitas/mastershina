<button type="button" class="btn btn-default dropdown-toggle" id="dropdown-cart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
  <i class="fa fa-shopping-cart"></i> Товаров в корзине : {{basket.positions.length}} шт <i class="fa fa-caret-down"></i>
</button>
<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-cart" ng-show="basket.positions.length>0">
  <div ng-repeat="pos in basket.positions">
    <div class="media">
      <div class="media-left">
        <a href="/basket" ng-if="pos.url != undefined"><img class="media-object img-thumbnail" src="{{pos.url}}" width="50" alt="product"/></a>
      </div>
      <div class="media-body">
        <a href="/basket" class="media-heading" ng-bind="pos.title"></a>
        <div ng-bind-template="{{pos.count}} x {{t(pos.price)}}"></div>
      </div>
      <div class="media-right"><a href="#" onclick="return false" ng-click="basket.deleteCart(pos.position)" data-toggle="tooltip" title="Remove"><i class="fa fa-trash"></i></a></div>
    </div>  
  </div>
  <div class="subtotal-cart">Итого: <span ng-bind="t(basket.total())"></span></div>
  <div class="text-center">
    <div class="btn-group" role="group" aria-label="View Cart and Checkout Button">
      <a href="/basket" class="btn btn-default btn-sm" type="button"><i class="fa fa-shopping-cart"></i> Перйти к оформлению</a>
    </div>
  </div>
</div>
