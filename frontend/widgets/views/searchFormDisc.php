<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php 
//начало многосточной строки, можно использовать любые кавычки
$script = "
  var availableTags = ".json_encode($model->getBrand()).";
  console.log('TEST',availableTags)
  $( '#changebrend' ).autocomplete({
    source: availableTags
  });
  var form_data = $('#search-disc-form');  
  var width = $('#changewidth'); 
  var wradius = $('#changewradius'); 
  var count = $('#changecount'); 
  var borer = $('#changeborer'); 
  var stupica = $('#changestupica'); 
  var radius = $('#changeradius'); 
  var facturer = $('#changefacturer');
  var color = $('#changecolor'); 
  var type = $('#changetype');
  var vendor = $('#changevendor');
  var car = $('#changecar'); 
  var year = $('#changeyear');
  var modification = $('#changemodification');  

  function getCheckboxValue(obj){
    var r=[];
    obj.each(function(index){
      if($(this).is(':checked')){        
        r.push($(this).val());
      }
    });
    return r;
  }

  if(localStorage.getItem('filter') != null){
    $('#myTab a[href=\"#'+localStorage.getItem('filter')+'\"]').tab('show')
  }
  $('#myTab a').on('click',function (e) {
    e.preventDefault()
    localStorage.setItem('filter',e.target.href.split('#')[1]);
    $(this).tab('show');
    form_data.find('select :selected').attr('selected',false)
  });

  $('.changetype').on('change',function(){
    $('.changetype').removeClass('action');
    $(this).addClass('action');    
  })
  form_data.on('change',function(e){
    var target = $(e.target);
    if(target.hasClass('changetype')){
      form_data.find('select :selected').attr('selected',false)
      console.log('changetype')
    }
    var url = '/disc/filter/'+
		'?width='+width.val()+
		'&wradius='+wradius.val()+
		'&count='+count.val()+
		'&borer='+borer.val()+
		'&stupica='+stupica.val()+
		'&radius='+radius.val()+
		'&facturer='+facturer.val()+
		'&color='+color.val()+
		'&type='+getCheckboxValue($('.changetype'))+
		'&vendor='+vendor.val()+
		'&car='+car.val()+
		'&year='+year.val()+
		'&modification='+modification.val();

		$.get(url , function(response){
      var r = JSON.parse(response);
      var width_html,wradius_html,count_html,borer_html,stupica_html,radius_html,facturer_html,color_html,vendor_html,car_html,year_html,modification_html;
   // 		for(var i in r.width){var width_sel = width.val() == r.width[i]['key'] ? 'selected': ''; width_html += '<option '+width_sel+' value=\"'+r.width[i]['key']+'\">'+r.width[i]['val']+'</option>';}	
			// for(var i in r.wradius){var wradius_sel = wradius.val() == r.wradius[i]['key'] ? 'selected': ''; wradius_html += '<option '+wradius_sel+' value=\"'+r.wradius[i]['key']+'\">'+r.wradius[i]['val']+'</option>';}		
			// for(var i in r.count){var count_sel = count.val() == r.count[i]['key'] ? 'selected': ''; count_html += '<option '+count_sel+' value=\"'+r.count[i]['key']+'\">'+r.count[i]['val']+'</option>';}	
			// for(var i in r.borer){var borer_sel = borer.val() == r.borer[i]['key'] ? 'selected': ''; borer_html += '<option '+borer_sel+' value=\"'+r.borer[i]['key']+'\">'+r.borer[i]['val']+'</option>';}	
			// for(var i in r.stupica){var stupica_sel = stupica.val() == r.stupica[i]['key'] ? 'selected': ''; stupica_html += '<option '+stupica_sel+' value=\"'+r.stupica[i]['key']+'\">'+r.stupica[i]['val']+'</option>';}		
			// for(var i in r.radius){var radius_sel = radius.val() == r.radius[i]['key'] ? 'selected': ''; radius_html += '<option '+radius_sel+' value=\"'+r.radius[i]['key']+'\">'+r.radius[i]['val']+'</option>';}		
			// for(var i in r.facturer){var facturer_sel = facturer.val() == r.facturer[i]['key'] ? 'selected': ''; facturer_html += '<option '+facturer_sel+' value=\"'+r.facturer[i]['key']+'\">'+r.facturer[i]['val']+'</option>';}			
			// for(var i in r.color){var color_sel = color.val() == r.color[i]['key'] ? 'selected': ''; color_html += '<option '+color_sel+' value=\"'+r.color[i]['key']+'\">'+r.color[i]['val']+'</option>';}	
			// type
      for(var i in r['vendor']){vendor_html += '<option value=\"'+r['vendor'][i]['key']+'\">'+r['vendor'][i]['val']+'</option>';}
      for(var i in r['car']){car_html += '<option  value=\"'+r['car'][i]['key']+'\">'+r['car'][i]['val']+'</option>';}
      for(var i in r['year']){year_html += '<option value=\"'+r['year'][i]['key']+'\">'+r['year'][i]['val']+'</option>';}
      for(var i in r['modification']){modification_html += '<option value=\"'+r['modification'][i]['key']+'\">'+r['modification'][i]['val']+'</option>';}
			if(e.target.name == 'vendor'){        
        car.find(':selected').attr('selected',false);
        year.find(':selected').attr('selected',false);
        modification.find(':selected').attr('selected',false);
      }
      if(e.target.name == 'car'){
        year.find(':selected').attr('selected',false);
        modification.find(':selected').attr('selected',false);
      }
      if(e.target.name == 'year'){
        modification.find(':selected').attr('selected',false);
      }

      if(e.target.name != 'vendor' && vendor.val() == 'all'){vendor.html(vendor_html);}
      if(e.target.name != 'car' && car.val() == 'all'){car.html(car_html);}
      if(e.target.name != 'year' && year.val() == 'all'){year.html(year_html);}
      if(e.target.name != 'modification' && modification.val() == 'all'){modification.html(modification_html);}
   //    if(e.target.name != 'width' || width.val() == 'all'){width.html(width_html);}		 
			// if(e.target.name != 'wradius' || wradius.val() == 'all'){wradius.html(wradius_html);}	 
			// if(e.target.name != 'countBorer' || count.val() == 'all'){count.html(count_html);}		 
			// if(e.target.name != 'distanceBorer' || borer.val() == 'all'){borer.html(borer_html);}		 
			// if(e.target.name != 'stupica' || stupica.val() == 'all'){stupica.html(stupica_html);}	 
			// if(e.target.name != 'radius' || radius.val() == 'all'){radius.html(radius_html);}	 
			// if(e.target.name != 'manufacturer' || facturer.val() == 'all'){facturer.html(facturer_html);} 
			// if(e.target.name != 'color' || color.val() == 'all'){color.html(color_html);}		 
    });
  });
  $('#submit,#submitwo').on('click',function(){
    var width = $('#changewidth').val();
		var wradius = $('#changewradius').val();
		var count = $('#changecount').val();
		var borer = $('#changeborer').val();
		var stupica = $('#changestupica').val();
		var radius = $('#changeradius').val();
		var facturer = $('#changefacturer').val();
		var color = $('#changecolor').val();
		var type = $('#changetype').find('.action').val();
		var vendor = $('#changevendor').val();
		var car = $('#changecar').val();
		var year = $('#changeyear').val();
		var modification = $('#changemodification').val();
    var brend = $('#changebrend').val();
    console.log(type)
    var url = '/disc/';
    if(width != 'all'){ url += 'width-'+width+';'}
		if(wradius != 'all'){ url += 'wradius-'+wradius+';'}
		if(count != 'all'){ url += 'count-'+count+';'}
		if(borer != 'all'){ url += 'borer-'+borer+';'}
		if(stupica != 'all'){ url += 'stupica-'+stupica+';'}
		if(radius != 'all'){ url += 'radius-'+radius+';'}
		if(facturer != 'all'){ url += 'facturer-'+facturer+';'}
		if(color != 'all'){ url += 'color-'+color+';'}
		if(vendor != 'all'){ url += 'vendor-'+vendor+';'}
		if(car != 'all'){ url += 'car-'+car+';'}
		if(year != 'all'){ url += 'year-'+year+';'}
		if(modification != 'all'){ url += 'modification-'+modification+';'}
    if(brend != ''){url += 'brend-'+brend+';'}
		url += 'type-'+type+';';
    // // url += sort;
    window.location.pathname = url;
  })
  // Перезагрузка страницы
  $('.reset').on('click',function(){
    window.location.pathname = '/disc/all'
  })
";
$this->registerJs($script, yii\web\View::POS_READY);
?>

<div class="title"><span>Подобрать диски</span><span class="pull-right reset">Сбросить фильтр</span></div>
<?= Html::beginForm(Url::toRoute(['/disc/index']), 'get', ['id' => 'search-disc-form']) ?>
<ul id="myTab" class="nav nav-tabs">
  <li class="active"><a href="#params">По параметрам</a></li>
  <li><a href="#auto">По авто</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content tab-content-detail">
  <div class="tab-pane fade in active" id="params">
    <div class="well">
      <div class="ui-widget">
        <div class="form-group">
          <label><?= $model->getAttributeLabel('brend')?></label>
          <input type="text" name="brand" id="changebrend" class="form-control" value="<?= $model->brend;?>" />
        </div>
      </div>
	    <div class="row">
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('width'), 'width') ?>
						<?= Html::dropDownList('width', $model->width, $model->getWidth(),['class' => 'form-control','id'=>'changewidth']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('wradius'), 'wradius') ?>
						<?= Html::dropDownList('wradius', $model->wradius, $model->getRadius(),['class' => 'form-control','id'=>'changewradius']) ?>
	        </div>    
	      </div>

	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('countBorer'), 'countBorer') ?>
						<?= Html::dropDownList('countBorer', $model->countBorer, $model->getCount(),['class' => 'form-control','id'=>'changecount']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('distanceBorer'), 'distanceBorer') ?>
						<?= Html::dropDownList('distanceBorer', $model->distanceBorer, $model->getDistance(),['class' => 'form-control','id'=>'changeborer']) ?>
	        </div>    
	      </div>

	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('stupica'), 'stupica') ?>
						<?= Html::dropDownList('stupica', $model->stupica, $model->getStupica(),['class' => 'form-control','id'=>'changestupica']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('vylet'), 'vylet') ?>
						<?= Html::dropDownList('radius', $model->radius, $model->getVylet(),['class' => 'form-control','id'=>'changeradius']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('manufacturer'), 'manufacturer') ?>
						<?= Html::dropDownList('manufacturer', $model->manufacturer, $model->getManufacturer(),['class' => 'form-control','id'=>'changefacturer']) ?>
	        </div>
	      </div>

	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('color'), 'color') ?>
						<?= Html::dropDownList('color', $model->color, $model->getColor(),['class' => 'form-control','id'=>'changecolor']) ?>
	        </div>
	      </div>
	    </div>
	    <div class="row">
	      <div class="col-md-10 col-lg-9">
		    	<div class="form-group">    
					  <div id="changetype" class="radio">
					    <?php foreach ($model->getType() as $key => $value):?>
					      <label><input class="changetype <?= $model->type == $key ? 'action' : '';?>" <?= $model->type == $key ? 'checked' : '';?> type="radio" name="type" value="<?= $key?>"/><span class="glyphicon glyphicon-ok"></span> <?= $value?></label>
					    <?php endforeach;?>
					  </div>
					</div>
				</div>
	      <div class="col-md-2 col-lg-3 pull-right">
	        <?= Html::button('Подобрать',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'submit']) ?>
	      </div>
	    </div>
	  </div>
  </div>

  <div class="tab-pane fade" id="auto">   
    <div class="well">
      <div class="row">
        <div class="col-md-2 col-lg-2">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('vendor'), 'vendor') ?>
          <?= Html::dropDownList('vendor', $model->vendor,$model->getVendor(),['class' => 'form-control','id'=>'changevendor']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-2">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('car'), 'car') ?>
          <?= Html::dropDownList('car', $model->car,$model->getCar(),['class' => 'form-control','id'=>'changecar']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-2">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('year'), 'year') ?>
          <?= Html::dropDownList('year', $model->year,$model->getYear(),['class' => 'form-control','id'=>'changeyear']) ?>
          </div>    
        </div>
        <div class="col-md-3 col-lg-3">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('modification'), 'modification') ?>
          <?= Html::dropDownList('modification', $model->modification,$model->getModification(),['class' => 'form-control','id'=>'changemodification']) ?>
          </div>    
        </div>
        <div class="col-md-3 col-lg-3">
          <div class="form-group">
          <?= Html::button('Подобрать',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'submitwo']) ?>
          </div>
        </div>
      </div>
    </div> 
  </div>
</div>
<?= Html::endForm() ?>