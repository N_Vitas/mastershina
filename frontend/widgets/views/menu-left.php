<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>
<div class="title"><span>Категории</span></div>
<?php
  Nav::begin([
    'options' => [
        'class' => '',
        'role'=>"navigation",
    ],
  ]);
  $menuItems = [
    ['label' => 'Шины', 'url' => ['/shiny/index']],
    ['label' => 'Диски', 'url' => ['/disc/index']],
    ['label' => 'Масла', 'url' => ['/oil/index']],
    ['label' => 'Акумуляторы', 'url' => ['/battery/index']],    
  ];

  echo Nav::widget([
    'options' => ['class' => 'nav nav-pills nav-stacked'],
    'items' => $menuItems,
  ]);
  Nav::end();