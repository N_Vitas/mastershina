<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>
<?php
  NavBar::begin([
      // 'brandLabel' => 'My Company',
      // 'brandUrl' => Yii::$app->homeUrl,
      'options' => [
          'class' => 'navbar navbar-default shadow-navbar',
          'role'=>"navigation",
      ],
  ]);
  $menuItems = [
      ['label' => 'Главная', 'url' => ['/home/index']],
      ['label' => 'О компании', 'url' => ['/home/about','/home/sotrudnichestvo','/home/certificat','/home/vakansii'],
        'items' => [
          ['label' => 'О нас','url' => ['/home/about']],
          ['label' => 'Сотрудничество','url' => ['/home/sotrudnichestvo']],
          ['label' => 'Сертификат','url' => ['/home/certificat']],
          ['label' => 'Вакансии','url' => ['/home/vakansii']],
        ]
      ],
      ['label' => 'Услуги', 'url' => ['/service'],
        'items' => [
          ['label' => 'Выездной шиномонтаж','url' => ['/service/vyezdnoy-shinomontazh']],
          ['label' => 'Стационарный шиномонтаж','url' => ['/service/stacionarnyj-shinomontazh']],
          ['label' => 'Сезонное хранение шин','url' => ['/service/sezonnoe-hranenie-shin']],
          ['label' => 'Утилизация шин','url' => ['/service/utilizacija-iznoshennyh-shin']],
        ]
      ],
      ['label' => 'Доставка и оплата', 'url' => ['/service'],
        'items' => [
          ['label' => 'Доставка','url' => ['/service/delivery']],
          ['label' => 'Шины в кредит','url' => ['/service/credit-rassrochka']],
          ['label' => 'Расширенная гарантия','url' => ['/service/rasshirennaja-garantija']],
        ]
      ],
      [
        'label' => 'Прайс-лист',
        'url' => ['/home/price'],
      ],
      ['label' => 'Контакты', 'url' => ['/home/contact']],
  ];

  echo Nav::widget([
      'options' => ['class' => 'navbar-nav'],
      'items' => $menuItems,
  ]);
  NavBar::end();
?>