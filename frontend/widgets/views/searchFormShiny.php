<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

if($model->width){$this->params['breadcrumbs'][]=['label'=>$model->_width[$model->width],'url'=>['/shiny/shirina-'.$model->width]];}
if($model->height){$this->params['breadcrumbs'][]=['label'=>$model->_height[$model->height],'url'=>['/shiny/vysota-'.$model->height]];}
if($model->radius){$this->params['breadcrumbs'][]=['label'=>$model->_radius[$model->radius],'url'=>['/shiny/diametr-'.$model->radius]];}
if($model->season){$this->params['breadcrumbs'][]=['label'=>$model->_season[$model->season],'url'=>['/shiny/'.$model->season]];}
if($model->loads){$this->params['breadcrumbs'][]=['label'=>$model->_loads[$model->loads],'url'=>['/shiny/loads-'.$model->loads]];}
if($model->speed){$this->params['breadcrumbs'][]=['label'=>$model->_speed[$model->speed],'url'=>['/shiny/speed-'.$model->speed]];}
if($model->typeTC){$this->params['breadcrumbs'][]=['label'=>$model->_typeTC[$model->typeTC],'url'=>['/shiny/'.$model->typeTC]];}
?>

<?php 
//начало многосточной строки, можно использовать любые кавычки
$script = <<< JS
  var form_data = $('#search-shiny-form');
  var width = $('#changewidth'); 
  var height = $('#changeheight'); 
  var radius = $('#changeradius'); 
  var season = $('#changeseason'); 
  var loads = $('#changeloads'); 
  var speed = $('#changespeed'); 
  var typeTC = $('#changetypeTC');

  // $('#changewidth').on('change',function(){
  //   $.get('/shiny/size/?width='+$(this).val(), function(response) {
  //     var r = JSON.parse(response);var html;
  //     for(var i in r){
  //       console.log(r,i)
  //       html += '<option value="'+r[i]['key']+'">'+r[i]['val']+'</option>';
  //     }
  //     $('#changeheight').html(html);
  //   });
  // });
  form_data.on('change',function(e){  
    $.get('/shiny/size/?attr='+e.target.name+'&height='+height.val()+'&width='+width.val()+'&radius='+radius.val()+'&season='+season.val()+'&loads='+loads.val()+'&speed='+speed.val()+'&typeTC='+typeTC.val() , function(response) {
      var r = JSON.parse(response);
      var width_html,height_html,radius_html,season_html,loads_html,speed_html,typeTC_html;
      for(var i in r['width']){var width_sel = width.val() == r['width'][i]['key'] ? 'selected': ''; width_html += '<option '+width_sel+' value="'+r['width'][i]['key']+'">'+r['width'][i]['val']+'</option>';}
      for(var i in r['height']){var height_sel = height.val() == r['height'][i]['key'] ? 'selected': ''; height_html += '<option '+height_sel+' value="'+r['height'][i]['key']+'">'+r['height'][i]['val']+'</option>';}
      for(var i in r['radius']){var radius_sel = radius.val() == r['radius'][i]['key'] ? 'selected': ''; radius_html += '<option '+radius_sel+' value="'+r['radius'][i]['key']+'">'+r['radius'][i]['val']+'</option>';}
      for(var i in r['season']){var season_sel = season.val() == r['season'][i]['key'] ? 'selected': ''; season_html += '<option '+season_sel+' value="'+r['season'][i]['key']+'">'+r['season'][i]['val']+'</option>';}
      for(var i in r['loads']){var loads_sel = loads.val() == r['loads'][i]['key'] ? 'selected': ''; loads_html += '<option '+loads_sel+' value="'+r['loads'][i]['key']+'">'+r['loads'][i]['val']+'</option>';}
      for(var i in r['speed']){var speed_sel = speed.val() == r['speed'][i]['key'] ? 'selected': ''; speed_html += '<option '+speed_sel+' value="'+r['speed'][i]['key']+'">'+r['speed'][i]['val']+'</option>';}
      for(var i in r['typeTC']){var typeTC_sel = typeTC.val() == r['typeTC'][i]['key'] ? 'selected': ''; typeTC_html += '<option '+typeTC_sel+' value="'+r['typeTC'][i]['key']+'">'+r['typeTC'][i]['val']+'</option>';}
      if(e.target.name != 'width' || width.val() == 'all'){width.html(width_html);}
      if(e.target.name != 'height' || height.val() == 'all'){height.html(height_html);}
      if(e.target.name != 'radius' || radius.val() == 'all'){radius.html(radius_html);}
      if(e.target.name != 'season' || season.val() == 'all'){season.html(season_html);}
      if(e.target.name != 'loads' || loads.val() == 'all'){loads.html(loads_html);}
      if(e.target.name != 'speed' || speed.val() == 'all'){speed.html(speed_html);}
      if(e.target.name != 'typeTC' || typeTC.val() == 'all'){typeTC.html(typeTC_html);}
    });
  });

  $('#submit').on('click',function(){
    var sort = $('#changesort').val(); var width = $('#changewidth').val(); var height = $('#changeheight').val(); var radius = $('#changeradius').val(); var season = $('#changeseason').val(); var loads = $('#changeloads').val(); var speed = $('#changespeed').val(); var typeTC = $('#changetypeTC').val(); 
    console.log(sort,width,height,radius,season,typeTC);
    var url = '/shiny/';
    if(width != 'all'){ url += 'shirina-'+width+';'; }
    if(height != 'all'){ url += 'vysota-'+height+';' }
    if(radius != 'all'){ url += 'diametr-'+radius+';' }
    if(season != 'all'){ url += season+';' }
    if(loads != 'all'){ url += 'loads-'+loads+';' }
    if(speed != 'all'){ url += 'speed-'+speed+';' }
    if(typeTC != 'all'){ url += typeTC+';' }
    // url += sort;
    window.location.pathname = url;
  })
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<div class="title"><span>Подобрать шины</span></div>
<?= Html::beginForm(Url::toRoute(['/shiny/index']), 'get', ['id' => 'search-shiny-form']) ?>
<!-- <div class="form-group"> -->
<?php // Html::label($model->getAttributeLabel('sorter'), 'sorter') ?>
<?php // Html::dropDownList('sorter', $model->sorter, $model->getSort(),['class' => 'form-control selectpicker1','id'=>'changesort']) ?>
<!-- </div> -->
<div class="form-group">
<?= Html::label($model->getAttributeLabel('width'), 'width') ?>
<?= Html::dropDownList('width', $model->width,$model->getWidth(),['class' => 'form-control selectpicker1','id'=>'changewidth']) ?>
</div>
<div class="form-group">
<?= Html::label($model->getAttributeLabel('height'), 'height') ?>
<?= Html::dropDownList('height', $model->height,$model->getHeight(),['class' => 'form-control selectpicker1','id'=>'changeheight']) ?>
</div>
<div class="form-group">
<?= Html::label($model->getAttributeLabel('radius'), 'radius') ?>
<?= Html::dropDownList('radius', $model->radius,$model->getRadius(),['class' => 'form-control selectpicker1','id'=>'changeradius']) ?>
</div>
<div class="form-group">
<?= Html::label($model->getAttributeLabel('season'), 'season') ?>
<?= Html::dropDownList('season', $model->season, $model->getSeason(),['class' => 'form-control selectpicker1','id'=>'changeseason']) ?>
</div>
<div class="form-group">
<?= Html::label($model->getAttributeLabel('loads'), 'loads') ?>
<?= Html::dropDownList('loads', $model->loads, $model->getLoads(),['class' => 'form-control selectpicker1','id'=>'changeloads']) ?>
</div>
<div class="form-group">
<?= Html::label($model->getAttributeLabel('speed'), 'speed') ?>
<?= Html::dropDownList('speed', $model->speed, $model->getSpeed(),['class' => 'form-control selectpicker1','id'=>'changespeed']) ?>
</div>
<div class="form-group">
<?= Html::label($model->getAttributeLabel('typeTC'), 'typeTC') ?>
<?= Html::dropDownList('typeTC', $model->typeTC, $model->getTypeTC(),['class' => 'form-control selectpicker1','id'=>'changetypeTC']) ?>
</div>
<div class="form-group">
<?= Html::button('Подобрать шины',['class'=>'btn btn-block btn-primary','id'=>'submit']) ?>
</div>
<?= Html::endForm() ?>