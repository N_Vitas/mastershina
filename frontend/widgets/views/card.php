<?php
if($model->old_price > 0 && $model->price > 0)
  $pr = round(100 - ($model->price * 100 / $model->old_price),0,PHP_ROUND_HALF_UP);
else
  $pr = 0;
$starone = $rating['rating'] > 0 ? '':'-o';
$startwo = $rating['rating'] > 1 ? '':'-o';
$startree = $rating['rating'] > 2 ? '':'-o';
$starfour = $rating['rating'] > 3 ? '':'-o';
$starfive = $rating['rating'] > 4 ? '':'-o';
?>
<div class="box-product">
  <header class="box-product-card-header">
    <i class="fa fa-snowflake-o font-themes"></i>
  </header>
  <div class="img-wrapper">
    <a href="<?= $link?>">
      <img alt="Product" src="<?= $model->picture?>">
    </a>
    <?php if(count($featured)):?>glyphicon glyphicon-asterisk
      <div class="tags">
        <span class="label-tags"><span class="label label-<?= $featured['style']?> arrowed"><?= $featured['title']?></span></span>
      </div>
    <?php endif;?>
    <?php if(count($sale)):?>
      <div class="tags tags-left">
        <span class="label-tags"><span class="label label-<?= $sale['style']?> arrowed-right"><?= $sale['title']?></span></span>
      </div>
    <?php endif;?>
    <div class="option">
      <a href="#" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart font-themes"></i></a>
    </div>
  </div>
  <h6 class="cardTitle"><a href="<?= $link?>"><?= $model->title?></a></h6>
  <?php if($showPrice && $showOldPrice):?>
    <div class="price">
      <div><?= number_format($model->price,0,',',' ')?> <span class="label-tags"><span class="label label-default">-<?=$pr?>%</span></span></div>
      <span class="price-old"><?= number_format($model->old_price, 0, ',', ' ')?></span>
    </div>
  <?php endif;?>
  <?php if($showPrice && !$showOldPrice):?>
    <div class="price">
      <span><?= $model->price?></span>
    </div>
  <?php endif;?>
  <?php if(count($rating)):?>
    <div class="rating">
      <i class="fa fa-star<?= $starone?>"></i>
      <i class="fa fa-star<?= $startwo?>"></i>
      <i class="fa fa-star<?= $startree?>"></i>
      <i class="fa fa-star<?= $starfour?>"></i>
      <i class="fa fa-star<?= $starfive?>"></i>
      <a href="#">(<?=$rating['count']?> отзывов)</a>
    </div>
  <?php endif;?>
</div>