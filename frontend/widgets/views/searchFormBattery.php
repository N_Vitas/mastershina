<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php 
//начало многосточной строки, можно использовать любые кавычки
$script = "
  var availableTags = ".json_encode($model->getBrand()).";
  console.log('TEST',availableTags)
  $( '#changebrend' ).autocomplete({
    source: availableTags
  });
  var form_data = $('#search-battery-form');
  var capacity = $('#changecapacity');
	var current = $('#changecurrent');
	var polarity = $('#changepolarity');
	var length = $('#changelength');
	var width = $('#changewidth');
	var height = $('#changeheight');
	var country = $('#changecountry');
	var assurance = $('#changeassurance');
	var voltage = $('#changevoltage');
	var sorter = $('#changesorter');
  form_data.on('change',function(e){
    
    var url = '/battery/filter/'+

		'?sorter='+sorter.val()+
    '&capacity='+capacity.val()+
		'&current='+current.val()+
		'&polarity='+polarity.val()+
		'&length='+length.val()+
		'&width='+width.val()+
		'&height='+height.val()+
		'&country='+country.val()+
		'&assurance='+assurance.val()+
		'&voltage='+voltage.val();

		$.get(url , function(response){
      var r = JSON.parse(response);
      var capacity_html,current_html,polarity_html,length_html,width_html,height_html,country_html,assurance_html,voltage_html;
      for(var i in r.capacity){var capacity_sel = capacity.val() == r.capacity[i]['key'] ? 'selected' : ''; capacity_html += '<option '+capacity_sel+' value=\"'+r.capacity[i]['key']+'\">'+r.capacity[i]['val']+'</option>';} 
			for(var i in r.current){var current_sel = current.val() == r.current[i]['key'] ? 'selected' : ''; current_html += '<option '+current_sel+' value=\"'+r.current[i]['key']+'\">'+r.current[i]['val']+'</option>';} 
			for(var i in r.polarity){var polarity_sel = polarity.val() == r.polarity[i]['key'] ? 'selected' : ''; polarity_html += '<option '+polarity_sel+' value=\"'+r.polarity[i]['key']+'\">'+r.polarity[i]['val']+'</option>';} 
			for(var i in r.length){var length_sel = length.val() == r.length[i]['key'] ? 'selected' : ''; length_html += '<option '+length_sel+' value=\"'+r.length[i]['key']+'\">'+r.length[i]['val']+'</option>';} 
			for(var i in r.width){var width_sel = width.val() == r.width[i]['key'] ? 'selected' : ''; width_html += '<option '+width_sel+' value=\"'+r.width[i]['key']+'\">'+r.width[i]['val']+'</option>';} 
			for(var i in r.height){var height_sel = height.val() == r.height[i]['key'] ? 'selected' : ''; height_html += '<option '+height_sel+' value=\"'+r.height[i]['key']+'\">'+r.height[i]['val']+'</option>';} 
			for(var i in r.country){var country_sel = country.val() == r.country[i]['key'] ? 'selected' : ''; country_html += '<option '+country_sel+' value=\"'+r.country[i]['key']+'\">'+r.country[i]['val']+'</option>';} 
			for(var i in r.assurance){var assurance_sel = assurance.val() == r.assurance[i]['key'] ? 'selected' : ''; assurance_html += '<option '+assurance_sel+' value=\"'+r.assurance[i]['key']+'\">'+r.assurance[i]['val']+'</option>';} 
			for(var i in r.voltage){var voltage_sel = voltage.val() == r.voltage[i]['key'] ? 'selected' : ''; voltage_html += '<option '+voltage_sel+' value=\"'+r.voltage[i]['key']+'\">'+r.voltage[i]['val']+'</option>';} 

			if(/*e.target.name != 'capacity' && */capacity.val() == 'all'){capacity.html(capacity_html)}
			if(/*e.target.name != 'current' && */current.val() == 'all'){current.html(current_html)}
			if(/*e.target.name != 'polarity' && */polarity.val() == 'all'){polarity.html(polarity_html)}
			if(/*e.target.name != 'length' && */length.val() == 'all'){length.html(length_html)}
			if(/*e.target.name != 'width' && */width.val() == 'all'){width.html(width_html)}
			if(/*e.target.name != 'height' && */height.val() == 'all'){height.html(height_html)}
			if(/*e.target.name != 'country' && */country.val() == 'all'){country.html(country_html)}
			if(/*e.target.name != 'assurance' && */assurance.val() == 'all'){assurance.html(assurance_html)}
			if(/*e.target.name != 'voltage' && */voltage.val() == 'all'){voltage.html(voltage_html)}
    });
  })
  $('#submit').on('click',function(){
  	var sorter = $('#changesorter').val();
  	var capacity = $('#changecapacity').val();
		var current = $('#changecurrent').val();
		var polarity = $('#changepolarity').val();
		var length = $('#changelength').val();
		var width = $('#changewidth').val();
		var height = $('#changeheight').val();
		var country = $('#changecountry').val();
		var assurance = $('#changeassurance').val();
		var voltage = $('#changevoltage').val();
		var brend = $('#changebrend').val();

    var url = '/battery/';
    url += 'sorter-'+sorter+';'
    if(capacity != 'all'){url += 'capacity-'+capacity+';'}
		if(current != 'all'){url += 'current-'+current+';'}
		if(polarity != 'all'){url += 'polarity-'+polarity+';'}
		if(length != 'all'){url += 'length-'+length+';'}
		if(width != 'all'){url += 'width-'+width+';'}
		if(height != 'all'){url += 'height-'+height+';'}
		if(country != 'all'){url += 'country-'+country+';'}
		if(assurance != 'all'){url += 'assurance-'+assurance+';'}
		if(voltage != 'all'){url += 'voltage-'+voltage+';'}
    if(brend != ''){url += 'brend-'+brend+';'}
    window.location.pathname = url;
  })
  // Перезагрузка страницы
  $('.reset').on('click',function(){
    window.location.pathname = '/battery/all'
  })
";
$this->registerJs($script, yii\web\View::POS_READY);
?>
<div class="title"><span>Подобрать акумулятор</span><span class="pull-right reset">Сбросить фильтр</span></div>
<?= Html::beginForm(Url::toRoute(['/oil/index']), 'get', ['id' => 'search-battery-form']) ?>
<div class="tab-content tab-content-detail">
  <div class="tab-pane fade in active" id="params">
    <div class="well">
      <div class="ui-widget">
        <div class="form-group">
          <label><?= $model->getAttributeLabel('brend')?></label>
          <input type="text" name="brand" id="changebrend" class="form-control" value="<?= $model->brend;?>" />
        </div>
      </div>
	    <div class="row">
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('capacity'), 'capacity') ?>
						<?= Html::dropDownList('capacity', $model->capacity, $model->getCapacity(),['class' => 'form-control','id'=>'changecapacity']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('current'), 'current') ?>
						<?= Html::dropDownList('current', $model->current, $model->getCurrent(),['class' => 'form-control','id'=>'changecurrent']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('polarity'), 'polarity') ?>
						<?= Html::dropDownList('polarity', $model->polarity, $model->getPolarity(),['class' => 'form-control','id'=>'changepolarity']) ?>
	        </div>    
	      </div>

	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('length'), 'length') ?>
						<?= Html::dropDownList('length', $model->length, $model->getLength(),['class' => 'form-control','id'=>'changelength']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('width'), 'width') ?>
						<?= Html::dropDownList('width', $model->width, $model->getWidth(),['class' => 'form-control','id'=>'changewidth']) ?>
	        </div>    
	      </div> 
				<div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('height'), 'height') ?>
						<?= Html::dropDownList('height', $model->height, $model->getHeight(),['class' => 'form-control','id'=>'changeheight']) ?>
	        </div>    
	      </div> 
				<div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('country'), 'country') ?>
						<?= Html::dropDownList('country', $model->country, $model->getCountry(),['class' => 'form-control','id'=>'changecountry']) ?>
	        </div>    
	      </div> 
				<div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('assurance'), 'assurance') ?>
						<?= Html::dropDownList('assurance', $model->assurance, $model->getAssurance(),['class' => 'form-control','id'=>'changeassurance']) ?>
	        </div>    
	      </div> 
				<div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('voltage'), 'voltage') ?>
						<?= Html::dropDownList('voltage', $model->voltage, $model->getVoltage(),['class' => 'form-control','id'=>'changevoltage']) ?>
	        </div>    
	      </div> 
	      <div class="col-md-2 col-lg-3">
	        <div class="form-group">
						<?= Html::label($model->getAttributeLabel('sorter'), 'sorter') ?>
						<?= Html::dropDownList('sorter', $model->sorter, $model->_sorter,['class' => 'form-control','id'=>'changesorter']) ?>
	        </div>    
	      </div>
	      <div class="col-md-2 col-lg-3 pull-right">
	        <?= Html::button('Подобрать',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'submit']) ?>
	      </div>
	    </div>
	    <div class="row">
	    </div>
	  </div>
  </div>
</div>
<?= Html::endForm() ?>