<?php
if($model->old_price > 0 && $model->price > 0)
  $pr = round(100 - ($model->price * 100 / $model->old_price),0,PHP_ROUND_HALF_UP);
else
  $pr = 0;
$starone = $rating['rating'] > 0 ? '':'-o';
$startwo = $rating['rating'] > 1 ? '':'-o';
$startree = $rating['rating'] > 2 ? '':'-o';
$starfour = $rating['rating'] > 3 ? '':'-o';
$starfive = $rating['rating'] > 4 ? '':'-o';
?>
<div class="box-product">
  <p>Код <?= $model->code?></p>
  <?php if(isset($model->season) && isset($model->ship)):?>
  <div class="season-icon">    
    <?php if($model->season == 'Зимние' || $model->season == 'Всесезонные'):?>
    <img class="icon" src="/images/shiny/1.png"/>
    <?php endif;?>
    <?php if($model->season == 'Летние' || $model->season == 'Всесезонные'):?>
    <img class="icon" src="/images/shiny/2.png"/>
    <?php endif;?>
    <?php if($model->ship == 1 || $model->ship == 2):?>
    <img class="icon" src="/images/shiny/4.png"/>
    <?php endif;?>
  </div>
  <?php endif;?>
  <div class="img-wrapper">
    <a href="<?= $link?>">
      <img alt="Product" src="<?= $model->getPicture()?>">
    </a>
    <?php if(count($featured)):?>
      <div class="tags">
        <span class="label-tags"><span class="label label-<?= $featured['style']?> arrowed"><?= $featured['title']?></span></span>
      </div>
    <?php endif;?>
    <?php if(count($sale)):?>
      <div class="tags tags-left">
        <span class="label-tags"><span class="label label-<?= $sale['style']?> arrowed-right"><?= $sale['title']?></span></span>
      </div>
    <?php endif;?>
    <!-- <div class="option">
      <a href="#" onclick="return false" data-toggle="tooltip" ng-click="basket.putCart(<?= $model->id;?>,'<?= $category?>')" title="Add to Cart"><i class="fa fa-shopping-cart font-themes"></i></a>
    </div> -->
  </div>
  <h6 class="cardTitle"><a style="font-size:16px;" href="<?= $link?>"><?= $model->brend->title." ".$model->title?></a></h6>
  <p style="margin:0px 0px 2px"><?= isset($model->width,$model->height,$model->radius)?$model->width."/".$model->height."/".$model->radius:'';?></p>
    <p style="margin:0px 0px 2px"><?= isset($model->season)?$model->season:'';?> <?= isset($model->speed)?$model->speed/*."  ".$model->loads*/:'';?> <?= isset($model->loads)?$model->loads:'';?></p>
  <?php if($showPrice && $showOldPrice):?>
    <div class="price">
      <div><?= number_format($model->price,0,',',' ')?> тг.<span style="color:#666666;" class="label-tags pull-right"> в наличии <?= $model->ostatok?><!-- <span class="label label-default">-<?=$pr?>%</span> --></span></div>
     <!--  <span class="price-old"><?= number_format($model->old_price, 0, ',', ' ')?></span> -->
    </div>
  <?php endif;?>
  <?php if($showPrice && !$showOldPrice):?>
    <div class="price">
      <span><?= $model->price?></span>
    </div>
  <?php endif;?>
  <?php if(count($rating)):?>
    <div class="rating">
      <i class="fa fa-star<?= $starone?>"></i>
      <i class="fa fa-star<?= $startwo?>"></i>
      <i class="fa fa-star<?= $startree?>"></i>
      <i class="fa fa-star<?= $starfour?>"></i>
      <i class="fa fa-star<?= $starfive?>"></i>
      <a href="#">(<?=$rating['count']?> отзывов)</a>
    </div>
  <?php endif;?>
  <a href="#" onclick="return false" class="btn btn-theme btn-block" ng-click="basket.putCart(<?= $model->id;?>,'<?= $category?>')" title="Add to Cart"><i class="fa fa-shopping-cart font-themes"></i> В корзину</a>
</div>