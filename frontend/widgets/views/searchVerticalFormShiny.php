<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

if($model->width){$this->params['breadcrumbs'][]=['label'=>$model->_width[$model->width],'url'=>['/shiny/shirina-'.$model->width]];}
if($model->height){$this->params['breadcrumbs'][]=['label'=>$model->_height[$model->height],'url'=>['/shiny/vysota-'.$model->height]];}
if($model->radius){$this->params['breadcrumbs'][]=['label'=>$model->_radius[$model->radius],'url'=>['/shiny/diametr-'.$model->radius]];}
if($model->season){$this->params['breadcrumbs'][]=['label'=>$model->_season[$model->season],'url'=>['/shiny/'.$model->season]];}
if($model->loads){$this->params['breadcrumbs'][]=['label'=>$model->_loads[$model->loads],'url'=>['/shiny/loads-'.$model->loads]];}
// if($model->speed){$this->params['breadcrumbs'][]=['label'=>$model->_speed[$model->speed],'url'=>['/shiny/speed-'.$model->speed]];}
if($model->typeTC){$this->params['breadcrumbs'][]=['label'=>$model->_typeTC[$model->typeTC],'url'=>['/shiny/'.$model->typeTC]];}
?>

<?php
//начало многосточной строки, можно использовать любые кавычки
$script = "
  var availableTags = ".json_encode($model->getBrand()).";
  console.log('TEST',availableTags)
  $( '#changebrend' ).autocomplete({
    source: availableTags
  });

  if(localStorage.getItem('filter') != null){
    $('#myTab a[href=\"#'+localStorage.getItem('filter')+'\"]').tab('show')
  }

  var form_data = $('#search-shiny-form');
  var width = $('#changewidth'); 
  var height = $('#changeheight'); 
  var radius = $('#changeradius'); 
  var season = $('#changeseason'); 
  var loads = $('#changeloads'); 
  var speed = $('#changespeed'); 
  var typeTC = $('#changetypeTC');
  var vendor = $('#changevendor');
  var car = $('#changecar'); 
  var year = $('#changeyear');
  var modification = $('#changemodification');  
  var runflat = $('.changerunflat'); 
  var ship = $('.changeship');

  $('#myTab a').on('click',function (e) {
    e.preventDefault()
    localStorage.setItem('filter',e.target.href.split('#')[1]);
    $(this).tab('show');
    form_data.find('select :selected').attr('selected',false)
  });
   $('.changeseason').on('change',function(){
    $('.changeseason').removeClass('action');
    $(this).addClass('action');    
  })
  $('.changetypeTC','.changerunflat','.changeship').on('change',function(){
    if($(this).hasClass('action'))
      $(this).removeClass('action');
    else
      $(this).addClass('action');
  })
  
  $('.filter_toggle').on('click',function(){
      if($(this).attr('toggle') == 'down'){
        $(this).html('<span class=\"glyphicon glyphicon-chevron-up\"></span> Меньше фильтров');
        $(this).attr('toggle','up');
      }
      else{
        $(this).html('<span class=\"glyphicon glyphicon-chevron-down\"></span> Больше фильтров');
        $(this).attr('toggle','down');
      }
      $('.filter_more').slideToggle();
    }
  );
  function getCheckboxValue(obj){
    var r=[];
    obj.each(function(index){
      if($(this).is(':checked')){        
        r.push($(this).val());
      }
    });
    return r;
  }
  form_data.on('submit',function(e){
    e.preventDefault() 
  })
  form_data.on('change',function(e){     
    e.preventDefault()
    var target = $(e.target);
    if(target.hasClass('changetypeTC') || target.hasClass('changerunflat') || target.hasClass('changeship') || target.hasClass('changeseason'))
    form_data.find('select :selected').attr('selected',false)
    var url = '/shiny/size/?height='+height.val()+
    '&width='+width.val()+
    '&radius='+radius.val()+
    '&season='+season.find('.action').val()+
    '&loads='+loads.val()+
    '&speed='+getCheckboxValue($('.changespeed'))+
    '&typeTC='+getCheckboxValue($('.changetypeTC'))+
    '&runflat='+getCheckboxValue(runflat)+
    '&vendor='+vendor.val()+
    '&car='+car.val()+
    '&year='+year.val()+
    '&modification='+modification.val()+
    '&ship='+getCheckboxValue(ship);

    $.get(url , function(response){
      var r = JSON.parse(response);
      // form_data.find('select').attr('disabled',false);
      var width_html,height_html,radius_html,season_html,loads_html,speed_html,typeTC_html,vendor_html,car_html,year_html,modification_html;

      for(var i in r['vendor']){vendor_html += '<option value=\"'+r['vendor'][i]['key']+'\">'+r['vendor'][i]['val']+'</option>';}
      for(var i in r['car']){car_html += '<option  value=\"'+r['car'][i]['key']+'\">'+r['car'][i]['val']+'</option>';}
      for(var i in r['year']){year_html += '<option value=\"'+r['year'][i]['key']+'\">'+r['year'][i]['val']+'</option>';}
      for(var i in r['modification']){modification_html += '<option value=\"'+r['modification'][i]['key']+'\">'+r['modification'][i]['val']+'</option>';}

      for(var i in r['width']){var width_sel = width.val() == r['width'][i]['key'] ? 'selected': ''; width_html += '<option '+width_sel+' value=\"'+r['width'][i]['key']+'\">'+r['width'][i]['val']+'</option>';}
      for(var i in r['height']){var height_sel = height.val() == r['height'][i]['key'] ? 'selected': ''; height_html += '<option '+height_sel+' value=\"'+r['height'][i]['key']+'\">'+r['height'][i]['val']+'</option>';}
      for(var i in r['radius']){var radius_sel = radius.val() == r['radius'][i]['key'] ? 'selected': ''; radius_html += '<option '+radius_sel+' value=\"'+r['radius'][i]['key']+'\">'+r['radius'][i]['val']+'</option>';}
      // for(var i in r['season']){var season_sel = season.val() == r['season'][i]['key'] ? 'selected': ''; season_html += '<option '+season_sel+' value=\"'+r['season'][i]['key']+'\">'+r['season'][i]['val']+'</option>';}
      for(var i in r['loads']){var loads_sel = loads.val() == r['loads'][i]['key'] ? 'selected': ''; loads_html += '<option '+loads_sel+' value=\"'+r['loads'][i]['key']+'\">'+r['loads'][i]['val']+'</option>';}
      for(var i in r['speed']){var speed_sel = speed.val() == r['speed'][i]['key'] ? 'selected': ''; speed_html += '<option '+speed_sel+' value=\"'+r['speed'][i]['key']+'\">'+r['speed'][i]['val']+'</option>';}
      for(var i in r['typeTC']){var typeTC_sel = typeTC.val() == r['typeTC'][i]['key'] ? 'selected': ''; typeTC_html += '<option '+typeTC_sel+' value=\"'+r['typeTC'][i]['key']+'\">'+r['typeTC'][i]['val']+'</option>';}

      if(e.target.name == 'vendor'){        
        car.find(':selected').attr('selected',false);
        year.find(':selected').attr('selected',false);
        modification.find(':selected').attr('selected',false);
      }
      if(e.target.name == 'car'){
        year.find(':selected').attr('selected',false);
        modification.find(':selected').attr('selected',false);
      }
      if(e.target.name == 'year'){
        modification.find(':selected').attr('selected',false);
      }

      if(e.target.name == 'width'){        
        height.find(':selected').attr('selected',false);
        radius.find(':selected').attr('selected',false);
      }
      if(e.target.name == 'height'){
        radius.find(':selected').attr('selected',false);
      }

      if(e.target.name != 'vendor' && vendor.val() == 'all'){vendor.html(vendor_html);}
      if(e.target.name != 'car' && car.val() == 'all'){car.html(car_html);}
      if(e.target.name != 'year' && year.val() == 'all'){year.html(year_html);}
      if(e.target.name != 'modification' && modification.val() == 'all'){modification.html(modification_html);}

      if(e.target.name != 'width' || width.val() == 'all'){width.html(width_html);}
      if(e.target.name != 'height' || height.val() == 'all'){height.html(height_html);}
      if(e.target.name != 'radius' || radius.val() == 'all'){radius.html(radius_html);}
      // if(e.target.name != 'season' || season.val() == 'all'){season.html(season_html);}
      // if(e.target.name != 'loads' || loads.val() == 'all'){loads.html(loads_html);}
      // if(e.target.name != 'speed' || speed.val() == 'all'){speed.html(speed_html);}
      // if(e.target.name != 'typeTC' || typeTC.val() == 'all'){typeTC.html(typeTC_html);}
    });
  });

  $('#submit').on('click',function(){
    var sort = $('#changesort').val(); var width = $('#changewidth').val(); var height = $('#changeheight').val(); var radius = $('#changeradius').val(); var season = $('#changeseason').find('.action').val(); var loads = $('#changeloads').val(); var speed = getCheckboxValue($('.changespeed')); var typeTC = getCheckboxValue($('.changetypeTC')); 
    var ship = $('.changeship').is(':checked');var podship = $('.changepodship').is(':checked'); var runflat = $('.changerunflat').is(':checked');
    var brend = $('#changebrend').val();
    console.log(sort,width,height,radius,season,typeTC);
    var url = '/shiny/';
    if(width != 'all'){ url += 'shirina-'+width+';'; }
    if(height != 'all'){ url += 'vysota-'+height+';' }
    if(radius != 'all'){ url += 'diametr-'+radius+';' }
    if(season != 'all'){ url += season+';' }
    // if(loads != 'all'){ url += 'loads-'+loads+';' }
    // if(speed != ''){ url += 'speed-'+speed+';' }
    if(typeTC != ''){ url += typeTC+';' }
    if(runflat != ''){ url += 'runflat;'}
    console.log(podship)
    if(ship && podship){ url += 'allship;'}
    if(ship && !podship){ url += 'ship;'}
    if(!ship && podship){ url += 'podship;'}
    if(brend != ''){url += 'brend-'+brend+';'}
    // url += sort;
    window.location.pathname = url;
  })

  $('#submitwo').on('click',function(){
    var sort = $('#changesort').val(); var modification = $('#changemodification').val(); var vendor = $('#changevendor').val(); var car = $('#changecar').val(); var year = $('#changeyear').val(); var season = $('#changeseason').find('.action').val(); var loads = $('#changeloads').val(); var speed = getCheckboxValue($('.changespeed')); var typeTC = getCheckboxValue($('.changetypeTC')); 
    var ship = $('.changeship').is(':checked');var podship = $('.changepodship').is(':checked'); var runflat = $('.changerunflat').is(':checked');
    var url = '/shiny/';
    if(vendor != 'all'){ url += 'make-'+vendor+';'; }
    if(car != 'all'){ url += 'car-'+car+';' }
    if(year != 'all'){ url += 'year-'+year+';' }
    if(modification != 'all'){ url += 'modification-'+modification+';' }
    if(season != 'all'){ url += season+';' }
    // if(loads != 'all'){ url += 'loads-'+loads+';' }
    // if(speed != ''){ url += 'speed-'+speed+';' }
    if(typeTC != ''){ url += typeTC+';' }
    if(runflat != ''){ url += 'runflat;'}
    if(ship && podship){ url += 'allship;'}
    if(ship && !podship){ url += 'ship;'}
    if(!ship && podship){ url += 'podship;'}
    // url += sort;
    window.location.pathname = url;
  })

  $('.reset').on('click',function(){
    window.location.pathname = '/shiny/all'
  })
";
$this->registerJs($script, yii\web\View::POS_READY);
?>
<div class="title"><span>Подобрать шины</span><span class="pull-right reset">Сбросить фильтр</span></div>
<?= Html::beginForm(Url::toRoute(['/shiny/index']), 'get', ['id' => 'search-shiny-form']) ?>
  <div class="form-group">    
    <div id="changetypeTC" class="radio">
      <?php foreach ($model->getTypeTC() as $key => $value):?>
        <label><input class="changetypeTC <?= $model->typeTC == $key ? 'action' : '';?>" <?= $model->typeTC == $key ? 'checked' : '';?> type="radio" name="typeTC" value="<?= $key?>"/><span class="glyphicon glyphicon-ok"></span> <?= $value?></label>               
      <?php endforeach;?>
    </div>
  </div>

<ul id="myTab" class="nav nav-tabs">
  <li class="active"><a href="#params">По параметрам</a></li>
  <li><a href="#auto">По авто</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content tab-content-detail">
  <div class="tab-pane fade in active" id="params">
    <div class="well">
      <div class="ui-widget">
        <div class="form-group">
          <label><?= $model->getAttributeLabel('brend')?></label>
          <input type="text" name="brand" id="changebrend" class="form-control" value="<?= $model->brend;?>" />
        </div>
      </div>
      <div class="row">
        <div class="col-md-2 col-lg-3">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('width'), 'width') ?>
          <?= Html::dropDownList('width', $model->width,$model->getWidth(),['class' => 'form-control','id'=>'changewidth']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-3">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('height'), 'height') ?>
          <?= Html::dropDownList('height', $model->height,$model->getHeight(),['class' => 'form-control','id'=>'changeheight']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-3">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('radius'), 'radius') ?>
          <?= Html::dropDownList('radius', $model->radius,$model->getRadius(),['class' => 'form-control','id'=>'changeradius']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-3">
          <div class="form-group">
          <?= Html::button('Подобрать',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'submit']) ?>
          </div>
        </div>
        <!-- <div class="col-md-2 col-lg-3">
           <div class="form-group">
           <?= Html::button('Сброс',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'reset']) ?>
           </div>
         </div> -->
      </div>
    </div>
  </div>

  <div class="tab-pane fade" id="auto">  
    <div class="well">
      <div class="row">
        <div class="col-md-2 col-lg-2">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('vendor'), 'vendor') ?>
          <?= Html::dropDownList('vendor', $model->vendor,$model->getVendor(),['class' => 'form-control','id'=>'changevendor']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-2">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('car'), 'car') ?>
          <?= Html::dropDownList('car', $model->car,$model->getCar(),['class' => 'form-control','id'=>'changecar']) ?>
          </div>    
        </div>
        <div class="col-md-2 col-lg-2">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('year'), 'year') ?>
          <?= Html::dropDownList('year', $model->year,$model->getYear(),['class' => 'form-control','id'=>'changeyear']) ?>
          </div>    
        </div>
        <div class="col-md-3 col-lg-3">
          <div class="form-group">
          <?= Html::label($model->getAttributeLabel('modification'), 'modification') ?>
          <?= Html::dropDownList('modification', $model->modification,$model->getModification(),['class' => 'form-control','id'=>'changemodification']) ?>
          </div>    
        </div>
        <div class="col-md-3 col-lg-3">
          <div class="form-group">
          <?= Html::button('Подобрать',['style'=>'margin-top:25px;','class'=>'btn btn-block btn-theme','id'=>'submitwo']) ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div id="changeseason" class="col-md-12 col-lg-12 radio">
    <label><input class="changeseason <?= $model->season == 'all' || $model->season == '' ? 'action' : '';?>" <?= $model->season == 'all' || $model->season == '' ? 'checked' : '';?> type="radio" name="season" value="all"/><span class="glyphicon glyphicon-ok"></span> Все</label>
    <label><input class="changeseason <?= $model->season == 'vsesezonnye' ? 'action' : '';?>" <?= $model->season == 'vsesezonnye' ? 'checked' : '';?> type="radio" name="season" value="vsesezonnye"/><span class="glyphicon glyphicon-ok"></span> Всесезонные</label>
    <label><input class="changeseason <?= $model->season == 'zimnie' ? 'action' : '';?>" <?= $model->season == 'zimnie' ? 'checked' : '';?> type="radio" name="season" value="zimnie"/><span class="glyphicon glyphicon-ok"></span> Зимние</label>
    <label><input class="changeseason <?= $model->season == 'letnie' ? 'action' : '';?>" <?= $model->season == 'letnie' ? 'checked' : '';?> type="radio" name="season" value="letnie"/><span class="glyphicon glyphicon-ok"></span> Летние</label>

    <label class="labelchange"><input class="changerunflat <?= $model->runflat ? 'action' : '';?>" <?= $model->runflat ? 'checked' : '';?> type="checkbox" name="runflat" value="runflat"/><span class="glyphicon glyphicon-ok"></span> Runflat (<?= mb_strtolower($model->getAttributeLabel('runflat'))?>)</label>
    <label class="labelchange"><input class="changeship <?= $model->ship == 1 || $model->ship == 2? 'action' : '';?>" <?= $model->ship == 1 || $model->ship == 2? 'checked' : '';?> type="checkbox" name="ship" value="ship"/><span class="glyphicon glyphicon-ok"></span> <?= $model->getAttributeLabel('ship')?></label>
    <label class="labelchange"><input class="changepodship <?= $model->ship == 3 || $model->ship == 2? 'action' : '';?>" <?= $model->ship == 3 || $model->ship == 2? 'checked' : '';?> type="checkbox" name="podship" value="podship"/><span class="glyphicon glyphicon-ok"></span> <?= $model->getAttributeLabel('podship')?></label>

    <!-- <label class="filter_toggle pull-right" toggle="down"><span class="glyphicon glyphicon-chevron-down"></span> Больше фильтров</label> -->
  </div>
</div>
<?php /*
<div class="row filter_more">
  <div class="col-md-4 col-lg-4">
    <div class="form-group">
      <?= Html::label($model->getAttributeLabel('loads'), 'loads') ?>
      <!-- <div id="changeloads" class="radio">
      <?php/* foreach ($model->getLoads() as $key => $value):?>
        <label><input class="changeloads <?= isset($model->loads[$key]) ? 'action' : '';?>" <?= isset($model->loads[$key]) ? 'checked' : '';?> type="checkbox" name="loads[]" value="<?= $key?>"/><span class="glyphicon glyphicon-ok"></span> <?= $value?></label>               
      <?php endforeach;*/?><?php /*
      </div> -->
      <?= Html::dropDownList('loads', $model->loads, $model->getLoads(),['class' => 'form-control','id'=>'changeloads']) ?>
    </div>
  </div>
  <div class="col-md-8 col-lg-8">
    <div class="form-group">
      <?= Html::label($model->getAttributeLabel('speed'), 'speed') ?>
      <div id="changespeed" class="radio">
      <?php foreach ($model->getSpeed() as $key => $value):?>
        <label><input class="changespeed <?= isset($model->speed[$key]) ? 'action' : '';?>" <?= isset($model->speed[$key]) ? 'checked' : '';?> type="checkbox" name="speed[]" value="<?= $key?>"/><span class="glyphicon glyphicon-ok"></span> <?= $value?></label>               
      <?php endforeach;?>
      </div>
    </div>
  </div>
</div>
*/?>
<?= Html::endForm() ?>