<div class="container">
  <div class="row">
    <div class="col-md-4 col-sm-6">
      <div class="title-footer"><span>О компании</span></div>
      <ul>
        <li>
          Шинный центр <a href="/home/about">Мастершина</a> - продажа шин и дисков, хранение шин, стационарный и выездной шиномонтаж. Доставка во все города Казахстана.
        </li>
      </ul>
    </div>
    <div class="col-md-4 col-sm-6">
      <div class="title-footer"><span>Информация</span></div>
      <ul>
        <li><i class="fa fa-angle-double-right"></i> <a href="/home/price">Прайс</a></li>
        <li><i class="fa fa-angle-double-right"></i> <a href="/home/dogovor-hranenija">Договор хранения</a></li>
        <li><i class="fa fa-angle-double-right"></i> <a href="/home/uslovija-vozvrata">Условия возврата шин</a></li>
        <li><i class="fa fa-angle-double-right"></i> <a href="/service/rasshirennaja-garantija">Расширенная гарантия</a></li>
      </ul>
    </div>
    <div class="clearfix visible-sm-block"></div>
    <div class="col-md-4 col-sm-6">
      <div class="title-footer"><span>Категории</span></div>
      <ul>
        <li><i class="fa fa-angle-double-right"></i> <a href="/shiny">Шины</a></li>
        <li><i class="fa fa-angle-double-right"></i> <a href="/disc">Диски</a></li>
        <li><i class="fa fa-angle-double-right"></i> <a href="/oil">Масла</a></li>
        <li><i class="fa fa-angle-double-right"></i> <a href="/battery">Аккумуляторы</a></li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 col-sm-6">
      <div class="title-footer"><span>Контакты</span></div>
      <ul class="footer-icon">
        <li><span><i class="fa fa-map-marker"></i></span> г.Алматы, ул. Садовникова, 99</li>
        <li><span><i class="fa fa-phone"></i></span> <a href="tel:+77273906404">+7 727 390 64 04</a></li>
        <li><span><i class="fa fa-phone"></i></span> <a href="tel:+77017157157">+7 701 7 157 157</a></li>
        <li><span><i class="fa fa-envelope"></i></span> <a href="mailto:info@mastershina.kz">info@mastershina.kz</a></li>
      </ul>
    </div>
    <div class="col-md-4 col-sm-6">
      <div class="title-footer"><span>Мы в соц. сетях</span></div>
      <div>Мастершина</div>
      <ul class="follow-us">
        <li><a target="_blank" href="https://www.facebook.com/Mastershina.Kazakstan/"><i class="fa fa-facebook"></i></a></li>
        <li><a target="_blank" href="https://www.instagram.com/mastershina_kz/"><i class="fa fa-instagram"></i></a></li>
        <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-rss"></i></a></li> -->
      </ul>
      <div>Выездной шиномонтаж</div>
      <ul class="follow-us">
        <li><a target="_blank" href="https://www.facebook.com/viezdnoy.shinomontaj"><i class="fa fa-facebook"></i></a></li>
        <li><a target="_blank" href="https://www.instagram.com/viezdnoy_shinomontaj"><i class="fa fa-instagram"></i></a></li>
        <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-rss"></i></a></li> -->
      </ul>
    </div>
    <div class="clearfix visible-sm-block"></div>
    <div class="col-md-4 col-sm-6">
      <div class="title-footer"><span>Способы оплаты</span></div>
      <p>Принимаем наличные и карточки в кассе магазина. А также онлайн оплату.</p>
      <img src="/images/payment-1.png" alt="Payment-1">
      <img src="/images/payment-2.png" alt="Payment-2">
      <!-- <img src="/images/payment-3.png" alt="Payment-3"> -->
      <img src="/images/payment-4.png" alt="Payment-4">
      <!-- <img src="/images/payment-5.png" alt="Payment-5"> -->
    </div>
  </div>
</div>
<div class="text-center copyright">
  ТОО "МАСТЕРШИНА" &copy; <?= date("Y")?> , РЕСПУБЛИКА КАЗАХСТАН, АЛМАТЫ.
</div>