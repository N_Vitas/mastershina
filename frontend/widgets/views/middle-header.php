<div class="container">
  <div class="row">
    <div class="col-md-3 logo">
      <a href="/"><img alt="Logo" src="/images/logo-<?= \frontend\assets\AppAsset::$style?>.png" class="img-responsive" /></a>
    </div>
    <div class="col-sm-8 col-md-6 search-box m-t-2">
      
    </div>
    <div class="col-sm-4 col-md-3 cart-btn hidden-xs m-t-2">
      <?= \frontend\widgets\Basket::widget();?>
    </div>
  </div>
</div>