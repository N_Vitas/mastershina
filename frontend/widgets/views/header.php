<?php
// use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\LoginForm;

$auth = new LoginForm();
/*
if (Yii::$app->user->isGuest) {
  $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
  $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
  $menuItems[] = '<li>'
      . Html::beginForm(['/site/logout'], 'post')
      . Html::submitButton(
          'Logout (' . Yii::$app->user->identity->username . ')',
          ['class' => 'btn btn-link logout']
      )
      . Html::endForm()
      . '</li>';
}*/
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <ul class="list-inline pull-left">
        <!-- <li>
          <select class="selectpicker" data-width="95px" data-style="btn-default">
            <option value="English" data-content="<img alt='English' src='/images/en.jpg'> English">English</option>
            <option value="French" data-content="<img alt='French' src='/images/fr.jpg'> French">French</option>
          </select>
        </li>
        <li>
          <select class="selectpicker" data-width="70px" data-style="btn-default">
            <option value="USD">$ USD</option>
            <option value="EUR">€ EUR</option>
          </select>
        </li> -->
        <li class="hidden-xs"><a href="tel:+77273906404"><i class="fa fa-phone"></i> +7 727 390 64 04</a></li>
        <li class="hidden-xs"><a href="tel:+77017157157">+7 701 7 157 157</a></li>
        <li class="hidden-xs"><a href="/"><i class="fa fa-clock-o"></i> пн-пт: 9:00&mdash;19:00</a></li>
        <li class="hidden-xs"><a href="/">сб-вс: 10:00&mdash;17:00</a></li>
      </ul>
      <!-- <ul class="list-inline pull-right"> -->
        <!-- <li class="hidden-xs"><a href="wishlist.html"><i class="fa fa-heart"></i> Wishlist (3)</a></li> -->
        <!-- <li class="hidden-xs"><a href="compare.html"><i class="fa fa-align-left"></i> Compare (4)</a></li> -->
        <?php //if(!\Yii::$app->user->isGuest):?>
        <!-- <li>
          <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Добро пожаловать <?= \Yii::$app->user->identity->username?> <span class="caret"></span>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn" data-dropdown-out="fadeOut">
              <?php $form = ActiveForm::begin(['action'=>'/home/logout','id' => 'login-form']); ?>
                <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-long-arrow-right"></i> Выйти</button>
              <?php ActiveForm::end(); ?>
            </div>
          </div>
        </li> -->
        <?php //else:?>
        <!-- <li>
          <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Авторизация <span class="caret"></span>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn" data-dropdown-out="fadeOut">
              <?php $form = ActiveForm::begin(['action'=>'/home/login','id' => 'login-form']); ?>
              <?= $form->field($auth, 'username')->textInput(['placeholder' => 'Логин']) ?>
              <?= $form->field($auth, 'password')->passwordInput(['placeholder' => 'Пароль']) ?>
              <?= $form->field($auth, 'rememberMe',[
                'inputTemplate' => '{input}<span></span>',
              ])->checkbox(['class' => 'checkbox']) ?>
                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-right"></i> Войти</button>
                <a class="btn btn-default btn-sm pull-right" href="register.html" role="button">Регистрация</a>
              <?php ActiveForm::end(); ?>
            </div>
          </div>
        </li> -->
        <?php //endif;?>
      <!-- </ul> -->
    </div>
  </div>
</div>