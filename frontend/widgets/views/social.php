<div class="row top">
  <div class="col-md-12">
    <div class="title"><span>Поделиться</span></div>
    <div class="share-button m-b-3">
      <button type="button" class="btn btn-primary"><i class="fa fa-facebook"></i></button>
      <button type="button" class="btn btn-info"><i class="fa fa-twitter"></i></button>
      <button type="button" class="btn btn-danger"><i class="fa fa-google-plus"></i></button>
      <button type="button" class="btn btn-primary"><i class="fa fa-linkedin"></i></button>
      <button type="button" class="btn btn-warning"><i class="fa fa-envelope"></i></button>
    </div>
  </div>
</div>