<?php

namespace frontend\widgets;
use Yii;

class MenuBottom extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("menu-bottom");
	}
}