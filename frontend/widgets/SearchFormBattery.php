<?php

namespace frontend\widgets;

use Yii;
use common\models\SearchBattery;

class SearchFormBattery extends \yii\base\Widget
{
	public $model;

  public function init(){
  	if($this->model == null)
  		$this->model = new SearchBattery();
  }

	public function run()
	{
		return $this->render("searchFormBattery",[
			'model'=>$this->model,
		]);
	}
}