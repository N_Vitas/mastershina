<?php

namespace frontend\widgets;
use \yii\web\View;

class BrandSlider extends \yii\base\Widget
{
    public function run()
    {
      return $this->render('brandSlider');
    }
}