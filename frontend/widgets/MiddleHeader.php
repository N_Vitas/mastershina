<?php

namespace frontend\widgets;
use Yii;

class MiddleHeader extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("middle-header");
	}
}