<?php

namespace frontend\widgets;

use Yii;
use common\models\SearchShiny;

class SearchFormShiny extends \yii\base\Widget
{
	public $model;
	public $vertical = true;

  public function init(){
  	if($this->model == null)
  		$this->model = new SearchShiny();
  }

	public function run()
	{
		if($this->vertical){
			return $this->render("searchVerticalFormShiny",[
				'model'=>$this->model,
			]);
		}
		return $this->render("searchFormShiny",[
			'model'=>$this->model,
		]);
	}
}