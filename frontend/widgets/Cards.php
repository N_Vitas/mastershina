<?php

namespace frontend\widgets;
use Yii;

class Cards extends \yii\base\Widget
{
	public $model;	
	public $link = '#';
	public $category = 'shiny';
	public $showPrice = true;
	public $showOldPrice = true;
	public $sale = [];
	public $featured = [];
	public $rating = [];

	public function init(){
		if (empty($this->model)) {
      throw new Exception("Error Processing Request", 1);      
    }
	}

	public function run()
	{
		// var_dump($model);
		return $this->render("cards",[
			'model'=>$this->model,
			'link'=>$this->link,
			'showPrice'=>$this->showPrice,
			'showOldPrice'=>$this->showOldPrice,
			'featured'=>$this->featured,
			'sale'=>$this->sale,
			'rating'=>$this->rating,
			'category'=>$this->category,
		]);
	}
}