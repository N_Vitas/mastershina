<?php

namespace frontend\widgets;
use Yii;

class HeaderTop extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("header");
	}
}