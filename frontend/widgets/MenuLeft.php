<?php

namespace frontend\widgets;
use Yii;

class MenuLeft extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("menu-left");
	}
}