<?php

namespace frontend\widgets;

use Yii;

class Social extends \yii\base\Widget
{
	public function run()
	{
		return $this->render("social");
	}
}