<?php

namespace frontend\widgets;
use Yii;

class MenuTop extends \yii\base\Widget
{

	public function run()
	{
		return $this->render("menu-top");
	}
}