<?php
namespace frontend\components;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use common\models\LoginForm;

class BaseController extends Controller{

	public function init(){
		return parent::init();    
	}
	/**
   * @inheritdoc
   */
  public function behaviors()
  {
  	// public $layout = 'landing';
    return array_merge(parent::behaviors(), [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'signup'],
        'rules' => [
          [
            'actions' => ['captcha','index'],
            'allow' => true,
          ],
          [
            'actions' => ['signup'],
            'allow' => true,
            'roles' => ['?'],
          ],
          [
            'actions' => ['logout','login'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ]);
  }

  public function beforeAction($action)
	{
		// Подгрузка JS файлов
		$jsFile = 'js/controllers/' . $this->id . '.js';
		if (is_file($jsFile)) {
			$this->view->registerJsFile($jsFile, ['depends' => '\frontend\assets\AppAsset']);
		}
		$jsFile = 'js/controllers/' . $this->id . '.' . $action->id . '.js';
		if (is_file($jsFile)) {
			$this->view->registerJsFile($jsFile, ['depends' => '\frontend\assets\AppAsset']);
		}

		return parent::beforeAction($action);
	}
  /**
   * @inheritdoc
   */
  public function actions()
  {
    return array_merge(parent::actions(), [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ]);
  }
}
?>