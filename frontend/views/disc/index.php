<?php
use yii\helpers\Url;
use frontend\widgets\MenuLeft;
use frontend\widgets\Cards;
/* @var $this yii\web\View */
$this->title = 'Блог от интернет-магазина МастерШина';
$this->params['breadcrumbs'][] = ['label' => 'Диски', 'url' => ['/disc']];
?>
  <!-- <div class="row">
    <div class="col-md-12">
      <div class="title"><span><a href="products.html">Фильтр <i class="fa fa-chevron-circle-right"></i></a></span></div>
    </div>
  </div> -->
  <div class="row">
    <div class="col-md-3 m-t-1">
      <?= MenuLeft::widget()?>  
      <div class="title"></div>
    </div>
    <!-- End New Arrivals & Best Selling -->
    <div class="clearfix visible-sm visible-xs"></div>
    <div class="col-md-9">
      <?= yii\widgets\Breadcrumbs::widget([
        'homeLink'      =>  [
          'label'     =>  Yii::t('yii', 'Главная'),
          'url'       =>  ['/'],
          // 'class'     =>  'home',
          'template'  =>  '<span class="glyphicon glyphicon-home"></span> {link}',
        ],
        'links' => $this->params['breadcrumbs'],
        'itemTemplate'  =>  ' / {link}',
        // 'tag'           =>  'ul',
      ])?>
      <?= \frontend\widgets\SearchFormDisc::widget(['model'=>$searchModel])?>   
      <!-- Featured -->
      <div class="title"><span>Каталог Дисков</span></div>
      <?= yii\widgets\ListView::widget([        
        'dataProvider' => $disc,
        'itemView' => function($model){
          return Cards::widget([
            'category'=>'disc',
            'model'=>$model,
            'link'=> '/disc/detail/'.$model->url,
            // 'sale'=>['title'=> 'Акция','style'=>'danger'],
            // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
            // 'rating'=>['count'=>5,'rating'=>4],
          ]);
        },
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-sm-4 col-lg-3 box-product-outer',
        ],
        'layout' => "{summary}<div class='clearfix'></div>{items}<div class='clearfix'></div>{pager}", // "{sorter}\n{summary}\n{items}\n{pager}
        'summary' => "Показано {count} из {totalCount}",
        'emptyText' => 'Ничего не найдено',

      ]);?>
      <!-- End Featured -->
      <div class="clearfix"></div>
      <!-- Collection -->
      <div class="title m-t-2"><span>Рекомендуем к этому товару</span></div>
      <div class="product-slider owl-controls-top-offset">
        <?php foreach ($shiny as $model):?>
          <div class="box-product-outer">            
            <?= Cards::widget([
              'category'=>'shiny',
              'model'=>$model,
              'link'=> '/shiny/detail/'.$model->url,
              // 'sale'=>['title'=> 'Акция','style'=>'danger'],
              // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
              // 'rating'=>['count'=>5,'rating'=>4],
            ])?>
          </div>
        <?php endforeach;?>
      </div>
      <!-- End Collection -->
    </div>
  </div>