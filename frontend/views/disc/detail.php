<?php
use frontend\widgets\MenuLeft;
use frontend\widgets\Social;
use frontend\widgets\Basket;
use yii\helpers\Url;
use frontend\widgets\Cards;
use common\models\Disc;
use common\models\Service;

$this->params['breadcrumbs'][] = ['label' => 'Диски', 'url' => ['/disc/index']];

// $width='',$wradius='',$count='',$borer='',$stupica='',$radius='',$facturer='',$color='',$vendor='',$type='',$car='',$year='',$modification=''
// if($model->size){$this->params['breadcrumbs'][]=['label'=>$model->size,'url'=>['/disc/'.$model->generateUrl($model->size)]];}
// if($model->borer){$this->params['breadcrumbs'][]=['label'=>$model->borer,'url'=>['/disc/shirina-'.$model->generateUrl($model->borer)]];}
// if($model->stupica){$this->params['breadcrumbs'][]=['label'=>$model->stupica,'url'=>['/disc/vysota-'.$model->generateUrl($model->stupica)]];}
// if($model->radius){$this->params['breadcrumbs'][]=['label'=>$model->radius,'url'=>['/disc/diametr-'.$model->generateUrl($model->radius)]];}
// if($model->color){$this->params['breadcrumbs'][]=['label'=>$model->color,'url'=>['/disc/diametr-'.$model->generateUrl($model->color)]];}
// if($model->brend->title){$this->params['breadcrumbs'][]=['label'=>$model->brend->title,'url'=>['/disc/brend-'.$model->brend->url]];}
// if($model->title){$this->params['breadcrumbs'][]=['label'=>$model->title,'url'=>['/disc/speed-'.$model->generateUrl($model->model)]];}


$this->registerJs("mastershina.controller('DiscController', function() {
  var disc = this; 
  disc.ostatok = ".json_encode($model->ostatok == 'Нет в наличии' ? 0 : preg_replace("/\D/", "", $model->ostatok))."  
  disc.coll = parseInt(disc.ostatok) > 4 ? 4 : parseInt(disc.ostatok);
  disc.price = ".json_encode($model->price)."
  disc.priceOld = ".json_encode($model->old_price)."
  disc.bstyle = 'danger'
  // калькуляция
  disc.total = 0 // Общая сумма шин

  if(disc.ostatok > 0){disc.bstyle = 'success'}
  if(disc.ostatok > 0){disc.sklad = disc.ostatok}else{disc.sklad = 'нет'}

  disc.summer= function(){
    // Если вдруг кол шин больше чем остаток на складе
    if(disc.coll > parseInt(disc.ostatok)){
      disc.coll = disc.ostatok;
    }
    disc.total = disc.coll * disc.price;
  }

  disc.summer();

})", yii\web\View::POS_END);
?>
<div class="row" ng-controller="DiscController as disc"> 
  <div class="col-md-3"><?= MenuLeft::widget()?></div>
  <!-- End New Arrivals & Best Selling -->
  <div class="clearfix visible-sm visible-xs"></div>
  <div class="col-md-9">
    <div class="row">
    <?= yii\widgets\Breadcrumbs::widget([
      'homeLink'      =>  [
        'label'     =>  Yii::t('yii', 'Главная'),
        'url'       =>  ['/'],
        // 'class'     =>  'home',
        'template'  =>  '<span class="glyphicon glyphicon-home"></span> {link}',
      ],
      'links' => $this->params['breadcrumbs'],
      'itemTemplate'  =>  ' / {link}',
      // 'tag'           =>  'ul',
    ])?>
    <div class="clearfix"></div>
      <!-- Image List -->
      <div class="col-sm-3">
        <div class="image-detail">      
          <img src="<?= $model->getPicture()?>" alt="">
        </div>
      </div>
      <!-- End Image List -->
      <div class="col-sm-9">
        <div class="title-detail"><?= $model->brend->title." ".$model->title." ".$model->size." ".$model->borer." ".$model->radius?></div>
        <table class="table table-detail">
          <tbody>
            <tr>
              <td>Цена</td>
              <td>
                <div class="price">
                  <div ng-bind-template="{{t(disc.price)}} со скидкой"></div>
                  <span class="price-old" ng-bind-template="{{t(disc.priceOld)}} в кредит"></span>
                </div>
              </td>
            </tr>
            <tr>
              <td>В наличии</td>
              <td>
                <!-- <div class="row">
                  <div class="col-md-2 col-sx-2"><span class="label label-{{disc.bstyle}} arrowed">{{disc.sklad}}</span></div>
                  <div class="col-md-10 col-sx-10">
                  </div>
                </div>  -->               
                <span class="label label-{{disc.bstyle}} arrowed" ng-bind="disc.sklad"></span>
                <a href="#" ng-hide="disc.ostatok > 0" data-toggle="modal" data-target="#myModal"><span class="label label-info">Сообщить о наличии</span></a>
                <div class="modal fade" id="myModal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Сообщить о наличии</h4>
                      </div>
                      <div class="modal-body">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="email или телефон для оповещения">                  
                           <span class="input-group-btn">
                            <button class="btn btn-theme" data-dismiss="modal" type="button"><i class="fa fa-envelope-o"></i> Отправить</button>
                          </span>
                        </div>
                      </div>
                      <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-primary">Сохранить изменения</button>
                      </div> -->
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-12 col-lg-12" ng-show="disc.ostatok > 0">
        <div class="card-panel">
          <div class="row">
            <div class="col-md-6">
              <table>
                <tbody>              
                  <tr>
                    <td>Кол-во дисков</td>
                    <td class="input-qty">
                      <input type="text" ng-model="disc.coll" ng-change="disc.summer()" class="form-control text-center"/>
                    </td>
                  </tr>
                  <tr> 
                    <td class="qty-res" ng-bind="t(disc.price)"></td> 
                    <td class="qty-total" ng-bind="t(disc.total)"></td>          
                  </tr>
                </tbody>
              </table>
              
            </div>
            <div class="col-md-3">
              <button class="btn btn-block btn-theme" ng-click="basket.putCart(<?= $model->id;?>,'disc',disc.coll)" type="button"><i class="fa fa-shopping-cart"></i> В корзину</button>
            </div>
            <div class="col-md-3">
              <!-- <button class="btn btn-block btn-theme" disabled type="button"><i class="fa fa-money"></i> Купить кредит</button> -->
              <a href="/service/delivery" class="btn">Доставка</a>
              <a disabled class="btn"><i class="fa fa-user-md"></i> Консультация</a>
            </div>
          </div>      
        </div>
      </div>
      <div class="col-md-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Характеристики</a></li>
        </ul>
        <!-- End Nav tabs -->
        <!-- Tab panes -->
        <div class="tab-content tab-content-detail">

            <!-- Detail Tab Content -->
            <div role="tabpanel" class="tab-pane active" id="detail">
              <div class="well">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td><?= $model->getAttributeLabel('size')?></td>
                      <td><?= $model->size?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('borer')?></td>
                      <td><?= $model->borer?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('stupica')?></td>
                      <td><?= $model->stupica?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('radius')?></td>
                      <td><?= $model->radius?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('color')?></td>
                      <td><?= $model->color?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('type')?></td>
                      <td><?= $model->type?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('manufacturer')?></td>
                      <td><?= $model->manufacturer?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- End Detail Tab Content -->
            <?php if($model->description->text != null):?>
              <div class="title"><span>Описание</span></div>
              <div class="tab-pane active">
                <div class="well"><?= $model->description->text ?></div>
              </div>
            <?php endif;?>
        </div>
        <!-- End Tab panes -->
      </div>
      <div class="clearfix visible-sm visible-xs"></div>
      <?php 
      $disc = Disc::find()->where(['size'=>$model->size,'borer'=>$model->borer,'radius'=>$model->radius])
        ->andWhere(['!=','url',$model->url])
        ->andWhere(['>','price',0])
        ->andWhere(['>','ostatok',0])
        ->orderBy(['price'=>SORT_DESC])->all();
      if($disc):
      ?>
      <div class="col-md-12">
        <div class="title"><span>Похожие товары</span></div>
        <div class="product-slider owl-controls-top-offset">
          <?php foreach ($disc as $model):?>
            <div class="box-product-outer">            
              <?= Cards::widget([
                'model'=>$model,
                'link'=> '/disc/detail/'.$model->url,
                // 'sale'=>['title'=> 'Акция','style'=>'danger'],
                // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
                // 'rating'=>['count'=>5,'rating'=>4],
              ])?>
            </div>
          <?php endforeach;?>
        </div>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>