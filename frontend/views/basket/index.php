<?php 
use frontend\widgets\MenuLeft;
?>
<div class="row">
  <div class="col-md-3">
    <?= MenuLeft::widget()?>
  </div>
  <div class="col-md-7">
  	<!-- Block basket -->  	
		<div class="row title" ng-show="basket.positions.length > 0">			
			<div class="hidden-xs col-md-3 col-md-offset-2"><p class="">Наименование</p></div>
			<div class="hidden-xs col-md-2"><p class="">Кол.</p></div>
			<div class="hidden-xs col-md-2"><p class="">Цена</p></div>
			<div class="hidden-xs col-md-2"><p class="">Сумма</p></div>	
		</div>
		<div class="striped" ng-repeat="pos in basket.positions">
			<div class="row">
				<div class="col-md-2"><img class="img-thumbnail img-basket" src="{{pos.url}}" height="50" alt="{{pos.title}}"/></div>
				<div class="col-md-3"><div class="vertical basket-title">{{pos.title}}</div></div>
				<div class="col-md-2">			
		      <div class="input-group bootstrap-touchspin verticalinput">
		      	<input type="text" ng-model="pos.count" ng-change="basket.updateCart(pos)" class="form-control text-center"/>
		      	<span class="input-group-btn-vertical">
		      		<button ng-click="basket.up(pos)" class="btn btn-default bootstrap-touchspin-up" type="button"><i class="glyphicon glyphicon-chevron-up"></i></button>
		      		<button ng-click="basket.down(pos)" class="btn btn-default bootstrap-touchspin-down" type="button"><i class="glyphicon glyphicon-chevron-down"></i></button>
		      	</span>
		      </div>
				</div>
				<div class="col-md-2" ng-hide="basket.payment == 'cash'"><div class="vertical basket-price">{{t(pos.old_price)}}</div></div>
				<div class="col-md-2" ng-hide="basket.payment == 'cash'"><div class="vertical basket-summ">{{t(pos.old_price * pos.count)}}</div></div>
				<div class="col-md-2" ng-show="basket.payment == 'cash'"><div class="vertical basket-price">{{t(pos.price)}}</div></div>
				<div class="col-md-2" ng-show="basket.payment == 'cash'"><div class="vertical basket-summ">{{t(pos.price * pos.count)}}</div></div>
				<div class="hidden-xs col-md-1"><div class="vertical basket-remove"><a href="#" onclick="return false" ng-click="basket.deleteCart(pos.position)"><i class="fa fa-trash"></i></a></div></div>
				<div class="visible-xs col-md-12"><a href="#" class="btn btn-theme btn-block" onclick="return false" ng-click="basket.deleteCart(pos.position)"><i class="fa fa-trash"></i></a></div>
			</div>
		</div>
		<div class="form-group">    
		  <div class="well well-small">
				<div class="title"><span>Способ оплаты</span></div>	
		    <div class="radio">	      
	        <label><input type="radio" checked="checked" name="payment" ng-model="basket.payment" value="cash" /><span class="glyphicon glyphicon-ok"></span> {{basket.payment_label['cash']}}</label> 
	        <label><input type="radio" checked name="payment" ng-model="basket.payment" value="credit" /><span class="glyphicon glyphicon-ok"></span> {{basket.payment_label['credit']}}</label>     
		    </div>
		    <small ng-show="basket.payment == 'credit'">Внимание покупка в расрочку или в кредит возможна только по городу Алматы!</small>
		  </div>
		</div>
		<div class="form-group">    
		  <div class="well well-small">
				<div class="title"><span>Доставка</span></div>	
		    <div class="radio">	      
	        <label><input type="radio" checked="checked" name="delivery" ng-model="basket.delivery" value="samovyvos" /><span class="glyphicon glyphicon-ok"></span> {{basket.delivery_label['samovyvos']}}</label> 
	        <label><input type="radio" checked name="delivery" ng-model="basket.delivery" value="city_square" /><span class="glyphicon glyphicon-ok"></span> {{basket.delivery_label['city_square']}}</label>     
	        <label><input type="radio" checked name="delivery" ng-model="basket.delivery" value="region" /><span class="glyphicon glyphicon-ok"></span> {{basket.delivery_label['region']}}</label>     
		    </div>
		    <div class="form-group" ng-show="basket.delivery == 'region'">
          <label for="city">{{basket.delivery_label['city']}}</label>
          <select class="form-control" ng-model="basket.city" name="city">
						<option ng-repeat="option in basket.delivery_option.region" value="{{option.title}}">{{option.title}}</option>
					</select> 
          <label for="type">{{basket.delivery_label['type']}}</label>
          <select class="form-control" ng-model="basket.type"="type">
						<option value="Post Exspres">Компания Post Exspres - 1 300 тг. за колесо</option>
						<option value="DPD">Компания DPD - 1 500 тг. за колесо</option>
					</select>        
				</div>
		  </div>
		</div>
    <div class="alert alert-success">
      <p class="black">Сумма : {{t(basket.priceTotal)}}</p>
      <p class="black" ng-show="basket.delivery == 'region'">Доставка : {{t(basket.deliveryTotal)}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<span>Срок {{basket.delivery_time[basket.city]}}</span></p>
      <p class="black" ng-show="basket.delivery == 'city_square'">Доставка : {{basket.deliveryTotal > 0 ? t(basket.deliveryTotal) : "бесплатно"}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<span>Срок на следующий день</span></p>  
      <p class="black"><b ng-bind-template="Итого : {{t(basket.priceTotal + basket.deliveryTotal)}}"></b></p>
    </div>
		<!-- End block basket -->
		<!-- Block forms -->
		<form class="basketForm" id="basketForm" name="basketForm" ng-submit="orderform.sendBasketForm()" ng-controller="BasketForm" >
			<input type="hidden" name="delivery" value="{{basket.delivery_label[basket.delivery]}}" />
			<input type="hidden" name="payment" value="{{basket.payment_label[basket.payment]}}" />
			<input type="hidden" name="data" value="{{basket.positions}}" />
			<input type="hidden" name="price" value="{{basket.total()}}" />
			<div class="form-group">
				<label class="control-label">Как Вас зовут?</label>
				<input type="text" name="name" changename required ng-model="orderform.name" class="form-control" autofocus="" placeholder="Как менеджеру обратиться к Вам по телефону."/>
			</div>
			<div class="form-group">
			  <label class="control-label">E-mail</label>
			  <input type="text" name="email" changemail required ng-model="orderform.email" class="form-control" placeholder="На этот ящик будут приходить уведомления о статусе Вашего заказа."/>			  
			</div>
			<div class="form-group">
			  <label class="control-label">Телефон</label>
			  <input type="text" name="phone" changephone required ng-model="orderform.phone" class="form-control" placeholder="Например +77273170077"/>	
				<!-- <p ng-show="basketForm.phone.$invalid" class="help-block help-block-error">Email cannot be blank.</p> -->
			</div>
			<div class="form-group">
			  <label class="control-label">Адрес доставки</label>
			  <textarea class="form-control" changeaddress required name="address" ng-model="orderform.address" placeholder="Например г.Алматы, ул. Садовникова, 99 (Лазарева)"></textarea>
			</div>
			<div class="form-group">
			  <label class="control-label">Комментарий</label>
			  <textarea class="form-control" changeaddress name="comment" ng-model="orderform.comment"></textarea>
			</div>
			<div class="form-group">
        <button type="submit" class="btn btn-theme" name="contact-button">Оформить</button>
      </div>
		</form>
		<!-- End block forms -->
  </div>

  <div class="col-md-2">
  </div>
</div>