<?php
use frontend\widgets\MenuLeft;
use frontend\widgets\Cards;
use common\models\Battery;

$this->params['breadcrumbs'][] = ['label' => 'Аккумуляторы', 'url' => ['/battery/index']];
$this->registerJs("mastershina.controller('BatteryController', function() {
  var battery = this; 
  battery.ostatok = ".json_encode($model->ostatok == 'Нет в наличии' ? 0 : preg_replace("/\D/", "", $model->ostatok))."  
  battery.coll = parseInt(battery.ostatok) > 4 ? 4 : parseInt(battery.ostatok);
  battery.price = ".json_encode($model->price)."
  battery.priceOld = ".json_encode($model->old_price)."
  battery.bstyle = 'danger'
  // калькуляция
  battery.total = 0 // Общая сумма шин

  if(battery.ostatok > 0){battery.bstyle = 'success'}
  if(battery.ostatok > 0){battery.sklad = battery.ostatok}else{battery.sklad = 'нет'}

  battery.summer= function(){
    // Если вдруг кол шин больше чем остаток на складе
    if(battery.coll > parseInt(battery.ostatok)){
      battery.coll = battery.ostatok;
    }
    battery.total = battery.coll * battery.price;
  }

  battery.summer();

})", yii\web\View::POS_END);
?>
<div class="row" ng-controller="BatteryController as battery"> 
  <div class="col-md-3"><?= MenuLeft::widget()?></div>
  <!-- End New Arrivals & Best Selling -->
  <div class="clearfix visible-sm visible-xs"></div>
  <div class="col-md-9">
    <div class="row">
      <?= yii\widgets\Breadcrumbs::widget([
        'homeLink'      =>  [
          'label'     =>  Yii::t('yii', 'Главная'),
          'url'       =>  ['/'],
          // 'class'     =>  'home',
          'template'  =>  '<span class="glyphicon glyphicon-home"></span> {link}',
        ],
        'links' => $this->params['breadcrumbs'],
        'itemTemplate'  =>  ' / {link}',
        // 'tag'           =>  'ul',
      ])?>
      <div class="clearfix"></div>
      <!-- Image List -->
      <div class="col-sm-3">
        <div class="image-detail">      
          <img src="<?= $model->getPicture()?>" alt="">
        </div>
      </div>
      <!-- End Image List -->
      <div class="col-sm-9">
        <div class="title-detail"><?= $model->brend->title." ".$model->title ?></div>
        <table class="table table-detail">
          <tbody>
            <tr>
              <td>Цена</td>
              <td>
                <div class="price">
                  <div ng-bind-template="{{t(battery.price)}} со скидкой"></div>
                  <span class="price-old" ng-bind-template="{{t(battery.priceOld)}} в кредит"></span>
                </div>
              </td>
            </tr>
            <tr>
              <td>В наличии</td>
              <td>
                <!-- <div class="row">
                  <div class="col-md-2 col-sx-2"><span class="label label-{{battery.bstyle}} arrowed">{{battery.sklad}}</span></div>
                  <div class="col-md-10 col-sx-10">
                  </div>
                </div>  -->               
                <span class="label label-{{battery.bstyle}} arrowed" ng-bind="battery.sklad"></span>
                <a href="#" ng-hide="battery.ostatok > 0" data-toggle="modal" data-target="#myModal"><span class="label label-info">Сообщить о наличии</span></a>
                <div class="modal fade" id="myModal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Сообщить о наличии</h4>
                      </div>
                      <div class="modal-body">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="email или телефон для оповещения">                  
                           <span class="input-group-btn">
                            <button class="btn btn-theme" data-dismiss="modal" type="button"><i class="fa fa-envelope-o"></i> Отправить</button>
                          </span>
                        </div>
                      </div>
                      <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-primary">Сохранить изменения</button>
                      </div> -->
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-12 col-lg-12" ng-show="battery.ostatok > 0">
        <div class="card-panel">
          <div class="row">
            <div class="col-md-6">
              <table>
                <tbody>              
                  <tr>
                    <td>Кол-во масел</td>
                    <td class="input-qty">
                      <input type="text" ng-model="battery.coll" ng-change="battery.summer()" class="form-control text-center"/>
                    </td>
                  </tr>
                  <tr> 
                    <td class="qty-res" ng-bind="t(battery.price)"></td> 
                    <td class="qty-total" ng-bind="t(battery.total)"></td>          
                  </tr>
                </tbody>
              </table>
              
            </div>
            <div class="col-md-3">
              <button class="btn btn-block btn-theme" ng-click="basket.putCart(<?= $model->id;?>,'battery',battery.coll)" type="button"><i class="fa fa-shopping-cart"></i> В корзину</button>
            </div>
            <div class="col-md-3">
              <!-- <button class="btn btn-block btn-theme" disabled type="button"><i class="fa fa-money"></i> Купить кредит</button> -->
              <a href="/service/delivery" class="btn">Доставка</a>
              <a disabled class="btn"><i class="fa fa-user-md"></i> Консультация</a>
            </div>
          </div>      
        </div>
      </div>
      <div class="col-md-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Характеристики</a></li>
        </ul>
        <!-- End Nav tabs -->
        <!-- Tab panes -->
        <div class="tab-content tab-content-detail">

            <!-- Detail Tab Content -->
            <div role="tabpanel" class="tab-pane active" id="detail">
              <div class="well">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td><?= $model->getAttributeLabel('article')?></td>
                      <td><?= $model->article?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('capacity')?></td>
                      <td><?= $model->capacity?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('current')?></td>
                      <td><?= $model->current?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('polarity')?></td>
                      <td><?= $model->polarity?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('length')?></td>
                      <td><?= $model->length?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('width')?></td>
                      <td><?= $model->width?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('height')?></td>
                      <td><?= $model->height?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('country')?></td>
                      <td><?= $model->country?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('assurance')?></td>
                      <td><?= $model->assurance?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('voltage')?></td>
                      <td><?= $model->voltage?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- End Detail Tab Content -->
            <?php if($model->description->text != null):?>
              <div class="title"><span>Описание</span></div>
              <div class="tab-pane active">
                <div class="well"><?= $model->description->text ?></div>
              </div>
            <?php endif;?>
        </div>
        <!-- End Tab panes -->
      </div>
      
      <div class="clearfix visible-sm visible-xs"></div>
      <?php 
      $disc = Battery::find()->where(['capacity'=>$model->capacity,'current'=>$model->current,'length'=>$model->length,'width'=>$model->width,'height'=>$model->height,'voltage'=>$model->voltage])
        ->andWhere(['!=','url',$model->url])
        ->andWhere(['>','price',0])
        ->andWhere(['>','ostatok',0])
        ->orderBy(['price'=>SORT_DESC])->all();
      if($disc):
      ?>
      <div class="col-md-12">
        <div class="title"><span>Похожие товары</span></div>
        <div class="product-slider owl-controls-top-offset">
          <?php foreach ($disc as $model):?>
            <div class="box-product-outer">            
              <?= Cards::widget([
                'model'=>$model,
                'link'=> '/oil/detail/'.$model->url,
                // 'sale'=>['title'=> 'Акция','style'=>'danger'],
                // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
                // 'rating'=>['count'=>5,'rating'=>4],
              ])?>
            </div>
          <?php endforeach;?>
        </div>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>