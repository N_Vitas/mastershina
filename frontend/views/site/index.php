<?php

/* @var $this yii\web\View */
$this->title = 'Блог от интернет-магазина МастерШина';
?>

      <div class="row">

        <!-- New Arrivals & Best Selling -->
        <div class="col-md-3 m-b-1">
          <div class="title"><span><a href="products.html">Масла <i class="fa fa-chevron-circle-right"></i></a></span></div>
          <div class="widget-slider owl-controls-top-offset m-b-2">
            <?php foreach ($oil as $model):?>
            <div class="box-product-outer">
              <div class="box-product">
                <div class="img-wrapper">
                  <a href="detail.html">
                    <img alt="Product" src="<?= $model->picture?>">
                  </a>
                  <div class="tags tags-left">
                    <span class="label-tags"><a href="products.html"><span class="label label-success arrowed-right">Новое поступление</span></a></span>
                  </div>
                  <div class="option">
                    <a href="#" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                    <a href="#" data-toggle="tooltip" title="Add to Compare"><i class="fa fa-align-left"></i></a>
                    <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i class="fa fa-heart"></i></a>
                  </div>
                </div>
                <h6><a href="detail.html"><?= $model->title?></a></h6>
                <div class="price">
                  <div><?= $model->price?></div>
                </div>
              </div>
            </div>
            <?php endforeach;?>
          </div>
          <div class="title"><span><a href="products.html">Диски <i class="fa fa-chevron-circle-right"></i></a></span></div>
          <div class="widget-slider owl-controls-top-offset">
            <?php foreach ($disc as $model):?>
              <div class="box-product-outer">
                <div class="box-product">
                  <div class="img-wrapper">
                    <a href="detail.html">
                      <img alt="Product" src="<?= $model->picture?>">
                    </a>
                    <div class="tags tags-left">
                      <span class="label-tags"><span class="label label-primary arrowed-right">Популярное</span></span>
                      <span class="label-tags"><span class="label label-default arrowed-right">Топ недели</span></span>
                    </div>
                    <div class="option">
                      <a href="#" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                      <a href="#" data-toggle="tooltip" title="Add to Compare"><i class="fa fa-align-left"></i></a>
                      <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i class="fa fa-heart"></i></a>
                    </div>
                  </div>
                  <h6><a href="detail.html"><?= $model->title?></a></h6>
                  <div class="price">
                    <div><?= $model->price?> <span class="label-tags"><span class="label label-danger arrowed">-10%</span></span></div>
                    <span class="price-old"><?= $model->old_price?></span>
                  </div>
                  <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <a href="#">(5 reviews)</a>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
            </div>
        </div>
        <!-- End New Arrivals & Best Selling -->

        <div class="clearfix visible-sm visible-xs"></div>

        <div class="col-md-9">

          <!-- Featured -->
          <div class="title"><span>Featured Products</span></div>
          <?php foreach ($shiny as $model):?>
            <div class="col-sm-4 col-lg-3 box-product-outer">
              <div class="box-product">
                <div class="img-wrapper">
                  <a href="/produkt/<?= $model->title?>">
                    <img alt="Product" src="<?= $model->picture?>">
                  </a>
                  <div class="tags">
                    <span class="label-tags"><span class="label label-default arrowed">Featured</span></span>
                  </div>
                  <div class="tags tags-left">
                    <span class="label-tags"><span class="label label-danger arrowed-right">Sale</span></span>
                  </div>
                  <div class="option">
                    <a href="#" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                    <a href="#" data-toggle="tooltip" title="Add to Compare"><i class="fa fa-align-left"></i></a>
                    <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i class="fa fa-heart"></i></a>
                  </div>
                </div>
                <h6><a href="/produkt/<?= $model->title?>"><?= $model->title?></a></h6>
                <div class="price">
                  <div><?= $model->price?> <span class="label-tags"><span class="label label-default">-10%</span></span></div>
                  <span class="price-old"><?= $model->old_price?></span>
                </div>
                <div class="rating">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-half-o"></i>
                  <a href="#">(5 reviews)</a>
                </div>
              </div>
            </div>
          <?php endforeach;?>
          <!-- End Featured -->

          <div class="clearfix visible-sm visible-xs"></div>

          <!-- Collection -->
          <div class="title m-t-2"><span>V-Neck Collection</span></div>
          <div class="product-slider owl-controls-top-offset">
            <?php foreach ($battery as $model):?>
              <div class="box-product-outer">
                <div class="box-product">
                  <div class="img-wrapper">
                    <a href="detail.html">
                      <img alt="Product" src="<?= $model->picture?>">
                    </a>
                    <div class="tags">
                      <span class="label-tags"><span class="label label-danger arrowed">Sale</span></span>
                      <span class="label-tags"><span class="label label-default arrowed">Collection</span></span>
                    </div>
                    <div class="option">
                      <a href="#" data-toggle="tooltip" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                      <a href="#" data-toggle="tooltip" title="Add to Compare"><i class="fa fa-align-left"></i></a>
                      <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i class="fa fa-heart"></i></a>
                    </div>
                  </div>
                  <h6><a href="detail.html"><?= $model->model?></a></h6>
                  <div class="price">
                    <div><?= $model->price?> <span class="label-tags"><span class="label label-danger arrowed">-10%</span></span></div>
                    <span class="price-old"><?= $model->old_price?></span>
                  </div>
                  <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                    <a href="#">(5 reviews)</a>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
          </div>
          <!-- End Collection -->

        </div>

      </div>

      <!-- Brand & Clients -->
      <div class="row">
        <div class="col-xs-12 m-t-1">
          <div class="title text-center"><span>Brand & Clients</span></div>
          <div class="brand-slider owl-controls-top-offset">
            <div class="brand">
              <a href=""><img src="images/demo/brand1.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand2.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand3.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand4.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand5.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand1.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand2.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand3.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand4.png" alt=""></a>
            </div>
            <div class="brand">
              <a href=""><img src="images/demo/brand5.png" alt=""></a>
            </div>
          </div>
        </div>
      </div>
      <!-- End Brand & Clients -->