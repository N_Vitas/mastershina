﻿<?php
use frontend\widgets\MenuLeft;
use frontend\widgets\Social;
use frontend\widgets\Basket;
use yii\helpers\Url;
use frontend\widgets\Cards;
use common\models\Shiny;
use common\models\Service;

$jobs = ['disc'=>['sedan'=>0,'jeep'=>0],'nodisc'=>['sedan'=>0,'jeep'=>0]];
$shinyjobs = Service::find()->innerjoinWith('detail')->where(['in','service.id',[5,7]])->all();

foreach ($shinyjobs as $val) {
  $jobsdetail = $val->detail;
  foreach ($jobsdetail as $detail) {
    if($detail->params == intval($model->radius) && $val->id == 5){
      $jobs['nodisc']['jeep']=$detail->price;
    }
    if($detail->params == $model->height && $val->id == 5){
      $jobs['nodisc']['sedan']=$detail->price;
    }
    if($detail->params == intval($model->radius) && $val->id == 7){
      $jobs['disc']['jeep']=$detail->price/4;
    }
    if($detail->params == $model->height && $val->id == 7){
      $jobs['disc']['sedan']=$detail->price/4;
    }
  }
}

$aboniment = ['disc'=>['sedan'=>0,'jeep'=>0],'nodisc'=>['sedan'=>0,'jeep'=>0]];
$abon = Service::find()->innerjoinWith('detail')->where(['in','service.id',[23,24]])->all();
foreach ($abon as $val) {
  $abondetail = $val->detail;
  foreach ($abondetail as $detail) {
    if($detail->params == intval($model->radius) && $val->id == 23){
      $aboniment['nodisc']['jeep']=$detail->price;
    }
    if($detail->params == $model->height && $val->id == 23){
      $aboniment['nodisc']['sedan']=$detail->price;
    }
    if($detail->params == intval($model->radius) && $val->id == 24){
      $aboniment['disc']['jeep']=$detail->price;
    }
    if($detail->params == $model->height && $val->id == 24){
      $aboniment['disc']['sedan']=$detail->price;
    }
  }
}
$bakup = ['disc'=>['sedan'=>0,'jeep'=>0],'nodisc'=>['sedan'=>0,'jeep'=>0]];
$shinybakup = Service::find()->innerjoinWith('detail')->where(['in','service.id',[15,16,17,18]])->all(); 
foreach ($shinybakup as $val) {
  $bakupdetail = $val->detail;
  foreach ($bakupdetail as $detail) {
    if($detail->params == 1 && $val->id == 15)
    $bakup['nodisc']['sedan']=$detail->price;
    if($detail->params == 1 && $val->id == 16)
    $bakup['nodisc']['jeep']=$detail->price;
    if($detail->params == 1 && $val->id == 17)
    $bakup['disc']['sedan']=$detail->price;
    if($detail->params == 1 && $val->id == 18)
    $bakup['disc']['jeep']=$detail->price;
  }  
}
$this->params['breadcrumbs'][] = ['label' => 'Шины', 'url' => ['/shiny']];
if($model->season){$this->params['breadcrumbs'][]=['label'=>$model->season,'url'=>['/shiny/'.$model->generateUrl($model->season)]];}
if($model->width){$this->params['breadcrumbs'][]=['label'=>$model->width,'url'=>['/shiny/shirina-'.$model->generateUrl($model->width)]];}
if($model->height){$this->params['breadcrumbs'][]=['label'=>$model->height,'url'=>['/shiny/vysota-'.$model->generateUrl($model->height)]];}
if($model->radius){$this->params['breadcrumbs'][]=['label'=>$model->radius,'url'=>['/shiny/diametr-'.$model->generateUrl($model->radius)]];}
if($model->brend->title){$this->params['breadcrumbs'][]=['label'=>$model->brend->title,'url'=>['/shiny/brend-'.$model->brend->url]];}
if($model->title){$this->params['breadcrumbs'][]=['label'=>$model->title,'url'=>['/shiny/speed-'.$model->generateUrl($model->model)]];}
// if($model->typeTC){$this->params['breadcrumbs'][]=['label'=>$model->typeTC,'url'=>['/shiny/'.$model->generateUrl($model->typeTC)]];}
$this->registerJs("mastershina.controller('ShinyController', function() {
  var shiny = this; 
  shiny.user = 1
  shiny.ostatok = ".json_encode($model->ostatok == 'Нет в наличии' ? 0 : preg_replace("/\D/", "", $model->ostatok))."  
  shiny.coll = parseInt(shiny.ostatok) > 4 ? 4 : parseInt(shiny.ostatok);
  shiny.price = ".json_encode($model->price)."
  shiny.priceOld = ".json_encode($model->old_price)."
  shiny.bstyle = 'danger'
  shiny.auto = 'jeep'
  // калькуляция
  shiny.total = 0 // Общая сумма шин
  shiny.aboniment = ".json_encode($aboniment,true)."
  shiny.abonimentprice = 0
  shiny.abonimenttotal = 0
  shiny.bakupprice = ".json_encode($bakup,true)." // Сумма комплекса хранения
  shiny.bakuplink = false
  shiny.prbukup = 20 // Процент хранения
  shiny.bakupprice6 = 0
  shiny.bakupprice12 = 0
  shiny.bigtotalbukup6 = 0 // Итого хранения
  shiny.skidkabukup6 = 0 // Сумма скидки хранения
  shiny.sktotalbukup6 = 0 // Итого скидки хранения
  shiny.bigtotalbukup12 = 0 // Итого хранения
  shiny.skidkabukup12 = 0 // Сумма скидки хранения
  shiny.sktotalbukup12 = 0 // Итого скидки хранения

  shiny.jobs = ".json_encode($jobs,true)." // Сумма комплекса шиномонтажа
  shiny.jobsprice = 0
  shiny.bigtotaljob = 0 // Итого шиномонтажа
  shiny.prjob = 20 // Процент шиномонтажа
  shiny.skidkajob = 0 // Сумма скидки шиномонтажа
  shiny.sktotaljob = 0 // Итого скидки шиномонтажа  
  if(shiny.jobsprice.jeep == 0){shiny.auto = 'sedan'}
  if(shiny.ostatok > 0){shiny.bstyle = 'success'}
  if(shiny.ostatok > 0){shiny.sklad = shiny.ostatok}else{shiny.sklad = 'нет'}
  
  shiny.setAuto = function(){
    if(shiny.jobs.nodisc.jeep == 0)
      shiny.auto = 'sedan';
  }
  shiny.summer= function(){
    // Если вдруг кол шин больше чем остаток на складе
    if(shiny.coll > parseInt(shiny.ostatok)){
      shiny.coll = shiny.ostatok;
    }
    shiny.setAuto()
    shiny.total = shiny.coll * shiny.price;

    if(shiny.bakuplink){      
      shiny.abonimentprice = shiny.aboniment.disc[shiny.auto]
      shiny.jobsprice = shiny.jobs.disc[shiny.auto] * shiny.coll;
    }
    else{
      shiny.abonimentprice = shiny.aboniment.nodisc[shiny.auto]
      shiny.jobsprice = shiny.jobs.nodisc[shiny.auto] * shiny.coll;
    }      
    shiny.abonimenttotal = shiny.total+shiny.abonimentprice;
    shiny.bigtotaljob = shiny.total+shiny.jobsprice;
    shiny.skidkajob = shiny.jobsprice * shiny.prjob / 100;
    shiny.sktotaljob = shiny.bigtotaljob - shiny.skidkajob;

    if(shiny.bakuplink)
      shiny.bakupprice6 = shiny.bakupprice.disc[shiny.auto] * shiny.coll * 6
    else
      shiny.bakupprice6 = shiny.bakupprice.nodisc[shiny.auto] * shiny.coll * 6
    shiny.bigtotalbukup6 = shiny.total+shiny.bakupprice6;
    shiny.skidkabukup6 = shiny.bakupprice6 * shiny.prbukup / 100;
    shiny.sktotalbukup6 = shiny.bigtotalbukup6 - shiny.skidkabukup6;

    if(shiny.bakuplink)
      shiny.bakupprice12 = shiny.bakupprice.disc[shiny.auto] * shiny.coll * 12
    else
      shiny.bakupprice12 = shiny.bakupprice.nodisc[shiny.auto] * shiny.coll * 12
    shiny.bigtotalbukup12 = shiny.total+shiny.bakupprice12;
    shiny.skidkabukup12 = shiny.bakupprice12 * shiny.prbukup / 100;
    shiny.sktotalbukup12 = shiny.bigtotalbukup12 - shiny.skidkabukup12;
  }
  shiny.handleClickDisc = function(){
    shiny.bakuplink = !shiny.bakuplink;
    shiny.summer();
  }
  shiny.getbakuplink = function(){
    if(shiny.bakuplink)
      return 'с диском'
    else
      return 'без диска'
  }
  shiny.getautotype = function(){
    if(shiny.auto == 'jeep')
      return '4х4, Джип, Минивэн'
    else
      return 'Легковые автомобили'
  }

  shiny.summer();

  // shiny.createServiceOrder = function(title,user_id,params,type,basket){
  //   var data = {title:title,user_id:user_id,params:params,type:type}
  //   // $(this).html('<i class=\"fa fa-shopping-cart\"></i> Услуга добавлена')
  //   $.ajax({
  //     type:'POST',
  //     url:'/api/service-order-create',
  //     data:data
  //   }).done(function(data){
  //     if(data.result){
  //       basket.putCart(data.id,'service',1)
  //     }
  //   });
  // }

})", yii\web\View::POS_END);

// $total = round($model->price * 40 /100,0,PHP_ROUND_HALF_UP);
?>
<div class="row" ng-controller="ShinyController as shiny"> 
  <div class="col-md-3">
    <?= MenuLeft::widget()?>
  </div>
  <!-- End New Arrivals & Best Selling -->
  <div class="clearfix visible-sm visible-xs"></div>
  <div class="col-md-9">
    <?= yii\widgets\Breadcrumbs::widget([
      'homeLink'      =>  [
        'label'     =>  Yii::t('yii', 'Главная'),
        'url'       =>  ['/'],
        // 'class'     =>  'home',
        'template'  =>  '<span class="glyphicon glyphicon-home"></span> {link}',
      ],
      'links' => $this->params['breadcrumbs'],
      'itemTemplate'  =>  ' / {link}',
      // 'tag'           =>  'ul',
    ])?>
    <div class="clearfix"></div>
    <div class="row">
      <!-- Image List -->
      <div class="col-sm-3">
        <div class="image-detail">      
          <img src="<?= $model->getPicture()?>" alt="">
        </div>
      </div>
      <!-- End Image List -->
      <div class="col-sm-9">
        <div class="title-detail"><?= $model->brend->title." ".$model->title." ".$model->width." ".$model->height." ".$model->radius?></div>
        <table class="table table-detail">
          <tbody>
            <tr>
              <td>Цена</td>
              <td>
                <div class="price">
                  <div ng-bind-template="{{t(shiny.price)}} со скидкой"></div>
                  <span class="price-old" ng-bind-template="{{t(shiny.priceOld)}} в кредит"></span>
                </div>
              </td>
            </tr>
            <tr>
              <td>В наличии</td>
              <td>
                <!-- <div class="row">
                  <div class="col-md-2 col-sx-2"><span class="label label-{{shiny.bstyle}} arrowed">{{shiny.sklad}}</span></div>
                  <div class="col-md-10 col-sx-10">
                  </div>
                </div>  -->               
                <span class="label label-{{shiny.bstyle}} arrowed" ng-bind="shiny.sklad"></span>
                <a href="#" ng-hide="shiny.ostatok > 0" data-toggle="modal" data-target="#myModal"><span class="label label-info">Сообщить о наличии</span></a>
                <div class="modal fade" id="myModal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Сообщить о наличии</h4>
                      </div>
                      <div class="modal-body">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="email или телефон для оповещения">                  
                           <span class="input-group-btn">
                            <button class="btn btn-theme" data-dismiss="modal" type="button"><i class="fa fa-envelope-o"></i> Отправить</button>
                          </span>
                        </div>
                      </div>
                      <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-primary">Сохранить изменения</button>
                      </div> -->
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-12 col-lg-12" ng-show="shiny.ostatok > 0">
        <div class="card-panel">
          <div class="row">
            <div class="col-md-6">
              <table>
                <tbody>              
                  <tr>
                    <td>Шины</td>
                    <td class="input-qty">
                      <input type="text" ng-model="shiny.coll" ng-change="shiny.summer()" class="form-control text-center"/>
                    </td>
                  </tr>
                  <tr> 
                    <td class="qty-res" ng-bind="t(shiny.price)"></td> 
                    <td class="qty-total" ng-bind="t(shiny.total)"></td>          
                  </tr>
                </tbody>
              </table>
              
            </div>
            <div class="col-md-3">
              <button class="btn btn-block btn-theme" ng-click="basket.putCart(<?= $model->id;?>,'shiny',shiny.coll)" type="button"><i class="fa fa-shopping-cart"></i> В корзину</button>
            </div>
            <div class="col-md-3">
              <!-- <button class="btn btn-block btn-theme" disabled type="button"><i class="fa fa-money"></i> Купить кредит</button> -->
              <a href="/service/delivery" class="btn">Доставка</a>
              <a disabled class="btn"><i class="fa fa-user-md"></i> Консультация</a>
            </div>
          </div>      
        </div>
      </div>

      <div class="col-md-12 col-lg-12">
        <div class="title"><span>Комплексное предложение</span></div>
        <div class="card-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">    
                <div class="radio" ng-show="shiny.jobs.nodisc.jeep > 0 && shiny.jobs.nodisc.sedan > 0 && shiny.jobs.disc.jeep > 0 && shiny.jobs.disc.sedan > 0">
                    <label>
                      <input class="changetypeTC action" type="radio" ng-model="shiny.auto" ng-click="shiny.summer()" name="auto" value="jeep"/>
                      <span class="glyphicon glyphicon-ok"></span> 4х4, Джип, Минивэн
                    </label> 
                    <label>
                      <input class="changetypeTC action" type="radio" ng-model="shiny.auto" ng-click="shiny.summer()" name="auto" value="sedan"/>
                      <span class="glyphicon glyphicon-ok"></span> Легковые автомобили
                    </label>            
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="media">
                <img class="pull-left media-object thrumb" src="/images/tires-hranenie-tolko-shini.jpg" alt="...">
                <div class="media-body">
                  <h4 class="media-heading">Сезонное хранение шин <span class="psevdolink" ng-click="shiny.handleClickDisc()" ng-bind="shiny.getbakuplink()"></span> 6 месяцев</h4>
                  <h4 class="qty-usluga-total" ng-show="shiny.ostatok > 0"><span ng-bind="t(shiny.bakupprice6)"></span></h4>
                  <!-- <p class="qty-usluga-total" ng-show="shiny.ostatok > 0">Сумма шин : <span ng-bind="t(shiny.total)"></span></p> -->
                  <p class="qty-usluga-total" ng-show="shiny.ostatok > 0">Итого <span ng-bind="t(shiny.bigtotalbukup6)"></span> (<span ng-bind="t(shiny.bakupprice6)"></span> + <span ng-bind="t(shiny.total)"></span> сумма шин)</p>
                </div>
              </div>
            </div>
            <!-- <div class="col-md-3">
              <button class="btn btn-block btn-theme" ng-click="shiny.createServiceOrder('Сезонное хранение шин '+shiny.getbakuplink()+' 6 месяцев',shiny.user,shiny.getautotype(),'Хранение шин',basket)" 
              type="button"><i class="fa fa-shopping-cart"></i> Заказать услугу</button>
            </div> -->
          </div> 
          <hr/>   
          <div class="row" ng-show="shiny.jobs.nodisc.jeep > 0 || shiny.jobs.nodisc.sedan > 0"> 
            <div class="col-md-12">
              <div class="media">
                <img class="pull-left media-object thrumb" src="/images/4_5.jpg" alt="...">
                <div class="media-body">
                  <h4 class="media-heading">Шиномонтаж комфорт <span ng-bind="shiny.getautotype()"></span></h4>

                  <h4 class="qty-usluga-total" ng-show="shiny.ostatok > 0"><span ng-bind="t(shiny.jobsprice)"></span></h4>
                  <!-- <p class="qty-usluga-total" ng-show="shiny.ostatok > 0">Сумма шин : <span ng-bind="t(shiny.total)"></span></p> -->
                  <p class="qty-usluga-total" ng-show="shiny.ostatok > 0">Итого <span ng-bind="t(shiny.bigtotaljob)"></span> (<span ng-bind="t(shiny.jobsprice)"></span> + <span ng-bind="t(shiny.total)"></span> сумма шин)</p>
                </div>
              </div>
            </div> 
            <!-- <div class="col-md-3">
              <button class="btn btn-block btn-theme" disabled type="button"><i class="fa fa-shopping-cart"></i> Заказать услугу</button>
            </div> -->
          </div>   
          <hr/> 
          <div class="row" ng-show="shiny.jobs.nodisc.jeep > 0 || shiny.jobs.nodisc.sedan > 0">
            <div class="col-md-12">
              <div class="media">
                <img class="pull-left media-object thrumb" src="/images/4_5.jpg" alt="...">
                <div class="media-body">
                  <h4 class="media-heading">Годовой абонимент <span class="psevdolink" ng-click="shiny.handleClickDisc()" ng-bind="shiny.getbakuplink()"></span></h4>
                  <h4 class="qty-usluga-total" ng-show="shiny.ostatok > 0"><span ng-bind="t(shiny.abonimentprice)"></span></h4>
                  <!-- <p class="qty-usluga-total" ng-show="shiny.ostatok > 0">Сумма шин : <span ng-bind="t(shiny.total)"></span></p> -->
                  <p class="qty-usluga-total" ng-show="shiny.ostatok > 0">Итого <span ng-bind="t(shiny.abonimenttotal)"></span> (<span ng-bind="t(shiny.abonimentprice)"></span> + <span ng-bind="t(shiny.total)"></span> сумма шин)</p>                  
                </div>
              </div>
            </div>
            <!-- <div class="col-md-3">
              <button class="btn btn-block btn-theme" disabled type="button"><i class="fa fa-shopping-cart"></i> Заказать услугу</button>
            </div> -->
          </div> 
          <hr/> 
          <div class="row">
            <div class="col-md-12">
              <p class="qty-usluga-total">Заказать и оплатить услуги вы сможете по адресу г.Алматы, ул. Садовникова, 99. <a class="qty-usluga-total" href="/home/price">Посмотреть весь прайс-лист</a></p>              
            </div>
            <!-- <div class="col-md-3">
              <button class="btn btn-block btn-theme" disabled type="button"><i class="fa fa-shopping-cart"></i> Заказать услугу</button>
            </div> -->
          </div>  
        </div>
      </div>      

      <div class="col-md-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Характеристики</a></li>
          <!-- <li role="presentation"><a href="#review" aria-controls="review" role="tab" data-toggle="tab">Отзывы (2)</a></li> -->
        </ul>
        <!-- End Nav tabs -->
        <!-- Tab panes -->
        <div class="tab-content tab-content-detail">
            <!-- Detail Tab Content -->
            <div role="tabpanel" class="tab-pane active" id="detail">
              <div class="well">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td><?= $model->getAttributeLabel('code')?></td>
                      <td><?= $model->code?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('brend')?></td>
                      <td><?= $model->brend->title?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('model')?></td>
                      <td><?= $model->model?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('season')?></td>
                      <td><?= $model->season?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('runflat')?></td>
                      <td><?= $model->runflat? 'Да' : 'Нет'?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('ship')?></td>
                      <td><?= $model->getShipTitle()?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('width')?></td>
                      <td><?= $model->width?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('height')?></td>
                      <td><?= $model->height?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('radius')?></td>
                      <td><?= $model->radius?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('loads')?></td>
                      <td><?= $model->loads?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('speed')?></td>
                      <td><?= $model->speed?></td>
                    </tr>
                    <tr>
                      <td><?= $model->getAttributeLabel('typeTC')?></td>
                      <td><?= $model->typeTC?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- End Detail Tab Content -->

            <!-- Review Tab Content --
            <div role="tabpanel" class="tab-pane" id="review">
              <div class="well">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object img-thumbnail" alt="64x64" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+">
                    </a>
                    <div class="product-rating">
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star-half-o"></i>
                    </div>
                  </div>
                  <div class="media-body">
                    <h5 class="media-heading"><strong>John Thor</strong></h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                  </div>
                </div>
                <div class="media">
                   <div class="media-left">
                    <a href="#">
                      <img class="media-object img-thumbnail" alt="64x64" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+">
                    </a>
                    <div class="product-rating">
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star-half-o"></i>
                      <i class="fa fa-star-o"></i>
                    </div>
                  </div>
                  <div class="media-body">
                    <h5 class="media-heading"><strong>Michael</strong></h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                  </div>
                </div>
                <hr/>
                <h4 class="m-b-2">Add your review</h4>
                <form role="form">
                  <div class="form-group">
                    <label for="Name">Name</label>
                    <input type="text" id="Name" class="form-control" placeholder="Name">
                  </div>
                  <div class="form-group">
                    <label for="Email">Email</label>
                    <input type="text" id="Email" class="form-control" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label>Rating</label><div class="clearfix"></div>
                    <div class="input-rating"></div>
                  </div>
                  <div class="form-group">
                    <label for="Review">Your Review</label>
                    <textarea id="Review" class="form-control" rows="5" placeholder="Your Review"></textarea>
                  </div>
                  <button type="submit" class="btn btn-theme">Submit Review</button>
                </form>
              </div>
            </div> -->
            <!-- End Review Tab Content -->
        </div>
        <!-- End Tab panes -->
        <!-- Description Tab Content -->
        <?php if($model->description->text != null):?>
        <div class="title"><span>Описание</span></div>
        <div class="tab-pane">
          <div class="well"><?= $model->description->text ?></div>
        </div>
        <?php endif;?>
        <!-- End Description Tab Content -->
      </div>
      <div class="clearfix visible-sm visible-xs"></div>
      <?php 
      $shiny = Shiny::find()->where(['width'=>$model->width,'height'=>$model->height,'radius'=>$model->radius])
        ->andWhere(['!=','url',$model->url])
        ->andWhere(['>','price',0])
        ->andWhere(['>','ostatok',0])
        ->orderBy(['price'=>SORT_DESC])->all();
      if($shiny):
      ?>
      <div class="col-md-12">
        <div class="title"><span>Похожие товары</span></div>
        <div class="product-slider owl-controls-top-offset">
          <?php foreach ($shiny as $model):?>
            <div class="box-product-outer">            
              <?= Cards::widget([
                'model'=>$model,
                'link'=> '/shiny/detail/'.$model->url,
                // 'sale'=>['title'=> 'Акция','style'=>'danger'],
                // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
                // 'rating'=>['count'=>5,'rating'=>4],
              ])?>
            </div>
          <?php endforeach;?>
        </div>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>