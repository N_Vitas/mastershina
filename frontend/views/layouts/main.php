<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\MenuTop;
use frontend\widgets\MenuBottom;
use frontend\widgets\MiddleHeader;
use frontend\widgets\HeaderTop;
use frontend\widgets\BrandSlider;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  </head>
  <body ng-app="mastershinaApp" ng-controller="AppController">
    <?php $this->beginBody() ?>
    <div class="top-header">
      <?= HeaderTop::widget() ?>
    </div>
 
    <div class="middle-header">
      <?= MiddleHeader::widget() ?>
    </div>
 
    <!-- <nav class="navbar navbar-default" role="navigation"> -->
      <?= MenuTop::widget() ?>      
    <!-- </nav> -->
    <div class="container m-t-2">
      <?= $content;?>
      <?= BrandSlider::widget() ?>
    </div>
 
    <div class="footer">
        <?= MenuBottom::widget() ?>
    </div>
 
    <!-- Back to top button -->
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop:0},500); return false">
      <i class="fa fa-angle-double-up"></i>
    </a>
    <?php $this->endBody() ?>
    <!-- Javascript plugins goes here -->
  </body>
</html>
<?php $this->endPage() ?>