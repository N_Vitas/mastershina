<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Сезонное хранение шин';
$this->params['breadcrumbs'][] = $this->title;	
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
<div class="column-center-padding">

<p style="text-align: center"><img alt="Сезонное хранение шин-МастерШина-1" src="/images/xp1.jpg" style="width: 529px; height: 308px;" title="Сезонное хранение шин-1"></p>

<p style="text-align: center"><b>Доверьте сезонное хранение шин нам и избавьте себя от дополнительных хлопот!</b></p>

<p style="text-align: center">Стоимость хранения 1 комплекта шин – от 1 200 тг/месяц.</p>


<div style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; font-weight: normal; line-height: 18.200000762939453px; text-align: start;">
<div style="text-align: center;"><span style="line-height: 18.2000007629395px; text-align: start;">&nbsp;&nbsp;</span></div>
</div>


<p><b>Преимущества для Вас: </b></p>

<ul>
  <li>Больше не нужно возить шины в багажнике и на заднем сиденье.</li>
  <li>Не надо думать, куда убрать колеса, и как их потом оттуда достать.</li>
  <li>Вы можете заказать услугу мобильного шиномонтажа и сразу оставить резину на хранение.</li>
</ul>

<p><b>Преимущества для шин: </b></p>

<ul>
  <li>Отличная осанка (вертикальное положение)</li>
  <li>Комфортные климатические условия</li>
  <li>Увеличение времени эксплуатации</li>
  <li>Расположение шин на стеллажах зависит от самого комплекта:</li>
  <li><b><span style="color: #ff0000">Шины без дисков.</span></b> Шины складируются вертикально на специальных стеллажах. Каждые 4 недели их поворачивают, во избежание деформаций с какой-либо стороны.</li>
  <li><b><span style="color: #ff0000">Шины на дисках.</span> </b>В этом случае шины складируются горизонтально. Давление в шинах должно быть не больше 1.5 атмосфер</li>
</ul>


<p><b>Мы гарантируем, Ваши шины будут довольны! </b></p>


<p><b>Как это сделать: </b></p>

<p style="text-align: justify">Вы можете заключить договор хранения при заказе выездного шиномонтажа или в нашем стационарном пункте. Срок хранения определяется Вами и всегда может быть изменен по Вашему желанию. Ваши шины маркируются и отправляются на склад.</p>


<p style="text-align: justify">Выдача шин производится по Вашему запросу с уведомлением за 3 дня до желаемой даты выдачи шин</p>


<p style="text-align: justify"><b>Документы для Вас: </b></p>

<ul>
  <li>Договор хранения</li>
  <li>Фискальный чек на принятую сумму</li>
</ul>


<p style="text-align: justify">При выдаче шин подписывается акт приема-передачи, в котором Вы удостоверяете отсутствие претензий по услуге хранения. Все риски прописаны в договоре и оплачиваются компанией.</p>


<p style="text-align: center"><img alt="Сезонное хранение шин-МастерШина-2" src="/images/012.jpg" style="width: 200px; height: 266px;" title="Сезонное хранение шин-2">&nbsp;<img alt="Сезонное хранение шин-МастерШина-3" src="/images/014.jpg" style="width: 200px; height: 267px;" title="Сезонное хранение шин-3"></p>


<p style="text-align: center;"><a href="/home/price"><b>ПРАЙС-ЛИСТ</b></a></p>

<p style="text-align: center;"><a href="/home/dogovor-hranenija" target="_blank">Ознакомиться с договором хранения шин</a></p>


</div>

    </div>
  </div>
</div>