<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Доставка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
    	<div class="column-center-padding">

			<div><span style="background-color: rgb(255, 255, 255);"><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">Вы можете оформить и<span class="apple-converted-space"><b>&nbsp;</b></span><strong>купить шины и диски через Интернет</strong><span class="apple-converted-space">&nbsp;</span>или осуществить заказ по телефону<span class="apple-converted-space">&nbsp;</span></span><strong><span style="font-size: 11.5pt; line-height: 115%;"><span class="ringo-mob">+7 778 096 91 45</span>, <span class="ringo-gorod">+7 727 335 69 94</span> </span></strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">.</span><br>
			<span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;"><span style="">Форма оплаты возможна как за наличный расчет так и безналичный расчет.&nbsp;</span><br>
			<span style="">Предоставляется годовая гарантия на товар.</span></span></span>
			</div>

			<div>&nbsp;</div>

			<p style="text-align: justify">&nbsp;<b style="color: rgb(255, 0, 0);"><img alt="" height="12" src="/images/li_art.gif" width="14">&nbsp;</b><b style="line-height: 11.7pt; text-indent: 0cm;"><span new="" style="font-size:10.0pt;font-family:">Самовывоз с магазина</span></b></p>

			<p style="text-align: justify">&nbsp;</p>

			<p class="MsoListParagraph" style="margin: 0cm 0cm 0.0001pt; text-align: justify; text-indent: 0cm; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><b><span new="" style="font-size:10.0pt;font-family:"><o:p></o:p></span></b></p>

			<p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt; text-align: justify; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><b><span new="" style="font-size:10.0pt;font-family:">Вы всегда можете получить заказанный товар в шинном центре МастерШина по </span></b><a href="/home/contact" target="_blank"><b><span new="" style="font-size:10.0pt;
			color:#db1a1a;">адресу ул.Садовникова 99</span></b></a><b><span new="" style="font-size:10.0pt;font-family:">.</span></b><span new="" style="font-size:10.0pt;font-family:"><o:p></o:p></span></p>

			<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; text-align: justify; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><b><span new="" style="font-size:10.0pt;font-family:">Действует предварительная запись на стационарный шиномонтаж.<o:p></o:p></span></b></p>

			<p style="text-align: justify">&nbsp;</p>

			<p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;">&nbsp;</p>

			<p style="text-align: justify">&nbsp;</p>

			<p style="text-align: justify"><b><span style="color:#FF0000;"><img alt="" height="12" src="/images/li_art.gif" width="14">&nbsp;Доставка по Алматы:</span></b></p>

			<p style="text-align: justify"><br>
			<span style="font-size: 13.3333px; line-height: 15.6px;"><b>Доставка осуществляется &nbsp;в течении дня после подтверждения наличия товара на складе компании. &nbsp;</b></span></p>

			<p style="text-align: justify">&nbsp;</p>

			<p style="text-align: justify"><b>Стоимость доставки по г. Алматы составляет <span style="color: rgb(255, 0, 0);">2000тг.</span></b></p>

			<p style="text-align: justify"><b>П<span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px;">ри стоимости заказа </span><span style="color: rgb(255, 0, 0);"><span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px;">более 50 тыс тг. </span><span style="color: rgb(0, 0, 0);"><span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px;">д</span><span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px;">оставка по Алматы </span></span><span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px;">бесплатная&nbsp;</span></span></b></p>

			<p style="text-align: justify">&nbsp;</p>

			<p class="MsoNormal" style="margin-bottom: 12pt; text-align: justify; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><span style="font-size: 10pt; font-family: Arial, sans-serif;">Оплата возможна как за наличный расчет</span><span style="font-size: 10pt; font-family: Arial, sans-serif;">&nbsp;</span><u><span style="font-size: 10pt; font-family: Arial, sans-serif;">(только в тенге)</span></u><span style="font-size: 10pt; font-family: Arial, sans-serif;">&nbsp;</span><span style="font-size: 10pt; font-family: Arial, sans-serif;">на месте получения товара, так и по безналичному расчету.&nbsp;</span><span new="" style="font-size:10.0pt;font-family:"><o:p></o:p></span></p>

			<p style="text-align: justify">&nbsp;<span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px;">Доставка возможна так же в оговоренный с клиентом срок - от двух до десяти дней.</span></p>

			<p style="text-align: justify"><br>
			<b><span style="font-size: 10pt; font-family: Arial, sans-serif;">При заказе шин обязательно попробуйте услугу</span><span style="font-size: 10pt;">&nbsp;</span></b><a href="/service/vyezdnoy-shinomontazh/" target="_blank"><span style="color: rgb(255, 0, 0);"><b><span style="font-size: 10pt; text-decoration: none;">ВЫЕЗДНОЙ ШИНОМОНТАЖ</span></b></span><span style="color: rgb(0, 0, 0);"><span style="font-size: 10pt; text-decoration: none;">.</span></span></a></p>

			<p style="text-align: justify">&nbsp;</p>

			<p style="text-align: justify"><br>
			<span style="color:#FF0000;"><b><span style="color:#000000;"><span style="color:#FF0000;"><img alt="" height="12" src="/images/li_art.gif" width="14">&nbsp;Доставка в регионы</span></span></b> </span></p>

			<p style="text-align: justify"><b><span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px; line-height: 18.2000007629395px; text-align: start;">Доставка по другим регионам - в срок от двух до 10 дней в зависимости от удаленности от Алматы.</span></b></p>

			<p style="text-align: justify"><br>
			<span style="color:#FF0000;"><b><span style="color:#000000;">Порядок заказа товара в регионы:</span></b></span></p>

			<p style="text-align: justify"><br>
			<span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">1. После подтверждения наличия товара на складе, менеджер компании МастерШина выписывает счет на оплату товара.<br>
			2. Оплата возможна только по безналичному расчету либо от юридического лица, либо от физического через любой банк в Вашем городе.<br>
			3. По приходу оплаты на расчетный счет компании МастерШина, товар</span><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">&nbsp;</span><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;"> доставляется на склад транспортной компании (в г.Алматы).<br>
			3. Транспортная компания доставляет Ваш заказ до склада в Вашем городе.<br>
			4. Услуги по доставке товара Вы оплачиваете транспортной компании на месте</span><span new="" style="font-size:10.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:
			">&nbsp;</span><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">получения товара.<br>
			Для доставки автошин прямо по адресу покупателя мы пользуемся услугами</span><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">&nbsp;</span><span new="" style="font-size:11.0pt;line-height:115%;font-family:"><b><span new="" style="font-size:10.0pt;mso-bidi-font-size:11.0pt;
			line-height:115%;font-family:">"Pony Express"</span></b></span></p>

			<p>&nbsp;</p>

			<p>&nbsp;<span style="color: rgb(59, 58, 58); font-family: Arial, sans-serif; font-size: 10pt; line-height: 11.7pt; text-align: justify;">&nbsp;</span><b style="color: rgb(59, 58, 58); font-family: Arial, sans-serif; font-size: 10pt; line-height: 11.7pt; text-align: justify;">Для подбора автошин Вы можете:</b></p>

			<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><span new="" style="font-size:10.0pt;font-family:">- использовать "</span><a href="/shiny/" target="_blank"><b><span new="" style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:">Поиск по шинам</span></b></a><span new="" style="font-size:10.0pt;font-family:
			">";<br>
			- отправить запрос на наш электронный адрес -</span><span new="" style="font-size:
			10.0pt;mso-bidi-font-size:11.0pt;font-family:">&nbsp;</span><b><span new="" style="font-size:10.0pt;font-family:">info@mastershina.kz</span></b><span new="" style="font-size:10.0pt;font-family:">;<br>
			- обратиться за консультацией к менеджерам Компании.<br>
			<!--[if !supportLineBreakNewLine]--><br>
			<!--[endif]--><o:p></o:p></span></p>

			<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><span new="" style="font-size:10.0pt;font-family:"><span class="ringo-mob">+7 778 096 91 45</span></span><br>
			<span class="ringo-gorod">+7 727 335 69 94</span></p>

			<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;">&nbsp;</p>

			<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;">&nbsp;</p>

			<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 11.7pt; background-position: initial initial; background-repeat: initial initial;"><span new="" style="font-size:10.0pt;font-family:"><o:p></o:p></span></p>

			<p>&nbsp;<strong style="font-family: Arial, Helvetica, sans-serif;"><u>Доставка шин и дисков осуществляется в следующие города Казахстана:</u></strong></p>
			<table class="table table-border">
				<tbody>
					<tr>
						<td>Астана</span></td>
						<td>Кокшетау</span></td>
						<td>Костанай</span></td>
						<td>Атырау</span></td>
						<td>Актау</span></td>
					</tr>
					<tr>
						<td>Актобе</span></td>
						<td>Петропавловск</span></td>
						<td>Семей</span></td>
						<td>Усть-Каменогорск</span></a></td>
						<td>Балхаш</span></td>
					</tr>
					<tr>
						<td>Караганда</span></td>
						<td>Жезказган</span></td>
						<td>Тараз</span></td>
						<td>Кызылорда</span></td>
						<td>Капшагай</span></td>
					</tr>
					<tr>
						<td>Каскелен</span></td>
						<td>Павлодар</span></td>
						<td>Талдыкорган</span></td>
						<td>Уральск</span></td>
						<td>Шымкент</span></td>
					</tr>
				</tbody>
			</table>
			</div>
		</div>
	  </div>
	</div>
</div>
