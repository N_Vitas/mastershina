<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Стационарный шиномонтаж';
$this->params['breadcrumbs'][] = $this->title;	
$this->registerJS("
function initMap() {
  var pos = {lat: 43.229799, lng: 76.874042};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: pos
  });
  var image = {
	  url: '/images/icomred.png',
	  size: new google.maps.Size(30, 30),
	  origin: new google.maps.Point(10, 0),
	  anchor: new google.maps.Point(0, 20),
  };
  var marker = new google.maps.Marker({
    position: pos,
    map: map,
    icon: image,
    title: 'Hello World!',
    optimized: false
  });
  var myoverlay = new google.maps.OverlayView();
  myoverlay.draw = function () {
      this.getPanes().markerLayer.id='markerLayer';
  };
	myoverlay.setMap(map);
}
initMap()
",yii\web\View::POS_READY);
$this->registerJsFile("//maps.googleapis.com/maps/api/js?key=AIzaSyADmGt83XcSYrm57SXk7CdE_LDqvXy3j9w", ['depends' => '\frontend\assets\AppAsset']);
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>

<div class="column-center-padding">        

<p style="text-align: justify;">Стационарная шиномонтажная станция компании&nbsp;<b>"МАСТЕРШИНА"&nbsp;</b>предлагает весь спектр шиномонтажных работ, которые выполняются на современном высокотехнологическом оборудовании, позволяющем монтировать колеса для легковых автомобилей, внедорожников, легких грузовых автомобилей и микроавтобусов, и имеющем высокую точность балансировки.<br>
<br>
В нашей компании работают опытные специалисты с многолетним стажем работы.&nbsp;</p>

<p style="text-align: justify;">Мы знаем, что качественно произведенный шиномонтаж – это залог безопасности наших клиентов на дороге!&nbsp;</p>

<p style="text-align: justify;">И самое приятное: у нас низкие цены! Приезжайте и убедитесь сами!</p>

<p style="text-align: justify;">Вы также всегда можете оставить у нас колеса на хранение!</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;"><b>Адрес:</b>&nbsp;г. Алматы, ул. Садовникова (Лазарева), 99. Между ул. Джандосова и ул. Сатпаева</p>

<div class="row">
  <div class="col-md-12"><div id="map" style="height: 400px;width: 100%;"></div></div>
  <div class="col-md-6"><img class="img-thumbnail" alt="Стационарный шиномонтаж-МастерШина-1" src="/images/img1.jpg" style="width: 550px; height: 309px;" title="Стационарный шиномонтаж-1"></div>
  <div class="col-md-6"><img class="img-thumbnail" alt="Стационарный шиномонтаж-МастерШина-2" src="/images/img2.jpg" style="width: 550px; height: 309px;" title="Стационарный шиномонтаж-2"></div>
</div>
</div>

    </div>
  </div>
</div>