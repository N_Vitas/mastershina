<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Утилизация шин';
$this->params['breadcrumbs'][] = $this->title;	
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>

<div class="column-center-padding">
          

<p style="text-align: center"><img alt="" src="/images/utilizasia-shin.png" style="width: 730px; height: 316px;"></p>


<p style="text-align: justify">&nbsp;В последние годы во всех странах мира большое внимание уделяется проблеме утилизации образующихся и постоянно растущих количествах отходов производства и потребления, в том числе изношенных шин. Вышедшие из эксплуатации автопокрышки являются одним из самых многотоннажных полимерных отходов.<br>
<br>
Проблема переработки изношенных шин имеет важное экологическое значение, поскольку изношенные шины накапливаются в местах их эксплуатации (в автохозяйствах, на аэродромах, промышленных и сельскохозяйственных предприятиях, шиномонтажных мастерских, горно-обогатительных комбинатах и т.д.). Вывозимые на свалки или рассеянные на окружающих территориях, шины длительное время загрязняют окружающую среду вследствие высокой стойкости к воздействию внешних факторов (солнечного света, кислорода, озона, микробиологических воздействий).<br>
<br>
Засыпанная землёй шина разлагается более 150 лет. Места их скопления, особенно в регионах с жарким климатом, служат благоприятной средой обитания и размножения для грызунов, змей и насекомых, являющихся разносчиками различных заболеваний. Известно много случаев, когда свалки покрышек служили главными причинами эпидемий в городах и на обширных территориях.<br>
<br>
Кроме того, шины обладают высокой пожароопасностью и относятся к 4 классу опасности, а продукты их неконтролируемого сжигания оказывают крайне вредное влияние на окружающую среду (почву, водный, воздушный бассейны). Температура горения шины равна температуре горения каменного угля, поэтому потушить такое возгорание крайне сложно.<br>
Проблема использования изношенных шин имеет также существенное экономическое значение, поскольку потребности хозяйства в природных ресурсах непрерывно растут, а их стоимость постоянно повышается.<br>
<br>
Переработка изношенных шин, содержащих помимо резины, технические свойства которой близки к первоначальным, так же большое количество армирующих текстильных и металлических материалов, является источником экономии природных ресурсов.<br>
<br>
В соответствии с п.1 ст.288 Экологического Кодекса Республики Казахстан (далее - ЭК РК) изношенные шины подлежат обязательной утилизации, однако п.6 ст.292 ЭК РК не допускает несанкционированное сжигание отходов, а п.5 ст.301 предусматривает запрет на захоронение шин на полигонах.</p>



<p style="text-align: center;"><b>У нас есть решение вопроса утилизации шин!!!</b></p>

<p style="text-align: center;">.<br>
<b>Будьте уверенны, Ваши шины действительно будут утилизированы!</b></p>

<p style="margin-top: 10px; text-align: center;"><span style="font-size:14.0pt;font-family:Tahoma;color:windowtext">ТАРИФЫ ПО ПРИЕМУ ШИН НА УТИЛИЗАЦИЮ</span></p>


<div align="center" style="margin: 0px; padding: 0px; color: rgb(59, 58, 58); font-family: Arial, Helvetica, sans-serif; line-height: 14px;">
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="margin: 0px 0px 0px 14.4pt; padding: 0px; border-collapse: collapse; font-size: 12px; width: 468pt; " width="624">
  <tbody>
    <tr style="margin: 0px; padding: 0px; height: 12.75pt;">
      <td nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 225pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; height: 12.75pt;" width="300">
      <p class="MsoNormal" style="margin: 0px; padding: 0px;"><span style="margin: 0px; padding: 0px; font-family: Arial; color: rgb(0, 51, 102);">Легковые и внедорожные шины весом до 15кг</span></p>
      </td>
      <td nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 74.5pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; height: 12.75pt;" width="99">
      <p align="center" class="MsoNormal" style="margin: 0px; padding: 0px; text-align: center;"><span style="margin: 0px; padding: 0px; font-family: Arial;">200 тг/шт</span></p>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; height: 12.75pt;">
      <td nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 225pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; height: 12.75pt;" width="300">
      <p class="MsoNormal" style="margin: 0px; padding: 0px;"><span style="margin: 0px; padding: 0px; font-family: Arial; color: rgb(0, 51, 102);">Легковые и внедорожные шины весом от 15кг до 30кг</span></p>
      </td>
      <td nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 74.5pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; height: 12.75pt;" width="99">
      <p align="center" class="MsoNormal" style="margin: 0px; padding: 0px; text-align: center;"><span style="margin: 0px; padding: 0px; font-family: Arial;">400 тг/шт</span></p>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; height: 12.75pt;">
      <td nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 225pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; height: 12.75pt;" width="300">
      <p class="MsoNormal" style="margin: 0px; padding: 0px;"><span style="margin: 0px; padding: 0px; font-family: Arial; color: rgb(0, 51, 102);">Грузовые и крупногабаритные шины</span><span style="color: rgb(255, 0, 0);"><b><span style="margin: 0px; padding: 0px; font-family: Arial;"> *</span></b></span></p>
      </td>
      <td nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 74.5pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; height: 12.75pt;" width="99">
      <p align="center" class="MsoNormal" style="margin: 0px; padding: 0px; text-align: center;"><span style="margin: 0px; padding: 0px; font-family: Arial;">30 тг/кг</span></p>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; height: 3.5pt;">
      <td colspan="2" nowrap="nowrap" style="margin: 0px; padding: 0cm 5.4pt; border-collapse: collapse; vertical-align: top; width: 468pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; height: 3.5pt;" width="624">
      <p class="MsoNormal" style="text-align: left; margin: 0px; padding: 0px;"><font color="#ff0000" face="Arial"><span style="line-height: 14px;"><b>* - вес рассчитывается из табличных значений минус 20% веса на износ</b></span></font></p>
      </td>
    </tr>
  </tbody>
</table>
</div>

<div id="page">
<div class="text-center">
<p><a href="/home/price"><b>ПРАЙС-ЛИСТ</b></a></p>
</div>
</div>


</div>

    </div>
  </div>
</div>