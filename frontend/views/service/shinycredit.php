<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Шины в кредит';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
    	<div class="column-center-padding">          

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Как часто Ваше желание или необходимость купить что-либо вступает в противоречие с Вашими&nbsp;текущими финансовыми возможностями?<o:p></o:p></span></p>

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Шинный центр «МастерШина» предлагает Вам воспользоваться программой потребительского кредитования, устранив это противоречие и приобрести выбранный на нашем сайте товар в кредит или товар в рассрочку удобным Вам способом.</span></p>

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp;</p>

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 10pt; font-family: Tahoma, sans-serif;"><o:p></o:p></span></p>

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Что Вам нужно сделать, чтобы получить шины/диски в кредит? <o:p></o:p></span></b></p>

<ol>
	<li><span style="line-height: 13.45pt; font-size: 10pt; font-family: Tahoma, sans-serif;">Выберите нужный товар на нашем сайте </span><span lang="EN-US" style="line-height: 13.45pt; font-size: 10pt; font-family: Tahoma, sans-serif;">mastershina</span><span style="line-height: 13.45pt; font-size: 10pt; font-family: Tahoma, sans-serif;">.</span><span lang="EN-US" style="line-height: 13.45pt; font-size: 10pt; font-family: Tahoma, sans-serif;">kz</span><span style="line-height: 13.45pt; font-size: 10pt; font-family: Tahoma, sans-serif;"> и нажмите кнопку «Расрочка/ Кредит»<o:p></o:p></span></li>
	<li><span style="line-height: 13.45pt; font-size: 10pt; font-family: Tahoma, sans-serif;">Перейдя к корзине, заполните всю необходимую контактную информацию, затем сформируйте заказ <o:p></o:p></span></li>
	<li><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">После оформления заказа с Вами свяжется наш менеджер и вышлет Вам чек-заявку.<o:p></o:p></span></li>
	<li><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Распечатайте эту чек-заявку и выберите удобный для Вас административный пункт Банка Хоум Кредит для консультации по условиям оформления потребительского кредита.<o:p></o:p></span></li>
	<li><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">После одобрения заказа Банком с вами свяжется менеджер шинного центра «МастерШина» для согласования даты и способа передачи товара</span><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">.</span></li>
</ol>

<p>&nbsp;</p>

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 10pt; font-family: Tahoma, sans-serif;"><o:p></o:p></span></p>

<p style="margin-left: 18pt; text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Важно! </span></b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Для оформления кредита необходимо иметь при себе удостоверение личности. Для получателей пенсии необходимо дополнительно пенсионное удостоверение.</span></p>

<p style="margin-left: 18pt; text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp;</p>

<p style="margin-left: 18pt; text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 10pt; font-family: Tahoma, sans-serif;"><o:p></o:p></span></p>

<p style="margin-left: 18pt; text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">В случае возникновения вопросов, обращайтесь по тел <span class="ringo-mob">+7 778 096 91 43</span>, <span class="ringo-gorod">+7 727 335 69 95</span> или пишите на e-mail:</span></b> <b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">info@mastershina.kz<o:p></o:p></span></b></p>

<p style="text-align: justify; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp;</p>

<p align="center" style="margin-left: 18pt; text-align: center; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">Административные пункты Банка Хоум</span></b></p>

<p>&nbsp;</p>

<center>
<table align="center" border="1" bordercolor="#6A6A6A" cellpadding="0" cellspacing="0" style="width:638px;" width="798">
	<tbody>
		<tr bgcolor="#BFBFBF">
			<td nowrap="nowrap" style="width:140px;height:22px;">
			<p align="center"><b>Торговый центр</b></p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:22px;">
			<p align="center"><b>Торговая точка</b></p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:22px;">
			<p align="center"><b>Адрес</b></p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">Шинный центр</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">МАСТЕРШИНА</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Садовникова, 99, уг.ул.Сатпаева (район ТРК АДК)</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТРК Mega Center Alma-Ata</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">м. IPOINT</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Розыбакиева, 247а.</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТД СТУДЕНТ</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">2&nbsp;этаж</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">АТАКЕНТ&nbsp;ул.Тимирязева, 42, уг.ул.Манаса</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:23px;">
			<p align="center">ТД САЛАМАТ 3</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:23px;">
			<p align="center">53 бутик</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:23px;">
			<p align="center">ул.Розыбакиева,72, уг.ул.Шевченко</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТД BAUMARKT</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">строительный магазин</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Толе би,&nbsp;189а, уг.ул.Розыбакиева</p>
			</td>
		</tr>
		<tr>
			<td style="width:140px;height:21px;">
			<p align="center">CARREFOUR</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">Галерея сотовых телефонов</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Кабдолова, 1/4</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">АРМАДА</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">Евро-Мебель, блок 2, ряд 5Д</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Кабдолова, 1/8</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТД ЛЮМИР</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">ТОО "Опт Трэйд", отопительные котлы, 1 этаж</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">пр.Саина, уг.ул.Шаляпина</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">МАСТЕРГАЗ</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">установка газового оборудования, напротив СТО GT SERVICE</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Тлендиева, 258Д</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">12 месяцев</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">строительный магазин</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">пр.Райымбека, 512, уг.ул.Саина</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТД МОБИЛА</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">сотовые телефоны слева от входа)</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Макатаева, 81, уг.ул.Абылай хана</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТД ГУМ</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">сотовые телефоны (эскалатор)</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Кунаева, 21, уг.ул.Маметовой</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">ТД МЕРЕЙ</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">мебель (рядом с таможней)</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">пр.Суюнбая,&nbsp;2,&nbsp;уг.пр.Райымбека</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:21px;">
			<p align="center">БЦ ЛОТОС</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:21px;">
			<p align="center">Chocomart, 3 этаж, вход с торца</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:21px;">
			<p align="center">ул.Макатаева,117, уг.ул.Масанчи</p>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" style="width:140px;height:22px;">
			<p align="center">ТЦ КУНГЕЙ</p>
			</td>
			<td nowrap="nowrap" style="width:267px;height:22px;">
			<p align="center">мебельный магазин</p>
			</td>
			<td nowrap="nowrap" style="width:232px;height:22px;">
			<p align="center">Алматы-1,&nbsp;ул Молдагалиева,&nbsp;2</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p align="center" style="margin-left: 18pt; text-align: center; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp;</p>

<p align="center" style="margin-left: 18pt; text-align: center; line-height: 13.45pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 10pt; font-family: Tahoma, sans-serif;"><o:p></o:p></span></b></p>
<!--<p class="MsoNormal" style="text-align: center;">&nbsp;Шины в кредит очень удобная и простая услуга для наших клиентов.&nbsp;Ведь не всегда мы обладаем деньгами в полном объеме, или хотим приобрести более качественный товар, который стоит дороже, чем мы рассчитывали.&nbsp;</p>
<p class="MsoNormal" style="text-align: justify;">&nbsp;</p>
<p class="MsoNormal" style="text-align: center;"><b><img src="/upload/userfiles/1.jpg" width="300" height="200" alt="" />&nbsp;&nbsp;<img src="/upload/userfiles/6.jpg" width="308" height="200" alt="" /></b></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: center;"><b>Также можно оформить шины в рассрочку. </b></p>
<p style="text-align: center;"><b><span style="color: rgb(255, 0, 0);">Оформление товара в кредит возможно только при личном посещении нашего магазина. </span></b></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;<img src="/upload/userfiles/kredit_s1.jpg" width="300" height="300" alt="" /></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: center;"><b>&nbsp;Вместе с менеджером вы подберете нужные вам шины, получите счет. </b></p>
<p style="text-align: center;">&nbsp;<b>По этому счету менеджер банка оформит заявку на получение товара в кредит.&nbsp;</b></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: center;"><b><img src="/upload/userfiles/3.jpg" width="300" height="227" alt="" /></b></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: center;"><span style="color: rgb(255, 0, 0);"><b>Оформление заявки занимает 10-15 мин.</b></span><b> </b></p>
<p style="text-align: center;"><b>Столько же продлится ожидание ответа банка. </b></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><b>После подтверждения Вашей заявки вы можете получить товар в магазине.&nbsp;</b></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: center;">&nbsp;<img src="/upload/userfiles/4.jpg" width="200" height="227" alt="" /><img src="/upload/userfiles/7.jpg" width="227" height="227" alt="" /></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><b>Условия кредитования для клиента<span style="color: rgb(255, 0, 0);">*</span>:</b></p>
<p class="MsoNormal" style="text-align: justify;">1) Кредиты предоставляются гражданам РК в возрасте от 22 до 68 лет</p>
<p class="MsoNormal" style="text-align: justify;">2) Для оформления требуется 1 документ – удостоверение личности.</p>
<p class="MsoNormal" style="text-align: justify;">3) Сумма кредита – от 10 000 до 750 000 тенге</p>
<p class="MsoNormal" style="text-align: justify;">4) Первоначальный взнос – от 0%</p>
<p class="MsoNormal" style="text-align: justify;">5) Срок кредитования – от 3 до 48 месяцев</p>
<p class="MsoNormal" style="text-align: justify;">6) Без поручителей и залога</p>
<p class="MsoNormal" style="text-align: justify;">7) Без комиссий за рассмотрение и выдачу кредита</p>
<p class="MsoNormal" style="text-align: justify;">&nbsp;</p>
<p class="MsoNormal" style="text-align: justify;">&nbsp;</p>
<p class="ListParagraphCxSpFirst" style="text-align: left; margin-left: 18pt;"><span style="color: rgb(255, 0, 0);"><span style="background-color: rgb(255, 255, 255);"><b><span style="font-size: 9pt; line-height: 115%; font-family: Arial, sans-serif;">*Таблица - рассрочка.</span></b></span></span></p>
<table width="490" cellspacing="0" cellpadding="0" border="1" style="border-collapse:
    collapse;width:368pt">
    <colgroup><col width="53" style="width: 40pt; text-align: center;" />  <col width="107" style="width: 80pt; text-align: center;" />  <col width="85" style="width: 64pt; text-align: center;" />  <col width="114" style="width: 86pt; text-align: center;" />  <col width="131" style="width: 98pt; text-align: center;" />  </colgroup>
    <tbody>
        <tr height="20" style="height:15.0pt">
            <td width="359" height="20" style="height: 15pt; width: 270pt; text-align: center;" colspan="4" class="xl63"><b>Примерный расчет при покупке шин в рассрочку</b></td>
            <td width="131" style="width: 98pt; text-align: center;" class="xl63">&nbsp;</td>
        </tr>
        <tr height="60" style="height:45.0pt">
            <td width="53" height="60" style="height: 45pt; width: 40pt; text-align: center;" class="xl64">Срок</td>
            <td width="107" style="border-left-style: none; width: 80pt; text-align: center;" class="xl65">&nbsp; Предоплата от   клиента</td>
            <td width="85" style="width: 64pt; text-align: center;" class="xl64">Стоимость товара</td>
            <td width="114" style="border-left-style: none; width: 86pt; text-align: center;" class="xl65">Ежемесячный   платеж</td>
            <td width="131" style="border-left-style: none; width: 98pt; text-align: center;" class="xl66">Общая сумма   платежей по кредиту</td>
        </tr>
        <tr height="19" style="height:14.25pt">
            <td height="19" style="height: 14.25pt; border-top-style: none; text-align: center;" class="xl67">3</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl68">0,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl69">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 33 333&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl71">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 100 000&nbsp;&nbsp;</td>
        </tr>
        <tr height="19" style="height:14.25pt">
            <td height="19" style="height: 14.25pt; border-top-style: none; text-align: center;" class="xl72">4</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl73">0,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl74">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl75">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 25 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 100 000&nbsp;&nbsp;</td>
        </tr>
        <tr height="19" style="height:14.25pt">
            <td height="19" style="height: 14.25pt; border-top-style: none; text-align: center;" class="xl67">5</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl68">15,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl69">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl71">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 85 000&nbsp;&nbsp;</td>
        </tr>
        <tr height="19" style="height:14.25pt">
            <td height="19" style="height: 14.25pt; border-top-style: none; text-align: center;" class="xl72">6</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl73">25,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl74">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl75">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 12 500&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 75 000&nbsp;&nbsp;</td>
        </tr>
        <tr height="19" style="height:14.25pt">
            <td height="19" style="height: 14.25pt; border-top-style: none; text-align: center;" class="xl77">7</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl68">35,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl78">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9 286&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl71">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 65 000&nbsp;&nbsp;</td>
        </tr>
        <tr height="19" style="height:14.25pt">
            <td height="19" style="height: 14.25pt; border-top-style: none; text-align: center;" class="xl77">9</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl68">45,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl78">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6 111&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl71">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 55 000&nbsp;&nbsp;</td>
        </tr>
        <tr height="20" style="height:15.0pt">
            <td height="20" style="height: 15pt; border-top-style: none; text-align: center;" class="xl79">12</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl80">55,0%</td>
            <td style="border-top-style: none; text-align: center;" class="xl81">&nbsp;&nbsp;&nbsp;   100 000&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl82">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 750&nbsp;&nbsp;</td>
            <td style="border-top-style: none; border-left-style: none; text-align: center;" class="xl83">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 45 000&nbsp;&nbsp;</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p class="ListParagraphCxSpLast" style="text-align: justify; margin-left: 18pt; mso-add-space: auto;">&nbsp;&nbsp;</p>
<p style="text-align: justify;"><span style="color: rgb(255, 0, 0);"><b>*&nbsp;</b></span>Подробнее об условиях кредитования Вы можете узнать, <span style="color: rgb(61, 61, 61); font-family: Arial; font-size: 9pt; mso-fareast-language: RU;">позвонив по телефонам:</span></p>
<p style="text-align: justify;"><b><span style="color: rgb(255, 0, 0);">+7 727 390 64 04<br />
+7 777 7 157 157<br />
+7 701 7 157 157<br />
+7 707 7 157 157 </span></b><span style="color: rgb(61, 61, 61); font-family: Arial; font-size: 9pt; mso-fareast-language: RU;"> </span><span style="color: rgb(255, 0, 0);"><span style="font-family: Arial; font-size: 9pt; mso-fareast-language: RU;"> </span></span></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>--></center>


</div>
		</div>
	</div>
</div>