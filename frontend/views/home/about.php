<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'О нас';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
		  <div class="column-center-padding">         

			<h2><strong>Шинный центр МастерШина</strong></h2>

			<p>&nbsp;</p>

			<p align="justify"><a href="/images/big/img3.jpg" onclick="return imgOpen(this,'MASTERSHINA');" target="_photo" vlink="red"><img align="right" alt="О Компании-МастерШина-1" border="0" hspace="10" src="/images/small/img3s.jpg" title="О Компании-1" vspace="5"></a>Шинный центр МастерШина – самый простой и выгодный сервис для приобретения автомобильных шин и дисков в Алматы и по всей стране! Компания была создана в 2010 году. Ведущие специалисты нашей фирмы работают в шинной отрасли более 10 лет.&nbsp;<br>
			<br>
			У нас Вы можете купить шины и диски ведущих европейских, азиатских, американских и российских производителей, а также купить шины и диски редко встречающихся моделей, размеров и форм. Представленные на нашем сайте каталог шин и каталог дисков помогут быстрее сориентироваться в многообразии продукции и найти необходимые вам шины и диски несколькими различными способами - по производителю, по размеру и даже по конкретному автомобилю.&nbsp;<br>
			<br>
			<a href="/images/big/img5.jpg" onclick="return imgOpen(this,'MASTERSHINA');" target="_photo" vlink="red"><img align="left" alt="О Компании-МастерШина-2" border="0" hspace="10" src="/images/small/img5s.jpg" title="О Компании-2" vspace="5"></a>К Вашим услугам консультации наших специалистов, которые всегда готовы помочь определиться с выбором нужного комплекта шин и дисков. Для Вашего удобства на сайте организованы заказ обратного звонка и онлайн чат с консультантом, которые Вы можете видеть по бокам на всех страницах нашего сайта.&nbsp;<br>
			<br>
			Шинный центр МастерШина предлагает бесплатную доставку по Алматы. Доставка в другие регионы осуществляется через транспортные компании. Мы подберем для Вас наиболее оптимальный по тарифам и срокам доставки вариант. Доставка заказов до самой транспортной компании осуществляется бесплатно!&nbsp;<br>
			<br>
			При заказе шин обязательно попробуйте услугу&nbsp;<b><a href="/stacionarnyj-shinomontazh/" target="_blank">ШИНОМОНТАЖ</a>.</b>&nbsp;Наша бригада переобует Ваш автомобиль на&nbsp;<a href="/kontakty/" target="_blank">базе&nbsp;"МастерШина"</a>. С услугой&nbsp;<b><a href="/sezonnoe-hranenie-shin/">СЕЗОННОЕ ХРАНЕНИЕ</a></b>&nbsp;Вам больше не придется думать как и где хранить свои колеса.&nbsp;<br>
			<br>
			<a href="/images/big/img4.jpg" onclick="return imgOpen(this,'MASTERSHINA');" target="_photo" vlink="red"><img align="right" alt="О Компании-МастерШина-3" border="0" hspace="10" src="/images/small/img4s.jpg" title="О Компании-3" vspace="5"></a>Наши цены на литые диски и автомобильные шины Вас приятно удивят. К Вашим услугам наличная и безналичная система оплаты. Реализована возможность оформления шин и дисков в&nbsp;<b><a href="/kredit-rassrochka/">КРЕДИТ И РАССРОЧКУ.</a></b>&nbsp;<br>
			<br>
			Для корпоративных клиентов действует гибкая система скидок.&nbsp;<br>
			<br>
			Желаем удачных покупок, приятных поездок и безопасности в путешествиях! Команда шинного центра МастерШина рада этому помочь!</p>


			</div>
	</div>
</div>
</div>
