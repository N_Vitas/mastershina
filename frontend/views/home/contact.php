<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\widgets\MenuLeft;

$this->title = 'Контактная информация';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJS("
function initMap() {
  var pos = {lat: 43.229799, lng: 76.874042};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: pos
  });
  var image = {
    url: '/images/icomred.png',
    size: new google.maps.Size(30, 30),
    origin: new google.maps.Point(10, 0),
    anchor: new google.maps.Point(0, 20),
  };
  var marker = new google.maps.Marker({
    position: pos,
    map: map,
    icon: image,
    title: 'Hello World!',
    optimized: false
  });
  var myoverlay = new google.maps.OverlayView();
  myoverlay.draw = function () {
      this.getPanes().markerLayer.id='markerLayer';
  };
  myoverlay.setMap(map);
}
initMap()
",yii\web\View::POS_READY);
$this->registerJsFile("//maps.googleapis.com/maps/api/js?key=AIzaSyADmGt83XcSYrm57SXk7CdE_LDqvXy3j9w", ['depends' => '\frontend\assets\AppAsset']);
?>
<div class="site-contact">
  <div class="row">
    <!-- Левый блок -->
    <div class="col-md-3">
    <?= MenuLeft::widget() ?>
    </div>
    <!-- Конец левого блока -->
    <!-- Правый блок -->
    <div class="col-lg-9">
        <div class="title"><span><?= Html::encode($this->title) ?></span></div>
        <div class="row">
          <div class="col-lg-12">
            <div class="well">
              <p>
                  <font color="red"><b>Уважаемые покупатели</b><br>
                  Обращаем ваше внимание, что при получении товара с нашего склада самовывозом сделать это можно ТОЛЬКО при предварительном резерве у менеджера нашей компании. </font>
              </p>

              <div>
                <h4>Адрес и время работы офиса, склада и шиномонтажа:</h4>
                <div>г.Алматы, ул. Садовникова, 99 (Лазарева)</div>
                <div>(район ТРК "АДК")</div>
                <div>пн-пт: 9.00 - 19.00</div>
                <div>сб: 10.00 - 17.00</div>
                <div>вс: 10.00 - 17.00</div>
              </div>                          
            </div>
          </div>
          <!-- End New Arrivals & Best Selling -->
          <div class="clearfix visible-sm visible-xs"></div>
          <div class="col-lg-12">
            <div class="title"><span>ТЕЛЕФОНЫ</span></div>            
          </div>

          <div class="col-lg-6">
            
            <h4>Шины и диски</h4>
            <div><a href="tel:+77273906404">+7 727 390 64 04</a></div>
            <div><a href="tel:+77017157157">+7 701 7 157 157</a></div>
            <div><a href="tel:+77077157157">+7 707 7 157 157</a></div>
            <div><a href="tel:+77777157157">+7 777 7 157 157</a></div>

            <h4>Запись на шиномонтаж</h4>
            <div><a href="tel:+77787460006">+7 778 746 00 06</a></div>
            <div><a href="tel:+77273383334">+7 727 338 33 34</a></div>

            <h4>Сезонное хранение</h4>
            <div><a href="tel:+77787460008">+7 778 746 00 08</a></div>
            <div><a href="tel:+77273383335">+7 727 338 33 35</a></div>

            <h4>Бухгалтерия и отдел по работе с корпоративными клиентами</h4>
            <div><a href="tel:+77787460007">+7 778 746 00 07</a></div>
            <div><a href="tel:+77273383337">+7 727 338 33 37</a></div>
            <div><a href="mailto:info@mastershina.kz">info@mastershina.kz</a></div>

            <h4>РЕКВИЗИТЫ</h4>

            <div>ТОО «Outdoor Production»</div>
            <div>БИН 061240003232</div>
            <div>АФ АО «Казкоммерцбанк»</div>
            <div>БИК KZKOKZKX</div>
            <div>ИИК KZ489261802164433000</div>
          </div>
          <div class="col-lg-6">
            <img class="img-thumbnail" src="/images/map.jpg"/>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"><div id="map" style="height: 400px;width: 100%;"></div></div>
        </div>
        <hr/>
        <div class="row">
          <div class="col-lg-12">
            <div class="title"><span>ФОРМА ОБРАТНОЙ СВЯЗИ</span></div>            
          </div>
          <!-- End New Arrivals & Best Selling -->
          <div class="clearfix visible-sm visible-xs"></div>
          <div class="col-lg-12">
            <p>Для отзывов и предложений. При подаче запроса о наличии и стоимости шин, не забывайте указывать полный размер.</p>
            <!-- Форма обратной связи -->
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'phone') ?>
                <?= $form->field($model, 'subject') ?>
                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
            <!-- Конец формы обратной связи -->                   
          </div>
        </div>
    </div>
    <!-- Конец правого блока -->
  </div>

</div>

<!--
<td id="column-center" valign="top">
  <div id="go_content"></div>

        <div class="column-center-padding">
                <h1 style="margin-top:14px; font-size:16px; margin-bottom:20px;">
                        Контакты                    </h1>
          

<p><br>
<font color="red"><b>Уважаемые покупатели</b><br>
Обращаем&nbsp;ваше внимание, что при получении товара с нашего склада самовывозом сделать это можно ТОЛЬКО при предварительном резерве у менеджера нашей компании. </font><br>
&nbsp;</p>

<table width="100%">
    <tbody>
        <tr>
            <td width="40%">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                        <div itemscope="" itemtype="http://schema.org/Organization">
                        <p><b>Адрес и время работы офиса, склада и шиномонтажа:</b></p>

                        <p>&nbsp;&nbsp;г.Алматы, ул. Садовникова, 99&nbsp;(Лазарева)</p>

                        <p>(район ТРК "АДК")</p>

                        <p><font face="Arial, Helvetica, sans-serif">пн-пт: 9.00 - 19.00</font></p>

                        <p><font face="Arial, Helvetica, sans-serif">сб: 10.00 - 17.00</font></p>

                        <p><span style="font-family: Arial, Helvetica, sans-serif;">вс:&nbsp;</span><span style="font-family: Arial, Helvetica, sans-serif;">10.00 - 17.00</span></p>

                        <p>&nbsp;</p>

                        <p><b>ТЕЛЕФОНЫ</b></p>

                        <p><b>Шины и диски</b></p>

                        <p>+7 727 390 64 04<br>
                        +7 701 7 157 157</p>

                        <p>+7 707 7 157 157<br>
                        +7 777 7 157 157</p>

                        <p>&nbsp;</p>

                        <p><b>Запись на шиномонтаж</b></p>

                        <p>+7 778 746 00 06<br>
                        +7 727 338 33 34</p>

                        <p>&nbsp;</p>

                        <p><b>Сезонное хранение&nbsp;</b></p>

                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td align="left" width="150">+7 778 746 00 08<br>
                                    +7 727 338 33 35</td>
                                </tr>
                            </tbody>
                        </table>

                        <p>&nbsp;</p>

                        <p><b>Бухгалтерия и отдел по работе с корпоративными клиентами</b></p>

                        <p>+7 778 746 00 07</p>

                        <p>&nbsp;</p>

                        <p>+7 727 338 33 37&nbsp;</p>

                        <p>&nbsp;</p>

                        <p><a href="https://e.mail.ru/sentmsg?compose&amp;To=info@mastershina.kz" style="color: rgb(8, 87, 166); font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">info@mastershina.kz</a></p>

                        <p>&nbsp;</p>

                        <p><b>РЕКВИЗИТЫ</b></p>

                        <p><span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">ТОО «Outdoor Production»</span><br style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">
                        <span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">БИН 061240003232</span><br style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">
                        <span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">АФ АО «Казкоммерцбанк»</span><br style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">
                        <span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">БИК KZKOKZKX</span><br style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">
                        <span style="font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 13px;">ИИК KZ489261802164433000</span></p>
                        </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td>
            <p><b><a id="map1">Cхема проезда</a></b>:</p>
            <a href="http://maps.yandex.ru/?um=y9BOpday9ZuyW6r1E1B2csZCavbKNwyf&amp;l=sat%2Csat" target="_blank"><img alt="Контакты-МастерШина-1" src="http://mastershina.kz/upload/images/map.jpg" title="Контакты-1"></a><br>
            &nbsp;</td>
            <tr><td colspan="2">

</td>
        </tr>
    </tbody>
</table>

<p>Въезд в шинный центр МастерШина со стороны улиц Сатпаева и Джандосова</p>

<p><br>
<ymaps id="ymaps1500002259029339051" style="display: block; width: 600px; height: 450px;"><ymaps class="ymaps-2-1-53-map" style="width: 600px; height: 450px;"><ymaps class="ymaps-2-1-53-map ymaps-2-1-53-i-ua_js_yes ymaps-2-1-53-map-bg-ru ymaps-2-1-53-islets_map-lang-ru" style="width: 600px; height: 450px;"><ymaps class="ymaps-2-1-53-inner-panes"><ymaps class="ymaps-2-1-53-events-pane ymaps-2-1-53-user-selection-none" unselectable="on" style="height: 100%; width: 100%; top: 0px; left: 0px; position: absolute; z-index: 4000; cursor: url(&quot;https://api-maps.yandex.ru/2.1.53/build/release/images/util_cursor_storage_grab.cur&quot;) 16 16, url(&quot;https://api-maps.yandex.ru/2.1.53/build/release/images/util_cursor_storage_grab.cur&quot;), move;"></ymaps><ymaps class="ymaps-2-1-53-ground-pane" style="top: 0px; left: 0px; position: absolute; transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) scale(1, 1); z-index: 801;"><ymaps style="position: absolute; z-index: 150;"><canvas height="706" width="856" style="position: absolute; width: 856px; height: 706px; left: -128px; top: -128px;"></canvas></ymaps></ymaps><ymaps class="ymaps-2-1-53-copyrights-pane" style="height: 0px; bottom: 5px; right: 3px; top: auto; left: 10px; position: absolute; z-index: 8002; margin-bottom: 0px;"><ymaps><ymaps class="ymaps-2-1-53-copyright ymaps-2-1-53-copyright_logo_no"><ymaps class="ymaps-2-1-53-copyright__fog">…</ymaps><ymaps class="ymaps-2-1-53-copyright__wrap"><ymaps class="ymaps-2-1-53-copyright__layout"><ymaps class="ymaps-2-1-53-copyright__content-cell"><ymaps class="ymaps-2-1-53-copyright__content"><ymaps class="ymaps-2-1-53-copyright__text">© Яндекс</ymaps><ymaps class="ymaps-2-1-53-copyright__agreement">&nbsp;<a class="ymaps-2-1-53-copyright__link" target="_blank" href="https://yandex.ru/legal/maps_termsofuse/">Условия использования</a></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-copyright__logo-cell"><a class="ymaps-2-1-53-copyright__logo" href="" target="_blank"></a></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-map-copyrights-promo"><iframe src="https://api-maps.yandex.ru/services/inception/?lang=ru_RU&amp;iframe_id=3558&amp;url=%2Fmap&amp;api_version=2.1.53&amp;counter_prefix=constructor&amp;mode=release&amp;referer_host=mastershina.kz" width="171" height="29" scrolling="no" frameborder="0" style="overflow: hidden;"></iframe></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls-pane" style="width: 100%; top: 0px; left: 0px; position: absolute; z-index: 7203;"><ymaps class="ymaps-2-1-53-controls__toolbar" style="margin-top: 10px;"><ymaps class="ymaps-2-1-53-controls__toolbar_left"><ymaps class="ymaps-2-1-53-controls__control_toolbar ymaps-2-1-53-user-selection-none" style="margin-right: 0px; margin-left: 10px;" unselectable="on"><ymaps><ymaps class="ymaps-2-1-53-float-button ymaps-2-1-53-_hidden-text" style="max-width: 90px" title="Определить ваше местоположение"><ymaps class="ymaps-2-1-53-float-button-icon ymaps-2-1-53-float-button-icon_icon_geolocation"></ymaps><ymaps class="ymaps-2-1-53-float-button-text"></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__control_toolbar" style="margin-right: 0px; margin-left: 10px;"><ymaps><ymaps class="ymaps-2-1-53-searchbox"><ymaps id="id_150000225920737853621" class="ymaps-2-1-53-user-selection-none" unselectable="on"><ymaps><ymaps class="ymaps-2-1-53-float-button ymaps-2-1-53-_hidden-icon" style="max-width: 72px" title="Найти"><ymaps class="ymaps-2-1-53-float-button-icon"></ymaps><ymaps class="ymaps-2-1-53-float-button-text">Найти</ymaps></ymaps></ymaps></ymaps><ymaps id="id_150000225920737853622"></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__control_toolbar ymaps-2-1-53-user-selection-none" style="margin-right: 0px; margin-left: 10px;" unselectable="on"><ymaps><ymaps class="ymaps-2-1-53-float-button ymaps-2-1-53-_hidden-text" style="max-width: 28px" title=""><ymaps class="ymaps-2-1-53-float-button-icon ymaps-2-1-53-float-button-icon_icon_routes"></ymaps><ymaps class="ymaps-2-1-53-float-button-text"></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__toolbar_right"><ymaps class="ymaps-2-1-53-controls__control_toolbar" style="margin-right: 10px; margin-left: 0px;"><ymaps><ymaps class="ymaps-2-1-53-traffic"><ymaps id="id_150000225920737853644" unselectable="on" class="ymaps-2-1-53-user-selection-none"><ymaps><ymaps class="ymaps-2-1-53-float-button ymaps-2-1-53-float-button_traffic_left"><ymaps class="ymaps-2-1-53-traffic__icon ymaps-2-1-53-traffic__icon_icon_off ymaps-2-1-53-float-button-icon"></ymaps><ymaps class="ymaps-2-1-53-float-button-text"><ymaps>Пробки</ymaps></ymaps></ymaps></ymaps><ymaps></ymaps></ymaps><ymaps id="id_150000225920737853645"><ymaps><ymaps class="ymaps-2-1-53-traffic__panel ymaps-2-1-53-popup ymaps-2-1-53-popup_direction_down ymaps-2-1-53-popup_to_bottom ymaps-2-1-53-popup_theme_ffffff ymaps-2-1-53-user-selection-none" unselectable="on" style="width: 270px;"><ymaps class="ymaps-2-1-53-traffic__tail ymaps-2-1-53-popup__tail"></ymaps><ymaps class="ymaps-2-1-53-traffic__panel-content"><ymaps id="id_150000225920737853646"><ymaps><ymaps class="ymaps-2-1-53-traffic__switcher"><ymaps class="ymaps-2-1-53-traffic__switcher-item ymaps-2-1-53-traffic__switcher-item_data_actual ymaps-2-1-53-traffic__switcher-item_selected_yes">Сегодня</ymaps><ymaps class="ymaps-2-1-53-traffic__switcher-item ymaps-2-1-53-traffic__switcher-item_data_archive">Статистика</ymaps></ymaps></ymaps></ymaps><ymaps></ymaps><ymaps></ymaps><ymaps></ymaps><ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__control_toolbar" style="margin-right: 10px; margin-left: 0px;"><ymaps><ymaps class="ymaps-2-1-53-listbox ymaps-2-1-53-listbox_opened_no ymaps-2-1-53-listbox_align_right" style="width: 69px;"><ymaps class="ymaps-2-1-53-listbox__button ymaps-2-1-53-_visible-arrow ymaps-2-1-53-_hidden-icon ymaps-2-1-53-user-selection-none" title="" unselectable="on"><ymaps class="ymaps-2-1-53-listbox__button-icon"></ymaps><ymaps class="ymaps-2-1-53-listbox__button-text">Слои</ymaps><ymaps class="ymaps-2-1-53-listbox__button-arrow"></ymaps></ymaps><ymaps class="ymaps-2-1-53-listbox__panel ymaps-2-1-53-i-custom-scroll" style="transform: translate3d(0px, 0px, 0px) scale(1, 1);"><ymaps class="ymaps-2-1-53-listbox__list" style="max-height: 999999px;"><ymaps><ymaps><ymaps id="id_150000225920737853654"><ymaps unselectable="on" class="ymaps-2-1-53-user-selection-none"><ymaps class="ymaps-2-1-53-listbox__list-item ymaps-2-1-53-listbox__list-item_selected_yes"><ymaps class="ymaps-2-1-53-listbox__list-item-text">Схема</ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps><ymaps><ymaps id="id_150000225920737853655"><ymaps unselectable="on" class="ymaps-2-1-53-user-selection-none"><ymaps class="ymaps-2-1-53-listbox__list-item ymaps-2-1-53-listbox__list-item_selected_no"><ymaps class="ymaps-2-1-53-listbox__list-item-text">Спутник</ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps><ymaps><ymaps id="id_150000225920737853656"><ymaps unselectable="on" class="ymaps-2-1-53-user-selection-none"><ymaps class="ymaps-2-1-53-listbox__list-item ymaps-2-1-53-listbox__list-item_selected_no"><ymaps class="ymaps-2-1-53-listbox__list-item-text">Гибрид</ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps><ymaps><ymaps id="id_150000225920737853652"><ymaps><ymaps class="ymaps-2-1-53-listbox__list-separator"></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps><ymaps><ymaps id="id_150000225920737853653"><ymaps unselectable="on" class="ymaps-2-1-53-user-selection-none"><ymaps class="ymaps-2-1-53-listbox__list-item ymaps-2-1-53-listbox__list-item_selected_no"><ymaps class="ymaps-2-1-53-listbox__list-item-text">Панорамы</ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__control_toolbar ymaps-2-1-53-user-selection-none" style="margin-right: 10px; margin-left: 0px;" unselectable="on"><ymaps><ymaps class="ymaps-2-1-53-float-button ymaps-2-1-53-_hidden-text" style="max-width: 28px" title=""><ymaps class="ymaps-2-1-53-float-button-icon ymaps-2-1-53-float-button-icon_icon_expand"></ymaps><ymaps class="ymaps-2-1-53-float-button-text"></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__bottom" style="top: 450px;"><ymaps class="ymaps-2-1-53-controls__control" style="margin-right: 0px; margin-left: 0px; bottom: 30px; top: auto; right: 10px; left: auto;"><ymaps><ymaps style="display: block;"><ymaps style="display: inline-block; height: 100%; vertical-align: top;"><ymaps id="id_150000225920737853637"><ymaps><ymaps class="ymaps-2-1-53-scaleline" style="width: 72px; min-width: 59px"><ymaps class="ymaps-2-1-53-scaleline__left"><ymaps class="ymaps-2-1-53-scaleline__left-border"></ymaps><ymaps class="ymaps-2-1-53-scaleline__left-line"></ymaps></ymaps><ymaps class="ymaps-2-1-53-scaleline__center"><ymaps class="ymaps-2-1-53-scaleline__label">500&nbsp;м</ymaps></ymaps><ymaps class="ymaps-2-1-53-scaleline__right"><ymaps class="ymaps-2-1-53-scaleline__right-border"></ymaps><ymaps class="ymaps-2-1-53-scaleline__right-line"></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps style="display: inline-block; width: 10px; height: 0"></ymaps><ymaps style="display: inline-block;"><ymaps id="id_150000225920737853638" unselectable="on" class="ymaps-2-1-53-user-selection-none"><ymaps><ymaps class="ymaps-2-1-53-float-button ymaps-2-1-53-_hidden-text" style="max-width: 28px" title="Измерение расстояний на карте"><ymaps class="ymaps-2-1-53-float-button-icon ymaps-2-1-53-float-button-icon_icon_ruler"></ymaps><ymaps class="ymaps-2-1-53-float-button-text"></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-controls__control" style="margin-right: 0px; margin-left: 0px; bottom: auto; top: 108px; right: auto; left: 10px;"><ymaps><ymaps class="ymaps-2-1-53-zoom" style="height: 150px"><ymaps class="ymaps-2-1-53-zoom__plus ymaps-2-1-53-zoom__button ymaps-2-1-53-float-button ymaps-2-1-53-user-selection-none" unselectable="on"><ymaps class="ymaps-2-1-53-zoom__icon ymaps-2-1-53-float-button-icon"></ymaps></ymaps><ymaps class="ymaps-2-1-53-zoom__minus ymaps-2-1-53-zoom__button ymaps-2-1-53-float-button ymaps-2-1-53-user-selection-none" unselectable="on"><ymaps class="ymaps-2-1-53-zoom__icon ymaps-2-1-53-float-button-icon"></ymaps></ymaps><ymaps class="ymaps-2-1-53-zoom__scale"><ymaps class="ymaps-2-1-53-zoom__runner ymaps-2-1-53-zoom__button ymaps-2-1-53-float-button ymaps-2-1-53-zoom__runner__transition ymaps-2-1-53-touch-action-none ymaps-2-1-53-user-selection-none" unselectable="on" style="top: 36px;"><ymaps class="ymaps-2-1-53-zoom__icon ymaps-2-1-53-float-button-icon ymaps-2-1-53-float-button-icon_icon_runner"></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-searchpanel-pane" style="width: 100%; top: 0px; left: 0px; position: absolute; z-index: 10412;"><ymaps><ymaps class="ymaps-2-1-53-search ymaps-2-1-53-search_layout_panel ymaps-2-1-53-searchbox__panel-layout"><ymaps class="ymaps-2-1-53-search__layout"><ymaps id="id_150000225920737853623"><ymaps><ymaps class="ymaps-2-1-53-searchbox__input-cell"><ymaps class="ymaps-2-1-53-searchbox-input"><ymaps class="ymaps-2-1-53-searchbox-input__icon"></ymaps><input class="ymaps-2-1-53-searchbox-input__input" placeholder="Адрес или объект"><ymaps class="ymaps-2-1-53-searchbox-input__clear-button"></ymaps></ymaps><ymaps class="ymaps-2-1-53-searchbox-list-button"></ymaps></ymaps><ymaps class="ymaps-2-1-53-searchbox__button-cell"><ymaps class="ymaps-2-1-53-searchbox-button ymaps-2-1-53-user-selection-none" unselectable="on"><ymaps class="ymaps-2-1-53-searchbox-button-text">Найти</ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-searchbox__fold-button-cell"><ymaps class="ymaps-2-1-53-searchbox__fold-button"><ymaps class="ymaps-2-1-53-searchbox__fold-button-icon"></ymaps></ymaps></ymaps></ymaps><ymaps id="id_150000225920737853624"><ymaps><ymaps class="ymaps-2-1-53-islets_serp-popup ymaps-2-1-53-islets__hidden"><ymaps class="ymaps-2-1-53-islets_serp-popup__tail"></ymaps><ymaps class="ymaps-2-1-53-islets_serp" style="max-height: 400px;"><ymaps id="id_150000225920737853625"><ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-areas-pane" style="top: 0px; left: 0px; position: absolute; transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) scale(1, 1); z-index: 1605;"><ymaps class="ymaps-2-1-53-node-pane ymaps-2-1-53-user-selection-none" unselectable="on" style="z-index: 0; top: 0px; left: 0px; position: absolute;"><ymaps class="graphics-canvas" style="top: 0px; left: 0px; transform: translateZ(0px); position: absolute; z-index: 400000001;"><ymaps><canvas width="128" height="180" style="position: absolute; width: 128px; height: 180px; left: 204px; top: 133px;"></canvas></ymaps></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-places-pane" style="top: 0px; left: 0px; position: absolute; transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) scale(1, 1); z-index: 3206;"><ymaps class="ymaps-2-1-53-placemark-overlay ymaps-2-1-53-user-selection-none" unselectable="on" style="z-index: 800000000; height: 0px; width: 0px; position: absolute; left: 246px; top: 169px;"><ymaps class="ymaps-2-1-53-svg-icon ymaps_https___api_maps_yandex_ru_2_1_53_333986379159islands_dotIcon___1BAD03__1BAD03_34x41_1500002352231" style="position: absolute; width: 34px; height: 41px; left: -11px; top: -38px;"></ymaps></ymaps></ymaps><ymaps class="ymaps-2-1-53-balloon-pane" style="top: 0px; left: 0px; position: absolute; transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) scale(1, 1); z-index: 5607;"></ymaps></ymaps></ymaps></ymaps></ymaps><br>
<br>
&nbsp;</p>


<br>

<br>

<b><p><a id="contact"> ФОРМА ОБРАТНОЙ СВЯЗИ</a></p></b>

<br>

<br>





Для отзывов и предложений. <br>

При подаче запроса о наличии и стоимости шин, не забывайте указывать полный размер. 



<div class="anounce" id="feedback_protocol"></div><br>

<div id="feedback">

<fieldset id="contactUsForm">

<!--<legend>ФОРМА ОБРАТНОЙ СВЯЗИ</legend> 

<div class="alert forward">* Обязательные поля</div>

<br class="clearBoth">

<label for="contactname" class="inputLabel">ФИО:</label>



<input class="feed_input" id="feedback_fio" style="width:300px;" type="text" value=""><span class="alert">*</span><br class="clearBoth">

        <label for="contactname" class="inputLabel">Контактный телефон:</label>

        <input class="feed_input" type="text" id="feedback_tel" style="width:300px;" size="40"><span class="alert">*</span><br class="clearBoth">

        <label for="contactname" class="inputLabel">E-mail:</label><input class="feed_input" id="feedback_email" style="width:300px;" type="text" value=""><span class="alert">*</span><br class="clearBoth">

        <label for="email-address" class="inputLabel">Тема сообщения:</label><input class="feed_input" id="feedback_theme" style="width:300px;" type="text" value=""><span class="alert">*</span><br class="clearBoth">

        <label for="enquiry">Сообщение:<span class="alert">*</span></label><textarea class="feed_input" id="feedback_text" rows="7" cols="30" name="enquiry" style="float:left;margin:4px 0;width:93%;"></textarea> </fieldset>

    <div class="buttonRow back">

        <input type="image" id="do_feedback" title=" Отправить " alt="Отправить" src="/incom/template/template1/images/send_button.png">

    </div>

    <!--<div class="buttonRow back">

 <a href="javascript:history.back();">

    <img width="47" height="25" title=" Back " alt="Back" src="/incom/template/template1/images/buttons/english/button_back.gif">

 </a>

</div>

</div>

<script type="text/javascript" src="/incom/template/template1/js/jscript_jquery-1.3.2.min.js"></script>

<style>

.feed_input {

    border: 1px solid '.$border_color.';

    margin: 0 0 3px;

    vertical-align: middle;

    width: 49%;

}

</style>

<script type="text/javascript">

jQuery(document).ready(function()

{

    jQuery("#do_feedback").click(function()

    {

        var error_msg = "";

        var focused = 0;

        

        jQuery("#feedback_msg").html("").hide();

        

        if (jQuery("#feedback_fio").val() == ''){ 

            error_msg = error_msg+"1"; 

            jQuery("#feedback_fio").css("border", "1px solid red");             

            if (focused == 0) { 

                focused = 1; 

                jQuery("#feedback_fio").focus(); 

            }       

        } 

        else 

        { 

            jQuery("#feedback_fio").css("border", "1px solid green"); 

        }

        

        if (jQuery("#feedback_tel").val() == ''){ 

            error_msg = error_msg+"1"; 

            jQuery("#feedback_tel").css("border", "1px solid red");             

            if (focused == 0) { 

                focused = 1; 

                jQuery("#feedback_tel").focus(); 

            }       

        } 

        else 

        { 

            jQuery("#feedback_tel").css("border", "1px solid green"); 

        }

        

        if (jQuery("#feedback_email").val() == ''){ 

            error_msg = error_msg+"1"; 

            jQuery("#feedback_email").css("border", "1px solid red");   

            if (focused == 0) { 

                focused = 1; jQuery("#feedback_email").focus(); 

            }       

        } 

        else 

        { 

            jQuery("#feedback_email").css("border", "1px solid green");

        }

        

        if (jQuery("#feedback_theme").val() == ''){ 

            error_msg = error_msg+"1"; 

            jQuery("#feedback_theme").css("border", "1px solid red");   

            if (focused == 0) { 

                focused = 1; jQuery("#feedback_theme").focus(); 

            }       

        } 

        else 

        { 

            jQuery("#feedback_theme").css("border", "1px solid green");

        }

        

        if (jQuery("#feedback_text").val() == '')   { 

            error_msg = error_msg+"1"; 

            jQuery("#feedback_text").css("border", "1px solid red");        

            if (focused == 0) { 

                focused = 1; 

                jQuery("#feedback_text").focus(); 

            }   

        } 

        else 

        { 

            jQuery("#feedback_text").css("border", "1px solid green");

        }

    

        // Ошибок нет

        if (error_msg == "")

        {

            jQuery.ajax(

            {

                url: "/ru/page.php",

                data: "do=send&fio="+jQuery("#feedback_fio").val()+"&tel="+jQuery("#feedback_tel").val()+"&mail="+jQuery("#feedback_email").val()+"&theme="+jQuery("#feedback_theme").val()+"&text="+jQuery("#feedback_text").val()+"&x=secure",

                type: "POST",

                dataType : "html",

                cache: false,

                success:  function(data)  { jQuery("#feedback_protocol").html(data); },

                error: function()         { jQuery("#feedback_protocol").html("<p style=\"color:red;\">Невозможно связаться с сервером!</p>"); }

            });

            

        } else {

          jQuery("#feedback_protocol").html("Заполните обязательные поля").css("padding", "20px 0px").css("color", "red").slideDown(700);

        }

    });



});-->



</script></div></td>