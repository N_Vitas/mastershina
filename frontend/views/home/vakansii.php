<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'ВАКАНСИИ КОМПАНИИ';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJS("
	var show;
	function hidetxt(type){
	 param=document.getElementById(type);
	 if(param.style.display == 'none') {
	 if(show) show.style.display = 'none';
	 param.style.display = 'block';
	 show = param;
	 }else param.style.display = 'none'
	}
");
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
    	<div class="column-center-padding">         

				<p align="justify" class="MsoNormal">Наши сотрудники – это основа нашего успеха!!! Шинный Центр Мастершина предлагает отличные возможности развития профессионалам, которые могут способствовать успеху нашего бизнеса и бизнеса наших клиентов!<o:p></o:p></p>

				<p align="justify" class="MsoNormal">Если Вы ориентированы на успех, компетентны и инициативны, энергичны и коммуникабельны, заинтересованы в результатах своего труда и хотели бы работать у нас, отправьте свое резюме на адрес <a href="mailto:cv@mastershina.kz">cv@mastershina.kz</a></p>

				<p class="MsoNormal">&nbsp;</p>

				<div><a href="javascript:void(0)" onclick="hidetxt('div1'); return false;" rel="nofollow">Менеджер по продажам</a>

				<div id="div1" style="display: block;">&nbsp;
				<p align="justify" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:

				15.6pt;vertical-align:top"><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">&nbsp;</span></b><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">В динамично развивающийся Шинный Центр МАСТЕРШИНА требуются активные молодые люди на должность менеджера по продажам! Если Вы нацелены на результат, не любите лень и планируете активно развиваться как финансово так и как личность - предлагаем пройти собеседование в нашей компании! Подробно узнать о деятельности компании Вы можете на сайте <a href="http://WWW.MASTERSHINA.KZ">WWW.MASTERSHINA.KZ</a></span></b><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

				<p class="MsoNormal">Высылайте Ваши резюме на адрес : cv@mastershina.kz<o:p></o:p></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">ДОЛЖЕН ЗНАТЬ.</span></b><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l6 level1 lfo1;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">1.<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Основы ценообразования и маркетинга</span><span lang="EN-US" style="font-size: 9pt; font-family: Arial, sans-serif;">;</span><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l6 level1 lfo1;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">2.<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Психологию и принципы продаж</span><span lang="EN-US" style="font-size: 9pt; font-family: Arial, sans-serif;">;</span><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l6 level1 lfo1;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">3.<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Этику делового общения</span><span lang="EN-US" style="font-size: 9pt; font-family: Arial, sans-serif;">;</span><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l6 level1 lfo1;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">4.<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Правила установления деловых контактов и ведения телефонных переговоров;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l6 level1 lfo1;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">5.<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Правила эксплуатации вычислительной техники</span><span lang="EN-US" style="font-size: 9pt; font-family: Arial, sans-serif;">.</span><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">&nbsp;</span></b></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">Требования:</span></b><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Образование – минимум среднее специальное;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Опыт работы в продажах (не менее 1 года);&nbsp;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Грамотная четкая дикция;&nbsp;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Умение найти общий язык с клиентом;&nbsp;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Знание MS Office (EXEL не ниже среднего)&nbsp;и 1С;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Исполнительность;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Коммуникабельность;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Опыт работы оператором call центра приветствуется;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Нацеленность на результат;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Отсутствие вредных привычек приветствуется;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l4 level1 lfo2;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><b><i><span style="font-size: 9pt; font-family: Arial, sans-serif;">Желание зарабатывать много денег!!!</span></i></b><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">&nbsp;</span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">Условия:</span></b><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l0 level1 lfo3;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Фиксированная зарплата + % от продаж;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l0 level1 lfo3;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Официальное трудоустройство;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l0 level1 lfo3;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Гибкий рабочий график;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l0 level1 lfo3;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Хороший молодой коллектив;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l0 level1 lfo3;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Комфортное рабочее место;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l0 level1 lfo3;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Возможность карьерного роста в динамично развивающейся компании.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><br>
				<span style="font-size: 9pt; font-family: Arial, sans-serif;"><!--[if !supportLineBreakNewLine]--><br>
				<!--[endif]--><o:p></o:p></span></p>

				<h2><b><span style="font-size: 9pt; font-family: Arial, sans-serif;">ФУНКЦИИ И ДОЛЖНОСТНЫЕ ОБЯЗАННОСТИ</span></b><span style="font-size: 9pt; font-family: Arial, sans-serif;"><o:p></o:p></span></h2>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">Организация и ведение продаж продукции Шинного Центра:<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">работа с впервые обратившимися клиентами,&nbsp;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">поиск потенциальных клиентов;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">ведение коммерческих переговоров с клиентами в интересах Шинного Центра;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">оперативное реагирование на информацию, поступающую от клиентов, и доведение ее до сведения &nbsp;Руководителя отдела продаж;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">выяснение потребностей клиентов в продукции, реализуемой Шинным Центром, и согласование заказа с клиентом в соответствие с его потребностями и наличием ассортимента на складском комплексе Шинного Центра;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">мотивация клиентов на работу с Шинным Центром, в соответствии с утвержденными программами по стимулированию сбыта;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Закрытие сделок с клиентом.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l5 level1 lfo4;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Участвовать в общекорпоративных мероприятиях.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">&nbsp;</span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">Планирование и аналитическая работа:<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">составление ежемесячного плана продаж;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">предоставление отчетов по итогам работы в соответствии с регламентом работы отдела и Шинного Центра.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">заниматься самообучением: изучение спецификаций и потребительских свойств всех, вновь поступивших, товаров, технологий их применения, спецификации альтернативных материалов, необходимых для работы.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">&nbsp;</span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">Обеспечение продаж:<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">прием и обработка заказов клиентов, оформление необходимых документов, связанных с отгрузкой продукции для клиентов Шинного Центра, закрепленных за собой, а также для клиентов, закрепленных за соответствующими ведущими менеджерами по продажам при нахождении их вне офиса;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">осуществление информационной поддержки клиентов;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Формировать коммерческие предложения по товарным группам.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">осуществление информирования клиентов обо всех изменениях в ассортименте, увеличениях и снижениях цен, акциях по стимулирования спроса, времени прихода продукции на склад;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">окончательное согласование с клиентом условий по ценам, дате отгрузки и способу доставки продукции;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">передача заявок на доставку продукцию клиентам в отдел логистики;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">участие в разработке и реализации проектов связанных с деятельностью отдела продаж;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">взаимодействие с подразделениями Шинного Центра с целью выполнения возложенных задач;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">участие в рабочих совещаниях;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">ведение рабочей и отчетной документации.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l1 level1 lfo6;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">поддержание в актуальном состоянии данных о клиенте в информационной системе.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">&nbsp;</span></p>

				<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:

				justify;line-height:15.6pt;vertical-align:top"><span style="font-size: 9pt; font-family: Arial, sans-serif;">Контрольная:<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l3 level1 lfo7;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">контроль отгрузок продукции клиентам;<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l3 level1 lfo7;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">контроль финансовой дисциплины клиента на основе документов, получаемых от отдела учета в торговле и предупреждение о сроках оплаты.<o:p></o:p></span></p>

				<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;

				margin-left:24.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;

				line-height:15.6pt;mso-list:l3 level1 lfo7;tab-stops:list 36.0pt;vertical-align:

				top"><!--[if !supportLists]--><span style="font-size: 10pt; font-family: Symbol;">·<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size: 9pt; font-family: Arial, sans-serif;">Сообщать Руководителю отдела продаж о действиях сотрудников других служб, мешающих выполнению прямых функциональных обязанностей.<o:p></o:p></span></p>
				</div>
				</div>

				<p>&nbsp;</p>

				<div><a href="#" onclick="hidetxt('div2'); return false;" rel="nofollow">Шиномонтажник</a>

				<div id="div2" style="display:block;">&nbsp;
				<p class="MsoNormal">Знаешь работу шиномонтажника? Есть желание иметь официальную работу? Приходи к нам.</p>

				<p class="MsoNormal"><b>&nbsp;</b></p>

				<p class="MsoNormal"><b>от 60 000 KZT</b></p>

				<p class="MsoNormal">&nbsp;</p>

				<p class="MsoNormal"><b>Условия работы:</b></p>

				<ul>
					<li>Оклад + процент.</li>
					<li>Официальное трудоустройство, соц пакет</li>
					<li>Скользящий график</li>
					<li>Молодой коллектив (24 - 32 года)</li>
					<li>Шинный центр "МастерШина", Сатпаева (р-н ТРК АДК)</li>
				</ul>

				<p class="MsoNormal">&nbsp;</p>

				<p class="MsoNormal"><b>Требования:</b></p>

				<ul>
					<li>Опыт работы.</li>
					<li>Наличие документов</li>
					<li>Исполнительность</li>
				</ul>

				<p class="MsoNormal">&nbsp;</p>

				<p><b>Обязанности:</b></p>

				<ul>
					<li>Качественное оказание шиномонтажных услуг</li>
					<li>Ремонт порезов и проколов</li>
					<li>Правка железных и легкосплавных дисков</li>
					<li>Замена шпилек и болтов</li>
					<li>Заполнение заказ-нарядов по выполненным работам</li>
					<li>Замена колодок (дисковых и барабанных тормозов)</li>
					<li>Поддержание чистоты на рабочем месте</li>
					<li>Учет и контроль наличия рабочего инструмента и расходных материалов</li>
				</ul>

				<p class="MsoNormal"><b>Некурящие у нас зарабатывают больше, а непьющие работают дольше.</b></p>

				<p class="MsoNormal">&nbsp;</p>

				<p class="MsoNormal"><o:p></o:p></p>

				<p class="MsoNormal"><o:p></o:p></p>
				</div>
				</div>

				<p class="MsoNormal">&nbsp;</p>

				<p align="justify" class="MsoNormal">Кроме того, Шинный центр МАСТЕРШИНА &nbsp;рад предложить студентам 3-4 курсов отличную возможность пройти производственную практику в рамках их академической программы. У Вас появится возможность получить знания по своей специальности и изучить особенности работы одного из лидирующих Шинных центрах в Алматы. Мы принимаем студентов, которые разделяют наши ценности и придерживаются правил и дисциплины. Шинный центр Мастершина предлагает пройти производственную практику на условиях полной занятости в течение 1-3 месяцев по следующим профилям: производство (механик-шиномонтажник),бухгалтерия, административный персонал (офис менеджер, ассистент руководителя), продажи (менеджер по продажам, менеджер по работе с корпоративными клиентами, account - менеджер)<o:p></o:p></p>

				<p align="justify" class="MsoNormal">Если&nbsp; Вы&nbsp; желаете пройти производственную практику в&nbsp; Шинном центре МАСТЕРШИНА, просим связаться с нами&nbsp; по&nbsp; телефону <span class="ringo-gorod">+7 727 335 69 82</span> или отправить заявку в произвольной форме на адрес <a href="mailto:CV@MASTERSHINA.KZ">CV@MASTERSHINA.<span lang="EN-US">KZ</span></a> с темой ПРАКТИКА .</p>

				<p class="MsoNormal">&nbsp;</p>


				</div>
	</div>
</div>
</div>