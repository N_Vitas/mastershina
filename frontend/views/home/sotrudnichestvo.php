<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Сотрудничество';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
    	<div class="column-center-padding"> 

				<p style="text-align: center;">&nbsp;<img align="middle" alt="Сотрудничество-МастерШина-1" border="10" height="330" src="/images/2461dffb83cd60863ff9d596671dbaf5.jpg" title="Сотрудничество-1" width="440"></p>

				<p style="text-align: center;"><b>Вы работаете в сфере автобизнеса и хотели бы получать дополнительную прибыль за счет продажи автошин?</b></p>
				<p style="text-align: center;"><b>Вы можете стать представителем нашей компании в Вашем городе!</b></p>
				<p>Для этого Вам нужен только компьютер, доступ в интернет и желание продавать шины. Мы предоставим Вам онлайн доступ в нашу информационную систему, где Вы всегда будете видеть актуальные цены и наличие товара на складе компании.</p>
				<p>Удобные варианты оплаты и оперативная доставка товара в Ваш город.</p>

				<p>&nbsp;</p>

				<p>Для получения дополнительной информации заполните заявку с темой сообщения и указанием города, например&nbsp;<b><i>Дилер из Актау.</i></b></p>

				<p>&nbsp;</p>

				<p><a href="/home/contact" target="_blank"><b>Заполнить и отправить заявку здесь</b></a></p>

				<p>&nbsp;</p>

				<p>&nbsp;</p>


			</div>
	</div>
</div>
</div>