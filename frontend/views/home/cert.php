<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\MenuLeft;

$this->title = 'Подарочный Сертификат';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
	<div class="row">
	  <div class="col-md-3 m-b-1">
	  <?= MenuLeft::widget() ?>
	  
	  </div>
	  <div class="clearfix visible-sm visible-xs"></div>

	  <div class="col-md-9">
    	<div class="title"><span><?= Html::encode($this->title) ?></span></div>
    	<div class="column-center-padding">        

				<center><img alt="Подарочный Сертификат-МастерШина-1" src="/images/cert.jpg" style="width: 600px; height: 297px;" title="Подарочный Сертификат-1"></center>

				<p>&nbsp;</p>

				<p>Мы предоставляем Вам возможность сделать действительно полезный подарок. Если Ваш близкий человек является автовладельцем, то Подарочный сертификат на товары и услуги шинного центра МастерШина придется как нельзя кстати.&nbsp;</p>

				<center>Дарите с удовольствием!</center>
				&#65279;&nbsp;<br>
				<br>
				&nbsp;
				<center><b><font color="red">Правила приобретения и использования Подарочного сертификата.</font></b></center>
				<br>
				<b><font color="red">1.</font></b>&nbsp;Подарочный сертификат дает возможность владельцу оплатить любые услуги,а также приобрести товар в шинном центре МастерШина в пределах указанной на нем суммы.&nbsp;<br>
				<br>
				<b><font color="red">2.</font></b>&nbsp;Cрок действия сертификата 3 месяца с даты приобретения. Пролонгация срока действия Подарочного сертификата не производится.&nbsp;<br>
				<br>
				<b><font color="red">3.</font></b>&nbsp;Подарочный сертификат не является именным. Любое физическое лицо, предъявившее Подарочный сертификат, может воспользоваться им. В случае утраты Подарочного сертификата, в том числе хищения, сертификат не может быть восстановлен, и денежные средства не могут быть возвращены.&nbsp;<br>
				<br>
				<b><font color="red">4.</font></b>&nbsp;Обмен одного или нескольких Подарочных сертификатов на другие Подарочные сертификаты с целью изменения номинала не производится.&nbsp;<br>
				<br>
				<b><font color="red">5.</font></b>&nbsp;Если цена выбранного товара и/или услуги ниже номинала Подарочного Сертификата, остаток денежными средствами не выплачивается; Если цена выбранного товара и/или услуги выше номинала Подарочного Сертификата, предъявитель может доплатить недостающую сумму.&nbsp;<br>
				<br>
				<b><font color="red">6.</font></b>&nbsp;Подарочный сертификат нельзя использовать для обналичивания денежных средств.&nbsp;<br>
				<br>
				<b><font color="red">7.</font></b>&nbsp;Допускается суммирование номиналов нескольких Подарочных Сертификатов. Одну покупку можно оплатить несколькими подарочными сертификатами. При возврате товара, оплаченного с использованием Подарочного сертификата, возврат денежных средств осуществляется на новый Подарочный сертификат.&nbsp;<br>
				<br>
				<b><font color="red">8.</font></b>&nbsp;Доставка сертификата осуществляется в квадрате улиц Калдаякова, Аль Фараби, Момышулы, Рыскулова стоимостью 500 тенге. &#65279;&nbsp;&#65279;
			</div>
	</div>
	</div></div>
