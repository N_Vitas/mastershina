<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use common\models\Service;
use frontend\widgets\MenuLeft;

$this->title = 'Прайс-лист на услуги шинного центра';
$this->params['breadcrumbs'][] = $this->title;
$models = Service::find()->innerjoinWith('detail')->where(['service.type'=>'Шиномонтаж'])->all(); 
$material = Service::find()->innerjoinWith('detail')->where(['service.type'=>'Расходные материалы'])->all();
$remont = Service::find()->innerjoinWith('detail')->where(['service.type'=>'Ремонт'])->all();
$dop = Service::find()->innerjoinWith('detail')->where(['service.type'=>'Дополнительные услуги'])->all();
$shinybakup = Service::find()->innerjoinWith('detail')->where(['in','service.id',[15,16,17,18]])->all(); 
$aboniment = Service::getAbonimentPrice();
?>
<div class="site-price">
<div class="row">
  <div class="col-md-3 m-b-1">
  <?= MenuLeft::widget() ?>
  
  </div>
  <div class="clearfix visible-sm visible-xs"></div>

  <div class="col-md-9">

    <div class="title"><span>Годовой абонимент для постоянных клиентов</span></div>
	  <table class="table table-bordered table-compare">
	   	<thead>
	   		<tr>
	   			<th class="compare-product col-md-3">Наименование услуг</th>
	   			<th class="compare-product" colspan="10" class="text-center">Легковые автомобили</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		<tr>
	   			<th class="compare-product">Высота профиля шины</th>
		    	<?php	foreach ($aboniment['nodisc'][0]['params'] as $a):?>
					<th class="compare-product text-center"><?= $a['param']?></th>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['nodisc'][0]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][0]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['nodisc'][1]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][0]['params'] as $a):?>
					<td><?= number_format($aboniment['nodisc'][1]['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['nodisc'][4]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][4]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td class="text-warning"><?= $aboniment['nodisc'][6]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][6]['params'] as $a):?>
					<td class="text-warning"><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<th><?= $aboniment['nodisc'][8]['title']?></th>	   			
		    	<?php	foreach ($aboniment['nodisc'][8]['params'] as $a):?>
					<th><?= number_format($a['price'],0,',',' ')?></th>
	   			<?php endforeach;?>
	   		</tr>

	   		<tr>
	   			<td><?= $aboniment['disc'][0]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][0]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['disc'][1]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][0]['params'] as $a):?>
					<td><?= number_format($aboniment['disc'][1]['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['disc'][4]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][4]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td class="text-warning"><?= $aboniment['disc'][6]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][6]['params'] as $a):?>
					<td class="text-warning"><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<th><?= $aboniment['disc'][8]['title']?></th>	   			
		    	<?php	foreach ($aboniment['disc'][8]['params'] as $a):?>
					<th><?= number_format($a['price'],0,',',' ')?></th>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td rowspan="3">Приятный бонус</td>		
	   			<td style="background:white;text-align:center;" colspan="10"><?= $aboniment['service'][0]['title']?></td>
	   		</tr>
	   		<tr>
	   			<td style="background:white;text-align:center;" colspan="10"><?= $aboniment['service'][1]['title']?></td>
	   		</tr>
	   		<tr>
	   			<td style="background:white;text-align:center;" colspan="10"><?= $aboniment['service'][2]['title']?></td>
	   		</tr>
	   	</tbody>
	  </table>
	  <hr/>
	  <table class="table table-bordered table-compare">
	   	<thead>
	   		<tr>
	   			<th class="compare-product col-md-3">Наименование услуг</th>
	   			<th class="compare-product" colspan="10" class="text-center">4х4, Джип, Минивэн</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		<tr>
	   			<th class="compare-product">Радиус диска</th>
		    	<?php	foreach ($aboniment['nodisc'][2]['params'] as $a):?>
					<th class="compare-product text-center">R<?= $a['param']?></th>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['nodisc'][2]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][2]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['nodisc'][3]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][0]['params'] as $a):?>
					<td><?= number_format($aboniment['nodisc'][3]['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['nodisc'][5]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][5]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td class="text-warning"><?= $aboniment['nodisc'][7]['title']?></td>	   			
		    	<?php	foreach ($aboniment['nodisc'][7]['params'] as $a):?>
					<td class="text-warning"><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<th><?= $aboniment['nodisc'][9]['title']?></th>	   			
		    	<?php	foreach ($aboniment['nodisc'][9]['params'] as $a):?>
					<th><?= number_format($a['price'],0,',',' ')?></th>
	   			<?php endforeach;?>
	   		</tr>

	   		<tr>
	   			<td><?= $aboniment['disc'][2]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][2]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['disc'][3]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][0]['params'] as $a):?>
					<td><?= number_format($aboniment['disc'][3]['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td><?= $aboniment['disc'][5]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][5]['params'] as $a):?>
					<td><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<td class="text-warning"><?= $aboniment['disc'][7]['title']?></td>	   			
		    	<?php	foreach ($aboniment['disc'][7]['params'] as $a):?>
					<td class="text-warning"><?= number_format($a['price'],0,',',' ')?></td>
	   			<?php endforeach;?>
	   		</tr>
	   		<tr>
	   			<th><?= $aboniment['disc'][9]['title']?></th>	   			
		    	<?php	foreach ($aboniment['disc'][9]['params'] as $a):?>
					<th><?= number_format($a['price'],0,',',' ')?></th>
	   			<?php endforeach;?>
	   		</tr>
	   	</tbody>
	  </table>
    <div class="title"><span><?= Html::encode($this->title) ?></span></div>
	  <table class="table table-bordered">
	   	<thead>
	   		<tr>
	   			<th>Выездной шиномонтаж за выезд</th>
					<th>Услуга отогрева или зарядки АКБ</th>
					<th>Заберем-доставим шины</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		<tr>
	   			<td class="text-center">2000</td>   			
					<td class="text-center">5000</td>
					<td class="text-center">2000</td>
	   		</tr>
	   	</tbody>
	  </table>
	  <div class="clear"></div>
	  <hr/>

	  <table class="table table-bordered table-compare">
	   	<thead>
	   		<tr>
	   			<th class="compare-product">Наименование услуг</th>
	   			<th class="compare-product" colspan="10" class="text-center">Легковые автомобили</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		<tr>
	   			<th class="compare-product">Высота профиля шины</th>   			
					<th class="compare-product text-center">70</th>
					<th class="compare-product text-center">65</th>
					<th class="compare-product text-center">60</th>
					<th class="compare-product text-center">55</th>
					<th class="compare-product text-center">50</th>
					<th class="compare-product text-center">45</th>
					<th class="compare-product text-center">40</th>
					<th class="compare-product text-center">35*</th>
					<th class="compare-product text-center">30*</th>
					<th class="compare-product text-center">25*</th>
	   		</tr>
		    <?php	foreach ($models as $model):?>
				<?php $details = $model->detail;
				// $aboniment['disc'][0]['title'] 
				// $aboniment['disc'][0]['type'] = 'Легковые автомобили',
				// $aboniment['disc'][0]['params'][] = ['price'=>0,'param'=>0]
				?>
	   		<tr>
	   			<td><?= $model->title?></td>
					<?php	foreach ($details as $detail):?>
					<?php if($detail->title == "Легковые автомобили"):?>
					<td><?= $detail->price ?></td>
	 				<?php endif;?>
	 				<?php endforeach;?>
	 			</tr>
	   		<?php endforeach;?>
	   		<tr>
	   			<td colspan="11">* без стоимости расходных материалов</td>
	   		</tr>
	   	</tbody>
	  </table>
	  <div class="clear"></div>
	  <hr/>
	  <table class="table table-bordered table-compare">
	   	<thead>
	   		<tr>
	   			<th class="compare-product">Наименование услуг</th>
	   			<th class="compare-product" colspan="10" class="text-center">4х4, Джип, Минивэн</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		<tr>
	   			<td class="compare-product">Радиус диска</td> 
	   			<th class="compare-product text-center">R15</th>
	   			<th class="compare-product text-center">R16</th>
	   			<th class="compare-product text-center">R17</th>
	   			<th class="compare-product text-center">R18</th>
	   			<th class="compare-product text-center">R19</th>
	   			<th class="compare-product text-center">R20</th>
	   			<th class="compare-product text-center">R21</th>
	   			<th class="compare-product text-center">R22</th>
	   			<th class="compare-product text-center">R24*</th>
	   			<th class="compare-product text-center">R26*</th>
	   		</tr>
		    <?php	foreach ($models as $model):?>
				<?php $details = $model->detail;?>
	   		<tr>
	   			<td><?= $model->title?></td>
					<?php	foreach ($details as $detail):?>
					<?php if($detail->title == "4х4, Джип, Минивэн"):?>
					<td><?= $detail->price ?></td>
	 				<?php endif;?>
	 				<?php endforeach;?>
	 			</tr>
	   		<?php endforeach;?>
	   		<tr>
	   			<td colspan="11">* без стоимости расходных материалов</td>
	   		</tr>
	   	</tbody>
	  </table>
	  <div class="clear"></div>
	  <hr/>
		<table class="table table-bordered table-compare">
			<thead>
				<tr>
					<th colspan="2" class="compare-product">Сезонное хранение в месяц</th>
				</tr>
			</thead>
			<tbody>
	  	<?php	foreach ($shinybakup as $model):?>
 				<tr>
 					<th colspan="2"><?= $model->title?></th>
 				</tr>
 				<?php $items = $model->detail;?>		 				
		  	<?php	foreach ($items as $model):?>
	 				<tr>
	 					<td><?= $model->title?></td>
	 					<td><?= $model->price?> тг.</td>
	 				</tr> 					
		   	<?php endforeach;?>					
	   	<?php endforeach;?>
			</tbody>
		</table>
	  <hr/>
		<table class="table table-bordered table-compare">
			<thead>
				<tr>
					<th colspan="2" class="compare-product">Расходные материалы</th>
				</tr>
			</thead>
			<tbody>
	  	<?php	foreach ($material as $model):?>
 				<tr>
 					<th colspan="2"><?= $model->title?></th>
 				</tr>
 				<?php $items = $model->detail;?>		 				
		  	<?php	foreach ($items as $model):?>
	 				<tr>
	 					<td><?= $model->title?></td>
	 					<td><?= $model->price?> тг./<?= $model->unit?></td>
	 				</tr> 					
		   	<?php endforeach;?>					
	   	<?php endforeach;?>
			</tbody>
		</table>
	  <?php	foreach ($remont as $model):?>
		  <div class="clear"></div>
		  <hr/>
	  	<table class="table table-bordered table-compare">
				<thead>
					<tr>
						<th colspan="2" class="compare-product"><?= $model->title?></th>
					</tr>
				</thead>
				<tbody>
 				<?php $items = $model->detail;?>		 				
		  	<?php	foreach ($items as $model):?>
	 				<tr>
	 					<td><?= $model->title?></td>
	 					<?php if($model->params == ">="):?>
	 						<td>от <?= $model->price?> тг./<?= $model->unit?></td>
	 					<?php else:?>
	 						<td><?= $model->price?> тг./<?= $model->unit?></td>
	 					<?php endif;?>
	 				</tr> 					
		   	<?php endforeach;?>	
	   		<tr>
	   			<td colspan="2">* - Цены на ремонт указаны без шиномонтажных работ</td>
	   		</tr>
				</tbody>
			</table>
	  <?php endforeach;?>

  	<?php	foreach ($dop as $model):?>
		  <div class="clear"></div>
		  <hr/>
	  	<table class="table table-bordered table-compare">
				<thead>
					<tr>
						<th colspan="2" class="compare-product"><?= $model->title?></th>
					</tr>
				</thead>
				<tbody>
 				<?php $items = $model->detail;?>		 				
		  	<?php	foreach ($items as $model):?>
	 				<tr>
	 					<td><?= $model->title?></td>
	 					<?php if($model->params == ">="):?>
	 						<td>от <?= $model->price?> тг./<?= $model->unit?></td>
	 					<?php elseif($model->params == "plus"):?>
	 						<td>плюс <?= $model->price?> тг. за 1 <?= $model->unit?></td>
	 					<?php else:?>
	 						<td><?= $model->price?> тг./<?= $model->unit?></td>
	 					<?php endif;?>
	 				</tr> 					
		   	<?php endforeach;?>	
	   		<tr>
	   			<td colspan="2">*** - при снятии и установке старых датчиков давления компания ответственности не несет</td>
	   		</tr>
				</tbody>
			</table>
	  <?php endforeach;?>
	  <div class="clear"></div>
	  <hr/>
  	<table class="table table-bordered table-compare">
			<thead>
				<tr>
					<th colspan="2" class="compare-product">Сложный монтаж</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Деформированные шины</td>
					<td rowspan="3">плюс 50% от прайса</td>
				</tr> 
				<tr>
					<td>Шины с технологией Run Flat**</td>
					<!-- <td>плюс 50% от прайса</td> -->
				</tr> 
				<tr>
					<td>Грязевые шины (MUD terrain, M/T)**</td>
					<!-- <td>плюс 50% от прайса</td> -->
				</tr> 
				<tr>
					<td>Монтаж дисков "ХРОМ"</td>
					<td>плюс 500 тг за 1 ед.</td>
				</tr> 					
   		<tr>
   			<td colspan="2">* - Цены на ремонт указаны без шиномонтажных работ</td>
   		</tr>
			</tbody>
		</table>
	</div>
</div>
</div>
