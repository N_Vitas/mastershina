<div class="column-center-padding">
                <h1 style="margin-top:14px; font-size:16px; margin-bottom:20px;">
                        Публичная оферта Договора хранения                    </h1>
          

<p class="MsoNormal"><strong><span style="font-size:11.0pt;mso-bidi-font-weight:normal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></p>

<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size:11.0pt;mso-bidi-font-weight:normal">&nbsp;ДОГОВОР ХРАНЕНИЯ</span></strong></p>

<p class="MsoNormal" style="text-align: center;">&nbsp;</p>

<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size:11.0pt;mso-bidi-font-weight:normal"><o:p></o:p></span></strong></p>

<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;&nbsp; от «___» _____________ 2015г<o:p></o:p></b></p>

<p class="MsoNormal"><b>Индивидуальный предприниматель</b><span style="font-size:9.0pt"> Мирзатбаева Зухра, Св. ИП серия 07915 № 0231018 именуемое в дальнейшем "Хранитель", с одной стороны, и </span><b>&nbsp;&nbsp;ФИО____________________________________, &nbsp;ИИН_________________________,</b><b><span style="font-size:9.0pt"><o:p></o:p></span></b></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">именуемый в дальнейшем "Поклажедатель", с другой стороны, заключили настоящий Договор о нижеследующем:<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">&nbsp;</span></p>

<p align="center" class="MsoNormal" style="text-align:center"><b>Преамбула<o:p></o:p></b></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">Настоящий Договор представляет собой публичную оферту, размещенную на официальном веб-сайте.<!--  Хранителя, расположенного по адресу: <a href="/sezonnoe-hranenie-shin/">СЕЗОННОЕ ХРАНЕНИЕ ШИН</a> --></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>1. Предмет договора.</strong><span style="font-size:9.0pt"><o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">1.1. Предметом договора являются неподлежащие лицензированию услуги по сезонному хранению автомобильных шин и (или) дисков, принадлежащих Поклажедателю согласно Акту приема-возврата, который оформляется Приложением №1 к настоящему Договору. <o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>2. Обязанности сторон.</strong><strong><span style="font-size:9.0pt"><o:p></o:p></span></strong></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.1. Хранитель обязуется хранить автошины и диски, переданные ему Поклажедателем, и возвратить эти автошины и диски в сохранности.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.2. Хранитель обязан по требованию Поклажедателя возвратить принятые на хранение автошины и диски в течение 3-х суток с момента требования, оформив при этом Акт приема-возврата товарно-материальных ценностей.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.3. Хранитель обязан возвратить Поклажедателю те самые автошины и диски, которые были переданы на хранение.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.4. Хранитель обязуется принять меры для сохранности переданных ему автошин и дисков, обязательность которых предусмотрена законом или нормами (противопожарными, санитарными, охранными).<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.5. Хранитель обязуется не использовать без согласия Поклажедателя переданные на хранение автошины и диски, а равно не предоставлять возможность пользоваться ими третьим лицам.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.6. По истечении срока хранения, обусловленного настоящим Договором, Поклажедатель обязан немедленно забрать переданные на хранение автошины и диски. <o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">2.7. Поклажедатель обязан выплатить Хранителю вознаграждение за хранение в размере и на условиях, предусмотренных настоящим Договором.<o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>3. Сумма Договора и порядок расчётов.<o:p></o:p></strong></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">3.1. Вознаграждение за хранение по настоящему Договору указывается в Приложении №1 и определяется согласно установленного прейскуранта:</span><span lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US"><o:p></o:p></span></p>

<table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="width:489.5pt;margin-left:4.95pt;border-collapse:collapse;mso-yfti-tbllook:
    1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt" width="653">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:38.25pt">
			<td style="width: 248.55pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 38.25pt;" width="331">
			<p align="center" class="MsoNormal" style="text-align:center"><span arial="" style="font-family:">&nbsp;<o:p></o:p></span></p>
			</td>
			<td style="width: 2cm; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 38.25pt;" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><b>за 1 колесо в месяц<o:p></o:p></b></p>
			</td>
			<td style="width: 2cm; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 38.25pt;" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><b>за 4 колеса в месяц<o:p></o:p></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1;height:12.75pt">
			<td nowrap="nowrap" style="width: 248.55pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 12.75pt;" valign="bottom" width="331">
			<p class="MsoNormal"><span style="font-size:9.0pt;">Легковые шины<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">300<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">1 200<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2;height:12.75pt">
			<td nowrap="nowrap" style="width: 248.55pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 12.75pt;" valign="bottom" width="331">
			<p class="MsoNormal"><span style="font-size:9.0pt;">Легковые шины с дисками<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">350<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">1 400<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3;height:12.75pt">
			<td nowrap="nowrap" style="width: 248.55pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 12.75pt;" valign="bottom" width="331">
			<p class="MsoNormal">Шины для внедорожников, SUV, минивэн</p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">425<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">1 700<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:12.75pt">
			<td nowrap="nowrap" style="width: 248.55pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 12.75pt;" valign="bottom" width="331">
			<p class="MsoNormal">Шины с дисками для внедорожников, SUV, минивэн</p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">500<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width:2.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt" valign="bottom" width="76">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="font-size:9.0pt">2 000<o:p></o:p></span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal" style="margin-left:36.0pt;text-align:justify"><span style="font-size:9.0pt">*- Подробности узнавайте у менеджеров Компании<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">&nbsp;</span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">3.2 Услуга вывоза и доставки автошин и дисков Поклажедателю предоставляется Хранителем без вознаграждения, при условии заказа услуги выездного шиномонтажа, либо оплачивается по <b>2000/3000 тг за поездку</b>.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">3.3. При оплате за хранение автошин в конце срока хранения вознаграждение рассчитывается согласно прейскуранту за фактический период хранения. Поклажедатель лишается возможности участия в АКЦИИ.<o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>4. Ответственность сторон и форс-мажор.</strong><strong><span style="font-size:9.0pt"><o:p></o:p></span></strong></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.1. Хранитель отвечает за утрату, недостачу или повреждение автошин и дисков, если не докажет, что утрата, недостача или повреждение произошли вследствие непреодолимой силы. При этом Поклажедатель понимает и соглашается, что в процессе очистки автошин и дисков могут быть выявлены ранее незамеченные повреждения. <o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.2. Убытки, причиненные Поклажедателю утратой, недостачей или повреждением автошин и дисков, возмещаются Хранителем в размере остаточной стоимости автошин и дисков, определяемой с общего согласия сторон.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.3. Если изменение условий хранения необходимо для устранения опасности утраты или повреждения автошин и дисков, Хранитель вправе изменить место хранения, не уведомляя Поклажедателя.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.4. Если по истечении срока хранения более 7 дней, находящиеся на хранении автошины и диски не взяты обратно Поклажедателем, он обязуется уплатить Хранителю вознаграждение в размере 2% от суммы вознаграждения согласно настоящего Договора за каждый просроченный день.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.5. При неисполнении Поклажедателем своей обязанности взять автошины и диски обратно, в том числе при его уклонении от получения автошин и дисков, Хранитель вправе, через 60 суток&nbsp; после завершения срока настоящего Договора, утилизировать или реализовать автошины и диски Поклажедателя без уведомления Поклажедателя. Остаточная стоимость автошин Поклажедателю не выплачивается.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.6. Если хранение прекращается до истечения обусловленного срока по желанию Поклажедателя, Хранитель имеет право оставить вознаграждение в размере полной суммы.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.7. Если хранение прекращается досрочно по обстоятельствам, возникшим со стороны Хранителя, он обязан вернуть сумму вознаграждения Поклажедателю.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">4.8. Сторона, не исполнившая или ненадлежащим образом исполнившая свои обязательства по настоящему Договору при выполнении его условий, несёт ответственность, если не докажет, что ненадлежащее исполнение обязательств оказалось невозможным вследствие непреодолимой силы (форс-мажор).<o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>5. Срок действия Договора.</strong><strong><span style="font-size:9.0pt"><o:p></o:p></span></strong></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">5.1. Срок, на который заключен настоящий Договор, соответствует сроку хранения шин и (или) дисков и указывается в Приложении №1.<o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><b>6. Особые условия</b><span style="font-size:9.0pt"><o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">Поклажедатель, подписывая Приложение №1 к настоящему Договору, признает, что ознакомлен и полностью согласен с положениями настоящего Договора.<o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>7. Иные положения.</strong><strong><span style="font-size:9.0pt"><o:p></o:p></span></strong></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">7.1. По всем вопросам, не предусмотренным настоящим Договором, стороны будут руководствоваться действующим законодательством Республики Казахстан.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">7.2. В случае возникновения споров по настоящему Договору стороны примут все меры к разрешению их путём переговоров между собой. Если стороны не смогут прийти к согласию, не решат спор путём переговоров, то данный спор подлежат рассмотрению в судебных органах Республики Казахстан.<o:p></o:p></span></p>

<p align="center" class="MsoNormal" style="text-align:center"><strong>8. Адреса и реквизиты сторон<o:p></o:p></strong></p>

<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
    mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:
    .5pt solid windowtext;mso-border-insidev:.5pt solid windowtext">
	<tbody>
		<tr>
			<td style="width:266.4pt;border:solid windowtext 1.0pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="355">
			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">Хранитель</span></b><strong><span style="font-size:9.0pt;mso-bidi-font-weight:normal"><o:p></o:p></span></strong></p>
			</td>
			<td style="width:266.4pt;border:solid windowtext 1.0pt;
            border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="355">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="font-size:8.0pt">Поклажедатель</span></b><strong><span style="font-size:9.0pt;mso-bidi-font-weight:normal"><o:p></o:p></span></strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:266.4pt;border:solid windowtext 1.0pt;
            border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="355">
			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">ИП Мирзатбаева З.Х.<o:p></o:p></span></b></p>

			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">РНН 600719117325 <o:p></o:p></span></b></p>

			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">ИИН 550919401524<o:p></o:p></span></b></p>

			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">АГФ АО «Банк Центр Кредит»<o:p></o:p></span></b></p>

			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">БИК </span></b><b><span lang="EN-US" style="font-size:8.0pt;mso-ansi-language:EN-US">KCJBKZKX</span></b><b><span style="font-size:8.0pt"><o:p></o:p></span></b></p>

			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">ИИК </span></b><b><span lang="EN-US" style="font-size:8.0pt;mso-ansi-language:EN-US">KZKZ</span></b><b><span style="font-size:8.0pt">07319</span></b><b><span lang="EN-US" style="font-size:8.0pt;
            mso-ansi-language:EN-US">A</span></b><b><span style="font-size:8.0pt">010003523141 <o:p></o:p></span></b></p>

			<p class="MsoNormal" style="text-align:justify"><b><span style="font-size:8.0pt">050034, г.Алматы, ул.Казакова 22/2<o:p></o:p></span></b></p>

			<p class="MsoNormal"><b><span style="font-size:8.0pt">тел. 390-64-04</span></b><strong><span style="font-size:9.0pt;mso-bidi-font-weight:normal"><o:p></o:p></span></strong></p>
			</td>
			<td style="width:266.4pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="355">
			<p align="center" class="MsoNormal" style="text-align:center"><strong>&nbsp;</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:266.4pt;border:solid windowtext 1.0pt;
            border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="355">
			<p class="MsoNormal" style="text-align:justify"><span style="font-size:8.0pt">_______________________&nbsp;&nbsp; <b>Мирзатбаева З.Х. <o:p></o:p></b></span></p>

			<p class="consplusnonformat"><span new="" roman="" style="font-size:6.0pt;font-family:" times="">(ФИО, подпись)<strong><span style="font-weight:normal"><o:p></o:p></span></strong></span></p>
			</td>
			<td style="width:266.4pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="355">
			<p class="consplusnonformat"><span new="" roman="" style="font-size:6.0pt;font-family:" times="">______________________________________<o:p></o:p></span></p>

			<p class="consplusnonformat"><span new="" roman="" style="font-size:6.0pt;font-family:" times="">(ФИО, подпись)</span><strong><span courier="" new="" style="font-size:9.0pt;font-family:"><o:p></o:p></span></strong></p>
			</td>
		</tr>
	</tbody>
</table>

<p align="center" class="MsoNormal" style="text-align:center"><strong>&nbsp;</strong></p>

<p class="consplusnonformat"><span new="" roman="" style="font-size:6.0pt;font-family:" times="">&nbsp;</span></p>

<p class="MsoNormal"><span style="font-size:8.0pt">&nbsp;</span></p>

<table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="width:18.0cm;margin-left:5.4pt;border-collapse:collapse;mso-yfti-tbllook:
    1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt" width="680">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td colspan="2" style="width:108.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="145">
			<p class="MsoNormal"><span style="color:#514F50">+7&nbsp;727&nbsp;390 64 04<o:p></o:p></span></p>
			</td>
			<td colspan="3" style="width:152.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="203">
			<p class="MsoNormal"><span style="color:#514F50">+7&nbsp;777 7&nbsp;157 157<o:p></o:p></span></p>
			</td>
			<td colspan="3" style="width:158.55pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="211">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">Приложение №1<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td colspan="2" style="width:108.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="145">
			<p class="MsoNormal"><span style="color:#514F50">+7&nbsp;701 7&nbsp;157 157<o:p></o:p></span></p>
			</td>
			<td colspan="3" style="width:152.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="203">
			<p class="MsoNormal"><span style="color:#514F50">+7&nbsp;707 7&nbsp;157 157<o:p></o:p></span></p>
			</td>
			<td colspan="3" style="width:158.55pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="211">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">к Договору хранения<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td colspan="2" style="width:108.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="145">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="77">&nbsp;</td>
			<td style="width:46.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:72.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="97">&nbsp;</td>
			<td style="width:46.5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:39.25pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="52">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:3;height:30.0pt">
			<td colspan="9" style="width:433.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:30.0pt" valign="top" width="577">
			<table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="width:498.5pt;border-collapse:collapse;mso-yfti-tbllook:1184;
                mso-padding-alt:0cm 5.4pt 0cm 5.4pt" width="665">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
                        height:33.0pt">
						<td style="width: 132.85pt; border-style: solid; border-color: windowtext black windowtext windowtext; border-width: 1pt; padding: 0cm 5.4pt; height: 33pt; background: rgb(216, 216, 216);" valign="top" width="177">
						<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">большой ассортимент автошин и дисков<o:p></o:p></span></b></p>
						</td>
						<td style="width:98.65pt;border-top:solid windowtext 1.0pt;
                        border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
                        background:#D8D8D8;padding:0cm 5.4pt 0cm 5.4pt;height:33.0pt" valign="top" width="132">
						<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">выездной шиномонтаж<o:p></o:p></span></b></p>
						</td>
						<td style="width: 95.75pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 33pt; background: rgb(216, 216, 216);" valign="top" width="128">
						<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">стационарный шиномонтаж<o:p></o:p></span></b></p>
						</td>
						<td style="width:87.0pt;border-top:solid windowtext 1.0pt;
                        border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
                        mso-border-left-alt:solid windowtext .5pt;background:#D8D8D8;padding:0cm 5.4pt 0cm 5.4pt;
                        height:33.0pt" valign="top" width="116">
						<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">отогрев авто зарядка АКБ<o:p></o:p></span></b></p>
						</td>
						<td style="width:84.25pt;border-top:solid windowtext 1.0pt;
                        border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
                        background:#D8D8D8;padding:0cm 5.4pt 0cm 5.4pt;height:33.0pt" valign="top" width="112">
						<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">правка&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; дисков<o:p></o:p></span></b></p>
						</td>
					</tr>
				</tbody>
			</table>

			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;</span></p>

			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">АКТ ПРИЁМА-ВОЗВРАТА<br>
			ТОВАРНО-МАТЕРИАЛЬНЫХ ЦЕННОСТЕЙ<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="81">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="77">&nbsp;</td>
			<td style="width:46.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td colspan="4" style="width:206.55pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="275">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">«___» _____________ 2015г<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">Передал:<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width: 358.9pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;<o:p></o:p></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="64">
			<p class="MsoNormal"><span style="color:#514F50">ИИН:<o:p></o:p></span></p>
			</td>
			<td colspan="3" style="width:152.35pt;border-top:solid windowtext 1.0pt;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="203">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">&nbsp;<o:p></o:p></span></b></p>
			</td>
			<td style="width:72.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="97">
			<p class="MsoNormal"><span style="color:#514F50">тел.:<o:p></o:p></span></p>
			</td>
			<td colspan="2" style="width:85.75pt;border-top:solid windowtext 1.0pt;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">&nbsp;<o:p></o:p></span></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">&nbsp;</td>
			<td colspan="2" nowrap="nowrap" style="width:106.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="141">
			<p class="MsoNormal"><span style="color:#514F50">срок хранения, мес.:<o:p></o:p></span></p>
			</td>
			<td style="width:46.35pt;border-top:solid windowtext 1.0pt;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="62">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="color:#514F50">&nbsp;<o:p></o:p></span></b></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:120.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="161">
			<p class="MsoNormal"><span style="color:#514F50">Сумма оплаты, тг.: ___<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width: 85.75pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">Принял:<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="479">
			<p class="MsoNormal" style="text-align:justify"><span style="color:#514F50">ИП Мирзатбаева З.Х. &nbsp;ИИН 550919401524, ул.Садовникова, 99<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:9;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="81">&nbsp;</td>
			<td nowrap="nowrap" style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="77">&nbsp;</td>
			<td style="width:46.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:72.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="97">&nbsp;</td>
			<td style="width:46.5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:39.25pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="52">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:10;height:15.75pt">
			<td rowspan="2" style="width: 13.65pt; border-style: solid; border-color: windowtext windowtext black; border-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">№<o:p></o:p></span></p>
			</td>
			<td colspan="8" nowrap="nowrap" style="width:419.35pt;border-top:
            solid windowtext 1.0pt;border-left:none;border-bottom:solid windowtext 1.0pt;
            border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="559">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">ДАННЫЕ ПРЕДМЕТА<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11;height:16.5pt">
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext 1.0pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="81">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">Размер<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">Наименование (модель, шипы, диски, колпачок диска)<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">1<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="479">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">2<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="479">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:14;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">3<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="479">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:15;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">4<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="479">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:16;height:16.5pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">5<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext 1.0pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="64">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="77">&nbsp;</td>
			<td style="width:46.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="62">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="64">&nbsp;</td>
			<td style="width:72.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="97">&nbsp;</td>
			<td style="width:46.5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="62">&nbsp;</td>
			<td style="width:39.25pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="52">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18;height:16.5pt">
			<td style="width: 13.65pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 16.5pt;" width="18">
			<p class="MsoNormal"><span style="color:#514F50">№<o:p></o:p></span></p>
			</td>
			<td style="width: 60.45pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">DOT номер<o:p></o:p></span></p>
			</td>
			<td colspan="5" nowrap="nowrap" style="width: 273.15pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="top" width="364">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">Замеченные повреждения<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:85.75pt;border-top:
            solid windowtext 1.0pt;border-left:none;border-bottom:solid windowtext 1.0pt;
            border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">Страна пр-ва<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:19;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">1<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="5" nowrap="nowrap" style="width:273.15pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="364">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:85.75pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:20;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">2<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="5" nowrap="nowrap" style="width:273.15pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="364">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:85.75pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:21;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">3<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="5" nowrap="nowrap" style="width:273.15pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="364">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:85.75pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:22;height:15.75pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.75pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">4<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="5" nowrap="nowrap" style="width:273.15pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="364">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:85.75pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black 1.0pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:23;height:16.5pt">
			<td style="width: 13.65pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" width="18">
			<p align="right" class="MsoNormal" style="text-align:right"><span style="color:#514F50">5<o:p></o:p></span></p>
			</td>
			<td style="width:60.45pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext 1.0pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="5" nowrap="nowrap" style="width:273.15pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
            mso-border-bottom-alt:solid windowtext 1.0pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" valign="top" width="364">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
			<td colspan="2" nowrap="nowrap" style="width:85.75pt;border-top:
            none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="114">
			<p align="center" class="MsoNormal" style="text-align:center"><span style="color:#514F50">&nbsp;<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:24;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="81">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="77">&nbsp;</td>
			<td style="width:46.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:72.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="97">&nbsp;</td>
			<td style="width:46.5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:39.25pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="52">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:25;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">Передал:<o:p></o:p></span></p>
			</td>
			<td colspan="4" nowrap="nowrap" style="width: 200.35pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="267">&nbsp;</td>
			<td colspan="3" nowrap="nowrap" style="width: 158.55pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="211">
			<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;<o:p></o:p></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:26;height:9.95pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:9.95pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="81">&nbsp;</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="font-size: 6pt;">(ФИО, подпись)<o:p></o:p></span></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:27;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">Принял:<o:p></o:p></span></p>
			</td>
			<td colspan="4" nowrap="nowrap" style="width: 200.35pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="267">
			<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;<o:p></o:p></b></p>
			</td>
			<td colspan="3" nowrap="nowrap" style="width: 158.55pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="211">
			<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;<o:p></o:p></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:28;height:9.95pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:9.95pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="81">&nbsp;</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="font-size: 6pt;">(ФИО, подпись)<o:p></o:p></span></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:29;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="81">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="77">&nbsp;</td>
			<td style="width:46.35pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:72.8pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="97">&nbsp;</td>
			<td style="width:46.5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:39.25pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="52">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:30;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="81">
			<p class="MsoNormal"><span style="color:#514F50">Возвратил:<o:p></o:p></span></p>
			</td>
			<td colspan="4" nowrap="nowrap" style="width: 200.35pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="267">
			<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;<o:p></o:p></b></p>
			</td>
			<td colspan="3" nowrap="nowrap" style="width: 158.55pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="211">
			<p align="center" class="MsoNormal" style="text-align:center"><b>&nbsp;<o:p></o:p></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:31;height:9.95pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:9.95pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="81">&nbsp;</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="font-size: 6pt;">(ФИО, подпись)<o:p></o:p></span></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:32;height:16.5pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt" width="18">&nbsp;</td>
			<td colspan="3" style="width:166.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:16.5pt" valign="top" width="222">
			<p class="MsoNormal"><span style="color:#514F50">Получил, претензий не имею:<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width: 46.35pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="62">
			<p class="MsoNormal"><b>&nbsp;<o:p></o:p></b></p>
			</td>
			<td nowrap="nowrap" style="width: 48pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="64">
			<p class="MsoNormal"><b>&nbsp;<o:p></o:p></b></p>
			</td>
			<td nowrap="nowrap" style="width: 72.8pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="97">
			<p class="MsoNormal"><b>&nbsp;<o:p></o:p></b></p>
			</td>
			<td nowrap="nowrap" style="width: 46.5pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="62">
			<p class="MsoNormal"><span style="font-size: 11pt; font-family: Calibri, sans-serif;">&nbsp;<o:p></o:p></span></p>
			</td>
			<td nowrap="nowrap" style="width: 39.25pt; border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 16.5pt;" valign="bottom" width="52">
			<p class="MsoNormal"><b>&nbsp;<o:p></o:p></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:33;height:9.95pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:9.95pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="81">&nbsp;</td>
			<td colspan="7" nowrap="nowrap" style="width:358.9pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:9.95pt" valign="top" width="479">
			<p align="center" class="MsoNormal" style="text-align:center"><b><span style="font-size: 6pt;">(ФИО, подпись)<o:p></o:p></span></b></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:34;mso-yfti-lastrow:yes;height:15.75pt">
			<td style="width:13.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" width="18">&nbsp;</td>
			<td style="width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="81">&nbsp;</td>
			<td style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="64">&nbsp;</td>
			<td style="width:58.0pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="77">&nbsp;</td>
			<td colspan="3" nowrap="nowrap" style="width:167.15pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt" valign="bottom" width="223">
			<p class="MsoNormal"><b>"___" __________ 20__г.<o:p></o:p></b></p>
			</td>
			<td style="width:46.5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="62">&nbsp;</td>
			<td style="width:39.25pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:15.75pt" valign="top" width="52">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">&nbsp;</span></p>

<!-- <p style="text-align: justify"><a href="/sezonnoe-hranenie-shin/" target="_blank"><b>Расценки на сезонное хранение шин</b></a></p>

<p style="text-align: justify">&nbsp;</p>

<h3 style="text-align: left; "><a href="/upload/userfiles/Price_mastershina.zip"><b>СКАЧАТЬ ПРАЙС-ЛИСТ</b></a></h3> -->

<p>&nbsp;</p>


</div>
</div>