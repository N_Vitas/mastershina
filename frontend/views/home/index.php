<?php
use yii\helpers\Url;
use frontend\widgets\MenuLeft;
use frontend\widgets\Cards;
/* @var $this yii\web\View */
$this->title = 'Блог от интернет-магазина МастерШина';
?>
  <div class="row">
    <div class="col-md-3 m-b-1">
    <?= MenuLeft::widget() ?>
    
    </div>
    <!-- End New Arrivals & Best Selling -->

    <div class="clearfix visible-sm visible-xs"></div>

    <div class="col-md-9">
    
      <div class="title"><span>Шины</span></div>
      <div class="product-slider owl-controls-top-offset">
        <?php foreach ($shiny as $model):?>
          <div class="box-product-outer">            
            <?= Cards::widget([
              'category'=>'shiny',
              'model'=>$model,
              'link'=> '/shiny/detail/'.$model->url,
              // 'sale'=>['title'=> 'Акция','style'=>'danger'],
              // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
              // 'rating'=>['count'=>5,'rating'=>4],
            ])?>
          </div>
        <?php endforeach;?>
      </div>

      <div class="clearfix visible-sm visible-xs"></div>

      <div class="title"><span>Диски</span></div>
      <div class="product-slider owl-controls-top-offset">
        <?php foreach ($disc as $model):?>
          <div class="box-product-outer">            
            <?= Cards::widget([
              'category'=>'disc',
              'model'=>$model,
              'link'=> '/disc/detail/'.$model->url,
              // 'sale'=>['title'=> 'Акция','style'=>'danger'],
              // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
              // 'rating'=>['count'=>5,'rating'=>4],
            ])?>
          </div>
        <?php endforeach;?>
      </div>

      <div class="clearfix visible-sm visible-xs"></div>

      <div class="title"><span>Масла</span></div>
      <div class="product-slider owl-controls-top-offset">
        <?php foreach ($oil as $model):?>
          <div class="box-product-outer">            
            <?= Cards::widget([
              'category'=>'oil',
              'model'=>$model,
              'link'=> '/oil/detail/'.$model->url,
              // 'sale'=>['title'=> 'Акция','style'=>'danger'],
              // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
              // 'rating'=>['count'=>5,'rating'=>4],
            ])?>
          </div>
        <?php endforeach;?>
      </div>

      <div class="title"><span>Акумуляторы</span></div>
      <div class="product-slider owl-controls-top-offset">
        <?php foreach ($battery as $model):?>
          <div class="box-product-outer">            
            <?= Cards::widget([
              'category'=>'battery',
              'model'=>$model,
              'link'=> '/battery/detail/'.$model->url,
              // 'sale'=>['title'=> 'Акция','style'=>'danger'],
              // 'featured'=>['title'=> 'Рекомендуемые','style'=>'default'],
              // 'rating'=>['count'=>5,'rating'=>4],
            ])?>
          </div>
        <?php endforeach;?>
      </div>
    </div>

  </div>