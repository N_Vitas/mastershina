<?php
namespace frontend\controllers;

use frontend\components\BaseController;

use common\models\SearchDisc;
use common\models\Shiny;
use common\models\Disc;

/**
 * Disc controller
 */
class DiscController extends BaseController
{
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex($params)
  {
    $data = explode(";",$params);
    $search=[];
    if(count($data) > 0)
    for ($i=0; $i <= count($data); $i++) {
      $search = $this->getParams($data[$i],$search);
    }  
    // var_dump($search);
    $searchModel = new SearchDisc();
    $disc = $searchModel->search($search);
    // $shiny->query->where(['>','ostatok',0]);
    $shiny = Shiny::find()->limit(20)->all();
    return $this->render('index',compact('shiny','disc','searchModel'));
  }

  public function actionDetail($params='')
  { 
    if($params != ''){
      if($model = Disc::find()->where('url=:url',[':url'=>$params])->one()){
        return $this->render('detail',compact('model'));        
      }else{
        throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');
      }
    }    
    throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');   
  }
  private function getParams($params,$search){
    if(preg_match('/width[\w\-]+/', $params)) {
      $search['width'] = str_replace('width-','',$params);
    }
    if(preg_match('/wradius[\w\-]+/', $params)) {
      $search['wradius'] = str_replace('wradius-','',$params);
    }
    if(preg_match('/count[\w\-]+/', $params)) {
      $search['count'] = str_replace('count-','',$params);
    }
    if(preg_match('/borer[\w\-]+/', $params)) {
      $search['borer'] = str_replace('borer-','',$params);
    }
    if(preg_match('/stupica[\w\-]+/', $params)) {
      $search['stupica'] = str_replace('stupica-','',$params);
    }
    if(preg_match('/radius[\w\-]+/', $params)) {
      $search['radius'] = str_replace('radius-','',$params);
    }
    if(preg_match('/facturer[\w\-]+/', $params)) {
      $search['facturer'] = str_replace('facturer-','',$params);
    }
    if(preg_match('/color[\w\-]+/', $params)) {
      $search['color'] = str_replace('color-','',$params);
    }
    if(preg_match('/type[\w\-]+/', $params)) {
      $search['type'] = str_replace('type-','',$params);
    }
    if(preg_match('/vendor[\w\-]+/', $params)) {
      $search['vendor'] = str_replace('vendor-','',$params);
    }
    if(preg_match('/car[\w\-]+/', $params)) {
      $search['car'] = str_replace('car-','',$params);
    }
    if(preg_match('/year[\w\-]+/', $params)) {
      $search['year'] = str_replace('year-','',$params);
    }
    if(preg_match('/modification[\w\-]+/', $params)) {
      $search['modification'] = str_replace('modification-','',$params);
    }
    if(preg_match('/brend[\w\-]+/', $params)){
      $search['brend'] = str_replace('brend-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    return $search;
  }

  public function actionFilter($width='',$wradius='',$count='',$borer='',$stupica='',$radius='',$facturer='',$color='',$vendor='',$type='',$car='',$year='',$modification=''){
    $where = [];
    $andWhereSize = [];
    $andWhereBorer = [];
    $auto=[];
    $option['width'][] = ['key'=>'all','val'=>'Все'];
    $option['wradius'][] = ['key'=>'all','val'=>'Все'];
    $option['count'][] = ['key'=>'all','val'=>'Все'];
    $option['borer'][] = ['key'=>'all','val'=>'Все'];
    $option['stupica'][] = ['key'=>'all','val'=>'Все'];
    $option['radius'][] = ['key'=>'all','val'=>'Все'];
    $option['facturer'][] = ['key'=>'all','val'=>'Все'];
    $option['color'][] = ['key'=>'all','val'=>'Все'];

    $search = new SearchDisc();
    if($vendor != '' && $vendor != 'all'){$auto['vendor']=$search->_vendor[$vendor];}
    if($car != '' && $car != 'all'){$auto['car']=$search->_car[$car];}
    if($year != '' && $year != 'all'){$auto['year']=$search->_year[$year];}
    if($modification != '' && $modification != 'all'){$auto['modification']=$search->_modification[$modification];}
    foreach ($search->getVendor($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['vendor'][] = ['key'=>$key,'val' => $value];
    }
    foreach ($search->getCar($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['car'][] = ['key'=>$key,'val' => $value];
    }
    foreach ($search->getYear($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['year'][] = ['key'=>$key,'val' => $value];
    }
    foreach ($search->getModification($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['modification'][] = ['key'=>$key,'val' => $value];
    }

    if($width != '' && $width != 'all' && $wradius != '' && $wradius != 'all'){
      $where['size'] = $search->_radius[$wradius].'x'.$search->_width[$width];
    }
    else{
      if($width != '' && $width != 'all'){
        $andWhereSize = ['like','size','x'.$width];
      }
      if($wradius != '' && $wradius != 'all'){
        $andWhereSize = ['like','size',$wradius.'x'];
      }
    } 
    if($count != '' && $count != 'all' && $borer != '' && $borer != 'all'){
      $where['borer'] = $search->_countBorer[$count].'x'.$search->_distanceBorer[$borer];
    }
    else{
      if($count != '' && $count != 'all'){
        $andWhereBorer = ['like','borer',$count.'x'];
      }
      if($borer != '' && $borer != 'all'){
        $andWhereBorer = ['like','borer','x'.$borer];
      }
    }
    if($stupica != '' && $stupica != 'all'){$where['stupica'] = $search->_stupica[$stupica];}
    if($radius != '' && $radius != 'all'){$where['radius'] = $search->_vylet[$radius];}
    if($facturer != '' && $facturer != 'all'){$where['facturer'] = $search->_facturer[$facturer];}
    if($color != '' && $color != 'all'){$where['color'] = $search->_color[$color];}
    // Оставил из-за отсутствия времени 
    // if($m = Disc::find()->select('size')->where($where)->andWhere($andWhereSize)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('size')->all()){
    //   foreach ($m as $model) {
    //     if($model->size == ''){
    //       continue;
    //     }
    //     $t = explode('x',$model->size);        
    //     $option['width'][] = ['key'=>$model->generateUrl($t[1]),'val' => $t[1]];
    //     $option['wradius'][] = ['key'=>$model->generateUrl($t[0]),'val' => $t[0]];
    //   }


    //   $option['width'] = $this->my_unique($option['width']);
    //   $option['wradius'] = $this->my_unique($option['wradius']);
    // }
    // if($m = Disc::find()->select('borer')->where($where)->andWhere($andWhereBorer)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('borer')->all()){
    //   foreach ($m as $model) {
    //     if($model->borer == ''){
    //       continue;
    //     }
    //     $t = explode('x',$model->borer);        
    //     $option['count'][] = ['key'=>$model->generateUrl($t[0]),'val' => $t[0]];
    //     $option['borer'][] = ['key'=>$model->generateUrl($t[1]),'val' => $t[1]];
    //   }


    //   $option['count'] = $this->my_unique($option['count']);
    //   $option['borer'] = $this->my_unique($option['borer']);
    // }
    return json_encode($option,JSON_UNESCAPED_UNICODE);
  }

  private function my_unique($array){
    $aux_res = array() ;
    $result_res = array() ;

    //создание одномерного массива
    foreach ($array as $key => $value) {
      $aux_res[$key] = $value['key'];
    }
   
    $aux_res = array_unique($aux_res) ;

    //собираем обратно
    foreach ($aux_res as $key => $value) {
      $result_res[$key] = $array[$key];
    }
    return $result_res;   
  }
}
