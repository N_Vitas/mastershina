<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\components\BaseController;

/**
 * Site controller
 */
class DeliveryController extends BaseController
{  
  public function actionIndex()
  {
    return $this->render('delivery');
  }
  public function actionCreditRassrochka()
  {
    return $this->render('shinycredit');
  }  
  public function actionRasshirennajaGarantija()
  {
    return $this->render('rasshirennaja-garantija');
  }
}
