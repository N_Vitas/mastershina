<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\components\BaseController;

/**
 * Site controller
 */
class ServiceController extends BaseController
{  
  public function actionDelivery()
  {
    return $this->render('delivery');
  }
  public function actionCreditRassrochka()
  {
    return $this->render('shinycredit');
  }  
  public function actionRasshirennajaGarantija()
  {
    return $this->render('rasshirennaja-garantija');
  } 
  public function actionVyezdnoyShinomontazh()
  {
    return $this->render('vyezdnoyshinomontazh');
  }
  public function actionStacionarnyjShinomontazh()
  {
  	return $this->render('stacionarnyj-shinomontazh');
  }
	public function actionSezonnoeHranenieShin(){
		return $this->render('sezonnoe-hranenie-shin');	
	}
	public function actionUtilizacijaIznoshennyhShin(){
		return $this->render('utilizacija-iznoshennyh-shin');	
	}
}
