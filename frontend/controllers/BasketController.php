<?php
namespace frontend\controllers;

use frontend\components\BaseController;

use Yii;
use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;

/**
 * Battery controller
 */
class BasketController extends BaseController
{
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex()
  { 
    return $this->render('index');
  }
  public function actionSuccess()
  { 
    return $this->render('success');
  }

}
