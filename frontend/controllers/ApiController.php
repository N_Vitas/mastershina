<?php
namespace frontend\controllers;

use frontend\components\BaseController;

use yii;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;
use common\models\Order;
use common\models\ServiceOrder;
/**
 * Oil controller
 */
class ApiController extends yii\web\Controller
{
  public function behaviors()
  {
      $behaviors = parent::behaviors();
      // $behaviors['authenticator'] = [
      //     'class' => 'frontend\components\HttpHeaderAuth',
      //     'except' => ['index'],
      // ];
      $behaviors['corsFilter'] = [
          'class' => \yii\filters\Cors::className(),
          'cors' => [
              'Origin' => ['*'],
          ],

      ];
      return $behaviors;
  }

  public function actions()
  {
      return [
        'error' => [
          'class' => 'yii\web\ErrorAction',
        ]
      ];
  }

  public function init(){
    Yii::$app->response->format = Response::FORMAT_JSON; 
  }
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex($params='')
  {   
    // $shiny = Shiny::find()->orderBy('radius')->groupBy('radius')->all();
    // foreach ($shiny as $model) {
    //   echo "'".$model->generateUrl($model->radius)."'=>'".$model->radius."',";
    // }
    if(empty($params))
    return ['site'=>yii::$app->name];
    return ['site'=>yii::$app->name,'params'=>$params];
  }

  public function actionBasket($params='')
  {
    // Yii::$app->cart->removeAll();   
    // $shiny = Shiny::find()->where(['>','price',0])->limit(6)->all();
    // if($shiny){
    //   foreach ($shiny as $model) {
    //     $i = 1;
    //     Yii::$app->cart->put($model, $i);
    //     if($i > 2)$i=0;
    //     $i++;
    //   }      
    // }
    $basket = [];
    if(Yii::$app->cart->getCount()){
      foreach (Yii::$app->cart->getPositions() as $id => $model) {
        $basket[] = [
          'position' => $id,
          'code' => $model->code,
          'sklad' => $model->sklad,
          'title' => $model->title,
          'url' => $model->picture,
          'count' => $model->getQuantity(),
          'price' => $model->price,
          'old_price' => $model->old_price,
          'ostatok' => $model->ostatok,
        ];        
      }
    }

    return $basket;
  }

  public function actionBasketPut($id,$cat,$quantity=1)
  {
    if($id && $cat){
      switch ($cat) {
        case 'shiny':
          $model = Shiny::find()->select(['id','code','sklad','title','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'disc':
          $model = Disc::find()->select(['id','code','sklad','title','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'battery':
          $model = Battery::find()->select(['id','code','sklad','title','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'oil':
          $model = Oil::find()->select(['id','code','sklad','title','picture','price','old_price','ostatok'])->where('id = :id',[':id'=>$id])->one();
          break;
        case 'service':
          $model = ServiceOrder::find()->where('id = :id',[':id'=>$id])->one();
          break;
        default:
          $model = false;
          break;
      }
      // $shiny = Shiny::find()->where(['>','price',0])->limit(6)->all();
      if($model){
        Yii::$app->cart->put($model, $quantity);
      }
      $basket = [];
      if(Yii::$app->cart->getCount()){
        foreach (Yii::$app->cart->getPositions() as $id => $model) {
          $basket[] = [
            'position' => $id,
            'code' => $model->code,
            'sklad' => $model->sklad,
            'title' => $model->title,
            'url' => $model->picture,
            'count' => $model->getQuantity(),
            'price' => $model->price,
            'old_price' => $model->old_price,
            'ostatok' => $model->ostatok,
          ];        
        }
      }

      return $basket;      
    }
    throw new yii\web\HttpException(404,"неверные данные");
    
    // Yii::$app->cart->removeAll();   
  }

  public function actionBasketDelete($position)
  {
    if($position){      
      $basket = [];
      if(Yii::$app->cart->getCount()){
        Yii::$app->cart->removeById($position);
        foreach (Yii::$app->cart->getPositions() as $id => $model) {
          $basket[] = [
            'position' => $id,
            'code' => $model->code,
            'sklad' => $model->sklad,
            'title' => $model->title,
            'url' => $model->picture,
            'count' => $model->getQuantity(),
            'price' => $model->price,
            'old_price' => $model->old_price,
            'ostatok' => $model->ostatok,
          ];        
        }
      }

      return $basket;      
    }
    throw new yii\web\HttpException(404,"неверные данные");
    
    // Yii::$app->cart->removeAll();   
  }

  public function actionBasketUpdate($position, $count){
    Yii::$app->cart->update(Yii::$app->cart->getPositionById($position),$count);
  }

  public function actionOrderCreate(){      
    $order = new Order();
    // $order->delivery = Yii::$app->request->post("delivery");
    // $order->payment = Yii::$app->request->post("payment");
    // $order->name = Yii::$app->request->post("name");
    // $order->email = Yii::$app->request->post("email");
    // $order->phone = Yii::$app->request->post("phone");
    // $order->address = Yii::$app->request->post("address");
    // $order->comment = Yii::$app->request->post("comment");
    // $order->data = Yii::$app->request->post("data");
    // $order->price = Yii::$app->request->post("price");
    // $order->status = Order::ORDER_CREATE;
    $order->attributes = Yii::$app->request->post();
    if($order->save()){
      Yii::$app->cart->removeAll();
      return ['result'=>true,'id'=>$order->id];      
    }
    else
      return array_merge(['result'=>false],$order->errors);
  }

  public function actionServiceOrderCreate(){
    $order = new ServiceOrder();
    $order->attributes = Yii::$app->request->post();
    if($order->save()){
      return ['result'=>true,'id'=>$order->id];      
    }
    else
      return array_merge(['result'=>false],$order->errors);
  }
}
