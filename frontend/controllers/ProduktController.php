<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\components\BaseController;


use light\yii2\XmlParser;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Request;

use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;

/**
 * Site controller
 */
class ProduktController extends BaseController //yii\web\Controller
{
  public $productName;
  public function beforeAction($action)
  {    
    return parent::beforeAction($action);
  }
  public function actions()
  {
    return parent::actions();
  }
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex()
  {    
    $url = \Yii::$app->request->get('url');    
    if($url != null){
      if($model = Shiny::find()->where(['url'=>$url])->one()){
        return $this->render('index',compact('model'));        
      }
      if($model = Battery::find()->where(['url'=>$url])->one()){
        return $this->render('index',compact('model'));        
      }
      if($model = Oil::find()->where(['url'=>$url])->one()){
        return $this->render('index',compact('model'));        
      }
      if($model = Disc::find()->where(['url'=>$url])->one()){
        return $this->render('disc',compact('model'));        
      }
      return $this->goHome();
    }
    return $this->goHome();
  }
}
