<?php
namespace frontend\controllers;

use frontend\components\BaseController;

use common\models\SearchOil;
use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;

/**
 * Oil controller
 */
class OilController extends BaseController
{
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex($params)
  {       
    $data = explode(";",$params);
    $search=[];
    if(count($data) > 0)
    for ($i=0; $i <= count($data); $i++) {
      $search = $this->getParams($data[$i],$search);
    }  
    // var_dump($search);
    $searchModel = new SearchOil();
    $oil = $searchModel->search($search);
    // $shiny->query->where(['>','ostatok',0]);
    $battery = Battery::find()->limit(20)->all();
    return $this->render('index',compact('battery','oil','searchModel'));
  }

  public function actionDetail($params='')
  { 
    if($params != ''){
      if($model = Oil::find()->where('url=:url',[':url'=>$params])->one()){
        return $this->render('detail',compact('model'));        
      }else{
        throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');
      }
    }   
    throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');    
  }

  public function actionFilter($size='',$viscosity_1='',$type_fluid='',$type_engine=''){
    // $row = 'type_engine';
    // $file = '';
    // $m = Oil::find()->select($row)->where(['>','ostatok',0])->andWhere(['>','price',0])->orderBy($row)->groupBy($row)->all();
    // $file .= "['all'=>'Все'";
    // foreach($m as $model){
    //   if($model->$row == '')
    //     continue;
    //   $file .= ",'".$model->generateUrl($model->$row)."'=>'".$model->$row."'";
    // } 
    // $file .= "];";
    // file_put_contents('oil.txt', $file);
    // return;
    $where = [];
    $option['size'][] = ['key'=>'all','val'=>'Все'];
    $option['viscosity_1'][] = ['key'=>'all','val'=>'Все'];
    $option['type_fluid'][] = ['key'=>'all','val'=>'Все'];
    $option['type_engine'][] = ['key'=>'all','val'=>'Все'];

    $search = new SearchOil();
    if($size != '' && $size != 'all'){$where['size'] = $search->_size[$size];}
    if($viscosity_1 != '' && $viscosity_1 != 'all'){$where['viscosity_1'] = $search->_viscosity_1[$viscosity_1];}
    if($type_fluid != '' && $type_fluid != 'all'){$where['type_fluid'] = $search->_type_fluid[$type_fluid];}
    if($type_engine != '' && $type_engine != 'all'){$where['type_engine'] = $search->_type_engine[$type_engine];}

    if($m = Oil::find()->select('size')->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('size')->all()){
      foreach ($m as $model) {
        if($model->size == '')
          continue;
        $option['size'][] = ['key'=>$model->generateUrl($model->size),'val'=>$model->size];
      }
    }
    if($m = Oil::find()->select('viscosity_1')->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('viscosity_1')->all()){
      foreach ($m as $model) {
        if($model->viscosity_1 == '')
          continue;
        $option['viscosity_1'][] = ['key'=>$model->generateUrl($model->viscosity_1),'val'=>$model->viscosity_1];
      }
    }
    if($m = Oil::find()->select('type_fluid')->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('type_fluid')->all()){
      foreach ($m as $model) {
        if($model->type_fluid == '')
          continue;
        $option['type_fluid'][] = ['key'=>$model->generateUrl($model->type_fluid),'val'=>$model->type_fluid];
      }
    }
    if($m = Oil::find()->select('type_engine')->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('type_engine')->all()){
      foreach ($m as $model) {
        if($model->type_engine == '')
          continue;
        $option['type_engine'][] = ['key'=>$model->generateUrl($model->type_engine),'val'=>$model->type_engine];
      }
    }
    return json_encode($option,JSON_UNESCAPED_UNICODE);
  }

  private function getParams($params,$search){
    if(preg_match('/sorter[\w\-]+/',$params)){$search['sorter'] = str_replace('sorter-','',$params);}
    if(preg_match('/size[\w\-]+/',$params)){$search['size'] = str_replace('size-','',$params);}
    if(preg_match('/viscosity_1[\w\-]+/',$params)){$search['viscosity_1'] = str_replace('viscosity_1-','',$params);}
    if(preg_match('/type_fluid[\w\-]+/',$params)){$search['type_fluid'] = str_replace('type_fluid-','',$params);}
    if(preg_match('/type_engine[\w\-]+/',$params)){$search['type_engine'] = str_replace('type_engine-','',$params);}
    if(preg_match('/brend[\w\-]+/', $params)){
      $search['brend'] = str_replace('brend-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    return $search;
  }
}
