<?php
namespace frontend\controllers;

use frontend\components\BaseController;

use common\models\SearchBattery;
use common\models\Battery;
use common\models\Oil;

/**
 * Battery controller
 */
class BatteryController extends BaseController
{
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex($params)
  {        
    $data = explode(";",$params);
    $search=[];
    if(count($data) > 0)
    for ($i=0; $i <= count($data); $i++) {
      $search = $this->getParams($data[$i],$search);
    }  
    $searchModel = new SearchBattery();
    $battery = $searchModel->search($search);
    $oil = Oil::find()->limit(20)->all();
    return $this->render('index',compact('battery','oil','searchModel'));
  }

  public function actionDetail($params='')
  { 
    if($params != ''){
      if($model = Battery::find()->where('url=:url',[':url'=>$params])->one()){
        return $this->render('detail',compact('model'));        
      }else{
        throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');
      }
    } 
    throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');     
  }
  public function actionFilter($capacity='',$current='',$polarity='',$length='',$width='',$height='',$country='',$assurance='',$voltage=''){
    // $row = 'capacity';
    // $row = 'current';
    // $row = 'polarity';
    // $row = 'length';
    // $row = 'width';
    // $row = 'height';
    // $row = 'country';
    // $row = 'assurance';
    // $row = 'voltage';
    // $file;
    // $m = Battery::find()->select($row)->where(['>','ostatok',0])->andWhere(['>','price',0])->orderBy($row)->groupBy($row)->all();
    // $file .= "['all'=>'Все'";
    // foreach($m as $model){
    //   if($model->$row == '')
    //     continue;
    //   $file .= ",'".$model->generateUrl($model->$row)."'=>'".$model->$row."'";
    // } 
    // $file .= "];";
    // file_put_contents('battery.txt',$file);
    // return;
    $where = [];
    $option['capacity'][] = ['key'=>'all','val'=>'Все'];
    $option['current'][] = ['key'=>'all','val'=>'Все'];
    $option['polarity'][] = ['key'=>'all','val'=>'Все'];
    $option['length'][] = ['key'=>'all','val'=>'Все'];
    $option['width'][] = ['key'=>'all','val'=>'Все'];
    $option['height'][] = ['key'=>'all','val'=>'Все'];
    $option['country'][] = ['key'=>'all','val'=>'Все'];
    $option['assurance'][] = ['key'=>'all','val'=>'Все'];
    $option['voltage'][] = ['key'=>'all','val'=>'Все'];
    $search = new SearchBattery();
    if($capacity != 'all'){$where['capacity'] = $search->_capacity[$capacity];}
    if($current != 'all'){$where['current'] = $search->_current[$current];}
    if($polarity != 'all'){$where['polarity'] = $search->_polarity[$polarity];}
    if($length != 'all'){$where['length'] = $search->_length[$length];}
    if($width != 'all'){$where['width'] = $search->_width[$width];}
    if($height != 'all'){$where['height'] = $search->_height[$height];}
    if($country != 'all'){$where['country'] = $search->_country[$country];}
    if($assurance != 'all'){$where['assurance'] = $search->_assurance[$assurance];}
    if($voltage != 'all'){$where['voltage'] = $search->_voltage[$voltage];}

    if($m = Battery::find()->select('capacity')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('capacity')->all()){
      foreach($m as $model){
        if($model->capacity == '')
          continue;
        $option['capacity'][] = ['key'=>$model->generateUrl($model->capacity),'val'=>$model->capacity];
      }      
    }
    if($m = Battery::find()->select('current')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('current')->all()){
      foreach($m as $model){
        if($model->current == '')
          continue;
        $option['current'][] = ['key'=>$model->generateUrl($model->current),'val'=>$model->current];
      }      
    }
    if($m = Battery::find()->select('polarity')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('polarity')->all()){
      foreach($m as $model){
        if($model->polarity == '')
          continue;
        $option['polarity'][] = ['key'=>$model->generateUrl($model->polarity),'val'=>$model->polarity];
      }      
    }
    if($m = Battery::find()->select('length')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('length')->all()){
      foreach($m as $model){
        if($model->length == '')
          continue;
        $option['length'][] = ['key'=>$model->generateUrl($model->length),'val'=>$model->length];
      }      
    }
    if($m = Battery::find()->select('width')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('width')->all()){
      foreach($m as $model){
        if($model->width == '')
          continue;
        $option['width'][] = ['key'=>$model->generateUrl($model->width),'val'=>$model->width];
      }      
    }
    if($m = Battery::find()->select('height')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('height')->all()){
      foreach($m as $model){
        if($model->height == '')
          continue;
        $option['height'][] = ['key'=>$model->generateUrl($model->height),'val'=>$model->height];
      }      
    }
    if($m = Battery::find()->select('country')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('country')->all()){
      foreach($m as $model){
        if($model->country == '')
          continue;
        $option['country'][] = ['key'=>$model->generateUrl($model->country),'val'=>$model->country];
      }      
    }
    if($m = Battery::find()->select('assurance')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('assurance')->all()){
      foreach($m as $model){
        if($model->assurance == '')
          continue;
        $option['assurance'][] = ['key'=>$model->generateUrl($model->assurance),'val'=>$model->assurance];
      }      
    }
    if($m = Battery::find()->select('voltage')->where($where)
      ->andWhere(['>','ostatok',0])->andWhere(['>','price',0])->groupBy('voltage')->all()){
      foreach($m as $model){
        if($model->voltage == '')
          continue;
        $option['voltage'][] = ['key'=>$model->generateUrl($model->voltage),'val'=>$model->voltage];
      }      
    }

    return json_encode($option,JSON_UNESCAPED_UNICODE);
  }

  private function getParams($params,$search){
    if(preg_match('/sorter[\w\-]+/', $params)){$search['sorter'] = str_replace('sorter-','',$params);}
    if(preg_match('/capacity[\w\-]+/', $params)){$search['capacity'] = str_replace('capacity-','',$params);}
    if(preg_match('/current[\w\-]+/', $params)){$search['current'] = str_replace('current-','',$params);}
    if(preg_match('/polarity[\w\-]+/', $params)){$search['polarity'] = str_replace('polarity-','',$params);}
    if(preg_match('/length[\w\-]+/', $params)){$search['length'] = str_replace('length-','',$params);}
    if(preg_match('/width[\w\-]+/', $params)){$search['width'] = str_replace('width-','',$params);}
    if(preg_match('/height[\w\-]+/', $params)){$search['height'] = str_replace('height-','',$params);}
    if(preg_match('/country[\w\-]+/', $params)){$search['country'] = str_replace('country-','',$params);}
    if(preg_match('/assurance[\w\-]+/', $params)){$search['assurance'] = str_replace('assurance-','',$params);}
    if(preg_match('/voltage[\w\-]+/', $params)){$search['voltage'] = str_replace('voltage-','',$params);}
    if(preg_match('/brend[\w\-]+/', $params)){
      $search['brend'] = str_replace('brend-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    return $search;
  }
}
