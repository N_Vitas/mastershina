<?php
namespace frontend\controllers;

use frontend\components\BaseController;

use common\models\SearchShiny;
use common\models\Battery;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;

/**
 * Shiny controller
 */
class ShinyController extends BaseController
{
  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex($params='')
  {   
    $data = explode(";",$params);
    $search=[];
    if(count($data) > 0)
    for ($i=0; $i <= count($data); $i++) {
      $search = $this->getParams($data[$i],$search,($i+1));
    }  
    // var_dump($search);
    $searchModel = new SearchShiny();
    $shiny = $searchModel->search($search);
    // $shiny->query->where(['>','ostatok',0]);
    $disc = Disc::find()->limit(20)->all();
    return $this->render('index',compact('shiny','disc','searchModel'));
  }

  public function actionDetail($params='')
  { 
    if($params != ''){
      if($model = Shiny::find()->where('url=:url',[':url'=>$params])->one()){
        return $this->render('detail',compact('model'));        
      }else{
        throw new \yii\web\NotFoundHttpException('По вашему запросу ничего не найдено!');
      }
    }      

    $searchModel = new SearchShiny();
    $shiny = $searchModel->search(\Yii::$app->request->get());
    $disc = Disc::find()->limit(20)->all();
    return $this->render('index',compact('shiny','disc','searchModel'));
  }

  private function getParams($params,$search,$attr=0){
    $session = \Yii::$app->session;    
    switch ($params) {
      case 'title': $search['sorter']=$params; $attr=0; break;
      case 'price': $search['sorter']=$params; $attr=0; break;
      case 'letnie': $search['season']=$params; $attr=0; break;
      case 'zimnie': $search['season']=$params; $attr=0; break;
      case 'vsesezonye': $search['season']=$params; $attr=0; break;
      case 'vsesezonnye': $search['season']=$params; $attr=0; break;
      case 'gruzovye': $search['typeTC']/*[$params]*/=$params; $attr=0; break;
      case 'legkovye': $search['typeTC']/*[$params]*/=$params; $attr=0; break;
      case 'microavtobusnye': $search['typeTC']/*[$params]*/=$params; $attr=0; break;
      case 'runflat': $search['runflat']=true; $attr=0; break;
      case 'ship': $search['ship']=1; $attr=0; break; 
      case 'allship': $search['ship']=2; $attr=0; break; 
      case 'podship': $search['ship']=3; $attr=0; break; 
      case 'all': $search['typeTC']/*[$params]*/=$params; $attr=0; break;     
    }   
    // echo count($p)."</br>";
    if(preg_match('/loads[\w\-]+/', $params)) {
      $search['loads'] = str_replace('loads-','',$params);
      $attr=0;
    }
    if(preg_match('/speed[\w\-\,]+/', $params)) {
      $speed = str_replace('speed-','',$params);
      $sd = explode(',',$speed);
      foreach ($sd as $s) {
        $search['speed'][$s] = $s;
      }
      $attr=0;
    }
    if(preg_match('/shirina[\w\-]+/', $params)){
      $search['width'] = str_replace('shirina-','',$params);
      $session['filter-shiny'] = 'params';
    }
    if(preg_match('/vysota[\w\-]+/', $params)){
      $search['height'] = str_replace('vysota-','',$params);
      $session['filter-shiny'] = 'params';
    }
    if(preg_match('/diametr[\w\-]+/', $params)){
      $search['radius'] = str_replace('diametr-','',$params);
      $session['filter-shiny'] = 'params';
    }

    if(preg_match('/make[\w\-]+/', $params)){
      $search['vendor'] = str_replace('make-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    if(preg_match('/car[\w\-]+/', $params)){
      $search['car'] = str_replace('car-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    if(preg_match('/year[\w\-]+/', $params)){
      $search['year'] = str_replace('year-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    if(preg_match('/modification[\w\-]+/', $params)){
      $search['modification'] = str_replace('modification-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    if(preg_match('/brend[\w\-]+/', $params)){
      $search['brend'] = str_replace('brend-','',$params);
      $session['filter-shiny'] = 'auto';
    }
    // $p = explode(',',$params);
    // if(count($p) > 1 && $attr > 0){
    //   foreach ($p as $v) {
    //       $search['typeTC'][$v] = $v;
    //   }
    // }
    return $search;
  }

  public function actionSize($width='',$height='',$radius='',$season='',$loads='',$speed='',$typeTC='',$vendor='',$car='',$year='',$modification='',$runflat=0,$ship=0)
  { 
    // $rrr = \yii\helpers\ArrayHelper::map(\common\models\Vehicle::find()->select(['modification'])->groupBy('modification')->all(),function($model){
    //   $r = new Shiny();
    //   return $r->generateUrl($model->modification);
    // },'modification');
    // foreach ($rrr as $key => $value) {
    //   echo "'".$key."'=>'".$value."',";
    // }

    $option['width'][] = ['key'=>'all','val'=>'Все'];
    $option['height'][] = ['key'=>'all','val'=>'Все'];
    $option['radius'][] = ['key'=>'all','val'=>'Все'];
    $option['season'][] = ['key'=>'all','val'=>'Все'];
    $option['loads'][] = ['key'=>'all','val'=>'Все'];
    $option['speed'][] = ['key'=>'all','val'=>'Все'];
    // $option['vendor'][] = ['key'=>'all','val'=>'Все'];
    // $option['car'][] = ['key'=>'all','val'=>'Все'];
    // $option['year'][] = ['key'=>'all','val'=>'Все'];
    // $option['modification'][] = ['key'=>'all','val'=>'Все'];

    $where = [];
    $auto=[];
    $search = new SearchShiny();
    if($vendor != '' && $vendor != 'all'){$auto['vendor']=$search->_vendor[$vendor];}
    if($car != '' && $car != 'all'){$auto['car']=$search->_car[$car];}
    if($year != '' && $year != 'all'){$auto['year']=$search->_year[$year];}
    if($modification != '' && $modification != 'all'){$auto['modification']=$search->_modification[$modification];}
    foreach ($search->getVendor($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['vendor'][] = ['key'=>$key,'val' => $value];
    }
    foreach ($search->getCar($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['car'][] = ['key'=>$key,'val' => $value];
    }
    foreach ($search->getYear($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['year'][] = ['key'=>$key,'val' => $value];
    }
    foreach ($search->getModification($auto) as $key => $value) {
      if($key == ''){          
        continue;
      }
      $option['modification'][] = ['key'=>$key,'val' => $value];
    }
    
    if($width != '' && $width != 'all'){$where['width']=$search->_width[$width];}
    if($height != '' && $height != 'all'){$where['height']=$search->_height[$height];}
    if($radius != '' && $radius != 'all'){$where['radius']=$search->_radius[$radius];}
    if($season != '' && $season != 'all'){$where['season']=$search->_season[$season];}
    // if($loads != '' && $loads != 'all'){$where['loads']=$search->_loads[$loads];}
    // if($speed != '' && $speed != 'all'){$where['speed']=$search->_speed[$speed];}
    if($runflat != '' && $speed != 'all'){$where['runflat']=1;}
    if($ship != '' && $speed != 'all'){$where['ship']=1;}
    // if($typeTC != '' && $typeTC != 'all'){$where['typeTC']=$search->_typeTC[$typeTC];}
    if($typeTC != '' && $typeTC != 'all'){
      switch ($typeTC) {
        case 'gruzovye':
          $where['typeTC'] = 'грузовые';
          break;
        case 'legkovye':
          $where['typeTC'] = 'легковые/4х4';
          break;
        case 'microavtobusnye':
          $where['typeTC'] = 'микроавтобусные';
          break;
      }
      // $where['typeTC']=$typewhere;
      // var_dump($where);
    }
    if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('width')->all()){
      foreach ($m as $model) {
        if($model->width == ''){
          continue;
        }
        $option['width'][] = ['key'=>$model->generateUrl($model->width),'val' => $model->width];
      }
    }
    if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('height')->all()){
      foreach ($m as $model) {
        if($model->height == ''){          
          continue;
        }
        $option['height'][] = ['key'=>$model->generateUrl($model->height),'val' => $model->height];
      }
    }
    if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('radius')->all()){
      foreach ($m as $model) {
        if($model->radius == ''){
          continue;
        }
        $option['radius'][] = ['key'=>$model->generateUrl($model->radius),'val' => str_replace('R', '', $model->radius)];          
      }
    }
    // if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('season')->all()){
    //   foreach ($m as $model) {
    //     if($model->season == ''){
    //       continue;
    //     }
    //     $option['season'][] = ['key'=>$model->generateUrl($model->season),'val' => $model->season];
    //   }
    // }
    if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('loads')->all()){
      foreach ($m as $model) {
        if($model->loads == ''){
          continue;
        }
        $option['loads'][] = ['key'=>$model->generateUrl($model->loads),'val' => $model->loads];
      }
    }
    // if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('speed')->all()){
    //   foreach ($m as $model) {
    //     if($model->speed == ''){
    //       continue;
    //     }
    //     $option['speed'][] = ['key'=>$model->generateUrl($model->speed),'val' => $model->speed];
    //   }
    // }
    // if($m = Shiny::find()->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('typeTC')->all()){
    //   foreach ($m as $model) {
    //     if($model->typeTC == ''){
    //       continue;
    //     }
    //     $option['typeTC'][] = ['key'=>$model->generateUrl($model->typeTC),'val' => $model->typeTC];
    //   }
    // }
    return json_encode($option,JSON_UNESCAPED_UNICODE);
  }
}
