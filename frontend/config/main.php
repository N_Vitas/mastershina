<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
  'id' => 'app-frontend',
  'name' => 'Mastershina',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'controllerNamespace' => 'frontend\controllers',
  'aliases' => [
      '@bower' => '@vendor/bower-asset',
      '@npm'   => '@vendor/npm-asset',
  ],
  'components' => [
    'request' => [
      'csrfParam' => '_csrf-frontend',
      'enableCsrfValidation' => true,
    ],
    'user' => [
      'identityClass' => 'common\models\User',
      'enableAutoLogin' => true,
      'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
    ],
    'cart' => [
      'class' => 'yz\shoppingcart\ShoppingCart',
      'cartId' => 'mastershina_cart',
    ],
    'session' => [
      // this is the name of the session cookie used for login on the frontend
      'name' => 'advanced-frontend',
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'home/error',
    ],
    
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
        '/' => 'home/index',
        // 'site/login' => 'home/login',
        // 'site/logout' => 'home/logout',
        // 'produkt/<url:[\w\-]+>' => 'produkt/index',
        // 'disc/<url:[\w\-]+>' => 'disc/index',
        // 'oil/<url:[\w\-]+>' => 'oil/index',
        // 'shiny/<action:[\w\-]+>' => 'shiny/index',
        // 'battery/<url:[\w\-]+>' => 'battery/index',
        // 'shiny/<sorter:(title|price)>/<size:[\w\-]+>/<season:[\w\-]+>/<loads:[\w\-]+>/<speed:[\w\-]+>/<typeTC:[\w\-]+>/<page:\d+>/<per-page:\d+>' => 'shiny/index',
        // 'shiny/<sorter:(title|price)>;<width:[\w\-]+>;<height:[\w\-]+>;<radius:[\w\-]+>;<season:[\w\-]+>;<speed:[\w\-]+>;<typeTC:[\w\-]+>' => 'shiny/index',
        // 'shiny/<page:\d+>/<per-page:\d+>' => 'shiny/index',
        // 'disc/<sorter:(title|price)>/<size:[\w\-]+>/<radius:[\w\-]+>/<type:[\w\-]+>/<manufacturer:[\w\-]+>/<page:\d+>/<per-page:\d+>' => 'disc/index',
        // 'disc/<sorter:(title|price)>/<size:[\w\-]+>/<radius:[\w\-]+>/<type:[\w\-]+>/<manufacturer:[\w\-]+>' => 'disc/index',
        // 'disc/<page:\d+>/<per-page:\d+>' => 'disc/index', 
        // 'oil/<sorter:(title|price)>/<size:[\w\-]+>/<viscosity_1:[\w\-]+>/<type_fluid:[\w\-]+>/<type_engine:[\w\-]+>/<page:\d+>/<per-page:\d+>' => 'oil/index',
        // 'oil/<sorter:(title|price)>/<size:[\w\-]+>/<viscosity_1:[\w\-]+>/<type_fluid:[\w\-]+>/<type_engine:[\w\-]+>' => 'oil/index',
        'api/<action>' => 'api/<action>',
        'home/<action>' => 'home/<action>',
        'site/<action>' => 'site/<action>', 
        'basket/<action>' => 'basket/<action>', 
        'service/<action>' => 'service/<action>',  
        'delivery/<action>' => 'delivery/<action>',   
        [
          'pattern' => '<controller>/<page:\d+>/<per-page:\d+>/<params:[\w\-\;\/]+>',
          'route' => '<controller>/index',
        ],
        // [
        //   'pattern' => '<controller>/<page:\d+>/<params:[\w\-\;\/]+>',
        //   'route' => '<controller>/index',
        // ],
        [
          'pattern' => '<controller>/<params:[\w\-\;\,]+>',
          'route' => '<controller>/index',
        ],
        [
          'pattern' => '<controller>/<action>',
          'route' => '<controller>/<action>',
        ],
        [
          'pattern' => '<controller>/<action>/<id:\d+>',
          'route' => '<controller>/<action>',
        ],
        [
          'pattern' => '<controller>/<action>/<params:[\w\-\;]+>',
          'route' => '<controller>/<action>',
        ],
        // 'page/<url:[\w\-]+>' => 'site/page',
        // 'shiny/<url:([\d]+\-[\d]+\-r[\w\-]+)>' => 'catalog/show',
        // 'country/<url:\w+>/R<radius:\w+>' => 'country/index',
        // 'country/<url:\w+>' => 'country/index',
        // 'brand/<url:[\w\-]+>/R<radius:\w+>' => 'brand/index',
        // 'brand/<url:[\w\-]+>' => 'brand/index',
        // 'shiny/<season:(summer|winter|semi)>/<width:[0-9.]+>-<profile:[0-9.,]+>-R<radius:[0-9,Cc]+>' => 'catalog/index',
        // 'shiny/<season:(summer|winter|semi)>' => 'catalog/index',
        // 'season/<url:\w+>/R<radius:\w+>' => 'season/index',
        // 'season/<url:\w+>' => 'season/index',
        // 'p/<name:[\w\-\d]+>' => 'page/show',
      ],
    ],
      
  ],
  'params' => $params,
  'defaultRoute' => 'home',
];
