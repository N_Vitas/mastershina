<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
// use libphonenumber\PhoneNumberUtil;
// use libphonenumber\PhoneNumberFormat;
// use libphonenumber\NumberParseException;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $body;
    public $verifyCode;

    // protected $strict = true;
    // protected $countryAttribute;
    // protected $country;
    // protected $format = true;


    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email','phone', 'subject','body'], 'required','message'=>'Ведите значение {attribute}.'],
            // phone has to be a valid phone number
            // ['phone', 'integer'],
            // email has to be a valid email address
            ['email', 'email','message'=>'{attribute} не соответствует формату.'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha','message'=>'Некорректно введен {attribute}.'],
            // ['verifyCode', 'captcha','captchaAction'=>'home/captcha'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'email' => 'E-mail',
            'subject' => 'Тема сообщения',
            'phone' => 'Контактный телефон',
            'body' => 'Сообщение',
            'verifyCode' => 'Проверочный код',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
}
