<?php

use yii\db\Migration;

class m170313_041744_add_column_order extends Migration
{
    
    public function up()
    {
      $this->addColumn('order', 'status_payment', $this->integer(5)->notNull()->defaultValue(0)->after('status')); 
      $this->addColumn('order', 'status_delivery', $this->integer(5)->notNull()->defaultValue(0)->after('status_payment')); 
      $this->addColumn('order', 'id_crm', $this->string(25)->notNull()->defaultValue('')->after('id')); 
      $this->addColumn('order', 'theme', $this->string(255)->notNull()->defaultValue('')->after('comment'));
      $this->addColumn('order', 'role', $this->string(25)->notNull()->defaultValue('web')->after('data')); 
      $this->addColumn('order', 'author', $this->integer(11)->notNull()->defaultValue(0)->after('role'));
      $this->addColumn('order', 'manager_crm', $this->integer(11)->notNull()->defaultValue(0)->after('author'));
      $this->createIndex('index_status_payment', 'order', ['status_payment']);
      $this->createIndex('index_status_delivery', 'order', ['status_delivery']);
    }
    public function down()
    {
      $this->dropColumn('order', 'status_payment');
      $this->dropColumn('order', 'status_delivery');
      $this->dropColumn('order', 'id_crm');
      $this->dropColumn('order', 'theme');
      $this->dropColumn('order', 'role');
      $this->dropColumn('order', 'author');
      $this->dropColumn('order', 'manager_crm');
    }
}
