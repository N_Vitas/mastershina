<?php

use yii\db\Migration;

class m170705_111653_create_service extends Migration
{
  public function up()
  {
    $this->createTable('service', [      
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'title' => "VARCHAR(255)  NOT NULL",
      'type' => "VARCHAR(255)  NOT NULL",
    ]);

    $this->insert('service',['id'=>1,'title'=>'Снятие и установка колеса','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>2,'title'=>'Демонтаж покрышки','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>3,'title'=>'Монтаж покрышки','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>4,'title'=>'Балансировка','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>5,'title'=>'Комплекс услуг 1-го колеса','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>6,'title'=>'Комплекс услуг 4-х колес','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>7,'title'=>'Готовый комплект (на дисках)','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>8,'title'=>'Сложный монтаж','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>9,'title'=>'Дополнительные услуги','type' =>'Шиномонтаж']);
    $this->insert('service',['id'=>10,'title'=>'Грузики балансировочные','type' =>'Расходные материалы']);
    $this->insert('service',['id'=>11,'title'=>'Вентили','type' =>'Расходные материалы']);
    $this->insert('service',['id'=>12,'title'=>'Пакеты','type' =>'Расходные материалы']);
    $this->insert('service',['id'=>13,'title'=>'Ремонтные работы','type' =>'Ремонт']);
    $this->insert('service',['id'=>14,'title'=>'Дископравные работы','type' =>'Ремонт']);
    $this->insert('service',['id'=>15,'title'=>'Легковые шины','type' =>'Хранение шин']);
    $this->insert('service',['id'=>16,'title'=>'Легковые шины с дисками','type' =>'Хранение шин']);
    $this->insert('service',['id'=>17,'title'=>'Шины для внедорожников, SUV, минивэн','type' =>'Хранение шин']);
    $this->insert('service',['id'=>18,'title'=>'Шины с дисками для внедорожников, SUV, минивэн','type' =>'Хранение шин']);
    $this->insert('service',['id'=>19,'title'=>'Легковые и внедорожные шины весом до 15кг, SUV, минивэн','type' =>'Утилизация']);
    $this->insert('service',['id'=>20,'title'=>'Легковые и внедорожные шины весом от 15кг до 30кг, SUV, минивэн','type' =>'Утилизация']);
    $this->insert('service',['id'=>21,'title'=>'Грузовые и крупногабаритные шины, SUV, минивэн','type' =>'Утилизация']);
    $this->insert('service',['id'=>22,'title'=>'Дополнительные услуги','type' =>'Дополнительные услуги']);
    $this->insert('service',['id'=>23,'title'=>'Годовой абонемент комплект (без дисках)','type' =>'Абонимент']);
    $this->insert('service',['id'=>24,'title'=>'Годовой абонемент комплект (на дисках)','type' =>'Абонимент']);
  }

  public function down()
  {
    $this->dropTable('service');
    return true;
  }
}
