<?php

use yii\db\Migration;

class m170227_111245_create_order extends Migration
{
  public function up()
  {
    $this->createTable('order', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'name' => "VARCHAR(255)  NOT NULL",
      'email' => "VARCHAR(255)  NOT NULL",
      'phone' => "VARCHAR(255) NOT NULL",
      'address' => "VARCHAR(25)  NOT NULL",
      'comment' => "VARCHAR(255)  NOT NULL",
      'delivery' => "VARCHAR(255) NOT NULL",
      'payment' => "VARCHAR(255) NOT NULL",
      'price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'created_at' => "INT(10) UNSIGNED NOT NULL",
      'updated_at' => "INT(10) UNSIGNED NOT NULL",
      'data' => "TEXT NOT NULL",
      'status' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
    ]);
    $this->createIndex('index_name_status', 'order', ['name','status']);

  }

  public function down()
  {
    $this->dropTable('order');
    return true;
  }
}
