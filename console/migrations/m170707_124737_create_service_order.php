<?php

use yii\db\Migration;

class m170707_124737_create_service_order extends Migration
{
  public function up()
  {
    $this->createTable('service_order', [      
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'user_id' => "INT(11)  NOT NULL",
      'title' => "VARCHAR(255)  NOT NULL",
      'params' => "VARCHAR(255)  NOT NULL",
      'type' => "VARCHAR(255)  NOT NULL",
      'created_at' => "INT(10) UNSIGNED NOT NULL",
      'status' => "MEDIUMINT(8)  NOT NULL DEFAULT 0",
    ]);
  }

  public function down()
  {
    $this->dropTable('service_order');
    return true;
  }
}