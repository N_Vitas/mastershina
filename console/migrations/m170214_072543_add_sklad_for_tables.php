<?php

use yii\db\Migration;

class m170214_072543_add_sklad_for_tables extends Migration
{
    public function up()
    {
      $this->addColumn('shiny', 'sklad', $this->string(255)->notNull()->defaultValue('')->after('ostatok')); 
      $this->addColumn('disc', 'sklad', $this->string(255)->notNull()->defaultValue('')->after('ostatok'));
      $this->addColumn('oil', 'sklad', $this->string(255)->notNull()->defaultValue('')->after('ostatok'));
      $this->addColumn('battery', 'sklad', $this->string(255)->notNull()->defaultValue('')->after('ostatok'));
    }
    public function down()
    {
      $this->dropColumn('shiny', 'sklad');
      $this->dropColumn('disc', 'sklad');
      $this->dropColumn('oil', 'sklad');
      $this->dropColumn('battery', 'sklad');
    }
}