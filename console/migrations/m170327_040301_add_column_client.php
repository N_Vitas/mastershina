<?php

use yii\db\Migration;

class m170327_040301_add_column_client extends Migration
{
    public function up()
    {
      $this->addColumn('client', 'iin', $this->integer(12)->notNull()->defaultValue(0));
      $this->addColumn('client', 'export', $this->integer(1)->notNull()->defaultValue(1));
    }
    public function down()
    {
      $this->dropColumn('client', 'iin');
      $this->dropColumn('client', 'export');
    }
}
