<?php

use yii\db\Migration;

class m170316_084934_alter_column_shiny extends Migration
{
    public function up()
    {
        $this->alterColumn('shiny','width',$this->integer(10)->notNull()->defaultValue(0));
        $this->alterColumn('shiny','height',$this->integer(10)->notNull()->defaultValue(0));
        $this->alterColumn('shiny','radius',$this->integer(10)->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->alterColumn('shiny','width',$this->string(255)->notNull()->defaultValue(''));
        $this->alterColumn('shiny','height',$this->string(255)->notNull()->defaultValue(''));
        $this->alterColumn('shiny','radius',$this->string(255)->notNull()->defaultValue(''));
    }
}
