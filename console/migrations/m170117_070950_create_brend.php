<?php

use yii\db\Migration;

class m170117_070950_create_brend extends Migration
{
  public function up(){
    $this->createTable('brend', [
      'id' => "SMALLINT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'title' => "VARCHAR(255) UNIQUE NOT NULL",
      'url' => "VARCHAR(255)  NOT NULL",
      'text' => "TEXT  NULL",
      'meta_title' => "VARCHAR(255)  NULL",
      'meta_keywords' => "VARCHAR(255)  NULL",
      'meta_description' => "VARCHAR(255)  NULL",
      'status' => "TINYINT(1)  UNSIGNED NULL",
    ]);
    $this->createIndex('index_url_status', 'brend', ['url', 'status']);
  }
  public function down(){
    $this->dropTable('brend');
    return true;
  }
}
