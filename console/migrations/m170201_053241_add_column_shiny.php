<?php

use yii\db\Migration;

class m170201_053241_add_column_shiny extends Migration
{
    public function up()
    {
        $this->addColumn('shiny', 'width', $this->string()->notNull()->defaultValue('')->after('size'));
        $this->addColumn('shiny', 'height', $this->string()->notNull()->defaultValue('')->after('width'));
        $this->addColumn('shiny', 'radius', $this->string()->notNull()->defaultValue('')->after('height'));
    }

    public function down()
    {
        $this->dropColumn('shiny', 'width');
        $this->dropColumn('shiny', 'height');
        $this->dropColumn('shiny', 'radius');
    }
}
