<?php

use yii\db\Migration;

class m170326_110832_add_column_order extends Migration
{
    public function up()
    {
      $this->addColumn('order', 'export', $this->integer(1)->notNull()->defaultValue(1));
    }
    public function down()
    {
      $this->dropColumn('order', 'export');
    }
}
