<?php

use yii\db\Migration;

class m170117_080140_create_oil extends Migration
{
  public function up()
  {
    /*
    Вязкость_1
    Вязкость_2
    ТипЖидкости
    Объем
    ТипДвигателя
    Название
    */
    $this->createTable('oil', [      
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'parent_brend_id' => "SMALLINT(6) UNSIGNED NOT NULL",
      'title' => "VARCHAR(255)  NOT NULL",
      'price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'old_price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'currency' => "VARCHAR(10)  NOT NULL DEFAULT 'KZT'",
      'model' => "TEXT NULL",
      'delivery' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 1",
      'ostatok' => "VARCHAR(25)  NOT NULL",
      'picture' => "VARCHAR(255)  NOT NULL",
      'viscosity_1' => "VARCHAR(100)  NOT NULL",
      'viscosity_2' => "VARCHAR(100)  NOT NULL",
      'type_fluid' => "VARCHAR(255)  NOT NULL",
      'type_engine' => "VARCHAR(255)  NOT NULL",        
      'size' => "VARCHAR(255)  NOT NULL",
      'status' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
      'code' => "VARCHAR(25)  NOT NULL",
      'url' => "VARCHAR(255)  NOT NULL",
      'meta_title' => "VARCHAR(255)  NULL",
      'meta_keywords' => "VARCHAR(255)  NULL",
      'meta_description' => "VARCHAR(255)  NULL",
    ]);
    $this->createIndex('index_parent_brend_id', 'oil', ['parent_brend_id']);
    $this->createIndex('index_code', 'oil', ['code']);

  }

  public function down()
  {
    $this->dropTable('oil');
    return true;
  }
}
