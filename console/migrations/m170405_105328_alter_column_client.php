<?php

use yii\db\Migration;

class m170405_105328_alter_column_client extends Migration
{
    public function up()
    {
        $this->alterColumn('client','phone','TEXT NOT NULL');
    }

    public function down()
    {
        $this->alterColumn('client','phone',$this->string(255)->notNull()->defaultValue(''));
    }
}
