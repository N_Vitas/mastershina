<?php

use yii\db\Migration;

class m170326_143137_add_column_client extends Migration
{
    public function up()
    {
      $this->addColumn('client', 'bin', $this->integer(12)->notNull()->defaultValue(0));
      $this->addColumn('client', 'type', $this->string(255)->notNull()->defaultValue(''));
    }
    public function down()
    {
      $this->dropColumn('client', 'bin');
      $this->dropColumn('client', 'type');
    }
}