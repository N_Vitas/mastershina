<?php

use yii\db\Migration;

class m170314_181215_alter_order_column extends Migration
{
    public function up()
    {
        $this->alterColumn('order','comment','TEXT NOT NULL');
    }

    public function down()
    {
        $this->alterColumn('order','comment',$this->string(255)->notNull()->defaultValue(''));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
