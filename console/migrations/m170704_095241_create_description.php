<?php

use yii\db\Migration;

class m170704_095241_create_description extends Migration
{
  public function up()
  {
    $this->createTable('description', [      
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'code' => "VARCHAR(255)  NOT NULL",
      'text' => "TEXT NULL",
    ]);
    $this->createIndex('index_code', 'description', ['code']);
  }

  public function down()
  {
    $this->dropTable('description');
    return true;
  }
}
