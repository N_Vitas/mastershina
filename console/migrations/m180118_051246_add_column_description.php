<?php

use yii\db\Migration;

class m180118_051246_add_column_description extends Migration
{    
    public function up()
    {
      $this->addColumn('oil', 'description', 'TEXT  NOT NULL DEFAULT ""');
      $this->addColumn('shiny', 'description', 'TEXT  NOT NULL DEFAULT ""');
      $this->addColumn('battery', 'description', 'TEXT  NOT NULL DEFAULT ""');
      $this->addColumn('disc', 'description', 'TEXT  NOT NULL DEFAULT ""');
    }
    public function down()
    {
      $this->dropColumn('oil', 'description');
      $this->dropColumn('shiny', 'description');
      $this->dropColumn('battery', 'description');
      $this->dropColumn('disc', 'description');
    }
}
