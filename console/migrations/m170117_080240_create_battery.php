<?php

use yii\db\Migration;

class m170117_080240_create_battery extends Migration
{
  public function up()
  {
    /*
    Артикул
    Ёмкость
    СтартовыйТок
    Полярность
    Бренд
    Название
    Длина
    Ширина
    Высота
    Страна
    Гарантия
    Напряжение
    */
    $this->createTable('battery', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'parent_brend_id' => "SMALLINT(6) UNSIGNED NOT NULL",
      'title' => "VARCHAR(255)  NOT NULL",
      'price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'old_price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'currency' => "VARCHAR(10)  NOT NULL DEFAULT 'KZT'",
      'picture' => "VARCHAR(255)  NOT NULL",
      'model' => "TEXT NULL",
      'delivery' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 1",
      'ostatok' => "VARCHAR(25)  NOT NULL",
      'article' => "VARCHAR(25)  NOT NULL",
      'capacity' => "VARCHAR(25)  NOT NULL",
      'current' => "VARCHAR(25)  NOT NULL",
      'polarity' => "VARCHAR(25)  NOT NULL",  
      'length' => "TINYINT(5) UNSIGNED NOT NULL",
      'width' => "TINYINT(5) UNSIGNED NOT NULL",
      'height' => "TINYINT(5) UNSIGNED NOT NULL",
      'country' => "VARCHAR(255)  NOT NULL", 
      'assurance' => "VARCHAR(255) NOT NULL",
      'voltage' => "TINYINT(4) UNSIGNED NOT NULL",
      'status' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
      'code' => "VARCHAR(25)  NOT NULL",
      'url' => "VARCHAR(255)  NOT NULL",
      'meta_title' => "VARCHAR(255)  NULL",
      'meta_keywords' => "VARCHAR(255)  NULL",
      'meta_description' => "VARCHAR(255)  NULL",
    ]);
    $this->createIndex('index_parent_brend_id', 'battery', ['parent_brend_id']);
    $this->createIndex('index_code', 'battery', ['code']);

  }

  public function down()
  {
    $this->dropTable('battery');
    return true;
  }
}
