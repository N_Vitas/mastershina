<?php

use yii\db\Migration;

class m170327_031055_order_history extends Migration
{
  public function up()
  {
    $this->createTable('order_history', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'order_id' => "MEDIUMINT(8)  NOT NULL",
      'type' => "VARCHAR(255)  NOT NULL",
      'data_old' => "TEXT  NOT NULL",
      'data_new' => "TEXT NOT NULL",
      'created_at' => "INT(10) UNSIGNED NOT NULL",
      'updated_at' => "INT(10) UNSIGNED NOT NULL",
    ]);
    $this->createIndex('index_order_id', 'order_history', ['order_id']);
    $this->createIndex('index_type', 'order_history', ['type']);

  }

  public function down()
  {
    $this->dropTable('order_history');
    return true;
  }
}
