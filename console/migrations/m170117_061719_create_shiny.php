<?php

use yii\db\Migration;

class m170117_061719_create_shiny extends Migration
{
  public function up()
  {
    /*
    СтараяЦена
    Цена
    currencyId
    model
    delivery
    Ostatatok
    */
    $this->createTable('shiny', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'parent_brend_id' => "SMALLINT(6) UNSIGNED NOT NULL",
      'parent_auto_id' => "SMALLINT(6) UNSIGNED NOT NULL",
      'title' => "VARCHAR(255)  NOT NULL",
      'price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'old_price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'currency' => "VARCHAR(10)  NOT NULL DEFAULT 'KZT'",
      'model' => "TEXT NULL",
      'delivery' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 1",
      'ostatok' => "VARCHAR(25)  NOT NULL",
      'size' => "VARCHAR(25)  NOT NULL",
      'season' => "VARCHAR(25)  NOT NULL",
      'picture' => "VARCHAR(255)  NOT NULL",
      'loads' => "VARCHAR(255)  NOT NULL",
      'speed' => "VARCHAR(255)  NOT NULL",
      'typeTC' => "VARCHAR(255)  NOT NULL",        
      'runflat' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
      'ship' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
      'status' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
      'code' => "VARCHAR(25)  NOT NULL",
      'url' => "VARCHAR(255)  NOT NULL",
      'meta_title' => "VARCHAR(255)  NULL",
      'meta_keywords' => "VARCHAR(255)  NULL",
      'meta_description' => "VARCHAR(255)  NULL",
    ]);
    $this->createIndex('index_parent_brend_id', 'shiny', ['parent_brend_id']);
    $this->createIndex('index_parent_auto_id', 'shiny', ['parent_auto_id']);
    $this->createIndex('index_code', 'shiny', ['code']);

  }

  public function down()
  {
    $this->dropTable('shiny');
    return true;
  }
}
