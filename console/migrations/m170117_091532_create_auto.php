<?php

use yii\db\Migration;

class m170117_091532_create_auto extends Migration
{
  public function up(){
    $this->createTable('auto', [
      'id' => "SMALLINT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'title' => "VARCHAR(255) UNIQUE NOT NULL",
      'url' => "VARCHAR(255)  NOT NULL",
      'make' => "VARCHAR(50)  NOT NULL",
      'model' => "VARCHAR(50)  NOT NULL",
      'modification' => "VARCHAR(50)  NOT NULL",
      'year' => "INT(4) NOT NULL",
      'meta_title' => "VARCHAR(255)  NULL",
      'meta_keywords' => "VARCHAR(255)  NULL",
      'meta_description' => "VARCHAR(255)  NULL",
      'status' => "TINYINT(1)  UNSIGNED NULL",
    ]);
    $this->createIndex('index_url_status', 'auto', ['url', 'status']);
    $this->createIndex('index_make', 'auto', ['make']);
    $this->createIndex('index_model', 'auto', ['model']);
    $this->createIndex('index_modification', 'auto', ['modification']);
    $this->createIndex('index_year', 'auto', ['year']);
  }
  public function down(){
    $this->dropTable('auto');
    return true;
  }
}
