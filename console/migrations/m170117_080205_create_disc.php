<?php

use yii\db\Migration;

class m170117_080205_create_disc extends Migration
{
  public function up()
  {
    /*
    Размер
    Сверловка
    ДиаметрСтупицы
    Вылет
    Цвет
    Тип
    Производитель
    Название
    */
    $this->createTable('disc', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'parent_brend_id' => "SMALLINT(6) UNSIGNED NOT NULL",
      'parent_auto_id' => "SMALLINT(6) UNSIGNED NOT NULL",
      'title' => "VARCHAR(255)  NOT NULL",
      'price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'old_price' => "MEDIUMINT(8) NOT NULL DEFAULT 0",
      'currency' => "VARCHAR(10)  NOT NULL DEFAULT 'KZT'",
      'picture' => "VARCHAR(255)  NOT NULL",
      'model' => "TEXT NULL",
      'delivery' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 1",
      'ostatok' => "VARCHAR(25)  NOT NULL",
      'size' => "VARCHAR(25)  NOT NULL",
      'borer' => "VARCHAR(25)  NOT NULL",
      'stupica' => "FLOAT NOT NULL",
      'radius' => "TINYINT(5) UNSIGNED NOT NULL",
      'color' => "VARCHAR(255)  NOT NULL",  
      'type' => "VARCHAR(255)  NOT NULL", 
      'manufacturer' => "VARCHAR(255) NOT NULL",
      'status' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
      'code' => "VARCHAR(25)  NOT NULL",
      'url' => "VARCHAR(255)  NOT NULL",
      'meta_title' => "VARCHAR(255)  NULL",
      'meta_keywords' => "VARCHAR(255)  NULL",
      'meta_description' => "VARCHAR(255)  NULL",
    ]);
    $this->createIndex('index_parent_brend_id', 'disc', ['parent_brend_id']);
    $this->createIndex('index_parent_auto_id', 'disc', ['parent_auto_id']);
    $this->createIndex('index_code', 'disc', ['code']);
  }

  public function down()
  {
    $this->dropTable('disc');
    return true;
  }
  
}
