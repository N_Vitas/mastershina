<?php

use yii\db\Migration;

class m170209_081041_add_column_auto extends Migration
{
    public function up()
    {
      $this->addColumn('auto', 'shiny_id', 'MEDIUMINT(8)  NOT NULL'); 
      $this->addColumn('auto', 'disk_id', 'MEDIUMINT(8)  NOT NULL');
      $this->addColumn('auto', 'auto_id', 'INT(10)  NOT NULL');

      $this->createIndex('index_auto_id_shiny_id', 'auto', ['auto_id', 'shiny_id']);
      $this->createIndex('index_auto_id_disk_id', 'auto', ['auto_id', 'disk_id']);
    }
    public function down()
    {
      $this->dropColumn('auto', 'shiny_id');
      $this->dropColumn('auto', 'disk_id');
    }

}