<?php

use yii\db\Migration;

class m170705_113459_create_service_detail extends Migration
{
  public function up()
  {
    $this->createTable('service_detail', [      
      'id' => "MEDIUMINT(8) NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'service_id' => "MEDIUMINT(8) NOT NULL",
      'title' => "VARCHAR(255)",
      'type' => "VARCHAR(255)",
      'params' => "VARCHAR(255)",
      'unit' => "VARCHAR(255)",
      'price' => "INT(20)",
    ]);
    $this->createIndex('index_service_id', 'service_detail', ['service_id']);

    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>300]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>300]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>300]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>300]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>350]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>350]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>400]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>400]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>500]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>600]);

    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>250]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>250]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>250]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>300]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>350]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>400]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>500]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>600]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>750]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>1100]);

    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>250]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>250]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>250]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>300]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>350]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>400]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>500]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>600]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>750]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>1100]);

    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>400]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>400]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>400]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>500]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>550]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>650]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>700]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>750]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>800]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>900]);

    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>1200]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>1200]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>1200]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>1400]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>1600]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>1800]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>2100]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>2350]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>2800]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>3700]);

    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>4800]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>4800]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>4800]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>5600]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>6400]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>7200]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>8400]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>9400]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>11200]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>14800]);

    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>2800]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>2800]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>2800]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>3200]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>3600]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>4000]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>4400]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>4600]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>5200]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>6000]);

    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>400]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>400]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>450]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>550]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>650]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>750]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>800]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>900]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>1000]);
    $this->insert('service_detail',['service_id'=>1,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>1000]);

    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>300]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>300]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>350]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>350]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>450]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>550]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>650]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>750]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>1100]);
    $this->insert('service_detail',['service_id'=>2,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>1250]);

    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>300]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>300]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>350]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>350]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>450]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>550]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>650]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>750]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>1100]);
    $this->insert('service_detail',['service_id'=>3,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>1250]);

    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>550]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>600]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>650]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>750]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>800]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>850]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>950]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>950]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>1000]);
    $this->insert('service_detail',['service_id'=>4,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>1100]);

    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>1550]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>1600]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>1800]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>2000]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>2350]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>2700]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>3050]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>3350]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>4200]);
    $this->insert('service_detail',['service_id'=>5,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>4600]);

    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>6200]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>6400]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>7200]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>8000]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>9400]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>10800]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>12200]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>13400]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>16800]);
    $this->insert('service_detail',['service_id'=>6,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>18400]);

    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>3800]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>4000]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>4400]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>5200]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>5800]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>6400]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>7000]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>7400]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>8000]);
    $this->insert('service_detail',['service_id'=>7,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>8400]);

    $this->insert('service_detail',['service_id'=>10,'title'=>'Набивные для стандартных дисков',
      'unit'=>'шт','price'=>100]);
    $this->insert('service_detail',['service_id'=>10,'title'=>'Набивные для литых дисков',
      'unit'=>'шт','price'=>100]);
    $this->insert('service_detail',['service_id'=>10,'title'=>'Самоклеющиеся',
      'unit'=>'шт','price'=>150]);

    $this->insert('service_detail',['service_id'=>11,'title'=>'Установка вентиля',
      'unit'=>'шт','price'=>300]);
    $this->insert('service_detail',['service_id'=>11,'title'=>'Установка вентиля (сборный)',
      'unit'=>'шт','price'=>500]);

    $this->insert('service_detail',['service_id'=>12,'title'=>'Упаковка колес в пакеты',
      'unit'=>'шт','price'=>150]);

    $this->insert('service_detail',['service_id'=>13,'title'=>'Ремонт прокола (жгут)',
      'unit'=>'колесо','params'=>'=','price'=>500]);
    $this->insert('service_detail',['service_id'=>13,'title'=>'Ремонт прокола (латка)',
      'unit'=>'колесо','params'=>'>=','price'=>1000]);
    $this->insert('service_detail',['service_id'=>13,'title'=>'Ремонт бокового пореза',
      'unit'=>'колесо','params'=>'>=','price'=>2000]);
    $this->insert('service_detail',['service_id'=>13,'title'=>'Косметический ремонт шины',
      'unit'=>'колесо','params'=>'>=','price'=>1000]);

    $this->insert('service_detail',['service_id'=>14,'title'=>'Правка железных дисков',
      'unit'=>'точка','params'=>'>=','price'=>1500]);
    $this->insert('service_detail',['service_id'=>14,'title'=>'Правка легкосплавных дисков',
      'unit'=>'точка','params'=>'>=','price'=>3000]);
    $this->insert('service_detail',['service_id'=>14,'title'=>'Косметический ремонт дисков (покраска Акрил)',
      'params'=>'>=','price'=>5000]);
    $this->insert('service_detail',['service_id'=>14,'title'=>'Аргонная сварка, за 1 см',
      'unit'=>'см','params'=>'>=','price'=>1000]);

    $this->insert('service_detail',['service_id'=>15,'unit'=>'колесо','title'=>'За 1 колесо','params'=>'1','price'=>250]);
    $this->insert('service_detail',['service_id'=>16,'unit'=>'колесо','title'=>'За 1 колесо','params'=>'1','price'=>300]);
    $this->insert('service_detail',['service_id'=>17,'unit'=>'колесо','title'=>'За 1 колесо','params'=>'1','price'=>400]);
    $this->insert('service_detail',['service_id'=>18,'unit'=>'колесо','title'=>'За 1 колесо','params'=>'1','price'=>450]);
    $this->insert('service_detail',['service_id'=>15,'unit'=>'колесо','title'=>'За 4 колеса','params'=>'4','price'=>1000]);
    $this->insert('service_detail',['service_id'=>16,'unit'=>'колесо','title'=>'За 4 колеса','params'=>'4','price'=>1200]);
    $this->insert('service_detail',['service_id'=>17,'unit'=>'колесо','title'=>'За 4 колеса','params'=>'4','price'=>1600]);
    $this->insert('service_detail',['service_id'=>18,'unit'=>'колесо','title'=>'За 4 колеса','params'=>'4','price'=>1800]);

    $this->insert('service_detail',['service_id'=>19,'unit'=>'шт','price'=>200]);
    $this->insert('service_detail',['service_id'=>20,'unit'=>'шт','price'=>400]);
    $this->insert('service_detail',['service_id'=>21,'unit'=>'кг','price'=>30]);

    $this->insert('service_detail',['service_id'=>22,'params'=>'>=',
      'unit'=>'колесо','price'=>5000,'title'=>'Оптимизация балансировки (4колеса)']);
    $this->insert('service_detail',['service_id'=>22,'params'=>'=',
      'unit'=>'машина','price'=>100,'title'=>'Проверка и корректировка давления']);
    $this->insert('service_detail',['service_id'=>22,'params'=>'=',
      'unit'=>'колесо','price'=>300,'title'=>'Герметизация борта безкамерной покрышки']);
    $this->insert('service_detail',['service_id'=>22,'params'=>'=',
      'unit'=>'колесо','price'=>200,'title'=>'Смазка резьбы (1 колесо)']);
    $this->insert('service_detail',['service_id'=>22,'params'=>'>=',
      'unit'=>'колесо','price'=>1000,'title'=>'Чистка диска  от 1000 тг']);
    $this->insert('service_detail',['service_id'=>22,'params'=>'=',
      'unit'=>'колесо','price'=>300,'title'=>'Монтаж автокамеры 300 тг']);
    $this->insert('service_detail',['service_id'=>22,'params'=>'plus',
      'unit'=>'ед','price'=>500,'title'=>'Установка датчиков давления***']);

    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>19200]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>19200]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>19200]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>20480]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>21760]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>23040]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>24960]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>26560]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>29440]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>35200]);

    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>26240]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>26560]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>27840]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>29120]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>31360]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>33600]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>35840]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>37760]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>43200]);
    $this->insert('service_detail',['service_id'=>23,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>45760]);    

    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'70','price'=>17920]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'65','price'=>17920]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'60','price'=>17920]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'55','price'=>18560]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'50','price'=>19200]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'45','price'=>19840]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'40','price'=>20480]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'35','price'=>20800]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'30','price'=>21760]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'Легковые автомобили',
      'unit'=>'колесо','type'=>'Высота профиля шины','params'=>'25','price'=>23040]);

    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'15','price'=>25280]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'16','price'=>25600]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'17','price'=>26240]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'18','price'=>27520]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'19','price'=>28480]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'20','price'=>29440]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'21','price'=>30400]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'22','price'=>31040]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'24','price'=>32000]);
    $this->insert('service_detail',['service_id'=>24,'title'=>'4х4, Джип, Минивэн',
      'unit'=>'колесо','type'=>'Радиус диска','params'=>'26','price'=>32640]);
  }

  public function down()
  {
    $this->dropTable('service_detail');
    return true;
  }
}
