<?php

use yii\db\Migration;

class m170410_161947_alter_column_type extends Migration
{
    
    public function up()
    {
        $this->alterColumn('shiny','width','FLOAT(10) NOT NULL DEFAULT 0');
        $this->alterColumn('shiny','height','FLOAT(10) NOT NULL DEFAULT 0');
        $this->alterColumn('shiny','radius','FLOAT(10) NOT NULL DEFAULT 0');
        $this->alterColumn('disc','radius','VARCHAR(25) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->alterColumn('shiny','width','INT(10) NOT NULL DEFAULT 0');
        $this->alterColumn('shiny','height','INT(10) NOT NULL DEFAULT 0');
        $this->alterColumn('shiny','radius','INT(10) NOT NULL DEFAULT 0');
        $this->alterColumn('disc','radius','VARCHAR(25) NOT NULL DEFAULT 0');
    }
}