<?php

use yii\db\Migration;

class m170525_081738_add_column_client extends Migration
{
    public function up()
    {
      $this->addColumn('client', 'client_id', $this->string(25)->defaultValue(0));
      $this->addColumn('client', 'tiresinstorage', $this->string()->defaultValue('нет'));
      $this->addColumn('client', 'contacts', $this->string()->defaultValue(''));
      $this->addColumn('client', 'tc', $this->string()->defaultValue(''));
    }
    public function down()
    {
      $this->dropColumn('client', 'client_id');
      $this->dropColumn('client', 'tiresinstorage');
      $this->dropColumn('client', 'contacts');
      $this->dropColumn('client', 'tc');
    }
}
