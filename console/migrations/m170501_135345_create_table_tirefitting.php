<?php

use yii\db\Migration;

class m170501_135345_create_table_tirefitting extends Migration
{
  public function up()
  {
    $this->createTable('tirefitting', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      "time" => "TIMESTAMP NOT NULL",
      "data" => "TEXT NOT NULL",
    ]);
  }

  public function down()
  {
    $this->dropTable('tirefitting');
    return true;
  }
}
