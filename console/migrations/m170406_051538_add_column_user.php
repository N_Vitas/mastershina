<?php

use yii\db\Migration;

class m170406_051538_add_column_user extends Migration
{
    
    public function up()
    {
      $this->addColumn('user', 'id_crm', $this->string(255)->notNull()->defaultValue(''));
      $this->addColumn('user', 'firstname', $this->string(255)->notNull()->defaultValue(''));
      $this->addColumn('user', 'lastname', $this->string(255)->notNull()->defaultValue(''));
      $this->addColumn('user', 'secondname', $this->string(255)->notNull()->defaultValue(''));
    }
    public function down()
    {
      $this->dropColumn('user', 'id_crm');
      $this->dropColumn('user', 'firstname');
      $this->dropColumn('user', 'lastname');
      $this->dropColumn('user', 'secondname');
    }
}
