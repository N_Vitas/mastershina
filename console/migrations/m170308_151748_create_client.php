<?php

use yii\db\Migration;

class m170308_151748_create_client extends Migration
{
    public function up()
  {
    $this->createTable('client', [
      'id' => "MEDIUMINT(8)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
      'name' => "VARCHAR(255)  NOT NULL",
      'email' => "VARCHAR(255)  NOT NULL",
      'phone' => "VARCHAR(255) NOT NULL",
      'address' => "VARCHAR(25)  NOT NULL",
      'comment' => "VARCHAR(255)  NOT NULL",
      'manager' => "VARCHAR(255) NOT NULL",
      'created_at' => "INT(10) UNSIGNED NOT NULL",
      'updated_at' => "INT(10) UNSIGNED NOT NULL",
      'data' => "TEXT NOT NULL",
      'status' => "TINYINT(1) UNSIGNED NOT NULL DEFAULT 0",
    ]);
    $this->createIndex('index_name_status', 'client', ['name','status']);

  }

  public function down()
  {
    $this->dropTable('client');
    return true;
  }
}
