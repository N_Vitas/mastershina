<?php

namespace console\controllers;

use yii;
// use webtoucher\amqp\controllers\AmqpConsoleController;
use yii\base\ErrorException;
use yii\console\Controller;
use common\models\Order;
use common\models\User;
use \devmustafa\amqp\PhpAmqpLib\Connection\AMQPConnection;
/**
* Импорт данных из 1С.
*/


function shutdown($ch, $conn) {
  $ch->close();
  $conn->close();
}

class ListenController extends Controller
{  
  public function actionReset(){
    set_time_limit(0);
    date_default_timezone_set('Asia/Almaty');
    error_reporting(E_ALL);

    if(yii::$app->params["production"]){
      $exchange = 'exchange';
      $queue = 'queue';      
    }else{      
      $exchange = 'test-exchange';
      $queue = 'test-queue';
    }
    $consumer_tag = 'consumer_1';

    $ignCinf = true;

    $conn = new AMQPConnection(yii::$app->amqp->host, yii::$app->amqp->port, yii::$app->amqp->user, yii::$app->amqp->password,yii::$app->amqp->vhost);
    $ch = $conn->channel();
    $ch->exchange_declare($exchange, 'direct', false, true, false);
    $ch->queue_bind($queue, $exchange);
    $ch->basic_consume($queue, $consumer_tag, false, $ignCinf, false, false, function($msg){
      $data = unserialize($msg->body);
      var_dump($data);
    });


    register_shutdown_function('console\controllers\shutdown', $ch, $conn);

    // Loop as long as the channel has callbacks registered
    while (count($ch->callbacks)) {
      $ch->wait();
    }
  }  
  /**
  * Перебирает файл импорта и записывает в базу.
  */
  public function actionRun(){
    set_time_limit(0);
    date_default_timezone_set('Asia/Almaty');
    error_reporting(E_ALL);
    
    if(yii::$app->params["production"]){
      $exchange = 'exchange';
      $queue = 'queue';      
    }else{      
      $exchange = 'test-exchange';
      $queue = 'test-queue';
    }
    $consumer_tag = 'consumer_1';

    $ignCinf = false;

    $conn = new AMQPConnection(yii::$app->amqp->host, yii::$app->amqp->port, yii::$app->amqp->user, yii::$app->amqp->password,yii::$app->amqp->vhost);
    $ch = $conn->channel();
    $ch->exchange_declare($exchange, 'direct', false, true, false);
    $ch->queue_bind($queue, $exchange);
    $ch->basic_consume($queue, $consumer_tag, false, $ignCinf, false, false, function($msg){
      $data = unserialize($msg->body);
      $response = $this->sendCrm($data);
      while ($response == false) {
        sleep(2);
        echo "Повторная отправка ".json_encode($data,JSON_UNESCAPED_UNICODE)." ".date("d-m-Y H:i:s");
        $response = $this->sendCrm($data);
      }
      if($order = Order::findOne($response['id'])){
        $model = $this->createModel($response);
        foreach ($model as $key => $value) {
          $order->$key = $value;
        }
        if(!$order->save()){
          var_dump($order->errors);
        }
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        if($data["payment"] == "В кредит"){
          Yii::$app->mailer
            ->compose(
                ['html' => 'credit-html'],
                ['model' => $data]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($data["email"])
            ->setSubject("Чек-заявка для оформления кредита №".$data["id"])
            ->send();
          echo "Письмо отправлено на ".$data["email"];
        }
      }else{
        echo "Model ".$response['id']." Not found!".PHP_EOL;
      }
    });


    register_shutdown_function('console\controllers\shutdown', $ch, $conn);

    // Loop as long as the channel has callbacks registered
    while (count($ch->callbacks)) {
      $ch->wait();
    }
  }  
  private function sendCrm($data){
    $ch = curl_init(yii::$app->params["crm_url"]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: application/json;charset=utf-8',
        'Content-Type: application/json;charset=utf-8',
        'Connection: Keep-Alive'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$data],JSON_UNESCAPED_UNICODE));
    // $res = str_replace($UTF8_BOM,"",trim(curl_exec($ch)));
    $res = trim(curl_exec($ch));
    var_dump($res);
    curl_close($ch);
    $response = json_decode($res,true);
    if(is_array($response) && count($response) > 0){
      return $response;
    }
    return false;
  }

  private function createModel($data){
    if(isset($data['data'])){$data['data'] = json_encode($data['data'],JSON_UNESCAPED_UNICODE);}
    if(isset($data['comment'])){$data['comment'] = json_encode($data['comment'],JSON_UNESCAPED_UNICODE);}
    if(isset($data['status'])){
      if(is_int($data['status'])){
        $data['status'] = $data['status'];
      }else{
        switch($data['status']){
          case Order::$statuses['order'][Order::ORDER_DECORATED]:
            $data['status'] = Order::ORDER_DECORATED;
            break;
          case Order::$statuses['order'][Order::ORDER_COMPLETED]:
            $data['status'] = Order::ORDER_COMPLETED;
            break;
          case Order::$statuses['order'][Order::ORDER_PROCESSED]:
            $data['status'] = Order::ORDER_PROCESSED;
            break;
          case Order::$statuses['order'][Order::ORDER_PAYMENTS]:
            $data['status'] = Order::ORDER_PAYMENTS;
            break;
          case Order::$statuses['order'][Order::ORDER_RESERVE]:
            $data['status'] = Order::ORDER_RESERVE;
            break;
          case Order::$statuses['order'][Order::ORDER]:
            $data['status'] = Order::ORDER;
            break;
          case Order::$statuses['order'][Order::ORDER_ENTERED]:
            $data['status'] = Order::ORDER_ENTERED;
            break;
          case Order::$statuses['order'][Order::ORDER_DELIVERY]:
            $data['status'] = Order::ORDER_DELIVERY;
            break;
          case Order::$statuses['order'][Order::ORDER_SALES]:
            $data['status'] = Order::ORDER_SALES;
            break;
          case Order::$statuses['order'][Order::ORDER_OFFERT]:
            $data['status'] = Order::ORDER_OFFERT;
            break;
          case Order::$statuses['order'][Order::ORDER_CANCELED]:
            $data['status'] = Order::ORDER_CANCELED;
            break;
          default:
            $data['status'] = Order::ORDER_CREATE;
            break;
        }
      }
    }
    if(isset($data['author'])){
      if(!is_int($data['author'])){
        if($author = User::find()->where(['id_crm' => $data['author']])->one()){
          $data['author'] = $author->id;      
        }
      }
    }
    if(isset($data['manager_crm'])){    
      if(!is_int($data['manager_crm'])){
        if($manager_crm = User::find()->where(['id_crm' => $data['manager_crm']])->one()){
          $data['manager_crm'] = $manager_crm->id;      
        }
      }
    }
    return $data;
  }
}