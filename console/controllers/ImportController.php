<?php

namespace console\controllers;

use yii;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Battery;
use common\models\Brend;
use common\models\Disc;
use common\models\Oil;
use common\models\Shiny;
/**
* Импорт данных из 1С.
*/
class ImportController extends Controller
{  
  /**
  * Перебирает файл импорта и записывает в базу.
  */
  public function actionIndex()
  {
    try{
      date_default_timezone_set('Asia/Almaty');
      if(file_exists(Yii::getAlias('@ftp')."/importnew.xml")){
      // $this->moveImpportFile();return;
      echo "Начало разбора файла ".PHP_EOL;
      $objPHPExcel = simplexml_load_file(Yii::getAlias('@ftp')."/importnew.xml");        
      $oil = [];
      $shiny = [];
      $disc = [];
      $battery = [];
      $create = 0;
      $update = 0;
      $error = 0;

      $ci = 0;
      $co = count($objPHPExcel->offers);
      Console::startProgress($ci, $co);
      foreach ($objPHPExcel->offers as $keys => $objs) {
          foreach($objs as $obj){
            if((int) $obj->Цена == 0 || (int) $obj->СтараяЦена == 0){
              continue;
            }
            $tmp = [];$table=false;
            $tmp['code'] = (string) $obj['id'];
            $tmp['old_price'] = (int) $obj->СтараяЦена;
            $tmp['price'] = (int) $obj->Цена;
            $tmp['currencyId'] = (string) $obj->currencyId ;
            $tmp['picture'] = (string) $obj->picture;
            $tmp['delivery'] = (int) $obj->delivery;
            $tmp['ostatok'] = (string) $obj->Ostatatok;
            $tmp['sklad'] = (string) $obj->Sklad;
            $tmp['model'] = (string) $obj->model;
            foreach ($obj->param as $attr) {
              if(!$table && $attr['name'] == 'Вязкость_1'){
                $table = 'oil';
              }
              if(!$table && $attr['name'] == 'Сезонность'){
                $table = 'shiny';
              }
              if(!$table && $attr['name'] == 'Сверловка'){
                $table = 'disc';
              }
              if(!$table && $attr['name'] == 'Артикул'){
                $table = 'battery';
              }
              $a = (string)$attr['name'];       
              $tmp['params'][$a] = (string)$attr;
            }
            if($table == 'oil'){
              $oil[] = $tmp;
            }
            if($table == 'shiny'){
              $shiny[] = $tmp;
            }
            if($table == 'disc'){
              $disc[] = $tmp;
            }
            if($table == 'battery'){
              $battery[] = $tmp;
            }          
          }

          $ci++;        
          Console::updateProgress($ci, $co);
      }
      Console::endProgress();
      if(count($oil)>0){
      echo "Обработка товаров масла ".PHP_EOL;
      $ci = 0;
      $co = count($oil);
      Console::startProgress($ci, $co);
        foreach($oil as $item){
          $modelOil = Oil::find()->where(['code'=>$item['code']])->one();
          if(!$modelOil){
            $modelOil = new Oil();
            $modelOil->picture      = $item['picture'];
            $create++;
          }else{
            $update++;
            // continue;
          }
          $modelOil->code         = $item['code'];
          $modelOil->price        = $item['price'];
          $modelOil->old_price    = $item['old_price'];
          $modelOil->currency     = $item['currencyId'];
          $modelOil->model        = $item['model'];
          $modelOil->delivery     = $item['delivery'];
          $modelOil->ostatok      = $item['ostatok'];
          $modelOil->sklad      = $item['sklad'];
          $modelOil->status       = 1;
          foreach ($item['params'] as $key => $value) {
            switch ($key) {
              case 'Вязкость_1':
                $modelOil->viscosity_1  = $value;
                break;
              case 'Вязкость_2':
                $modelOil->viscosity_2  = $value;
                break;
              case 'ТипЖидкости':
                $modelOil->type_fluid   = $value;
                break;
              case 'Объем':
                $modelOil->size         = $value;
                break;
              case 'ТипДвигателя':
                $modelOil->type_engine  = $value;
                break;
              case 'Бренд':
                $modelOil->parent_brend_id  = $this->getBrend($value);
                break;
              case 'Название':
                if($value)
                  $modelOil->title        = $value;
                else
                  $modelOil->title        = $item['model'];
                break;
              default:
                break;
            }
          }
          if($modelOil->save()){
            unset($modelOil);          
          }else{
            // print_r($modelOil->errors);
            file_put_contents(__DIR__.'/done/oilErr.log','Ошибка импорта '.$modelOil->code.' '.date("m-d-Y H:i:s").PHP_EOL,FILE_APPEND);
            $error++;
          }
          $ci++;  
          Console::updateProgress($ci, $co);
        }  
        Console::endProgress();    
      } 
      if(count($shiny)>0){
      echo "Обработка товаров шин ".PHP_EOL;
      $ci = 0;
      $co = count($shiny);
      Console::startProgress($ci, $co);
        foreach($shiny as $item){
          $modelShiny = Shiny::find()->where(['code'=>$item['code']])->one();
          if(!$modelShiny){
            $modelShiny = new Shiny();
            $modelShiny->picture      = $item['picture'];
            $create++;
          }else{          
            $update++;
            // continue;
          }
          $modelShiny->code         = $item['code'];
          $modelShiny->price        = $item['price'];
          $modelShiny->old_price    = $item['old_price'];
          $modelShiny->currency     = $item['currencyId'];
          $modelShiny->model        = $item['model'];
          $modelShiny->delivery     = $item['delivery'];
          $modelShiny->ostatok      = $item['ostatok'];
          $modelShiny->sklad      = $item['sklad'];
          $modelShiny->status       = 1;
          foreach ($item['params'] as $key => $value) {
            switch ($key) {
              // case 'Размер':
              //   $modelShiny->size  = $value;
              //   break;
              case 'Ширина':
                $modelShiny->width  = $value == null ? 0 : $value;
                break;
              case 'Высота':
                $modelShiny->height  = $value == null ? 0 : $value;
                break;
              case 'Диаметр':
                $modelShiny->radius  = $value == null ? 0 : $value;
                break;
              case 'Сезонность':
                $modelShiny->season  = $value;
                break;
              case 'Шип':
                $modelShiny->ship   = $value == 'нет'? 0 : 1;
                break;
              case 'ИндексНагрузки':
                $modelShiny->loads         = $value;
                break;
              case 'ИндексСкорости':
                $modelShiny->speed  = $value;
                break;
              case 'ТипТС':
                $modelShiny->typeTC         = $value;
                break;
              case 'RunFlat':
                $modelShiny->runflat  = (int) $value;
                break;
              case 'Бренд':
                $modelShiny->parent_brend_id  = $this->getBrend($value);
                break;
              case 'Название':
                if($value)
                  $modelShiny->title        = $value;
                else
                  $modelShiny->title        = $item['model'];
                break;
              default:
                break;
            }
          }
          if($modelShiny->save()){
            // echo $modelShiny->id.PHP_EOL;
            unset($modelShiny);          
          }else{
            // print_r($modelShiny->errors);
            file_put_contents(__DIR__.'/done/shinyErr.log','Ошибка импорта '.$modelShiny->code.' '.date("m-d-Y H:i:s").PHP_EOL,FILE_APPEND);
            $error++;
          }
          $ci++;  
          Console::updateProgress($ci, $co);
        }  
        Console::endProgress(); 
      } 
      if(count($disc)>0){
      echo "Обработка товаров дисков ".PHP_EOL;
      $ci = 0;
      $co = count($disc);
      Console::startProgress($ci, $co);
        foreach($disc as $item){
          $modelDisc = Disc::find()->where(['code'=>$item['code']])->one();
          if(!$modelDisc){
            $modelDisc = new Disc();
            $modelDisc->picture      = $item['picture'];
            $create++;
          }else{
            $update++;
            // continue;
          }
          $modelDisc->code         = $item['code'];
          $modelDisc->price        = $item['price'];
          $modelDisc->old_price    = $item['old_price'];
          $modelDisc->currency     = $item['currencyId'];
          $modelDisc->model        = $item['model'];
          $modelDisc->delivery     = $item['delivery'];
          $modelDisc->ostatok      = $item['ostatok'];
          $modelDisc->sklad      = $item['sklad'];
          $modelDisc->status       = 1;
          foreach ($item['params'] as $key => $value) {
            switch ($key) {
              case 'Размер':
                $modelDisc->size  = $value;
                break;
              case 'Сверловка':
                $modelDisc->borer  = $value;
                break;
              case 'ДиаметрСтупицы':
                $modelDisc->stupica   = $value == null ? 0 :$value;
                break;
              case 'Вылет':
                $modelDisc->radius    = $value == null ? 0 :$value;
                break;
              case 'Производитель':
                $modelDisc->manufacturer  = $value;
                $modelDisc->parent_brend_id  = $this->getBrend($value);
                break;
              case 'Тип':
                $modelDisc->type  = $value;
                break;
              case 'Цвет':
                $modelDisc->color  = $value == null ? 'без цвета' : $value;
                break;
              case 'Бренд':
                $modelDisc->parent_brend_id  = $this->getBrend($value);
                break;
              case 'Название':
                if($value)
                  $modelDisc->title        = $value;
                else
                  $modelDisc->title        = $item['model'];
                break;
              default:
                break;
            }
          }
          if($modelDisc->save()){
            unset($modelDisc);          
          }else{
            // print_r($modelDisc->errors);
            file_put_contents(__DIR__.'/done/discErr.log','Ошибка импорта '.$modelDisc->code.' '.date("m-d-Y H:i:s").PHP_EOL,FILE_APPEND);
            $error++;
          }
          $ci++;  
          Console::updateProgress($ci, $co);
        }  
        Console::endProgress();     
      } 
      if(count($battery)>0){
      echo "Обработка товаров аккумуляторов ".PHP_EOL;
      $ci = 0;
      $co = count($battery);
      Console::startProgress($ci, $co);
        foreach($battery as $item){
          $modelBattery = Battery::find()->where(['code'=>$item['code']])->one();
          if(!$modelBattery){
            $modelBattery = new Battery();
            $modelBattery->picture      = $item['picture'];
            $create++;
          }else{
            $update++;
            // continue;
          }
          $modelBattery->code         = $item['code'];
          $modelBattery->price        = $item['price'];
          $modelBattery->old_price    = $item['old_price'];
          $modelBattery->currency     = $item['currencyId'];
          $modelBattery->model        = $item['model'];
          $modelBattery->delivery     = $item['delivery'];
          $modelBattery->ostatok      = $item['ostatok'];
          $modelBattery->sklad      = $item['sklad'];
          $modelBattery->status       = 1;
          foreach ($item['params'] as $key => $value) {
            switch ($key) {
              case 'Артикул':
                $modelBattery->article  = $value;
                break;
              case 'Ёмкость':
                $modelBattery->capacity  = $value;
                break;
              case 'СтартовыйТок':
                $modelBattery->current   = $value;
                break;
              case 'Полярность':
                $modelBattery->polarity  = $value;
                break;
              case 'Длина':
                $modelBattery->length    = (int) $value;
                break;
              case 'Ширина':
                $modelBattery->width     = (int) $value;
                break;
              case 'Высота':
                $modelBattery->height  = (int) $value;
                break;
              case 'Страна':
                $modelBattery->country  = $value;
                break;
              case 'Гарантия':
                $modelBattery->assurance  = $value;
                break;
              case 'Напряжение':
                $modelBattery->voltage  = (int) $value;
                break;
              case 'Бренд':
                $modelBattery->parent_brend_id  = $this->getBrend($value);
                break;
              case 'Название':
                if($value)
                  $modelBattery->title        = $value;
                else
                  $modelBattery->title        = $item['model'];
                break;
              default:
                break;
            }
          }
          if($modelBattery->save()){
            unset($modelBattery);          
          }else{
            // print_r($modelBattery->errors);
            file_put_contents(__DIR__.'/done/batteryErr.log','Ошибка импорта '.$modelBattery->code.' '.date("m-d-Y H:i:s").PHP_EOL,FILE_APPEND);
            $error++;
          }
          $ci++;
          Console::updateProgress($ci, $co);
        }  
        Console::endProgress();       
      } 

      echo "Создано ".$create." отмененных обновлений ".$update." ошибок ".$error.PHP_EOL;

      file_put_contents(__DIR__.'/done/import.log','Импорт успешно завершент Создано '.$create.' обновлено '.$update.' отменено '.$error.' дата импорта '.date("m-d-Y H:i:s").PHP_EOL,FILE_APPEND);

      // echo "Обновляем картинки ".PHP_EOL;
      // $this->actionUploadImage();
      $this->moveImpportFile();
      }else{
        echo "Файл импорта не найден".PHP_EOL;
      } 
    }
    catch (ErrorException $e) {
      file_put_contents(__DIR__."/done/importErr.log",'Импорт завершент с ошибкой '.date("m-d-Y H:i:s").PHP_EOL.$e->getMessage().PHP_EOL,FILE_APPEND);
    }
  }
  private function getBrend($title){
    if(!$title){
      $title = 'Другие бренды';
    }
    $brend = Brend::find()->where(['title'=>$title])->one();
    if(!$brend){
      $brend = new Brend();                
      $brend->title = $title;
      $brend->save();
    }
    return $brend->id;
  }
  /**
  * Перебирает базу для скачивания изображений и копирует в локальную папку.
  */
  public function actionUploadImage()
  {
    /*Battery
    Disc
    Oil
    Shiny*/
    // другой вариант скачивания файла
    // $ch = curl_init($oil->picture);
    // $fp = fopen('./images/logo.png', 'wb');
    // curl_setopt($ch, CURLOPT_FILE, $fp);
    // curl_setopt($ch, CURLOPT_HEADER, 0);
    // curl_exec($ch);
    // curl_close($ch);
    // fclose($fp);
    $oils = Oil::find()->all();
    if($oils){      
      echo "Обработка картинок масла ".PHP_EOL;
      $ci = 0;
      $co = count($oils);
      Console::startProgress($ci, $co);
      foreach ($oils as $oil) {
        $this->moveParseFile($oil);      
        $ci++;  
        Console::updateProgress($ci, $co);
      }  
      Console::endProgress(); 
    }
    $oils = Shiny::find()->all();
    if($oils){      
      echo "Обработка картинок шин ".PHP_EOL;
      $ci = 0;
      $co = count($oils);
      Console::startProgress($ci, $co);
      foreach ($oils as $oil) {
        $this->moveParseFile($oil);      
        $ci++;  
        Console::updateProgress($ci, $co);
      }  
      Console::endProgress(); 
    }
    $oils = Disc::find()->all();
    if($oils){
      echo "Обработка картинок дисков ".PHP_EOL;
      $ci = 0;
      $co = count($oils);
      Console::startProgress($ci, $co);
      foreach ($oils as $oil) {
        $this->moveParseFile($oil);      
        $ci++;  
        Console::updateProgress($ci, $co);
      }  
      Console::endProgress();  
    }
    $oils = Battery::find()->all();
    if($oils){      
      echo "Обработка картинок аккумуляторов ".PHP_EOL;
      $ci = 0;
      $co = count($oils);
      Console::startProgress($ci, $co);
      foreach ($oils as $oil) {
        $this->moveParseFile($oil);      
        $ci++;  
        Console::updateProgress($ci, $co);
      }  
      Console::endProgress(); 
    }
    $this->moveImpportFile();
  }

  private function moveImpportFile(){
    if(file_exists(Yii::getAlias('@ftp')."/importnew.xml")){
      rename(Yii::getAlias('@ftp')."/importnew.xml", __DIR__."/done/importnew.xml"); 
    }
    echo "Импорт завершен".PHP_EOL;
  }

  private function moveParseFile($model){
    if($model->picture == "/upload/none_pic.jpg"){
      return;
    }
    if($model->picture == "http://mastershina.kz/upload/images/none_pic.jpg"){
      if(!file_exists(dirname(\Yii::$app->basePath)."/frontend/web/upload/none_pic.jpg")){
        $file = file_get_contents($model->picture);
        file_put_contents(dirname(\Yii::$app->basePath)."/frontend/web/upload/none_pic.jpg", $file);
      }
      $model->picture = "/upload/none_pic.jpg";
      $model->save();
      return;
    }
    $array = explode("/",$model->picture);
    $name = $array[(count($array)-1)];
    if(count(explode(".",$name))==1){
      file_put_contents('filelog.log','Ошибка загрузки файла '.$model->id.' '.$model->picture,FILE_APPEND);
      $model->picture = "/upload/none_pic.jpg";
      $model->save();
      // echo "Ошибка загрузки файла ".$name;
      return;
    }
    // echo "ID = ".$model->id.PHP_EOL;
    $file = false;
    if(count($array) > 3){ 
      $path = dirname(\Yii::$app->basePath)."/frontend/web/upload/".$name;
      if(file_exists($path)){
        $model->picture = "/upload/".$name;
        $model->save();
        // echo $name." Файл уже существует.".PHP_EOL;
        return;
      } 
      try{
        $file = file_get_contents($model->picture);     
      }
      catch(ErrorException $e){
        if(strripos($e->getMessage(),'404 Not Found')){
          file_put_contents('filelog.log','Ошибка загрузки файла '.$model->id.' '.$model->picture,FILE_APPEND);
          $model->picture = "/upload/none_pic.jpg";
          $model->save();
          return;
        }
      }      
      if($file){
        $path = dirname(\Yii::$app->basePath)."/frontend/web/upload/".$name;
        if(file_exists($path)){
          $model->picture = "/upload/".$name;
          $model->save();
          // echo $name." Файл уже существует.".PHP_EOL;
        }
        else if(file_put_contents($path, $file)){
          $model->picture = "/upload/".$name;
          $model->save();
          // echo $name." скопирован успешно.".PHP_EOL;        
        }            
      }else{  
        file_put_contents('filelog.log','Error upload file '.$model->id.' '.$model->picture,FILE_APPEND);
      }
    }
    // echo $array[(count($array)-1)]." локальный.".PHP_EOL;
    return;
  }
  /**
  * Перебирает файл импорта и проверяет наличие картинок на сайте.
  */
  public function actionCheckImages(){
    $objPHPExcel = simplexml_load_file(Yii::getAlias('@ftp')."/importnew.xml");   
    foreach ($objPHPExcel->offers as $keys => $objs) {
      foreach($objs as $obj){
        if(strripos($obj->picture,'none_pic.jpg')){
          file_put_contents('none_pic.log',(string) $obj['id'].' => '.$obj->picture."\n",FILE_APPEND);
          echo "write ".$obj->picture." done".PHP_EOL;
          continue;
        }
        if((string)$obj->picture === "http://mastershina.kz/upload/images/"){
          file_put_contents('checkimages.log',(string) $obj['id'].' => '.$obj->picture."\n",FILE_APPEND);
          echo "write ".$obj->picture." done".PHP_EOL;
          continue;
        }
        try{
          $file = file_get_contents($obj->picture);
          echo "check ".$obj->picture." done".PHP_EOL;
        }catch(ErrorException $e){
          if(strripos($e->getMessage(),'404 Not Found')){
            file_put_contents('checkimages.log',(string) $obj['id'].' => '.$obj->picture." 404 Not found\n",FILE_APPEND);
            echo "write ".$obj->picture." done".PHP_EOL;            
          }
        }
        // $tmp = [];$table=false;
        // $tmp['code'] = (string) $obj['id'];
        // $tmp['old_price'] = (int) $obj->СтараяЦена;
        // $tmp['price'] = (int) $obj->Цена;
        // $tmp['currencyId'] = (string) $obj->currencyId ;
        // $tmp['picture'] = (string) $obj->picture;
      }
    }
  }

  /**
  * Заполнение ширины высоты и радиуса из размера в таблице Shiny.
  */
  public function actionParseSize(){
    $models = Shiny::find()->all();
    foreach ($models as $m) {
      $a = explode("/",$m->size);      
      $b = explode(" ",$a[1]);
      $m->width = $a[0];
      $m->height = $b[0];
      $m->radius = $b[1];
      $m->save();
      echo $m->id.PHP_EOL;
    }
  }

  /**
  * Создание url адреса.
  */
  public function actionCreateUrl($table='all'){    
    switch ($table) {
      case 'oil':
        $oil = Oil::find()->all();
        foreach ($oil as $model) {
          $model->url = null;
          $model->save();
          echo 'oil '.$model->id.' done'.PHP_EOL;
        }
        break;
      case 'shiny':
        $shiny = Shiny::find()->all();
        foreach ($shiny as $model) {
          $model->url = null;
          $model->save();
          echo 'shiny '.$model->id.' done'.PHP_EOL;
        }
        break;
      case 'disc':
        $disc = Disc::find()->all();
        foreach ($disc as $model) {
          $model->url = null;
          $model->save();
          echo 'disc '.$model->id.' done'.PHP_EOL;
        }
        break;
      case 'battery':
        $battery = Battery::find()->all();
        foreach ($battery as $model) {
          $model->url = null;
          $model->save();
          echo 'battery '.$model->id.' done'.PHP_EOL;
        } 
        break;
      
      default:        
        $oil = Oil::find()->all();
        foreach ($oil as $model) {
          $model->url = null;
          $model->save();
          echo 'oil '.$model->id.' done'.PHP_EOL;
        }
        $shiny = Shiny::find()->all();
        foreach ($shiny as $model) {
          $model->url = null;
          $model->save();
          echo 'shiny '.$model->id.' done'.PHP_EOL;
        }
        $disc = Disc::find()->all();
        foreach ($disc as $model) {
          $model->url = null;
          $model->save();
          echo 'disc '.$model->id.' done'.PHP_EOL;
        }
        $battery = Battery::find()->all();
        foreach ($battery as $model) {
          $model->url = null;
          $model->save();
          echo 'battery '.$model->id.' done'.PHP_EOL;
        } 
        break;
    }
  }
}