<?php

namespace api\components;

use Yii;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class Controller extends ActiveController//\yii\rest\Controller
{

  public $enableCsrfValidation = false;
  private $_verbs = ['POST', 'PUT', 'DELETE', 'GET', 'OPTIONS'];
  
  public function actionOptions ()
  {
      if (Yii::$app->getRequest()->getMethod() !== 'OPTIONS') {
          Yii::$app->getResponse()->setStatusCode(405);
      }
      $options = $this->_verbs;
      Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $options));
  }
  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' => 'api\components\HttpHeaderAuth',
      'except' => ['login','token','response','upload','resend'],
    ];
    $behaviors['corsFilter'] = [
      'class' => \yii\filters\Cors::className(),
      'cors' => [
        'Origin' => ['*'],
        'Access-Control-Request-Method' => ['POST', 'PUT', "DELETE", 'GET', 'OPTIONS'],
        'Access-Control-Request-Headers' => ['*'],
        'Access-Control-Allow-Credentials' => true,
        'Access-Control-Max-Age' => 3600,
        'Access-Control-Expose-Headers' => [],
        // // Allow only POST and PUT methods
        // 'Access-Control-Request-Headers' => ['X-Key'],
        // // Allow only headers 'X-Wsse'
        // 'Access-Control-Allow-Credentials' => true,
        // // Allow OPTIONS caching
        // 'Access-Control-Max-Age' => 3600,
        // // Allow the X-Pagination-Current-Page header to be exposed to the browser.
        // 'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
      ],

    ];
    $behaviors['bootstrap'] = [
      'class' => ContentNegotiator::className(),
      'formats' => [
        'application/json' => Response::FORMAT_JSON,
      ]
    ];
    return $behaviors;
  }
}
?>