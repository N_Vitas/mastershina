<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace api\components;

use Yii;
use yii\web\ServerErrorHttpException;
use common\models\ICatElements;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;
use common\models\Battery;
/**
 * DeletePitstop implements the API endpoint for deleting a model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DeletePitstop extends \yii\rest\Action
{
    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        self::deleteAll($id);
    }
    public static function deleteAll($id)
    {
        $model = ICatElements::findOne($id);
        if ($model === null) {
            throw new ServerErrorHttpException('Не удалось удалить запись!.');
        }
        if($model['1c_code'] != null){
            $shinyes = Shiny::find()->where(['code' => $model['1c_code']])->all(); 
            foreach($shinyes as $shiny){
                if($shiny->delete() === false){
                    throw new ServerErrorHttpException('Не удалось удалить запись!.');
                }
            }
        }
        if($model['1c_code_disk'] != null){
            $discs = Disc::find()->where(['code' => $model['1c_code_disk']])->all(); 
            foreach($discs as $disc){
                if($disc->delete() === false){
                    throw new ServerErrorHttpException('Не удалось удалить запись!.');
                }
            }
        }
        if($model['a_1c'] != null){
            $batteryes = Battery::find()->where(['code' => $model['a_1c']])->all(); 
            foreach($batteryes as $battery){
                if($battery->delete() === false){
                    throw new ServerErrorHttpException('Не удалось удалить запись!.');
                }
            }
        }
        if($model['m_1c'] != null){
            $oiles = Oil::find()->where(['code' => $model['m_1c']])->all(); 
            foreach($oiles as $oil){
                if($oil->delete() === false){
                    throw new ServerErrorHttpException('Не удалось удалить запись!.');
                }
            }
        }
        if($model->delete() === false){
            throw new ServerErrorHttpException('Не удалось удалить запись!.');
        }
        Yii::$app->getResponse()->setStatusCode(204);
    }
}