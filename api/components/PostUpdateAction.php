<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace api\components;

use Yii;
use yii\web\ServerErrorHttpException;
use common\models\Followers;
/**
 * DeleteAction implements the API endpoint for deleting a model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PostUpdateAction extends \yii\rest\Action
{
    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        var_dump(yii::$app->request->queryParams);die;
        file_put_contents(\Yii::getAlias('@api/components').'/filelog.log', var_dump(yii::$app->request->rawBody()));
        Yii::$app->getResponse()->setStatusCode(204);
    }
}