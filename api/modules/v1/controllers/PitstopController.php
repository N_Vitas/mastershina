<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use api\components\DeletePitstop;
use yii\web\HttpException;
use common\models\ICatElements;
use common\models\SearchApiDataProvider;

class PitstopController extends Controller
{
  public $modelClass = 'common\models\ICatElements';

  public function actions()
	{

		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
    // $actions['delete'] = [
    //   'class' => 'api\components\DeletePitstop',
    //   'modelClass' => 'common\models\ICatElements',
    // ];
    unset($actions['delete']);
		return $actions;
  }
  
  public function actionDeleteAll($id)
  {
    return DeletePitstop::deleteAll($id);
  }

  public function prepareDataProvider()
  {

    $searchModel = new SearchApiDataProvider();
    $models = $searchModel->search(Yii::$app->request->get());
    return [
      'models' => $models->getModels(),
      // получение количества данных на текущей странице
      'count' => $models->getCount(),
      'total' => $models->getTotalCount(),
    ];
  }

  public function actionSearch()
  {

    $searchModel = new SearchApiDataProvider();
    $models = $searchModel->search(Yii::$app->request->get());
    return [
      'models' => $models->getModels(),
      // получение количества данных на текущей странице
      'count' => $models->getCount(),
      'total' => $models->getTotalCount(),
    ];
  }
  public function actionShinyNoPick()
  {
    $dataset = ICatElements::find()->where(['tire_photo' => 'none_pic.jpg'])->where(['tire_photo' => ''])->all();
    return $dataset;
  }
  public function actionDiscNoPick()
  {
    $dataset = ICatElements::find()->where(['disk_img' => 'none_pic.jpg'])->where(['disk_img' => ''])->all();
    return $dataset;
  }
  public function actionBatteryNoPick()
  {
    $dataset = ICatElements::find()->where(['a_img' => 'none_pic.jpg'])->where(['a_img' => ''])->all();
    return $dataset;
  }
  public function actionOilNoPick()
  {
    $dataset = ICatElements::find()->where(['m_img' => 'none_pic.jpg'])->where(['m_img' => ''])->all();
    return $dataset;
  }
}
?>