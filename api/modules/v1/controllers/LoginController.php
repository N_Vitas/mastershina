<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use common\models\LoginForm;

class LoginController extends Controller
{
    public $modelClass = 'common\models\LoginForm';

    public function actionToken()
    {
        if(Yii::$app->request->getMethod() === 'OPTIONS'){
            return;
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return Yii::$app->user->identity;
        }
        Yii::$app->getResponse()->setStatusCode(403);
        return ["result" => false];
    }
}
?>