<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use common\models\Disc;

class DiscController extends Controller
{
	public $modelClass = 'common\models\Disc';
  public function actionIndex()
  {
  	$dataset = ICatElements::find()->select('max(id)')->scalar();
  	return $dataset;
  }
  public function actionNoPick()
  {
    $dataset = Disc::find()->where(['picture' => '/upload/none_pic.jpg'])
    ->orWhere(['picture' => '/upload/'])
    ->orWhere(['picture' => ''])->all();
    return $dataset;
  }
}
?>