<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use common\models\ICatElements;
use common\models\Shiny;
use common\models\Disc;
use common\models\Oil;
use common\models\Battery;
use common\models\ImagesFile;

class ImagesController extends Controller
{
	public $modelClass = 'common\models\ImagesFile';
  public function actionNoPickCount()
  {
    $dataset["oldSite"]["shiny"] = ICatElements::find()
        ->where(['tire_photo' => 'none_pic.jpg'])
        ->orWhere(['tire_photo' => ''])
        ->count();
    $dataset["oldSite"]["disk"] = ICatElements::find()
        ->where(['disk_img' => 'none_pic.jpg'])
        ->orWhere(['disk_img' => ''])
        ->count();
    $dataset["oldSite"]["oil"] = ICatElements::find()
        ->where(['m_img' => 'none_pic.jpg'])
        ->orWhere(['m_img' => ''])
        ->count();
    $dataset["oldSite"]["battery"] = ICatElements::find()
        ->where(['a_img' => 'none_pic.jpg'])
        ->orWhere(['a_img' => ''])
        ->count();
    $dataset["newSite"]["shiny"] = Shiny::find()
        ->where(['picture' => '/upload/none_pic.jpg'])
        ->orWhere(['picture' => '/upload/'])
        ->orWhere(['picture' => ''])
        ->count();
    $dataset["newSite"]["disk"] = Disc::find()
        ->where(['picture' => '/upload/none_pic.jpg'])
        ->orWhere(['picture' => '/upload/'])
        ->orWhere(['picture' => ''])
        ->count();
    $dataset["newSite"]["oil"] = Oil::find()
        ->where(['picture' => '/upload/none_pic.jpg'])
        ->orWhere(['picture' => '/upload/'])
        ->orWhere(['picture' => ''])
        ->count();
    $dataset["newSite"]["battery"] = Battery::find()
        ->where(['picture' => '/upload/none_pic.jpg'])
        ->orWhere(['picture' => '/upload/'])
        ->orWhere(['picture' => ''])
        ->count();
    return $dataset;
  }

  public function actionUpdateProduct(){
    if(!$code = yii::$app->request->post("code")){
        return ["result" => false];
    }
    if(!$description = yii::$app->request->post("description")){
        return ["result" => false];
    }
    if(!$images = yii::$app->request->post("images")){
        return ["result" => false];
    }
    if(!$title = yii::$app->request->post("title")){
        return ["result" => false];
    }
    // Обновляем старый сайт.
    if($pitstop = ICatElements::find()->where(["or",
        ["1c_code" => $code],
        ["1c_code_disk" => $code],
        ["m_1c" => $code],
        ["a_1c" => $code],
    ])->one()){
        $pitstop->tire_photo = $images;
        $pitstop->tire_desc = $description;
        $pitstop->disk_img = $images;
        $pitstop->disk_text = $description;
        $pitstop->a_img = $images;
        $pitstop->a_text = $description;
        $pitstop->m_img = $images;
        $pitstop->m_text = $description;
        if(!$pitstop->save()){
            return ["result" => false,"error"=>$pitstop->errors];
        }
    }
    if($shiny = Shiny::find()->where(['code' => $code])->one()){
        $shiny->picture = "/upload/".$images;
        $shiny->description = $description;
        if(!$shiny->save()){
            return ["result" => false,"error"=>$shiny->errors];
        }
    }
    else if($disk = Disc::find()->where(['code' => $code])->one()){
        $disk->picture = "/upload/".$images;
        $disk->description = $description;
        if(!$disk->save()){
            return ["result" => false,"error"=>$disk->errors];
        }
    }
    else if($oil = Oil::find()->where(['code' => $code])->one()){
        $oil->picture = "/upload/".$images;
        $oil->description = $description;
        if(!$oil->save()){
            return ["result" => false,"error"=>$oil->errors];
        }
    }
    else if($battery = Battery::find()->where(['code' => $code])->one()){
        $battery->picture = "/upload/".$images;
        $battery->description = $description;
        if(!$battery->save()){
            return ["result" => false,"error"=>$battery->errors];
        }
    }
    return ["result" => true];
  }

  public function actionUpload()
    {
        $model = new ImagesFile();
        // var_dump($_POST,$_FILES);die;
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                // file is uploaded successfully
                return ["result" => true];
            }
        }

        return $model;
    }
}
?>