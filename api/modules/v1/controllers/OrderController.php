<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\web\HttpException;
use common\models\OrderApi;
use common\models\OrderHistory;
use common\models\Order;

class OrderController extends Controller
{
	public $modelClass = 'common\models\OrderApi';

	public function actions()
	{
		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
		// $actions['update'] = [
	 //            'class' => 'api\components\PostUpdateAction',
	 //            'modelClass' => $this->modelClass,
	 //            'checkAccess' => [$this, 'checkAccess']
	 //         ];
		unset($actions['delete'],$actions['create']);
		return $actions;
	}

	public function prepareDataProvider()
	{
			return new \yii\data\ActiveDataProvider([
				'query' => \common\models\Order::find()->where(["export"=>1])->orderBy(['created_at'=> SORT_DESC]),
			]);
	}

	public function actionResend(){
		$models = Order::find()->where(["id_crm" => ""])->all();
		foreach($models as $model){
			$model->comment = json_decode($model->comment,true);
			$model->data = json_decode($model->data,true);
			$model->status = Order::$statuses['order'][$model->status];
			$model->author = $model->user->id_crm;
			$model->manager_crm = $model->manager->id_crm;
			if(yii::$app->params["production"]){
			  $exchange = 'exchange';
			  $queue = 'queue';      
			}else{      
			  $exchange = 'test-exchange';
			  $queue = 'test-queue';
			}
			$message = serialize($model->attributes);
			Yii::$app->amqp->declareExchange($exchange, $type = 'direct', $passive = false, $durable = true, $auto_delete = false);
			Yii::$app->amqp->declareQueue($queue, $passive = false, $durable = true, $exclusive = false, $auto_delete = false);
			Yii::$app->amqp->bindQueueExchanger($queue, $exchange, $routingKey = $queue);
			Yii::$app->amqp->publish_message($message, $exchange, $routingKey = $queue, $content_type = 'applications/json', $app_id = Yii::$app->name);
		}
		return $models;
	}
}
?>