<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;
use common\models\ClientApi;

class ClientController extends Controller
{
  public $modelClass = 'common\models\ClientApi';

  public function actions()
	{
		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
		unset($actions['delete']);
		return $actions;
	}

	public function actionImport(){
		if(yii::$app->request->post()){
			// $data = '{"list": [{"id":"CRM067626","name":"Ташимбаева Хорлан Газизбековна","iin":"700306400452","tiresinstorage":"Да","contacts": [{"phone":"+77019779333"}],"tc": [{"model":"WTouarek","inumber":"A773 KYO","size":"235/65/17"}]}]}';
			$data =  yii::$app->request->post("list");
      foreach ($data as $item) {
      	if(!$model = \common\models\Client::find()->where(["client_id"=>$item["id"]])->one()){
      		$model = new \common\models\Client();
      		$model->data = '{"order_id":"0"}';
      	}
      	$model->client_id = $item["id"];
      	$model->manager = "Система";
      	$model->name = $item["name"];
      	$model->tiresinstorage = $item["tiresinstorage"];
      	if(count($item["contacts"])){
      		foreach ($item["contacts"] as $value) {      			
		      	if($value["phone"]){
		      		$model->phone = $value["phone"];	      		
		      	}
		      	if(count($item["email"])){
		      		$model->email = $value["email"];	      		
		      	}
      		}
      		$model->contacts = json_encode($item["contacts"],JSON_UNESCAPED_UNICODE);
      	}
      	if(count($item["tc"])){
      		$model->tc = json_encode($item["tc"],JSON_UNESCAPED_UNICODE);     		
      	}
      	if($model->save()){
      		return ["status"=>"OK"];
      	}else{
      		return $model->errors;
      	}
      }
			return $data;
		}
		throw new HttpException(403,'Failed to delete the object for unknown reason.');
	}

	public function prepareDataProvider()
   {
   	if(yii::$app->request->get("name"))
			return new \yii\data\ActiveDataProvider([
				'query' => \common\models\ClientApi::find()->where('name=:name',[":name"=>yii::$app->request->get("name")]),
			]);
		else			
			return new \yii\data\ActiveDataProvider([
				'query' => \common\models\ClientApi::find()->where('name=:name',[":name"=>yii::$app->request->get("name")]),
			]);
	}
}
?>