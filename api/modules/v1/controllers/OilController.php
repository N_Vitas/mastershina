<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;
use common\models\Oil;

class OilController extends Controller
{
	public $modelClass = 'common\models\Oil';

  public function actionNavigate()
  {
    $provider = new ActiveDataProvider([
        'query' => Oil::find(),
        'pagination' => [
          'pageSize' => 10
        ],
    ]);

    if($search = Yii::$app->getRequest()->get('search')){
      $provider->andFilterWhere(['like','title',$search]);
    }
    return [
      'models' => $provider->getModels(),
      'count' => $provider->getCount(),
      'totalCount' => $provider->getTotalCount(),
    ];
  }
  public function actionNoPick()
  {
    $dataset = Oil::find()->where(['picture' => '/upload/none_pic.jpg'])
    ->orWhere(['picture' => '/upload/'])
    ->orWhere(['picture' => ''])->with('brend')->all();
    return $dataset;
  }
}
?>