<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use common\models\Shiny;

class ShinyController extends Controller
{
	public $modelClass = 'common\models\Shiny';
  public function actionIndex()
  {
  	$dataset = ICatElements::find()->select('max(id)')->scalar();
  	return $dataset;
  }
  public function actionNoPick()
  {
    $dataset = Shiny::find()->where(['picture' => '/upload/none_pic.jpg'])
    ->orWhere(['picture' => '/upload/'])
    ->orWhere(['picture' => ''])->all();
    return $dataset;
  }
}
?>