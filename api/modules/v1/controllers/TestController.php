<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use common\models\Person;
use common\models\OrderApi;
use common\models\Tirefitting;
use frontend\models\SignupForm;

class TestController extends Controller
{
	public $modelClass = 'yii\base\Model';
  public function actionResponse()
  {
  	// $max = OrderApi::find()->select('max(id)')->scalar();
   //  $item[] = ["id" => (int) $max,"id_crm" => "000057929","manager_crm" =>"" ,"status"=> "Подбор товара","status_payment" => 0,"status_delivery" => 0,"export" => 1];
   //  return $item;
  	$users = [];
  	foreach (Yii::$app->request->post() as $attributes) {
  		$model = new SignupForm();
      if ($model->load($attributes)) {
        if ($user = $model->signup()) {
          $users[] = $user;
        }
      }else{
      	$users[] = $model->errors;
      }
  	}
  	return $users;
  } 

//   public function actionMsTest(){
//     $query = "select p.SN, p.Account, s.Name ServName, p.IDService, DateIn, p.Amount-p.Commission AmountonAccount,
// case when p.IDTerminal=27574 then 1 when p.IDTerminal=27735 then 2 when p.IDTerminal=58603 then 3 else 4 end Flag
// from Payments p with (nolock)
// inner join Service s with (nolock) on p.IDService=s.IDService
// left join widget.dbo.Payments w with (nolock) on p.SN=w.[Transaction]
// where p.DateIn >='2017-03-01' and DateIn<dateadd(d,1,'2017-03-27') and DateOut>'2000-01-01'
// and s.IDProviders=918";
//     return Yii::$app->db2->createCommand($query)->execute();
//     // return Person::find()->select(['ID','Name','Surname'])->limit(100)->all();
//   }
}
?>