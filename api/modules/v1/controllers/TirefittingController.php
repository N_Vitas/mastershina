<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use common\models\Tirefitting;

class TirefittingController extends Controller
{
  public $modelClass = 'common\models\Tirefitting';

  public function actions()
	{
		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
		unset($actions['delete']);
		return $actions;
	}

	public function prepareDataProvider()
  {			
		return $this->queryCRM('http://82.200.146.158/crm-test/hs/ForJivositeMastershina/tire');
	}

	private function queryCRM($url,$method='get',$params=null){
   	$client = new \yii\httpclient\Client();
    if($params != null){
    	$response = $client->createRequest()->setMethod($method)->setOptions(['timeout' => 15])->setUrl($url)->setData($params)->send(); 	
    }else{    	
    	$response = $client->createRequest()->setMethod($method)->setOptions(['timeout' => 15])->setUrl($url)->send();
    }
    if($response->isOk) {
      return $response->data;
    }else{
    	return false;
    }
	}
}
?>