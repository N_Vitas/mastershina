<?php

namespace api\modules\v1\controllers;

use Yii;
use api\components\Controller;
use yii\web\HttpException;
use common\models\Battery;

class BatteryController extends Controller
{
	public $modelClass = 'common\models\Battery';
  public function actionIndex()
  {
  	$dataset = ICatElements::find()->select('max(id)')->scalar();
  	return $dataset;
  }
  public function actionNoPick()
  {
    $dataset = Battery::find()->where(['picture' => '/upload/none_pic.jpg'])
    ->orWhere(['picture' => '/upload/'])
    ->orWhere(['picture' => ''])->all();
    return $dataset;
  }
}
?>