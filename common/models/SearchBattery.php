<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Battery;

class SearchBattery extends Battery
{
  public $_capacity = ['all'=>'Все','100'=>'100','105'=>'105','11'=>'11','120'=>'120','125'=>'125','132'=>'132','140'=>'140','141'=>'141','145'=>'145','180'=>'180','190'=>'190','200'=>'200','215'=>'215','221'=>'221','230'=>'230','235'=>'235','38'=>'38','41'=>'41','45'=>'45','50'=>'50','53'=>'53','55'=>'55','60'=>'60','62'=>'62','64'=>'64','65'=>'65','66'=>'66','70'=>'70','71'=>'71','72'=>'72','74'=>'74','75'=>'75','77'=>'77','80'=>'80','90'=>'90','91'=>'91','92'=>'92','95'=>'95'];
  public $_current = ['all'=>'Все','100'=>'100','1150'=>'1150','120'=>'120','140'=>'140','160'=>'160','1000'=>'1 000','1150'=>'1 150','1200'=>'1 200','1250'=>'1 250','1300'=>'1 300','200'=>'200','300'=>'300','340'=>'340','390'=>'390','420'=>'420','450'=>'450','460'=>'460','480'=>'480','500'=>'500','510'=>'510','520'=>'520','540'=>'540','550'=>'550','560'=>'560','580'=>'580','600'=>'600','630'=>'630','640'=>'640','670'=>'670','680'=>'680','700'=>'700','710'=>'710','720'=>'720','750'=>'750','760'=>'760','800'=>'800','810'=>'810','850'=>'850','870'=>'870','880'=>'880','900'=>'900','925'=>'925','950'=>'950'];
  public $_polarity = ['all'=>'Все','0--sprava'=>'0 (+ справа)','1--sleva'=>'1 (+ слева)','1-sleva'=>'1(+ слева)','3--sverhu'=>'3 (+ сверху)','4--snizu'=>'4 (+ снизу)','4-bolt'=>'4 (болт)'];
  public $_length = ['all'=>'Все','187'=>'187','207'=>'207','230'=>'230','234'=>'234','238'=>'238','240'=>'240','242'=>'242','255'=>'255'];
  public $_width = ['all'=>'Все','127'=>'127','128'=>'128','170'=>'170','172'=>'172','173'=>'173','175'=>'175','178'=>'178','189'=>'189','195'=>'195','210'=>'210','222'=>'222','223'=>'223','240'=>'240','249'=>'249','255'=>'255'];
  public $_height = ['all'=>'Все','175'=>'175','189'=>'189','190'=>'190','195'=>'195','198'=>'198','200'=>'200','203'=>'203','213'=>'213','215'=>'215','217'=>'217','220'=>'220','223'=>'223','230'=>'230','240'=>'240','243'=>'243','255'=>'255'];
  public $_country = ['all'=>'Все','belarus'=>'Беларусь','germaniya'=>'Германия','polsha'=>'Польша','ssha'=>'США','yaponiya'=>'Япония'];
  public $_assurance = ['all'=>'Все','12'=>'12'];
  public $_voltage = ['all'=>'Все','12'=>'12'];
  public $_sorter = ['title'=>'Названию','price'=>'Цене'];
  public $sorter,$brend;
  public function rules()
  {
    return [
      [['sorter','capacity','current','polarity','length','width','height','country','assurance','voltage','brend'],'save']
    ];
  }

  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'sorter'=>'Сортировать по',
      'brend' => 'Бренд',
    ]);
  }

  public function getBrand(){
    $shiny = Battery::find()->groupBy(['parent_brend_id'])->with('brend')->all();
    return ArrayHelper::getColumn($shiny,'brend.url');
  }
  public function getCapacity(){
    $w = $this->initSearch();
    if(count($w) > 0){      
      if($a = Battery::find()->select('capacity')->where($w)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('capacity')->all()){
        return ['all'=>'Все']+ArrayHelper::map($a,
        function($model){
          return $model->generateUrl($model->capacity);
        },'capacity');
      }
      return ['all'=>'Все'];
    }
    return $this->_capacity;
  }
  public function getCurrent(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('current')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('current')->all(),function($model){
          return $model->generateUrl($model->current);
        },'current');
    }
    return $this->_current;
  }
  public function getPolarity(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('polarity')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('polarity')->all(),function($model){
          return $model->generateUrl($model->polarity);
        },'polarity');
    }
    return $this->_polarity;
  }
  public function getLength(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('length')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('length')->all(),function($model){
          return $model->generateUrl($model->length);
        },'length');
    }
    return $this->_length;
  }
  public function getWidth(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('width')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('width')->all(),function($model){
          return $model->generateUrl($model->width);
        },'width');
    }
    return $this->_width;
  }
  public function getHeight(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('height')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('height')->all(),function($model){
          return $model->generateUrl($model->height);
        },'height');
    }
    return $this->_height;
  }
  public function getCountry(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('country')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('country')->all(),function($model){
          return $model->generateUrl($model->country);
        },'country');
    }
    return $this->_country;
  }
  public function getAssurance(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('assurance')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('assurance')->all(),function($model){
          return $model->generateUrl($model->assurance);
        },'assurance');
    }
    return $this->_assurance;
  }
  public function getVoltage(){
    $w = $this->initSearch();
    if(count($w) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Battery::find()->select('voltage')->where($w)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('voltage')->all(),function($model){
          return $model->generateUrl($model->voltage);
        },'voltage');
    }
    return $this->_voltage;
  }

  private function initSearch(){
    $where = [];

    if($this->capacity != 'index' && $this->capacity != '' && $this->capacity != 'all'){$where['capacity']=$this->_capacity[$this->capacity];}
    if($this->current != 'index' && $this->current != '' && $this->current != 'all'){$where['current']=$this->_current[$this->current];}
    if($this->polarity != 'index' && $this->polarity != '' && $this->polarity != 'all'){$where['polarity']=$this->_polarity[$this->polarity];}
    if($this->length != 'index' && $this->length != '' && $this->length != 'all'){$where['length']=$this->_length[$this->length];}
    if($this->width != 'index' && $this->width != '' && $this->width != 'all'){$where['width']=$this->_width[$this->width];}
    if($this->height != 'index' && $this->height != '' && $this->height != 'all'){$where['height']=$this->_height[$this->height];}
    if($this->country != 'index' && $this->country != '' && $this->country != 'all'){$where['country']=$this->_country[$this->country];}
    if($this->assurance != 'index' && $this->assurance != '' && $this->assurance != 'all'){$where['assurance']=$this->_assurance[$this->assurance];}
    if($this->voltage != 'index' && $this->voltage != '' && $this->voltage != 'all'){$where['voltage']=$this->_voltage[$this->voltage];}
    
    return $where;
  }

  public function search($params) {

    $query = self::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['title' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->initParams($params)) {
        return $dataProvider;
    }
    switch($this->sorter){
      case 'price':
      $query->orderBy(['price'=>SORT_ASC]);
      break;
      case 'title':
      $query->orderBy(['title'=>SORT_ASC]);
      break;
    }
    if($this->capacity != 'all' && $this->capacity != ''){
      $query->andFilterWhere(['like','capacity',$this->_capacity[$this->capacity]]);
    }
    if($this->current != 'all' && $this->current != ''){
      $query->andFilterWhere(['like','current',$this->_current[$this->current]]);
    }
    if($this->polarity != 'all' && $this->polarity != ''){
      $query->andFilterWhere(['like','polarity',$this->_polarity[$this->polarity]]);
    }
    if($this->length != 'all' && $this->length != ''){
      $query->andFilterWhere(['like','length',$this->_length[$this->length]]);
    }
    if($this->width != 'all' && $this->width != ''){
      $query->andFilterWhere(['like','width',$this->_width[$this->width]]);
    }
    if($this->height != 'all' && $this->height != ''){
      $query->andFilterWhere(['like','height',$this->_height[$this->height]]);
    }
    if($this->country != 'all' && $this->country != ''){
      $query->andFilterWhere(['like','country',$this->_country[$this->country]]);
    }
    if($this->assurance != 'all' && $this->assurance != ''){
      $query->andFilterWhere(['like','assurance',$this->_assurance[$this->assurance]]);
    }
    if($this->voltage != 'all' && $this->voltage != ''){
      $query->andFilterWhere(['like','voltage',$this->_voltage[$this->voltage]]);
    }
    if($this->brend != ''){
      $brend = Brend::find()->where(['url'=>str_replace('brend-','',$this->brend)])->one();
      $query->andFilterWhere(['parent_brend_id' => $brend->id]);
    }

    return $dataProvider;
  }

  private function initParams($params){
    $res = false;
    if(!empty($params['sorter'])){$this->sorter = $params['sorter'];$res = true;}
    if(!empty($params['capacity'])){$this->capacity = $params['capacity'];$res = true;}
    if(!empty($params['current'])){$this->current = $params['current'];$res = true;}
    if(!empty($params['polarity'])){$this->polarity = $params['polarity'];$res = true;}
    if(!empty($params['length'])){$this->length = $params['length'];$res = true;}
    if(!empty($params['width'])){$this->width = $params['width'];$res = true;}
    if(!empty($params['height'])){$this->height = $params['height'];$res = true;}
    if(!empty($params['country'])){$this->country = $params['country'];$res = true;}
    if(!empty($params['assurance'])){$this->assurance = $params['assurance'];$res = true;}
    if(!empty($params['voltage'])){$this->voltage = $params['voltage'];$res = true;}
    if($params['brend'] != ''){$this->brend = $params['brend'];$res = true;}
    return $res;
  }
}
