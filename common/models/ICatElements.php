<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "i_cat_elements".
 *
 * @property integer $id
 * @property integer $id_section
 * @property string $version
 * @property string $url
 * @property integer $active
 * @property integer $id_sort
 * @property integer $new
 * @property integer $spec
 * @property integer $top
 * @property string $name
 * @property integer $price
 * @property integer $price_old
 * @property string $image
 * @property string $image1
 * @property string $image2
 * @property string $image3
 * @property string $anounce
 * @property string $text
 * @property string $art
 * @property string $brand
 * @property string $title
 * @property string $keyw
 * @property string $descr
 * @property string $tire_brand
 * @property string $1c_code
 * @property string $tire_name_new
 * @property string $tire_load
 * @property string $tire_speed
 * @property string $tire_time
 * @property string $tire_ts
 * @property string $tire_width
 * @property string $tire_rf
 * @property string $tire_thorn
 * @property string $tire_param
 * @property string $tire_price
 * @property integer $tire_price1
 * @property string $tire_kol
 * @property string $tire_photo
 * @property string $tire_photo1
 * @property string $tire_photo2
 * @property string $tire_desc
 * @property string $disk_brand
 * @property string $1c_code_disk
 * @property string $disk_name
 * @property string $disk_width
 * @property string $disk_diam
 * @property string $disk_sver
 * @property string $disk_vilet
 * @property string $disk_color
 * @property string $disk_type
 * @property integer $disk_price
 * @property integer $disk_price1
 * @property integer $disk_kol
 * @property string $disk_img
 * @property string $disk_img1
 * @property string $disk_img2
 * @property string $disk_text
 * @property string $time_url
 * @property string $ts_url
 * @property integer $a_act
 * @property string $a_id
 * @property string $a_1c
 * @property string $a_brand
 * @property string $a_name
 * @property integer $a_kol
 * @property string $a_length
 * @property string $a_width
 * @property string $a_height
 * @property string $a_em
 * @property string $a_tok
 * @property string $a_pol
 * @property string $a_nap
 * @property string $a_gar
 * @property string $a_proiz
 * @property integer $a_price
 * @property integer $a_price1
 * @property string $a_img
 * @property string $a_img1
 * @property string $a_img2
 * @property string $a_text
 * @property integer $m_act
 * @property string $m_id
 * @property string $m_1c
 * @property string $m_brand
 * @property string $m_name
 * @property string $m_type
 * @property string $m_ob
 * @property string $m_vz1
 * @property string $m_vz2
 * @property integer $m_kol
 * @property string $m_dvig
 * @property integer $m_price
 * @property integer $m_price1
 * @property string $m_img
 * @property string $m_img1
 * @property string $m_img2
 * @property string $m_text
 */
class ICatElements extends \yii\db\ActiveRecord
{

    const SHINY = 0;
    const DISC = 1;
    const OIL = 2;
    const BATTERY = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'i_cat_elements';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_section', 'active', 'id_sort', 'new', 'spec', 'top', 'price', 'price_old', 'tire_price1', 'disk_price', 'disk_price1', 'disk_kol', 'a_act', 'a_kol', 'a_price', 'a_price1', 'm_act', 'm_kol', 'm_price', 'm_price1', 'tire_kol'], 'integer'],
            [['url', 'new'], 'required'],
            [['url', 'image', 'image1', 'image2', 'image3', 'anounce', 'text', 'brand', 'title', 'keyw', 'descr', 'tire_param', 'tire_photo', 'tire_photo1', 'tire_photo2', 'tire_desc', 'disk_img', 'disk_img1', 'disk_img2', 'disk_text', 'a_img', 'a_img1', 'a_img2', 'a_text', 'm_img', 'm_img1', 'm_img2', 'm_text'], 'string'],
            [['version'], 'string', 'max' => 20],
            [['name', 'art', 'tire_brand', '1c_code', 'tire_name_new', 'tire_load', 'tire_speed', 'tire_time', 'tire_ts', 'tire_width', 'tire_rf', 'tire_thorn', 'tire_price', 'disk_brand', '1c_code_disk', 'disk_name', 'disk_width', 'disk_diam', 'disk_sver', 'disk_vilet', 'disk_color', 'disk_type', 'time_url', 'ts_url', 'a_id', 'a_1c', 'a_brand', 'a_name', 'a_length', 'a_width', 'a_height', 'a_em', 'a_tok', 'a_pol', 'a_nap', 'a_gar', 'a_proiz', 'm_id', 'm_1c', 'm_brand', 'm_name', 'm_type', 'm_ob', 'm_vz1', 'm_vz2', 'm_dvig'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_section' => 'Id Section',
            'version' => 'Version',
            'url' => 'Url',
            'active' => 'Active',
            'id_sort' => 'Id Sort',
            'new' => 'New',
            'spec' => 'Spec',
            'top' => 'Top',
            'name' => 'Name',
            'price' => 'Price',
            'price_old' => 'Price Old',
            'image' => 'Image',
            'image1' => 'Image1',
            'image2' => 'Image2',
            'image3' => 'Image3',
            'anounce' => 'Anounce',
            'text' => 'Text',
            'art' => 'Art',
            'brand' => 'Brand',
            'title' => 'Title',
            'keyw' => 'Keyw',
            'descr' => 'Descr',
            'tire_brand' => 'Tire Brand',
            '1c_code' => '1c Code',
            'tire_name_new' => 'Tire Name New',
            'tire_load' => 'Tire Load',
            'tire_speed' => 'Tire Speed',
            'tire_time' => 'Tire Time',
            'tire_ts' => 'Tire Ts',
            'tire_width' => 'Tire Width',
            'tire_rf' => 'Tire Rf',
            'tire_thorn' => 'Tire Thorn',
            'tire_param' => 'Tire Param',
            'tire_price' => 'Tire Price',
            'tire_price1' => 'Tire Price1',
            'tire_kol' => 'Tire Kol',
            'tire_photo' => 'Tire Photo',
            'tire_photo1' => 'Tire Photo1',
            'tire_photo2' => 'Tire Photo2',
            'tire_desc' => 'Tire Desc',
            'disk_brand' => 'Disk Brand',
            '1c_code_disk' => '1c Code Disk',
            'disk_name' => 'Disk Name',
            'disk_width' => 'Disk Width',
            'disk_diam' => 'Disk Diam',
            'disk_sver' => 'Disk Sver',
            'disk_vilet' => 'Disk Vilet',
            'disk_color' => 'Disk Color',
            'disk_type' => 'Disk Type',
            'disk_price' => 'Disk Price',
            'disk_price1' => 'Disk Price1',
            'disk_kol' => 'Disk Kol',
            'disk_img' => 'Disk Img',
            'disk_img1' => 'Disk Img1',
            'disk_img2' => 'Disk Img2',
            'disk_text' => 'Disk Text',
            'time_url' => 'Time Url',
            'ts_url' => 'Ts Url',
            'a_act' => 'A Act',
            'a_id' => 'A ID',
            'a_1c' => 'A 1c',
            'a_brand' => 'A Brand',
            'a_name' => 'A Name',
            'a_kol' => 'A Kol',
            'a_length' => 'A Length',
            'a_width' => 'A Width',
            'a_height' => 'A Height',
            'a_em' => 'A Em',
            'a_tok' => 'A Tok',
            'a_pol' => 'A Pol',
            'a_nap' => 'A Nap',
            'a_gar' => 'A Gar',
            'a_proiz' => 'A Proiz',
            'a_price' => 'A Price',
            'a_price1' => 'A Price1',
            'a_img' => 'A Img',
            'a_img1' => 'A Img1',
            'a_img2' => 'A Img2',
            'a_text' => 'A Text',
            'm_act' => 'M Act',
            'm_id' => 'M ID',
            'm_1c' => 'M 1c',
            'm_brand' => 'M Brand',
            'm_name' => 'M Name',
            'm_type' => 'M Type',
            'm_ob' => 'M Ob',
            'm_vz1' => 'M Vz1',
            'm_vz2' => 'M Vz2',
            'm_kol' => 'M Kol',
            'm_dvig' => 'M Dvig',
            'm_price' => 'M Price',
            'm_price1' => 'M Price1',
            'm_img' => 'M Img',
            'm_img1' => 'M Img1',
            'm_img2' => 'M Img2',
            'm_text' => 'M Text',
        ];
    }

    public function getType() {
        if($this['1c_code']) {
            return self::SHINY;
        }
        if($this['1c_code_disk']) {
            return self::DISC;
        }
        if($this['m_1c']) {
            return self::OIL;
        }
        if($this['a_1c']) {
            return self::BATTERY;
        }
    }
    public static function translit($attr) {
        $search = ['кг.','км.','/4х4',' ','(',')','/','.','а','е','й','о','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
        $replace = ['kg','km','','-','','','-','-','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
        return str_replace($search, $replace, mb_strtolower(trim($attr)));
    }
    public function updateShiny($data) {
        foreach($data as $key => $value){
            switch($key){
                case 'СтараяЦена':
                $this->tire_price1 = $value;
                break;
                case 'Цена':
                $this->tire_price = $value;
                break;
                case '_id':
                $this['1c_code'] = $value;
                break;
                case 'picture':
                $this->tire_photo = 'none_pic.jpg';
                $img = @end(@explode('/', $value));
                $findme = ['.png', '.jpg', '.JPEG', '.jpeg', '.bmp', '.gif'];
                foreach($findme as $i => $t){
                    if(strpos($img, $t)) {
                        $this->tire_photo = $img; 
                    }
                }
                break;
                case 'model':
                break;
                case 'delivery':
                break;
                case 'Ostatatok':
                switch($value) {
                    case 'Нет в наличии':
                    $this->tire_kol = 0;
                    break;
                    case 'Больше 4':
                    $this->tire_kol = 5;
                    break;
                    default:
                    $this->tire_kol = $value;
                    break;
                }
                break;
                case 'param':
                foreach($value as $pkey => $pval){
                    switch($pval['_name']){
                        case 'Размер':
                        $this->tire_width = $pval['__text'];
                        break;
                        case 'Сезонность':
                        $this->tire_time = $pval['__text'];
                        break;
                        case 'Шип':
                        $this->tire_thorn = $pval['__text'];
                        break;
                        case 'Бренд':
                        $this->tire_brand = $pval['__text'];
                        break;
                        case 'Название':
                        $this->tire_name_new = $pval['__text'];
                        break;
                        case 'ИндексНагрузки':
                        $this->tire_load = $pval['__text'];
                        break;
                        case 'ИндексСкорости':
                        $this->tire_speed = $pval['__text'];
                        break;
                        case 'ТипТС':
                        $this->tire_ts = $pval['__text'];
                        break;
                        case 'RunFlat':
                        $this->tire_rf = $pval['__text'];
                        break;
                    }
                }
                break;
            }
        }
        $this->url = self::translit(trim($this->tire_brand))."-".self::translit(trim(str_replace('  ',' ',$this->tire_name_new)))."-".$this["1c_code"];
    }
    public function updateDisc($data) {
        foreach($data as $key => $value){
            switch($key){
                case 'СтараяЦена':
                $this->disk_price1 = $value;
                break;
                case 'Цена':
                $this->disk_price = $value;
                break;
                case '_id':
                $this['1c_code_disk'] = $value;
                break;
                case 'picture':
                $this->disk_img = 'none_pic.jpg';
                $img = @end(@explode('/', $value));
                $findme = ['.png', '.jpg', '.JPEG', '.jpeg', '.bmp', '.gif'];
                foreach($findme as $i => $t){
                    if(strpos($img, $t)) {
                        $this->disk_img = $img; 
                    }
                }
                break;
                case 'model':
                break;
                case 'delivery':
                break;
                case 'Ostatatok':
                switch($value) {
                    case 'Нет в наличии':
                    $this->disk_kol = 0;
                    break;
                    case 'Больше 4':
                    $this->disk_kol = 5;
                    break;
                    default:
                    $this->disk_kol = $value;
                    break;
                }
                break;
                case 'param':
                foreach($value as $pkey => $pval){
                    switch($pval['_name']){
                        case 'Размер':
                        $this->disk_width = $pval['__text'];
                        break;
                        case 'Сверловка':
                        $this->disk_sver = $pval['__text'];
                        break;
                        case 'ДиаметрСтупицы':
                        $this->disk_diam = $pval['__text'];
                        break;
                        case 'Производитель':
                        $this->disk_brand = $pval['__text'];
                        break;
                        case 'Название':
                        $this->disk_name = $pval['__text'];
                        break;
                        case 'Вылет':
                        $this->disk_vilet = $pval['__text'];
                        break;
                        case 'Тип':
                        $this->disk_type = $pval['__text'];
                        break;
                        case 'Цвет':
                        $this->disk_color = $pval['__text'];
                        break;
                    }
                }
                break;
            }
        }
        $this->url = self::translit(trim($this->disk_brand))."-".self::translit(trim(str_replace('  ',' ',$this->disk_name)))."-".$this["1c_code_disk"];
    }
    public function updateOil($data) {
        foreach($data as $key => $value){
            switch($key){
                case 'СтараяЦена':
                $this->m_price1 = $value;
                break;
                case 'Цена':
                $this->m_price = $value;
                break;
                case '_id':
                $this->m_1c = $value;
                break;
                case 'picture':
                $this->m_img = 'none_pic.jpg';
                $img = @end(@explode('/', $value));
                $findme = ['.png', '.jpg', '.JPEG', '.jpeg', '.bmp', '.gif'];
                foreach($findme as $i => $t){
                    if(strpos($img, $t)) {
                        $this->m_img = $img; 
                    }
                }
                break;
                case 'model':
                break;
                case 'delivery':
                break;
                case 'Ostatatok':
                switch($value) {
                    case 'Нет в наличии':
                    $this->m_kol = 0;
                    break;
                    case 'Больше 4':
                    $this->m_kol = 5;
                    break;
                    default:
                    $this->m_kol = $value;
                    break;
                }
                break;
                case 'param':
                foreach($value as $pkey => $pval){
                    switch($pval['_name']){
                        case 'Вязкость_1':
                        $this->m_vz1 = $pval['__text'];
                        break;
                        case 'Вязкость_2':
                        $this->m_vz2 = $pval['__text'];
                        break;
                        case 'ТипЖидкости':
                        $this->m_type = $pval['__text'];
                        break;
                        case 'Бренд':
                        $this->m_brand = $pval['__text'];
                        break;
                        case 'Название':
                        $this->m_name = $pval['__text'];
                        break;
                        case 'Объем':
                        $this->m_ob = $pval['__text'];
                        break;
                        case 'ТипДвигателя':
                        $this->m_dvig = $pval['__text'];
                        break;
                    }
                }
                break;
            }
        }
        $this->url = self::translit(trim($this->m_brand))."-".self::translit(trim(str_replace('  ',' ',$this->m_name)))."-".$this->m_1c;
    }
    public function updateBattery($data) {
        foreach($data as $key => $value){
            switch($key){
                case 'СтараяЦена':
                $this->m_price1 = $value;
                break;
                case 'Цена':
                $this->m_price = $value;
                break;
                case '_id':
                $this->a_1c = $value;
                break;
                case 'picture':
                $this->m_img = 'none_pic.jpg';
                $img = @end(@explode('/', $value));
                $findme = ['.png', '.jpg', '.JPEG', '.jpeg', '.bmp', '.gif'];
                foreach($findme as $i => $t){
                    if(strpos($img, $t)) {
                        $this->m_img = $img; 
                    }
                }
                break;
                case 'model':
                break;
                case 'delivery':
                break;
                case 'Ostatatok':
                switch($value) {
                    case 'Нет в наличии':
                    $this->m_kol = 0;
                    break;
                    case 'Больше 4':
                    $this->m_kol = 5;
                    break;
                    default:
                    $this->m_kol = $value;
                    break;
                }
                break;
                case 'param':
                foreach($value as $pkey => $pval){


                    switch($pval['_name']){
                        case 'Ёмкость':
                            $this->a_em = $pval['__text'];
                            break;
                        case 'СтартовыйТок':
                            $this->a_tok = $pval['__text'];
                            break;
                        case 'Полярность':
                            $this->a_pol = $pval['__text'];
                            break;
                        case 'Бренд':
                            $this->a_brand = $pval['__text'];
                            break;
                        case 'Название':
                            $this->a_name = $pval['__text'];
                            break;
                        case 'Длина':
                            $this->a_length = $pval['__text'];
                            break;
                        case 'Ширина':
                            $this->a_width = $pval['__text'];
                            break;
                        case 'Высота':
                            $this->a_height = $pval['__text'];
                            break;
                        case 'Страна':
                            $this->a_proiz = $pval['__text'];
                            break;
                        case 'Гарантия':
                            $this->a_gar = $pval['__text'];
                            break;
                        case 'Напряжение':
                            $this->a_nap = $pval['__text'];
                            break;
                        case 'Артикул':
                            $this->m_act = $pval['__text'];
                            break;
                    }
                }
                break;
            }
        }
        $this->url = self::translit(trim($this->a_brand))."-".self::translit(trim(str_replace('  ',' ',$this->a_name)))."-".$this->a_1c;
    }
}
