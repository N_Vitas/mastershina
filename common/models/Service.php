<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $title
 * @property string $type
 */
class Service extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'service';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['title', 'type'], 'required'],
      [['title', 'type'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'title' => 'Title',
      'type' => 'Type',
    ];
  }
  // Для связи детализации $model = Service::find()->innerjoinWith('detail')->all(); 
  public function getDetail(){
    return $this->hasMany(ServiceDetail::className(), ['service_id' => 'id']);
  }

  /**
   * @ Калюкуляция годового абонимента
   */
  public static function getAbonimentPrice(){
    $aboniment = [
      'disc'=>[
        ['title'=>'Готовый комплект (на дисках)','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Сезонное хранение легковые шины с диском','type'=>'Легковые автомобили','price'=>0],
        ['title'=>'Готовый комплект (на дисках)','type'=>'4х4, Джип, Минивэн','params'=>[]],
        ['title'=>'Сезонное хранение шины с дисками для внедорожников, SUV, минивэн','type'=>'4х4, Джип, Минивэн','price'=>0],
        ['title'=>'Полная стоимость за два сезона','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Полная стоимость за два сезона','type'=>'4х4, Джип, Минивэн','params'=>[]],
        ['title'=>'Стоимость годового абонимента с 20% скидкой','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Стоимость годового абонимента с 20% скидкой','type'=>'4х4, Джип, Минивэн','params'=>[]],
        ['title'=>'Ваша выгода','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Ваша выгода','type'=>'4х4, Джип, Минивэн','params'=>[]],
      ],
      'nodisc'=>[
        ['title'=>'Комплекс услуг шиномонтажа за 4 колеса','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Сезонное хранение легковые шины','type'=>'Легковые автомобили','price'=>0],
        ['title'=>'Комплекс услуг шиномонтажа за 4 колеса','type'=>'4х4, Джип, Минивэн','params'=>[]],
        ['title'=>'Сезонное хранение  шины для внедорожников, SUV, минивэн','type'=>'4х4, Джип, Минивэн','price'=>0],
        ['title'=>'Полная стоимость за два сезона','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Полная стоимость за два сезона','type'=>'4х4, Джип, Минивэн','params'=>[]],
        ['title'=>'Стоимость годового абонимента с 20% скидкой','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Стоимость годового абонимента с 20% скидкой','type'=>'4х4, Джип, Минивэн','params'=>[]],
        ['title'=>'Ваша выгода','type'=>'Легковые автомобили','params'=>[]],
        ['title'=>'Ваша выгода','type'=>'4х4, Джип, Минивэн','params'=>[]],
      ],
      'service'=>[
        ['title'=>'Выездной шиномонтаж без оплаты за выезд','price'=>2000],
        ['title'=>'Услуга отогрева или зарядки АКБ бесплатно','price'=>5000],
        ['title'=>'Заберем-доставим шины бесплатно','price'=>2000],
      ]
    ];

    $mon = ServiceDetail::find()->where(['in','service_id',[6,7]])->all(); 
    $bak = ServiceDetail::find()->where(['params'=>4])->all();
    foreach ($mon as $model) {
      if($model->service_id == 6){
        if($model->title == 'Легковые автомобили')
        $aboniment['nodisc'][0]['params'][]=['price'=>$model->price,'param'=>$model->params];
        else        
        $aboniment['nodisc'][2]['params'][]=['price'=>$model->price,'param'=>$model->params];      
      }
      if($model->service_id == 7){
        if($model->title == 'Легковые автомобили')
        $aboniment['disc'][0]['params'][]=['price'=>$model->price,'param'=>$model->params];
        else        
        $aboniment['disc'][2]['params'][]=['price'=>$model->price,'param'=>$model->params];        
      }
    }
    foreach ($bak as $model) {
      switch ($model->service_id){
        case 15:
          $aboniment['nodisc'][1]['price'] = $model->price;
          break;
        case 16:
          $aboniment['disc'][1]['price'] = $model->price;
          break;
        case 17:
          $aboniment['nodisc'][3]['price'] = $model->price;
          break;
        case 18:
          $aboniment['disc'][3]['price'] = $model->price;
          break;
      }
    }
    foreach ($aboniment['nodisc'][0]['params'] as $key => $a) {
      $serv = $aboniment['service'][0]['price'] + $aboniment['service'][1]['price'] + $aboniment['service'][2]['price'];
      $shiny = $a['price']*2;
      $dis = $aboniment['nodisc'][2]['params'][$key]['price']*2;
      $b = $aboniment['nodisc'][1]['price']*12;
      $bd = $aboniment['nodisc'][3]['price']*12;
      $total = $shiny+$b;
      $totaldisc = $dis+$bd;

      $aboniment['nodisc'][4]['params'][] = ['params'=>$a['param'],'price'=>$total];
      $aboniment['nodisc'][6]['params'][] = ['params'=>$a['param'],'price'=>$total-($total*20/100)];
      $aboniment['nodisc'][8]['params'][] = ['params'=>$a['param'],'price'=>($total*20/100)+$serv];

      $aboniment['nodisc'][5]['params'][] = ['params'=>$aboniment['nodisc'][2]['params'][$key]['param'],'price'=>$totaldisc];
      $aboniment['nodisc'][7]['params'][] = ['params'=>$aboniment['nodisc'][2]['params'][$key]['param'],'price'=>$totaldisc-($totaldisc*20/100)];
      $aboniment['nodisc'][9]['params'][] = ['params'=>$aboniment['nodisc'][2]['params'][$key]['param'],'price'=>($totaldisc*20/100)+$serv];
    }
    foreach ($aboniment['disc'][0]['params'] as $key => $a) {
      $serv = $aboniment['service'][0]['price'] + $aboniment['service'][1]['price'] + $aboniment['service'][2]['price'];
      $shiny = $a['price']*2;
      $dis = $aboniment['disc'][2]['params'][$key]['price']*2;
      $b = $aboniment['disc'][1]['price']*12;
      $bd = $aboniment['disc'][3]['price']*12;
      $total = $shiny+$b;
      $totaldisc = $dis+$bd;

      $aboniment['disc'][4]['params'][] = ['params'=>$a['param'],'price'=>$total];
      $aboniment['disc'][6]['params'][] = ['params'=>$a['param'],'price'=>$total-($total*20/100)];
      $aboniment['disc'][8]['params'][] = ['params'=>$a['param'],'price'=>($total*20/100)+$serv];

      $aboniment['disc'][5]['params'][] = ['params'=>$aboniment['disc'][2]['params'][$key]['param'],'price'=>$totaldisc];
      $aboniment['disc'][7]['params'][] = ['params'=>$aboniment['disc'][2]['params'][$key]['param'],'price'=>$totaldisc-($totaldisc*20/100)];
      $aboniment['disc'][9]['params'][] = ['params'=>$aboniment['disc'][2]['params'][$key]['param'],'price'=>($totaldisc*20/100)+$serv];
    }
    return $aboniment;
  }
}