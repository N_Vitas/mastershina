<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Shiny;
use common\models\Vehicle;

class SearchShiny extends Shiny
{
  // Добавляем автомобили в шины
  public $vendor,$car,$year,$modification;
  public $_vendor = [];
  public $_car = [];
  public $_year = [];
  public $_modification = [];
  // чтоб не делать дополнительные запросы подготовим наборы данных.
  public $_width = ['all'=>'Все','10-00'=>'10.00','11-00'=>'11.00','11-2'=>'11.2','12-00'=>'12.00','12-4'=>'12.4','12-5'=>'12.5','1200'=>'1200','1220'=>'1220','13-00'=>'13.00','13-6'=>'13.6','1300'=>'1300','135'=>'135','14-00'=>'14.00','14-9'=>'14.9','1400'=>'1400','145'=>'145','15-00'=>'15.00','15-5'=>'15.5','1500'=>'1500','155'=>'155','16-00'=>'16.00','16-5'=>'16.5','16-6'=>'16.6','16-9'=>'16.9','165'=>'165','17-5'=>'17.5','175'=>'175','18-00'=>'18.00','18-4'=>'18.4','185'=>'185','19-5'=>'19.5','195'=>'195','20-5'=>'20.5','205'=>'205','21-0'=>'21.0','21-3'=>'21.3','215'=>'215','225'=>'225','23-1'=>'23.1','23-5'=>'23.5','235'=>'235','2400'=>'2400','245'=>'245','255'=>'255','26-5'=>'26.5','265'=>'265','275'=>'275','28'=>'28','28-1'=>'28.1','285'=>'285','29-5'=>'29.5','295'=>'295','30'=>'30','30-5'=>'30.5','300'=>'300','305'=>'305','31'=>'31','315'=>'315','32'=>'32','325'=>'325','33'=>'33','335'=>'335','35'=>'35','360'=>'360','365'=>'365','37'=>'37','38'=>'38','385'=>'385','390'=>'390','4-00'=>'4.00','40'=>'40','400'=>'400','405'=>'405','42'=>'42','420'=>'420','425'=>'425','435'=>'435','445'=>'445','450'=>'450','480'=>'480','5-00'=>'5.00','500'=>'500','520'=>'520','530'=>'530','6-00'=>'6.00','6-25'=>'6.25','6-50'=>'6.50','600'=>'600','650'=>'650','7-00'=>'7.00','7-50'=>'7.50','700'=>'700','8-15'=>'8.15','8-25'=>'8.25','8-3'=>'8.3','800'=>'800','9-00'=>'9.00','9-5'=>'9.5'];

  public $_height = ['all'=>'Все','0'=>'0','10-5'=>'10.5','11-5'=>'11.5','12-5'=>'12.5','13-5'=>'13.5','14-5'=>'14.5','15'=>'15','20'=>'20','25'=>'25','30'=>'30','35'=>'35','40'=>'40','400'=>'400','45'=>'45','50'=>'50','500'=>'500','530'=>'530','55'=>'55','6-45'=>'6.45','60'=>'60','600'=>'600','65'=>'65','7'=>'7','70'=>'70','75'=>'75','8'=>'8','80'=>'80','82'=>'82','85'=>'85','9'=>'9','9-5'=>'9.5','90'=>'90','95'=>'95'];

  public $_radius = ['all'=>'Все','0'=>'0','10'=>'10','12'=>'12','12c'=>'12C','13'=>'13','13c'=>'13C','14'=>'14','14c'=>'14C','15'=>'15','15-3'=>'15.3','15c'=>'15C','16'=>'16','16-5'=>'16.5','16c'=>'16C','17'=>'17','17-5'=>'17.5','17c'=>'17C','18'=>'18','19'=>'19','19-5'=>'19.5','20'=>'20','21'=>'21','22'=>'22','22-5'=>'22.5','24'=>'24','25'=>'25','26'=>'26','28'=>'28','30'=>'30','32'=>'32','33'=>'33','34'=>'34','35'=>'35','38'=>'38','42'=>'42','508'=>'508','533'=>'533','635'=>'635','8'=>'8','9'=>'9','9-5'=>'9.5','r13'=>'13','r14'=>'14','r15'=>'15','r15c'=>'15C','r16'=>'16','r17'=>'17','r18'=>'18','r19'=>'19','r20'=>'20'];

  public $_season = [
  'all'=>'Все',
  'vsesezonnye'=>'Всесезонные',
  'zimnie'=>'Зимние',
  'letnie'=>'Летние'
  ];
  public $_loads = ['all'=>'Все','8-56-kg'=>'8 (56 кг.)','68-315-kg'=>'68 (315 кг.)','69-325-kg'=>'69 (325 кг.)','71-345-kg'=>'71 (345 кг.)','73-365-kg'=>'73 (365 кг.)','74-375-kg'=>'74 (375 кг.)','75-387-kg'=>'75 (387 кг.)','76-400-kg'=>'76 (400 кг.)','77-412--kg'=>'77 (412 кг.)','78-425-kg'=>'78 (425 кг.)','79-437---kg'=>'79 (437 кг.)','80-450-kg'=>'80 (450 кг.)','81-462-kg'=>'81 (462 кг.)','82-475-kg'=>'82 (475 кг.)','83-487--kg'=>'83 (487 кг.)','84-500-kg'=>'84 (500 кг.)','85-515-kg'=>'85 (515 кг.)','86-530-kg'=>'86 (530 кг.)','87-545-kg'=>'87 (545 кг.)','88-560-kg'=>'88 (560 кг.)','89-580-kg'=>'89 (580 кг.)','90-600-kg'=>'90 (600 кг.)','91-615-kg'=>'91 (615 кг.)','92-630-kg'=>'92 (630 кг.)','93-650--kg'=>'93 (650 кг.)','94-670-kg'=>'94 (670 кг.)','95-690---kg'=>'95 (690 кг.)','96-710-kg'=>'96 (710 кг.)','97-730-kg'=>'97 (730 кг.)','98-750-kg'=>'98 (750 кг.)','99-775-kg'=>'99 (775 кг.)','100-800-kg'=>'100 (800 кг.)','101-825-kg'=>'101 (825 кг.)','102-850-kg'=>'102 (850 кг.)','103-875-kg'=>'103 (875 кг.)','104-900-kg'=>'104 (900 кг.)','105-925-kg'=>'105 (925 кг.)','106-950-kg'=>'106 (950 кг.)','107-975-kg'=>'107 (975 кг.)','108-1000-kg'=>'108 (1000 кг.)','109-1030-kg'=>'109 (1030 кг.)','110-1060-kg'=>'110 (1060 кг.)','111-1090--kg'=>'111 (1090 кг.)','112-1120-kg'=>'112 (1120 кг.)','113-1150-kg'=>'113 (1150 кг.)','114-1180-kg'=>'114 (1180 кг.)','115-1215-kg'=>'115 (1215 кг.)','116-1250-kg'=>'116 (1250 кг.)','117-1285-kg'=>'117 (1285 кг.)','118-1320-kg'=>'118 (1320 кг.)','119-1360-kg'=>'119 (1360 кг.)','120-1400-kg'=>'120 (1400 кг.)','121-1450-kg'=>'121 (1450 кг.)','122-1500-kg'=>'122 (1500 кг.)','123-1550-kg'=>'123 (1550 кг.)','124-1600-kg'=>'124 (1600 кг.)','125-1650-kg'=>'125 (1650 кг.)','126-1700-kg'=>'126 (1700 кг.)','127-1750-kg'=>'127 (1750 кг.)','128-1800-kg'=>'128 (1800 кг.)','129-1850-kg'=>'129 (1850 кг.)','130-1900-kg'=>'130 (1900 кг.)','131-1950-kg'=>'131 (1950 кг.)','133-2060-kg'=>'133 (2060 кг.)','134-2120-kg'=>'134 (2120 кг.)','136-2240-kg'=>'136 (2240 кг.)','137-2300-kg'=>'137 (2300 кг.)','138-2360-kg'=>'138 (2360 кг.)','139-2430-kg'=>'139 (2430 кг.)','140-2500-kg'=>'140 (2500 кг.)','141-2575-kg'=>'141 (2575 кг.)','142-2650-kg'=>'142 (2650 кг.)','143-2725-kg'=>'143 (2725 кг.)','144-2800-kg'=>'144 (2800 кг.)','145-2900-kg'=>'145 (2900 кг.)','146-3000-kg'=>'146 (3000 кг.)','148-3150-kg'=>'148 (3150 кг.)','150-3350-kg'=>'150 (3350 кг.)','152-3550-kg'=>'152 (3550 кг.)','153-3650-kg'=>'153 (3650 кг.)','154-3750-kg'=>'154 (3750 кг.)','155-3875-kg'=>'155 (3875 кг.)','156-4000-kg'=>'156 (4000 кг.)','157-4125-kg'=>'157 (4125 кг.)','160-4500-kg'=>'160 (4500 кг.)','164-5000-kg'=>'164 (5000 кг.)','165-5150-kg'=>'165 (5150 кг.)','166-5300-kg'=>'166 (5300 кг.)','167-5450-kg'=>'167 (5450 кг.)','168-5600-kg'=>'168 (5600 кг.)','169-5800-kg'=>'169 (5800 кг.)','170-6000-kg'=>'170 (6000 кг.)','171-6150-kg'=>'171 (6150 кг.)','172-6300-kg'=>'172 (6300 кг.)','173-6500-kg'=>'173 (6500 кг.)','174-6700-kg'=>'174 (6700 кг.)','177-7300-kg'=>'177 (7300 кг.)','181-8250-kg'=>'181 (8250 кг.)','182-8500-kg'=>'182 (8500 кг.)','183-8750-kg'=>'183 (8750 кг.)','185-9250-kg'=>'185 (9250 кг.)','186-9500-kg'=>'186 (9500 кг.)','188-10000-kg'=>'188 (10000 кг.)','193-11500-kg'=>'193 (11500 кг.)','195-12150-kg'=>'195 (12150 кг.)'];
  public $_speed = [
    'a2-10-km'=>'A2 (10 км.)',
    'a3-15-km'=>'A3 (15 км.)',
    'a4-20-km'=>'A4 (20 км.)',
    'a5-25-km'=>'A5 (25 км.)',
    'a6-30-km'=>'A6 (30 км.)',
    'a7-35-km'=>'A7 (35 км.)',
    'a8-40-km'=>'A8 (40 км.)',
    'b-50-km'=>'B (50 км.)',
    'd-65-km'=>'D (65 км.)',
    'e-70-km'=>'E (70 км.)',
    'f-80-km'=>'F (80 км.)',
    'g-90-km'=>'G (90 км.)',
    'h-210-km'=>'H (210 км.)',
    'j-100-km'=>'J (100 км.)',
    'k-110-km'=>'K (110 км.)',
    'l-120-km'=>'L (120 км.)',
    'm-130-km'=>'M (130 км.)',
    'n-140-km'=>'N (140 км.)',
    'p-150-km'=>'P (150 км.)',
    'q-160-km'=>'Q (160 км.)',
    'q-160-km'=>'Q (160 км.)',
    'r-170-km'=>'R (170 км.)',
    's-180-km'=>'S (180 км.)',
    't-190-km'=>'T (190 км.)',
    'v-240-km'=>'V (240 км.)',
    'v-240-km'=>'V (240 км.)',
    'w-270-km'=>'W (270 км.)',
    'y-300-km'=>'Y (300 км.)',
    'y-300-km'=>'Y (300 км.)'
  ];
  public $_typeTC = [
    'all'=>'Все',
    'gruzovye'=>'Грузовой',
    'legkovye'=>'Легковой',
    'microavtobusnye'=>'Микроавтобусы',
  ];
  public $_sorter = ['title'=>'Названию','price'=>'Цене'];
  public $sorter,$brend;  
  public function rules()
  {
    return [
      [['sorter','size','height','radius','season','season','loads','speed','typeTC','vendor','car','year','modification','brend'],'save']
    ];
  }

  public function init(){
    $this->typeTC = 'legkovye';
    $this->_vendor = HelperVendor::getMake();
    $this->_car = HelperVendor::getModel();
    $this->_year = HelperVendor::getYear();
    $this->_modification = HelperVendor::getModification();
    return parent::init();
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'sorter'=>'Сортировать по',
      'vendor'=>'Марка',
      'car'=>'Модель',
      'year'=>'Год',
      'modification'=>'Модификация',
      'podship' => 'Под шип',
      'brend' => 'Бренд',
    ]);
  }
  public function getBrand(){
    $shiny = Shiny::find()->groupBy(['parent_brend_id'])->with('brend')->all();
    return ArrayHelper::getColumn($shiny,'brend.url');
  }
  public function getSort(){return $this->_sorter;}
  public function getWidth(){
    $where = $this->initSearch();
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['width'])->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('width')->all(),function($model){return $model->generateUrl($model->width);},'width');     
    }
    return $this->_width;}
  public function getHeight(){
    $where = $this->initSearch();
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['height'])->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('height')->all(),function($model){return $model->generateUrl($model->height);},'height');     
    } 
    return $this->_height;
  }
  public function getRadius(){
    $where = $this->initSearch();
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['radius'])->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('radius')->all(),function($model){			return $model->generateUrl($model->radius);},
      	function($model){
    			return str_replace('R', '', $model->radius);
    		});      
    } 
    return $this->_radius;
  }
  public function getSeason(){
    // $where = $this->initSearch();
    // if(count($where) > 0){
    //   $season['all'] = "Все";
    //   if($m = Shiny::find()->where($where)->groupBy('season')->all()){
    //     foreach ($m as $model) {
    //       if($model->season == ''){continue;} 
    //       $season[$model->generateUrl($model->season)] = $model->season;
    //     }
    //   }
    //   return $season;
    // }    
    return $this->_season;
  }
  public function getLoads(){
    $where = $this->initSearch();
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Shiny::find()->select(['loads'])->where($where)->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('loads')->all(),function($model){return $model->generateUrl($model->loads);},'loads');
    }    
    return $this->_loads;
  }
  public function getSpeed(){
    // $where = $this->initSearch();
    // if(count($where) > 0){
    //   $speed['all'] = "Все";
    //   if($m = Shiny::find()->where($where)->groupBy('speed')->all()){
    //     foreach ($m as $model) {
    //       if($model->speed == ''){continue;} 
    //       $speed[$model->generateUrl($model->speed)] = $model->speed;
    //     }
    //   }
    //   return $speed;
    // }    
    return $this->_speed;
  }
  public function getTypeTC(){
    // $where = $this->initSearch();
    // if(count($where) > 0){
    //   $typeTC['all'] = "Все";
    //   if($m = Shiny::find()->where($where)->groupBy('typeTC')->all()){
    //     foreach ($m as $model) {
    //       if($model->typeTC == ''){continue;} 
    //       $typeTC[$model->generateUrl($model->typeTC)] = $model->typeTC;
    //     }
    //   }
    //   return $typeTC;
    // }    
    return $this->_typeTC;
  }

  public function getVendor($where=[]){
    if(isset($where['vendor']))unset($where['vendor']);
    $this->_vendor = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['vendor'])->where($where)->groupBy('vendor')->all(),function($model){return $this->replaceKey($model,'vendor');},'vendor');
    return $this->_vendor;
  }
  public function getCar($where=[]){
    if(isset($where['car']))unset($where['car']);
    $this->_car = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['car'])->where($where)->groupBy('car')->all(),function($model){return $this->replaceKey($model,'car');},'car');
    return $this->_car;
  }
  public function getYear($where=[]){
    if(isset($where['year']))unset($where['year']);
    $this->_year = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),function($model){return $this->replaceKey($model,'year');},'year');
    return $this->_year;
    // return ['all'=>'Все',ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),'year','year')];
  }
  public function getModification($where=[]){
    if(isset($where['modification']))unset($where['modification']);
    $this->_modification = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['modification'])->where($where)->groupBy('modification')->all(),function($model){return $this->replaceKey($model,'modification');},'modification');
    return $this->_modification;
  }
  public function replaceKey($model,$key){
    $r = new Shiny();
    return $r->generateUrl($model->$key);
  }
  // public function scenarios()
  // {  
  //     return Model::scenarios();
  // }

  public function search($params) {

    $query = Shiny::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['price' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);

    // var_dump($this->brand,$params);die;
    if (/*$this->validate()*/!$this->initParams($params)) {
      $query->andFilterWhere(['ship'=>0]);
      return $dataProvider;
    }
    switch($this->sorter){
      case 'price':
      $query->orderBy(['price'=>SORT_ASC]);
      break;
      case 'title':
      $query->orderBy(['title'=>SORT_ASC]);
      break;
    }
    // if($this->size != 'all'){
    //   $query->andFilterWhere(['size' => $this->_size[$this->size]]);
    // }
    $auto=[];
    if($this->vendor != '' && $this->vendor != 'all'){$auto['vendor']=$this->_vendor[$this->vendor];}
    if($this->car != '' && $this->car != 'all'){$auto['car']=$this->_car[$this->car];}
    if($this->year != '' && $this->year != 'all'){$auto['year']=$this->_year[$this->year];}
    if($this->modification != '' && $this->modification != 'all'){$auto['modification']=$this->_modification[$this->modification];}
    if(count($auto)>0){
      $vehicle = Vehicle::find()->select(['tyres_factory'])->where($auto)->groupBy('tyres_factory')->asArray()->all();
      $size=[];
    	$a = [];
      $_w = [0];
      $_h = [0];
      $_r = [0];
      if(count($vehicle) > 0){
	      foreach ($vehicle as $key => $value) {
	        if($value['tyres_factory'] == ''){
	          continue;
	        }
	        $a = array_merge($a,explode("|", $value['tyres_factory']));
	        foreach ($a as $s) {       	
	        	$size = array_merge($size,explode(",",$s));
	        }
	      }
	      if(count($size)>0){
	      	$size = array_unique($size);
	      	foreach ($size as $obj) {
	      		$t = explode("/",$obj);
	      		$tm = explode(" ",$t[1]);
	      		$_w[] = $t[0];
			      $_h[] = $tm[0];
			      $_r[] = preg_replace("/[^\d]+/","",$tm[1]);
	      	}
	    		$_w = array_unique($_w);
	    		$_h = array_unique($_h);
	    		$_r = array_unique($_r);
	      }      	
      }      	
      $query->andFilterWhere(['in','width',$_w]);
      $query->andFilterWhere(['in','height',$_h]);
      $query->andFilterWhere(['in','radius',$_r]); 
    }

    if($this->runflat){
      $query->andFilterWhere(['runflat' => 1]);
    }
    if($this->ship){
      if($this->ship == 1){
        $query->andFilterWhere(['ship' => 1]);        
      }elseif($this->ship == 2){
        $query->andFilterWhere(['in','ship',[1,2]]);
      }elseif($this->ship == 3){
        $query->andFilterWhere(['ship'=>2]);
      }
    }else{
      $query->andFilterWhere(['ship' => 0]);
    }
    if($this->width != 'all'){
      $query->andFilterWhere(['width' => $this->_width[str_replace('shirina-','',$this->width)]]);
    }
    if($this->height != 'all'){
      $query->andFilterWhere(['height' => $this->_height[str_replace('vysota-','',$this->height)]]);
    }
    if($this->radius != 'all'){
      $query->andFilterWhere(['radius' => $this->_radius[str_replace('diametr-','',$this->radius)]]);
    }
    if($this->season != 'all'){
      $query->andFilterWhere(['season' => $this->_season[$this->season]]);
    }
    if($this->loads != 'all'){
      $query->andFilterWhere(['loads' => $this->_loads[str_replace('loads-','',$this->loads)]]);
    }
    if($this->brend != ''){
      $brend = Brend::find()->where(['url'=>str_replace('brend-','',$this->brend)])->one();
      $query->andFilterWhere(['parent_brend_id' => $brend->id]);
    }
    if(is_array($this->speed) && count($this->speed) > 0){
      foreach ($this->speed as $value) {
        if($value == '')continue;
        $w[] = $this->_speed[$value];        
      }
      $query->andFilterWhere(['speed' => $w]);//this->_speed[str_replace('speed-','',$this->speed)]]);
    }

    if($this->typeTC != 'all'){
    	switch ($this->typeTC) {
    		case 'gruzovye':
      		$query->andFilterWhere(['typeTC' => 'грузовые']);
    			break;
    		case 'legkovye':
      		$query->andFilterWhere(['typeTC' => 'легковые/4х4']);
    			break;
    		case 'microavtobusnye':
      		$query->andFilterWhere(['typeTC' => 'микроавтобусные']);
    			break;
    	}
    }
    // if(is_array($this->typeTC) && count($this->typeTC) > 0){
    //   foreach ($this->typeTC as $value) {
    //     $w[] = $this->_typeTC[$value];        
    //   }
    //   $query->andFilterWhere(['typeTC' => $w]);
    // }

    return $dataProvider;
  }
  private function initSearch(){
    $where = [];
    if($this->width != 'index' && $this->width != '' && $this->width != 'all'){$where['width']=$this->_width[$this->width];}
    if($this->height != 'index' && $this->height != '' && $this->height != 'all'){$where['height']=$this->_height[$this->height];}
    if($this->radius != 'index' && $this->radius != '' && $this->radius != 'all'){$where['radius']=$this->_radius[$this->radius];}
    if($this->season != 'index' && $this->season != '' && $this->season != 'all'){$where['season']=$this->_season[$this->season];}
    if($this->loads != 'index' && $this->loads != '' && $this->loads != 'all'){$where['loads']=$this->_loads[$this->loads];}
    // Поиск по типу
    if($this->typeTC != 'index' && $this->typeTC != '' && $this->typeTC != 'all'){
    	switch ($this->typeTC) {
    		case 'gruzovye':
      		$where['typeTC'] = 'грузовые';
    			break;
    		case 'legkovye':
      		$where['typeTC'] = 'легковые/4х4';
    			break;
    		case 'microavtobusnye':
      		$where['typeTC'] = 'микроавтобусные';
    			break;
    	}
    }
    // if($this->speed != 'index' && $this->speed != '' && $this->speed != 'all'){$where['speed']=$this->_speed[$this->speed];}
    // if(is_array($this->typeTC) && count($this->typeTC) > 0 ){$where['typeTC']=$this->_typeTC[$this->typeTC];}
    return $where;
  }
  private function initParams($params){
    $res = false;
    if($params['vendor'] != ''){$this->vendor = $params['vendor'];$res = true;}
    if($params['car'] != ''){$this->car = $params['car'];$res = true;}
    if($params['year'] != ''){$this->year = $params['year'];$res = true;}
    if($params['modification'] != ''){$this->modification = $params['modification'];$res = true;}
    if($params['brend'] != ''){$this->brend = $params['brend'];$res = true;}

    if($params['sorter'] != ''){$this->sorter = $params['sorter'];$res = true;}
    if($params['width'] != ''){$this->width = $params['width'];$res = true;}
    if($params['height'] != ''){$this->height = $params['height'];$res = true;}
    if($params['radius'] != ''){$this->radius = $params['radius'];$res = true;}
    if($params['price'] != ''){$this->price = $params['price'];$res = true;}
    if($params['season'] != ''){$this->season = $params['season'];$res = true;}
    if($params['loads'] != ''){$this->loads = $params['loads'];$res = true;}
    if($params['speed'] != ''){$this->speed = $params['speed'];$res = true;}
    if($params['typeTC'] != ''){$this->typeTC = $params['typeTC'];$res = true;}
    if($params['runflat']){$this->runflat = $params['runflat'];$res = true;}
    if($params['ship']){$this->ship = $params['ship'];$res = true;}
    return $res;
  }
}
