<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use common\models\User;
use common\models\OrderHistory;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $comment
 * @property string $delivery
 * @property string $payment
 * @property integer $price
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $data
 * @property integer $status
 * @property integer status_payment
 * @property integer status_delivery
 * @property string id_crm
 * @property string theme
 * @property string role
 * @property integer author
 * @property integer manager_crm
 */
class Order extends ActiveRecord
{
  const ORDER_CREATE = 0;
  const ORDER_DECORATED = 1;
  const ORDER_PROCESSED = 2;
  const ORDER_COMPLETED = 3;
  const ORDER_PAYMENTS = 4;
  const ORDER_RESERVE = 5;
  const ORDER = 6;
  const ORDER_ENTERED = 7;
  const ORDER_DELIVERY = 8;
  const ORDER_SALES = 9;
  const ORDER_OFFERT = 10;
  const ORDER_CANCELED = 11;
  public static $statuses = [
    'order' => [
      -1 => 'Выберите статус заказа',
      self::ORDER_CREATE => 'Создана',
      self::ORDER_DECORATED => 'В работе',
      self::ORDER_PROCESSED => 'Закрыта',
      self::ORDER_COMPLETED => 'Подбор товара',
      self::ORDER_PAYMENTS => 'Счет на оплату',
      self::ORDER_RESERVE => 'В резерве',
      self::ORDER => 'Заказ',
      self::ORDER_ENTERED => 'Поступил',
      self::ORDER_DELIVERY => 'На доставке',
      self::ORDER_SALES => 'Продано',
      self::ORDER_OFFERT => 'Коммерческое предложение',
      self::ORDER_CANCELED => 'Не обработано',
    ],
    'delivery' => [
      -1 => 'Выберите статус доставки',
      self::ORDER_CREATE => 'На оформлении',
      self::ORDER_DECORATED => 'Оформлен',
      self::ORDER_PROCESSED => 'В процессе',
      self::ORDER_COMPLETED => 'Доставлен',
      self::ORDER_CANCELED => 'Отменен',
    ],
    'payment' => [
      -1 => 'Выберите статус оплаты',
      self::ORDER_CREATE => 'Не оплачен',
      self::ORDER_DECORATED => 'Оформлен',
      self::ORDER_PROCESSED => 'В обработке',
      self::ORDER_COMPLETED => 'Оплачен',
      self::ORDER_CANCELED => 'Отменен',
    ],
  ];
  public static $status_style = [
    self::ORDER_CREATE => 'warning',
    self::ORDER_DECORATED => 'info',
    self::ORDER_PROCESSED => 'primary',
    self::ORDER_COMPLETED => 'success',
    self::ORDER_CANCELED => 'danger',
    self::ORDER_PAYMENTS  => 'primary',
    self::ORDER_RESERVE  => 'primary',
    self::ORDER  => 'primary',
    self::ORDER_ENTERED  => 'primary',
    self::ORDER_DELIVERY  => 'primary',
    self::ORDER_SALES  => 'primary',
    self::ORDER_OFFERT  => 'primary'
  ];

  public static $themes = [
    'Шины',
    'Диски',
    'Стационарный шиномонтаж',
    'Выездной шиномонтаж',
    'Сезонное хранение',
    'Бухгалтерия',
    'Аккумулятор',
    'в кредит и рассрочку консультация',
    'Вопросы не по существу',
    'Годовой абонемент в рассрочку',
    'Грузовые шины',
    'Диски и Шины',
    'Как проехать',
    'Консультация клиента',
    'Масла и жидкости',
    'Нет В наличии',
    'Переадресация на другого менеджера',
    'Повторный звонок',
    'СВП - система выравнивания плитки',
    'Служебный звонок',
    'Тендер',
    'Утилизация',
  ];
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'order';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name','phone', 'delivery', 'payment', 'data'], 'required'],
      [['price', 'created_at', 'updated_at', 'status','status_payment','status_delivery','author','manager_crm'], 'integer'],
      [['data'], 'string'],
      [['id_crm','role'], 'string', 'max' => 25],
      [['name', 'email', 'phone', 'delivery', 'payment','address', 'comment','theme'], 'string', 'max' => 255],
    ];
  }

  // public function afterFind()
  // {
  //     parent::afterFind();
  //     $this->data = json_decode($this->data);
  // }
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ]
    ];
  }
  public function afterSave($insert, $changedAttributes) {
    if($insert){
      $client = Client::find()->where(['phone' => $this->phone])->one();
      $history = new OrderHistory();
      $history->order_id = $this->id;
      $history->type = OrderHistory::CREATE;
      $history->data_old = OrderHistory::CREATE;
      $history->data_new = json_encode($this->attributes,JSON_UNESCAPED_UNICODE);
      $history->save();
      if($client){
        $json = json_decode($client->data,true);
        $json['order_id'] = $json['order_id'].",".$this->id;
        if($client->phone != $this->phone){
          $json['phone'][$client->phone] = $client->phone;
          $client->phone = $this->phone;
        }
        $client->data = json_encode($json,JSON_UNESCAPED_UNICODE);
        $client->save();
      }else{
        $client = new Client();
        $client->name = $this->name;
        $client->email = $this->email;
        $client->phone = $this->phone;
        $client->address = $this->address;
        $client->manager = $this->role;
        $client->data = json_encode(['order_id' => $this->id],JSON_UNESCAPED_UNICODE);
        $client->save();
      }
      $this->exportCrm();
    }else{
      $history = new OrderHistory();
      $history->order_id = $this->id;
      $history->type = OrderHistory::CHANGE;
      $history->data_old = json_encode($changedAttributes,JSON_UNESCAPED_UNICODE);
      $history->data_new = json_encode($this->attributes,JSON_UNESCAPED_UNICODE);
      $history->save();      
    }
    return parent::afterSave($insert, $changedAttributes);
  } 
  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Имя',
      'email' => 'Email',
      'phone' => 'Телефон',
      'address' => 'Адрес',
      'comment' => 'Комментарий',
      'delivery' => 'Доставка',
      'payment' => 'Платеж',
      'price' => 'Итого',
      'created_at' => 'Дата создания',
      'updated_at' => 'Дата обновления',
      'data' => 'Товары',
      'status' => 'Статус заказа',
      'status_payment'=>'Статус оплаты',
      'status_delivery'=>'Статус доставки',
      'id_crm'=>'ID CRM',
      'theme'=>'Тема заявки',
      'role'=>'Роль автора',
      'author'=>'Автор заявки',
      'manager_crm'=>'Ответственный',
      'firstname'=>'Фамилия',
      'lastname'=>'Имя',
      'secondname'=>'Отчество',
      'username'=>'Логин',
    ];
  }

  public function getUser(){
    return $this->hasOne(User::className(), ['id' => 'author']);
  }
  public function getManager(){
    return $this->hasOne(User::className(), ['id' => 'manager_crm']);
  }
  public static function getCategoryName($position){
    $name = explode("-", $position);
    switch ($name[0]) {
      case 'shiny':
        return "Шины";
      case 'disc':
        return "Диски";
      case 'oil':
        return "Масло";
      case 'battery':
        return "Аккумулятор";    
      default:
        return "Шины";
    }
  }
  private function exportCrm(){
    $model = self::find()->where(['id'=>$this->id])->one();
    $model->comment = json_decode($model->comment,true);
    $model->data = json_decode($model->data,true);
    $model->status = self::$statuses['order'][$model->status];
    $model->author = $model->user->id_crm;
    $model->manager_crm = $model->manager->id_crm;
    if(yii::$app->params["production"]){
      $exchange = 'exchange';
      $queue = 'queue';      
    }else{      
      $exchange = 'test-exchange';
      $queue = 'test-queue';
    }
    $message = serialize($model->attributes);
    Yii::$app->amqp->declareExchange($exchange, $type = 'direct', $passive = false, $durable = true, $auto_delete = false);
    Yii::$app->amqp->declareQueue($queue, $passive = false, $durable = true, $exclusive = false, $auto_delete = false);
    Yii::$app->amqp->bindQueueExchanger($queue, $exchange, $routingKey = $queue);
    Yii::$app->amqp->publish_message($message, $exchange, $routingKey = $queue, $content_type = 'applications/json', $app_id = Yii::$app->name);
  }
}
