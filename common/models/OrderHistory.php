<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use common\models\User;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $type
 * @property string $data_old
 * @property string $data_new
 * @property integer $created_at
 * @property integer $updated_at
 */
class OrderHistory extends ActiveRecord
{
  const CREATE = "Создание";
  const CHANGE = "Изменение";
  const DELETE = "Удаление";
  const EXPORT = "Экспотр в CRM";
  const IMPORT = "Импорт из CRM";
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'order_history';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['order_id','type','data_old','data_new'], 'required'],
      [['order_id'], 'integer'],
      [['type','data_old','data_new'], 'string'],
    ];
  }

  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'order_id' => 'Имя',
      'type' => 'Email',
      'data_old' => 'Телефон',
      'data_new' => 'Адрес',
      'created_at' => 'Дата создания',
      'updated_at' => 'Дата обновления',
    ];
  }

  public function getOrder(){
    return $this->hasOne(User::className(), ['id' => 'order_id']);
  }
}
