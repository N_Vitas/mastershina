<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use common\models\Description;

/**
 * This is the model class for table "shiny".
 *
 * @property integer $id
 * @property integer $parent_brend_id
 * @property integer $parent_auto_id
 * @property string $title
 * @property integer $price
 * @property integer $old_price
 * @property string $currency
 * @property string $model
 * @property integer $delivery
 * @property string $ostatok
 * @property string $size
 * @property string $width
 * @property string $height
 * @property string $radius
 * @property string $season
 * @property string $picture
 * @property string $loads
 * @property string $speed
 * @property string $typeTC
 * @property integer $runflat
 * @property integer $ship
 * @property integer $status
 * @property string $code
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Shiny extends \yii\db\ActiveRecord implements CartPositionInterface
{
  use CartPositionTrait;
  /**
   * @getId для корзины
   */

  public function getId()
  {
    return 'shiny-'.$this->id;
  }
  public function getPrice(){
    return $this->price;
  }
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'shiny';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['parent_brend_id', 'title', 'ostatok', 'season','width', 'height', 'radius', 'picture', 'typeTC', 'code'], 'required'],
      [['parent_brend_id', 'parent_auto_id', 'price', 'old_price', 'delivery', 'runflat', 'ship', 'status'], 'integer'],
      [['model','size', 'radius','description'], 'string'],
      [['width', 'height'],'safe'],
      [['title', 'picture', 'loads', 'speed', 'typeTC', 'url', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
      [['currency'], 'string', 'max' => 10],
      [['ostatok', 'size', 'season', 'code'], 'string', 'max' => 25],
    ];
  }

  // public function behaviors()
  // {
  //   $behaviors = parent::behaviors();
    
  //   $behaviors['slug'] = [
  //     'class' => SluggableBehavior::className(),
  //     // 'attribute' => 'title',
  //     'immutable' => true,
  //     'ensureUnique' => true,
  //     'slugAttribute' => 'url',
  //     'value' => function($event){
  //       return $this->generateUrl($event->sender->title."-".$event->sender->season."-".$event->sender->size);
  //     },
  //   ];
  //   return $behaviors;
  // }

  public function generateUrl($attr){
    $search = ['кг.','км.','/4х4',' ','(',')','/','.','а','е','й','о','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
    $replace = ['kg','km','','-','','','-','-','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
    return str_replace($search, $replace, mb_strtolower(trim($attr)));
  }
  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'parent_brend_id' => 'Бренд',
      'parent_auto_id' => 'Авто',
      'title' => 'Заголовок',
      'price' => 'Цена',
      'old_price' => 'Старая Цена',
      'currency' => 'Валюта',
      'brend' => 'Бренд',
      'model' => 'Модель',
      'delivery' => 'Доставка',
      'ostatok' => 'В наличии',
      'size' => 'Размер',
      'width' => 'Ширина',
      'height' => 'Высота',
      'radius' => 'Диаметр',
      'season' => 'Сезон',
      'picture' => 'Изображение',
      'loads' => 'Индекс нагрузки',
      'speed' => 'Индекс скорости',
      'typeTC' => 'Тип авто',
      'runflat' => 'Беспрокольные',
      'ship' => 'Шипованные',
      'status' => 'Статус',
      'code' => 'Код',
      'url' => 'Url адрес',
      'meta_title' => 'Сео заголовок',
      'meta_keywords' => 'Сео теги',
      'meta_description' => 'Сео описание',
      'description' => 'Описание',
    ];
  }


  public function getPicture(){
    $picture =  substr(strrchr($this->picture, '.'), 1);
    $picture = $picture != '' ? $this->picture : '/upload/none_pic.jpg';
    return $picture;
  }

  public function getShipTitle(){
    switch($this->ship){
      case 0: return "Нет";
      case 1: return "Да";
      case 2: return "Под шипы";
    }
  }
  
  public function getBrend(){
    return $this->hasOne(Brend::className(), ['id' => 'parent_brend_id']);
  }
  public function getDescription(){
    return $this->hasOne(Description::className(), ['code' => 'code']);
  }

  public function loadPitstop($model) {
    if($brend = Brend::find()->where(['title' => $model->tire_brand])->one()){
      $this->parent_brend_id = $brend->id;
    } else {
      $brend = new Brend();
      $brend->title = $model->tire_brand;
      $brend->url = $brend->generateUrl($model->tire_brand);
      $brend->save();
      $this->parent_brend_id = $brend->id;
    }
    $this->currency = 'KZT';
    $this->title = (string) $model->tire_name_new;
    $this->price = $model->tire_price;
    $this->old_price = $model->tire_price1;
    $this->model = $model->tire_name_new;
    $this->delivery = 1;
    $this->ostatok = (string) $model->tire_kol;
    $this->size = $model->tire_width;
    $radius = preg_split("/[\s]+/", $model->tire_width);
    $width = preg_split("/[\s]?[\/]+/", $radius[0]);
    $height = $width[1] || 0;
    $this->width = $width[0];
    $this->height = $height;
    $this->radius = $radius[1];
    $this->season = $model->tire_time;
    $this->picture = '/upload/'.$model->tire_photo;
    $this->loads = (string) $model->tire_load || $this->loads;
    $this->speed = (string) $model->tire_speed || $this->speed;
    $this->typeTC = (string) $model->tire_ts || $this->typeTC;
    $this->runflat = $model->tire_rf == 'нет'? 0 : 1;
    $this->ship = $model->tire_thorn == 'нет'? 0 : 1;
    $this->status = 1;
    $this->code = $model['1c_code'];
    $this->url = $this->generateUrl($model->tire_brand." ".$this->title." ".$this->code);
    // var_dump($this);die;
  }
  // public function getAuto(){
  //   return $this->hasOne(Auto::className(), ['id' => 'parent_auto_id']);
  // }
}
