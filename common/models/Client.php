<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $comment
 * @property string $manager
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $data
 * @property integer $status
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }
    
    public function behaviors()
      {
        return [
          [
            'class' => TimestampBehavior::className(),
            'attributes' => [
              ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
              ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
            ],
          ]
        ];
      }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'manager'], 'required'],
            [['email'], 'unique'],
            [['created_at', 'updated_at', 'status','bin','iin','export'], 'integer'],
            [['data','phone','address'], 'string'],
            [['name', 'email',  'comment', 'manager','type'], 'string', 'max' => 255],
        ];
    }
    
    // public function afterFind()
    // {
    //     parent::afterFind();
    //     $this->data = json_decode($this->data);
    // }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'comment' => 'Comment',
            'manager' => 'Manager',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'data' => 'Data',
            'status' => 'Status',
            'bin' => 'Бин',
            'type' => 'Тип', 
            'iin' => 'ИИН', 
            'export' => 'Экспорт'
        ];
    }
}
