<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "disc".
 *
 * @property integer $id
 * @property integer $parent_brend_id
 * @property integer $parent_auto_id
 * @property string $title
 * @property integer $price
 * @property integer $old_price
 * @property string $currency
 * @property string $picture
 * @property string $model
 * @property integer $delivery
 * @property string $ostatok
 * @property string $size
 * @property string $borer
 * @property double $stupica
 * @property integer $radius
 * @property string $color
 * @property string $type
 * @property string $manufacturer
 * @property integer $status
 * @property string $code
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Disc extends \yii\db\ActiveRecord implements CartPositionInterface
{
  use CartPositionTrait;
  /**
   * @getId для корзины
   */

  public function getId()
  {
    return 'disc-'.$this->id;
  }
  public function getPrice(){
    return $this->price;
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'disc';
  }
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      // [['title', 'picture', 'ostatok', 'size', 'borer', 'stupica', 'radius', 'color', 'type', 'manufacturer', 'code'], 'required'],
      [['parent_brend_id', 'parent_auto_id', 'price', 'old_price', 'delivery', 'status'], 'integer'],
      [['model','description'], 'string'],
      [['title', 'picture', 'color', 'type', 'manufacturer', 'url', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
      [['currency'], 'string', 'max' => 10],
      [['radius','stupica'], 'safe'],
      [['ostatok', 'size', 'borer', 'code'], 'string', 'max' => 25],
    ];
  }
  
  // public function behaviors()
  // {
  //   return [
  //     [
  //       'class' => SluggableBehavior::className(),
  //       // 'attribute' => 'title',
  //       'immutable' => true,
  //       'ensureUnique' => true,
  //       'slugAttribute' => 'url',
  //       'value' => function($event){
  //         $search = [' ','(',')','/','.','а','e','й','o','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
  //         $replace = ['-','','','-','','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
  //         return trim(str_replace($search, $replace, mb_strtolower($event->sender->title)));
  //       },
  //     ],
  //   ];
  // }
  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'parent_brend_id' => 'Бренд',
      'parent_auto_id' => 'Авто',
      'price' => 'Цена',
      'old_price' => 'Старая Цена',
      'currency' => 'Валюта',
      'model' => 'Наименование',
      'picture' => 'Изображение',
      'delivery' => 'Доставка',
      'ostatok' => 'В наличии',
      'status' => 'Статус',
      'title' => 'Заголовок',
      'size' => 'Размер (радиус х ширина диска)',
      'borer' => 'Сверловка (кол-во х расстояние отверстий)',
      'stupica' => 'Диаметр ступицы',
      'radius' => 'Вылет',
      'color' => 'Цвет',
      'type' => 'Тип',
      'manufacturer' => 'Бренд',
      'code' => 'Код',
      'url' => 'Url адрес',
      'meta_title' => 'Сео заголовок',
      'meta_keywords' => 'Сео теги',
      'meta_description' => 'Сео описание',
      'description' => 'Описание',
    ];
  }

  public function getPicture(){
    $picture =  substr(strrchr($this->picture, '.'), 1);
    $picture = $picture != '' ? $this->picture : '/upload/none_pic.jpg';
    return $picture;
  }

  public function generateUrl($attr){
    $search = [',','кг.','км.','/4х4',' ','(',')','/','.','а','е','й','о','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
    $replace = ['','kg','km','','-','','','-','-','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
    return str_replace($search, $replace, mb_strtolower(trim($attr)));
  }
  
  public function getBrend(){
    return $this->hasOne(Brend::className(), ['id' => 'parent_brend_id']);
  }

  public function getDescription(){
    return $this->hasOne(Description::className(), ['code' => 'code']);
  }
  public function loadPitstop($model) {
    if($brend = Brend::find()->where(['title' => $model->disk_brand])->one()){
      $this->parent_brend_id = $brend->id;
    } else {
      $brend = new Brend();
      $brend->title = $model->disk_brand;
      $brend->url = $brend->generateUrl($model->disk_brand);
      $brend->save();
      $this->parent_brend_id = $brend->id;
    }
    $this->price = $model->disk_price;
    $this->old_price = $model->disk_price1;
    $this->currency = 'KZT';
    $this->model = $model->disk_brand;
    $this->picture = $model->disk_img;
    $this->delivery = 1;
    $this->ostatok = $model->disk_kol;
    $this->status = 1;
    $this->title = $model->disk_name;
    $this->size = $model->disk_width;
    $this->borer = $model->disk_sver;
    $this->stupica = $model->disk_diam;
    $this->radius = $model->disk_vilet;
    $this->color = $model->disk_color;
    $this->type = $model->disk_type;
    $this->manufacturer = $model->disk_brand;
    $this->code = $model['1c_code_disk'];
    $this->url = $this->generateUrl($model->disk_brand." ".$this->title." ".$this->code);
  }
  // public function getAuto(){
  //   return $this->hasOne(Auto::className(), ['id' => 'parent_auto_id']);
  // }
}
