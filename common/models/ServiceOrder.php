<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "service_order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $params
 * @property string $type
 * @property integer $created_at
 * @property integer $status
 */
class ServiceOrder extends ActiveRecord implements CartPositionInterface
{
  use CartPositionTrait;
  /**
   * @getId для корзины
   * @getPrice для корзины
   * $url картинка для корзины
   * $old_price для корзины
   * $ostatok для корзины
   */

  public function getId()
  {
    return 'service-'.$this->id;
  }
  public function getPrice()
  {
    return 0;
  }

  const SERVICE_ORDER_CREATE = 0;
  const SERVICE_ORDER_COMPLETED = 1;

  public static $statuses = [
    self::SERVICE_ORDER_CREATE => 'Создана',
    self::SERVICE_ORDER_COMPLETED => 'Оформлена',
  ];
  public $picture = '/images/tires-hranenie-tolko-shini.jpg';
  public $old_price = '0';
  public $ostatok = '0';

  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'attributes' => [ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']],
      ]
    ];
  }
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'service_order';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id', 'title', 'params', 'type'], 'required'],
      [['user_id', 'created_at', 'status'], 'integer'],
      [['title', 'params', 'type','picture','old_price','ostatok'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'user_id' => 'User ID',
      'title' => 'Title',
      'params' => 'Params',
      'type' => 'Type',
      'created_at' => 'Created At',
      'status' => 'Status',
    ];
  }
}
