<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\ICatElements;
use common\models\Shiny;
use common\models\Oil;
use common\models\Disc;
use common\models\Battery;

class SearchApiDataProvider extends ICatElements
{
  public $shiny;
  public $disc;
  public $oil;
  public $battery;
  public function rules()
  {
    return [
      [['shiny','disc','oil','battery'],'save'],
      [['id_section', 'active', 'id_sort', 'new', 'spec', 'top', 'price', 'price_old', 'tire_price1', 'disk_price', 'disk_price1', 'disk_kol', 'a_act', 'a_kol', 'a_price', 'a_price1', 'm_act', 'm_kol', 'm_price', 'm_price1', 'tire_kol'], 'integer'],
      [['url', 'new'], 'required'],
      [['url', 'image', 'image1', 'image2', 'image3', 'anounce', 'text', 'brand', 'title', 'keyw', 'descr', 'tire_param', 'tire_photo', 'tire_photo1', 'tire_photo2', 'tire_desc', 'disk_img', 'disk_img1', 'disk_img2', 'disk_text', 'a_img', 'a_img1', 'a_img2', 'a_text', 'm_img', 'm_img1', 'm_img2', 'm_text'], 'string'],
      [['version'], 'string', 'max' => 20],
      [['name', 'art', 'tire_brand', '1c_code', 'tire_name_new', 'tire_load', 'tire_speed', 'tire_time', 'tire_ts', 'tire_width', 'tire_rf', 'tire_thorn', 'tire_price', 'disk_brand', '1c_code_disk', 'disk_name', 'disk_width', 'disk_diam', 'disk_sver', 'disk_vilet', 'disk_color', 'disk_type', 'time_url', 'ts_url', 'a_id', 'a_1c', 'a_brand', 'a_name', 'a_length', 'a_width', 'a_height', 'a_em', 'a_tok', 'a_pol', 'a_nap', 'a_gar', 'a_proiz', 'm_id', 'm_1c', 'm_brand', 'm_name', 'm_type', 'm_ob', 'm_vz1', 'm_vz2', 'm_dvig'], 'string', 'max' => 250],
    ];
  }
  public function init(){
    return parent::init();
  }
  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'shiny' => 'Шины',
      'disc' => 'Диски',
      'oil' => 'Масла',
      'battery' => 'Аккумуляторы.',
    ]);
  }
  // public function getCustom($models){
  //   $provider = [];
  //   foreach($models as $model){
  //     if($model['1c_code']){
  //       $provider['shiny'][] = trim($model['1c_code']);
  //     }
  //     if($model['1c_code_disk']){
  //       $provider['disc'][] = trim($model['1c_code_disk']);
  //     }
  //     if($model->a_1c){
  //       $provider['battery'][] = trim($model->a_1c);
  //     }
  //     if($model->m_1c){
  //       $provider['oil'][] = trim($model->m_1c);
  //     }
  //   }
  //   if(count($provider['shiny']) > 0){
  //     $provider['shiny'] = Shiny::find()->where(['in','code',$model['1c_code']])->asArray()->all();
  //   }
  //   if(count($provider['disc']) > 0){
  //     var_dump($provider['disc']);
  //     $provider['disc'] = Disc::find()->where(['like','code',$model['1c_code_disk']])->asArray()->all();
  //     var_dump($provider['disc']);die;
  //   }
  //   if(count($provider['battery']) > 0){
  //     $provider['battery'] = Oil::find()->where(['in','code',$model->a_1c])->asArray()->all();
  //   }
  //   if(count($provider['oil']) > 0){
  //     $provider['oil'] = Battery::find()->where(['in','code',$model->m_1c])->asArray()->all();
  //   }

  //   return $provider;
  // }

  public function search($params) {
    $limit = 10;
    if((int) $params['limit'] > 0){
      $limit = $params['limit'];
    }
    $query = self::find();
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => $limit,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['url' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    // собираем масив датапровайдера
    
    if (!$this->load($params)) {
        return $dataProvider;
    }
    
    if($this->shiny){
      $query->where(['!=','1c_code','']);
    }
    if($this->disc){
      $query->where(['!=','1c_code_disk','']);
    }
    if($this->oil){
      $query->where(['!=','m_1c','']);
    }
    if($this->battery){
      $query->where(['!=','a_1c','']);
    }
    
    foreach($this->getAttributes() as $key=> $value){
      if($value){
        $query->andFilterWhere(['like',$key,$value]);
      }
    }

    return $dataProvider;
  }
}
