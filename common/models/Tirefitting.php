<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tirefitting".
 *
 * @property integer $id
 * @property string $time
 * @property string $data
 */
class Tirefitting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tirefitting';
    }
    public function afterFind()
    {
        $this->data = json_decode($this->data,true);
        parent::afterFind();
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'safe'],
            [['data'], 'required'],
            [['data'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'data' => 'Data',
        ];
    }
}
