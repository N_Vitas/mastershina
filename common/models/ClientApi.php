<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $comment
 * @property string $manager
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $data
 * @property integer $status
 */
class ClientApi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }
    
    public function behaviors()
      {
        return [
          [
            'class' => TimestampBehavior::className(),
            'attributes' => [
              ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
              ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
            ],
          ]
        ];
      }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'manager'], 'required'],
            [['created_at', 'updated_at', 'status','bin','iin','export'], 'integer'],
            [['data','contacts','tc'], 'safe'],
            [['name', 'email', 'phone', 'comment', 'manager','type','client_id','tiresinstorage'], 'string', 'max' => 255],
            [['address'], 'string'],
        ];
    }
    
    public function afterFind()
    {
        $this->data = json_decode($this->data);
        $this->tc = json_decode($this->tc);
        $this->contacts = json_decode($this->contacts);
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
      $this->data = json_encode($this->data,JSON_UNESCAPED_UNICODE);
      return parent::beforeSave($insert);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'comment' => 'Comment',
            'manager' => 'Manager',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'data' => 'Data',
            'status' => 'Status',
            'bin' => 'Бин',
            'type' => 'Тип', 
            'iin' => 'ИИН', 
            'export' => 'Экспорт',
            'client_id'=>'ID CRM',
            'tiresinstorage'=>'На хранении',
            'contacts'=>'Контакты',
            'tc'=>'Автомобили',
        ];
    }
}
