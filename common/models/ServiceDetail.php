<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_detail".
 *
 * @property integer $id
 * @property integer $service_id
 * @property string $title
 * @property string $type
 * @property string $params
 * @property string $unit
 * @property integer $price
 */
class ServiceDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id'], 'required'],
            [['service_id', 'price'], 'integer'],
            [['title', 'type', 'params', 'unit'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'title' => 'Title',
            'type' => 'Type',
            'params' => 'Params',
            'unit' => 'Unit',
            'price' => 'Price',
        ];
    }
}
