<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Oil;

class SearchOil extends Oil
{
  public $_size = ['all'=>'Все','100gr'=>'100гр','100ml'=>'100мл','10gr'=>'10гр','10l'=>'10л','10ml'=>'10мл','12-5gr'=>'12.5гр','125ml'=>'125мл','150ml'=>'150мл','15l'=>'15л','180kg'=>'180кг','1kg'=>'1кг','1l'=>'1л','200l'=>'200л','200ml'=>'200мл','205l'=>'205л','20gr'=>'20гр','20l'=>'20л','250gr'=>'250гр','250ml'=>'250мл','25kg'=>'25кг','300l'=>'300л','300ml'=>'300мл','30ml'=>'30мл','330ml'=>'330мл','400gr'=>'400гр','400ml'=>'400мл','450ml'=>'450мл','4l'=>'4л','500l'=>'500л','500ml'=>'500мл','50ml'=>'50мл','5kg'=>'5кг','5l'=>'5л','600ml'=>'600мл','60l'=>'60л','75ml'=>'75мл','910gr'=>'910гр'];
  public $_viscosity_1 = ['all'=>'Все','0'=>'0','10'=>'10','15'=>'15','1100'=>'1 100','1200'=>'1 200','1300'=>'1 300','1400'=>'1 400','1600'=>'1 600','1700'=>'1 700','1800'=>'1 800','20'=>'20','5'=>'5','75'=>'75','750'=>'750','80'=>'80','85'=>'85'];
  public $_type_fluid = ['all'=>'Все','antigel-v-diztoplivo'=>'Антигель в дизтопливо','antifriz'=>'Антифриз','antifriktsionnaya-prisadka'=>'Антифрикционная присадка','antifriktsionnaya-prisadka-v-transmissionnoe-maslo'=>'Антифрикционная присадка в трансмиссионное масло','biomaslo-dlya-tsepey-benzopil'=>'Биомасло для цепей бензопил','dizelnyy-antigel'=>'Дизельный антигель','kanistra'=>'Канистра','kompressornoe-maslo'=>'Компрессорное масло','konsistentnaya-smazka'=>'Консистентная смазка','maslo'=>'Масло','maslo-gidravlicheskoe'=>'Масло гидравлическое','maslo-dlya-motovilok-i-amortizatorov'=>'Масло для мотовилок и амортизаторов','maslo-motornoe'=>'Масло моторное','motornoe-maslo'=>'Моторное масло','motornoe-maslo-dlya-mototsikla'=>'Моторное масло для мотоцикла','nezamerzayushchaya-zhidkost'=>'Незамерзающая жидкость','ochistitel'=>'Очиститель','pasta'=>'Паста','pena'=>'Пена','pistolet'=>'Пистолет','polirol'=>'Полироль','preobrazovatel'=>'Преобразователь','prisadka'=>'Присадка','prisadka-v-akpp'=>'Присадка в АКПП','prisadka-v-benzin'=>'Присадка в бензин','prisadka-v-dizel'=>'Присадка в дизель','prisadka-s-disulfidom-molibdena'=>'Присадка с дисульфидом молибдена','razmorazhivatel-stekol'=>'Размораживатель стекол','rastvoritel-rzhavchiny'=>'Растворитель ржавчины','salfetka'=>'Салфетка','smazka'=>'Смазка','smazka-dlya-stupits-podshipnikov'=>'Смазка для ступиц подшипников','sprey'=>'Спрей','sredstvo'=>'Средство','sredstvo-dlya-promyvki-dvigatelya'=>'Средство для промывки двигателя','sredstvo-dlya-promyvki-transmissiy'=>'Средство для промывки трансмиссий','stabilizator'=>'Стабилизатор','stabilizator-benzina'=>'Стабилизатор бензина','stekloomyvatel'=>'Стеклоомыватель','stop-shum-gidrokompensatorov'=>'Стоп-шум гидрокомпенсаторов','tormoznaya-zhidkost'=>'Тормозная жидкость','transmissionnoe-maslo'=>'Трансмиссионное масло'];
  public $_type_engine = ['all'=>'Все','akpp'=>'АКПП','benzinovyy'=>'бензиновый','benzinovyy-dizelnyy'=>'бензиновый/дизельный','dizelnyy'=>'дизельный','mkpp'=>'МКПП'];
  public $_sorter = ['title'=>'Названию','price'=>'Цене'];
  public $sorter,$brend;
  public function rules()
  {
    return [
      [['sorter','size','viscosity_1','type_fluid','type_engine','brend'],'save']
    ];
  }

  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'sorter'=>'Сортировать по',
      'brend' => 'Бренд'
    ]);
  }

  public function getBrand(){
    $shiny = Oil::find()->groupBy(['parent_brend_id'])->with('brend')->all();
    return ArrayHelper::getColumn($shiny,'brend.url');
  }
  public function getSort(){return $this->_sorter;}
  public function getSize(){    
    $where = $this->initSearch();    
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['size'])->where($where)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('size')->all(),function($model){return $model->generateUrl($model->size);},'size');     
    }
    return $this->_size;
  }
  public function getViscosity(){  
    $where = $this->initSearch();    
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['viscosity_1'])->where($where)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('viscosity_1')->all(),function($model){return $model->generateUrl($model->viscosity_1);},'viscosity_1');     
    }
    return $this->_viscosity_1;
  }
  public function getType(){    
    $where = $this->initSearch();    
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['type_fluid'])->where($where)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('type_fluid')->all(),function($model){return $model->generateUrl($model->type_fluid);},'type_fluid');     
    }
    return $this->_type_fluid;
  }
  public function getTypeEngine(){
    $where = $this->initSearch();    
    if(count($where) > 0){
      return ['all'=>'Все']+ArrayHelper::map(Oil::find()->select(['type_engine'])->where($where)
        ->andWhere(['>','price',0])->andWhere(['>','ostatok',0])->groupBy('type_engine')->all(),function($model){return $model->generateUrl($model->type_engine);},'type_engine');     
    }
    return $this->_type_engine;
  }  

  private function initSearch(){
    $where = [];
    if($this->size != 'index' && $this->size != '' && $this->size != 'all'){$where['size']=$this->_size[$this->size];}
    if($this->viscosity_1 != 'index' && $this->viscosity_1 != '' && $this->viscosity_1 != 'all'){$where['viscosity_1']=$this->_viscosity_1[$this->viscosity_1];}
    if($this->type_fluid != 'index' && $this->type_fluid != '' && $this->type_fluid != 'all'){$where['type_fluid']=$this->_type_fluid[$this->type_fluid];}
    if($this->type_engine != 'index' && $this->type_engine != '' && $this->type_engine != 'all'){$where['type_engine']=$this->_type_engine[$this->type_engine];}
    return $where;
  }

  public function search($params) {

    $query = self::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['title' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->initParams($params)) {
        return $dataProvider;
    }
    switch($this->sorter){
      case 'price':
      $query->orderBy(['price'=>SORT_ASC]);
      break;
      case 'title':
      $query->orderBy(['title'=>SORT_ASC]);
      break;
    }
    if($this->size != 'all'){
      $query->andFilterWhere(['like','size',$this->_size[$this->size]]);
    }
    if($this->viscosity_1 != 'all'){
      $query->andFilterWhere(['like','viscosity_1',$this->_viscosity_1[$this->viscosity_1]]);
    }
    if($this->type_fluid != 'all'){
      $query->andFilterWhere(['like','type_fluid',$this->_type_fluid[$this->type_fluid]]);
    }
    if($this->type_engine != 'all'){
      $query->andFilterWhere(['like','type_engine',$this->_type_engine[$this->type_engine]]);
    }
    if($this->brend != ''){
      $brend = Brend::find()->where(['url'=>str_replace('brend-','',$this->brend)])->one();
      $query->andFilterWhere(['parent_brend_id' => $brend->id]);
    }

    return $dataProvider;
  }

  private function initParams($params){
    $res = false;
    if(!empty($params['sorter'])){$this->sorter = $params['sorter'];$res = true;}
    if(!empty($params['size'])){$this->size = $params['size'];$res = true;}
    if(!empty($params['viscosity_1'])){$this->viscosity_1 = $params['viscosity_1'];$res = true;}
    if(!empty($params['type_fluid'])){$this->type_fluid = $params['type_fluid'];$res = true;}
    if(!empty($params['type_engine'])){$this->type_engine = $params['type_engine'];$res = true;}
    if($params['brend'] != ''){$this->brend = $params['brend'];$res = true;}
    return $res;
  }
}
