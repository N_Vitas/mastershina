<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "i_cat_elements".
 *
 * @property integer $id
 * @property string $c_code
 * @property string $tire_desc
 * @property string $1c_code_disk
 * @property string $disk_text
 * @property string $a_1c
 * @property string $a_text
 * @property string $m_1c
 * @property string $m_text
 */
class DescriptionTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'i_cat_elements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tire_desc', 'disk_text', 'a_text', 'm_text'], 'string'],
            [['c_code', 'c_code_disk', 'a_1c', 'm_1c'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c_code' => '1c Code',
            'tire_desc' => 'Tire Desc',
            'c_code_disk' => '1c Code Disk',
            'disk_text' => 'Disk Text',
            'a_1c' => 'A 1c',
            'a_text' => 'A Text',
            'm_1c' => 'M 1c',
            'm_text' => 'M Text',
        ];
    }
}
