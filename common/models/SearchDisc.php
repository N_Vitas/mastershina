<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Disc;
use common\models\Vehicle;
use common\models\HelperVendor;

class SearchDisc extends Disc
{
  // Добавляем автомобили в шины
  public $vendor,$car,$year,$modification,$width,$countBorer,$distanceBorer,$wradius;
  public $_vendor = [];
  public $_car = [];
  public $_year = [];
  public $_modification = [];
  public $_radius = [
    'all'=>'Все',
    '12' => '12',
    '13' => '13',
    '14' => '14',
    '15' => '15',
    '16' => '16',
    '17' => '17',
    '17-5' => '17.5',
    '18' => '18',
    '19' => '19',
    '20' => '20',
    '21' => '21',
    '22' => '22',
    '22-5' => '22.5'
  ];
  public $_width = [
    'all'=>'Все',
    '4'=>'4',
    '4-5'=>'4.5',
    '5'=>'5',
    '5-5'=>'5.5',
    '6'=>'6',
    '6-5'=>'6.5',
    '6-75'=>'6.75',
    '7'=>'7',
    '7-5'=>'7.5',
    '8'=>'8',
    '8-25'=>'8.25',
    '8-5'=>'8.5',
    '8-6'=>'8.6',
    '9'=>'9',
    '9-5'=>'9.5',
    '10'=>'10',
    '10-5'=>'10.5',
    '11'=>'11',
    '11-75'=>'11.75'
  ];
  public $_countBorer = [
    'all'=>'Все',
    '3'=>'3',
    '4'=>'4',
    '5'=>'5',
    '6'=>'6',
    '10'=>'10',
  ];
  public $_distanceBorer = [
    'all'=>'Все',
    '20'=>'20',
    '98'=>'98',
    '100'=>'100',
    '105'=>'105',
    '108'=>'108',
    '110'=>'110',
    '112'=>'112',
    '114'=>'114',
    '114-3'=>'114.3',
    '115'=>'115',
    '118'=>'118',
    '120'=>'120',
    '127'=>'127',
    '130'=>'130',
    '135'=>'135',
    '139'=>'139',
    '139-7'=>'139.7',
    '150'=>'150',
    '170'=>'170',
    '225'=>'225',
    '335'=>'335',
  ];
  /**
  */
// 1. Размер (радиус х ширина диска)
// 2. Сверловка (кол-во х расстояние отверстий)
// 3. Диаметр ступицы
// 4. Вылет
// 5. Бренд
// 6. Название
// 7. Тип
// 8. Цвет
  public $_size = ['all'=>'Все','12x4'=>'12x4','13x4-5'=>'13x4.5','13x5'=>'13x5','13x5-5'=>'13x5.5','14x5'=>'14x5','14x5-5'=>'14x5.5','14x6'=>'14x6','15-x6-5'=>'15 x6.5','15x10'=>'15x10','15x5'=>'15x5','15x5-5'=>'15x5.5','15x6'=>'15x6','15x6-5'=>'15x6.5','15x7'=>'15x7','15x7-5'=>'15x7.5','15x8'=>'15x8','16x10'=>'16x10','16x5'=>'16x5','16x5-5'=>'16x5.5','16x6'=>'16x6','16x6-5'=>'16x6.5','16x7'=>'16x7','16x7-5'=>'16x7.5','16x8'=>'16x8','17-5x6-75'=>'17.5x6.75','17x5'=>'17x5','17x6-5'=>'17x6.5','17x7'=>'17x7','17x7-5'=>'17x7.5','17x8'=>'17x8','17x8-5'=>'17x8.5','17x9'=>'17x9','18x5'=>'18x5','18x7'=>'18x7','18x7-5'=>'18x7.5','18x8'=>'18x8','18x8-5'=>'18x8.5','18x9'=>'18x9','19x7-5'=>'19x7.5','19x8'=>'19x8','19x8-5'=>'19x8.5','19x8-6'=>'19x8.6','19x9'=>'19x9','19x9-5'=>'19x9.5','20x10'=>'20x10','20x10-5'=>'20x10.5','20x11'=>'20x11','20x8'=>'20x8','20x8-5'=>'20x8.5','20x9'=>'20x9','20x9-5'=>'20x9.5','21x10'=>'21x10','21x11-5'=>'21x11.5','21x8-5'=>'21x8.5','21x9-5'=>'21x9.5','22-5x11-75'=>'22.5x11.75','22-5x8-25'=>'22.5x8.25','22-5x9'=>'22.5x9','22x10'=>'22x10','22x8-5'=>'22x8.5','22x9'=>'22x9','22x9-5'=>'22x9.5'];

  public $_stupica = [
    'all'=>'Все',
    '0'=>'0',
    '35'=>'35',
    '54'=>'54',
    '54.1'=>'54.1',
    '56'=>'56',
    '56-1'=>'56.1',
    '56-3'=>'56.3',
    '56-5'=>'56.5',
    '56-6'=>'56.6',
    '56-7'=>'56.7',
    '57'=>'57',
    '57-1'=>'57.1',
    '58'=>'58',
    '58-1'=>'58.1',
    '58-5'=>'58.5',
    '58-6'=>'58.6',
    '59'=>'59',
    '59-1'=>'59.1',
    '60'=>'60',
    '60-1'=>'60.1',
    '60-5'=>'60.5',
    '63-3'=>'63.3',
    '63-35'=>'63.35',
    '63-4'=>'63.4',
    '64-1'=>'64.1',
    '65'=>'65',
    '65-1'=>'65.1',
    '66'=>'66',
    '66-1'=>'66.1',
    '66-5'=>'66.5',
    '66-6'=>'66.6',
    '67'=>'67',
    '67-1'=>'67.1',
    '69-1'=>'69.1',
    '69-3'=>'69.3',
    '70-1'=>'70.1',
    '70-2'=>'70.2',
    '70-3'=>'70.3',
    '71-1'=>'71.1',
    '71-5'=>'71.5',
    '71-6'=>'71.6',
    '72-5'=>'72.5',
    '72-55'=>'72.55',
    '72-6'=>'72.6',
    '73'=>'73',
    '73-1'=>'73.1',
    '74'=>'74',
    '74-1'=>'74.1',
    '75-1'=>'75.1',
    '76-1'=>'76.1',
    '77-1'=>'77.1',
    '77-7'=>'77.7',
    '78-1'=>'78.1',
    '79-5'=>'79.5',
    '84-1'=>'84.1',
    '84-2'=>'84.2',
    '87-1'=>'87.1',
    '92-4'=>'92.4',
    '95-3'=>'95.3',
    '98'=>'98',
    '98-5'=>'98.5',
    '101-1'=>'101.1',
    '105-2'=>'105.2',
    '106'=>'106',
    '106-1'=>'106.1',
    '106-2'=>'106.2',
    '106-5'=>'106.5',
    '106-6'=>'106.6',
    '108'=>'108',
    '108-1'=>'108.1',
    '108-5'=>'108.5',
    '109-7'=>'109.7',
    '110'=>'110',
    '110-1'=>'110.1',
    '110-2'=>'110.2',
    '110-3'=>'110.3',
    '110-5'=>'110.5',
    '111'=>'111',
    '111-6'=>'111.6',
    '112'=>'112',
    '114-3'=>'114.3',
    '130'=>'130',
    '160'=>'160',
    '176'=>'176',
    '178'=>'178',
    '28'=>'28'
  ];

  public $_vylet = ['all'=>'Все','1'=>'1','2'=>'2','8'=>'8','10'=>'10','12'=>'12','13'=>'13','14'=>'14','15'=>'15','17'=>'17','18'=>'18','20'=>'20','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','29'=>'29','30'=>'30','31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50','51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59','60'=>'60','62'=>'62','65'=>'65','66'=>'66','67'=>'67','98'=>'98','105'=>'105','106'=>'106','120'=>'120','123'=>'123','145'=>'145','165'=>'165','175'=>'175','177'=>'177','185'=>'185'];
  public $_type = ['all'=>'Все','lеgkоsplavnyе'=>'Легкосплавные','shtampоvannyе'=>'Штампованные',];
  public $_facturer = ['all'=>'Все','4go'=>'4GO','aed'=>'AED','alutec'=>'Alutec','ats'=>'ATS','better'=>'Better','d&p'=>'D&P','eurodisk'=>'Eurodisk','futek'=>'Futek','gr'=>'GR','k&k'=>'K&K','legeartis'=>'LegeArtis','lenso'=>'Lenso','mefro'=>'Mefro','nitro'=>'Nitro','o-green'=>'O Green','replica'=>'Replica','rial'=>'Rial','rplc'=>'RPLC','skad'=>'Skad','stark'=>'Stark','tech-line'=>'Tech Line','yamato'=>'Yamato','gaz'=>'ГАЗ','drugiе-brеndy'=>'Другие бренды','kik'=>'КиК','krkz'=>'КрКЗ','tzsk'=>'ТЗСК','chkpz'=>'ЧКПЗ'];
  public $_color = [
    "all" => "Все",
    "bbk" => "BBK",
    "bd" => "BD",
    "bdm" => "BDM",
    "bfp" => "BFP",
    "bfp-22" => "BFP 22",
    "bfpr" => "BFPR",
    "bfpriep" => "BFPRI(EP)",
    "bkf" => "BKF",
    "bkfp" => "BKFP",
    "bkk" => "BKK",
    "bkm" => "BKM",
    "black" => "Black",
    "black-briliant" => "Black Briliant",
    "black-glossy-polished" => "black glossy polished",
    "black-matt" => "black matt",
    "black-matt-polished" => "black matt polished",
    "black-matt" => "black-matt",
    "blizzard-grey-l-p" => "blizzard grey l p",
    "blizzard-grey-l-p" => "blizzard grey l-p",
    "blizzard-grey-lip-polished" => "blizzard-grey lip polished",
    "blm" => "BLM",
    "bmf" => "BMF",
    "bmfl" => "BMFL",
    "bml" => "BML",
    "byl" => "BYL",
    "carbon-grey" => "carbon grey",
    "carbon-norm" => "CARBON NORM",
    "carbon-grey" => "carbon-grey",
    "cbj" => "CBJ",
    "chrome" => "chrome",
    "chrome-black" => "chrome black",
    "chrome-grey" => "chrome grey",
    "dfs" => "DFS",
    "diamant-schwarz" => "diamant schwarz",
    "diamant-schwarz-f-p" => "diamant schwarz f p",
    "diamant-schwarz-p-t" => "diamant schwarz p t",
    "diamant-schwarz" => "diamant-schwarz ",
    "diamant-schwarz-f-p" => "diamant-schwarz f-p",
    "diamant-silber" => "diamant-silber",
    "ga" => "GA",
    "gaf" => "GAF",
    "gd" => "GD",
    "gm" => "GM",
    "gmbwep" => "GMBW(EP)",
    "gmfp" => "GMFP",
    "gmmf" => "GMMF",
    "gmriep" => "GMRI(EP)",
    "gmrwep" => "GMRW(EP)",
    "graphit" => "graphit",
    "graphit-matt-f-p" => "graphit matt f p",
    "grey" => "grey",
    "grey-matt" => "grey matt",
    "grey-silver" => "grey silver",
    "grey-matt" => "grey-matt",
    "grey-silber" => "grey-silber",
    "grey-silver" => "grey-silver",
    "gw-ray" => "GW-RAY",
    "hb" => "HB",
    "hbk" => "HBK",
    "hbm" => "HBM",
    "hdf" => "HDF",
    "hs" => "HS",
    "ice" => "ICE",
    "mb" => "MB",
    "mb-xrom" => "MB xром",
    "mb-chernyy-s-polirovkoy" => "MB черный с полировкой",
    "mbf" => "MBF",
    "mbfp" => "mbfp",
    "mbfp-22" => "MBFP 22",
    "mbfp22" => "mbfp22",
    "mbfpnorm" => "MBFPnorm",
    "mbmf" => "MBMF",
    "mbmfl" => "MBMFL",
    "mbrl" => "MBRL",
    "mercury" => "MERCURY",
    "mfp" => "MFP",
    "mgmfp" => "MGMFP",
    "mib" => "MIB",
    "ms" => "MS",
    "polar-silber" => "polar silber",
    "polar-silber" => "polar-silber",
    "polar-silver" => "polar-silver",
    "pure-polar" => "PURE POLAR",
    "pure-si" => "PURE SI",
    "pure-sil" => "PURE SIL",
    "pure-sil-norm" => "PURE SIL norm",
    "racing-black-d-p" => "racing black d p",
    "racing-grey" => "racing grey",
    "racing-schwarz" => "racing schwarz",
    "racing-schwarz-h-p" => "racing schwarz h p",
    "racing-black" => "racing-black",
    "racing-schwarz" => "racing-schwarz",
    "rallye-black" => "rallye black",
    "rallye-whit" => "rallye whit",
    "rds" => "RDS",
    "roxx-emerald" => "Roxx Emerald",
    "roxx-marine" => "Roxx Marine",
    "royal-silber" => "royal silber",
    "royal-silver" => "royal-silver",
    "s" => "S",
    "s-2kit" => "S (2KIT)",
    "s1kit" => "S(1KIT)",
    "s22" => "S22",
    "satin-silber" => "satin silber",
    "schwarz" => "schwarz",
    "sf" => "SF",
    "sfp" => "SFP",
    "shb" => "SHB",
    "silver" => "Silver",
    "silver-black" => "silver black",
    "silver-bronze" => "silver bronze",
    "silver-grey" => "silver grey",
    "silver-black" => "silver-black",
    "silver-grey" => "silver-grey",
    "smf" => "SMF",
    "smfl" => "SMFL",
    "snow" => "SNOW",
    "sterling-silber" => "sterling silber",
    "sterling-silber-p-t" => "sterling silber p t",
    "sterling-silber" => "sterling-silber",
    "titanium" => "titanium",
    "w" => "W",
    "wfp" => "WFP",
    "white" => "White",
    "white-r22" => "White-R22",
    "wrep" => "WR(EP)",
    "wriep" => "WRI(EP)",
    "wrwep" => "WRW(EP)",
    "wsrg" => "WSRG",
    "x-ray" => "X RAY",
    "x-ray" => "X-RAY",
    "xrom" => "xром",
    "almaz" => "алмаз",
    "almaz-black" => "Алмаз black",
    "almaz-antratsit" => "алмаз антрацит",
    "almaz-belyy" => "алмаз белый",
    "almaz-blek-aurum" => "алмаз блэк аурум",
    "almaz-vayt" => "алмаз вайт",
    "almaz-grey" => "алмаз грей",
    "almaz-matovyy" => "алмаз матовый",
    "almaz-met" => "Алмаз мэт",
    "almaz-platina" => "алмаз платина",
    "almaz-super" => "алмаз супер",
    "almaz-chernyy" => "алмаз черный",
    "almaz--belyy" => "алмаз- белый",
    "almaz-antratsit" => "алмаз-антрацит",
    "almaz-belyy" => "алмаз-белый",
    "almaz-blek" => "алмаз-блэк",
    "almaz-grey" => "алмаз-грей",
    "almaz-matovyy" => "алмаз-матовый",
    "almaz-met" => "алмаз-мэт",
    "almaz-selena" => "алмаз-селена",
    "almaz-chernyy" => "алмаз-черный",
    "antratsit-almaz" => "антрацит алмаз",
    "antratsit-almaz-matovyy" => "антрацит алмаз матовый",
    "bez-tsveta" => "без цвета",
    "belyy" => "белый",
    "binario" => "Бинарио",
    "blek-aurum" => "блэк аурум",
    "blek-platinum" => "блэк платинум",
    "blek-platinum" => "блэк-платинум",
    "brimetall" => "бриметалл",
    "bts" => "бц",
    "venge" => "Венге",
    "galvano" => "гальвано",
    "gonochnyy" => "гоночный",
    "grey" => "грей",
    "grey-matovyy" => "грей-матовый",
    "knr" => "кнр",
    "matovyy-polirovannyy" => "матовый полированный",
    "metall" => "металл",
    "metallik" => "металлик",
    "platina" => "платина",
    "selena" => "селена",
    "selena-kombi" => "селена комби",
    "selena-super" => "селена супер",
    "sereb-chernyy" => "сереб черный",
    "serebristye" => "серебристые",
    "serebristyy" => "серебристый",
    "serebro" => "серебро",
    "seryy" => "серый",
    "seryy-mefro" => "серый Mefro",
    "silver" => "сильвер",
    "sparkl-silver" => "спаркл силвер",
    "stalnye" => "стальные",
    "temno-seryy" => "темно серый",
    "chernye-polirovannoe-serebro" => "черные, полированное серебро",
    "chernyy" => "черный",
    "chernyy-glyantsevyy-polirovannyy" => "черный глянцевый полированный",
    "chernyy-matovyy" => "черный матовый",
    "chernyy-s-dymkoy" => "черный с дымкой",
    "chernyy-matovyy" => "черный-матовый"
  ];
  private $_sorter = ['title'=>'Названию','price'=>'Цене'];
  public $sorter,$brend;

  public function init(){
    $this->_vendor = HelperVendor::getMake();
    $this->_car = HelperVendor::getModel();
    $this->_year = HelperVendor::getYear();
    $this->_modification = HelperVendor::getModification();
    $this->type = 'all';
    return parent::init();
  }

  public function rules()
  {
    return [
      [['sorter','size','radius','type','manufacturer','vendor','car',
      'year','modification','width','countBorer','distanceBorer','vylet','stupica','brend'],'save']
    ];
  }

  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(),[
      'sorter'=>'Сортировать по',
      'vendor'=>'Марка',
      'car'=>'Модель',
      'year'=>'Год',
      'modification'=>'Модификация',
      'size' => 'Размер',
      'width' => 'Ширина',
      'countBorer' => 'Кол. отверстий',
      'distanceBorer' => 'Раст. отверстий',
      'vylet' => 'Вылет',
      'stupica' => 'Диаметр ступицы',
      'wradius' => 'Радиус',
      'brend' => 'Бренд',
      // Ширина диска Кол. отверстий Раст. отверстий Диаметр ступицы Вылет Производитель Название Тип Цвет
    ]);
  }

  public function getBrand(){
    $shiny = Disc::find()->groupBy(['parent_brend_id'])->with('brend')->all();
    return ArrayHelper::getColumn($shiny,'brend.url');
  }
  public function replaceKey($model,$key){
    $r = new Disc();
    return $r->generateUrl($model->$key);
  }
  public function getSort(){return $this->_sorter;}
  public function getSize(){return $this->_size;}
  public function getWidth(){return $this->_width;}
  public function getRadius(){return $this->_radius;}
  public function getCount(){return $this->_countBorer;}
  public function getDistance(){return $this->_distanceBorer;}
  public function getStupica(){return $this->_stupica;}
  public function getVylet(){return $this->_vylet;}
  public function getManufacturer(){return $this->_facturer;}  
  public function getType(){return $this->_type;}
  public function getColor(){return $this->_color;}

  public function getVendor($where=[]){
    if(isset($where['vendor']))unset($where['vendor']);
    $this->_vendor = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['vendor'])->where($where)->groupBy('vendor')->all(),function($model){return $this->replaceKey($model,'vendor');},'vendor');
    return $this->_vendor;
  }
  public function getCar($where=[]){
    if(isset($where['car']))unset($where['car']);
    $this->_car = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['car'])->where($where)->groupBy('car')->all(),function($model){return $this->replaceKey($model,'car');},'car');
    return $this->_car;
  }
  public function getYear($where=[]){
    if(isset($where['year']))unset($where['year']);
    $this->_year = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),function($model){return $this->replaceKey($model,'year');},'year');
    return $this->_year;
    // return ['all'=>'Все',ArrayHelper::map(Vehicle::find()->select(['year'])->where($where)->groupBy('year')->all(),'year','year')];
  }
  public function getModification($where=[]){
    if(isset($where['modification']))unset($where['modification']);
    $this->_modification = ['all'=>'Все']+ArrayHelper::map(Vehicle::find()->select(['modification'])->where($where)->groupBy('modification')->all(),function($model){return $this->replaceKey($model,'modification');},'modification');
    return $this->_modification;
  }
  // public function scenarios()
  // {  
  //     return Model::scenarios();
  // }

  public function search($params) {

    $query = self::find()->where(['status' => 1]);
    $query->andWhere(['>','price',0]);
    $query->andWhere(['>','ostatok',0]);
    // собираем масив датапровайдера
    $dataProvider = new ActiveDataProvider([
      'query' => $query, // конечный запрос в базу за данными
      // Лимит постраничной навигации
      'pagination' => [
        'pagesize' => 12,
      ],
      // сортировака по полям
      'sort' => [
        'defaultOrder' => ['price' => SORT_ASC], // сортировка по умолчанию
      ],
    ]);
    if (!$this->initParams($params)) {
        return $dataProvider;
    }
    $auto=[];
    if($this->vendor != '' && $this->vendor != 'all'){$auto['vendor']=$this->_vendor[$this->vendor];}
    if($this->car != '' && $this->car != 'all'){$auto['car']=$this->_car[$this->car];}
    if($this->year != '' && $this->year != 'all'){$auto['year']=$this->_year[$this->year];}
    if($this->modification != '' && $this->modification != 'all'){$auto['modification']=$this->_modification[$this->modification];}
    if(count($auto)>0){
      $vehicle = Vehicle::find()->select(['param_pcd','param_dia'])->where($auto)->groupBy(['param_pcd','param_dia'])->asArray()->all();
      $param_pcd =[0];
      $param_dia =[0];
      if(count($vehicle) > 0){
        foreach ($vehicle as $key => $value) {
          // borer
          if($value['param_pcd'] != ''){
            $param_pcd[] = str_replace('*', 'x', $value['param_pcd']);            
          }
          // stupica
          if($value['param_dia'] != ''){
            $param_dia[] = $value['param_dia'];            
          }
        }
        $param_pcd = array_unique($param_pcd);
        $param_dia = array_unique($param_dia);      
      }  
      $query->andFilterWhere(['in','borer',$param_pcd]);
      // $query->andFilterWhere(['in','stupica',$param_dia]);
    }
    switch($this->sorter){
      case 'price':
      $query->orderBy(['price'=>SORT_ASC]);
      break;
      case 'title':
      $query->orderBy(['title'=>SORT_ASC]);
      break;
    }
    if($this->width != null && $this->width != 'all'){
      $query->andFilterWhere(['like','size','x'.$this->_width[$this->width]]);
    }
    if($this->wradius != null && $this->wradius != 'all'){
      $query->andFilterWhere(['like','size',$this->_radius[$this->wradius].'x']);
    }
    if($this->radius != null && $this->radius != 'all'){
      $query->andFilterWhere(['like','radius',$this->_vylet[$this->radius]]);
    }
    if($this->type != null && $this->type != 'all'){
      $query->andFilterWhere(['type' => $this->_type[$this->type]]);
    }
    if($this->manufacturer != null && $this->manufacturer != 'all'){
      $query->andFilterWhere(['manufacturer' => $this->_facturer[$this->manufacturer]]);
    }
    if($this->countBorer != null && $this->countBorer != 'all'){
      $query->andFilterWhere(['like','borer',$this->_countBorer[$this->countBorer].'x']);
    }
    if($this->distanceBorer != null && $this->distanceBorer != 'all'){
      $query->andFilterWhere(['like','borer','x'.$this->_distanceBorer[$this->distanceBorer]]);
    }
    if($this->stupica != null && $this->stupica != 'all'){
      $query->andFilterWhere(['like','stupica',$this->_stupica[$this->stupica]]);
    }
    if($this->color != null && $this->color != 'all'){
      $query->andFilterWhere(['like','color',$this->_color[$this->color]]);
    }
    if($this->brend != ''){
      $brend = Brend::find()->where(['url'=>str_replace('brend-','',$this->brend)])->one();
      $query->andFilterWhere(['parent_brend_id' => $brend->id]);
    }
    return $dataProvider;
  }

  private function initParams($params){
    $res = false;
    if(!empty($params['width'])){$this->width = $params['width'];$res = true;}
    if(!empty($params['wradius'])){$this->wradius = $params['wradius'];$res = true;}
    if(!empty($params['count'])){$this->countBorer = $params['count'];$res = true;}
    if(!empty($params['borer'])){$this->distanceBorer = $params['borer'];$res = true;}
    if(!empty($params['stupica'])){$this->stupica = $params['stupica'];$res = true;}
    if(!empty($params['radius'])){$this->radius = $params['radius'];$res = true;}
    if(!empty($params['facturer'])){$this->manufacturer = $params['facturer'];$res = true;}
    if(!empty($params['color'])){$this->color = $params['color'];$res = true;}
    if(!empty($params['type'])){$this->type = $params['type'];$res = true;}
    if(!empty($params['vendor'])){$this->vendor = $params['vendor'];$res = true;}
    if(!empty($params['car'])){$this->car = $params['car'];$res = true;}
    if(!empty($params['year'])){$this->year = $params['year'];$res = true;}
    if(!empty($params['modification'])){$this->modification = $params['modification'];$res = true;}
    if($params['brend'] != ''){$this->brend = $params['brend'];$res = true;}
    // if(!empty($params['sorter'])){$this->sorter = $params['sorter'];$res = true;}
    // if(!empty($params['size'])){$this->size = $params['size'];$res = true;}
    // if(!empty($params['radius'])){$this->radius = $params['radius'];$res = true;}
    // if(!empty($params['type'])){$this->type = $params['type'];$res = true;}
    // if(!empty($params['manufacturer'])){$this->manufacturer = $params['manufacturer'];$res = true;}
    return $res;
  }
}
