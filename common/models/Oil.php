<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "oil".
 *
 * @property integer $id
 * @property integer $parent_brend_id
 * @property string $title
 * @property integer $price
 * @property integer $old_price
 * @property string $currency
 * @property string $model
 * @property integer $delivery
 * @property string $ostatok
 * @property string $picture
 * @property string $viscosity_1
 * @property string $viscosity_2
 * @property string $type_fluid
 * @property string $type_engine
 * @property string $size
 * @property integer $status
 * @property string $code
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Oil extends \yii\db\ActiveRecord implements CartPositionInterface
{
  use CartPositionTrait;
  /**
   * @getId для корзины
   */

  public function getId()
  {
    return 'oil-'.$this->id;
  }
  public function getPrice(){
    return $this->price;
  }
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'oil';
  }

  // public $brand;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['parent_brend_id', 'title', 'ostatok', 'picture', 'type_fluid', 'code'], 'required'],
      [['parent_brend_id', 'price', 'old_price', 'delivery', 'status'], 'integer'],
      [['model','description'], 'string'],
      [['title', 'picture', 'type_fluid', 'type_engine', 'size', 'url', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
      [['currency'], 'string', 'max' => 10],
      [['ostatok', 'code'], 'string', 'max' => 25],
      [['viscosity_1', 'viscosity_2'], 'string', 'max' => 100],
      [['brand'],'safe'],
    ];
  }

  // public function behaviors()
  // {
  //   return [
  //     [
  //       'class' => SluggableBehavior::className(),
  //       // 'attribute' => ['title','type_fluid'],
  //       'immutable' => true,
  //       'ensureUnique' => true,
  //       'slugAttribute' => 'url',
  //       'value' => function($event){
  //         $search =  [' ','(',')','/','.','а','e','й','o','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
  //         $replace = ['-','','','-','','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
  //         return trim(str_replace($search, $replace, mb_strtolower($event->sender->title."-".$event->sender->type_fluid)));
  //       },
  //     ],
  //   ];
  // }
  
  // public function afterFind(){
  //   // $this->parent_brend_id = $this->getBrend();
  //   // $model = $this->getBrend();
  //   $this->brand = $this->brend->title;
  //   // var_dump($this->brend->title);die;
  //   return parent::afterFind();
  // }
  
  public function getPicture(){
    $picture =  substr(strrchr($this->picture, '.'), 1);
    $picture = $picture != '' ? $this->picture : '/upload/none_pic.jpg';
    return $picture;
  }
  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'parent_brend_id' => 'Бренд',
      'title' => 'Наименование',
      'price' => 'Цена',
      'brand' => 'Бренд',
      'old_price' => 'Старая цена',
      'currency' => 'Валюта',
      'model' => 'Наименование',
      'delivery' => 'Доставка',
      'ostatok' => 'В наличии',
      'picture' => 'Изображение',
      'viscosity_1' => 'Вязкость',
      'viscosity_2' => 'Вязкость',
      'type_fluid' => 'Тип жидкости',
      'type_engine' => 'Тип двигателя',
      'size' => 'Объем',
      'status' => 'Статус',
      'code' => 'Код',
      'url' => 'Url адрес',
      'meta_title' => 'Сео заголовок',
      'meta_keywords' => 'Сео теги',
      'meta_description' => 'Сео описание',
      'description' => 'Описание',
    ];
  }

  public function generateUrl($attr){
    $search = [' ','+',',','кг.','км.','/4х4',' ','(',')','/','.','а','е','й','о','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
    $replace = ['','','','kg','km','','-','','','-','-','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
    return str_replace($search, $replace, mb_strtolower(trim($attr)));
  }
  
  public function getBrend(){
    return $this->hasOne(Brend::className(), ['id' => 'parent_brend_id']);
  }
  
  public function getDescription(){
    return $this->hasOne(Description::className(), ['code' => 'code']);
  }
  public function loadPitstop($model) {
    if($brend = Brend::find()->where(['title' => $model->m_brand])->one()){
      $this->parent_brend_id = $brend->id;
    } else {
      $brend = new Brend();
      $brend->title = $model->m_brand;
      $brend->url = $brend->generateUrl($model->m_brand);
      $brend->save();
      $this->parent_brend_id = $brend->id;
    }
    $this->title = $model->m_name;
    $this->price = $model->m_price;
    $this->old_price = $model->m_price1;
    $this->currency = 'KZT';
    $this->model = $model->m_brand;
    $this->delivery = 1;
    $this->ostatok = $model->m_kol;
    $this->picture = $model->m_img;
    $this->viscosity_1 = $model->m_vz1;
    $this->viscosity_2 = $model->m_vz2;
    $this->type_fluid = $model->m_type;
    $this->type_engine = $model->m_dvig;
    $this->size = $model->m_ob;
    $this->status = 1;
    $this->code = $model->m_1c;
    $this->url = $this->generateUrl($model->m_brand." ".$this->title." ".$this->code);
  }
}
