<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vehicle".
 *
 * @property integer $id
 * @property string $vendor
 * @property string $car
 * @property string $year
 * @property string $modification
 * @property string $param_pcd
 * @property string $param_dia
 * @property string $param_nut
 * @property string $param_bolt
 * @property string $tyres_factory
 * @property string $tyres_replace
 * @property string $tyres_tuning
 * @property string $wheels_factory
 * @property string $wheels_replace
 * @property string $wheels_tuning
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor', 'car', 'year', 'modification', 'param_pcd', 'param_dia', 'param_nut', 'param_bolt', 'tyres_factory', 'tyres_replace', 'tyres_tuning', 'wheels_factory', 'wheels_replace', 'wheels_tuning'], 'required'],
            [['tyres_factory', 'tyres_replace', 'tyres_tuning', 'wheels_factory', 'wheels_replace', 'wheels_tuning'], 'string'],
            [['vendor', 'car', 'year', 'modification'], 'string', 'max' => 255],
            [['param_pcd', 'param_nut', 'param_bolt'], 'string', 'max' => 32],
            [['param_dia'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendor' => 'Vendor',
            'car' => 'Car',
            'year' => 'Year',
            'modification' => 'Modification',
            'param_pcd' => 'Param Pcd',
            'param_dia' => 'Param Dia',
            'param_nut' => 'Param Nut',
            'param_bolt' => 'Param Bolt',
            'tyres_factory' => 'Tyres Factory',
            'tyres_replace' => 'Tyres Replace',
            'tyres_tuning' => 'Tyres Tuning',
            'wheels_factory' => 'Wheels Factory',
            'wheels_replace' => 'Wheels Replace',
            'wheels_tuning' => 'Wheels Tuning',
        ];
    }
}
