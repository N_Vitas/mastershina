<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Person".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Surname
 * @property string $Patronymic
 * @property string $RNN
 * @property string $INN
 * @property string $Birthdate
 * @property integer $Gender
 * @property string $Email
 * @property integer $Status
 * @property string $Balance
 * @property string $RegDate
 * @property string $ActivDate
 * @property string $Login
 * @property string $Password
 * @property double $BlockedBalance
 * @property double $CrLimit
 * @property string $Settings
 * @property integer $ApiEnabled
 * @property integer $join
 * @property integer $last_login
 * @property integer $logins
 * @property integer $howRegister
 * @property integer $smsSend
 * @property integer $distribution
 * @property integer $typeOfUser
 * @property integer $IDParent
 * @property integer $Destination
 * @property string $Password_T
 * @property integer $CounterPassword
 * @property string $CellPhone
 * @property integer $OldId
 * @property integer $FPerson
 *
 * @property PushDeliverySent[] $pushDeliverySents
 * @property PushPersonServiceDebt[] $pushPersonServiceDebts
 * @property PushPersonStaticGroupList[] $pushPersonStaticGroupLists
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Person';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Surname', 'Patronymic', 'RNN', 'INN', 'Email', 'Login', 'Password', 'Settings', 'Password_T', 'CellPhone'], 'string'],
            [['Birthdate', 'RegDate', 'ActivDate'], 'safe'],
            [['Gender', 'Status', 'ApiEnabled', 'join', 'last_login', 'logins', 'howRegister', 'smsSend', 'distribution', 'typeOfUser', 'IDParent', 'Destination', 'CounterPassword', 'OldId', 'FPerson'], 'integer'],
            [['Balance', 'BlockedBalance', 'CrLimit'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Surname' => 'Surname',
            'Patronymic' => 'Patronymic',
            'RNN' => 'Rnn',
            'INN' => 'Inn',
            'Birthdate' => 'Birthdate',
            'Gender' => 'Gender',
            'Email' => 'Email',
            'Status' => 'Status',
            'Balance' => 'Balance',
            'RegDate' => 'Reg Date',
            'ActivDate' => 'Activ Date',
            'Login' => 'Login',
            'Password' => 'Password',
            'BlockedBalance' => 'Blocked Balance',
            'CrLimit' => 'Cr Limit',
            'Settings' => 'Settings',
            'ApiEnabled' => 'Api Enabled',
            'join' => 'Join',
            'last_login' => 'Last Login',
            'logins' => 'Logins',
            'howRegister' => 'How Register',
            'smsSend' => 'Sms Send',
            'distribution' => 'Distribution',
            'typeOfUser' => 'Type Of User',
            'IDParent' => 'Idparent',
            'Destination' => 'Destination',
            'Password_T' => 'Password  T',
            'CounterPassword' => 'Counter Password',
            'CellPhone' => 'Cell Phone',
            'OldId' => 'Old ID',
            'FPerson' => 'Fperson',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPushDeliverySents()
    {
        return $this->hasMany(PushDeliverySent::className(), ['IDPerson' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPushPersonServiceDebts()
    {
        return $this->hasMany(PushPersonServiceDebt::className(), ['IDPerson' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPushPersonStaticGroupLists()
    {
        return $this->hasMany(PushPersonStaticGroupList::className(), ['IDPerson' => 'ID']);
    }
}
