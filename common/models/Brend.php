<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "brend".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $text
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $status
 */
class Brend extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'brend';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['title'], 'required'],
      [['text'], 'string'],
      [['status'], 'integer'],
      [['title', 'url', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
      [['title'], 'unique'],
    ];
  }

  public function behaviors()
  {
    return [
      [
        'class' => SluggableBehavior::className(),
        // 'attribute' => 'title',
        'immutable' => true,
        'ensureUnique' => true,
        'slugAttribute' => 'url',
        'value' => function($event){
          return $this->generateUrl($event->sender->title);
        },
      ],
    ];
  }
  public function generateUrl($attr){
    $search = ['кг.','км.','/4х4',' ','(',')','/','.','а','е','й','о','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
    $replace = ['kg','km','','-','','','-','-','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
    return str_replace($search, $replace, mb_strtolower(trim($attr)));
  }
  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'title' => 'Нименование',
      'url' => 'Url адрес',
      'text' => 'Описание',
      'meta_title' => 'Сео заголовок',
      'meta_keywords' => 'Сео теги',
      'meta_description' => 'Сео описание',
      'status' => 'Статус',
    ];
  }
}
