<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "auto".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $shiny_id
 * @property integer $disk_id
 */
class Auto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'shiny_id', 'disk_id','auto_id'], 'integer'],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'auto_id' => 'Auto_id ID',
            'shiny_id' => 'Shiny ID',
            'disk_id' => 'Disk ID',
        ];
    }
}
