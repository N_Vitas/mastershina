<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "battery".
 *
 * @property integer $id
 * @property integer $parent_brend_id
 * @property string $title
 * @property integer $price
 * @property integer $old_price
 * @property string $currency
 * @property string $picture
 * @property string $model
 * @property integer $delivery
 * @property string $ostatok
 * @property string $article
 * @property string $capacity
 * @property string $current
 * @property string $polarity
 * @property integer $length
 * @property integer $width
 * @property integer $height
 * @property string $country
 * @property string $assurance
 * @property integer $voltage
 * @property integer $status
 * @property string $code
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Battery extends \yii\db\ActiveRecord implements CartPositionInterface
{
  use CartPositionTrait;
  /**
   * @getId для корзины
   */

  public function getId()
  {
    return 'battery-'.$this->id;
  }
  public function getPrice(){
    return $this->price;
  }
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'battery';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['parent_brend_id', 'title', 'picture', 'ostatok', 'capacity', 'current', 'polarity', 'voltage', 'code'], 'required'],
      [['parent_brend_id', 'price', 'old_price', 'delivery', 'length', 'width', 'height', 'voltage', 'status'], 'integer'],
      [['model','description'], 'string'],
      [['title', 'picture', 'country', 'assurance', 'url', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
      [['currency'], 'string', 'max' => 10],
      [['ostatok', 'article', 'capacity', 'current', 'polarity', 'code'], 'string', 'max' => 25],
    ];
  }
  
  // public function behaviors()
  // {
  //   return [
  //     [
  //       'class' => SluggableBehavior::className(),
  //       // 'attribute' => 'title',
  //       'immutable' => true,
  //       'ensureUnique' => true,
  //       'slugAttribute' => 'url',
  //       'value' => function($event){
  //         $search = [' ','(',')','/','.','а','e','й','o','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
  //         $replace = ['-','','','-','','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
  //         return trim(str_replace($search, $replace, mb_strtolower($event->sender->title)));
  //       },
  //     ],
  //   ];
  // }
  /**
   * @inheritdoc
   */

  public function getPicture(){
    $picture =  substr(strrchr($this->picture, '.'), 1);
    $picture = $picture != '' ? $this->picture : '/upload/none_pic.jpg';
    return $picture;
  }
  
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'parent_brend_id' => 'Бренд',      
      'title' => 'Заголовок',
      'price' => 'Цена',
      'old_price' => 'Старая Цена',
      'currency' => 'Валюта',
      'picture' => 'Изображение',
      'model' => 'Наименование',
      'delivery' => 'Доставка',
      'ostatok' => 'В наличии',
      'article' => 'Артикул',
      'capacity' => 'Ёмкость',
      'current' => 'Стартовый ток',
      'polarity' => 'Полярность',
      'length' => 'Длина',
      'width' => 'Ширина',
      'height' => 'Высота',
      'country' => 'Страна',
      'assurance' => 'Гарантия',
      'voltage' => 'Напряжение',
      'status' => 'Статус',
      'code' => 'Код',
      'url' => 'Url адрес',
      'meta_title' => 'Сео заголовок',
      'meta_keywords' => 'Сео теги',
      'meta_description' => 'Сео описание',
      'description' => 'Описание',
    ];
  }
  public function generateUrl($attr){
    $search = [' ','+',',','кг.','км.','/4х4',' ','(',')','/','.','а','е','й','о','у','ш','э','б','ё','к','п','ф','щ','ю','в','ж','л','р','х','ъ','г','з','м','с','ц','ы','д','и','н','т','ч','ь','я'];
    $replace = ['','','','kg','km','','-','','','-','-','a','e','y','o','u','sh','e','b','e','k','p','f','shch','yu','v','zh','l','r','h','','g','z','m','s','ts','y','d','i','n','t','ch','','ya'];
    return str_replace($search, $replace, mb_strtolower(trim($attr)));
  }
  public function getBrend(){
    return $this->hasOne(Brend::className(), ['id' => 'parent_brend_id']);
  }
  public function getDescription(){
    return $this->hasOne(Description::className(), ['code' => 'code']);
  }
  public function loadPitstop($model) {
    if($brend = Brend::find()->where(['title' => $model->a_brand])->one()){
      $this->parent_brend_id = $brend->id;
    } else {
      $brend = new Brend();
      $brend->title = $model->a_brand;
      $brend->url = $brend->generateUrl($model->a_brand);
      $brend->save();
      $this->parent_brend_id = $brend->id;
    }
    $this->title = $model->a_name;
    $this->price = $model->a_price;
    $this->old_price = $model->a_price1;
    $this->currency = 'KZT';
    $this->picture = $model->a_img;
    $this->model = $model->a_brand;
    $this->delivery = 1;
    $this->ostatok = $model->a_kol;
    $this->article = $model->a_act;
    $this->capacity = $model->a_em;
    $this->current = $model->a_tok;
    $this->polarity = $model->a_pol;
    $this->length = $model->a_length;
    $this->width = $model->a_width;
    $this->height = $model->a_height;
    $this->country = $model->a_proiz;
    $this->assurance = $model->a_gar;
    $this->voltage = $model->a_nap;
    $this->status = 1;
    $this->code = $model->a_1c;
    $this->url = $this->generateUrl($model->a_brand." ".$this->title." ".$this->code);
  }
}
