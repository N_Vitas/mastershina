<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@ftp', dirname(dirname(__DIR__)) . '/ftp');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
