<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'noreply@mastershina.kz',//'no-replay.mastershina.kz@yandex.ru',
    'user.passwordResetTokenExpire' => 3600,
];
